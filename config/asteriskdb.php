<?php 

require_once "global.php";

$conexion2 = new mysqli(DB_HOST2, DB_USERNAME2, DB_PASSWORD2, DB_NAME2);

//mysqli_query($conexion, 'SET NAMES "'.DB_ENCODE.'"');
//mysqli_query($conexion, 'SET lc_time_names="'.DB_NAMES.'"');

if(mysqli_connect_errno()){
	printf("Fallo la conexion con la BD: %s \n", mysqli_connect_error());
	exit();
}


if(!function_exists('ejecutarConsulta2')){
    
	function ejecutarConsulta2($sql){
		global $conexion2;
		$query = $conexion2->query($sql);
		return $query;
	}

	function ejecutarConsultaSimpleFila2($sql){
		global $conexion2;
		$query = $conexion2->query($sql);
		$row = $query->fetch_assoc();
		return $row;
	}

	function ejecutarConsulta_retornarID2($sql){
		global $conexion2;
		$query = $conexion2->query($sql);
		return $conexion2->insert_id;
	}

	function ejecutarConsu_retornarID2($sql){
		global $conexion2;
		$query = $conexion2->query($sql);
		if($query){
			return $conexion2->insert_id;
		}else{
			return false;
		}
	}

	function limpiarCadena2($str){
		global $conexion2;
		$str = mysqli_real_escape_string($conexion2, trim($str));
		return htmlspecialchars($str);
	}

}


?>