<?php
require_once '../public/build/lib/PHPMailer/class.phpmailer.php';
require_once '../public/build/lib/PHPMailer/class.smtp.php';

require_once '../config/global.php';

function SendEmail($objParams, $isTesting = TESTINGMODE){
	//configuracion por defecto
    $Mailer = new PHPMailer();
    $Mailer->isSMTP();
    $Mailer->CharSet = 'UTF-8';
    $Mailer->SMTPAuth = true;
    $Mailer->SMTPSecure = "tls";
    $Mailer->SMTPDebug = 0;
    $Mailer->Debugoutput = 'html';

    //configuracion estatica pero modificable
    //ideal no cambiar cuenta de envio de las notificaciones
    $Mailer->Port     = (isset($objParams->port) ? $objParams->port : 587);
    $Mailer->Host     = (isset($objParams->host) ? $objParams->host : "www.fabrimetalsa.cl");
    $Mailer->Username = (isset($objParams->userName) ? $objParams->userName : "notificaciones@fabrimetalsa.cl");
    $Mailer->Password = (isset($objParams->passWord) ? $objParams->passWord : "*RUJQtbV!wK*");
    $Mailer->From     = (isset($objParams->emailFrom) ? $objParams->emailFrom : "notificaciones@fabrimetalsa.cl");
    $Mailer->FromName = (isset($objParams->fromName) ? $objParams->fromName : "Sistema de Notificaciones Fabrimetal");

    //configuracion variable
    $Mailer->Subject  = (isset($objParams->subject) ? $objParams->subject : "Sol. Análisis de Falla N° ");
    $Mailer->msgHTML($objParams->message);
    //lista de destinatarios
    if (isset($objParams->emails)) {
        if (count($objParams->emails)) {
            for($i = 0; $i < count($objParams->emails); $i ++){
                //verifico que en el campo email vengan varios emails separados por ;
                $email = trim($objParams->emails[$i]->email);
                if (strpos($email, ';')) {
                    $addr = explode(';', $email);
                    foreach ($addr as $ad) {
                        $Mailer->addAddress( trim($ad), '' );
                    }
                }
                else {
                    $Mailer->addAddress(
                                            $objParams->emails[$i]->email,
                                            $objParams->emails[$i]->name
                                       );
                }

            }
        }
    }
    //lista de adjuntos
    if (isset($objParams->attachments)) {
    	if (count($objParams->attachments)) {
			for($i = 0; $i < count($objParams->attachments); $i ++){
		        $Mailer->AddStringAttachment(
		        								$objParams->attachments[$i]->string,
		        								$objParams->attachments[$i]->filename,
		        								$objParams->attachments[$i]->encoding,
		        								$objParams->attachments[$i]->type,
		        							);
		    }
    	}
    }

    if ($isTesting) {
    	//para usar con software Test Mail Server Tool
    	$Mailer->SMTPAuth = false;
    	$Mailer->SMTPSecure = "";
    	$Mailer->Port = 25;
    	$Mailer->Host = 'localhost';
    }

    //resultado final se devuelve como objeto
  	$result = new stdClass();
    if (!$Mailer->send()) {
    	$result->send = 0;
		$result->message = "Error al enviar correo: " . $Mailer->ErrorInfo . "<br>";
    } else {
    	$result->send = 1;
		$result->message = "Correo enviado exitosamente<br>";
    }
    
    return $result;
}
?>