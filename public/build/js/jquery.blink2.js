(function($) {
    $.fn.blink = function(options) {
        var defaults = { delay: 1000 };
        var options = $.extend(defaults, options);
        return $(this).each(function(idx, itm) {
            var handle = setInterval(function() {
                if ($(itm).is('.blinker'))
                    $(itm).fadeTo(300, 0.7).fadeTo(300, 1.0);
            }, options.delay);

            $(itm).data('handle', handle);
        });
    }
    $.fn.unblink = function() {
        return $(this).each(function(idx, itm) {
            var handle = $(itm).data('handle');
            if (handle) {
                clearInterval(handle);
                $(itm).fadeTo(300, 1.0).fadeTo(300, 1.0);
                $(itm).removeClass("blinker");
                $(itm).addClass('unblinker');
            }
        });
    }
}(jQuery))