<?php
ob_start();
session_start();
require 'header.php';
?>
<div class="right_col" role="main">
    <div class="row text-center">
        <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h3>MANTENCIÓN (<span id="encartera"></span>)</h3> 
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="col-md-7 col-sm-12 col-xs-12" style="overflow:hidden;">
                        <div class="row tile_count ">
                            <div class="col-md-3 col-sm-6 col-xs-12 tile_stats_count">
                                <span class="count_top"><i class="fa fa-clock-o"></i> #GSE Hoy</span>
                                <div class="count" id="mdia"></div>
                                <span class="count_bottom"><i class=""><i class="fa fa-sort-asc"></i>N° </i> GSE - Hoy</span>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12 tile_stats_count">
                                <span class="count_top"><i class="fa fa-clock-o"></i> Porcentaje</span>
                                <div class="count" id="mpordia"> </div>
                                <span class="count_bottom"><i class=""><i class="fa fa-sort-asc"></i>% </i> GSE - Hoy</span>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12 tile_stats_count">
                                <span class="count_top"><i class="fa fa-clock-o"></i> #GSE Mes</span>
                                <div class="count" id="mmes"></div>
                                <span class="count_bottom"><i class=""><i class="fa fa-sort-asc"></i>N° </i> GSE - Mes</span>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12 tile_stats_count">
                                <span class="count_top"><i class="fa fa-clock-o"></i> Porcentaje</span>
                                <div class="count" id="mpormes"></div>
                                <span class="count_bottom"><i class=""><i class="fa fa-sort-asc"></i>% </i> GSE - Mensual</span>
                            </div>                                    
                        </div>
                        <div class="row tile_count ">
                            <div class="col-md-12 col-sm-12 col-xs-12" style="overflow:hidden;">
                                <div class="col-md-12" style="overflow:hidden;">
                                    <canvas height="100%" id="lineChart1"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-12 col-xs-12" style="overflow:hidden;">
                        <div class="row top_tiles">
                            <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="tile-stats">
                                    <div class="count" id="mproceso"></div>
                                    <h3>En proceso</h3>
                                </div>
                            </div>
                        </div>
                        <div class="row top_tiles">
                            <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="tile-stats">
                                    <div class="count"  id="mcompleto"></div>
                                    <h3>Completo</h3>
                                </div>
                            </div>
                        </div>
                        <div class="row top_tiles">
                            <div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="tile-stats">
                                    <div class="count" id="mcerrado"></div>
                                    <h3>Pendiente de firma</h3>
                                </div>
                            </div>
                            <div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="tile-stats">
                                    <div class="count" id="mcerradosf"></div>
                                    <h3>Cerrado Sin firma</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-sm-12 col-xs-12" style="overflow:hidden;">
                <div class="x_panel">
                    <div class="x_title">
                        <h2 id="mes"></h2> <h3>REPARACIONES</h3> 
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="col-md-7 col-sm-12 col-xs-12" style="overflow:hidden;">
                            <div class="row tile_count">
                                <table class="countries_list" id="tblreparaciones">
                                    <thead>
                                        <tr>
                                            <td></td>
                                            <td>Día</td>
                                            <td>Mes</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-12 col-xs-12" style="overflow:hidden;">
                            <div class="row tile_count well">
                                <div class="col-md-12 col-sm-12 col-xs-12 tile">
                                    <span class="green" >En Proceso</span>
                                    <h2 class="green" id="rproceso"></h2>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 tile">
                                    <span class="green">Completado</span>
                                    <h2 class="green" id="rcompleto"></h2>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12 tile">
                                    <span class="green">Pendiente de firma</span>
                                    <h2 class="green" id="rcerrado"></h2>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12 tile">
                                    <span class="green">Cerrado Sin firma</span>
                                    <h2 class="green" id="rcerradoSF"></h2>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12" style="overflow:hidden;">
                            <canvas height="100%" id="reparaciones"></canvas>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-sm-12 col-xs-12" style="overflow:hidden;">
                <div class="x_panel">
                    <div class="x_title">
                        <h2 id="mes"></h2> <h3>EMERGENCIA</h3> 
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="col-md-7 col-sm-12 col-xs-12" style="overflow:hidden;">
                            <div class="row tile_count">
                                <table class="countries_list" id="tblemergencia">
                                    <thead>
                                        <tr>
                                            <td></td>
                                            <td>Día</td>
                                            <td>Mes</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-12 col-xs-12" style="overflow:hidden;">
                            <div class="row tile_count well">
                                <div class="col-md-12 col-sm-12 col-xs-12 tile">
                                    <span class="green" >En Proceso</span>
                                    <h2 class="green" id="eproceso"></h2>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 tile">
                                    <span class="green">Completado</span>
                                    <h2 class="green" id="ecompleto"></h2>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12 tile">
                                    <span class="green">Pendiente de firma</span>
                                    <h2 class="green" id="ecerrado"></h2>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12 tile">
                                    <span class="green">Cerrado S/firma</span>
                                    <h2 class="green" id="ecerradoSF"></h2>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12" style="overflow:hidden;">
                            <canvas height="100%" id="emergencias"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
require 'footer.php';
?>
<script src="../production/scripts/dashgse.js"></script>
