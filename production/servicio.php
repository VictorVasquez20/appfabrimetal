<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {

    require 'header.php';

    if ($_SESSION['administrador'] == 1 || $_SESSION['GGuias'] == 1 || $_SESSION['Contabilidad'] == 1 || $_SESSION['APresupuesto'] == 1) {?>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Guias de Servicio</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-tooltip="tooltip" title="Operaciones" role="button" aria-expanded="false"><i class="fa fa-cog"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a id="op_listar" onclick="mostarform(false)"><i class="fa fa-list-alt"></i> Listar</a>
                                            </li>
                                        </ul>
                                    </li>                     
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div id="filtroguias" class="x_content">
                                <div class="form-group">
                                    <div class="col-md-2">
                                        <label>SERVICIO</label>
                                        <select id="selservicio" name="selservicio" class="selectpicker form-control"></select>
                                    </div>
                                    <div class="col-md-2">
                                        <label>MES</label>
                                        <select id="mes" name="mes" class="selectpicker form-control">
                                            <option value="0">TODOS</option>
                                            <option value="1">ENERO</option>
                                            <option value="2">FEBRERO</option>
                                            <option value="3">MARZO</option>
                                            <option value="4">ABRIL</option>
                                            <option value="5">MAYO</option>
                                            <option value="6">JUNIO</option>
                                            <option value="7">JULIO</option>
                                            <option value="8">AGOSTO</option>
                                            <option value="9">SEPTIEMBRE</option>
                                            <option value="10">OCTUBRE</option>
                                            <option value="11">NOVIEMBRE</option>
                                            <option value="12">DICIEMBRE</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <label>AÑO</label>
                                        <select id="ano" name="ano" class="selectpicker form-control">
                                            <option value="0">TODOS</option>
                                            <?php
                                                $anoInicio = 2018;
                                                $anoFin = date('Y');
                                                while($anoInicio <= $anoFin){
                                                    echo '<option value="'.$anoInicio.'">'.$anoInicio.'</option>';
                                                    $anoInicio++;
                                                }

                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <label>SUPERVISOR</label>
                                        <select id="supervisor" name="supervisor" class="selectpicker form-control"></select>
                                    </div>
                                    <div class="col-md-2">
                                        <label>TECNICO</label>
                                        <select id="tecnico" name="tecnico" class="selectpicker form-control"></select>
                                    </div>
                                    <div class="col-md-2">
                                        <br>
                                        <button type="button" class="btn btn-info" onclick="listar();">BUSCAR</button>
                                    </div>
                                </div>
                            </div>
                            <div id="listadoguias" class="x_content">

                                <table id="tblguias" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>OPCIONES</th>
                                            <th>SERVICIO</th>
                                            <th>N. DOCTO</th>
                                            <th>TIPO</th>
                                            <th>EQUIPO</th>
                                            <th>EDIFICIO</th>
                                            <th>ESTADO FIN</th>
                                            <th>FECHA</th>
                                            <th>REQ. FIRMA</th>
                                            <th>FIRMADA</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="editar" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">
                                                <span aria-hidden="true">&times;</span>
                                                <span class="sr-only">Close</span>
                                            </button>
                                            <h4 class="modal-title" id="myModalLabel">EDITAR SERVICIO</h4>
                                        </div>
                                        <form id="formservicio" name="formservicio" class="form form-horizontal">
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                        <label>ASCENSOR</label>
                                                        <select class="selectpicker form-control" id="idascensor" name="idascensor" required="campo requerido"></select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                                        <label>TIPO SERVICIO</label>
                                                        <select class="selectpicker form-control" id="idtservicio" name="idtservicio"  required="campo requerido"></select>
                                                        <input type="hidden" class="form-control col-md-4" name="idservicio" id="idservicio">
                                                        <input type="hidden" class="form-control col-md-4" name="idtecnico" id="idtecnico">
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                                        <label>REQ. FIRMA</label>
                                                        <select class="selectpicker form-control" id="reqfirma" name="reqfirma"  required="campo requerido">
                                                            <option value="0">NO</option>
                                                            <option value="1">SI</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                                        <label>FECHA INI.</label>
                                                        <div class='input-group date' id='myDatepicker'>
                                                            <input type='text' class="form-control" id="created_time" name="created_time"  required="campo requerido"/>
                                                            <span class="input-group-addon">
                                                               <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                                        <label>ESTADO INI.</label>
                                                        <select class="selectpicker form-control" id="estadoini" name="estadoini"  required="campo requerido"></select>
                                                    </div>
                                                </div>	
                                                <div class="form-group">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                        <label>OBSERVACION INI.</label>
                                                        <textarea class="form-control" id="observacionini" name="observacionini"  required="campo requerido"></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                                        <label>FECHA FIN</label>
                                                        <div class='input-group date' id='myDatepicker2'>
                                                            <input type='text' class="form-control" id="closed_time" name="closed_time"  required="campo requerido"/>
                                                            <span class="input-group-addon">
                                                               <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                                        <label>ESTADO FIN</label>
                                                        <select class="selectpicker form-control" id="estadofin" name="estadofin"  required="campo requerido"></select>
                                                    </div>
                                                </div>	
                                                <div class="form-group">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                        <label>OBSERVACION FIN</label>
                                                        <textarea class="form-control" id="observacionfin" name="observacionfin"  required="campo requerido"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="limpiar();">CANCELAR</button>
                                                <button type="submit" class="btn btn-primary" name="editarservicio" id="editarservicio">EDITAR</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            
                            <!-- Modal -->
                            <div class="modal fade" id="procesar" role="dialog">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">
                                                <span aria-hidden="true">&times;</span>
                                                <span class="sr-only">Close</span>
                                            </button>
                                            <h4 class="modal-title" id="myModalLabel">PROCESAR SERVICIO</h4>
                                        </div>
                                        <form id="formprocesarservicio" name="formprocesarservicio" class="form form-horizontal" enctype="multipart/form-data">
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                        <label>CENTRO DE COSTO</label>
                                                        <select name="idcentrocos" id="idcentrocos" class="selectpicker select2_single form-control" data-live-search="true" required="campo requerido"></select>
                                                        <input type="hidden" name="idservicio1" id="idservicio1" value="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback well" id="infodev">
                                                        
                                                    </div>
                                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                                                        <label class="control-label">COMENTARIO</label>
                                                        <textarea name="infopro" id="infopro" class="form-control" required="" style="text-transform:uppercase;"></textarea>
                                                    </div>
                                                </div>
                                                <button type="button" class="btn btn-xs btn-dark" onclick="adjunto();">Adjuntar otro archivo</button>
                                                <div id="container">
                                                    <div class="form-group">
                                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                                            <label>NOMBRE ARCHIVO</label>
                                                            <input type="text" name="nombfile[]" id="nombfile0" class="form-control">
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                                            <label>ARCHIVO</label>
                                                            <input id="link0" name="docto[]" type="file" class="custom-file-input"  accept="application/pdf">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="limpiararchivo();">CANCELAR</button>
                                                <button type="submit" class="btn btn-primary" name="editarservicio" id="editarservicio">PROCESAR</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="modal fade" id="comentario" role="dialog">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">
                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">
                                                <span aria-hidden="true">&times;</span>
                                                <span class="sr-only">Close</span>
                                            </button>
                                            <h4 class="modal-title" id="myModalLabel">MOTIVO DE DEVOLUCIÓN</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback well" id="infodev2"></div> 
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <!-- Formulario modificar ascensor -->
                            <div id="mostrarguia" class="x_content">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalPreview" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" id="contenido">
                    
                </div>
            </div>
        </div>
        <!-- /page content -->
        <hidden id="edit-modal" name="edit-modal" data-toggle="modal" data-target="#editar"></hidden>
        <hidden id="proceso-modal" name="proceso-modal" data-toggle="modal" data-target="#procesar"></hidden>
        <hidden id="comentario-modal" name="comentario-modal" data-toggle="modal" data-target="#comentario"></hidden>
        <!-- /page content -->
        <?php
    } else {
        require 'nopermiso.php';
    }
    require 'footer.php';
    ?>
    <script src="../public/build/js/libs/png_support/png.js"></script>
    <script src="../public/build/js/libs/png_support/zlib.js"></script>
    <script src="../public/build/js/jspdf.debug.js"></script>
    <script src="../public/build/js/jspdf.plugin.autotable.js"></script>
    <script src="../public/build/js/jsPDFcenter.js"></script>
    <script src="../public/build/js/SimpleTableCellEditor.js"></script>
    <script type="text/javascript" src="scripts/servicio.js"></script>

    <script>
        $(document).ready(function () {

            editor = new SimpleTableCellEditor("tblguias");
            editor.SetEditableClass("editMe");

            $('#tblguias').on("cell:edited", function (event) {
                console.log(`'${event.oldValue}' changed to '${event.newValue}'`);
            });

        });

    </script>

    <?php
}
ob_end_flush();
?>