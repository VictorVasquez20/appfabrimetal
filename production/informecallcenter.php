<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {
    require 'header.php'; ?>

        <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>INFORME DE CALL CENTER</h2>
                                <div class="clearfix"></div>
                            </div>
                            <div  class="x_content">
                                <form class="form-inline" id="form_filtros">
                                    <div class="form-group">
                                    <div class="col-md-5">
                                       <label for="">Año</label>
                                       <select class="form-control selectpicker"  id="anio" name="anio" required="required" ></select>  
                                   </div>
                                   <div class="col-md-5">
                                      <label for="">Mes</label>
                                      <select class="form-control selectpicker"  id="mes" name="mes" required="required" ></select>  
                                    </div>
                                        <div class="col-md-2">  <br>
                                        <button type="submit" class="btn btn-primary">Buscar</button>
                                    </div>
                                    </div>
                                </form>                           
                            </div>                            
                        </div> 
   
                            <div class="col-md-6 col-sm-12 col-xs-12" style="overflow:hidden;">
                                <div class="x_panel">
                                    <div class="x_title">                                  
                                        <h4 id="titulo_cantidadllamadashorario"></h4>
                                        <div class="clearfix"></div>
                                    </div>
                                           <div class="x_content">
                                                <table id="tblcantidadllamadasporhorario" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                                <thead>
                                                  <tr>
                                                    <th>Estado</th>
                                                    <th>Fin de Semana</th>
                                                    <th>Horario Nocturno</th>
                                                    <th>Horario Normal</th>
                                                    <th>Total Resultado</th>
                                                  </tr>
                                                </thead>
                                                <tbody></tbody>
                                              </table>
                                      </div> 
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-12 col-xs-12" style="overflow:hidden;">
                                <div class="x_panel">
                                    <div class="x_title">
                                    
                                        <h4 id="titulo_tiempopromediollamadas"></h4>
                                        <div class="clearfix"></div>
                                    </div>
                                  
                                    <div class="x_content">
                                        <table id="tblpromediollamadas" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                                <thead>
                                                  <tr>
                                                      <th>Tipos de Tiempo</th>
                                                    <th>Tiempo Promedio de Una Llamada</th>
                                                  </tr>
                                                </thead>
                                                <tbody></tbody>
                                              </table>
                                    </div>  
                                </div>
                            </div>
                                                       
                         <div class="x_panel">
                                <div class="x_title">
                                    <h4 id="titulo_graficocantidadllamadasporhora"></h4>
                                    <div class="clearfix"></div>
                                </div>
                               
                             <div class="x_content">
                                    <div class="col-md-12 col-sm-12 col-xs-12" style="overflow:hidden;">
                                        
                                        <div class="row tile_count ">
                                            <div class="col-md-12 col-sm-12 col-xs-12" style="overflow:hidden;">
                                                <div class="col-md-12" style="overflow:hidden;">
                                                    <canvas  id="mybarChart"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                                                                                                               
                </div>
            </div>
        </div>
    </div>
        
 <?php require 'footer.php'; ?>
        
    <script type="text/javascript" src="scripts/informecallcenter.js"></script>
    <?php
}
ob_end_flush();
?>

