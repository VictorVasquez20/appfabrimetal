<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {

    require 'header.php';

    if ($_SESSION['administrador'] == 1 || $_SESSION['Contratos'] == 1) {
        ?>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Contratos</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-tooltip="tooltip" title="Operaciones" role="button" aria-expanded="false"><i class="fa fa-cog"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a id="op_agregar" onclick="mostarform(true)"><i class="fa fa-plus"></i> Agregar</a>
                                            </li>
                                            <li><a id="op_listar" onclick="mostarform(false)"><i class="fa fa-list-alt"></i> Listar</a>
                                            </li>
                                        </ul>
                                    </li>                     
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div id="listadocontratos" class="x_content">

                                <table id="tblcontratos" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>OPCIONES</th>
                                            <th>ID CONTRATO</th>
                                            <th>FECHA</th>
                                            <th>TIPO</th>
                                            <th>CLIENTE</th>
                                            <th>UBICACION</th>                                                                                               
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>


                            <div id="formulariocontratos" class="x_content">
                                <br />
                                <div class="clearfix"></div>
                                <form id="formulario" name="formulario" method="post" class="form-horizontal form-label-left">
                                    <h4><b>DATOS DEL CONTRATO</b></h4>  
                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label for="tcontrato">Tipo de contrato <span class="required">*</span></label>
                                            <input type="hidden" id="idcontrato" name="idcontrato" class="form-control">
                                            <select class="form-control selectpicker" data-live-search="true" id="tcontrato" name="tcontrato" required="required"></select>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label for="ncontrato">Numero de contrato <span class="required">*</span></label>
                                            <input type="text" id="ncontrato" name="ncontrato" required="required" class="form-control">
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label for="nexterno">Identicacion contrato cliente</label>
                                            <input type="text" id="nexterno" name="nexterno" class="form-control">
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label for="nreferencia">Referencia tipo de contrato</label>
                                            <input id="nreferencia" class="form-control" type="text" name="nreferencia">
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label for="idperiocidad">Periocidad <span class="required">*</span></label>
                                            <select class="form-control selectpicker" data-live-search="true" id="idperiocidad" name="idperiocidad" required="required"></select>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label for="ubicacion">Ubicacion</label>
                                            <input type="text" id="ubicacion" name="ubicacion" class="form-control">
                                        </div>                         
                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label for="nclientes">Razones sociales <span class="required">*</span></label>
                                            <!--<input type="text" id="nclientes" name="nclientes" required="required" class="form-control">-->
                                            <select class="form-control selectpicker" data-live-search="false" id="nclientes" name="nclientes" required="required">
                                                <option value="" selected disabled>N° Razones Sociales</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label for="nedificios">Edificios <span class="required">*</span></label>
                                            <!--<input type="text" id="nedificios" name="nedificios" required="required" class="form-control">-->
                                            <select class="form-control selectpicker" data-live-search="false" id="nedificios" name="nedificios" required="required">
                                                <option value="" selected disabled>N° Edificios</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label for="fecha">Fecha firma de contrato <span class="required">*</span></label>
                                            <input type='date' id="fecha" name="fecha" class="form-control" required="required"/>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label for="fecha">Fecha inicio de contrato <span class="required">*</span></label>
                                            <input type='date' id="fecha_ini" name="fecha_ini" class="form-control" required="required"/>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label for="fecha">Fecha fin de contrato (Blanco Indefinido)</label>
                                            <input type='date' id="fecha_fin" name="fecha_fin" class="form-control"/>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label for="rautomatica">Renovacion Automatica <span class="required">*</span></label>
                                                <!--<input type="text" id="nedificios" name="nedificios" required="required" class="form-control">-->
                                            <select class="form-control selectpicker" data-live-search="false" id="nedificios" name="nedificios" required="required">
                                                <option value="" selected disabled>Renovacion Automatica?</option>
                                                <option value="0">NO</option>
                                                <option value="1">SI</option>
                                            </select>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                            <label for="observaciones">Observaciones adicionales del contrato</label>
                                            <textarea type="text" id="observaciones" name="observaciones" class="resizable_textarea form-control"></textarea>
                                        </div>
                                    </div>
                                    <br>
                                    <div id="formclientes" name="formclientes">
                                        <h4><b>Datos de razones sociales</b></h4>
                                        <div id="clientes" name="clientes">
                                        </div>
                                    </div>

                                    <div id="formedificios" name="formedificios">
                                        <h4><b>Datos de Edificios</b></h4>
                                        <div id="edificios" name="edificios">
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 left-margin">
                                            <button class="btn btn-primary" type="button" id="btnCancelar"  onclick="cancelarform()">Cancelar</button>
                                            <button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiar()">Limpiar</button>
                                            <button class="btn btn-success" type="submit" id="btnGuardar">Agregar</button>
                                        </div>
                                    </div>

                                </form>
                            </div>

                            <div id="formeditarcontrato" class="x_content">
                                <br />
                                <div class="clearfix"></div>
                                <form id="formulariocontrato" name="formulariocontrato" method="post" class="form-horizontal form-label-left">
                                    <h4><b>Datos del Contrato</b></h4>  
                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label for="ftcontrato">Tipo de contrato <span class="required">*</span></label>
                                            <input type="hidden" id="fidcontrato" name="fidcontrato" class="form-control">
                                            <select class="form-control selectpicker" data-live-search="true" id="ftcontrato" name="ftcontrato" required="required"></select>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label for="ncontrato">Numero de contrato <span class="required">*</span></label>
                                            <input type="text" id="fncontrato" name="fncontrato" required="required" class="form-control">
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label for="nexterno">Identicacion contrato cliente</label>
                                            <input type="text" id="fnexterno" name="fnexterno" class="form-control">
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label for="nreferencia">Referencia tipo de contrato</label>
                                            <input id="fnreferencia" class="form-control" type="text" name="fnreferencia">
                                        </div>

                                        <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                                            <label for="idperiocidad">Periocidad <span class="required">*</span></label>
                                            <select class="form-control selectpicker" data-live-search="true" id="fidperiocidad" name="fidperiocidad" required="required"></select>
                                        </div>

                                        <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                                            <label for="ubicacion">Archivador</label>
                                            <input type="text" id="fubicacion" name="fubicacion" class="form-control">
                                        </div>

                                        <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                            <label>Avenida<span class="required">*</span></label>
                                            <input type="text" class="form-control" name="fcalle" id="fcalle" required="Campo requerido">
                                        </div>
                                        <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                                            <label>Numero <span class="required">*</span></label>
                                            <input type="text" class="form-control" name="fnumero" id="fnumero" required="Campo requerido">
                                        </div>
                                        <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                                            <label>Oficina</label>
                                            <input type="text" class="form-control" name="foficina" id="foficina">
                                        </div>

                                        <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                            <label>Region <span class="required">*</span></label>
                                            <select class="form-control selectpicker" data-live-search="true" id="fidregiones" name="fidregiones" required="Campo requerido">
                                            </select>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                            <label>Provincia <span class="required">*</span></label>
                                            <select class="form-control selectpicker" data-live-search="true" id="fidprovincias" name="fidprovincias" required="Campo requerido">
                                            </select>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                            <label>Comuna <span class="required">*</span></label>
                                            <select class="form-control selectpicker" data-live-search="true" id="fidcomunas" name="fidcomunas" required="Campo requerido">
                                            </select>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label for="fecha">Fecha firma de contrato <span class="required">*</span></label>
                                            <input type='date' id="ffecha" name="ffecha" class="form-control" required="required"/>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label for="fecha">Fecha inicio de contrato</label>
                                            <input type='date' id="ffecha_ini" name="ffecha_ini" class="form-control"/>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label for="fecha">Fecha fin de contrato (Blanco Indefinido)</label>
                                            <input type='date' id="ffecha_fin" name="ffecha_fin" class="form-control"/>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label for="rautomatica">Renovacion Automatica <span class="required">*</span></label>
                                                <!--<input type="text" id="nedificios" name="nedificios" required="required" class="form-control">-->
                                            <select class="form-control selectpicker" data-live-search="false" id="nedificios" name="nedificios" required="required">
                                                <option value="" selected disabled>Renovacion Automatica?</option>
                                                <option value="0">NO</option>
                                                <option value="1">SI</option>
                                            </select>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                            <label for="observaciones">Observaciones adicionales del contrato</label>
                                            <textarea type="text" id="fobservaciones" name="fobservaciones" class="resizable_textarea form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 left-margin">
                                            <button class="btn btn-primary" type="button" id="btnCancelar"  onclick="cancelarform()">Cancelar</button>
                                            <button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiar()">Limpiar</button>
                                            <button class="btn btn-success" type="submit" id="btnGuardar">Agregar</button>
                                        </div>
                                    </div>

                                </form>
                            </div>

                            <div id="tabcontrato" class="x_content">

                                <div class="col-md-12 col-sm-12 col-xs-12" style="border:0px solid #e5e5e5;">
                                    <h1 class="prod_title" id="tabncontrato" name="tabncontrato"></h1>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <ul class="">
                                            <li>
                                                <h5><b>Direccion de Facturacion: </b></h5><p id="tabdireccion" name="tabdireccion"></p>
                                            </li>
                                            <li>
                                                <h5><b>Region: </b></h5><p id="tabregion" name="tabregion"></p>
                                            </li>
                                            <li>
                                                <h5><b>Provincia: </b></h5><p id="tabprovincia" name="tabprovincia"></p>
                                            </li>
                                            <li>
                                                <h5><b>Comuna: </b></h5><p id="tabcomuna" name="tabcomuna"></p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <ul class="">
                                            <li>
                                                <h5><b>Tipo contrato: </b></h5><p id="tabtcontrato" name="tabtcontrato"></p>
                                            </li>
                                            <li>
                                                <h5><b>Id Contrato cliente: </b></h5><p id="tabnexterno" name="tabexterno"></p>
                                            </li>
                                            <li>
                                                <h5><b>Id referencia contrato: </b></h5><p id="tabnreferencia" name="tabnreferencia"></p>
                                            </li>
                                            <li>
                                                <h5><b>Ubicacion Archivo Contratos: </b></h5><p id="tabubicacion" name="tabnubicacion"></p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <ul class="">
                                            <li>
                                                <h5><b>N° Razon social: </b></h5><p id="tabnrazon" name="tabnrazon"></p>
                                            </li>
                                            <li>
                                                <h5><b>N° Edificios: </b></h5><p id="tabnedificios" name="tabnedificios"></p>
                                            </li>
                                            <li>
                                                <h5><b>N° Ascensores: </b></h5><p id="tabnascensores" name="tabnascensores"></p>
                                            </li>
                                            <li>
                                                <h5><b>N° Centros Costo: </b></h5><p id="tabncentros" name="tabncentros"></p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="">
                                            <ul class="">
                                                <li>
                                                    <h5><b>Periocidad: </b></h5><p id="tabperiocidad" name="tabperiocidad"></p>
                                                </li>
                                                <li>
                                                    <h5><b>Fecha de firma: </b></h5><p id="tabfirma" name="tabfirma"></p>
                                                </li>
                                                <li>
                                                    <h5><b>Inicio del contrato: </b></h5><p id="tabinicio" name="tabinicio"></p>
                                                </li>
                                                <li>
                                                    <h5><b>Fin del contrato: </b></h5><p id="tabfin" name="tabfin"></p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>


                                    <div class="col-md-12">

                                        <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#tab_content1" role="tab" id="home-tab"  data-toggle="tab" aria-expanded="true">Clientes</a>
                                                </li>
                                                <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab"  data-toggle="tab" aria-expanded="false">Edificios</a>
                                                </li>
                                                <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Ascensores</a>
                                                </li>
                                                <li role="presentation" class=""><a href="#tab_content4" role="tab" id="profile-tab3" data-toggle="tab" aria-expanded="false">Centros de Costo</a>
                                                </li>
                                                <li role="presentation" class=""><a href="#tab_content5" role="tab" id="profile-tab4" data-toggle="tab" aria-expanded="false">Observaciones</a>
                                                </li>
                                            </ul>
                                            <div id="myTabContent" class="tab-content">
                                                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                                                    <table id="tbltabclientes" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th>OPCIONES</th>
                                                                <th>RUT</th>
                                                                <th>RAZON SOCIAL</th>
                                                                <th>DIRECCION</th>
                                                                <th>TIPO</th>
                                                                <th>REGION</th>
                                                                <th>COMUNA</th>                                                              
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                                                    <table id="tbltabedificios" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th>OPCIONES</th>
                                                                <th>DIRECCION</th>
                                                                <th>SEGMENTO</th>
                                                                <th>REGION</th>
                                                                <th>COMUNA</th>                                                            
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                                                    <table id="tbltabascensores" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th>OPCIONES</th>
                                                                <th>CODIGO</th>
                                                                <th>MARCA</th>
                                                                <th>MODELO</th>
                                                                <th>VALOR UF</th>
                                                                <th>EDIFICIO</th>
                                                                <th>REGION</th>
                                                                <th>COMUNA</th>                                                            
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>                                                   
                                                </div>
                                                <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="profile-tab">
                                                    <table class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>CODIGO</th>
                                                                <th>NOMBRE</th>
                                                                <th>TIPO</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="tabcentroscosto" name="tabcentroscosto"> 
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div role="tabpanel" class="tab-pane fade" id="tab_content5" aria-labelledby="profile-tab">
                                                    <p id="tabobservaciones" name="tabobservaciones"></p>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <!-- Formulario modificar ascensor -->


                        </div>
                    </div>
                </div>
            </div>
            <!-- /page content -->

            <!-- /page content -->
            <?php
        } else {
            require 'nopermiso.php';
        }
        require 'footer.php';
        ?>
        <script type="text/javascript" src="scripts/contrato.js"></script>
        <?php
    }
    ob_end_flush();
    ?>