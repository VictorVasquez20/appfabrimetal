<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>App Fabrimetal</title>

        <link rel="icon" href="../public/favicon.ico" type="image/x-icon" />


        <!-- Bootstrap -->
        <link href="../public/build/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="../public/build/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="../public/build/css/nprogress.css" rel="stylesheet">
        <!-- bootstrap-daterangepicker -->
        <link href="../public/build/css/daterangepicker.css" rel="stylesheet">
        <!-- Custom Theme Style -->
        <link href="../public/build/css/custom.min.css" rel="stylesheet">

        <!-- PNotify -->
        <link href="../public/build/css/pnotify.css" rel="stylesheet">
        <link href="../public/build/css/pnotify.buttons.css" rel="stylesheet">
        <link href="../public/build/css/pnotify.nonblock.css" rel="stylesheet">

        <!-- Datatables -->
        <link href="../public/build/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="../public/build/css/buttons.bootstrap.min.css" rel="stylesheet">
        <link href="../public/build/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
        <link href="../public/build/css/responsive.bootstrap.min.css" rel="stylesheet">
        <link href="../public/build/css/scroller.bootstrap.min.css" rel="stylesheet">
    </head>
    <body class="">
        <div class="container body" style="width: 100%; height: 100%; text-align: center;">
            <div class="main_container">
                <div class="right_col" role="main">
                    <div class="row">
                        <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="col-md-12 col-sm-12 col-xs-12" style="overflow:hidden;">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2 id="mes"></h2> <h3>FACTURACIÓN GUÍAS DE SERVICIO <span id="mesact"></span></h3> 
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <div class="row top_tiles" style="text-align: center;">
                                            <div class="animated flipInY col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <div class="tile-stats">
                                                    <div class="count" id="tot"></div>
                                                    <h3>TOTAL GSE</h3>
                                                </div>
                                            </div>
                                            <div class="animated flipInY col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <div class="tile-stats">
                                                    <div class="count" id="totfact"></div>
                                                    <h3>GSE FACTURADAS</h3>
                                                </div>
                                            </div>
                                            <div class="animated flipInY col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <div class="tile-stats">
                                                    <div class="count" id="totporc"></div>
                                                    <h3>(%) PORCENTAJE DE FACTURACIÓN</h3>
                                                </div>
                                            </div>
                                            <!--<div class="animated flipInY col-lg-3 col-md-6 col-sm-12 col-xs-12">
                                                <div class="tile-stats">
                                                    <div class="count" id="emer"></div>
                                                    <h3>EMERGENCIA</h3>
                                                </div>
                                            </div>-->
                                        </div>
                                    </div>
                                </div>
                                
                                
                                <div class="col-md-7 col-sm-12 col-xs-12" style="overflow:hidden;">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h3>MANTENCIÓN</h3> 
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">
                                            <div class="row tile_count ">
                                                <div class="col-md-4 col-sm-6 col-xs-12 tile_stats_count">
                                                    <span class="count_top"><i class="fa fa-clock-o"></i> #GSE TOTAL MANTENCIÓN</span>
                                                    <div class="count" id="mant"></div>
                                                </div>
                                                <div class="col-md-4 col-sm-6 col-xs-12 tile_stats_count">
                                                    <span class="count_top"><i class="fa fa-clock-o"></i> FACTURADAS</span>
                                                    <div class="count" id="mantfact"> </div>
                                                </div>
                                                <div class="col-md-4 col-sm-6 col-xs-12 tile_stats_count">
                                                    <span class="count_top"><i class="fa fa-clock-o"></i> PORCENTAJE (%)</span>
                                                    <div class="count" id="mantporc"></div>
                                                </div>                                   
                                            </div>
                                            <div class="row tile_count ">
                                                <div class="col-md-12 col-sm-12 col-xs-12" style="overflow:hidden;">
                                                    <div class="col-md-12" style="overflow:hidden;">
                                                        <canvas height="47%" id="grafico"></canvas>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5 col-sm-12 col-xs-12" style="overflow:hidden;">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h3>GUÍA DE SERVICIO</h3> 
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">
                                            <div class="row top_tiles">
                                                <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="tile-stats">
                                                        <div class="count" id="mdia"></div>
                                                        <h3>GSE Día</h3>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row top_tiles">
                                                <div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <div class="tile-stats">
                                                        <div class="count" id="mproceso"></div>
                                                        <h3>En proceso - día</h3>
                                                    </div>
                                                </div>
                                                <div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <div class="tile-stats">
                                                        <div class="count"  id="mcompleto"></div>
                                                        <h3>Completo - día</h3>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row top_tiles">
                                                <div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <div class="tile-stats">
                                                        <div class="count" id="mcerrado"></div>
                                                        <h3>Pendiente de firma - mes</h3>
                                                    </div>
                                                </div>
                                                <div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <div class="tile-stats">
                                                        <div class="count" id="mcerradosf"></div>
                                                        <h3>Cerrado Sin firma - mes</h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-12 col-xs-12" style="overflow:hidden;">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h3>REPARACIÓN</h3> 
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">
                                            <div class="col-md-12 col-sm-12 col-xs-12" style="overflow:hidden;">
                                                <div class="row tile_count ">
                                                    <div class="col-md-4 col-sm-6 col-xs-12 tile_stats_count">
                                                        <span class="count_top"><i class="fa fa-clock-o"></i> #GSE TOTAL REPARACIÓN</span>
                                                        <div class="count" id="rep"></div>
                                                    </div>
                                                    <div class="col-md-4 col-sm-6 col-xs-12 tile_stats_count">
                                                        <span class="count_top"><i class="fa fa-clock-o"></i> FACTURADAS</span>
                                                        <div class="count" id="repfact"> </div>
                                                    </div>
                                                    <div class="col-md-4 col-sm-6 col-xs-12 tile_stats_count">
                                                        <span class="count_top"><i class="fa fa-clock-o"></i> PORCENTAJE (%)</span>
                                                        <div class="count" id="repporc"></div>
                                                    </div>                                   
                                                </div>
                                                <div class="row tile_count ">
                                                    <div class="col-md-12 col-sm-12 col-xs-12" style="overflow:hidden;">
                                                        <div class="col-md-12" style="overflow:hidden;">
                                                            <canvas height="50%" id="reparacion"></canvas>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-12 col-xs-12" style="overflow:hidden;">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h3>EMERGENCIA</h3> 
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">
                                            <div class="col-md-12 col-sm-12 col-xs-12" style="overflow:hidden;">
                                                <div class="row tile_count ">
                                                    <div class="col-md-4 col-sm-6 col-xs-12 tile_stats_count">
                                                        <span class="count_top"><i class="fa fa-clock-o"></i> #GSE TOTAL EMERGENCIA</span>
                                                        <div class="count" id="emer"></div>
                                                    </div>
                                                    <div class="col-md-4 col-sm-6 col-xs-12 tile_stats_count">
                                                        <span class="count_top"><i class="fa fa-clock-o"></i> FACTURADAS</span>
                                                        <div class="count" id="emerfact"> </div>
                                                    </div>
                                                    <div class="col-md-4 col-sm-6 col-xs-12 tile_stats_count">
                                                        <span class="count_top"><i class="fa fa-clock-o"></i> PORCENTAJE (%)</span>
                                                        <div class="count" id="emerporc"></div>
                                                    </div>                                   
                                                </div>                                 

                                                <div class="row tile_count ">
                                                    <div class="col-md-12 col-sm-12 col-xs-12" style="overflow:hidden;">
                                                        <div class="col-md-12" style="overflow:hidden;">
                                                            <canvas height="50%" id="emergencia"></canvas>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- jQuery -->
        <script src="../public/build/js/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="../public/build/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="../public/build/js/fastclick.js"></script>
        <!-- NProgress -->
        <script src="../public/build/js/nprogress.js"></script>
        <!-- Chart.js -->
        <script src="../public/build/js/Chart.min.js"></script>
        <!-- jQuery Sparklines -->
        <script src="../public/build/js/jquery.sparkline.min.js"></script>
        <!-- Flot -->
        <script src="../public/build/js/jquery.flot.js"></script>
        <script src="../public/build/js/jquery.flot.pie.js"></script>
        <script src="../public/build/js/jquery.flot.time.js"></script>
        <script src="../public/build/js/jquery.flot.stack.js"></script>
        <script src="../public/build/js/jquery.flot.resize.js"></script>
        <!-- Flot plugins -->
        <script src="../public/build/js/jquery.flot.orderBars.js"></script>
        <script src="../public/build/js/jquery.flot.spline.min.js"></script>
        <script src="../public/build/js/curvedLines.js"></script>
        <!-- DateJS -->
        <script src="../public/build/js/date.js"></script>
        <!-- bootstrap-daterangepicker -->
        <script src="../public/build/js/moment.min.js"></script>
        <script src="../public/build/js/daterangepicker.js"></script>

        <!-- PNotify -->
        <script src="../public/build/js/pnotify.js"></script>
        <script src="../public/build/js/pnotify.buttons.js"></script>
        <script src="../public/build/js/pnotify.nonblock.js"></script>

        <!-- Datatables -->
        <script src="../public/build/js/jquery.dataTables.min.js"></script>
        <script src="../public/build/js/dataTables.bootstrap.min.js"></script>
        <script src="../public/build/js/dataTables.buttons.min.js"></script>
        <script src="../public/build/js/buttons.bootstrap.min.js"></script>
        <script src="../public/build/js/buttons.flash.min.js"></script>
        <script src="../public/build/js/buttons.html5.min.js"></script>
        <script src="../public/build/js/buttons.print.min.js"></script>
        <script src="../public/build/js/dataTables.fixedHeader.min.js"></script>
        <script src="../public/build/js/dataTables.keyTable.min.js"></script>
        <script src="../public/build/js/dataTables.responsive.min.js"></script>
        <script src="../public/build/js/responsive.bootstrap.js"></script>
        <script src="../public/build/js/dataTables.scroller.min.js"></script>
        <!-- Custom Theme Scripts -->
        <script src="../public/build/js/custom.js"></script>
        <script src="../production/scripts/dashcontabilidad.js"></script>
    </body>
</html>