<?php
/* ob_start();
  session_start();

  if (!isset($_SESSION["nombre"])) {
  header("Location:login.php");
  } else {

  require 'header.php'; */
?>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>App Fabrimetal</title>

        <link rel="icon" href="../public/favicon.ico" type="image/x-icon" />


        <!-- Bootstrap -->
        <link href="../public/build/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="../public/build/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="../public/build/css/nprogress.css" rel="stylesheet">
        <!-- bootstrap-daterangepicker -->
        <link href="../public/build/css/daterangepicker.css" rel="stylesheet">
        <!-- Custom Theme Style -->
        <link href="../public/build/css/custom.min.css" rel="stylesheet">
        <!-- Datatables -->
        <link href="../public/build/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="../public/build/css/buttons.bootstrap.min.css" rel="stylesheet">
        <link href="../public/build/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
        <link href="../public/build/css/responsive.bootstrap.min.css" rel="stylesheet">
        <link href="../public/build/css/scroller.bootstrap.min.css" rel="stylesheet">
    </head>
    <body class="">
        <div class="container body" style="width: 100%; height: 100%; text-align: center;">
            <div class="main_container">
                <div class="right_col" role="main">
                    <div class="row">
                        <div class="x_panel">
                            <div class="x_title">
                                <h3>ESTADO DE LOS EQUIPOS</h3> 
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <div class="col-md-7 col-sm-12 col-xs-12" style="overflow:hidden;">
                                    <!-- Datos actulidad en fallas -->
                                    <div class="row tile_count">
                                        <div class="col-md-3 col-sm-6 col-xs-12 tile_stats_count">
                                            <span class="count_top"><i class="fa fa-check"></i> NORMAL</span>
                                            <div class="count" id="num_normal"></div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-6 tile_stats_count">
                                            <span class="count_top"><i class="fa fa-ban"></i> DETENIDO </span>
                                            <div class="count"><a href="#" onclick="listarAlertaestado(7)" id="num_detenido"></a></div>            
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-6 tile_stats_count">
                                            <span class="count_top"><i class="fa fa-search"></i> OBSERVACION </span>
                                            <div class="count"><a href="#" onclick="listarAlertaestado(8)" id="num_observacion"></a></div>              
                                        </div>  
                                        <div class="col-md-3 col-sm-6 col-xs-6 tile_stats_count">
                                            <span class="count_top"><i class="fa fa-ambulance"></i> EMERGENCIA </span>
                                            <div class="count"><a href="#" onclick="listarAlertaestado(6)" id="num_emergencia"></a></div>              
                                        </div> 
                                    </div>

                                    <div class="row tile_count">            
                                        <div class="col-md-3 col-sm-6 col-xs-6 tile_stats_count">
                                            <span class="count_top"><i class="fa fa-cog"></i> MANTENCION </span>
                                            <div class="count"><a href="#" onclick="listarAlertaestado(2)" id="num_mantencion"></a></div>  
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-6 tile_stats_count">
                                            <span class="count_top"><i class="fa fa-wrench"></i> REPARACION </span>
                                            <div class="count"><a href="#" onclick="listarAlertaestado(3)" id="num_reparacion"></a></div>  
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-6 tile_stats_count">
                                            <span class="count_top"><i class="fa fa-wrench"></i> REP. MAYOR </span>
                                            <div class="count"><a href="#" onclick="listarAlertaestado(4)" id="num_mayor"></a></div>  
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-6 tile_stats_count">
                                            <span class="count_top"><i class="fa fa-cogs"></i> INGENIERIA </span>
                                            <div class="count"><a href="#" onclick="listarAlertaestado(5)" id="num_ingenieria"></a></div>  
                                        </div>       
                                    </div>
                                </div>
                                <div class="col-md-5 col-sm-12 col-xs-12" style="overflow:hidden;">
                                    <div class="row top_tiles">
                                        <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="tile-stats">
                                                <div class="count" id="ser_pro"></div>
                                                <h3>EN PROCESO</h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row top_tiles">
                                        <div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="tile-stats">
                                                <div class="count"  id="ser_com"></div>
                                                <h3>SERV. COMPLETO</h3>
                                            </div>
                                        </div>
                                        <div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="tile-stats">
                                                <div class="count"  id="ser_firma"></div>
                                                <h3>SERV. POR FIRMA</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <br />

                                <!-- Contenido Graficos -->
                                <div id="Charts" class="col-md-12 col-sm-12 col-xs-12">    
                                    <canvas  style="height: 30px;" id="mybarMant" height="100%"></canvas>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 col-sm-12 col-xs-12" id="datos" style="display: none;">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2>Guias Mantención</h2>
                                            <ul class="nav navbar-right panel_toolbox">
                                                <li><a class="" id="hiden"><i class="fa fa-close"></i></a></li>
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">
                                            <table class="table table-bordered" id="ascensoresestado">
                                                <thead>
                                                    <tr>
                                                        <th>CODIGO</th>
                                                        <th>EDIFICIO</th>
                                                        <th>UBICACION</th>
                                                        <th>CODIGO CLIENTE</th>
                                                        <th>MARCA</th>
                                                        <th>MODELO</th>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /Fin contenido graficos -->
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <!-- jQuery -->
        <script src="../public/build/js/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="../public/build/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="../public/build/js/fastclick.js"></script>
        <!-- NProgress -->
        <script src="../public/build/js/nprogress.js"></script>
        <!-- Chart.js -->
        <script src="../public/build/js/Chart.min.js"></script>
        <!-- jQuery Sparklines -->
        <script src="../public/build/js/jquery.sparkline.min.js"></script>
        <!-- Flot -->
        <script src="../public/build/js/jquery.flot.js"></script>
        <script src="../public/build/js/jquery.flot.pie.js"></script>
        <script src="../public/build/js/jquery.flot.time.js"></script>
        <script src="../public/build/js/jquery.flot.stack.js"></script>
        <script src="../public/build/js/jquery.flot.resize.js"></script>
        <!-- Flot plugins -->
        <script src="../public/build/js/jquery.flot.orderBars.js"></script>
        <script src="../public/build/js/jquery.flot.spline.min.js"></script>
        <script src="../public/build/js/curvedLines.js"></script>
        <!-- DateJS -->
        <script src="../public/build/js/date.js"></script>
        <!-- bootstrap-daterangepicker -->
        <script src="../public/build/js/moment.min.js"></script>
        <script src="../public/build/js/daterangepicker.js"></script>

        <!-- Datatables -->
        <script src="../public/build/js/jquery.dataTables.min.js"></script>
        <script src="../public/build/js/dataTables.bootstrap.min.js"></script>
        <script src="../public/build/js/dataTables.buttons.min.js"></script>
        <script src="../public/build/js/buttons.bootstrap.min.js"></script>
        <script src="../public/build/js/buttons.flash.min.js"></script>
        <script src="../public/build/js/buttons.html5.min.js"></script>
        <script src="../public/build/js/buttons.print.min.js"></script>
        <script src="../public/build/js/dataTables.fixedHeader.min.js"></script>
        <script src="../public/build/js/dataTables.keyTable.min.js"></script>
        <script src="../public/build/js/dataTables.responsive.min.js"></script>
        <script src="../public/build/js/responsive.bootstrap.js"></script>
        <script src="../public/build/js/dataTables.scroller.min.js"></script>
        <!-- Custom Theme Scripts -->
        <script src="../public/build/js/custom.min.js"></script>
        <script type="text/javascript" src="scripts/dashguias.js"></script>
    </body>
</html>

<?php /*
  require 'footer.php';
  }
  ob_end_flush(); */
?>
