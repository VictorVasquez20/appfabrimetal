<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {

    require 'header.php';
    ?>
    <!-- Contenido -->
    <div class="right_col" role="main">
        <div class="">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="x_panel fixed_height_600">
                        <div class="x_title">
                            <h4>MANTENCIÓN</h4>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="tile-stats">
                                    <div class="icon"><i class="fa fa-building"></i></div>
                                    <div class="count">1028</div>
                                    <h4>EQUIPOS EN CARTERA</h4>

                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <table class="" style="width:100%">
                                    <tr>
                                        <th>
                                            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                                <p class=""></p>
                                            </div>
                                            <div class="col-lg-3 col-md-7 col-sm-7 col-xs-7">
                                                <p class="">MES</p>
                                            </div>
                                            <div class="col-lg-2 col-md-5 col-sm-5 col-xs-5">
                                                <p class="">DIA</p>
                                            </div>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table class="tile_infop">
                                                <tr>
                                                    <td>
                                                        <p><i class="fa fa-wrench"></i>GUIA SERVICIO </p>
                                                    </td>
                                                    <td>5</td>
                                                    <td>5</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="dashboard-widget-content">
                                    <div class="sidebar-widget">
                                        <h4>META SERVICIOS</h4>
                                        <canvas width="150" height="80" id="chart_gauge_meta" class="" style="width: 160px; height: 100px;"></canvas>
                                        <div class="goal-wrapper">
                                            <span id="gauge-text" class="gauge-value gauge-chart pull-left">0</span>
                                            <span class="gauge-value pull-left">%</span>
                                            <span id="goal-text" class="goal-value pull-right">1561</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="x_panel fixed_height_600">
                        <div class="x_title">
                            <h4>REPARACIONES</h4>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <table class="" style="width:100%">
                                    <tr>
                                        <th>
                                            <div class="col-lg-6 col-md-7 col-sm-7 col-xs-7">
                                                <p class="">DEPARTAMENTO</p>
                                            </div>
                                            <div class="col-lg-3 col-md-7 col-sm-7 col-xs-7">
                                                <p class="">GSE MES</p>
                                            </div>
                                            <div class="col-lg-3 col-md-5 col-sm-5 col-xs-5">
                                                <p class="">GSE DIA</p>
                                            </div>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table class="tile_infop">
                                                <tr>
                                                    <td>
                                                        <p><i class="fa fa-wrench"></i>REP </p>
                                                    </td>
                                                    <td>5</td>
                                                    <td>5</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p><i class="fa fa-wrench"></i>REP MAYOR </p>
                                                    </td>
                                                    <td>5</td>
                                                    <td>10</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p><i class="fa fa-cogs"></i>ING CAMPO </p>
                                                    </td>
                                                    <td>5</td>
                                                    <td>5</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p><i class="fa fa-building-o"></i>NORMALIZACION </p>
                                                    </td>
                                                    <td>5</td>
                                                    <td>2</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="x_panel fixed_height_600">
                        <div class="x_title">
                            <h4>REPARACIONES</h4>
                            <div class="clearfix"></div>s
                        </div>
                        <div class="x_content">
                            <div class="dashboard-widget-content">
                                <div class="sidebar-widget">
                                    <h4>Profile Completion</h4>
                                    <canvas width="300" height="160" id="chart_gauge_01" class="" style="width: 320px; height: 200px;"></canvas>
                                    <div class="goal-wrapper">
                                        <span id="gauge-text" class="gauge-value gauge-chart pull-left">0</span>
                                        <span class="gauge-value pull-left">%</span>
                                        <span id="goal-text" class="goal-value pull-right">100%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>



    </div>
    </div>
    <!-- /Fin Contenido -->

    <?php
    require 'footer.php';
    ?>
    <script type="text/javascript" src="scripts/dashservicios.js"></script>

    <?php
}
ob_end_flush();
?>
