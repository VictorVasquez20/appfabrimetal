<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {
    require 'header.php';
    if ($_SESSION['administrador'] == 1) {
        ?>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>PROVEEDOR</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-cog"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a id="op_agregar" onclick="mostrarform(true)">Agregar</a></li>
                                            <li><a id="op_listar" onclick="mostrarform(false), mostrarficha(false)">Listar</a></li>
                                        </ul>
                                    </li>                     
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div id="listadoproveedor" class="x_content">
                                <div id="listadoproveedor" class="x_panel">
                                    <div class="form-group">
                                        <div class="col-md-3">
                                            <label>CATEGORIAS</label>
                                            <select id="filcategoria" name="filcategoria" class="selectpicker form-control"></select>
                                        </div>
                                        <div class="col-md-3">
                                            <br>
                                            <button type="button" class="btn btn-info" onclick="listar();">BUSCAR</button>
                                        </div>
                                    </div>
                                </div>
                                <table id="tblproveedor" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>RAZON SOCIAL</th>
                                            <th>RUT</th>
                                            <th>CATEGORIA</th>
                                            <th>CREACIÓN</th>
                                            <th>FACT. ULT. COMPRA</th>
                                            <th>EVALUACION</th>
                                            <th>CONDICIÓN</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>

                            <div id="formularioproveedor" class="x_content" style="display: none;">
                                <br />
                                <div class="col-md-12 center-margin">
                                    <form class="form-horizontal form-label-left" id="formulario" name="formulario">
                                        <div class="form-group">
                                            <label class="control-label col-md-2 col-sm-2 col-xs-12">RUT</label>
                                            <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control" name="rut" id="rut" required="Campo requerido" data-inputmask="'mask' : '[*9.][999.999]-[*]'">
                                            </div>
                                            <label class="control-label col-md-2 col-sm-2 col-xs-12">CATEGORIA</label>
                                            <div class="col-md-4 col-sm-4 col-xs-12 form-group">
                                                <select class="selectpicker form-control" id="idcategoria" name="idcategoria" data-live-search="true" required="required"></select>
                                            </div>
                                            <div class="col-md-1 col-sm-1 col-xs-1 form-group">
                                                <button type="button" name="addimp" id="addimp" class="btn btn-warning" data-toggle="modal" data-target="#agregacategoria"><i class="fa fa-plus-circle"></i></button>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-2 col-sm-2 col-xs-12">RAZON SOCIAL</label>
                                            <div class="col-md-10 col-sm-10 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control" name="razonsocial" id="razonsocial" required="Campo requerido">
                                                <input type="hidden" id="idproveedor" name="idproveedor" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-2 col-sm-2 col-xs-12">REGION</label>
                                            <div class="col-md-4 col-sm-4 col-xs-12 form-group">
                                                <select class="selectpicker form-control" id="idregion" name="idregion" data-live-search="true" required="required"></select>
                                            </div>
                                            <label class="control-label col-md-2 col-sm-2 col-xs-12">COMUNA</label>
                                            <div class="col-md-4 col-sm-4 col-xs-12 form-group">
                                                <select class="selectpicker form-control" id="idcomuna" name="idcomuna" data-live-search="true" required="required"></select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-2 col-sm-2 col-xs-12">DIRECCION</label>
                                            <div class="col-md-10 col-sm-10 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control" name="direccion" id="direccion" required="Campo requerido">
                                            </div>
                                        </div>

                                        <div id="" class="x_panel">
                                            <div class="form-group">
                                                <label class="control-label col-md-2 col-sm-2 col-xs-12">PERSONA DE CONTACTO</label>
                                                <div class="col-md-10 col-sm-10 col-xs-12 form-group has-feedback">
                                                    <input type="text" class="form-control" name="contacto" id="contacto">
                                                </div>
                                            </div>	

                                            <div class="form-group">
                                                <label class="control-label col-md-2 col-sm-2 col-xs-12">TELEFONO</label>
                                                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                                                    <input type="text" class="form-control" name="fono" id="fono">
                                                </div>
                                                <label class="control-label col-md-2 col-sm-2 col-xs-12">EMAIL</label>
                                                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                                                    <input type="text" class="form-control" name="email" id="email">
                                                </div>
                                            </div>	
                                        </div>

                                        <div id="" class="x_panel">
                                            <div class="form-group">
                                                <label class="control-label col-md-2 col-sm-2 col-xs-12">CONDICION DE PAGO</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12 form-group">
                                                    <select class="selectpicker form-control" id="idcondicionpago" name="idcondicionpago" data-live-search="true" required="required"></select>
                                                </div>
                                                <div class="col-md-1 col-sm-1 col-xs-1 form-group">
                                                    <button type="button" name="addimp" id="addimp" class="btn btn-warning" data-toggle="modal" data-target="#agregacondicion"><i class="fa fa-plus-circle"></i></button>
                                                </div>
                                            </div>	

                                            <div class="form-group">
                                                <label class="control-label col-md-2 col-sm-2 col-xs-12">PLAZO PAGO</label>
                                                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                                                    <input type="text" class="form-control" name="plazopago" id="plazopago" required="Campo requerido">
                                                </div>
                                                <label class="control-label col-md-2 col-sm-2 col-xs-12">SERVICIO DE FLETE</label>
                                                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                                                    <div class="">
                                                        <label>
                                                            <input name="servicioflete" id="servicioflete" type="checkbox" class="js-switch" value="1"/>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>	
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-2 col-sm-2 col-xs-12">OBSERVACIÓN</label>
                                            <div class="col-md-10 col-sm-10 col-xs-12 form-group has-feedback">
                                                <textarea id="observacion" name="observacion" class="form-control"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-2 col-sm-2 col-xs-12">CONDICIÓN</label>
                                            <div class="col-md-10 col-sm-10 col-xs-12 form-group has-feedback">
                                                <div class="">
                                                    <label>
                                                        <input name="condicion" id="condicion" type="checkbox" class="js-switch" value="1"/>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="ln_solid"></div> 
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <button class="btn btn-primary" type="button" id="btnCancelar" onclick="cancelarform()">Cancelar</button>
                                                <button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiar()">Limpiar</button>
                                                <button class="btn btn-success" type="submit" id="btnGuardar">Agregar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div id="ficha" class="x_content" style="display: none;">
                                <div role="tabpanel" class="tab-pane" id="tab_content11" aria-labelledby="home-tab">
                                    <div id="formularioascensor" class="x_panel">
                                        <div class="x_title">
                                            <h3 style="text-align: center;" id="razonsocial2"></h3>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">
                                            <div class="col-md-4 col-sm-12 col-xs-12">
                                                <ul class="list-unstyled text-left">
                                                    <input type="hidden" name="idproveedor1" id="idproveedor1">
                                                    <li><h5><i class="fa fa-check text-success"></i><b> RUT: </b></h5><p id="rut1" name="rut1"></p></li>
                                                    <li><h5><i class="fa fa-check text-success"></i><b> RAZON SOCIAL: </b></h5><p id="razonsocial1" name="razonsocial1"></p></li>
                                                    <li><h5><i class="fa fa-check text-success"></i><b> CATEGORIA: </b></h5><p id="categoria1" name="categoria1"></p></li>
                                                    <li><h5><i class="fa fa-check text-success"></i><b> CONDICIÓN DE PAGO: </b></h5><p id="condicion1" name="condicion1"></p></li>
                                                </ul>
                                            </div>
                                            <div class="col-md-4 col-sm-12 col-xs-12">
                                                <ul class="list-unstyled text-left">
                                                    <li><h5><i class="fa fa-check text-success"></i><b> REGIÓN: </b></h5><p id="region1" name="region1"></p></li>
                                                    <li><h5><i class="fa fa-check text-success"></i><b> COMUNA: </b></h5><p id="comuna1" name="comuna1"></p></li>
                                                    <li><h5><i class="fa fa-check text-success"></i><b> DIRECCIÓN: </b></h5><p id="direccion1" name="direccion1"></p></li>
                                                    <li><h5><i class="fa fa-check text-success"></i><b> PLAZO PAGO: </b></h5><p id="plazopago1" name="plazopago1"></p></li>
                                                </ul>
                                            </div>
                                            <div class="col-md-4 col-sm-12 col-xs-12">
                                                <ul class="list-unstyled text-left">
                                                    <li><h5><i class="fa fa-check text-success"></i><b> CONTACTO: </b></h5><p id="contacto1" name="contacto1"></p></li>
                                                    <li><h5><i class="fa fa-check text-success"></i><b> TELEFONO: </b></h5><p id="telefono1" name="telefono1"></p></li>
                                                    <li><h5><i class="fa fa-check text-success"></i><b> EMAIL: </b></h5><p id="email1" name="email1"></p></li>
                                                    <li><h5><i class="fa fa-check text-success"></i><b> SERVICIO DE FLETE: </b></h5><p id="flete1" name="flete1"></p></li>
                                                </ul>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <ul class="list-unstyled text-left">
                                                    <li><h5><i class="fa fa-check text-success"></i><b> OBSERVACIÓN: </b></h5><p id="observacion1" name="observacion1"></p></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!--div class="modal-footer">
                                            <div class="col-md-12 col-sm-12 col-xs-12 right">
                                                <button class="btn btn-primary" type="button" id="btnCancelar" onclick="mostrarficha(false);">Volver</button>
                                            </div>
                                        </div>-->
                                    </div>
                                </div>
                                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                    <ul id="myTab1" class="nav nav-tabs nav-justified" role="tablist">
                                        <!--<li role="presentation" class="active">
                                            <a href="#tab_content11" id="home-tabb" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">FICHA</a>
                                        </li>-->
                                        <li role="presentation" class="">
                                            <a href="#tab_content22" role="tab" id="profile-tabb" data-toggle="tab" aria-controls="profile" aria-expanded="false">INFORMACIÓN TRIBUTARIA</a>
                                        </li>
                                        <li role="presentation" class="active">
                                            <a href="#tab_content33" role="tab" id="profile-tabb3" data-toggle="tab" aria-controls="profile" aria-expanded="false">EJECUTIVO COMERCIAL</a>
                                        </li>
                                        <li role="presentation" class="">
                                            <a href="#tab_content44" role="tab" id="profile-tabb4" data-toggle="tab" aria-controls="profile" aria-expanded="false">DOCUMENTOS</a>
                                        </li>
                                        <li role="presentation" class="">
                                            <a href="#tab_content55" role="tab" id="profile-tabb5" data-toggle="tab" aria-controls="profile" aria-expanded="false">COMPRAS</a>
                                        </li>
                                    </ul>
                                    <div id="myTabContent2" class="tab-content">

                                        <div role="tabpanel" class="tab-pane fade" id="tab_content22" aria-labelledby="profile-tab">
                                            <div class="x_title">
                                                <h3 style="text-align: center;">INFORMACIÓN TRIBUTARIA</h3>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="x_content">
                                                SOON!!!
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content33" aria-labelledby="profile-tab">
                                            <div class="modal fade bs-example-modal-sm" id="agregarejecutivo" tabindex="-1" role="dialog" aria-hidden="true">
                                                <div class="modal-dialog modal-md">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                                            </button>
                                                            <h4 class="modal-title" id="myModalLabel">EJECUTIVO COMERCIAL</h4>
                                                        </div>
                                                        <form id="formejecutivo" name="formejecutivo" class="form form-horizontal">
                                                            <div class="modal-body">
                                                                <div class="form-group">
                                                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                                        <label class="control-label">NOMBRE</label>
                                                                        <input type="hidden" id="ejecutivo_id" name="ejecutivo_id">
                                                                        <input id="ejecutivo_nombre" name="ejecutivo_nombre" required="required" class="form-control">
                                                                    </div>
                                                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                                        <label class="control-label">APELLIDOS</label>
                                                                        <input id="ejecutivo_apellido" name="ejecutivo_apellido" required="required" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                                        <label class="control-label">TELEFONO</label>
                                                                        <input id="ejecutivo_fono" name="ejecutivo_fono" required="required" class="form-control">
                                                                    </div>
                                                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                                        <label class="control-label">EMAIL</label>
                                                                        <input id="ejecutivo_email" name="ejecutivo_email" required="required" class="form-control">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                                <button type="submit" class="btn btn-primary" name="agregaImportacion" id="agregaImportacion">Agregar</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>   
                                            <div id="formularioascensor" class="x_panel">
                                                <div class="x_panel">
                                                    <button type="button" name="addeje" id="addeje" class="btn btn-warning" data-toggle="modal" data-target="#agregarejecutivo"><i class="fa fa-plus-circle"></i> Ejecutivo</button>
                                                    <table id="tblejecutivo" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th>NOMBRE</th>
                                                                <th>FONO</th>
                                                                <th>EMIAL</th>
                                                                <th>#</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="tab_content44" aria-labelledby="profile-tab">

                                            <div class="modal fade bs-example-modal-sm" id="agregardocto" tabindex="-1" role="dialog" aria-hidden="true">
                                                <div class="modal-dialog modal-md">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                                            </button>
                                                            <h4 class="modal-title" id="myModalLabel">DOCUMENTOS</h4>
                                                        </div>
                                                        <form id="formdocto" name="formdocto" class="form form-horizontal">
                                                            <div class="modal-body">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-2 col-sm-2 col-xs-12">DESCRIPCION</label>
                                                                    <div class="col-md-10 col-sm-10 col-xs-12 form-group">
                                                                        <input type="hidden" id="iddocumento" name="iddocumento">
                                                                        <input id="descripcion" name="descripcion" required="required" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-2 col-sm-2 col-xs-12">ARCHIVO</label>
                                                                    <div class="col-md-10 col-sm-10 col-xs-12 form-group">
                                                                        <input type="file" name="docto" id="docto" class="custom-file-input">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                                <button type="submit" class="btn btn-primary" name="agrega" id="agrega">Agregar</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>  
                                            <div id="formularioascensor" class="x_panel">
                                                <button type="button" name="addeje" id="addeje" class="btn btn-warning" data-toggle="modal" data-target="#agregardocto"><i class="fa fa-plus-circle"></i> Documento</button>
                                                <div class="x_panel">   
                                                    <ul class="list-unstyled top_profiles scroll-view" id="documentoslista"></ul>
                                                </div>
                                            </div>
                                        </div>


                                        <div role="tabpanel" class="tab-pane fade" id="tab_content55" aria-labelledby="profile-tab">

                                            <div class="modal fade bs-example-modal-sm" id="agregarcompra" tabindex="-1" role="dialog" aria-hidden="true">
                                                <div class="modal-dialog modal-md">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                                            </button>
                                                            <h4 class="modal-title" id="myModalLabel">COMPRA</h4>
                                                        </div>
                                                        <form id="formcompra" name="formcompra" class="form form-horizontal">
                                                            <div class="modal-body">
                                                                <div class="form-group">
                                                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                                                        <label class="control-label">FACTURA</label>
                                                                        <input type="hidden" id="idcompra" name="idocmpra">
                                                                        <input id="factura" name="fcatura" required="required" class="form-control">
                                                                    </div>
                                                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                                                        <label class="control-label">ORDEN DE COMPRA</label>
                                                                        <input type="text" name="ordencompra" id="ordencompra" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                                                        <label class="control-label">FECHA COMPRA</label>
                                                                        <input type="date" id="buy_time" name="buy_time" required="required" class="form-control">
                                                                    </div>
                                                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                                                        <label class="control-label">FECHA LLEGADA</label>
                                                                        <input type="date" name="reception_time" id="reception_time" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                                        <label class="control-label">PRESUPUESTO</label>
                                                                        <select class="selectpicker form-control" id="presupuesto" name="presupuesto" data-live-search="true"></select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-2 col-sm-2 col-xs-12">FLETE</label>
                                                                    <div class="col-md-10 col-sm-10 col-xs-12 form-group has-feedback">
                                                                        <div class="">
                                                                            <label>
                                                                                <input name="flete" id="felete" type="checkbox" class="js-switch" value="1"/>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                                        <label class="control-label">EVALUACIÓN</label>
                                                                        <input type="hidden" id="evaluacion" name="evaluacion">
                                                                        <div class='starrr'></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                                <button type="submit" class="btn btn-primary" name="agrega" id="agrega">Agregar</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>  

                                            <div id="formularioascensor" class="x_panel">
                                                <button type="button" name="addeje" id="addeje" class="btn btn-warning" data-toggle="modal" data-target="#agregarcompra"><i class="fa fa-plus-circle"></i> COMPRA</button>
                                                <table id="tblcompra" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>FACTURA</th>
                                                            <th>PRESUPUESTO</th>
                                                            <th>ORDEN DE COMPRA</th>
                                                            <th>FECHA</th>
                                                            <th>EVALUACIÓN</th>
                                                            <th>#</th>

                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <div class="col-md-12 col-sm-12 col-xs-12 right">
                                        <button class="btn btn-primary" type="button" id="btnCancelar" onclick="mostrarficha(false);">Volver</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
        <div class="modal fade bs-example-modal-sm" id="agregacategoria" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">AGREGAR CATEGORIA</h4>
                    </div>
                    <form id="formulariocat" name="formulariocat" class="form form-horizontal">
                        <div class="modal-body">
                            <div class="form-group">
                                <!--<label class="control-label col-md-2 col-sm-2 col-xs-12">NOMBRE</label>-->
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                                    <label>NOMBRE</label>
                                    <input type="text" class="form-control" name="nombrecat" id="nombrecat" placeholder="nombre de categoria">
                                    <input type="hidden" id="idcategoriacat" name="idcategoriacat" class="form-control">
                                </div>
                            </div>	
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12">CONDICIÓN</label>
                                <div class="col-md-10 col-sm-10 col-xs-12 form-group has-feedback">
                                    <div class="">
                                        <label>
                                            <input name="condicioncat" id="condicioncat" type="checkbox" class="js-switch" value="1"/>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary" name="agregaImportacion" id="agregaImportacion">Agregar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>   
        <div class="modal fade bs-example-modal-sm" id="agregacondicion" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">AGREGAR CONDICIÓN DE PAGO</h4>
                    </div>
                    <form id="formulariocond" name="formulariocond" class="form form-horizontal">
                        <div class="modal-body">
                            <div class="form-group">
                                <!--<label class="control-label col-md-2 col-sm-2 col-xs-12">NOMBRE</label>-->
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                                    <label>NOMBRE</label>
                                    <input type="text" class="form-control" name="nombrecond" id="nombrecond" placeholder="nombre condicion">
                                    <input type="hidden" id="idcondicionpagocond" name="idcondicionpagocond" class="form-control">
                                </div>
                            </div>	
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12">CONDICIÓN</label>
                                <div class="col-md-10 col-sm-10 col-xs-12 form-group has-feedback">
                                    <div class="">
                                        <label>
                                            <input name="condicioncond" id="condicioncond" type="checkbox" class="js-switch" value="1"/>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary" name="agregaImportacion" id="agregaImportacion">Agregar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /page content -->
        <?php
    } else {
        require 'nopermiso.php';
    }
    require 'footer.php';
    ?>
    <script type="text/javascript" src="scripts/proveedor.js"></script>
    <?php
}
ob_end_flush();
