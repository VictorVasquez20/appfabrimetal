<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {
    require 'header.php';
    ?>

    <div class="right_col" role="main">
        <div class="">
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>ORDEN DE COMPRA</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-tooltip="tooltip" title="Operaciones" role="button" aria-expanded="false"><i class="fa fa-cog"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <!--<li><a id="op_agregar" onclick="mostrarform(true)"><i class="fa fa-list-alt"></i> Agregar</a></li>-->
                                        <li><a id="op_listar" onclick="mostrarform(false)"><i class="fa fa-list-alt"></i> Listar</a></li>
                                    </ul>
                                </li>                     
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div id="listadsolicitudes" class="x_content">
                            <table id="tblguias" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>C. COSTO</th>
                                        <th>NRO.</th>
                                        <th>FECHA</th>
                                        <th>PROVEEDOR</th>
                                        <th>ESTADO</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><button type="button" class="btn btn-xs" ><i class="fa fa-eye" onclick="mostrarform(true)"></i></button>
                                            <button type="button" class="btn btn-xs" ><i class="fa fa-print"></i></button></td>
                                        <td>KM81881 - CENTRO COSTO</td>
                                        <td>202</td>
                                        <td>17/07/2019</td>
                                        <td>PC FACTORY</td>
                                        <td><button type="button" class="btn btn-xs" style="background-color:grey; color: #fff;">Generada</button></td>

                                    </tr>
                                    <tr>
                                        <td><button type="button" class="btn btn-xs" ><i class="fa fa-eye" onclick="mostrarform(true)"></i></button>
                                            <button type="button" class="btn btn-xs" ><i class="fa fa-print"></i></button></td>
                                        <td>KM81881 - CENTRO COSTO</td>
                                        <td>201</td>
                                        <td>17/07/2019</td>
                                        <td>PC FACTORY</td>
                                        <td><button type="button" class="btn btn-xs" style="background-color: #006600; color: #fff;">Pagada</button></td>

                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div id="formsolicitudes" class="x_content" style="display: none ;">
                            <div class="col-md-12 center-margin">


                                <div class="tab-pane active" id="home">
                                    <ul class="stats-overview">
                                        <li>
                                            <span class="name"> N° ORDEN DE COMPRA </span>
                                            <span class="value text-success"> 202 </span>
                                        </li>
                                        <li>
                                            <span class="name"> TOTAL </span>
                                            <span class="value text-success"> 200.000 </span>
                                        </li>
                                        <li class="hidden-phone">
                                            <span class="name"> PRIORIDAD </span>
                                            <span class="value text-success"><span class="label label-success">normal</span></span>
                                        </li>
                                    </ul>
                                    <div class="x_panel">
                                        <div class="col-sm-6 invoice-col">
                                            <ul class="bs-glyphicons-list">
                                                <li>
                                                    <h5><b>Solicitante: </b></h5><p id="tabcontrato" name="tabcontrato">Aaron Zuñiga</p>
                                                </li> 
                                                <li>
                                                    <h5><b>Centro de costo: </b></h5><p id="tabedificio" name="tabedificio">KM81881 - CENTRO COSTO</p>
                                                </li> 
                                                <li>
                                                    <h5><b>Condicion de pago: </b></h5><p id="tabtipo" name="tabtipo">30 días</p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-6 invoice-col">
                                            <ul class="bs-glyphicons-list">
                                                <li>
                                                    <h5><b>Fecha Solicitud: </b></h5><p id="tabcodigo" name="tabcodigo">17/07/2019</p>
                                                </li>
                                                <li>
                                                    <h5><b>Proveedor: </b></h5><p id="tabcliente" name="tabcliente">PC FACTORY</p>
                                                </li>
                                                <li>
                                                    <h5><b>Moneda: </b></h5><p id="tabmarca" name="tabmarca">Euro</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="x_panel">
                                        <div class="x_title">DETALLE</div>
                                        <table id="detalle" class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>PRODUCTO</th>
                                                    <th>CANTIDAD</th>
                                                    <th>VALOR</th>
                                                    <th>TOTAL</th>
                                                    <th>OBSERVACIÓN</th>
                                                    <th>#</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>COMPUTADOR</td>
                                                    <td>1</td>
                                                    <td>250000</td>
                                                    <td>250000</td>
                                                    <td>All In One, Lenovo 19,5", 8gb, 1Tb</td>
                                                    <td><button type="button" class="btn btn-xs" ><i class="fa fa-trash"></i></button></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="x_panel">
                                        <form class="form-horizontal form-label-left" id="formulario" name="formulario">
                                            <fieldset class="well">
                                                <div class="x_title">FORMULARIO PAGO</div>
                                                <div class="x_panel">
                                                    <div class="x_title">ADJUNTAR COMPROBANTE DE PAGO</div>
                                                    <div class="form-group">
                                                        <label>Adjuntar nuevo archivo</label>
                                                        <input type="file" name="archivo" id="archivo">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Observación</label>
                                                        <textarea id="observacion" class="form-control"></textarea>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <div class="col-md-12 col-sm-12 col-xs-12 ">
                                                            <button class="btn btn-primary" type="button" id="btnCancelar" onclick="mostrarform(false)">Cancelar</button>
                                                            <button class="btn btn-success" type="button" id="btnGuardar">Guardar</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <?php require 'footer.php'; ?>

    <script type="text/javascript" src="scripts/soladquisiciones.js"></script>

    <?php
}
ob_end_flush();
