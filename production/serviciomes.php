<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {

    require 'header.php';

    if ($_SESSION['administrador'] == 1 || $_SESSION['GGuias'] == 1 || $_SESSION['AGuias'] == 1 || $_SESSION['Contabilidad'] == 1) {
        ?>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>GUIAS DE SERVICIO DEL MES</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-tooltip="tooltip" title="Operaciones" role="button" aria-expanded="false"><i class="fa fa-cog"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a id="op_listar" onclick="mostarform(false)"><i class="fa fa-list-alt"></i> Listar</a>
                                            </li>
                                        </ul>
                                    </li>                     
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div id="listadoguias" class="x_content">

                                <table id="tblguias" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>OPCIONES</th>
                                            <th>SERVICIO</th>
                                            <th>TIPO</th>
                                            <th>EQUIPO</th>
                                            <th>EDIFICIO</th>
                                            <th>FECHA</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>


                            <!-- Formulario modificar ascensor -->
                            <div id="mostrarguia" class="x_content">


                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <!-- /page content -->

            <!-- /page content -->
            <?php
        } else {
            require 'nopermiso.php';
        }
        require 'footer.php';
        ?>
        <script src="../public/build/js/libs/png_support/png.js"></script>
        <script src="../public/build/js/libs/png_support/zlib.js"></script>
        <script src="../public/build/js/jspdf.debug.js"></script>
        <script src="../public/build/js/jspdf.plugin.autotable.js"></script>
        <script src="../public/build/js/jsPDFcenter.js"></script>
        <script type="text/javascript" src="scripts/serviciomes.js"></script>
        <?php
    }
    ob_end_flush();
    ?>