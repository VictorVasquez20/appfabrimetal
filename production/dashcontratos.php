<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {

    require 'header.php';
    ?>
    <!-- Contenido -->
    <div class="right_col" role="main">

        <div class="row top_tiles" style="text-align: center;">
            <div class="animated flipInY col-lg-3 col-md-6 col-sm-12 col-xs-12">
                <div class="tile-stats">
                    <div class="count" id="num_contrato"></div>
                    <h3>Contratos</h3>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-6 col-sm-12 col-xs-12">
                <div class="tile-stats">
                    <div class="count" id="num_clientes"></div>
                    <h3>Clientes</h3>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-6 col-sm-12 col-xs-12">
                <div class="tile-stats">
                    <div class="count" id="num_edificios"></div>
                    <h3>Edificios</h3>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-6 col-sm-12 col-xs-12">
                <div class="tile-stats">
                    <div class="count" id="num_ascensores"></div>
                    <h3>Ascensores</h3>
                </div>
            </div>
        </div>
        <!-- Contenido Graficos -->
        <div id="Charts" class="row">         
            <div class="col-md-12 col-sm-12 col-xs-12">

                <div class="x_panel">
                    <div class="x_title">
                        <h2>Ascensores por región</h2>   
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="form-group">
                            <div class="col-md-5">
                                <label>MES</label>
                                <select id="mes" name="mes" class="selectpicker form-control" data-live-search="true">
                                    <option value="0">TODOS</option>
                                    <option value="1">ENERO</option>
                                    <option value="2">FEBRERO</option>
                                    <option value="3">MARZO</option>
                                    <option value="4">ABRIL</option>
                                    <option value="5">MAYO</option>
                                    <option value="6">JUNIO</option>
                                    <option value="7">JULIO</option>
                                    <option value="8">AGOSTO</option>
                                    <option value="9">SEPTIEMBRE</option>
                                    <option value="10">OCTUBRE</option>
                                    <option value="11">NOMVIEMBRE</option>
                                    <option value="12">DICIEMBRE</option>
                                </select>
                            </div>
                            <div class="col-md-5">
                                <label>AÑO</label>
                                <select id="ano" name="ano" class="selectpicker form-control" data-live-search="true"></select>
                            </div>
                            <div class="col-md-2">
                                <br>
                                <button type="button" class="btn btn-info" onclick="ActializarGraficosXregion();">BUSCAR</button>
                            </div>
                        </div>
                    </div>
                    <div class="x_content">
                        <div class="panel">
                            <canvas id="mybarChart"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Contratos en el año anterior</h2>                   
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <canvas id="mybarChartant"></canvas>
                    </div>
                </div>
            </div>           
        </div>
        <!-- /Fin contenido graficos -->

    </div>
    <!-- /Fin Contenido -->

    <?php
    require 'footer.php';
    ?>
    <script type="text/javascript" src="scripts/estado.js"></script>

    <?php
}
ob_end_flush();
?>
