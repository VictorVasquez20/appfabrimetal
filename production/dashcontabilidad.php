<?php
ob_start();
session_start();
require 'header.php';
?>
<div class="right_col" role="main">
    <div class="row">
        <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12" style="overflow:hidden;">
                <div class="x_panel">
                    <div class="x_title">
                        <h2 id="mes"></h2> <h3>CONTABILIDAD <span id="mesact"></span></h3> 
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row top_tiles" style="text-align: center;">
                            <div class="animated flipInY col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="tile-stats">
                                    <div class="count" id="tot"></div>
                                    <h3>TOTAL GSE</h3>
                                </div>
                            </div>
                            <div class="animated flipInY col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="tile-stats">
                                    <div class="count" id="totfact"></div>
                                    <h3>GSE FACTURADAS</h3>
                                </div>
                            </div>
                            <div class="animated flipInY col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="tile-stats">
                                    <div class="count" id="totporc"></div>
                                    <h3>% FACTURACIÓN</h3>
                                </div>
                            </div>
                            <!--<div class="animated flipInY col-lg-3 col-md-6 col-sm-12 col-xs-12">
                                <div class="tile-stats">
                                    <div class="count" id="emer"></div>
                                    <h3>EMERGENCIA</h3>
                                </div>
                            </div>-->
                        </div>
                    </div>
                </div>
                <div class="x_panel">
                    <div class="x_title">
                        <h3>MANTENCIÓN</h3> 
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="col-md-7 col-sm-12 col-xs-12" style="overflow:hidden;">
                            <div class="row tile_count ">
                                <div class="col-md-12 col-sm-12 col-xs-12" style="overflow:hidden;">
                                    <div class="col-md-12" style="overflow:hidden;">
                                        <canvas height="100%" id="grafico"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-12 col-xs-12" style="overflow:hidden;">
                            <div class="row top_tiles">
                                <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="tile-stats">
                                        <div class="count" id="mant"></div>
                                        <h3>TOTAL GSE MANTENCIÓN</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="row top_tiles">
                                <div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="tile-stats">
                                        <div class="count" id="mantfact"></div>
                                        <h3>FACTURADAS</h3>
                                    </div>
                                </div>
                                <div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="tile-stats">
                                        <div class="count" id="mantporc"></div>
                                        <h3>PORCENTAJE (%)</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-sm-12 col-xs-12" style="overflow:hidden;">
                    <div class="x_panel">
                        <div class="x_title">
                            <h3>REPARACIÓN</h3> 
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="col-md-12 col-sm-12 col-xs-12" style="overflow:hidden;">
                                <div class="row tile_count ">
                                    <div class="col-md-4 col-sm-6 col-xs-12 tile_stats_count">
                                        <span class="count_top"><i class="fa fa-clock-o"></i> #GSE TOTAL REPARACIÓN</span>
                                        <div class="count" id="rep"></div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-12 tile_stats_count">
                                        <span class="count_top"><i class="fa fa-clock-o"></i> FACTURADAS</span>
                                        <div class="count" id="repfact"> </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-12 tile_stats_count">
                                        <span class="count_top"><i class="fa fa-clock-o"></i> PORCENTAJE (%)</span>
                                        <div class="count" id="repporc"></div>
                                    </div>                                   
                                </div>
                                <div class="row tile_count ">
                                    <div class="col-md-12 col-sm-12 col-xs-12" style="overflow:hidden;">
                                        <div class="col-md-12" style="overflow:hidden;">
                                            <canvas height="55%" id="reparacion"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-sm-12 col-xs-12" style="overflow:hidden;">
                    <div class="x_panel">
                        <div class="x_title">
                            <h3>EMERGENCIA</h3> 
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="col-md-12 col-sm-12 col-xs-12" style="overflow:hidden;">
                                <div class="row tile_count ">
                                    <div class="col-md-4 col-sm-6 col-xs-12 tile_stats_count">
                                        <span class="count_top"><i class="fa fa-clock-o"></i> #GSE TOTAL EMERGENCIA</span>
                                        <div class="count" id="emer"></div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-12 tile_stats_count">
                                        <span class="count_top"><i class="fa fa-clock-o"></i> FACTURADAS</span>
                                        <div class="count" id="emerfact"> </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-12 tile_stats_count">
                                        <span class="count_top"><i class="fa fa-clock-o"></i> PORCENTAJE (%)</span>
                                        <div class="count" id="emerporc"></div>
                                    </div>                                   
                                </div>                                 

                                <div class="row tile_count ">
                                    <div class="col-md-12 col-sm-12 col-xs-12" style="overflow:hidden;">
                                        <div class="col-md-12" style="overflow:hidden;">
                                            <canvas height="55%" id="emergencia"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>

<?php
require 'footer.php';
?>
<script src="../production/scripts/dashcontabilidad.js"></script>

ob_end_flush();