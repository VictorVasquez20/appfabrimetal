<?php
ob_start();
session_start();

if(!isset($_SESSION["nombre"])){
    header("Location:login.php");
}else{
    
    require 'header.php';
    
    if( $_SESSION['administrador']==1)
    {
        
 ?>
        
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Solicitudes de identificadores para ascensores</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-tooltip="tooltip" title="Operaciones" role="button" aria-expanded="false"><i class="fa fa-cog"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a id="op_actualizar"><i class="fa fa-refresh"></i> Actualizar</a>
                          </li>
                        </ul>
                      </li>                     
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div id="listadosolicitudesid" class="x_content">

                    <table id="tblsolicitudesid" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Opciones</th>
                          <th>Contrato</th>
                          <th>Fecha</th>
                          <th>Region</th>
                          <th>N° Edificios</th>                          
                          <th>N° Ascensores</th>                          
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>


                  <div id="formulariosolicitudesid" class="x_content">
                    <br />
                    <div class="clearfix"></div>
                    <form id="formulario" name="formulario" method="post" class="form-horizontal form-label-left">
                    <div id="ascensoressoliid" name="ascensoressoliid">

                    </div> 
                    <div class="clearfix"></div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 left-margin">
                        <button class="btn btn-primary" type="button" id="btnCancelar"  onclick="cancelarform()">Cancelar</button>
                        <button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiar()">Limpiar</button>
                        <button class="btn btn-success" type="submit" id="btnGuardar">Agregar</button>
                      </div>
                    </div>

                    </form>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

  <!-- /page content -->
<?php 
}else{
  require 'nopermiso.php';
}
require 'footer.php';
?>
<script type="text/javascript" src="scripts/idnumbers.js"></script>
<?php 
}
ob_end_flush();
?>