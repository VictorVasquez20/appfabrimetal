<?php include_once 'header.php'; ?>
<div class="right_col" role="main">
    <div class="">

        <div class="clearfix"></div>

        <div class="row">
            
            <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h3>Dashboard Instalaciones por PM</h3> 
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <select class="form-control selectpicker" data-live-search="true" id="idpm" name="idpm" required="required"></select>                           
                    </div>  
                </div>
            </div>       
            
            <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12 resultados" style="display: none" >
                 <div class="x_panel">
                    <div class="x_title">
                        <h4>Resumen</h4> 
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">                       
                        <div class="row top_tiles">
                            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                              <div class="tile-stats">
                                <div class="icon"><i class="fa fa-folder-open"></i>
                                </div>
                                  <div class="count" id="totalProy">0</div>
                                <h5>&nbsp;&nbsp;TOTAL EQUIPOS</h5>
                                <p>&nbsp;</p>
                              </div>
                            </div>
                  
                            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                              <div class="tile-stats">
                                <div class="icon"><i class="fa fa-user"></i>
                                </div>
                                  <div class="count" id="proyIniciar">0</div>
                                <h5>&nbsp;&nbsp;TOTAL EQUIPOS ASIGNADOS</h5>
                                <p id="proyIniciarPorcentaje"></p>
                              </div>
                            </div>
                  
                            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                              <div class="tile-stats">
                                <div class="icon"><i class="fa fa-wrench"></i>
                                </div>
                                  <div class="count" id="proyEjecucion">0</div>
                                <h5>&nbsp;&nbsp;TOTAL EQUIPOS EN EJECUCION</h5>
                                <p id="proyEjecucionPorcentaje"></p>
                              </div>
                            </div>
                  
                            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                              <div class="tile-stats">
                                <div class="icon"><i class="fa fa-check-square-o"></i>
                                </div>
                                  <div class="count" id="proyEntregados">0</div>
                                <h5>&nbsp;&nbsp;TOTAL EQUIPOS ENTREGADOS</h5>
                                <p id="proyEntregadosPorcentaje"></p>
                              </div>
                            </div>
                          </div>      
                    </div>  
                </div>
            </div>

            <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12 resultados" style="display: none">
                <div class="x_panel">
                    <div class="x_title">
                        <h4>Cantidad de Equipos en Ejecuci&oacute;n</h4> 
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">                       
                            <div class="row tile_count" id="div_estado_proyectos"></div>                      
                    </div>  
                </div>
            </div>
            
            <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12 resultados" style="display: none" >
                <div class="x_panel">
                    <div class="x_title">
                        <h4>Gr&aacute;fico Cantidad de Equipos por Estado</h4> 
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content" id="divGraficoProyectosEstado"> </div>  
                </div>
            </div>
            
            <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12 resultados" style="display: none">
                <div class="x_panel">
                    <div class="x_title">
                        <h4>Visitas a Ascensores por Supervisor</h4>
                    </div>
                    <div class="x_content" id="divVisitasAscensores"></div>
                </div>
            </div>

            <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12 resultados" style="display: none" >
                <div class="x_panel">
                    <div class="x_title">
                        <h4>Tabla Resumen de Supervisores</h4>
                    </div>
                    <div class="x_content">
                         <table id="resumenVisitaSupervisor" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>SUPERVISOR</th>
                                    <th>PROYECTOS ASIGNADOS</th>
                                    <th>EQUIPOS</th>
                                    <th>PARADAS</th>
                                    <th>PROMEDIO DE DIFICULTAD</th>
                                    <th>VISITAS PROYECTOS</th>
                                    <th>VISITAS EQUIPOS</th>
                                    <th>PROMEDIO VISITAS PROYECTOS</th>
                                    <th>PROMEDIO VISITAS EQUIPOS</th>
                                    <th>PROMEDIO VISTAS PROYECTOS POR MES</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
      
        </div>
    </div>
</div>
<?php require 'footer.php'; ?>
<script src="../production/scripts/dashboardinstalaciones.js"></script>