<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {

    require 'header.php';

    if ($_SESSION['administrador'] == 1 || $_SESSION['Contratos'] == 1) {
        ?>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2 id="titulo_pagina"></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-tooltip="tooltip" title="Operaciones" role="button" aria-expanded="false"><i class="fa fa-cog"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a id="op_actualizar"><i class="fa fa-refresh"></i> Actualizar</a>
                                            </li>
                                            <li><a id="op_listar" onclick="mostarform(false)"><i class="fa fa-list-alt"></i> Listar</a>
                                            </li>
                                        </ul>
                                    </li>                     
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div id="listadoclientes" class="x_content">

                                <table id="tblclientes" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>OPCIONES</th>
                                            <th>RAZON SOCIAL</th>
                                            <th>RUT</th>
                                            <th>DIRECCION</th>
                                            <th>COMUNA</th>
                                            <th>REGION</th>                          
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>


                            <div id="formularioclientesid" class="x_content">

                                <div class="col-md-12 center-margin">
                                    <form class="form-horizontal form-label-left" id="formulario" name="formulario">
                                        <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                            <label>Razon Social <span class="required">*</span></label>
                                            <input type="hidden" id="idcliente" name="idcliente" class="form-control">
                                            <input type="text" class="form-control" name="razon" id="razon" required="Campo requerido">
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                            <label>RUT <span class="required">*</span></label>
                                            <input type="text" class="form-control" name="rut" id="rut" required="Campo requerido">
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                            <label>Tipo <span class="required">*</span></label>
                                            <select class="form-control selectpicker" data-live-search="true" id="idtcliente" name="idtcliente" required="Campo requerido">
                                            </select>
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                            <label>Avenida o Calle <span class="required">*</span></label>
                                            <input type="text" class="form-control" name="calle" id="calle" required="Campo requerido">
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label>Numero <span class="required">*</span></label>
                                            <input type="text" class="form-control" name="numero" id="numero" required="Campo requerido">
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label>Oficina</label>
                                            <input type="text" class="form-control" name="oficina" id="oficina">
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                            <label>Region <span class="required">*</span></label>
                                            <select class="form-control selectpicker" data-live-search="true" id="idregiones" name="idregiones" required="Campo requerido">
                                            </select>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                            <label>Provincia <span class="required">*</span></label>
                                            <select class="form-control selectpicker" data-live-search="true" id="idprovincias" name="idprovincias" required="Campo requerido">
                                            </select>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                            <label>Comuna <span class="required">*</span></label>
                                            <select class="form-control selectpicker" data-live-search="true" id="idcomunas" name="idcomunas" required="Campo requerido">
                                            </select>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="ln_solid"></div> 

                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <button class="btn btn-primary" type="button" id="btnCancelar" onclick="cancelarform()">Cancelar</button>
                                                <button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiar()">Limpiar</button>
                                                <button class="btn btn-success" type="submit" id="btnGuardar">Agregar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div id="tabcliente" class="x_content">   
                                <div class="col-md-12 col-sm-12 col-xs-12" style="border:0px solid #e5e5e5;">
                                    <h1 class="prod_title" id="tabrazonti" name="tabrazonti"></h1>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <ul class="">
                                            <li>
                                                <h5><b>Razon Social: </b></h5><p id="tabrazon" name="tabrazon"></p>
                                            </li>
                                            <li>
                                                <h5><b>RUT: </b></h5><p id="tabrut" name="tabrut"></p>
                                            </li>
                                            <li>
                                                <h5><b>Tipo: </b></h5><p id="tabtipo" name="tabtipo"></p>
                                            </li>                                            
                                        </ul>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <ul class="">
                                            <li>
                                                <h5><b>Region: </b></h5><p id="tabregion" name="tabregion"></p>
                                            </li>
                                            <li>
                                                <h5><b>Provincia: </b></h5><p id="tabprovincia" name="tabprovincia"></p>
                                            </li>
                                            <li>
                                                <h5><b>Comuna: </b></h5><p id="tabcomuna" name="tabcomuna"></p>
                                            </li>
                                        </ul>
                                    </div>


                                    <div class="col-md-12">

                                        <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#tab_content1" role="tab" id="home-tab"  data-toggle="tab" aria-expanded="true">Contratos</a>
                                                </li>
                                                <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab"  data-toggle="tab" aria-expanded="false">Edificios</a>
                                                </li>
                                                <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Ascensores</a>
                                                </li>
                                                <li role="presentation" class=""><a href="#tab_content4" role="tab" id="profile-tab3" data-toggle="tab" aria-expanded="false">Contactos Comerciales</a>
                                                </li>    
                                            </ul>

                                            <div id="myTabContent" class="tab-content">
                                                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                                                    <table id="tbltabcontratos" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th>OPCIONES</th> 
                                                                <th>N° CONTRATO</th>
                                                                <th>FECHA</th>
                                                                <th>TIPO</th>                                                              
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>                                                    
                                                </div>
                                                <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                                                    <table id="tbltabedificios" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th>OPCIONES</th>
                                                                <th>NOMBRE</th>
                                                                <th>DIRECCION</th>
                                                                <th>SEGMENTO</th>
                                                                <th>REGION</th>
                                                                <th>COMUNA</th>                                                               
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>    
                                                </div>
                                                <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                                                    <table id="tbltabascensores" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th>OPCIONES</th>
                                                                <th>CODIGO</th>
                                                                <th>TIPO</th>
                                                                <th>MARCA</th>
                                                                <th>MODELO</th>
                                                                <th>VALOR UF</th>
                                                                <th>EDIFICIO</th>
                                                                <th>CONTRATO</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="profile-tab">
                                                    <table id="tbltabcontactos" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th>OPCIONES</th>
                                                                <th>NOMBRE</th>
                                                                <th>EMAIL</th>
                                                                <th>TELF. MOVIL</th>
                                                                <th>TELF. RESIDENCIAL</th>  
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>                                                  
                                                </div>   
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- /page content -->
        <?php
    } else {
        require 'nopermiso.php';
    }
    require 'footer.php';
    ?>
    <script type="text/javascript" src="scripts/cliente.js"></script>
    <?php
}
ob_end_flush();
?>