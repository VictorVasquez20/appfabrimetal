<?php
ob_start();
session_start();
require 'header.php';
?>
<div class="right_col" role="main">
    <div class="row">
        <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <div class="x_panel">
                <div class="x_title">
                    <h3>COR</h3>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content text-center">
                    <div class="col-md-12 col-sm-12 col-xs-12" style="overflow:hidden;">
                        <div class="row tile_count ">
                            <div class="col-md-4 col-sm-6 col-xs-12 tile_stats_count">
                                <input type='hidden' name='cartera' id='cartera' />
                                <input type='hidden' name='gseem' id='gseem' />
                                <span class="count_top"><i class="fa fa-exclamation-circle"></i> TOTAL</span>
                                <div class="count" id="totalcor">0</div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12 tile_stats_count" style="color: #3498DB;">
                                <span class="count_top"><i class="fa fa-exclamation-circle"></i> REG. METROPOLITANA</span>
                                <div class="count" id="totalcorRM">0</div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12 tile_stats_count" style="color: #455C73;">
                                <span class="count_top"><i class="fa fa-exclamation-circle"></i> REGIONES</span>
                                <div class="count" id="totalcorREG">0</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12" style="overflow:hidden;">
                        <div class="row tile_count ">
                            <div class="col-md-4 col-sm-6 col-xs-12 tile_stats_count">
                                <input type='hidden' name='cartera' id='cartera' />
                                <input type='hidden' name='gseem' id='gseem' />
                                <span class="count_top"><i class="fa fa-exclamation-circle"></i> GSE Emergencia</span>
                                <div class="count" id="totalgseEme">0</div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12 tile_stats_count" style="color: #3498DB;">
                                <span class="count_top"><i class="fa fa-exclamation-circle"></i> Equipos en cartera</span>
                                <div class="count" id="totalCartera">0</div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12 tile_stats_count" style="color: #455C73;">
                                <span class="count_top"><i class="fa fa-exclamation-circle"></i> Promedio GSE Emergencia diario</span>
                                <div class="count" id="promGseEm">0</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="x_panel">
                <div id="filtro" class="x_content">
                    <div class="form-group">
                        <div class="col-md-2">
                            <label>REGIÓN</label>
                            <select id="selregion" name="selregion" class="selectpicker form-control">
                                <option value="">Seleccione</option>
                                <option value="TODAS">TODAS</option>
                                <option value="13">METROPOLITANA</option>
                                <option value="REG">REGIONES</option>
                                <option value="15">ARICA Y PARINACOTA</option>
                                <option value="01">TARAPACÁ</option>
                                <option value="02">ANTOFAGASTA</option>
                                <option value="03">ATACAMA</option>
                                <option value="04">COQUIMBO</option>
                                <option value="05">VALPARAÍSO</option>
                                <option value="06">O'HIGGINS</option>
                                <option value="07">MAULE</option>
                                <option value="16">ÑUBLE</option>
                                <option value="08">BÍO BÍO</option>
                                <option value="09">ARAUCANÍA</option>
                                <option value="14">LOS RÍOS</option>
                                <option value="10">LOS LAGOS</option>
                                <option value="11">AYSÉN</option>
                                <option value="12">MAGALLANES</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label>SUPERVISOR</label>
                            <select id="selsuperv" name="selsuperv" data-live-search="true" class="selectpicker form-control">
                                <option value="">Seleccione</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label>TÉCNICO</label>
                            <select id="seltecnico" name="seltecnico" data-live-search="true" class="selectpicker form-control">
                                <option value="">Seleccione</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <br>
                            <button type="button" class="btn btn-info" onclick="buscar();">BUSCAR</button>
                        </div>
                        <div class="col-md-2">
                            <br>
                            <button type="button" class="btn btn-info" onclick="detalle();" data-toggle="modal" data-target="#modalDetalle">DETALLE</button>
                        </div>
                    </div>
                </div>

                <div class="x_content text-center">
                    <div class="col-md-12 col-sm-12 col-xs-12" style="overflow:hidden;">
                        <div class="row tile_count ">
                            <div class="col-md-4 col-sm-6 col-xs-12 tile_stats_count" style="color: #3498DB;">
                                <span class="count_top"><i class="fa fa-exclamation-circle"></i> ULT. 7 DÍAS</span>
                                <div class="count" id="totalcor7Filtro">0</div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12 tile_stats_count" style="color: #455C73;">
                                <span class="count_top"><i class="fa fa-exclamation-circle"></i> ULT. 15 DÍAS</span>
                                <div class="count" id="totalcor15Filtro">0</div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12 tile_stats_count" style="color: #455C73;">
                                <span class="count_top"><i class="fa fa-exclamation-circle"></i> ULT. 30 DÍAS</span>
                                <div class="count" id="totalcorFiltro">0</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="x_content text-center">
                    <div class="col-md-12 col-sm-12 col-xs-12" style="overflow:hidden;">
                        <div class="row tile_count ">
                            <div class="col-md-6 col-sm-6 col-xs-12 tile_stats_count">
                                <span class="count_top"><i class="fa fa-exclamation-circle"></i> CANTIDAD GSE EMERGENCIA ULT. 30 DÍAS</span>
                                <div class="count" id="totalGseFiltro">0</div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 tile_stats_count" style="color: #3498DB;">
                                <span class="count_top"><i class="fa fa-exclamation-circle"></i> CANTIDAD DE EQUIPOS</span>
                                <div class="count" id="totalCarteraFiltro">0</div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>

        <div class="modal fade" id="modalDetalle" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">DETALLE GSE EMERGENCIA</h4>
                    </div>
                    <div class="modal-body">
                        <table id="tblGEm" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>OPCIONES</th>
                                    <th>GUÍA</th>
                                    <th>EDIFICIO</th>
                                    <th>EQUIPO</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                    <!-- Modal Footer -->
                    <div class="modal-footer">
                        <button type="reset" onclick="limpiaredificio()" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</div>

<?php
require 'footer.php';
?>
<script src="../public/build/js/jspdf.debug.js"></script>
    <script src="../public/build/js/jspdf.plugin.autotable.js"></script>
    <script src="../public/build/js/jsPDFcenter.js"></script>
    <script src="../public/build/js/SimpleTableCellEditor.js"></script>
<script src="../production/scripts/dashcor.js"></script>