<?php
session_start();

if(!isset($_SESSION["nombre"])){
    
  header("Location:login.php");
  
}else{

require 'header.php';
?>

<!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                      <h2>Gesti&oacute;n de Llamadas  del D&iacute;a</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                    
                  <div  class="x_content">
                    <table id="tbl_gestionllamadas" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Opciones</th>
                          <th>Origen</th>
                          <th>Destino</th>
                          <th>Fecha</th>
                          <th>Hora</th>
                          <th>Duraci&oacute;n</th>
                          <th>Conversaci&oacute;n</th>
                          <th>Tiempo Tono</th>
                          <th>Tonos</th>
                          <th>Tipo de llamada</th>
                          <th>Estado</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>

                    <!-- Modal -->
           <div class="modal fade" id="ModalGestion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
             <div class="modal-dialog" role="document">
               <div class="modal-content">
                 <div class="modal-header">
                
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                   </button>
                        <h5 class="modal-title" id="tituloModal"></h5>
                 </div>
                   <form id="formGestion" name="formGestion">
                       <input type="hidden" name="uniqueid" id="uniqueid">  
                       <input type="hidden" name="src" id="src">
                       <input type="hidden" name="dst" id="dst">
                       <input type="hidden" name="calldate" id="calldate">
                       <input type="hidden" name="estado" id="estado">
                        <div class="modal-body">
                            <div class="form-group row">
                            <label for="src" class="col-sm-4 col-form-label">Origen de la Llamada</label>
                            <div class="col-sm-8">
                                <p class="form-control-static" id="text_src"></p>
                            </div>
                            </div>

                             <div class="form-group row">
                            <label for="dst" class="col-sm-4 col-form-label">Destino de la llamada</label>
                            <div class="col-sm-8">
                                <p class="form-control-static" id="text_dst"></p>
                            </div>
                            </div>

                            <div class="form-group row">
                            <label for="calldate" class="col-sm-4 col-form-label">Fecha de la llamada</label>
                            <div class="col-sm-8">
                                <p class="form-control-static" id="text_calldate"></p>
                            </div>
                            </div>
                                                        
                            <div class="form-group row" id="form_rechazo">
                            <label for="observacion" class="col-sm-4 col-form-label">Indique el motivo</label>
                            <div class="col-sm-8">
                                <textarea class="form-control" id="observacion" name="observacion" rows="3" style="text-transform: uppercase" ></textarea>
                            </div>
                            </div>
                            
                            <div class="x_panel" id="form_ticket">
                                 <div class="x_title">
                                    <h2>Ticket de Emergencia</h2>
                                  <div class="clearfix"></div>
                                </div>
                             
                            <div  class="x_content">
                                    <div class="form-group row">
                                     <label for="edificio" class="col-sm-4 col-form-label">Edificio:</label>
                                     <div class="col-sm-8">
                                         <select class="form-control selectpicker" name="edificio" id="edificio" data-live-search="true" ></select>
                                     </div>
                                    </div> 
                                    <div class="form-group row">
                                     <label for="ascensor" class="col-sm-4 col-form-label">Ascensor:</label>
                                     <div class="col-sm-8">
                                         <select class="form-control selectpicker" name="ascensor" id="ascensor" data-live-search="true" ></select>
                                     </div>
                                    </div> 
                                   <h4><b>INFORMACION DE LA EMERGENCIA</b></h4>
                                    <div class="clearfix"></div>
                                    <div class="form-group row">
                                        <label for="descripcion" class="col-sm-4 col-form-label">Descripci&oacute;n de la Emergencia:</label>
                                        <div class="col-sm-8">
                                            <textarea class="form-control" name="descripcion" id="descripcion" style="text-transform: uppercase"></textarea>
                                        </div>
                                    </div>
                                    <h4><b>INFORMACION DEL CONTACTO</b></h4>
                                    <div class="clearfix"></div>
                                    <div class="form-group row">
                                      <label for="nombre" class="col-sm-4 col-form-label">Nombre y Apellidos:</label>
                                      <div class="col-sm-8">
                                          <input type="text" class="form-control" id="nombre" name="nombre" style="text-transform: uppercase">
                                      </div>
                                    </div>
                                    <div class="form-group row">
                                         <label for="correo" class="col-sm-4 col-form-label">Correo Electr&oacute;nico:</label>
                                    <div class="col-sm-8">
                                        <input type="email" class="form-control" id="correo" name="correo" style="text-transform: uppercase">
                                    </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="telefono" class="col-sm-4 col-form-label">Tel&eacute;fono:</label>
                                      <div class="col-sm-8">
                                          <input type="text" class="form-control" id="telefono" name="telefono">
                                      </div>
                                    </div> 
                               </div>
                            </div> 
                                                      
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                          <button type="submit" id="btnGuardarModalGestion" class="btn btn-primary">Guardar</button>
                        </div>
                     </form>
               </div>
             </div>
           </div>
                    
 
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->


<?php 
require 'footer.php';
?>

<script type="text/javascript" src="scripts/gestionllamadas.js"></script>

<?php
}
