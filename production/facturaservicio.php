<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {

    require 'header.php';

    if ($_SESSION['administrador'] == 1 || $_SESSION['Contabilidad'] == 1) {
        ?>
        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>FACTURAS</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-tooltip="tooltip" title="Operaciones" role="button" aria-expanded="false"><i class="fa fa-cog"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a id="op_listar" onclick="cancelar()">Listar</a></li>
                                        </ul>
                                    </li>                     
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div id="filtroguias" class="x_content">
                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label>MES</label>
                                        <select id="mes" name="mes" class="selectpicker form-control">
                                            <option value="0">TODOS</option>
                                            <option value="1">ENERO</option>
                                            <option value="2">FEBRERO</option>
                                            <option value="3">MARZO</option>
                                            <option value="4">ABRIL</option>
                                            <option value="5">MAYO</option>
                                            <option value="6">JUNIO</option>
                                            <option value="7">JULIO</option>
                                            <option value="8">AGOSTO</option>
                                            <option value="9">SEPTIEMBRE</option>
                                            <option value="10">OCTUBRE</option>
                                            <option value="11">NOVIEMBRE</option>
                                            <option value="12">DICIEMBRE</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label>AÑO</label>
                                        <select id="ano" name="ano" class="selectpicker form-control"></select>
                                    </div>
                                    <div class="col-md-3">
                                        <label>ESTADO</label>
                                        <select id="estado" name="estado" class="selectpicker form-control">
                                            <option value="N">TODOS</option>
                                            <option value="0">CREADA</option>
                                            <option value="1">PAGADA</option>
                                            <option value="2">ANULADA</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <br>
                                        <button type="button" class="btn btn-info" onclick="listar();">BUSCAR</button>
                                    </div>
                                </div>
                            </div>
                            <div id="listadofacturas" class="x_content table-responsive">
                                <table id="tblfacturas" class="table table-bordered table-striped" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>OPCIONES</th>
                                            <th>NRO. FACTURA</th>
                                            <th>FECHA</th>
                                            <th>MONTO</th>
                                            <TH>CREADOR</TH>
                                            <th>ESTADO</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <div id="mostrarfacturas" class="x_content">
                                <form class="form-horizontal" name="formulariofactura" id="formulariofactura">
                                    <div class="x_content well">
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <input type="hidden" name="idfactura" id="idfactura">
                                                    <label class="control-label">NRO. FACTURA <span class="required">*</span></label>
                                                    <input type="text" name="nrofactura" id="nrofactura" class="form-control" readonly="readOnly">
                                                </div>
                                                <div class="col-md-3 col-sm-3 col-xs-12">
                                                    <label class="control-label">FECHA </label>
                                                    <div class="col-md-11 xdisplay_inputx form-group has-feedback">
                                                        <input type="text" class="form-control has-feedback-left" id="fecha" name="fecha" aria-describedby="inputSuccess2Status2" readonly="readOnly">
                                                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-3 col-xs-12">
                                                    <label class="control-label">MONTO </label>
                                                    <input type="text" class="date-picker form-control" id="monto" name="monto" required="required" readonly="readOnly">
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <label class="control-label">ESTADO<span class="required">*</span></label>
                                                    <select class="selectpicker form-control" id="estado2" name="estado2" required="required">
                                                        <option value="0">CREADA</option>
                                                        <option value="1">PAGADA</option>
                                                        <option value="2">ANULADA</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <hr>
                                    <div class="clearfix"></div>
                                    <div class="x_title">
                                        <h2>GUIAS</h2>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <table id="tblguias" class="table table-bordered table-striped" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>SERVICIO</th>
                                                    <th>TIPO</th>
                                                    <th>EQUIPO</th>
                                                    <th>EDIFICIO</th>
                                                    <th>MES</th>
                                                    <th>FECHA</th>
                                                    <th>RUT</th>
                                                    <th>RAZON SOCIAL</th>  
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                        <div class="col-md-12 col-sm-12 col-xs-12 right">
                                            <button class="btn btn-primary" type="button" id="btnCancelar" onclick="cancelar();">CANCELAR</button>
                                            <button class="btn btn-success" type="submit" id="btnGuardar">GUARDAR</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- /page content -->
        <?php
    } else {
        require 'nopermiso.php';
    }
    require 'footer.php';
    ?>
    <script src="../public/build/js/libs/png_support/png.js"></script>
    <script src="../public/build/js/libs/png_support/zlib.js"></script>
    <script src="../public/build/js/jspdf.debug.js"></script>
    <script src="../public/build/js/jspdf.plugin.autotable.js"></script>
    <script src="../public/build/js/jsPDFcenter.js"></script>
    <script type="text/javascript" src="scripts/facturaservicio.js"></script>
    <?php
}
ob_end_flush();
?>
