<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {

    require 'header.php';

    if ($_SESSION['administrador'] == 1 || $_SESSION['GGuias'] == 1 || $_SESSION['Contabilidad'] == 1 || $_SESSION['APresupuesto'] == 1) { ?>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Ascensores Detenidos</h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <div class="row top_tiles" style="text-align: center;">
                                    <div class="animated flipInY col-lg-3 col-md-6 col-sm-12 col-xs-12">
                                        <div class="tile-stats">
                                            <div class="count" id="total_detenidos"></div>
                                            <h3>Total</h3>
                                        </div>
                                    </div>
                                    <div class="animated flipInY col-lg-3 col-md-6 col-sm-12 col-xs-12">
                                        <div class="tile-stats">
                                            <div class="count" id="en_evaluacion"></div>
                                            <h3>En Evaluación</h3>
                                        </div>
                                    </div>
                                    <div class="animated flipInY col-lg-3 col-md-6 col-sm-12 col-xs-12">
                                        <div class="tile-stats">
                                            <div class="count" id="cotizados"></div>
                                            <h3>Cotizados</h3>
                                        </div>
                                    </div>
                                    <div class="animated flipInY col-lg-3 col-md-6 col-sm-12 col-xs-12">
                                        <div class="tile-stats">
                                            <div class="count" id="aprobados"></div>
                                            <h3>Aprobados</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12" style="overflow:hidden;">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h3 id="">Guías de servicio finalizadas con el equipo detenido</h3> 
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <div class="panel">
                                            <canvas height="40%" id="mybarChart"></canvas>
                                        </div>
                                    </div>  
                                </div>
                            </div>

                            <div id="listadodetenidos" class="x_content">

                                <table id="tbldetenidos" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>GSE</th>
                                            <th>EDIFICIO</th>
                                            <th>EQUIPO</th>
                                            <th>FECHA</th>
                                            <th>TÉCNICO</th>
                                            <th>DÍAS DETENIDO</th>
                                            <th>REGIÓN</th>
                                            <th>ESTADO</th>
                                            <th>DEPTO RESPONSABLE</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php
    } else {
        require 'nopermiso.php';
    }
    require 'footer.php';
    ?>
    <script src="../public/build/js/libs/png_support/png.js"></script>
    <script src="../public/build/js/libs/png_support/zlib.js"></script>
    <script src="../public/build/js/jspdf.debug.js"></script>
    <script src="../public/build/js/jspdf.plugin.autotable.js"></script>
    <script src="../public/build/js/jsPDFcenter.js"></script>
    <script src="../public/build/js/SimpleTableCellEditor.js"></script>
    <script type="text/javascript" src="scripts/detenidos.js"></script>


<?php
}
ob_end_flush();
?>