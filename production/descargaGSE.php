<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {

    require 'header.php';

    if ($_SESSION['administrador'] == 1 || $_SESSION['GGuias'] == 1 || $_SESSION['AGuias'] == 1 || $_SESSION['Contabilidad'] == 1 || $_SESSION['APresupuesto'] == 1) {
        ?>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Guias de Servicio</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-tooltip="tooltip" title="Operaciones" role="button" aria-expanded="false"><i class="fa fa-cog"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a id="op_listar" onclick="mostarform(false)"><i class="fa fa-list-alt"></i> Listar</a>
                                            </li>
                                        </ul>
                                    </li>                     
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div id="filtroguias" class="x_content">
                                <div class="form-group">
                                    <div class="col-md-1">
                                        <label>SERVICIO</label>
                                        <select id="selservicio" name="selservicio" class="selectpicker form-control">
                                            <option value="" selected disabled="">Seleccione Sevicio</option>
                                            <option value="1">REPARACION</option>
                                            <option value="2">EMERGENCIA</option>
                                            <option value="3">MANTENCION</option>
                                            <option value="4">REPARACION MAYOR</option>
                                            <option value="5">INGENIERIA</option>
                                            <option value="6">NORMALIZACION</option>
                                            <option value="7">VISITA</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <label>EDIFICIO</label>
                                        <select id="seledificio" name="seledificio" required data-live-search="true" class="selectpicker form-control"></select>
                                    </div>
                                    <div class="col-md-2">
                                        <label>Equipo</label>
                                        <select id="selequipo" name="selequipo" data-live-search="true" class="selectpicker form-control"></select>
                                    </div>
                                    <div class="col-md-2">
                                        <label>DESDE</label>
                                        <input type="date" name="desde" id="desde" class="form-control" value="<?php echo date('Y-m');?>-01" max="<?php echo date('Y-m-d'); ?>">
                                    </div>
                                    <div class="col-md-2">
                                        <label>HASTA</label>
                                        <input type="date" name="hasta" id="hasta" class="form-control" max="<?php echo date('Y-m-d'); ?>" value="<?php echo date('Y-m-d'); ?>">
                                    </div>
                                    <div class="col-md-2">
                                        <label>SUPERVISOR</label>
                                        <select id="supervisor" name="supervisor" class="selectpicker form-control"></select>
                                    </div>
                                    <div class="col-md-1">
                                        <br>
                                        <button type="button" class="btn btn-info" onclick="listar();">BUSCAR</button>
                                    </div>
                                </div>
                            </div>
                            <div id="listadoguias" class="x_content">

                                <table id="tblguias" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th style="width: 35px;">#</th>
                                            <th>SERVICIO</th>
                                            <th>TIPO</th>
                                            <th>EQUIPO</th>
                                            <th>EDIFICIO</th>
                                            <th>SUPERVISOR</th>
                                            <th>TECNICO</th>                                           
                                            <th>C. COSTO</th>
                                            <th>F. INI.</th>
                                            <th>H. INI.</th>
                                            <th>F. FIN</th>
                                            <th>H. FIN</th>
                                            <th>HRS SERVICIO</th>
                                            <th>HRS NORMAL</th>
                                            <th>HRS EXTRA</th>
                                            <th>ESTADO</th>
                                            <th style="display: none;">oculta</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
        <!-- /page content -->
        <style type="text/css">
            #map { z-index: 10000000000000; height: 600px;}
        </style>
        <div class="modal fade" id="modalPreview" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" id="contenido">
                    
                </div>
            </div>
        </div>

        <div class="modal fade" id="modalMapa" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" id="contenido-mapa">
                    <div id="map"></div>
                </div>
            </div>
        </div>
        <?php
    } else {
        require 'nopermiso.php';
    }
    require 'footer.php';
    ?>
    
    <link rel="stylesheet" href="../public/build/css/select.bootstrap.css" />
    <script src="../public/build/js/libs/png_support/png.js"></script>
    <script src="../public/build/js/libs/png_support/zlib.js"></script>
    <script src="../public/build/js/jspdf.debug.js"></script>
    <script src="../public/build/js/jspdf.plugin.autotable.js"></script>
    <script src="../public/build/js/jsPDFcenter.js"></script>
    <script src="../public/build/js/SimpleTableCellEditor.js"></script>
    <script src="../public/build/js/dataTables.select.js"></script>

     <script id="myScript"></script>
     <script src="../public/build/js/filesaver.js"></script>
     <script src="../public/build/js/jszip.js"></script>
     <script src="scripts/descargaGSE.js"></script>
    <!-- <script>
      var url = 'scripts/descargaGSE.js';
      var extra = '?t=';
      var randomNum = String((Math.floor(Math.random() * 20000000000)));
      document.getElementById('myScript').src = url + extra + randomNum;
    </script> -->

    <script>
        $(document).ready(function () {

            editor = new SimpleTableCellEditor("tblguias");
            editor.SetEditableClass("editMe");

            $('#tblguias').on("cell:edited", function (event) {
                console.log(`'${event.oldValue}' changed to '${event.newValue}'`);
            });

        });

        $('#modalMapa').on('hidden.bs.modal', function (e) {
            $("#contenido-mapa").html('<div id="map"></div>');
})

    </script>
    <style type="text/css">
        table.dataTable tbody td.select-checkbox:before,
        table.dataTable tbody th.select-checkbox:before {
          content: " " !important;
          margin-top: 0px !important;
          margin-left: 0px !important;
          border: 1px solid black !important;
          border-radius: 3px !important;
          width: 15px !important;
          height: 15px !important;
        }

        table.dataTable tr.selected td.select-checkbox:after,
        table.dataTable tr.selected th.select-checkbox:after {
          content: "✓" !important;
          font-size: 14px !important;
          margin-top: -7px !important;
          margin-left: 0px !important;
          text-align: center !important;
          text-shadow: 1px 1px #B0BED9, -1px -1px #B0BED9, 1px -1px #B0BED9, -1px 1px #B0BED9 !important;
          width: 15px !important;
          height: 15px !important;
        }
    </style>

    <?php
}
ob_end_flush();
?>