<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {

    require 'header.php';

    if ($_SESSION['administrador'] == 1 || $_SESSION['Contabilidad'] == 1) {?>
        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Factura - Guias de Servicio</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-tooltip="tooltip" title="Operaciones" role="button" aria-expanded="false"><i class="fa fa-cog"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a id="op_listar" onclick="actualizar()"><i class="fa fa-refresh"></i> Actualizar</a></li>
                                        </ul>
                                    </li>                     
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div id="filtroguias" class="x_content">
                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label>SERVICIO</label>
                                        <select id="selservicio" name="selservicio" class="selectpicker form-control"></select>
                                    </div>
                                    <div class="col-md-2">
                                        <label>MES</label>
                                        <select id="mes" name="mes" class="selectpicker form-control">
                                            <option value="0">TODOS</option>
                                            <option value="1">ENERO</option>
                                            <option value="2">FEBRERO</option>
                                            <option value="3">MARZO</option>
                                            <option value="4">ABRIL</option>
                                            <option value="5">MAYO</option>
                                            <option value="6">JUNIO</option>
                                            <option value="7">JULIO</option>
                                            <option value="8">AGOSTO</option>
                                            <option value="9">SEPTIEMBRE</option>
                                            <option value="10">OCTUBRE</option>
                                            <option value="11">NOVIEMBRE</option>
                                            <option value="12">DICIEMBRE</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <label>AÑO</label>
                                        <select id="ano" name="ano" class="selectpicker form-control"></select>
                                    </div>
                                    <div class="col-md-3">
                                        <label>C. COSTO</label>
                                        <select id="selcc" name="selcc" class="selectpicker form-control" data-live-search="true"></select>
                                    </div>
                                    <div class="col-md-2">
                                        <br>
                                        <button type="button" class="btn btn-info" onclick="listar();">BUSCAR</button>
                                    </div>
                                </div>
                            </div>
                            <div id="listadoguias" class="x_content table-responsive">
                                <table id="tblguias" class="table table-striped bulk_action" cellspacing="0" width="100%">
                                    <thead>
                                        <tr id="addfactura">
                                            <th colspan="8">
                                                <button type="button" name="addfactura" id="addfactura" class="btn btn-success" data-toggle="modal" data-target="#agregafactura">FACTURA <i class="fa fa-plus-circle"></i></button>
                                            </th>
                                        </tr>
                                        <tr class="headings">
                                            <th>OPCIONES</th>
                                            <th><input type="checkbox" id="check-all" class="flat" onclick="checar();"></th>
                                            <th class="column-title">ID</th>
                                            <th class="column-title">TIPO</th>
                                            <th class="column-title">EQUIPO</th>
                                            <th class="column-title">EDIFICIO</th>
                                            <th class="column-title">MES</th>
                                            <th class="column-title">FECHA</th>
                                            <th class="column-title">RUT</th>
                                            <th class="column-title">RAZON SOCIAL</th> 
                                            <th class="column-title">ARCHIVOS</th>
                                            <th class="column-title">C. COSTO</th>
                                            <th class="bulk-actions" colspan="8">
                                                <a class="antoo" style="color:#000000; font-weight:500;">Seleccionados ( <span class="action-cnt"> </span> )</a>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bs-example-modal-sm" id="agregafactura" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">AGREGA FACTURA</h4>
                    </div>
                    <form id="formulariofactura" name="formulariofactura" class="form form-horizontal">
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                    <label class="control-label"># NUMERO</label>
                                    <input type="text" id="nrofactura" name="nrofactura" class="form-control" required>
                                    <input type="hidden" class="form-control col-md-4" name="idfactura" id="idfactura" value="0">
                                    <input type="hidden" class="form-control col-md-4" name="estado" id="estado" value="1">
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                    <label class="control-label">FECHA</label>
                                    <input type="date" class="form-control col-md-4" name="fecha" id="fecha" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                                    <label class="control-label">MONTO</label>
                                    <input type="text" class="form-control" name="monto" id="monto" required>
                                </div>
                            </div>	
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="cancelar();">CANCELAR</button>
                            <button type="submit" class="btn btn-primary" name="agregaImportacion" id="agregaImportacion">PROCESAR</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /page content -->
         <div class="modal fade bs-example-modal-sm" id="Rechazar" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">ENVIAR GUÍA A REVISIÓN</h4>
                    </div>
                    <form id="formulariorechaza" name="formulariorechaza" class="form form-horizontal">
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback well" id="infopro">

                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                                    <label class="control-label">MOTIVO DEVOLUCIÓN</label>
                                    <textarea name="infodev" id="infodev" class="form-control" required="" style="text-transform:uppercase;"></textarea>
                                    <input type="hidden" class="form-control col-md-4" name="idservicio" id="idservicio" value="0">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="limpiardev();">CANCELAR</button>
                            <button type="submit" class="btn btn-primary" name="rech" id="rech">DEVOLVER</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="comentario" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">COMENTARIO PRODUCCIÓN</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback well" id="infopro2"></div> 
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="btn_rechazo" data-toggle="modal" data-target="#Rechazar"></div>
        <hidden id="comentario-modal" name="comentario-modal" data-toggle="modal" data-target="#comentario"></hidden>
        <!-- /page content -->
        <?php
    } else {
        require 'nopermiso.php';
    }
    require 'footer.php';
    ?>
    <script src="../public/build/js/libs/png_support/png.js"></script>
    <script src="../public/build/js/libs/png_support/zlib.js"></script>
    <script src="../public/build/js/jspdf.debug.js"></script>
    <script src="../public/build/js/jspdf.plugin.autotable.js"></script>
    <script src="../public/build/js/jsPDFcenter.js"></script>
    <script type="text/javascript" src="scripts/facturagse.js"></script>
    <?php
}
ob_end_flush();
?>
