<?php
if (strlen(session_id()) < 1) {
    session_start();
}
?>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>INSTALACIONES</title>

        <link rel="icon" href="../public/favicon.ico" type="image/x-icon" />

        <!-- Bootstrap -->
        <link href="../public/build/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="../public/build/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="../public/build/css/nprogress.css" rel="stylesheet">
        <!-- iCheck -->
        <link href="../public/build/css/green.css" rel="stylesheet">

        <!-- Datatables -->
        <link href="../public/build/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="../public/build/css/buttons.bootstrap.min.css" rel="stylesheet">
        <link href="../public/build/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
        <link href="../public/build/css/responsive.bootstrap.min.css" rel="stylesheet">
        <link href="../public/build/css/scroller.bootstrap.min.css" rel="stylesheet">

        <!-- bootstrap-progressbar -->
        <link href="../public/build/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="../public/build/css/custom.css" rel="stylesheet">
        <style>
            .loader {
                position: fixed;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url('../public/build/images/carga.gif') 50% 50% no-repeat rgb(249,249,249);
                opacity: .9;
            }
        </style>
    </head>

    <body class="login">
        <div class="container body">
            <div class="main_container">
                <!-- Contenido -->
                <div class="right_col" role="main">
                    <div class="">

                        <div class="clearfix"></div>
                        <div class="loader"></div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title text-center">
                                        <h1><b id="nombProy"></b>  <span id="importacion"></span> <br><p class="green" id="estadoproy"></p></h1>
                                    </div>
                                    <div class="x_content">
                                        <div class="col-md-2 col-sm-2 col-xs-12 profile_left">
                                            <div class="green">
                                                <div class="col-md-12">
                                                    <h3>SUPERVISOR</h3>
                                                </div>
                                            </div>
                                            <div class="profile_img">
                                                <div id="imgsup"></div>
                                            </div>
                                            <h3 id="nombsupervisor"></h3>
                                            <hr>
                                            <div class=" green">
                                                <div class="col-md-12">
                                                    <h3>PROJECT MANAGER</h3>
                                                </div>
                                            </div>
                                            <div class="profile_img">
                                                <div id="imgproj"></div>
                                            </div>
                                            <h3 id="nombproject"></h3>
                                        </div>
                                        <div id='revisar' class="col-md-10 col-sm-10 col-xs-12 profile_left">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="row top_tiles text-center">
                                                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                        <div class="tile-stats">
                                                            <div class="count" id="fecha"></div>
                                                            <h3>Fecha inicio</h3>
                                                        </div>
                                                    </div>
                                                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                        <div class="tile-stats">
                                                            <div class="count" id="centrocosto"></div>
                                                            <h3>Centro de Costo</h3>
                                                        </div>
                                                    </div>
                                                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                        <div class="tile-stats">
                                                            <div class="count" id="fechaactualiza"></div>
                                                            <h3>Fecha cambio de estado</h3>
                                                        </div>
                                                    </div>
                                                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                        <div class="tile-stats">
                                                            <div class="count" id="dias"></div>
                                                            <h3>Días desde último cambio de estado</h3>
                                                        </div>
                                                    </div>
                                                </div>

                                                <br />
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="">
                                                    <div class="x_title text-center">
                                                        <h3>ASCENSORES</h3>
                                                    </div>
                                                    <div class="x_content">
                                                        <table class="table table-striped projects" id="ascensores">
                                                            <thead>
                                                                <tr>
                                                                    <th style="width: 40%">CODIGO FM</th>
                                                                    <th style="width: 20%">PROGRESO</th>
                                                                    <th style="width: 40%">ESTADO</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="">
                                                    <div class="x_title text-center">
                                                        <h3>ÚLTIMAS 5 VISITAS</h3>
                                                    </div>
                                                    <ul class="messages" id="visitas">
                                                       
                                                    </ul>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <!-- jQuery -->
        <script src="../public/build/js/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="../public//build/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="../public/build/js/fastclick.js"></script>
        <!-- NProgress -->
        <script src="../public/build/js/nprogress.js"></script>

        <!-- Datatables -->
        <script src="../public/build/js/jquery.dataTables.min.js"></script>
        <script src="../public/build/js/dataTables.bootstrap.min.js"></script>
        <script src="../public/build/js/dataTables.buttons.min.js"></script>
        <script src="../public/build/js/buttons.bootstrap.min.js"></script>
        <script src="../public/build/js/buttons.flash.min.js"></script>
        <script src="../public/build/js/buttons.html5.min.js"></script>
        <script src="../public/build/js/buttons.print.min.js"></script>
        <script src="../public/build/js/dataTables.fixedHeader.min.js"></script>
        <script src="../public/build/js/dataTables.keyTable.min.js"></script>
        <script src="../public/build/js/dataTables.responsive.min.js"></script>
        <script src="../public/build/js/responsive.bootstrap.js"></script>
        <script src="../public/build/js/dataTables.scroller.min.js"></script>
        <script src="../public/build/js/jszip.min.js"></script>
        <script src="../public/build/js/pdfmake.min.js"></script>
        <script src="../public/build/js/vfs_fonts.js"></script>

        <!-- Bootbox Alert -->
        <script src="../public/build/js/bootbox.min.js"></script>


        <!-- Custom Theme Scripts -->
        <script src="../public/build/js/custom.js"></script>

        <script type="text/javascript" src="scripts/dashinstalacion.js"></script>

    </body>
</html>
