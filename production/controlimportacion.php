<?php ob_start(); session_start();
	if(!isset($_SESSION["nombre"])) {
		header("Location:login.php");
	}else{
		require 'header.php';
		if ($_SESSION['administrador'] == 1 || $_SESSION['Comex'] == 1) {
?>
			<!-- page content -->
			<style>
				/*estilos para que el comentario en el listado se ajuste a la celda*/
				/*se coloca este codigo js en la configuracion del datatable*/
				/*columnDefs: [
			        {
			            render: function (data, type, full, meta) {
			                return "<div class='text-wrap width-98'>" + data + "</div>";
			            },
			            targets: 1 //el indice segun el orden del campo a ajustar
			        }
			     ]*/
				.text-wrap{
				    white-space:normal;
				}
				.width-98{
				    width:98%;
				}

				/*estilo para generar scroll en el modal*/
				.modal-body {
				    max-height: calc(100vh - 150px);
				    overflow-y: auto;
				}
			</style>

			<div class="right_col" role="main">
				<div class="">
					<div class="clearfix"></div>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>PLANILLA IMPORTACIÓN</h2>
									<ul class="nav navbar-right panel_toolbox">
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-cog"></i></a>
											<ul class="dropdown-menu" role="menu">
												<?php //if($_SESSION['administrador'] == 1) { ?>
													<li>
														<a id="op_agregar" onclick="mostarform(true)">Agregar</a>
													</li>
												<?php //} ?>
												<li>
													<a id="op_listar" onclick="mostarform(false)">Listar</a>
												</li>
											</ul>
										</li>
									</ul>
									<div class="clearfix"></div>
								</div>
								<div class="row" id="filtros">
                                <div class="col-md-3 col-sm-12 col-xs-12 form-group" style="float: right;">
                                    <label for="listadoanio">Año</label>
                                    <select class="form-control selectpicker" data-live-search="true" id="listadoanio" name="listadoanio" required="required">
                                        <option value="" disabled selected=""> Seleccione Opción</option>
                                    </select>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12 form-group" style="float: right;">
                                    <label for="listadomes">Mes</label>
                                    <select class="form-control selectpicker" data-live-search="true" id="listadomes" name="listadomes" required="required">
                                        <option value="" disabled selected> Seleccione Opción</option>
                                        <option value="1" > Enero</option>
                                        <option value="2" > Febrero</option>
                                        <option value="3" > Marzo</option>
                                        <option value="4" > Abril</option>
										<option value="5" > Mayo</option>
                                        <option value="6" > Junio</option>
                                        <option value="7" > Julio</option>
                                        <option value="8" > Agosto</option>
                                        <option value="9" > Septiembre</option>
                                        <option value="10" > Octubre</option>
                                        <option value="11" > Noviembre</option>
                                        <option value="12" > Diciembre</option>
                                    </select>
                                </div>
                            </div>
								<div id="listadoimportacion" class="x_content">
									<table id="tblimportacion" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
										<thead>
											<tr>
	                                			<th> opciones</th>
	                                			<th> REF.</th>
	                                			<th> Fecha Pedido</th>
	                                			<th> Grupo KM</th>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
								<div id="formularioimportacion" class="x_content" style="display: none;">
									<br/>
									<div class="col-md-12 center-margin">
										<form class="form-horizontal form-label-left" id="formulario" name="formulario">
											<div class="form-group">
												<label class="control-label col-md-2 col-sm-2 col-xs-12">CODIGO REF.</label>
												<div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
													<input type="text" class="form-control col-md-4" name="ctli_numref" id="ctli_numref" required>
												</div>
												<label class="control-label col-md-2 col-sm-2 col-xs-12">Fecha de Solicitud</label>
												<div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
													<input type="date" class="form-control col-md-4" name="ctli_fechapedido" id="ctli_fechapedido" required="">
												</div>
											</div>
											<hr>
											<div class="col-md-12 col-sm-12 col-xs-10 form-group" id="agregar">
	                                            <div class="x_panel" id="agregado">
	                                                <div>
	                                                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
	                                                        <label for="idtascensor">KM <span class="required">*</span></label>
	                                                        <input type="hidden" id="id" name="id" class="form-control" value="N/A">
	                                                        <input type="text" id="ctli_codkm" name="ctli_codkm" class="form-control">
	                                                    </div>

	                                                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
	                                                        <label for="idtascensor">Descripción <span class="required">*</span></label>
	                                                        <input type="text" id="ctli_descripcion" name="ctli_descripcion" class="form-control">
	                                                    </div>

	                                                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
	                                                        <label for="idtascensor">ST <span class="required">*</span></label>
	                                                        <input type="text" id="ctli_codst" name="ctli_codst" class="form-control">
	                                                    </div>

	                                                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
	                                                        <label for="idtascensor">ST Fecha</label>
	                                                        <input type="date" id="ctli_codst_fecha" name="ctli_codst_fecha" class="form-control">
	                                                    </div>

	                                                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
	                                                        <label for="idtascensor">Cantidad <span class="required">*</span></label>
	                                                        <input type="number" id="ctli_cantidad" name="ctli_cantidad" class="form-control">
	                                                    </div>

	                                                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
	                                                        <label for="idtascensor">N° de Presupuesto <span class="required">*</span></label>
	                                                        <input type="text" id="ctli_numpres" name="ctli_numpres" class="form-control">
	                                                    </div>
	                                                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
	                                                        <label for="modelo">Solicitante <span class="required">*</span></label>
	                                                        <div class="row">
	                                                            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
	                                                                <select class="form-control" id="ctli_solicitante" name="ctli_solicitante" required="">
	                                                                	<option value="" selected disabled="">Seleccione</option>
	                                                                	<option value="Servicio">Servicio</option>
	                                                                	<option value="Instalaciones">Instalaciones</option>
	                                                                	<option value="Gerencia">Gerencia</option>
	                                                                	<option value="Otro">Otro</option>
	                                                                </select>
	                                                            </div>
	                                                        </div>
	                                                    </div>
	                                                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
	                                                        <label for="marca">Observación <span class="required">*</span></label>
	                                                        <div class="row">
	                                                            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
	                                                                <select class="form-control" id="ctli_observacion" name="ctli_observacion" required="">
	                                                                	<option value="" selected disabled="">Seleccione</option>
	                                                                	<option value="Order Bound">Order Bound</option>
	                                                                	<option value="Out of stock">Out of stock</option>
	                                                                	<option value="Not Exist">Not Exist</option>
	                                                                	<option value="Normal">Normal</option>
	                                                                	<option value="Manual">Manual</option>
	                                                                </select>
	                                                            </div>
	                                                        </div>
	                                                    </div>
	                                                    <div class="col-md-3 col-sm-12 col-xs-12 pull-right form-group">
	                                                        <label>&nbsp;</label><br>
	                                                        <?php //if($_SESSION['administrador'] == 1) { ?>
		                                                        <button id="btnGuardar" type="button" class="btn btn-success" onclick="guardar()">Agregar</button>
		                                                        <button id="btnEditar" type="button" class="btn btn-success" onclick="guardareditar()" style="display: none;">Editar</button>
		                                                        <button class="btn btn-primary" type="button" id="btnCancelar" onclick="cancelaredicion()">Cancelar</button>
	                                                    	<?php //} ?>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
										</form>
                                        <div class="x_panel" id="listagregado">
                                        	<table id="tblimportacionporcodigo" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
												<thead>
													<tr>
														<th> </th>
														<th> ST</th>
														<th> ST Fecha</th>
														<th> KM</th>
														<th> Descripción</th>
														<th> ST</th>
														<th> Cantidad</th>
														<th> N° de Presupuesto</th>
														<th> Solicitante</th>
														<th> Observación</th>
														<th> Status</th>
													</tr>
												</thead>
												<tbody></tbody>
											</table>
                                        </div>
										<div class="form-group">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<button class="btn btn-primary" type="button" id="btnCancelar" onclick="cancelarform()">Cancelar</button>
												<?php //if($_SESSION['administrador'] == 1) { ?>
													<button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiar()">Limpiar</button>
												<?php //} ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="modal fade" id="modalFechas" role="dialog">
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<!-- Modal Header -->
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">
									<span aria-hidden="true">&times;</span>
									<span class="sr-only">Close</span>
								</button>
								<h4 class="modal-title" id="myModalLabel">FECHAS DE LA IMPORTACION (<span id="codkm"></span>)</h4>
							</div>
							<form role="form" id="formfechas" name="formfechas">
								<input type="hidden" id="idcom" name="idcom" value="">
								<!-- Modal Body -->
								<div class="modal-body">
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-offset-2 col-sm-4 control-label">Fecha Estimada de Despacho</label>
										<div class="col-sm-3">
											<input type="date" class="form-control" id="ctli_fechaestdesp" name="ctli_fechaestdesp">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-offset-2 col-sm-4 control-label">Fecha de Despacho</label>
										<div class="col-sm-3">
											<input type="date" class="form-control" id="ctli_fechadesp" name="ctli_fechadesp">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-offset-2 col-sm-4 control-label">Fecha Estimada de Llegada</label>
										<div class="col-sm-3">
											<input type="date" class="form-control" id="ctli_fechaestllegada" name="ctli_fechaestllegada">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-offset-2 col-sm-4 control-label">Fecha de Llegada</label>
										<div class="col-sm-3">
											<input type="date" class="form-control" id="ctli_fechallegada" name="ctli_fechallegada">
										</div>
									</div>
									<div class="col-md-12 col-sm-12 col-xs-12 form-group" style="text-align: right;">
										<button type="reset" class="btn btn-default" data-dismiss="modal">Cerrar</button>
										<?php //if($_SESSION['administrador'] == 1) { ?>
											<button type="button" onclick="guardarFechas()" class="btn btn-primary submitBtn" id="btnGuardarFechas">Guardar Información</button>
										<?php //} ?>
									</div>
								</div>
							</form>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>

				<div class="modal fade" id="modalComent" role="dialog">
				    <div class="modal-dialog modal-lg">
				        <div class="modal-content">
				            <!-- Modal Header -->
				            <div class="modal-header">
				                <button type="button" class="close" data-dismiss="modal">
				                    <span aria-hidden="true">&times;</span>
				                    <span class="sr-only">Close</span>
				                </button>
				                <h4 class="modal-title" id="myModalLabel">COMENTARIOS DE LA IMPORTACION (<span id="codkm2"></span>)</h4>
				            </div>

				            <form role="form" id="formcomentario" name="formcomentario">
				            	<input type="hidden" id="idcom2" name="idcom" value="">
				                <!-- Modal Body -->
				                <div class="modal-body">
				                	<?php //if($_SESSION['administrador'] == 1) { ?>
					                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
					                        <label>Comentario <span class="required">*</span>(máx. 250 caractéres)</label>
					                        <textarea type="text" class="form-control" name="comentario" id="comentario" required="Campo requerido" rows="4" maxlength="250"></textarea>
					                    </div>
					                    <div id="contactoedi" name="contactoedi">
					                    </div>
					                    <div class="col-md-12 col-sm-12 col-xs-12 form-group" style="text-align: right;">
						                    <button type="reset" onclick="//limpiaredificio()" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						                    <button type="submit" class="btn btn-primary submitBtn" id="btnGuardarComent">Agregar comentario</button>
					                	</div>
					                <?php //} ?>

				                	<div class="col-md-12 col-sm-12 col-xs-12 form-group">
	        				            <div class="x_panel" id="listacomentarios">
	                                    	<table id="tblcomentarios" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
	        									<thead>
	        										<tr>
	        											<th> Fecha</th>
	        											<th> Usuario</th>
	        											<th> Comentario</th>
	        											<!-- <th> Observación</th> -->
	        										</tr>
	        									</thead>
	        									<tbody></tbody>
	        								</table>
	                                    </div>
				                	</div>

				                </div>
				                </div>
				                <div class="clearfix"></div>
				            </form>
				        </div>
				    </div>
				</div>
			</div>
			<!-- /page content -->
<?php
		}else{
			require 'nopermiso.php';
		}
		require 'footer.php';
?>
	<script type="text/javascript" src="scripts/controlimportacion.js"></script>
<?php }
ob_end_flush();
?>