<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {

    require 'header.php';

    if ($_SESSION['administrador'] == 1) {
        ?>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Productos</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-cog"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a id="op_agregar" onclick="mostrarform(true)">Agregar</a></li>
                                            <li><a id="op_listar" onclick="mostrarform(false); verProducto(false);">Listar</a></li>
                                        </ul>
                                    </li>                     
                                </ul>
                                <div class="clearfix"></div>

                                <div id="filtros" style="" class="dt-buttons btn-group">
                                    <p class="btn">FILTROS</p>
                                    <a class="btn btn-default buttons-copy buttons-html5" id="todos" onclick="filtrar('');">Todos</a>
                                    <select id="categoria22" class="selectpicker"></select>
                                </div>
                            </div>
                            <div id="listadoproductos" class="x_content">

                                <!--<div class="row top_tiles" id="productos">
                                    
                                </div>-->

                                <table id="tblproductos" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Codigo</th>
                                            <th>Nombre</th>
                                            <th>Categoria</th>
                                            <th>Stock total</th>
                                            <th>Vigencia</th>
                                            <th width="5%">acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>

                            <div id="formularioproductos" style="display:none;" class="x_content">
                                <br />
                                <form id="formulario" name="formulario" class="form-horizontal form-label-left input_mask">
                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <label>Codigo interno</label>
                                            <input type="text" class="form-control" style="text-transform:uppercase;" id="codigo" name="codigo" placeholder="Codigo">
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <label>Codigo barras</label>
                                            <input type="text" class="form-control" style="text-transform:uppercase;" id="codigobarras" name="codigobarras" placeholder="Codigo de barras">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <label>Nombre</label>
                                            <input type="hidden" name="idproducto" id="idproducto" value="">
                                            <input type="text" class="form-control" style="text-transform:uppercase;" id="nombre" name="nombre" placeholder="Nombre">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <label>Descripción</label>
                                            <input type="text" class="form-control" style="text-transform:uppercase;" id="descripcion" name="descripcion" placeholder="Descripcion de producto">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <label>Precio referencia</label>
                                            <input type="text" class="form-control has-feedback-left" id="precio" name="precio" placeholder="Precio">
                                        </div>
                                        
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <label>Categoria</label>
                                            <select class="form-control" id="categoria" name="categoria"></select>
                                        </div>

                                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                                            <label>Vigencia</label>
                                            <div class="">
                                                <label>
                                                    <input name="vigencia" id="vigencia" type="checkbox" class="icheckbox_flat-green" value="1"/>
                                                </label>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12 ">
                                            <button class="btn btn-primary" type="button" id="btnCancelar" onclick="cancelarform()">Cancelar</button>
                                            <button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiar()">Limpiar</button>
                                            <button class="btn btn-success" type="submit" id="btnGuardar">Guardar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div id="verProducto" style="display:none;" class="x_content">
                                <div class="x_title col-md-12">
                                    <h2 class="green" id="nombre2"></h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <section class="panel">

                                        <div class="x_title col-md-12">
                                            <p>BODEGA</p>
                                            <h2 class="green" id="bodega2"></h2>
                                            <input type="hidden" id="bodega" name="bodega">
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="panel-body">
                                            <span class="name"> Codigo </span>
                                            <h4 id="codigo2" class="green"></h4>
                                            <p>Categoria</p>
                                            <h4 id="categoria2"></h4>
                                            <p>Precio referencia</p>
                                            <h4 id="precio2"></h4>
                                            <br />
                                            <div class="alert alert-danger alert-dismissible fade in" role="alert" id="alertadiv">
                                                <!--<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>-->
                                                <strong>ALERTA!</strong> Queda poco stock!!
                                            </div>

                                        </div>
                                    </section>
                                </div>
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <ul class="stats-overview">
                                        <li>
                                            <span class="name"> Stock </span>
                                            <h3 class="text-success" id="stock2"></h3>
                                        </li>
                                        <li>
                                            <span class="name"> Stock en bodega</span>
                                            <h3 class="text-success" id="cantidadbodega"></h3>
                                        </li>
                                        <li class="hidden-phone">
                                            <span class="name"> Stock critico </span>
                                            <h3 class="text-success" id="stockcritico2"></h3>
                                        </li>
                                    </ul>
                                    <div class="x_panel">
                                        <div class="x_content">
                                            <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                                <ul id="myTab" class="nav nav-tabs nav-justified" role="tablist">
                                                    <li role="presentation" class="active">
                                                        <a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">
                                                            <i class="glyphicon glyphicon-save"></i> Ingreso
                                                        </a>
                                                    </li>
                                                    <li role="presentation" class="">
                                                        <a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">
                                                            <i class="glyphicon glyphicon-open"></i> Salida
                                                        </a>
                                                    </li>
                                                </ul>

                                                <div id="myTabContent" class="tab-content">
                                                    <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                                                        <div class="x_panel" style=" background-color: #E5FFE3;" >
                                                            <div class="x_content">
                                                                <form name="formIN" id="formIN" class="form-horizontal" role="form">

                                                                    <div class="modal-body">
                                                                        <div class="form-group">
                                                                            <label class="col-sm-2 control-label">Cantidad</label>
                                                                            <div class="col-sm-8">
                                                                                <input type="text" class="form-control" name="cantidadin" id="cantidadin">
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-sm-2 control-label">referencia</label>
                                                                            <div class="col-sm-8">
                                                                                <input type="text" class="form-control" name="referenciain" id="referenciain">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button class="btn btn-success" type="button" id="btnGuardar1" onclick="guardarStock(1);">Guardar</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade" id="tab_content2"  aria-labelledby="profile-tab">
                                                        <div class="x_panel" style=" background-color: #FFE3E3;">
                                                            <div class="x_content">
                                                                <form name="formOUT" id="formOUT" class="form-horizontal" role="form">

                                                                    <div class="modal-body">
                                                                        <div class="form-group">
                                                                            <label class="col-sm-2 control-label">Cantidad</label>
                                                                            <div class="col-sm-8">
                                                                                <input type="text" class="form-control" name="cantidad" id="cantidad">
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-sm-2 control-label">referencia</label>
                                                                            <div class="col-sm-8">
                                                                                <input type="text" class="form-control" name="referencia" id="referencia">
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-sm-2 control-label">Centro costo</label>
                                                                            <div class="col-sm-8">
                                                                                <select class="form-control" name="ccosto" id="ccosto"></select>
                                                                                <!--<input type="text" class="form-control" name="ccosto" id="ccosto">-->
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-sm-2 control-label">Solicitante</label>
                                                                            <div class="col-sm-8">
                                                                                <input type="text" class="form-control" name="solicitante" id="solicitante">
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-sm-2 control-label">Autoriza</label>
                                                                            <div class="col-sm-8">
                                                                                <input type="text" class="form-control" name="Autoriza" id="Autoriza">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button class="btn btn-success" type="button" id="btnGuardar2" onclick="guardarStock(0);">Guardar</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_title">
                                        <h3 style="text-align: center;">Movimiento Stock</h3>
                                    </div>
                                    <table id="tblstock" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>fecha</th>
                                                <th>Nombre</th>
                                                <th>cantidad</th>
                                                <th>Movimiento</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-primary" type="button" id="btnCancelar" onclick="cancelarver()">Volver</button>
                                </div>
                            </div>


                            <div class="modal fade bs-example-modal-sm" id="bodegas" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                                            <h4 class="modal-title" id="myModalLabel">SELECCIONAR BODEGA</h4>
                                        </div>
                                        <form id="formbodega" name="formbodega" class="form form-horizontal">
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                        <label class="control-label">BODEGA</label>
                                                        <select class="form-control" id="bodegasselect" name="bodegasselect" required=""></select>
                                                        <input type="hidden" name="idproductover" id="idproductover">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                <button type="submit" class="btn btn-primary" id="agregacentrocosto">Siguiete</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade bs-example-modal-sm" id="stock" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                                            <h4 class="modal-title" id="myModalLabel">STOCK POR BODEGA</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                <span class="price-tax">Producto</span>
                                                <h2 class="price" id="pdt"></h2>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-right">
                                                <span class="price-tax">stock total</span>
                                                <h1 class="price" id="cant"></h1>
                                            </div>
                                            <div class="clearfix"></div>
                                            <hr>
                                            <div class="row top_tiles" id="tiles">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <hidden id="bodega-modal" name="bodega-modal" data-toggle="modal" data-target="#bodegas"></hidden>
                            <hidden id="stock-modal" name="stock-modal" data-toggle="modal" data-target="#stock"></hidden>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
        <?php
    } else {
        require 'nopermiso.php';
    }
    require 'footer.php';
    ?>
    <script>
        $('#myDatepicker2').datetimepicker({
            format: 'DD/MM/YYYY'
        });
    </script>
    <script type="text/javascript" src="scripts/producto.js"></script>
    <?php
}
ob_end_flush();

