<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {

    require 'header.php';

    if ($_SESSION['administrador'] == 1 || $_SESSION['RPresupuesto'] == 1) {
?>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>PRESUPUESTOS</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-tooltip="tooltip" title="Operaciones" role="button" aria-expanded="false"><i class="fa fa-cog"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a id="op_listar" onclick="Actualizar()"><i class="fa fa-list-alt"></i> Actualizar</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div id="listadopre" class="x_content">

                                <table id="tblpre" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>SOLICITUD</th>
                                            <th>GSE</th>
                                            <th>EDIFICIO</th>
                                            <th>ID CLIENTE</th>
                                            <th>CODIGO FM</th>
                                            <th>ESTADO EQUIPO</th>
                                            <th>ACTUALIZADO</th>
                                            <th>ESTADO</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>


                            <!-- Formulario modificar ascensor -->
                            <div id="mostrarrev" class="x_content">
                                <br />
                                <form id="formulario" name="formulario" class="form-horizontal form-label-left input_mask">

                                    <h4><b>INFORMACION DE LA SOLICITUD</b></h4>
                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="idpresupuesto">NUMERO DE SOLICITUD</label>
                                        <input type="text" class="form-control" readonly="readonly" name="idpresupuesto" id="idpresupuesto">
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="fecha">FECHA Y HORA DE SOLICITUD</label>
                                        <input type="text" class="form-control" disabled="disabled" name="fecha" id="fecha">
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="edificio">EDIFICIO</label>
                                        <input type="text" class="form-control" disabled="disabled" name="edificio" id="edificio">
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="codigo">CODIGO FM</label>
                                        <input type="text" class="form-control" disabled="disabled" name="codigo" id="codigo">
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="equipo">EQUIPO</label>
                                        <input type="text" class="form-control" disabled="disabled" name="equipo" id="equipo">
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="descripcion">OBSERVACION DE LA SOLICITUD</label>
                                        <textarea type="text" id="descripcion" name="descripcion" disabled="disabled" class="resizable_textarea form-control"></textarea>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="descripcion">INFORMACION SOLICITADA</label>
                                        <textarea type="text" id="solinfo" name="solinfo" disabled="disabled" class="resizable_textarea form-control"></textarea>
                                    </div>

                                    <h4><b>INFORMACION DE LA REVISION</b></h4>
                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="descripcion">OBSERVACION DE LA REVISION</label>
                                        <textarea type="text" id="descripcionob" name="descripcionob" class="resizable_textarea form-control"></textarea>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="descripcion">IMAGENES DE REFERENCIA</label>
                                        <input id="foto[]" name="foto[]" type="file" multiple="multiple" accept="image/jpg, image/jpeg, image/png " class="custom-file-input">
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <button class="btn btn-primary" type="button" id="btnCancelar" onclick="cancelarformpro()">Cancelar</button>
                                            <button class="btn btn-success" type="submit" id="btnRevisar">Revisar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>


                            <div id="modalImagenes" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog" style="min-width:60%;">
                                    <form id="Motivoform" class="form-horizontal calender" role="form" enctype="multipart/form-data">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title" id="myModalLabel">IMAGENES ASOCIADAS</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div id="testmodal" style="padding: 5px 10px;">
                                                    <div class="form-group">
                                                        <div id="divimagenes"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default antoclose" data-dismiss="modal">Cerrar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- /page content -->

        <!-- /page content -->
    <?php
    } else {
        require 'nopermiso.php';
    }
    require 'footer.php';
    ?>
    <script src="../public/build/js/libs/png_support/png.js"></script>
    <script src="../public/build/js/libs/png_support/zlib.js"></script>
    <script src="../public/build/js/jspdf.debug.js"></script>
    <script src="../public/build/js/jspdf.plugin.autotable.js"></script>
    <script src="../public/build/js/jsPDFcenter.js"></script>
    <script type="text/javascript" src="scripts/presupuestosup.js"></script>
<?php
}
ob_end_flush();
?>