<?php
ob_start();
session_start();

if(!isset($_SESSION["nombre"])){
  header("Location:login.php");
}else{

require 'header.php';


 ?>
        <!-- Contenido -->
        <div class="right_col" role="main">
        
          <!-- Datos actulidad en fallas -->
          <div class="row tile_count">
            <div class="col-md-3 col-sm-6 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-check"></i> Contratos </span>
              <div class="count"><a href="#" id="num_contrato"></a></div>            
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-exclamation-triangle"></i> Clientes</span>
              <div class="count"><a href="#" id="num_clientes"></a></div>              
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-exclamation-triangle"></i> Edificios</span>
              <div class="count"><a href="#" id="num_edificios"></a></div>  
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-exclamation-triangle"></i> Ascensores</span>
              <div class="count"><a href="#" id="num_ascensores"></a></div>  
            </div>
          </div>

          <br />

		  <!-- Contenido Graficos -->
          <div id="Charts" class="row">         
              <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Contratos en el año</h2>                   
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <canvas id="mybarChart"></canvas>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Contratos en el año anterior</h2>                   
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <canvas id="mybarChartant"></canvas>
                  </div>
                </div>
              </div>           
          </div>
          <!-- /Fin contenido graficos -->
          
        </div>
        <!-- /Fin Contenido -->

<?php 
require 'footer.php';
?>
<script type="text/javascript" src="scripts/estado.js"></script>

<?php
}
ob_end_flush();
?>
