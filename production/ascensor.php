<?php
ob_start();

session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {

    require 'header.php';

    if ($_SESSION['administrador'] == 1 || $_SESSION['Contratos'] == 1) {
        ?>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Ascensores</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-tooltip="tooltip" title="Operaciones" role="button" aria-expanded="false"><i class="fa fa-cog"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a id="op_actualizar"><i class="fa fa-refresh"></i> Actualizar</a>
                                            </li>
                                            <li><a id="op_listar" onclick="mostarform(false)"><i class="fa fa-list-alt"></i> Listar</a>
                                            </li>
                                        </ul>
                                    </li>                     
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div id="listadoascensores" class="x_content">
                                <table id="tblascensores" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>OPCIONES</th>
                                            <th>CODIGO</th>
                                            <th>EDIFICIO</th>
                                            <th>CONTRATO</th>
                                            <th>EN CARTERA</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <div id="formularioascensor" class="x_content">
                                <div class="col-md-12 center-margin">
                                    <form class="form-horizontal form-label-left" id="formulario" name="formulario">
                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label>Tipo de Ascensor <span class="required">*</span></label>
                                            <input type="hidden" id="idascensor" name="idascensor" class="form-control">
                                            <select class="form-control selectpicker" data-live-search="true" id="idtascensor" name="idtascensor" required="Campo requerido">
                                            </select>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label>Marca <span class="required">*</span></label>
                                            <select class="form-control selectpicker" data-live-search="true" id="marca" name="marca" required="Campo requerido">
                                            </select>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label>Modelo<span class="required">*</span></label>
                                            <select class="form-control selectpicker" data-live-search="true" id="modelo" name="modelo" required="Campo requerido">
                                            </select>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label>Identidicador fabricante</label>
                                            <input type="text" class="form-control" name="ken" id="ken">
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label for="pservicio">Puesta servicio</label>
                                            <input type="date" id="pservicio" name="pservicio" class="form-control"/>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label for="gtecnica">Garantia tecnica</label>
                                            <input type="date" id="gtecnica" name="gtecnica" class="form-control"/>
                                        </div>
                                        <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                                            <label>Valor UF <span class="required">*</span></label>
                                            <input type="text" class="form-control" name="valoruf" id="valoruf" required="Campo requerido">
                                        </div>
                                        <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                                            <label>Valor CLP </label>
                                            <input type="text" class="form-control" name="valorclp" id="valorclp">
                                        </div>
                                        <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                                            <label>Paradas </label>
                                            <input type="text" class="form-control" name="paradas" id="paradas">
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label>Capacidad (Kg) </label>
                                            <input type="text" class="form-control" name="capkg" id="capkg">
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label>Capacidad personas </label>
                                            <input type="text" class="form-control" name="capper" id="capper">
                                        </div>
                                        <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                                            <label>Velocidad </label>
                                            <input type="text" class="form-control" name="velocidad" id="velocidad">
                                        </div>
                                        <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                                            <label for="dcs">Dcs <span class="required">*</span></label>
                                            <select class="form-control selectpicker" data-live-search="true" id="dcs" name="dcs" required="required">
                                                <option value="" selected disabled>Dcs</option>
                                                <option value="1">SI</option>
                                                <option value="0">NO</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                                            <label for="elink">Elink <span class="required">*</span></label>'+
                                            <select class="form-control selectpicker" data-live-search="true" id="elink" name="elink" required="required">
                                                <option value="" selected disabled>Elink</option>
                                                <option value="1">SI</option>
                                                <option value="0">NO</option>                                              
                                            </select>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="ln_solid"></div> 

                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <button class="btn btn-primary" type="button" id="btnCancelar" onclick="cancelarform()">Cancelar</button>
                                                <button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiar()">Limpiar</button>
                                                <button class="btn btn-success" type="submit" id="btnGuardar">Agregar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div id="tabascensor" class="x_content">   
                                <div class="col-md-12 col-sm-12 col-xs-12" style="border:0px solid #e5e5e5;">
                                    <h1 class="prod_title" id="tabcodigoti" name="tabcodigoti"></h1>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <ul class="">
                                            <li>
                                                <h5><b>Contrato: </b></h5><p id="tabcontrato" name="tabcontrato"></p>
                                            </li> 
                                            <li>
                                                <h5><b>Edificio: </b></h5><p id="tabedificio" name="tabedificio"></p>
                                            </li> 
                                            <li>
                                                <h5><b>Cliente: </b></h5><p id="tabcliente" name="tabcliente"></p>
                                            </li> 
                                            <li>
                                                <h5><b>Codigo: </b></h5><p id="tabcodigo" name="tabcodigo"></p>
                                            </li>                                            
                                            <li>
                                                <h5><b>Tipo: </b></h5><p id="tabtipo" name="tabtipo"></p>
                                            </li>
                                            <li>
                                                <h5><b>Marca: </b></h5><p id="tabmarca" name="tabmarca"></p>
                                            </li>
                                            <li>
                                                <h5><b>Modelo: </b></h5><p id="tabmodelo" name="tabmodelo"></p>
                                            </li> 
                                            <li>
                                                <h5><b>Identificador fabricante: </b></h5><p id="tabken" name="tabken"></p>
                                            </li> 
                                            <li>
                                                <h5><b>Puesta en servicio: </b></h5><p id="tabpservicio" name="tabpservicio"></p>
                                            </li>
                                            <li>
                                                <h5><b>Garantia tecnica: </b></h5><p id="tabgtecnica" name="tabgtecnica"></p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <ul class="">
                                            <li>
                                                <h5><b>Valor (UF): </b></h5><p id="tabvaloruf" name="tabvaloruf"></p>
                                            </li>
                                            <li>
                                                <h5><b>Valor (CLP): </b></h5><p id="tabvalorclp" name="tabvalorclp"></p>
                                            </li>
                                            <li>
                                                <h5><b>Capacidad (Kg): </b></h5><p id="tabcapkg" name="tabcapkg"></p>
                                            </li>
                                            <li>
                                                <h5><b>Capacidad (Personas): </b></h5><p id="tabcapper" name="tabcapper"></p>
                                            </li>
                                            <li>
                                                <h5><b>Velocidad: </b></h5><p id="tabvelocidad" name="tabvelocidad"></p>
                                            </li>                                            
                                            <li>
                                                <h5><b>DCS: </b></h5><p id="tabdcs" name="tabdcs"></p>
                                            </li>
                                            <li>
                                                <h5><b>ELink: </b></h5><p id="tabelink" name="tabelink"></p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- /page content -->
        <?php
    } else {
        require 'nopermiso.php';
    }
    require 'footer.php';
    ?>
    <script type="text/javascript" src="scripts/ascensor.js"></script>
    <?php
}
ob_end_flush();
?>