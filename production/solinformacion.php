<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {

    require 'header.php';

    if ($_SESSION['administrador'] == 1 ) {
        ?>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>SOLICITUDES DE INFORMACIÓN WEB</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-tooltip="tooltip" title="Operaciones" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-cog"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a id="op_actualizar" onclick="actualizar()">ACTUALIZAR</a>
                                            </li>
                                        </ul>
                                    </li>                     
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div id="listadoinformacion" class="x_content">

                                <table id="tblinformacion" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>id</th>
                                            <th>NOMBRE</th>
                                            <th>TELEFONO</th>                                       
                                            <th>EMAIL</th>
                                            <th>AREA</th>
                                            <!--<TH>MENSAJE</TH>-->
                                            <th>PROCESADO</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            
                            <div id="formularioinformacion" class="x_content" style="display:none;">
                                <br/>
                                <div class="col-md-12 center-margin">
                                    <form class="form-horizontal form-label-left" id="formulario" name="formulario">
                                        <section class="panel">
                                            <!--<div class="x_title">
                                                <h2 class="green">VISITA N° <b id="numerovisita"></b></h2>
                                                <div class="clearfix"></div>
                                            </div>-->
                                            <div class="panel-body">
                                                <div class="project_detail">
                                                    <p class="title"><h3>Solicitud de Información <span class="value text-bold" id="idinformacion"></span> </h3></p>
                                                    
                                                    
                                                    <div class="clearfix"></div>
                                                    
                                                    <p class="title">NUMERO DE SOLICITUD: <span class="value" id="idinfo"></span></p>
                                                    <p class="title">FECHA Y HORA: <span class="value text-primary text-bold" id="fecha"></span></p>
                                                    <p class="title">CONTACTO: <span class="value text-primary text-bold" id="contacto"></span></p>
                                                    <p class="title">RUT: <span class="value text-primary text-bold" id="rut"></span></p>
                                                    <p class="title">TELEFONO: <span class="value text-primary text-bold" id="telefono"></span></p>
                                                    <p class="title">EMAIL: <span class="value text-primary text-bold" id="email"></span></p>
                                                    <p class="title">AREA: <span class="value text-primary text-bold" id="area2"></span></p>
                                                    
                                                    <hr>
                                                    <p class="title">MENSAJE</p>
                                                    <p class="value text-primary text-bold" id="mensaje"></p>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <input type="hidden" id="idsolinformacion" name="idsolinformacion" class="form-control" required="Campo requerido">
                                                    <input type="hidden" id="area" name="area" class="form-control" required="Campo requerido">
                                                    <input type="hidden" id="procesado" name="procesado" class="form-control" required="Campo requerido">
                                                    <input type="hidden" id="condicion" name="condicion" class="form-control" required="Campo requerido">
                                                    
                                                </div>

                                                <div class="ln_solid"></div> 
                                                <div class="form-group">
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <button class="btn btn-success" type="submit" id="btnGuardar"><i class="fa fa-send"></i> Enviar email</button>
                                                        <button class="btn btn-primary" style="float: right;" type="button" id="btnCancelar" onclick="cancelarform()">Cancelar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <?php
    } else {
        require 'nopermiso.php';
    }
    require 'footer.php';
    ?>
    <script src="../public/build/js/jspdf.min.js"></script>
    <script src="../public/build/js/jspdf.plugin.autotable.js"></script>
    <script src="../public/build/js/jsPDFcenter.js"></script>


    <script type="text/javascript" src="scripts/solinformacion.js"></script>
    <?php
}
ob_end_flush();
?>