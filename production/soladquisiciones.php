<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {
    require 'header.php';
    ?>

    <div class="right_col" role="main">
        <div class="">
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>SOLICITUD DE ADQUISICIONES</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-tooltip="tooltip" title="Operaciones" role="button" aria-expanded="false"><i class="fa fa-cog"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a id="op_agregar" onclick="mostrarform(true)"><i class="fa fa-list-alt"></i> Agregar</a></li>
                                        <li><a id="op_listar" onclick="mostrarform(false); mostrarvista(false); cancelarform();"><i class="fa fa-list-alt"></i> Listar</a></li>
                                    </ul>
                                </li>                     
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div id="listadsolicitudes" class="x_content">
                            <table id="tblguias" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>C. COSTO</th>
                                        <th>NRO.</th>
                                        <th>FECHA</th>
                                        <th>PROVEEDOR</th>
                                        <th>ESTADO</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>

                        <div id="formsolicitudes" class="x_content" style="display: none ;">
                            <div class="col-md-12 center-margin">
                                <form class="form-horizontal form-label-left" id="formulario" name="formulario" >
                                    <div class="x_panel">
                                        <div class="form-group x_panel">
                                            <div class="col-md-3 col-sm-3 col-xs-12 form-group">
                                                <label>NRO SOLICITUD</label>
                                                <input type="text" class="form-control" id="idsolicitud" name="idsolicitud" readonly="" value="0">
                                                <input type="hidden" class="form-control" id="estado" name="estado" value="0">
                                                <p>* Este numero sera entregado una vez se guarde la solicitud.</p>
                                            </div>
                                            <div class="col-md-3 col-sm-3 col-xs-12">
                                                <label class="control-label">FECHA ENTREGA</label>
                                                <input type="date" class="form-control" id="fecha" name="fecha">
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <label class="control-label">CENTRO DE COSTO<span class="required">*</span></label>
                                                <select class="selectpicker select2_single form-control" id="ccosto" name="ccosto" data-live-search="true" required="required"></select>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 form-group">           
                                                <label>OBSERVACIÓN</label>
                                                <textarea class="form-control" id="observacion" name="observacion"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group x_panel">
                                            <div class="x_title">Adjuntar cotización</div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">COTIZACIÓN<span class="required">*</span></label>
                                                <input type="file" class="file-input" id="archivo" name="archivo">
                                                <input type="hidden" class="form-control" id="anterior" name="anterior">
                                                <input type="hidden" class="form-control" id="idcotizacion" name="idcotizacion">
                                            </div>

                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label class="control-label">MONEDA</label>
                                                <select class="form-control selectpicker" id="moneda" name="moneda" data-live-search="true" required=""></select>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12 form-group">
                                                <div class="col-md-11 col-sm-11 col-xs-12 form-group">
                                                    <label class="control-label">CONDICION DE PAGO</label>
                                                    <select class="selectpicker form-control" id="condpago" name="condpago" data-live-search="true" required="required"></select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <label class="control-label">PROVEEDOR<span class="required">*</span></label>
                                                <select class="selectpicker select2_single form-control" id="proveedor" name="proveedor" data-live-search="true" required="required"></select>
                                            </div>


                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="">
                                                    <ul id="archivos">

                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <div class="col-md-12 col-sm-12 col-xs-12 ">
                                                    <button class="btn btn-info" type="submit" id="btnGuardar2">Guardar borrador</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2>DETALLE</h2>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">
                                            <div class="form-group">
                                                <fieldset class="well">
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="col-md-4">
                                                            <label class="control-label">CATEGORIA PRODUCTO</label>
                                                            <select class="form-control selectpicker" name="det_categoria" id="det_categoria"></select>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <label class="control-label">PRODUCTO</label>
                                                            <select class="form-control selectpicker" name="det_producto" id="det_producto"></select>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <label class="control-label">CANTIDAD</label>
                                                            <input type="text" name="det_cantidad" id="det_cantidad" class="form-control" style='text-transform:uppercase;'>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <label class="control-label">VALOR</label>
                                                            <input type="text" name="det_valor" id="det_valor" class="form-control" style='text-transform:uppercase;'>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label class="control-label">OBSERVACION</label>
                                                            <input type="text" name="det_observacion" id="det_observacion" class="form-control" style='text-transform:uppercase;'>
                                                        </div>
                                                        <div class="col-md-2 right">
                                                            <br>    
                                                            <button name="addgarantia" id="addgarantia" style="float: right;" type="button" onclick="addDetalle()" class="btn btn-success">Agregar</button>
                                                        </div>
                                                    </div>

                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="x_content">
                                            <table id="detalle" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>PRODUCTO</th>
                                                        <th>CANTIDAD</th>
                                                        <th>VALOR</th>
                                                        <th>TOTAL</th>
                                                        <th>OBSERVACIÓN</th>
                                                        <th>#</th>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                                
                                            </table>

                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <div class="col-md-12 col-sm-12 col-xs-12 ">
                                            <button class="btn btn-primary" type="button" id="btnCancelar" onclick="cancelarform()">Cancelar</button>
                                            <button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiar()">Limpiar</button>
                                            <button class="btn btn-info" type="button" id="btnGuardar" onclick="terminarform()">Guardar</button>

                                            <!--<button class="btn btn-round btn-success" type="button" id="">ENVIAR A REVISIÓN</button>-->
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div id="versolicitudes" class="x_content" style="display: none ;">
                            <div class="x_content">
                                <div class="col-xs-2">
                                    <ul class="nav nav-tabs tabs-left">
                                        <li class="active"><a href="#home" data-toggle="tab">Revisión Solicitud</a></li>
                                        <li><a href="#profile" data-toggle="tab">Observaciones</a></li>
                                        <!--<li><a href="#messages" data-toggle="tab">Productos</a></li>-->
                                        <li><a href="#settings" data-toggle="tab">Archivos Adjuntos</a></li>
                                        <li>
                                            <p>
                                            <ul class="to_do">
                                                <li class="check-mark" id="nroarchivos">

                                                </li>
                                                <!--<li>
                                                    <div class="icheckbox_flat-green" style="position: relative;">
                                                        <input type="checkbox" class="flat" style="position: absolute; opacity: 0;" checked="true">
                                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                                    </div> Articulos valorizados 
                                                </li>-->
                                            </ul>

                                            </p>
                                        </li>
                                    </ul>
                                </div>

                                <div class="col-xs-10">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="home">
                                            <ul class="stats-overview">
                                                <li>
                                                    <span class="name"> N° Solicitud </span>
                                                    <span class="value text-success" id="ver_numero"> </span>
                                                </li>
                                                <li>
                                                    <span class="name"> Total </span>
                                                    <span class="value text-success" id="ver_total"> </span>
                                                </li>
                                                <li class="hidden-phone">
                                                    <span class="name"> Prioridad </span>
                                                    <span class="value text-success"><span class="label label-success">Normal</span></span>
                                                </li>
                                            </ul>
                                            <div class="x_panel">
                                                <div class="col-sm-6 invoice-col">
                                                    <ul class="bs-glyphicons-list">
                                                        <li>
                                                            <h5><b>Solicitante: </b></h5><p id="ver_solicita" name="tabcontrato">Aaron Zuñiga</p>
                                                        </li> 
                                                        <li>
                                                            <h5><b>Centro de costo: </b></h5><p id="ver_ccosto" name="tabedificio">KM81881 - CENTRO COSTO</p>
                                                        </li> 
                                                        <li>
                                                            <h5><b>Condicion de pago: </b></h5><p id="ver_condpago" name="tabtipo">30 días</p>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-sm-6 invoice-col">
                                                    <ul class="bs-glyphicons-list">
                                                        <li>
                                                            <h5><b>Fecha Solicitud: </b></h5><p id="ver_fecha" name="tabcodigo">17/07/2019</p>
                                                        </li>
                                                        <li>
                                                            <h5><b>Proveedor: </b></h5><p id="ver_proveedor" name="tabcliente">PC FACTORY</p>
                                                        </li>
                                                        <li>
                                                            <h5><b>Moneda: </b></h5><p id="ver_moneda" name="tabmarca">Euro</p>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="x_panel">
                                                <div class="x_title">DETALLE</div>
                                                <table id="ver_detalle" class="table table-bordered" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>PRODUCTO</th>
                                                            <th>CANTIDAD</th>
                                                            <th>VALOR</th>
                                                            <th>TOTAL</th>
                                                            <th>OBSERVACIÓN</th>
                                                            <!--<th>#</th>-->
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td colspan="4" class="text-right"><b>SUBTOTAL</b></td>
                                                            <td id="totdet"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4" class="text-right"><b>NETO</b></td>
                                                            <td id="neto"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4" class="text-right"><b>IVA 19%</b></td>
                                                            <td id="iva"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4" class="text-right"><b>TOTAL</b></td>
                                                            <td id="total"></td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                            <!--<div class="x_panel">
                                                <div class="x_title">FORMULARIO REVISIÓN</div>
                                                <div class="x_content">
                                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                                        <label class="control-label">CONDICION DE PAGO</label>
                                                        <select class="selectpicker form-control" id="idcondicionpago" name="idcondicionpago" data-live-search="true" required="required"></select>
                                                    </div>
                                                    
                                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                                        <label class="control-label">MONEDA</label>
                                                        <select class="form-control selectpicker" id="moneda" name="moneda" data-live-search="true" required=""></select>
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                        <label>Observación</label>
                                                        <textarea id="observacion" class="form-control"></textarea>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <div class="col-md-12 col-sm-12 col-xs-12 ">
                                                            <button class="btn btn-primary" type="button" id="btnCancelar">Cancelar</button>
                                                            <button class="btn btn-danger" type="button" id="btnLimpiar">Rechazar</button>
                                                            <button class="btn btn-success" type="button" id="btnGuardar">Aprobar</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>-->

                                        </div>
                                        <div class="tab-pane" id="profile">
                                            <ul class="messages" id="ver_observaciones">
                                            </ul>
                                        </div>
                                        <!--<div class="tab-pane" id="messages">
                                            <div class="x_panel">
                                                <div class="x_title">DETALLE</div>
                                                <table id="detalle" class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>PRODUCTO</th>
                                                            <th>CANTIDAD</th>
                                                            <th>VALOR</th>
                                                            <th>TOTAL</th>
                                                            <th>OBSERVACIÓN</th>
                                                            <th>#</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>COMPUTADOR</td>
                                                            <td>1</td>
                                                            <td>250000</td>
                                                            <td>250000</td>
                                                            <td>All In One, Lenovo 19,5", 8gb, 1Tb</td>
                                                            <td><button type="button" class="btn btn-xs" ><i class="fa fa-trash"></i></button></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>-->
                                        <div class="tab-pane" id="settings">
                                            <div class="x_panel">
                                                <div class="x_title">ARCHIVOS ADJUTNOS</div>
                                                <div class="attachment">
                                                    <ul id="ver_archivos">
                                                    </ul>
                                                </div>
                                            </div>
                                            <!--<div class="x_panel">
                                                <div class="x_title">ADJUNTAR NUEVO ARCHIVO</div>
                                                <div class="form-group">
                                                    <label>Adjuntar nuevo archivo</label>
                                                    <input type="file" name="archivo" id="archivo">
                                                </div>
                                            </div>-->
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="modal-footer">
                                    <div class="col-md-12 col-sm-12 col-xs-12 ">
                                        <button class="btn btn-primary" type="button" id="btnCancelar" onclick="mostrarform(false); mostrarvista(false);">Volver</button>

                                        <!--<button class="btn btn-round btn-success" type="button" id="">ENVIAR A REVISIÓN</button>-->
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php require 'footer.php'; ?>

    <script type="text/javascript" src="scripts/soladquisiciones.js"></script>

    <?php
}
ob_end_flush();
