var tabla;


//funcion que se ejecuta iniciando
function init(){
	ListarMovimientos();
}

function ListarMovimientos(){
		console.log("Carga Listado");
		GenerarListado();
		$("#Listado").show();
}



function GenerarListado(){
	console.log("Listado Movimientos");
	tabla=$('#tblmovimientos').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons:[
			'copyHtml5',
			'print',
			'excelHtml5',
			'csvHtml5',
			'pdf'
		],
		"ajax":{
			url:'../ajax/movimiento.php?op=listar',
			type:"get",
			dataType:"json",
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 15, //Paginacion 10 items
		"order" : [[1 , "asc"]] //Ordenar en base a la columna 0 descendente
	}).DataTable();
	
}



init();