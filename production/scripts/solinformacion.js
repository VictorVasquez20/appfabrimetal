/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var tabla;

//funcion que se ejecuta iniciando
function init(){
    mostrarform(false);
    listar();
     
    $("#formulario").on("submit", function(e){
        e.preventDefault();
        guardaryeditar(); 
    });
        
}

function mostrarform(flag){
    if(!flag){
        $("#listadoinformacion").show();
        $("#op_actualizar").show();
        $("#formularioinformacion").hide();
    }else{
        $("#listadoinformacion").hide();
        $("#op_actualizar").hide();
        $("#formularioinformacion").show();
    }
}

function listar(){
    tabla=$('#tblinformacion').dataTable({
            "aProcessing":true,
            "aServerSide": true,
            dom: 'Bfrtip',
            buttons:[
                    'copyHtml5',
                    'print',
                    'excelHtml5',
                    'csvHtml5',
                    'pdf'
            ],
            "ajax":{
                    url:'../ajax/solinformacion.php?op=listar',
                    type:"get",
                    dataType:"json",
                    error: function(e){
                            console.log(e.responseText);
                    }
            },
            "bDestroy": true,
            "iDisplayLength": 10,
            "order" : [[1 , "desc"]] 
    }).DataTable();
}

function actualizar() {
    tabla.ajax.reload();
}

function mostrar(idsolinformacion){
    $.post("../ajax/solinformacion.php?op=mostrar", {idsolinformacion: idsolinformacion}, function(data){
        data = JSON.parse(data);
        mostrarform(true);
        
        $("#idinformacion").html(data.idsolinformacion);
        $("#idinfo").html(data.idsolinformacion);
        $("#fecha").html(data.created_time);
        $("#contacto").html(data.nombre);
        $("#rut").html(data.rut);
        $("#telefono").html(data.telefono);
        $("#email").html(data.email);
        $("#area2").html(data.nombArea);
        $("#mensaje").html(data.mensaje);
        
        $("#area").val(data.area);
        $("#idsolinformacion").val(data.idsolinformacion);
        $("#procesado").val(data.procesado);
        $("#condicion").val(data.condicion);
        
        
    })
}

function guardaryeditar(){
    bootbox.confirm("Va a enviar este correo, ¿esta seguro?", function(result)
    {   
        if(result){
            $("#btnGuardar").prop("disabled", true);
            var formData = new FormData($("#formulario")[0]);
            $.ajax({
                    url:'../ajax/solinformacion.php?op=editar',
                    type:"POST",
                    data:formData,
                    contentType: false,
                    processData:false,

                    success: function(datos){
                            bootbox.alert(datos);
                            mostrarform(false);
                            tabla.ajax.reload();
                    }
            });
            $("#btnGuardar").prop("disabled", false);
        }
    });
    
}

function cancelarform(){
    mostrarform(false);
}

init();

