var arrmeses = ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"];
var arrmescompleto = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
var lineChart, mybarChart , mybarChart2;

function init(){
    grafico();
    /** PARA EL FILTRO DE FECHAS**/
    fe = new Date();
    fecha = (fe.getFullYear())+'-'+(fe.getMonth()+1)+'-'+(fe.getDay());
    cargadatos(fecha);
    cargaguias();
    
    $(document).ready(function(){
         new PNotify({
            title: 'INFORMACIÓN!',
            text: 'Proyecto iniciado a partir de mayo de 2019.',
            type: 'info',
            styling: 'bootstrap3'
        });
    });    
    
    
}

function actualiza(){
    
}

function cargadatos(fecha) {
    $.post("../ajax/dashcontabilidad.php?op=indicadoresfiltro&fecha="+fecha, function (data, status) {
        data = JSON.parse(data);
        //Actualizamos valores
        
        var sumtotal = 0, summant = 0, sumrep = 0,sumemerge = 0;
        var sumtotalfac = 0, summantfac = 0, sumrepfac = 0,sumemergefac = 0;
        
        var rep = 0, repm = 0, ing = 0, norm = 0;
        
        for (i = 0; i < data.length; i++) {
            
            if(data[i]["1"] == 1 || data[i]["1"] == 4 | data[i]["1"] == 5 | data[i]["1"] == 6){
                //reparacion
                sumrep = parseFloat(sumrep) + parseFloat(data[i]["2"]);
                
                if(data[i]["1"] == 1){
                    rep = parseFloat(data[i]["2"]);
                }else if (data[i]["1"] == 4){
                    repm = parseFloat(data[i]["2"]);
                }else if (data[i]["1"] == 5){
                    ing = parseFloat(data[i]["2"]);
                }else if(data[i]["1"] == 6){
                    norm = parseFloat(data[i]["2"]);
                }
                
            }else if(data[i]["1"] == 2){
                //emergencia
                sumemerge = parseFloat(sumemerge) + parseFloat(data[i]["2"]);
                
            }else if(data[i]["1"] == 3){
                //mantencion
                summant = parseFloat(summant) + parseFloat(data[i]["2"]);
                summantfac = parseFloat(summantfac) + parseFloat(data[i]["3"]);
            }
            
            sumtotal = parseFloat(sumtotal) + parseFloat(data[i]["2"]);
            sumtotalfac = parseFloat(sumtotalfac) + parseFloat(data[i]["3"]);
            
        }
        //TOTALES
        $("#gsetotal").empty();
        $("#gsetotal").html(sumtotal);
        
        porcm = parseFloat((parseFloat(summant) * 100) / parseFloat(sumtotal)).toFixed(1);
        $("#totmant").empty();
        $("#totmant").html(summant + '<span> ('+ porcm +'%)</span>');
        
        porcr = parseFloat((parseFloat(sumrep) * 100) / parseFloat(sumtotal)).toFixed(1);
        $("#totrep").empty();
        $("#totrep").html(sumrep + '<span> ('+ porcr +'%)</span>');
        
        porce =parseFloat((parseFloat(sumemerge) * 100) / parseFloat(sumtotal)).toFixed(1);
        $("#toteme").empty();
        $("#toteme").html(sumemerge + '<span> ('+ porce +'%)</span>');
        
        
        
        //MANTENCION
        $("#totmant2").empty();
        $("#totmant2").html(summant);
        
        $("#totmant3").empty();
        $("#totmant3").html(summant);
        
        
        
        
        $("#mantfact").empty();
        $("#mantfact").html(summantfac);
        
        $("#mantporc").empty();
        if(summant != 0){
            $("#mantporc").html(parseFloat((summantfac / summant) * 100).toFixed(2) + "%");
        }else{
            $("#mantporc").html("0%");
        }
        
        
        //REPARACION
        $("#totrep2").empty();
        $("#totrep2").html(sumrep);
        
        $("#totrep3").empty();
        $("#totrep3").html(sumrep);
        
        /* reparacion */
        $("#trep").empty();
        $("#trep").html(rep);
        
        /* reparacion mayor */
        $("#trepm").empty();
        $("#trepm").html(repm);
        
        /* normalizacion */
        $("#trepn").empty();
        $("#trepn").html(norm);
        
        /* ingenieria */
        $("#trepi").empty();
        $("#trepi").html(ing);
        
        
        
        
        //EMERGENCIA
        $("#toteme2").empty();
        $("#toteme2").html(sumemerge);
        
        $("#toteme3").empty();
        $("#toteme3").html(sumemerge);
        
        $("#emerfact").empty();
        $("#emerfact").html(sumemergefac);
        
        
        
        
    });
    
    
    $.post("../ajax/dashcontabilidad.php?op=gseemergenciafiltro&fecha="+fecha, function (data, status) {
        data = JSON.parse(data);
        
        var sumN = 0, sumD = 0, sumO = 0 ;
        for (i = 0; i < data.length; i++) {
            if(data[i]["5"] == 1){
                //normal
                sumN = data[i]['2'];
                
            }else if(data[i]["5"] == 7){
                //detenido
                sumD = data[i]['2'];
                
            }else if(data[i]["5"] == 8){
                //observacion  
                sumO = data[i]['2'];
            }
        }
        
        $("#temen").empty();
        $("#temen").html(sumN);
        
        $("#temed").empty();
        $("#temed").html(sumD);
        
        $("#temeo").empty();
        $("#temeo").html(sumO);
    });
}

function cargaguias() {
    $.post("../ajax/dashgse.php?op=datostodos", {"idtservicios": '1,2,3,4,5,6'}, function (data, status) {
        data = JSON.parse(data);
        //Actualizamos valores
        
        var gdia = 0, gproceso = 0, gcompleta = 0, gcerrada = 0, gcerradaSF = 0;
        
        for(i = 0; i < data.length; i++){
            
            if(data[i]["7"] == 1 || data[i]["7"] == 4 | data[i]["7"] == 5 | data[i]["7"] == 6){
                //reparacion
                gdia += parseInt(data[i]["2"]);
                gproceso += parseInt(data[i]["3"]);
                gcompleta += parseInt(data[i]["9"]);
                gcerrada += parseInt(data[i]["5"]);
                gcerradaSF += parseInt(data[i]["8"]);
                
            }else if(data[i]["7"] == 2){
                //emergencia
                //mantencion
                $("#edia").empty();
                $("#edia").html(data[i]["2"]);
                
                $("#eproceso").empty();
                $("#eproceso").html(data[i]["3"]);
                
                $("#efirmadas").empty();
                $("#efirmadas").html(data[i]["9"]);
                
                $("#ecerrado").empty();
                $("#ecerrado").html(data[i]["5"]);
                
                $("#ecerradosf").empty();
                $("#ecerradosf").html(data[i]["8"]);
                
            }else if(data[i]["7"] == 3){
                //mantencion
                $("#mdia").empty();
                $("#mdia").html(data[i]["2"]);
                
                $("#mproceso").empty();
                $("#mproceso").html(data[i]["3"]);
                
                $("#mfirmadas").empty();
                $("#mfirmadas").html(data[i]["9"]);
                
                $("#mcerrado").empty();
                $("#mcerrado").html(data[i]["5"]);
                
                $("#mcerradosf").empty();
                $("#mcerradosf").html(data[i]["8"]);
            }
        }
        
        $("#rdia").empty();
        $("#rdia").html(gdia);

        $("#rproceso").empty();
        $("#rproceso").html(gproceso);

        $("#rfirmadas").empty();
        $("#rfirmadas").html(gcompleta);

        $("#rcerrado").empty();
        $("#rcerrado").html(gcerrada);

        $("#rcerradosf").empty();
        $("#rcerradosf").html(gcerradaSF);
        
    });
}

function grafico() {
    $.post("../ajax/dashcontabilidad.php?op=grafico", function (data, status) {
        data = JSON.parse(data);
        arrmatn = [0,0,0,0,0,0,0,0,0,0,0,0,0];
        arrmant2 = [0,0,0,0,0,0,0,0,0,0,0,0,0];
        
        
        arrrep = [0,0,0,0,0,0,0,0,0,0,0,0,0];
        arrrepM = [0,0,0,0,0,0,0,0,0,0,0,0,0];
        arrrepN = [0,0,0,0,0,0,0,0,0,0,0,0,0];
        arrrepI = [0,0,0,0,0,0,0,0,0,0,0,0,0];
        
        
        var fecha = new Date();
        mesact = fecha.getMonth();
        
        messtr = arrmescompleto[mesact];
        
        $("#mesact").empty();
        $("#mesact").html(" (mes " + messtr + ")");
        
        for (i = 0; i < data.length; i++) {
            
            if(data[i]["4"] == 1 || data[i]["4"] == 4 | data[i]["4"] == 5 | data[i]["4"] == 6){
                //reparacion
                if(data[i]["4"] == 1){
                   arrrep[arrmeses.indexOf(data[i]['1'].substr(0,3))] = data[i]['2'];  
                }else if (data[i]["4"] == 4){
                    arrrepM[arrmeses.indexOf(data[i]['1'].substr(0,3))] = data[i]['2'];  
                }else if (data[i]["4"] == 5){
                    arrrepN[arrmeses.indexOf(data[i]['1'].substr(0,3))] = data[i]['2'];  
                }else if(data[i]["4"] == 6){
                    arrrepI[arrmeses.indexOf(data[i]['1'].substr(0,3))] = data[i]['2'];  
                }
            }else if(data[i]["4"] == 3){
                //mantencion
                arrmatn[arrmeses.indexOf(data[i]['1'].substr(0,3))] = data[i]['2'];  
                arrmant2[arrmeses.indexOf(data[i]['1'].substr(0,3))] = data[i]['3'];
            }
        }
        
        if ($('#grafico').length) {
            var ctx = document.getElementById("grafico");
            var lineChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: arrmeses,
                    datasets: [{
                            label: "Guias emitidas",
                            backgroundColor: "rgba(52,152,219, 0.31)",
                            borderColor: "rgba(52,152,219, 0.7)",
                            pointBorderColor: "rgba(52,152,219, 0.7)",
                            pointBackgroundColor: "rgba(52,152,219, 0.7)",
                            pointHoverBackgroundColor: "#fff",
                            pointHoverBorderColor: "rgba(220,220,220,1)",
                            pointBorderWidth: 1,
                            data: arrmatn
                        },
                        {
                            label: "Guias facturadas",
                            backgroundColor: "rgba(3, 88, 106, 0.3)",
                            borderColor: "rgba(3, 88, 106, 0.70)",
                            pointBorderColor: "rgba(3, 88, 106, 0.70)",
                            pointBackgroundColor: "rgba(3, 88, 106, 0.70)",
                            pointHoverBackgroundColor: "#fff",
                            pointHoverBorderColor: "rgba(151,187,205,1)",
                            pointBorderWidth: 1,
                            data: arrmant2
                        }]
                },
                options: {
                    legend: {
                        display: false
                    }
                }
            });

        }
                
        if ($('#reparacion').length) {
            var ctx = document.getElementById("reparacion");
            var mybarChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: arrmeses,
                    datasets: [{
                            label: "guias reparación",
                            backgroundColor: "#3498DB",
                            data: arrrep
                        },{
                            label: "guias reparación mayor",
                            backgroundColor: "#455C73",
                            data: arrrepM
                        },{
                            label: "guias normalización",
                            backgroundColor: "#5cb85c",
                            data: arrrepN
                        },{
                            label: "guias ingeniería",
                            backgroundColor: "#f0ad4e",
                            data: arrrepI
                        }]
                },
                options: {
                    legend: {
                        display: false
                    },
                    scales: {
                        yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                    }
                }
            });
        }
        
        
    });
    
    $.post("../ajax/dashcontabilidad.php?op=graficoemergencia", function (data, status) {
        data = JSON.parse(data);
        
        arremerN = [0,0,0,0,0,0,0,0,0,0,0,0,0];
        arremerD = [0,0,0,0,0,0,0,0,0,0,0,0,0];
        arremerO = [0,0,0,0,0,0,0,0,0,0,0,0,0];
        
        
              
        for (i = 0; i < data.length; i++) {
            
            if(data[i]["5"] == 1){
                //normal
                arremerN[arrmeses.indexOf(data[i]['1'].substr(0,3))] = data[i]['2']; 
                               
            }else if(data[i]["5"] == 7){
                //detenido
                arremerD[arrmeses.indexOf(data[i]['1'].substr(0,3))] = data[i]['2'];  
                                
            }else if(data[i]["5"] == 8){
                //observacion
                arremerO[arrmeses.indexOf(data[i]['1'].substr(0,3))] = data[i]['2'];  
                
            }
        }
               
        
        
        if ($('#emergencia').length) {
            var ctx = document.getElementById("emergencia");
            var mybarChart2 = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: arrmeses,
                    datasets: [{
                            label: "guias estado normal",
                            backgroundColor: "#3498DB",
                            data: arremerN
                        },{
                            label: "guias estado detenido",
                            backgroundColor: "#455C73",
                            data: arremerD
                        },{
                            label: "guias estado observacion",
                            backgroundColor: "#5cb85c",
                            data: arremerO
                        }]
                },
                options: {
                    legend: {
                        display: false
                    },
                    scales: {
                        yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                    }
                }
            });
        }
    });
}



init();