var tabla, tablafase;

function init(){
    mostrarform(false);
    listar();
    
     //condiciones de pago 
    $.post("../ajax/usuario.php?op=selectuser", (r) => {
        $("#iduser").html(r);
        $("#iduser").selectpicker('refresh');
    });
    
    $("#formulario").on("submit", (e) => {
        e.preventDefault();
        agregar();
    });
    
    tablafase = $("#tblfases").DataTable({
        "paging": false,
        "info": false,
        "bFilter": false,
        "bAutoWidth": false,
        "ordering": false
    });
}

function mostrarform(flag) {
    //limpiar();
    if (flag) {
        $("#listadccosto").hide();
        $("#formularioccosto").show();
        $("#op_agregar").hide();
        $("#op_listar").show();
    } else {

        $("#formularioccosto").hide();
        $("#listadccosto").show();
        $("#op_agregar").show();
        $("#op_listar").hide();
    }
}

function cancelarform(){
    limpiar();
    mostrarform(false);
    tabla.ajax.reload();
}

function listar(){
    tabla = $('#tblccosto').dataTable({
        "responsive": true,
        "aProcessing": true,
        "aServerSide": true,
        "scrollX": false,
        dom: 'Bfrtip',
        buttons: Botones,
        "language": Español,
        "ajax": {
            url: '../ajax/soladquisicion.php?op=aprolistartccosto',
            type: "POST",
            dataType: "json",
            error: (e) => {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 20, //Paginacion 10 items
        "order": [[0, "desc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}

function editar(tcentrocosto){
    mostrarform(true);
    $("#idtcentrocosto").val(tcentrocosto);
    
    
    tablafase.rows().remove().draw();
    $.post("../ajax/soladquisicion.php?op=aprolistarfases", {idtcentrocosto: tcentrocosto}, function (data, status) {
        data = JSON.parse(data);
        for (i = 0; i < data.length; i++) {
            tablafase.row.add([
                data[i]["0"],
                data[i]["1"],
                data[i]["2"],
                data[i]["3"]
            ]).draw(false);
        }
        $("#orden").val(data.length + 1);
    });
    
    /*tablafase = $('#tblfases').dataTable({
        "responsive": true,
        "aProcessing": true,
        "aServerSide": true,
        "scrollX": false,
        dom: 'Bfrtip',
        buttons: Botones,
        "language": Español,
        "ajax": {
            url: '../ajax/soladquisicion.php?op=aprolistarfases',
            type: "POST",
            dataType: "json",
            data: {
                idtcentrocosto: tcentrocosto
            },
            error: (e) => {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 20 //Paginacion 10 items
    }).DataTable();  */
    
    
}

function agregar(){
    var formData = new  FormData($("#formulario")[0]);
    
    $.ajax({
        url: '../ajax/soladquisicion.php?op=guardaryeditarfase',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,

        success: function (datos) {
            //bootbox.alert(datos);
            if (datos != '0') {
                new PNotify({
                    title: 'Correcto!',
                    text: 'Fase guardada con exito.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
                limpiar();
            } else {
                new PNotify({
                    title: 'Error!',
                    text: 'Fase no guardada.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
                limpiar();
            }
            tablafase.ajax.reload();
        }
    });
    limpiar();
}

function del(idfase){
    bootbox.confirm("va a eliminar esta fase, ¿esta seguro?", (resultado) => {
        if(resultado){
            $.ajax({
                url: '../ajax/soladquisicion.php?op=eliminar&idfase='+idfase,
                type: "POST",
                async: false,
                cache: false,
                timeout: 30000,
                contentType: false,
                processData: false,
                success: (datos) => {
                    if (datos != 0) {
                        new PNotify({
                            title: 'Correcto!',
                            text: 'Eliminado con exito.',
                            type: 'success',
                            styling: 'bootstrap3'
                        });
                    } else {
                        new PNotify({
                            title: 'Error!',
                            text: 'Hubo un error al eliminar.',
                            type: 'error',
                            styling: 'bootstrap3'
                        });
                    }
                    tablafase.ajax.reload();
                }
            });
        }
    });
}

function limpiar(){
    $("#completa").attr('checked', false);
    $('input.flat').iCheck({checkboxClass: 'icheckbox_flat-green', radioClass: 'iradio_flat-green'});
    $("#iduser").val("");
    $("#iduser").selectpicker("refresh");
    $("#orden").val("");
    $('input.flat').iCheck({checkboxClass: 'icheckbox_flat-green', radioClass: 'iradio_flat-green'});
};

init();