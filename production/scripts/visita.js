var tabla;

//funcion que se ejecuta iniciando
function init() {
    mostrarform(false);
    var url = window.location.pathname;
    var str = url.split("/");
    var lastUrl = str.pop();
    console.log(lastUrl);
    // console.log(str[3]);

    // if (str[3] == "visita.php") {
    if (lastUrl == "visita.php") {
        listar('1,2');
    } else {
        listarmodernizacion(2);
    }

    $('[data-toggle="tooltip"]').tooltip();

    $("#formulario").on("submit", function (e) {
        visita(e);
    });

}


// Otras funciones
function limpiar() {
    $("#idproyecto").val("");
    $("#idestado").val("");
    $("#observaciones").val("");
    $("#esetapa").prop("checked", false);
}


function mostrarform(flag) {
    limpiar();
    if (flag) {
        $("#listadoproyectos").hide();
        $("#formulariovisita").show();
        $("#op_actualizar").hide();
        $("#btnGuardar").prop("disabled", false);
    } else {
        $("#formulariovisita").hide();
        $("#listadoproyectos").show();
        $("#op_actualizar").show();
        $("#btnGuardar").prop("disabled", false);
    }

}

function revisarform(flag) {
    limpiar();
    if (flag) {
        $("#listadoproyectos").hide();
        $("#revisarestado").show();
        $("#op_actualizar").hide();
    } else {
        $("#revisarestado").hide();
        $("#listadoproyectos").show();
        $("#op_actualizar").show();
    }

}

function cancelarform() {
    limpiar();
    mostrarform(false);

}

function cancelarrevisar() {
    limpiar();
    revisarform(false);
}

function mostrar(idproyecto) {

    $.post("../ajax/visitasinstalacionesAjax.php?op=mostrar", {idproyecto: idproyecto}, function (data, status) {
        data = JSON.parse(data);
        mostrarform(true);

        $("#nombProy").empty();
        $("#created_time").empty();
        $("#estadoproy").empty();
        $("#codigo").empty();
        $("#ccnomb").empty();
        $("#pm").empty();
        $("#supervisor").empty();
        $("#numerovisita").empty();
        $("#ascensores").empty();

        $("#idproyecto").val(data.idproyecto);
        $("#idestado").val(data.estado);
        $("#idventa").val(data.idventa);


        $("#nombProy").append(data.nombre);
        $("#created_time").append(data.fecha);
        $("#estadoproy").append(data.estadonomb);
        $("#codigo").append(data.codigo);
        $("#ccnomb").append(data.ccnomb);
        $("#pm").append(data.pm);
        $("#supervisor").append(data.supervisor);
        $("#numerovisita").append(data.nrovisita);



        if (data.idventa && data.estado > 1) {

            $.post("../ajax/ascensorAjax.php?op=listar", {idventa: data.idventa}, function (data3, status) {
                data3 = JSON.parse(data3);
                var asce = "";
                for (i = 0; i < data3.length; i++) {
                    asce += '<div class="product_price col-md-6 col-sm-6 col-xs-12">'
                            + '    <input type="hidden" id="idascensor' + data3[i]['idascensor'] + '" name="idascensor[]" class="form-control" required="Campo requerido" value="' + data3[i]['idascensor'] + '">'
                            + '    <input type="hidden" id="estadoins' + data3[i]['idascensor'] + '" name="estadoins' + data3[i]['idascensor'] + '" class="form-control" required="Campo requerido" value="' + data3[i]['estadoins'] + '">'
                            + '    <label class="green">' + data3[i]['codigo'] + ' - ken: ' + data3[i]['ken'] + ' - ubicación: ' + data3[i]['ubicacion'] + '</label><br>'
                            + '    <label style="color:' + data3[i]['color'] + ';"> Etapa: ' + data3[i]['strestado'] + '</label>'
                            + '    <div class="col-md-12 col-sm-12 col-xs-12 form-group">'
                            + '        <p>Observaciones de la visita:</p>'
                            + '        <textarea type="text" id="observaciones' + data3[i]['idascensor'] + '" style="text-transform:uppercase;" name="observaciones' + data3[i]['idascensor'] + '" class="resizable_textarea form-control" required="Campo requerido"></textarea>'
                            + '    </div>'
                            + '    <div class="col-md-12 col-sm-12 col-xs-12 form-group" id="">'
                            + '        <p><input type="checkbox" id="esetapa' + data3[i]['idascensor'] + '" name="esetapa' + data3[i]['idascensor'] + '" value="1" class="js-switch"/> '
                            + '        Marque si la etapa fue completada</p>'
                            + '    </div>'
                            + '</div>';
                }
                $("#ascensores").append(asce);
            });
        }


        if (data.estado == 1) {
            $("#completada").hide();
            $("#esetapa").trigger('click').attr("checked", "checked");
            var btn = document.getElementById("btnGuardar");
            btn.innerHTML = "Iniciar Proyecto";
        } else {
            $("#completada").show();
            $("#esetapa").trigger('click').attr("checked", "checked");
            $("#esetapa").trigger('click').removeAttr("checked");
            var btn = document.getElementById("btnGuardar");
            btn.innerHTML = "Agregar";
        }


    });


}

function visitaotro(idproyecto) {

    $.post("../ajax/visitasinstalacionesAjax.php?op=mostrar", {idproyecto: idproyecto}, function (data, status) {
        data = JSON.parse(data);
        mostrarform(true);

        $("#nombProy").empty();
        $("#created_time").empty();
        $("#estadoproy").empty();
        $("#codigo").empty();
        $("#ccnomb").empty();
        $("#pm").empty();
        $("#supervisor").empty();
        $("#numerovisita").empty();
        $("#ascensores").empty();

        $("#idproyecto").val(data.idproyecto);
        $("#idestado").val(data.estado);
        $("#idventa").val(data.idventa);


        $("#nombProy").append(data.nombre);
        $("#created_time").append(data.fecha);
        $("#estadoproy").append(data.estadonomb);
        $("#codigo").append(data.codigo);
        $("#ccnomb").append(data.ccnomb);
        $("#pm").append(data.pm);
        $("#supervisor").append(data.supervisor);
        $("#numerovisita").append(data.nrovisita);



        if (data.idventa && data.estado > 1) {

            $.post("../ajax/ascensorAjax.php?op=listar", {idventa: data.idventa}, function (data3, status) {
                data3 = JSON.parse(data3);
                var asce = "";
                for (i = 0; i < data3.length; i++) {
                    asce += '<div class="product_price col-md-6 col-sm-6 col-xs-12">'
                            + '    <input type="hidden" id="idascensor' + data3[i]['idascensor'] + '" name="idascensor[]" class="form-control" required="Campo requerido" value="' + data3[i]['idascensor'] + '">'
                            + '    <input type="hidden" id="estadoins' + data3[i]['idascensor'] + '" name="estadoins' + data3[i]['idascensor'] + '" class="form-control" required="Campo requerido" value="' + data3[i]['estadoins'] + '">'
                            + '    <label class="green">' + data3[i]['codigo'] + ' - ken: ' + data3[i]['ken'] + ' - ubicación: ' + data3[i]['ubicacion'] + '</label><br>'
                            + '    <label style="color:' + data3[i]['color'] + ';"> Etapa: ' + data3[i]['strestado'] + '</label>'
                            + '    <div class="col-md-12 col-sm-12 col-xs-12 form-group">'
                            + '        <p>Observaciones de la visita:</p>'
                            + '        <textarea type="text" id="observaciones' + data3[i]['idascensor'] + '" style="text-transform:uppercase;" name="observaciones' + data3[i]['idascensor'] + '" class="resizable_textarea form-control" required="Campo requerido"></textarea>'
                            + '    </div>'
                            + '</div>';
                }
                $("#ascensores").append(asce);
            });
        }



        $("#completada").show();
        $("#esetapa").trigger('click').attr("checked", "checked");
        $("#esetapa").trigger('click').removeAttr("checked");
        var btn = document.getElementById("btnGuardar");
        btn.innerHTML = "Agregar";



    });


}


function revisar(idproyecto) {

    $.post("../ajax/visitasinstalacionesAjax.php?op=mostrar", {idproyecto: idproyecto}, function (data, status) {
        data = JSON.parse(data);
        revisarform(true);

        $("#nombProy2").empty();
        $("#created_time2").empty();
        $("#estadoproy2").empty();
        $("#codigo2").empty();
        $("#ccnomb2").empty();
        $("#pm2").empty();
        $("#supervisor2").empty();
        $("#numerovisita2").empty();
        $("#ascensores2").empty();

        $("#idproyecto2").val(data.idproyecto);
        $("#idestado2").val(data.estado);
        $("#idventa2").val(data.idventa);

        if (data.estado == 202) {
            $("#btnGrabar2").hide();
        }

        $("#nombProy2").append(data.nombre);
        $("#created_time2").append(data.fecha);
        $("#estadoproy2").append(data.estadonomb + " - (Duración: " + data.dias + " dias)");
        $("#codigo2").append(data.codigo);
        $("#ccnomb2").append(data.ccnomb);
        $("#pm2").append(data.pm);
        $("#supervisor2").append(data.supervisor);
        $("#numerovisita2").append(data.nrovisita);



        if (data.idventa) {

            $.post("../ajax/ascensorAjax.php?op=listar", {idventa: data.idventa}, function (data3, status) {
                data3 = JSON.parse(data3);
                var asce = "";
                for (i = 0; i < data3.length; i++) {
                    asce += '<div class="product_price col-md-6 col-sm-6 col-xs-12">'
                            + '    <input type="hidden" id="idascensor' + data3[i]['idascensor'] + '" name="idascensor[]" class="form-control" required="Campo requerido" value="' + data3[i]['idascensor'] + '">'
                            + '    <input type="hidden" id="estadoins' + data3[i]['idascensor'] + '" name="estadoins' + data3[i]['idascensor'] + '" class="form-control" required="Campo requerido" value="' + data3[i]['estadoins'] + '">'
                            + '    <label class="green">' + data3[i]['codigo'] + ' - ken: ' + data3[i]['ken'] + ' - ubicación: ' + data3[i]['ubicacion'] + '</label><br>'
                            + '    <label style="color:' + data3[i]['color'] + ';"> Etapa: ' + data3[i]['strestado'] + '</label>'
                            + '</div>';
                }
                $("#ascensores2").append(asce);
            });
        }


    });


}

function listar(tipoproyecto) {
    tabla = $('#tblproyectos').dataTable({
        "responsive": true,
        "aProcessing": true,
        "aServerSide": true,
        "scrollX": false,
        dom: 'Bfrtip',
        buttons: Botones,
        "language": Español,
        "ajax": {
            url: '../ajax/visitasinstalacionesAjax.php?op=listar',
            type: "get",
            dataType: "json",
            data: {'tipoproyecto': tipoproyecto},
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 20, //Paginacion 10 items
        "order": [[2, "desc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}

function listarmodernizacion(tipoproyecto) {
    tabla = $('#tblproyectos').dataTable({
        "responsive": true,
        "aProcessing": true,
        "aServerSide": true,
        "scrollX": false,
        dom: 'Bfrtip',
        buttons: Botones,
        "language": Español,
        "ajax": {
            url: '../ajax/visitasinstalacionesAjax.php?op=listarModernizacion',
            type: "get",
            dataType: "json",
            data: {'tipoproyecto': tipoproyecto},
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 20, //Paginacion 10 items
        "order": [[2, "desc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}

function visita(e) {
    e.preventDefault();
    $("#btnGuardar").prop("disabled", true);
    var formData = new FormData($("#formulario")[0]);

    //if( $('#esetapa').prop('checked')) {
    bootbox.confirm("Va a registrar las visitas a este proyecto, ¿esta seguro?", function (result) {
        if (result) {
            $.ajax({
                url: '../ajax/visitasinstalacionesAjax.php?op=visitayetapa',
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,

                success: function (datos) {
                    bootbox.alert(datos);
                    mostrarform(false);
                    tabla.ajax.reload();
                }
            });
        }
    });

    limpiar();

}

function finalizar() {

    $("#btnGuardar").prop("disabled", true);
    var formData = new FormData($("#formulario2")[0]);

    bootbox.confirm("Va a avanzar a este proyecto, ¿esta seguro?", function (result) {
        if (result) {
            $.ajax({
                url: '../ajax/visitasinstalacionesAjax.php?op=finalizarmodernizacion',
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,

                success: function (datos) {
                    bootbox.alert(datos);
                    revisarform(false);
                    tabla.ajax.reload();
                }
            });
        }
    });

    limpiar();
}

function pdf(idventa) {
    var a = document.createElement('a');
    a.href = '../ajax/venta.php?op=PDFCorto&idventa=' + idventa;
    a.download = 'venta.pdf';
    a.click();
    delete a;
}

function pdfvisitas(idproyecto) {
    /*alert('dsds');
    return;*/
    var a = document.createElement('a');
    a.href = '../ajax/visitasinstalacionesAjax.php?op=PDF&idproyecto=' + idproyecto;
    a.download = 'proyecto.pdf';
    a.click();
    delete a;
}

function MostrarDocumentos(idventa,idproyecto){
    var dibujar = '<div class="col-md-3 col-sm-12 col-xs-12 text-center" id="solicitud"><p style="padding:15px;"><span class="text-danger" onClick="MostrarPreview(\'reportepdf\','+idproyecto+')"><i class="fa fa-file-pdf-o fa-3x"></i></span><br><span><h4>Reporte PDF</h4></span></p></div><div class="col-md-3 col-sm-12 col-xs-12 text-center" id="ordencompra"><p style="padding:15px;"><span class="text-danger" onClick="MostrarPreview(\'memoventa\','+idventa+')"><i class="fa fa-file-pdf-o fa-3x"></i></span><br><span><h4>Memo de Venta</h4></span></p></div>';
    $("#dibujar").html(dibujar);
    $('#modalDocumentos').modal('show');
}

function MostrarPreview(tipo, id){
    if(tipo == 'reportepdf'){        
        var url = '../ajax/visitasinstalacionesAjax.php?op=PDF&idproyecto='+id;
        var object = '<object class="PDFdoc" width="100%" style="height: 45vw;" data="'+ url +' "></object>';
        // var object = '<object class="PDFdoc" width="100%" style="height: 45vw;" type="application/pdf" data="'+ url +' "></object>';
        $("#contenido").html(object);
    }else if(tipo == 'memoventa'){
        var url = '../ajax/venta.php?op=PDFCorto&idventa='+id;
        var object = '<object class="PDFdoc" width="100%" style="height: 45vw;" data="'+ url +' "></object>';
        $("#contenido").html(object);
    }
    $('#modalPreview').modal('show');
}

$('#modalDocumentos').on('hidden.bs.modal', function () {
    $("#dibujar").html('');
});
$('#modalPreview').on('hidden.bs.modal', function () {
    $("#contenido").html('');
});

init();