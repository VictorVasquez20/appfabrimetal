var tabla;

//funcion que se ejecuta iniciando
function init(){
	mostarform();
	listar();
}


function mostarform(){
		$("#listadocentros").show();
		$("#op_actualizar").show();
}

function listar(){
	tabla=$('#tblatencion').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons:[
			'copyHtml5',
			'print',
			'excelHtml5',
			'csvHtml5',
			'pdf'
		],
		"ajax":{
			url:'../ajax/solatencion.php?op=listar',
			type:"get",
			dataType:"json",
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 10,
		"order" : [[6 , "desc"]] 
	}).DataTable();
}

function procesado(idsolatencion){

	bootbox.confirm("ESTA SEGURO QUE QUIERE CAMBIAR EL ESTADO DE SOLICITUD A PROCESADA?", function(result){
		if(result){
			$.post("../ajax/solatencion.php?op=procesar",{idsolatencion:idsolatencion}, function(e){
				bootbox.alert(e);
				tabla.ajax.reload();
			});	
		}
	});
}

function descartar(idsolatencion){
    
        bootbox.prompt("INDIQUE EL MOTIVO POR EL QUE DESCARTA LA SOLICITUD?", function(result){
        if(result){ 
            var datos = new FormData();
            datos.append("idsolatencion", idsolatencion);
            datos.append("motivo", result);
            $.ajax({
                    url:'../ajax/solatencion.php?op=descartar',
                    type:"POST",
                    data:datos,
                    contentType: false,
                    processData:false,

                    success: function(datos){
                        bootbox.alert(datos);
                        tabla.ajax.reload();
                    }
            });
        }          
    });
    
}

function actualizar() {
    tabla.ajax.reload();
}


init();