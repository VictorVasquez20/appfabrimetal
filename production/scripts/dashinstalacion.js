/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var tabla;
var arreglo = new Array() ;
var posicion = 0;

function init() {
    tabla = $("#ascensores").DataTable({
        "paging": false,
        "info": false,
        "bFilter": false,
        "bAutoWidth": false,
        "ordering": false
    });
    
    getproyectos();
    setInterval("slider()", 20000);
}

function slider(){
    $(".loader").show(); 
    if(posicion < arreglo.length){
        mostrar(arreglo[posicion]['id']);
        posicion += 1;
        setTimeout(function() {
             $(".loader").hide('slow');
        },3000);
    }else{
        posicion = 0;
        getproyectos();
        
        mostrar(arreglo[posicion]['id']);
    }
}

function getproyectos(){
    $.post("../ajax/proyecto.php?op=proyectosdahboar", function (datos, status) {
        datos = JSON.parse(datos);
        arreglo = datos;
        
        console.log(arreglo);
        mostrar(arreglo[posicion]['id']);
    });
}

function mostrar(idproyecto) {
    $.post("../ajax/proyecto.php?op=dashboardinstalacion", {idproyecto: idproyecto}, function (data, status) {
        data = JSON.parse(data);

        $("#nombProy").empty();
        $("#nombProy").append(data.nombre);
        $("#importacion").empty();
        $("#importacion").append('(' + data.importacion + ')')
        $("#estadoproy").empty();
        $("#estadoproy").append(data.estadonomb);
        $("#nombsupervisor").empty();
        $("#nombsupervisor").append(data.supervisor);
        $("#nombproject").empty();
        $("#nombproject").append(data.pm);
        $("#fecha").empty();
        $("#fecha").append(data.fecha);
        $("#centrocosto").empty();
        $("#centrocosto").append(data.ccnomb);
        $("#fechaactualiza").empty();
        $("#fechaactualiza").append(data.fechaactualiza);
        $("#dias").empty();
        $("#dias").append(data.dias);
        $("#imgsup").empty();
        $("#imgsup").append('<img class="img-responsive avatar-view" src="../files/usuarios/' + data.imgsup +'" alt="Avatar" width="250" height="250" title="Change the avatar">');
        $("#imgproj").empty();
        $("#imgproj").append('<img class="img-responsive avatar-view" src="../files/usuarios/' + data.imgproject +'" alt="Avatar" width="250" height="250" title="Change the avatar">');

        if (data.idventa && data.estado > 1) {

            $.post("../ajax/ascensorAjax.php?op=listar", {idventa: data.idventa}, function (data3, status) {
                data3 = JSON.parse(data3);
                tabla.rows().remove().draw();
                for (i = 0; i < data3.length; i++) {
                    tabla.row.add([
                        '<b>' + data3[i]['codigo'] + '</b>',
                        '<td class="project_progress">'
                        +    '<div class="progress progress_sm">'
                        +        '<div class="progress-bar bg-green" role="progressbar" data-transitiongoal="' + data3[i]['carga'] +'" aria-valuenow="' + data3[i]['carga'] +'" style="width: ' + data3[i]['carga'] +'%;"></div>'
                        +    '</div>'
                        +    '<small>' + data3[i]['carga'] +'% Completo</small>'
                        +'</td>',
                        '<b>' + data3[i]['strestado'] + '</b>'
                    ]).draw(false);
                };
            });
            
            html = "";
            $.post("../ajax/visitasinstalacionesAjax.php?op=dashinstalacion", {idproyecto: idproyecto}, function (data2, status) {
                data2 = JSON.parse(data2);
                for (i = 0; i < data2.length; i++) {
                   html +=   '<li>'
                            +   '<img src="../files/usuarios/' + data2[i]['imagen'] +'" class="avatar" alt="Avatar">'
                            //+   '    <div class="message_date">'
                            //+   '        <h3 class="date text-info">' + data2[i]['dia'] +'</h3>'
                            //+   '        <p class="month">' + data2[i]['mes'] +'</p>'
                            //+   '    </div>'
                            +   '    <div class="message_wrapper">'
                            +   '        <h4 class="heading">' + data2[i]['asc'] + ' - ' + data2[i]['user'] + '</h4>'
                            +   '        <blockquote class="message">' + data2[i]['obs'] +'</blockquote>'
                            +   '    </div>'
                            +   '    <br>'
                            +   '    <p class="text-left">' + data2[i]['fecha'] + '</p>'
                            +   '</li>'
                };
                $("#visitas").empty();
                $("#visitas").append(html);
            });
        }
    });

    //visitas
}
init();

