var meses = {"":"TODOS","1":"ENERO","2":"FEBRERO","3":"MARZO","4":"ABRIL","5":"MAYO","6":"JUNIO","7":"JULIO","8":"AGOSTO","9":"SEPTIEMBRE","10":"OCTUBRE","11":"NOVIEMBRE","12":"DICIEMBRE",}
var tabla;

//funcion que se ejecuta iniciando
function init() {
    $.post("../ajax/controlimportacion.php?op=selectaniopantalla", function(r){
        $("#anio").html(r);
        var f= new Date();
        var anio,mes;
        
        anio = f.getFullYear();
        mes = parseInt(f.getMonth())+1;
        // alert(mes);
        // init();
        // $("#anio").val(2021);
        $('#anio').find('option[value=' + anio + ']').attr('selected', true).trigger("change");
    });

    $("#anio").on("change", function(e){
        $.get("../ajax/controlimportacion.php?op=selectmespantalla", {'anio':$("#anio").val()}, function(r){
            $("#mes").html(r);
        });
    });

    $('#form_filtros').on("submit", function(e){
        e.preventDefault();
        tablacontrolimportacion();
    });

    $('#form_filtros2').on("submit", function(e){
        e.preventDefault();
        tablacontrolimportacion2();
    });

    //si existe el elemento #tbllistaimportacion2 realizo una busqueda del año y mes actual automaticamente
    if ($('#tbllistaimportacion2').length)
        tablacontrolimportacion2();

    if ($('#tbllistaimportacion').length)
        tablacontrolimportacion();

    setInterval( function () {
        tabla.ajax.reload();
    }, 10000 );
}

function tablacontrolimportacion() {
    var f= new Date();
    var anio,mes;
    
    if($("#anio").val() == null || $("#anio").val() == ''){
        anio = f.getFullYear(); 
    }else{
        anio = $("#anio").val() 
    } 
    
     if($("#mes").val() == null || $("#mes").val() == ''){
        mes = ''; 
    }else{
        mes = $("#mes").val() 
    }

    tabla = $('#tbllistaimportacion').dataTable({
        "aProcessing":true,
        "aServerSide": true,
        "language": Español,
        dom: 'Bfrtip',
        "createdRow": function( row, data, dataIndex){
                // console.log(data[14]);
                if( data[14] == 'pendiente'){
                    $(row).addClass('rowRed');
                }
                else {
                    $(row).addClass('rowNormal');
                }
            },
        buttons:[
        ],
        "ajax":{
            url:'../ajax/controlimportacion.php?op=listarpantalla',
            type:"POST",
            dataType:"json",
                        data:{"anio":anio, 'mes': mes},
            error: function(e){
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
                 "iDisplayLength": 20, //Paginacion 
                 "order" : [[0 , "desc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}

function tablacontrolimportacion2() {
    var f= new Date();
    var anio,mes;
    
    if($("#anio").val() == null || $("#anio").val() == ''){
        anio = f.getFullYear(); 
    }else{
        anio = $("#anio").val();
    } 
    
     if($("#mes").val() == null || $("#mes").val() == ''){
        mes = parseInt(f.getMonth())+1; 
    }else{
        mes = $("#mes").val() 
    }

    tabla = $('#tbllistaimportacion2').dataTable({
        "aProcessing":true,
        "aServerSide": true,
        "language": Español,
        "searching": false,
        "paging": false,
        dom: 'Bfrtip',
        "createdRow": function( row, data, dataIndex){
                if( data[14] == 'pendiente'){
                    $(row).addClass('rowRed');
                }
                else {
                    $(row).addClass('rowNormal');
                }
            },
        buttons:[
        ],
        "ajax":{
            url:'../ajax/controlimportacion.php?op=listarpantalla',
            type:"POST",
            dataType:"json",
                        data:{"anio":anio, 'mes': mes, 'estado': 'pendiente'},
            error: function(e){
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
                 "iDisplayLength": 20, //Paginacion 
                 "order" : [[0 , "desc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}

function mostrarcomentarios(codigo, codkm){
    $('#idcom2').val(codigo); //asigno valor al campo oculto del formulario en el modal
    $('#codkm2').text(codkm);

    tabla3 = $('#tblcomentarios').dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Bfrtip',
        "autoWidth": false,
        buttons:[
        ],
        "language": Español,
        "ajax": {
            url: '../ajax/controlimportacion.php?op=listarcomentarios&codigo='+codigo,
            type: "get",
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        columnDefs: [
                {
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap width-98'>" + data + "</div>";
                    },
                    targets: 1
                },
                { "width": "18%", "targets": 0 },
                { "width": "15%", "targets": 1 },
             ],
        "bDestroy": true,
        "iDisplayLength": 5, //Paginacion 5 items
        "order": [[0, "desc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();

    $('#modalComent').modal('toggle');
}

$('body').on('submit', '#formcomentario', function(e) {
    e.preventDefault();
    var formData = new FormData($(this)[0]);
    // alert("Hello");

    $.ajax({
        url: '../ajax/controlimportacion.php?op=guardarcomentario',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function (datos) {
            if (datos !== 0){
                new PNotify({
                    title: 'Registrado!',
                    text: 'Comentario registrado con exito.',
                    type: 'success',
                    styling: 'bootstrap3'
                });

                $('#comentario').val('');
                tabla3.ajax.reload();
            }else{
                new PNotify({
                    title: 'Error!',
                    text: 'Hubo un error al registar. ',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
        }
    });
});

window.addEventListener('load', function () {
    var f= new Date();
    var mes;
    mes = parseInt(f.getMonth())+1;
    $('#mes').find('option[value=' + mes + ']').attr('selected', true);
});


init();