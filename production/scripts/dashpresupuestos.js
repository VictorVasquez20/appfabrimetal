var tabla;
var tabla2;

//funcion que se ejecuta iniciando
function init() {
    mostarform();
    listar();
    listardet();
    setInterval("Actualizar()", 5000);
    $('[data-toggle="tooltip"]').tooltip();
}



function mostarform() {
        $("#listadopre").show();
        $("#listadodet").show();
}


function listar() {
    tabla = $('#tblpre').dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Bfrtip',
        buttons: [
        ],
        "ajax": {
            url: '../ajax/presupuesto.php?op=listarpre',
            type: "get",
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 8, //Paginacion 10 items
        "order": [[0, "asc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}

function listardet() {
    tabla2 = $('#tbldet').dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Bfrtip',
        buttons: [
        ],
        "ajax": {
            url: '../ajax/presupuesto.php?op=listardet',
            type: "get",
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 8, //Paginacion 10 items
        "order": [[0, "asc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}



function Actualizar() {
    tabla.ajax.reload();
    tabla2.ajax.reload();
}


init();