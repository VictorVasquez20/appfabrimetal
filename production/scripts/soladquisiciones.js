var tabla, tabladet, vertabladet;
var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
function init() {
    mostrarform(false);
    listar();

    //select centro costo
    $.post("../ajax/centrocosto.php?op=selectcentro", (r) => {
        $("#ccosto").html(r);
        $("#ccosto").selectpicker('refresh');
    });
    //select monedas
    $.post("../ajax/moneda.php?op=selecttodasmonedas", (r) => {
        $("#moneda").html(r);
        $("#moneda").selectpicker('refresh');
    });
    //select proveedores
    $.post("../ajax/proveedor.php?op=selectproveedor", (r) => {
        $("#proveedor").html(r);
        $("#proveedor").selectpicker('refresh');
    });
    //condiciones de pago 
    $.post("../ajax/condicionpago.php?op=selectcondicion", (r) => {
        $("#condpago").html(r);
        $("#condpago").selectpicker('refresh');
    });

    //selectcategoria
    $.post("../ajax/categoria_productoAjax.php?op=selectcategoria", (r) => {
        $("#det_categoria").html(r);
        $("#det_categoria").selectpicker('refresh');
    });
    
    //producto
    $("#det_categoria").on('change', () => {
        $.post("../ajax/productoAjax.php?op=selectproductoscategoria", {categoria: $("#det_categoria").val() }, (r) => {
            $("#det_producto").html(r);
            $("#det_producto").selectpicker('refresh');
        });
    });
    

    $("#formulario").on('submit', (e) => {
        e.preventDefault();
        grabarencabezado();
    });

    tabladet = $('#detalle').dataTable({
            "responsive": true,
            "aProcessing": true,
            "aServerSide": true,
            "scrollX": false,
            dom: 'Bfrtip',
            buttons: Botones,
            "language": Español
        });
        
    vertabladet = $("#ver_detalle").DataTable({
        "responsive": true,
        "aProcessing": true,
        "aServerSide": true,
        "scrollX": false,
        dom: 'Bfrtip',
        buttons: Botones,
        "language": Español
    });
}

function mostrarform(flag) {
    //limpiar();
    if (flag) {
        $("#listadsolicitudes").hide();
        $("#formsolicitudes").show();
        $("#op_agregar").hide();
        $("#op_listar").show();
    } else {

        $("#formsolicitudes").hide();
        $("#listadsolicitudes").show();
        $("#op_agregar").show();
        $("#op_listar").hide();
    }

}

function mostrarvista(flag){
    if (flag) {
        $("#listadsolicitudes").hide();
        $("#versolicitudes").show();
        $("#op_agregar").hide();
        $("#op_listar").show();
    } else {

        $("#versolicitudes").hide();
        $("#listadsolicitudes").show();
        $("#op_agregar").show();
        $("#op_listar").hide();
    }  
}

function listar() {
    s = getURLParameter("search");
    tabla = $('#tblguias').dataTable({
        "responsive": true,
        "aProcessing": true,
        "aServerSide": true,
        "scrollX": false,
        dom: 'Bfrtip',
        buttons: Botones,
        "language": Español,
        "ajax": {
            url: '../ajax/soladquisicion.php?op=listar',
            type: "POST",
            dataType: "json",
            error: (e) => {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 20, //Paginacion 10 items
        "order": [[2, "desc"]],
        "oSearch": {"sSearch": s}//Ordenar en base a la columna 0 descendente
    }).DataTable();
}


/**
 * obtiene los valores de la URL
 * dando el nombre de un parametro enviado
 * @param {string} name
 * @returns {string}
 */
function getURLParameter(name) {
    return decodeURI(
            (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search) || [, ""])[1]
            );
}

function editar(idsolicitud) {
    $.post("../ajax/soladquisicion.php?op=mostrar", {idsolicitud: idsolicitud}, (data, status) => {
        data = JSON.parse(data);
        mostrarform(true);
        
        //ENCABEZADO
        $("#idsolicitud").val(data.idsolicitud);
        $("#estado").val(data.estado);
        $("#fecha").val(data.fecha);
        $("#ccosto").val(data.ccosto);
        $("#ccosto").selectpicker('refresh');
        $("#proveedor").val(data.proveedor);
        $("#proveedor").selectpicker('refresh');
        $("#moneda").val(data.moneda);
        $("#moneda").selectpicker('refresh');
        $("#condpago").val(data.condpago);
        $("#condpago").selectpicker('refresh');
        $("#prioridad").val(data.prioridad);
        $("#observacion").val(data.observacion);
        
        
        
         //COTIZACIONES
        $.post("../ajax/soladquisicion.php?op=listarcotizacion", {idsolicitud: idsolicitud}, (data, status) => {
            data = JSON.parse(data);
            
            $("#archivos").empty();
            
            for(i = 0; i < data.length; i++){
                $("#idcotizacion").val(data[i]['idcotizacion']);
                $("#anterior").val(data[i]['archivo']);
                $("#archivos").html('<li><a target="_blank" href="../files/cotizacion/' + data[i]['idsolicitud'] + '/' + data[i]['archivo'] + '" class="atch-thumb"><img src="../public/build/images/PDF-logo.png" alt="img" height="50" width="50" /></a><div class="file-name">click para descargar</div></li>');
                
            }
        });
        
        tabladet = $('#detalle').dataTable({
            "responsive": true,
            "aProcessing": true,
            "aServerSide": true,
            "scrollX": false,
            dom: 'Bfrtip',
            buttons: Botones,
            "language": Español,
            "ajax": {
                url: '../ajax/detsoladquisicion.php?op=listar',
                type: "POST",
                data: {idsolicitud: idsolicitud},
                dataType: "json",
                error: (e) => {
                    console.log(e.responseText);
                }
            },
            "bDestroy": true,
            "iDisplayLength": 20, //Paginacion 10 items
            "order": [[2, "desc"]] //Ordenar en base a la columna 0 descendente
        }).DataTable();

    });
}

function cancelarform() {
    limpiar();
    limpiardet();
    mostrarform(false);
}

function terminarform() {
    limpiar();
    limpiardet();
    tabla.ajax.reload();
    mostrarform(false);
    new PNotify({
        title: 'Exito!',
        text: 'Datos guardados con exito.',
        type: 'success',
        styling: 'bootstrap3'
    });
}

function grabarencabezado() {
    var formData = new FormData($("#formulario")[0]);
    $.ajax({
        url: '../ajax/soladquisicion.php?op=guardaryeditar',
        type: "POST",
        data: formData,
        async: false,
        cache: false,
        timeout: 30000,
        contentType: false,
        processData: false,
        success: (datos) => {
            if (!isNaN(datos) && datos != 0 && datos != "") {
                new PNotify({
                    title: 'Registrado!',
                    text: 'Solicitud guardada con exito.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
                $("#idsolicitud").val(datos);
            } else {
                new PNotify({
                    title: 'Error!',
                    text: 'Hubo un error al registar. '+datos,
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
            //limpiar();
            //mostrarform(false);
            //tabla.ajax.reload();
        }
    });

}

function limpiar() {
    $("#idsolicitud").val(0);
    $("#estado").val(0);
    $("#fecha").val("");
    $("#ccosto").val("");
    $("#ccosto").selectpicker('refresh');
    $("#proveedor").val("");
    $("#proveedor").selectpicker('refresh');
    $("#moneda").val("");
    $("#condpago").val("");
    $("#prioridad").val("");
    $("#observacion").val("");
    $("#anterior").val("");
    $("#archivo").val("");
    $("#archivos").empty();
}

function enviar(idsolicitud, fase, estado = 1){
    bootbox.confirm("Va a enviar esta solicitud a revisión, no podrá volver a editar los datos. ¿esta seguro?", (result) => {
        if(result){
            $.post("../ajax/soladquisicion.php?op=avanzar", {idsolicitud: idsolicitud, estado: estado, fase: (fase + 1)}, (data, status) => {
                data = JSON.parse(data);
                
                if (data != 0) {
                    new PNotify({
                        title: 'Exito!',
                        text: 'Solicitud Enviada a revisión.',
                        type: 'success',
                        styling: 'bootstrap3'
                    });
                } else {
                    new PNotify({
                        title: 'Error!',
                        text: 'Hubo un error, intente de nuevo mas tarde.',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                }
                tabla.ajax.reload();
            });
        }
    });
}

function ver(idsolicitud){
    $.post("../ajax/soladquisicion.php?op=ver", {idsolicitud: idsolicitud}, (data, status) =>{
        data = JSON.parse(data);
        mostrarvista(true);
        
        //TAB 1
        $("#ver_numero").empty();
        $("#ver_total").empty();
        $("#ver_solicita").empty();
        $("#ver_ccosto").empty();
        $("#ver_condpago").empty();
        $("#ver_fecha").empty();
        $("#ver_proveedor").empty();
        $("#ver_moneda").empty();
        
        $("#ver_numero").append(data.idsolicitud);
        //$("#ver_total").append(new Intl.NumberFormat("de-DE").format(data.total));
        $("#ver_solicita").append(data.usuario);
        $("#ver_ccosto").append(data.ccosto);
        $("#ver_condpago").append(data.condpago);
        $("#ver_fecha").append(data.fecha);
        $("#ver_proveedor").append(data.rut + ' / ' + data.razonsocial);
        $("#ver_moneda").append(data.strmoneda);
        
        
        vertabladet.rows().remove().draw();
        var total = 0, neto = 0, iva = 0, subtotal = 0;
        $.post("../ajax/detsoladquisicion.php?op=listar", {idsolicitud: idsolicitud}, function (data, status) {
            data = JSON.parse(data);
            for (i = 0; i < data.aaData.length; i++) {
                vertabladet.row.add([
                    data.aaData[i]["0"],
                    data.aaData[i]["1"],
                    data.aaData[i]["2"],
                    data.aaData[i]["3"],
                    data.aaData[i]["4"]
                ]).draw(false);
                
                subtotal += parseFloat(data.aaData[i]["3"]);
            }
            $("#totdet").empty();
            $("#totdet").html("<b>" + new Intl.NumberFormat("de-DE").format(subtotal) + "</b>");
            
            $("#neto").empty();
            $("#neto").html("<b>" + new Intl.NumberFormat("de-DE").format(subtotal) + "</b>");
            
            
            iva = (subtotal*19)/100;
            $("#iva").empty();
            $("#iva").html("<b>" + new Intl.NumberFormat("de-DE").format(iva) + "</b>");
            
            $("#total").empty();
            $("#total").html("<b>" + new Intl.NumberFormat("de-DE").format(subtotal + iva) + "</b>");
            
            $("#ver_total").append(new Intl.NumberFormat("de-DE").format(subtotal + iva));
            
        });
       
        //TAB 2
        //OBSERVACIONES
        $("#ver_observaciones").empty();
        html1 = "";
        html = '<li>'
              +'     <img src="../files/usuarios/noimg.jpg" class="avatar" alt="Avatar">'
              +'      <div class="message_wrapper">'
              +'          <h4 class="heading">'+ data.usuario +'</h4>'
              +'          <blockquote class="message">'+ data.observacion +'</blockquote><br>'
              +'          <p>' + formatofecha(data.created_time) + '</p>'
              +'      </div>'
              +'  </li>'
        //$("#ver_observaciones").append(html);
        
        $.post("../ajax/soladquisicion.php?op=listarobservacion", {idsolicitud: idsolicitud}, (dataObs, status) => {
            dataObs = JSON.parse(dataObs);
            for(i = 0; i < dataObs.length; i++){
                 html1 += '<li>'
                    +'     <img src="../files/usuarios/noimg.jpg" class="avatar" alt="Avatar">'
                    +'      <div class="message_wrapper">'
                    +'          <h4 class="heading">'+ dataObs[i]['nombre'] +'</h4>'
                    +'          <blockquote class="message">'+ dataObs[i]['observacion'] +'</blockquote><br>'
                    +'          <p>' + formatofecha(dataObs[i]['created_time']) + '</p>'
                    +'      </div>'
                    +'  </li>'
                
            }
            $("#ver_observaciones").append(html + html1);
        });
        
        

        
        //TAB 3
        
        vercotizaciones(idsolicitud);

    });
}

/**
 * Muestra las cotizaciones que se han subido a la solicitud de adquisicion.
 * @param {type} idsolicitud
 * @return {undefined}
 */
function vercotizaciones(idsolicitud) {
    archivos = "";
    //COTIZACIONES
    $.post("../ajax/soladquisicion.php?op=listarcotizacion", {idsolicitud: idsolicitud}, (data, status) => {
        data = JSON.parse(data);
        for (i = 0; i < data.length; i++) {
            if (data[i]['sel'] != "") {
                archivos += '<div class="col-md-4 col-sm-6 col-xs-12">'
                        + '<div class="pricing ui-ribbon-container">'
                        + '  <div class="ui-ribbon-wrapper">'
                        + '    <div class="ui-ribbon">'
                        + '       <i class="glyphicon glyphicon-star-empty"></i>'
                        + '    </div>'
                        + '  </div>'
                        + '  <div class="title">'
                        + '    <h2>archivo ' + (i + 1) + '</h2>'
                        + '    <h1>COTIZACIÓN ' + (i + 1) + '</h1>'
                        + '    <span>seleccionado</span>'
                        + '  </div>'
                        + '  <div class="x_content">'
                        + '    <div class="">'
                        + '      <div class="pricing_features">'
                        + '        <ul class="list-unstyled text-left">'
                        + '          <li><i class="fa fa-check text-success"></i> Proveedor: <br><strong>' + data[i]['razonsocial'] + '</strong></li>'
                        + '              <li><i class="fa fa-check text-success"></i> Rut: <br><strong>' + data[i]['rut'] + '</strong></li>'
                        + '              <li><i class="fa fa-check text-success"></i> Moneda: <br><strong>' + data[i]['moneda'] + '</strong></li>'
                        + '              <li><i class="fa fa-check text-success"></i> Condicion de pago: <br><strong>' + data[i]['condpago'] + '</strong></li>'
                        + '              <li><i class="fa fa-file-pdf-o text-danger"></i> - <a target="_blank" href="../files/cotizacion/' + data[i]['idsolicitud'] + '/' + data[i]['archivo'] + '">Descargar cotizacion</a></li>'
                        + '        </ul>'
                        + '      </div>'
                        + '    </div>'
                        + ' </div>'
                        + '</div>'
                        + '</div>';

            } else {
                archivos += '<div class="col-md-4 col-sm-6 col-xs-12">'
                        + '   <div class="pricing">'
                        + '     <div class="title">'
                        + '    <h2>archivo ' + (i + 1) + '</h2>'
                        + '    <h1>COTIZACIÓN ' + (i + 1) + '</h1>'
                        + '  </div>'
                        + '  <div class="x_content">'
                        + ' <div class="">'
                        + '      <div class="pricing_features">'
                        + '        <ul class="list-unstyled text-left">'
                        + '          <li><i class="fa fa-check text-success"></i> Proveedor: <br><strong>' + data[i]['razonsocial'] + '</strong></li>'
                        + '              <li><i class="fa fa-check text-success"></i> Rut: <br><strong>' + data[i]['rut'] + '</strong></li>'
                        + '              <li><i class="fa fa-check text-success"></i> Moneda: <br><strong>' + data[i]['moneda'] + '</strong></li>'
                        + '              <li><i class="fa fa-check text-success"></i> Condicion de pago: <br><strong>' + data[i]['condpago'] + '</strong></li>'
                        + '              <li><i class="fa fa-file-pdf-o text-danger"></i> - <a target="_blank" href="../files/cotizacion/' + data[i]['idsolicitud'] + '/' + data[i]['archivo'] + '">Descargar cotizacion</a></li>'
                        + '        </ul>'
                        + '      </div>'
                        + '    </div>'
                        /*+ '    <div class="pricing_footer">'
                        + '      <a href="javascript:cambiacotizacion('+ data[i]["idcotizacion"] +', '+ idsolicitud +');" class="btn btn-success btn-block" role="button"><span>Sleccionar cotización</span></a>'
                        + '    </div>'*/
                        + '  </div>'
                        + '</div>'
                        + '</div>'
            }

        }

        $("#nroarchivos").empty();
        if (data.length < 3) {
            $("#nroarchivos").append(
                    '<div class="icheckbox_flat-green" style="position: relative;">'
                    + '    <input type="checkbox" class="flat" style="position: absolute; opacity: 0;" >'
                    + '    <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>'
                    + '</div> <span id="nroarchivos"></span> Faltan ' + (3 - data.length) + ' Cotizaciones');
        } else {
            $("#nroarchivos").append(
                    '<div class="icheckbox_flat-green" style="position: relative;">'
                    + '    <input type="checkbox" class="flat" style="position: absolute; opacity: 0;" checked="true" >'
                    + '    <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>'
                    + '</div> <span id="nroarchivos"></span> Archivos correctos');
            $('input.flat').iCheck({checkboxClass: 'icheckbox_flat-green', radioClass: 'iradio_flat-green'});


        }
        $("#archivos").val(data.length);
        $("#ver_archivos").empty();
        $("#ver_archivos").append(archivos);
    });
}


/* MANEJO DEL DETALLE */

function limpiardet(){
    $("#det_categoria").val("");
    $("#det_categoria").selectpicker('refresh');
    $("#det_producto").val("");
    $("#det_producto").selectpicker('refresh');
    $("#det_cantidad").val("");
    $("#det_valor").val("");
    $("#det_observacion").val("");
    
    if($("#detalle tr").length > 2){
        tabladet.rows().remove().draw();
    }
}

function addDetalle() {
    var formData = new FormData();
    prod = $("#det_producto").val();
    cant = $("#det_cantidad").val();
    val = $("#det_valor").val();
    obs = $("#det_observacion").val();

    idsolicitud = $("#idsolicitud").val();
    
    if(idsolicitud == 0){
        bootbox.alert("debe Guardar el borrador de las solicitud para continuar.");
        return false;
    }
    
    if (!prod) {
        bootbox.alert("debe seleccionar un producto");
        return false;
    }

    if (!cant) {
        bootbox.alert("debe ingresar una cantidad");
        return false;
    }

    if (!val) {
        bootbox.alert("debe ingresar un valor");
        return false;
    }

    formData.append('iddetsol', 0);
    formData.append('idsolicitud', idsolicitud);
    formData.append('idproducto', prod);
    formData.append('cantidad', cant);
    formData.append('valor', val);
    formData.append('observacion', obs);

    $.ajax({
        url: '../ajax/detsoladquisicion.php?op=guardaryeditar',
        type: "POST",
        data: formData,
        async: false,
        cache: false,
        timeout: 30000,
        contentType: false,
        processData: false,
        success: (datos) => {
            if (!isNaN(datos) || datos != 0 || datos == "") {
                new PNotify({
                    title: 'Registrado!',
                    text: 'Detalle de Solicitud guardada con exito.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
            } else {
                new PNotify({
                    title: 'Error!',
                    text: 'Hubo un error al registar.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
            limpiardet();
            tabladet.ajax.reload();
        }
    });

}

function del(iddet){
    bootbox.confirm("va a eliminar este detalle, ¿esta seguro?", (resultado) => {
        if(resultado){
            $.ajax({
                url: '../ajax/detsoladquisicion.php?op=eliminar&iddet='+ iddet,
                type: "POST",
                async: false,
                cache: false,
                timeout: 30000,
                contentType: false,
                processData: false,
                success: (datos) => {
                    if (datos != 0) {
                        new PNotify({
                            title: 'Registrado!',
                            text: 'eliminado con exito.',
                            type: 'success',
                            styling: 'bootstrap3'
                        });
                    } else {
                        new PNotify({
                            title: 'Error!',
                            text: 'Hubo un error al eliminar.',
                            type: 'error',
                            styling: 'bootstrap3'
                        });
                    }
                    tabladet.ajax.reload();
                }
            });
        }
    });
    limpiardet();
}
init();

