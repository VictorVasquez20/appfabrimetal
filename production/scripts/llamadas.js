var tabla;
var refdata;
var ctx, mybarChart;
var Gauge, GaugeOption;
var Gaugemes, GaugeOptionmes;


//funcion que se ejecuta iniciando
function init() {
    CargarDatos();
    CargarGraficos();
    CargarDona();
    CargarDonaMes();
    ListarAlerta(false);
    setInterval("Actualizar()", 180000);
}

function ListarAlerta(flag) {

    if (flag) {
        $("#Charts").hide();
    } else {
        $("#Charts").show();
    }

}


function CargarGraficos() {

    $.post("../ajax/asterisk.php?op=DG", function (data, status) {
        data = JSON.parse(data);
        refdata = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

        for (var i = 0; i < data.aaData.length; i++) {
            refdata[data.aaData[i][0] - 1] = data.aaData[i][1];
        }

        ctx = document.getElementById("mybarChart");
        ctx.height = 125;
        mybarChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                datasets: [{
                        label: 'Llamadas',
                        backgroundColor: "#26B99A",
                        data: refdata
                    }]
            },

            options: {
                scales: {
                    yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                }
            }
        });
    });

}

function CargarDona(){

        var theme = {
              color: [
                  '#26B99A', '#34495E', '#BDC3C7', '#3498DB',
                  '#9B59B6', '#8abb6f', '#759c6a', '#bfd3b7'
              ],

              title: {
                  itemGap: 8,
                  textStyle: {
                      fontWeight: 'normal',
                      color: '#408829'
                  }
              },

              dataRange: {
                  color: ['#1f610a', '#97b58d']
              },

              toolbox: {
                  color: ['#408829', '#408829', '#408829', '#408829']
              },

              tooltip: {
                  backgroundColor: 'rgba(0,0,0,0.5)',
                  axisPointer: {
                      type: 'line',
                      lineStyle: {
                          color: '#408829',
                          type: 'dashed'
                      },
                      crossStyle: {
                          color: '#408829'
                      },
                      shadowStyle: {
                          color: 'rgba(200,200,200,0.3)'
                      }
                  }
              },

              dataZoom: {
                  dataBackgroundColor: '#eee',
                  fillerColor: 'rgba(64,136,41,0.2)',
                  handleColor: '#408829'
              },
              grid: {
                  borderWidth: 0
              },

              categoryAxis: {
                  axisLine: {
                      lineStyle: {
                          color: '#408829'
                      }
                  },
                  splitLine: {
                      lineStyle: {
                          color: ['#eee']
                      }
                  }
              },

              valueAxis: {
                  axisLine: {
                      lineStyle: {
                          color: '#408829'
                      }
                  },
                  splitArea: {
                      show: true,
                      areaStyle: {
                          color: ['rgba(250,250,250,0.1)', 'rgba(200,200,200,0.1)']
                      }
                  },
                  splitLine: {
                      lineStyle: {
                          color: ['#eee']
                      }
                  }
              },
              timeline: {
                  lineStyle: {
                      color: '#408829'
                  },
                  controlStyle: {
                      normal: {color: '#408829'},
                      emphasis: {color: '#408829'}
                  }
              },

              k: {
                  itemStyle: {
                      normal: {
                          color: '#68a54a',
                          color0: '#a9cba2',
                          lineStyle: {
                              width: 1,
                              color: '#408829',
                              color0: '#86b379'
                          }
                      }
                  }
              },
              map: {
                  itemStyle: {
                      normal: {
                          areaStyle: {
                              color: '#ddd'
                          },
                          label: {
                              textStyle: {
                                  color: '#c12e34'
                              }
                          }
                      },
                      emphasis: {
                          areaStyle: {
                              color: '#99d2dd'
                          },
                          label: {
                              textStyle: {
                                  color: '#c12e34'
                              }
                          }
                      }
                  }
              },
              force: {
                  itemStyle: {
                      normal: {
                          linkStyle: {
                              strokeColor: '#408829'
                          }
                      }
                  }
              },
              chord: {
                  padding: 4,
                  itemStyle: {
                      normal: {
                          lineStyle: {
                              width: 1,
                              color: 'rgba(128, 128, 128, 0.5)'
                          },
                          chordStyle: {
                              lineStyle: {
                                  width: 1,
                                  color: 'rgba(128, 128, 128, 0.5)'
                              }
                          }
                      },
                      emphasis: {
                          lineStyle: {
                              width: 1,
                              color: 'rgba(128, 128, 128, 0.5)'
                          },
                          chordStyle: {
                              lineStyle: {
                                  width: 1,
                                  color: 'rgba(128, 128, 128, 0.5)'
                              }
                          }
                      }
                  }
              },
              gauge: {
                  startAngle: 225,
                  endAngle: -45,
                  axisLine: {
                      show: true,
                      lineStyle: {
                          color: [[0.2, '#86b379'], [0.8, '#68a54a'], [1, '#408829']],
                          width: 8
                      }
                  },
                  axisTick: {
                      splitNumber: 10,
                      length: 12,
                      lineStyle: {
                          color: 'auto'
                      }
                  },
                  axisLabel: {
                      textStyle: {
                          color: 'auto'
                      }
                  },
                  splitLine: {
                      length: 18,
                      lineStyle: {
                          color: 'auto'
                      }
                  },
                  pointer: {
                      length: '90%',
                      color: 'auto'
                  },
                  title: {
                      textStyle: {
                          color: '#333'
                      }
                  },
                  detail: {
                      textStyle: {
                          color: 'auto'
                      }
                  }
              },
              textStyle: {
                  fontFamily: 'Arial, Verdana, sans-serif'
              }
          };

    
    //Consumimos el servicio DatosDona para traer informacion de las graficas
    $.post("../ajax/asterisk.php?op=DGau", function(data,status){
        data = JSON.parse(data);            
        //Inicializamos el grafico gauge
        Gauge = echarts.init(document.getElementById('echart_gauge'), theme);
        var porefi = parseFloat((data.contestadas.ncontestadas/data.llamados.nllamadas)*100).toFixed(2);
        
        GaugeOption={
            tooltip: {
              formatter: "{a} <br/>{b} : {c}%"
            },
            toolbox: {
              show: true,
              feature: {
                restore: {
                  show: true,
                  title: "Restore"
                },
                saveAsImage: {
                  show: true,
                  title: "Save Image"
                }
              }
            },
            series: [{
              name: 'Eficiencia en el dia',
              type: 'gauge',
              center: ['50%', '50%'],
              startAngle: 140,
              endAngle: -140,
              min: 100,
              max: 0,
              precision: 0,
              splitNumber: 10,
              axisLine: {
                show: true,
                lineStyle: {
                  color: [
                    [0.2, 'lightgreen'],
                    [0.5, 'skyblue'],
                    [0.8, 'orange'],
                    [1, '#ff4500']
                  ],
                  width: 30
                }
              },
              axisTick: {
                show: true,
                splitNumber: 5,
                length: 8,
                lineStyle: {
                  color: '#eee',
                  width: 1,
                  type: 'solid'
                }
              },
              axisLabel: {
                show: true,
                formatter: function(v) {
                  switch (v + '') {
                    case '10':
                      return 'a';
                    case '30':
                      return 'b';
                    case '60':
                      return 'c';
                    case '90':
                      return 'd';
                    default:
                      return '';
                  }
                },
                textStyle: {
                  color: '#333'
                }
              },
              splitLine: {
                show: true,
                length: 30,
                lineStyle: {
                  color: '#eee',
                  width: 2,
                  type: 'solid'
                }
              },
              pointer: {
                length: '80%',
                width: 8,
                color: 'auto'
              },
              title: {
                show: true,
                offsetCenter: ['-65%', -10],
                textStyle: {
                  color: '#333',
                  fontSize: 15
                }
              },
              detail: {
                show: true,
                backgroundColor: 'rgba(0,0,0,0)',
                borderWidth: 0,
                borderColor: '#ccc',
                width: 100,
                height: 40,
                offsetCenter: ['-60%', 10],
                formatter: '{value}%',
                textStyle: {
                  color: 'auto',
                  fontSize: 30
                }
              },
              data: [{
                value: porefi,
                name: 'Eficiencia'
              }]
            }]
          };        
        Gauge.setOption(GaugeOption);
        });       
}


function CargarDonaMes(){

        var theme2 = {
              color: [
                  '#26B99A', '#34495E', '#BDC3C7', '#3498DB',
                  '#9B59B6', '#8abb6f', '#759c6a', '#bfd3b7'
              ],

              title: {
                  itemGap: 8,
                  textStyle: {
                      fontWeight: 'normal',
                      color: '#408829'
                  }
              },

              dataRange: {
                  color: ['#1f610a', '#97b58d']
              },

              toolbox: {
                  color: ['#408829', '#408829', '#408829', '#408829']
              },

              tooltip: {
                  backgroundColor: 'rgba(0,0,0,0.5)',
                  axisPointer: {
                      type: 'line',
                      lineStyle: {
                          color: '#408829',
                          type: 'dashed'
                      },
                      crossStyle: {
                          color: '#408829'
                      },
                      shadowStyle: {
                          color: 'rgba(200,200,200,0.3)'
                      }
                  }
              },

              dataZoom: {
                  dataBackgroundColor: '#eee',
                  fillerColor: 'rgba(64,136,41,0.2)',
                  handleColor: '#408829'
              },
              grid: {
                  borderWidth: 0
              },

              categoryAxis: {
                  axisLine: {
                      lineStyle: {
                          color: '#408829'
                      }
                  },
                  splitLine: {
                      lineStyle: {
                          color: ['#eee']
                      }
                  }
              },

              valueAxis: {
                  axisLine: {
                      lineStyle: {
                          color: '#408829'
                      }
                  },
                  splitArea: {
                      show: true,
                      areaStyle: {
                          color: ['rgba(250,250,250,0.1)', 'rgba(200,200,200,0.1)']
                      }
                  },
                  splitLine: {
                      lineStyle: {
                          color: ['#eee']
                      }
                  }
              },
              timeline: {
                  lineStyle: {
                      color: '#408829'
                  },
                  controlStyle: {
                      normal: {color: '#408829'},
                      emphasis: {color: '#408829'}
                  }
              },

              k: {
                  itemStyle: {
                      normal: {
                          color: '#68a54a',
                          color0: '#a9cba2',
                          lineStyle: {
                              width: 1,
                              color: '#408829',
                              color0: '#86b379'
                          }
                      }
                  }
              },
              map: {
                  itemStyle: {
                      normal: {
                          areaStyle: {
                              color: '#ddd'
                          },
                          label: {
                              textStyle: {
                                  color: '#c12e34'
                              }
                          }
                      },
                      emphasis: {
                          areaStyle: {
                              color: '#99d2dd'
                          },
                          label: {
                              textStyle: {
                                  color: '#c12e34'
                              }
                          }
                      }
                  }
              },
              force: {
                  itemStyle: {
                      normal: {
                          linkStyle: {
                              strokeColor: '#408829'
                          }
                      }
                  }
              },
              chord: {
                  padding: 4,
                  itemStyle: {
                      normal: {
                          lineStyle: {
                              width: 1,
                              color: 'rgba(128, 128, 128, 0.5)'
                          },
                          chordStyle: {
                              lineStyle: {
                                  width: 1,
                                  color: 'rgba(128, 128, 128, 0.5)'
                              }
                          }
                      },
                      emphasis: {
                          lineStyle: {
                              width: 1,
                              color: 'rgba(128, 128, 128, 0.5)'
                          },
                          chordStyle: {
                              lineStyle: {
                                  width: 1,
                                  color: 'rgba(128, 128, 128, 0.5)'
                              }
                          }
                      }
                  }
              },
              gauge: {
                  startAngle: 225,
                  endAngle: -45,
                  axisLine: {
                      show: true,
                      lineStyle: {
                          color: [[0.2, '#86b379'], [0.8, '#68a54a'], [1, '#408829']],
                          width: 8
                      }
                  },
                  axisTick: {
                      splitNumber: 10,
                      length: 12,
                      lineStyle: {
                          color: 'auto'
                      }
                  },
                  axisLabel: {
                      textStyle: {
                          color: 'auto'
                      }
                  },
                  splitLine: {
                      length: 18,
                      lineStyle: {
                          color: 'auto'
                      }
                  },
                  pointer: {
                      length: '90%',
                      color: 'auto'
                  },
                  title: {
                      textStyle: {
                          color: '#333'
                      }
                  },
                  detail: {
                      textStyle: {
                          color: 'auto'
                      }
                  }
              },
              textStyle: {
                  fontFamily: 'Arial, Verdana, sans-serif'
              }
          };

    
    //Consumimos el servicio DatosDona para traer informacion de las graficas
    $.post("../ajax/asterisk.php?op=DGauM", function(data,status){
        data = JSON.parse(data);            
        //Inicializamos el grafico gauge
        Gaugemes = echarts.init(document.getElementById('echart_gaugemes'), theme2);
        var porefimes = parseFloat((data.contestadasmes.ncontestadas/data.llamadosmes.nllamadas)*100).toFixed(2);
        
        GaugeOptionmes={
            tooltip: {
              formatter: "{a} <br/>{b} : {c}%"
            },
            toolbox: {
              show: true,
              feature: {
                restore: {
                  show: true,
                  title: "Restore"
                },
                saveAsImage: {
                  show: true,
                  title: "Save Image"
                }
              }
            },
            series: [{
              name: 'Eficiencia en el Mes',
              type: 'gauge',
              center: ['50%', '50%'],
              startAngle: 140,
              endAngle: -140,
              min: 100,
              max: 0,
              precision: 0,
              splitNumber: 10,
              axisLine: {
                show: true,
                lineStyle: {
                  color: [
                    [0.2, 'lightgreen'],
                    [0.5, 'skyblue'],
                    [0.8, 'orange'],
                    [1, '#ff4500']
                  ],
                  width: 30
                }
              },
              axisTick: {
                show: true,
                splitNumber: 5,
                length: 8,
                lineStyle: {
                  color: '#eee',
                  width: 1,
                  type: 'solid'
                }
              },
              axisLabel: {
                show: true,
                formatter: function(v) {
                  switch (v + '') {
                    case '10':
                      return 'a';
                    case '30':
                      return 'b';
                    case '60':
                      return 'c';
                    case '90':
                      return 'd';
                    default:
                      return '';
                  }
                },
                textStyle: {
                  color: '#333'
                }
              },
              splitLine: {
                show: true,
                length: 30,
                lineStyle: {
                  color: '#eee',
                  width: 2,
                  type: 'solid'
                }
              },
              pointer: {
                length: '80%',
                width: 8,
                color: 'auto'
              },
              title: {
                show: true,
                offsetCenter: ['-65%', -10],
                textStyle: {
                  color: '#333',
                  fontSize: 15
                }
              },
              detail: {
                show: true,
                backgroundColor: 'rgba(0,0,0,0)',
                borderWidth: 0,
                borderColor: '#ccc',
                width: 100,
                height: 40,
                offsetCenter: ['-60%', 10],
                formatter: '{value}%',
                textStyle: {
                  color: 'auto',
                  fontSize: 30
                }
              },
              data: [{
                value: porefimes,
                name: '%'
              }]
            }]
          };        
        Gaugemes.setOption(GaugeOptionmes);
        });       
}


function ActializarGraficos() {

    $.post("../ajax/asterisk.php?op=DG", function (data, status) {
        data = JSON.parse(data);

        var updata = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        for (var i = 0; i < data.aaData.length; i++) {
            updata[data.aaData[i][0] - 1] = data.aaData[i][1];
        }

        for (var i = 0; i < updata.length; i++) {
            if (refdata[i] != updata[i]) {
                refdata[i] = updata[i];
                console.log("Consiguio diferencia");
                console.log(refdata);
                mybarChart.data.datasets[0]['data']=refdata;
                mybarChart.update();
            }

        }
    });
}

function ActializarDona() {

    $.post("../ajax/asterisk.php?op=DGau", function (data, status) {
        data = JSON.parse(data);
        porefi = parseFloat((data.contestadas.ncontestadas/data.llamados.nllamadas)*100).toFixed(2);      
        GaugeOption.series[0].data[0].value = porefi;
        Gauge.setOption(GaugeOption, true);
        
    });
}

function ActializarDonaMes() {

    $.post("../ajax/asterisk.php?op=DGauM", function (data, status) {
        data = JSON.parse(data);
        porefimes = parseFloat((data.contestadasmes.ncontestadas/data.llamadosmes.nllamadas)*100).toFixed(2);      
        GaugeOptionmes.series[0].data[0].value = porefimes;
        Gaugemes.setOption(GaugeOptionmes, true);
        
    });
}




function CargarDatos() {

    $.post("../ajax/asterisk.php?op=DL", function (data, status) {
        data = JSON.parse(data);
        //Actualizamos valores
        $("#num_llamadas").html(data.llamados.nllamadas);
        $("#num_contestadas").html(data.contestadas.ncontestadas);
        $("#num_nocontestadas").html(data.nocontestadas.nocontestadas);
        var indice = parseFloat((data.contestadas.ncontestadas / data.llamados.nllamadas ) * 100).toFixed(0);
        $("#indice").html(indice + " %");
        var promcontestar = parseFloat((data.tcontestar.total - data.tcontestar.conversacion) / data.llamados.nllamadas).toFixed(2);
        $("#promcontestar").html(promcontestar + " Seg");
        var promconversacion = parseFloat(data.tconversacion.conversacion / data.contestadas.ncontestadas).toFixed(2);
        $("#promllamada").html(promconversacion + " Seg");
    });
    
    
    $.post("../ajax/asterisk.php?op=DLM", function (data, status) {
        data = JSON.parse(data);
        //Actualizamos valores
        $("#num_llamadasmes").html(data.llamadosmes.nllamadas);
        $("#num_contestadasmes").html(data.contestadasmes.ncontestadas);
        $("#num_nocontestadasmes").html(data.nocontestadasmes.nocontestadas);
        var indicemes = parseFloat((data.contestadasmes.ncontestadas / data.llamadosmes.nllamadas ) * 100).toFixed(0);
        $("#indicemes").html(indicemes + " %");
        var promcontestarmes = parseFloat((data.tcontestarmes.total - data.tcontestarmes.conversacion) / data.llamadosmes.nllamadas).toFixed(2);
        $("#promcontestarmes").html(promcontestarmes + " Seg");
        var promconversacionmes = parseFloat(data.tconversacionmes.conversacion / data.contestadasmes.ncontestadas).toFixed(2);
        $("#promllamadames").html(promconversacionmes + " Seg");
    });
}

function Actualizar() {
    CargarDatos();
    ActializarGraficos();
    ActializarDona();
    ActializarDonaMes();
}

init();