var tabla,tabclientes,tabedificios,tabascensores;
var htmlre, htmlseg, htmltcli, htmltipo, htmlmarca;

//funcion que se ejecuta iniciando
function init(){
	mostarform(false);
	listar();

	$('[data-toggle="tooltip"]').tooltip(); 

	$('#formulario').on("submit", function(event){
		event.preventDefault();
		guardaryeditar();
	});
        
        $('#formulariocontrato').on("submit", function(event){
		event.preventDefault();
		editarbd();
	});
        
        
       
	$.post("../ajax/tcontrato.php?op=selecttcontrato", function(r){
		$("#tcontrato").html(r);
		$("#tcontrato").selectpicker('refresh');
	});

	$.post("../ajax/periocidad.php?op=selectperiocidad", function(r){
		$("#idperiocidad").html(r);
		$("#idperiocidad").selectpicker('refresh');
	});

	$.post("../ajax/regiones.php?op=selectRegiones", function(r){
		htmlre = r;
	});

	$.post("../ajax/tsegmento.php?op=selecttsegmento", function(r){
		htmlseg=r;
	});

	$.post("../ajax/tcliente.php?op=selecttcliente", function(r){
		htmltcli=r;
	});

	$.post("../ajax/tascensor.php?op=selecttascensor", function(r){
		htmltipo=r;
	});

	$.post("../ajax/marca.php?op=selectmarca", function(r){
		htmlmarca=r;
	});


	$("#nclientes").on("change", function(e){
		$("#clientes").empty();
		for (var i = 0; i < $("#nclientes").val() ; i++) {
			var myvar = '<h4>Cliente: '+(i+1)+'</h4>'+
                                        '<div class="row" id="cliente'+i+'">'+
                                            '<div class="col-md-4 col-sm-12 col-xs-12 form-group">'+
                                                '<label for="tcliente'+i+'">Tipo de cliente <span class="required">*</span></label>'+
                                                '<select class="form-control selectpicker" data-live-search="true" id="tcliente'+i+'" name="tcliente'+i+'" required="required"></select>'+
                                            '</div>'+
                                            '<div class="col-md-4 col-sm-12 col-xs-12 form-group">'+
                                                '<label for="rut'+i+'">RUT <span class="required">*</span></label>'+
                                                '<input type="text" id="rut'+i+'" name="rut'+i+'" required="required" class="form-control">'+
                                            '</div>'+
                                            '<div class="col-md-4 col-sm-12 col-xs-12 form-group">'+
                                                '<label for="razon_social'+i+'">Razon Social <span class="required">*</span></label>'+
                                                '<input type="text" id="razon_social'+i+'" name="razon_social'+i+'" required="required" class="form-control">'+
                                            '</div>'+
                                            '<div class="col-md-4 col-sm-12 col-xs-12 form-group">'+
                                                '<label for="calle'+i+'">Calle o Avenida <span class="required">*</span></label>'+
                                                '<input type="text" id="calle'+i+'" name="calle'+i+'" required="required" class="form-control">'+
                                            '</div>'+
                                            '<div class="col-md-4 col-sm-12 col-xs-12 form-group">'+
                                                '<label for="numero'+i+'">Numero <span class="required">*</span></label>'+
                                                '<input type="text" id="numero'+i+'" name="numero'+i+'" required="required" class="form-control">'+
                                            '</div>'+
                                            '<div class="col-md-2 col-sm-12 col-xs-12 form-group">'+
                                                '<label for="oficina'+i+'">Oficina </label>'+
                                                '<input type="text" id="oficina'+i+'" name="oficina'+i+'" class="form-control">'+
                                            '</div>'+
                                            '<div class="col-md-2 col-sm-12 col-xs-12 form-group">'+
                                                '<label for="ncon_cli'+i+'">Contactos </label>'+
                                                '<select class="form-control selectpicker" data-live-search="false" id="ncon_cli'+i+'" name="ncon_cli'+i+'" onchange="addformconcli('+i+')" required="required">'+
                                                    '<option value="" selected disabled>N° Contactos cliente</option>'+
                                                    '<option value="1">1</option>'+
                                                    '<option value="2">2</option>'+
                                                    '<option value="3">3</option>'+
                                                    '<option value="4">4</option>'+
                                                '</select>'+
                                            '</div>'+
                                            '<div class="col-md-4 col-sm-12 col-xs-12 form-group">'+
                                                '<label for="idregiones'+i+'">Region <span class="required">*</span></label>'+
                                                '<select class="form-control selectpicker" data-live-search="true" id="idregiones'+i+'" name="idregiones'+i+'" required="required" onchange="selectpro(1,'+i+')"></select>'+
                                            '</div>'+
                                            '<div class="col-md-4 col-sm-12 col-xs-12 form-group">'+
                                                '<label for="idprovincias'+i+'">Provincia <span class="required">*</span></label>'+
                                                '<select class="form-control selectpicker" data-live-search="true" id="idprovincias'+i+'" name="idprovincias'+i+'" required="required" onchange="selectcom(1,'+i+')"></select>'+
                                            '</div>'+
                                            '<div class="col-md-4 col-sm-12 col-xs-12 form-group">'+
                                                '<label for="idcomunas'+i+'">Comuna <span class="required">*</span></label>'+
                                                '<select class="form-control selectpicker" data-live-search="true" id="idcomunas'+i+'" name="idcomunas'+i+'" required="required"></select>'+
                                            '</div>'+
                                            '<div id="contactocli'+i+'" name="contactocli'+i+'">'+
                                            '</div>'+
                                        '</div>';
                             
	
			$("#clientes").append(myvar);
                        
                        $('#ncon_cli'+i+'').selectpicker({
  				liveSearch: false
			});

			$('#tcliente'+i+'').selectpicker({
  				liveSearch: false
			});
                        
			$('#tcliente'+i+'').html(htmltcli);
			$('#tcliente'+i+'').selectpicker('refresh');

			$('#idregiones'+i+'').selectpicker({
  				liveSearch: true
			});
			$('#idregiones'+i+'').html(htmlre);
			$('#idregiones'+i+'').selectpicker('refresh');

			$('#idprovincias'+i+'').selectpicker({
  				liveSearch: true
			});

			$('#idcomunas'+i+'').selectpicker({
  				liveSearch: true
			});
		}

		$("#formclientes").show();
	});


	$("#nedificios").on("change", function(e){
		$("#edificios").empty();
		for (var i = 0; i < $("#nedificios").val() ; i++) {
			var myvar = '<h4>Edificio: '+(i+1)+'</h4>'+
						'<div class="row" id="edificio'+i+'">'+
						'<div class="col-md-3 col-sm-12 col-xs-12 form-group">'+
						'<label for="idtsegmento'+i+'">Segmento <span class="required">*</span></label>'+
						'<select class="form-control selectpicker" data-live-search="true" id="idtsegmento'+i+'" name="idtsegmento'+i+'" required="required"></select>'+
						'</div>'+
						'<div class="col-md-3 col-sm-12 col-xs-12 form-group">'+
						'<label for="nombre'+i+'">Nombre de edificio <span class="required">*</span></label>'+
						'<input type="text" id="nombre'+i+'" name="nombre'+i+'" required="required" class="form-control">'+
						'</div>'+
						'<div class="col-md-3 col-sm-12 col-xs-12 form-group">'+
						'<label for="calle_ed'+i+'">Calle o Avenida <span class="required">*</span></label>'+
						'<input type="text" id="calle_ed'+i+'" name="calle_ed'+i+'" required="required" class="form-control">'+
						'</div>'+
						'<div class="col-md-3 col-sm-12 col-xs-12 form-group">'+
						'<label for="numero_ed'+i+'">Numero <span class="required">*</span></label>'+
						'<input type="text" id="numero_ed'+i+'" name="numero_ed'+i+'" required="required" class="form-control">'+
						'</div>'+
						'<div class="col-md-4 col-sm-12 col-xs-12 form-group">'+
						'<label for="idregiones_ed'+i+'">Region <span class="required">*</span></label>'+
						'<select class="form-control selectpicker" data-live-search="true" id="idregiones_ed'+i+'" name="idregiones_ed'+i+'" required="required" onchange="selectpro(2,'+i+')"></select>'+
						'</div>'+
						'<div class="col-md-4 col-sm-12 col-xs-12 form-group">'+
						'<label for="idprovincias_ed'+i+'">Provincia <span class="required">*</span></label>'+
						'<select class="form-control selectpicker" data-live-search="true" id="idprovincias_ed'+i+'" name="idprovincias_ed'+i+'" required="required" onchange="selectcom(2,'+i+')"></select>'+
						'</div>'+
						'<div class="col-md-4 col-sm-12 col-xs-12 form-group">'+
						'<label for="idcomunas_ed'+i+'">Comuna <span class="required">*</span></label>'+
						'<select class="form-control selectpicker" data-live-search="true" id="idcomunas_ed'+i+'" name="idcomunas_ed'+i+'" required="required"></select>'+
						'</div>'+
                                                '<div class="col-md-3 col-sm-12 col-xs-12 form-group">'+
                                                '<label for="corcorreo'+i+'">Coordinacion por correo </label>'+
                                                '<select class="form-control selectpicker" data-live-search="false" id="corcorreo'+i+'" name="corcorreo'+i+'" onchange="addformconedi('+i+')" required="required">'+
                                                    '<option value="" selected disabled>Correo coordinacion</option>'+
                                                    '<option value="1">SI</option>'+
                                                    '<option value="0">NO</option>'+
                                                '</select>'+
                                                '</div>'+
                                                '<div class="col-md-3 col-sm-12 col-xs-12 form-group">'+
                                                '<label for="residente'+i+'">Tecnico residente</label>'+
                                                '<select class="form-control selectpicker" data-live-search="false" id="residente'+i+'" name="residente'+i+'" onchange="addformconedi('+i+')" required="required">'+
                                                    '<option value="" selected disabled>Residente</option>'+
                                                    '<option value="1">SI</option>'+
                                                    '<option value="0">NO</option>'+
                                                '</select>'+
                                                '</div>'+
                                                '<div class="col-md-3 col-sm-12 col-xs-12 form-group">'+
                                                '<label for="ncon_edi'+i+'">Contactos </label>'+
                                                '<select class="form-control selectpicker" data-live-search="false" id="ncon_edi'+i+'" name="ncon_edi'+i+'" onchange="addformconedi('+i+')" required="required">'+
                                                    '<option value="" selected disabled>N° Contactos edificio</option>'+
                                                    '<option value="1">1</option>'+
                                                    '<option value="2">2</option>'+
                                                    '<option value="3">3</option>'+
                                                    '<option value="4">4</option>'+
                                                '</select>'+
                                                '</div>'+
                                                '<div class="col-md-3 col-sm-12 col-xs-12 form-group">'+
                                                '<label for="nascensores'+i+'">Ascensores </label>'+
                                                '<select class="form-control selectpicker" data-live-search="false" id="nascensores'+i+'" name="nascensores'+i+'" onchange="addformasc('+i+')" required="required">'+
                                                    '<option value="" selected disabled>N° Ascensores</option>'+
                                                    '<option value="1">1</option>'+
                                                    '<option value="2">2</option>'+
                                                    '<option value="3">3</option>'+
                                                    '<option value="4">4</option>'+
                                                    '<option value="5">5</option>'+
                                                    '<option value="6">6</option>'+
                                                    '<option value="7">7</option>'+
                                                    '<option value="8">8</option>'+
                                                    '<option value="9">9</option>'+
                                                    '<option value="10">10</option>'+
                                                    '<option value="11">11</option>'+
                                                    '<option value="12">12</option>'+
                                                    '<option value="13">13</option>'+
                                                    '<option value="14">14</option>'+
                                                    '<option value="15">15</option>'+
                                                    '<option value="16">16</option>'+
                                                    '<option value="17">17</option>'+
                                                    '<option value="18">18</option>'+
                                                    '<option value="19">19</option>'+
                                                    '<option value="20">20</option>'+
                                                    '<option value="21">21</option>'+
                                                    '<option value="22">22</option>'+
                                                    '<option value="23">23</option>'+
                                                    '<option value="24">24</option>'+
                                                '</select>'+
                                                '</div>'+
                                                '<div id="contactoedi'+i+'" name="contactoedi'+i+'">'+
						'</div>'+
						'<div id="ascensores'+i+'" name="ascensores'+i+'">'+
						'</div>'+
						'</div>';

	
			$("#edificios").append(myvar);
                        
                        $('#corcorreo'+i+'').selectpicker({
  				liveSearch: false
			});
                        
                        $('#residente'+i+'').selectpicker({
  				liveSearch: false
			});
                        
                        $('#ncon_edi'+i+'').selectpicker({
  				liveSearch: false
			});
                        
                        $('#nascensores'+i+'').selectpicker({
  				liveSearch: false
			});
                        
			$('#idtsegmento'+i+'').selectpicker({
  				liveSearch: false
			});
			$('#idtsegmento'+i+'').html(htmlseg);
			$('#idtsegmento'+i+'').selectpicker('refresh');

			$('#idregiones_ed'+i+'').selectpicker({
  				liveSearch: true
			});
			$('#idregiones_ed'+i+'').html(htmlre);
			$('#idregiones_ed'+i+'').selectpicker('refresh');

			$('#idprovincias_ed'+i+'').selectpicker({
  				liveSearch: true
			});

			$('#idcomunas_ed'+i+'').selectpicker({
  				liveSearch: true
			});
		}

		$("#formedificios").show();
	});
	
}


// Otras funciones
function limpiar(){
	
}

function mostarform(flag){
	limpiar();
	if(flag){
                $("#formeditarcontrato").hide();
		$("#tabcontrato").hide();
		$("#listadocontratos").hide();
		$("#formulariocontratos").show();
		$("#formclientes").hide();
		$("#formedificios").hide();
		$("#op_agregar").hide();
		$("#op_listar").show();
                $("#btnGuardar").prop("disabled", false);
                
	}else{
                $("#formeditarcontrato").hide();
		$("#tabcontrato").hide();
                $("#formulariocontratos").hide();
		$("#listadocontratos").show();
		$("#op_agregar").show();
		$("#op_listar").hide();
	}

}


function cancelarform(){
	mostarform(false);
}

function selectpro(tipo, numero){
	if(tipo == 1){
		$.get("../ajax/provincia.php?op=selectProvincia",{id:$('#idregiones'+numero+'').val()}, function(r){
			$('#idprovincias'+numero+'').html(r);
			$('#idprovincias'+numero+'').selectpicker('refresh');
		});
	}else{
		$.get("../ajax/provincia.php?op=selectProvincia",{id:$('#idregiones_ed'+numero+'').val()}, function(r){
			$('#idprovincias_ed'+numero+'').html(r);
			$('#idprovincias_ed'+numero+'').selectpicker('refresh');
		});
	}
}

function selectcom(tipo, numero){
	if(tipo == 1){

		$.get("../ajax/comunas.php?op=selectComunas",{id:$('#idprovincias'+numero+'').val()}, function(r){
			$('#idcomunas'+numero+'').html(r);
			$('#idcomunas'+numero+'').selectpicker('refresh');
		});
	}else{
		$.get("../ajax/comunas.php?op=selectComunas",{id:$('#idprovincias_ed'+numero+'').val()}, function(r){
			$('#idcomunas_ed'+numero+'').html(r);
			$('#idcomunas_ed'+numero+'').selectpicker('refresh');
		});
	}
}

function selectmodelo(edificio, ascensor){

		$.get("../ajax/modelo.php?op=selectmodelo",{id:$('#marca'+edificio+''+ascensor+'').val()}, function(r){
			$('#modelo'+edificio+''+ascensor+'').html(r);
			$('#modelo'+edificio+''+ascensor+'').selectpicker('refresh');
		});
	
}


function calCLP(edificio, ascensor){

		$.get("http://api.sbif.cl/api-sbifv3/recursos_api/uf?apikey=a870de8650ee0b4b00c4847a76e8af89a44fce67&formato=json", function(r){
			var vauf = r.UFs[0].Valor.replace(".","");
			var vauf = vauf.replace(",",".");
			var clp = (parseFloat($('#valoruf'+edificio+''+ascensor+'').val()))*(vauf);
			console.log(clp);
			$('#valorclp'+edificio+''+ascensor+'').val(clp.toFixed(2));
			
		});
	
}



function addformasc(numero){
		console.log("Agregando Formulario");
		$('#ascensores'+numero+'').empty();
		for (var i = 0; i < $('#nascensores'+numero+'').val() ; i++) {
			var myvar = '<div id="ascensor'+numero+''+i+'" name="ascensor'+numero+''+i+'">'+
						'<h5>Ascensor: '+(i+1)+' - Edificio: '+(numero+1)+'</h5>'+
						'<div class="col-md-3 col-sm-12 col-xs-12 form-group">'+
						'<label for="idtascensor'+numero+''+i+'">Tipo de Ascensor <span class="required">*</span></label>'+
						'<select class="form-control selectpicker" data-live-search="true" id="idtascensor'+numero+''+i+'" name="idtascensor'+numero+''+i+'" required="required"></select>'+
						'</div>'+
						'<div class="col-md-3 col-sm-12 col-xs-12 form-group">'+
						'<label for="marca'+numero+''+i+'">Marca <span class="required">*</span></label>'+
						'<select class="form-control selectpicker" data-live-search="true" id="marca'+numero+''+i+'" name="marca'+numero+''+i+'" required="required" onchange="selectmodelo('+numero+','+i+')">'+
						'</select>'+
						'</div>'+
						'<div class="col-md-3 col-sm-12 col-xs-12 form-group">'+
						'<label for="modelo'+numero+''+i+'">Modelo <span class="required">*</span></label>'+
						'<select class="form-control selectpicker" data-live-search="true" id="modelo'+numero+''+i+'" name="modelo'+numero+''+i+'" required="required">'+
						'</select>'+
						'</div>'+
                                                '<div class="col-md-3 col-sm-12 col-xs-12 form-group">'+
						'<label for="ken'+numero+''+i+'">Identificacion fabricante</label>'+
						'<input type="text" id="ken'+numero+''+i+'" name="ken'+numero+''+i+'" class="form-control">'+
						'</div>'+
                                                '<div class="col-md-3 col-sm-12 col-xs-12 form-group">'+
						'<label for="pservicio'+numero+''+i+'">Puesta Servicio</label>'+
						'<input type="date" id="pservicio'+numero+''+i+'" name="pservicio'+numero+''+i+'" class="form-control"/>'+
						'</div>'+
                                                '<div class="col-md-3 col-sm-12 col-xs-12 form-group">'+
						'<label for="gtecnica'+numero+''+i+'">Garantia Tecnica</label>'+
						'<input type="date" id="gtecnica'+numero+''+i+'" name="gtecnica'+numero+''+i+'" class="form-control"/>'+
						'</div>'+
						'<div class="col-md-2 col-sm-12 col-xs-12 form-group">'+
						'<label for="valoruf'+numero+''+i+'">Valor UF <span class="required">*</span></label>'+
						'<input type="text" id="valoruf'+numero+''+i+'" name="valoruf'+numero+''+i+'" required="required" class="form-control" onchange="calCLP('+numero+','+i+')">'+
						'</div>'+
						'<div class="col-md-2 col-sm-12 col-xs-12 form-group">'+
						'<label for="valorclp'+numero+''+i+'">Valor CLP <span class="required">*</span></label>'+
						'<input type="text" id="valorclp'+numero+''+i+'" name="valorclp'+numero+''+i+'" required="required" class="form-control">'+
						'</div>'+
                                                '<div class="col-md-2 col-sm-12 col-xs-12 form-group">'+
						'<label for="paradas'+numero+''+i+'">Paradas</label>'+
						'<input type="text" id="paradas'+numero+''+i+'" name="paradas'+numero+''+i+'" class="form-control">'+
						'</div>'+
                                                '<div class="col-md-3 col-sm-12 col-xs-12 form-group">'+
						'<label for="capkg'+numero+''+i+'">Capacidad (Kg)</label>'+
						'<input type="text" id="capkg'+numero+''+i+'" name="capkg'+numero+''+i+'" class="form-control">'+
						'</div>'+
                                                '<div class="col-md-3 col-sm-12 col-xs-12 form-group">'+
						'<label for="capper'+numero+''+i+'">Capacidad Personas </label>'+
						'<input type="text" id="capper'+numero+''+i+'" name="capper'+numero+''+i+'" class="form-control">'+
						'</div>'+
                                                '<div class="col-md-2 col-sm-12 col-xs-12 form-group">'+
						'<label for="velocidad'+numero+''+i+'">Velocidad </label>'+
						'<input type="text" id="velocidad'+numero+''+i+'" name="velocidad'+numero+''+i+'" class="form-control">'+
						'</div>'+
                                                '<div class="col-md-2 col-sm-12 col-xs-12 form-group">'+
						'<label for="dcs'+numero+''+i+'">Dcs <span class="required">*</span></label>'+
						'<select class="form-control selectpicker" data-live-search="true" id="dcs'+numero+''+i+'" name="dcs'+numero+''+i+'" required="required">'+
                                                    '<option value="" selected disabled>Dcs</option>'+
                                                    '<option value="1">SI</option>'+
                                                    '<option value="0">NO</option>'+
						'</select>'+
						'</div>'+
                                                '<div class="col-md-2 col-sm-12 col-xs-12 form-group">'+
						'<label for="elink'+numero+''+i+'">Elink <span class="required">*</span></label>'+
						'<select class="form-control selectpicker" data-live-search="true" id="elink'+numero+''+i+'" name="elink'+numero+''+i+'" required="required">'+
                                                    '<option value="" selected disabled>Elink</option>'+
                                                    '<option value="1">SI</option>'+
                                                    '<option value="0">NO</option>'+                                                
						'</select>'+
						'</div>'+
						'</div>';

			$('#ascensores'+numero+'').append(myvar);
			
                        $('#dcs'+numero+''+i+'').selectpicker({
  				liveSearch: false
			});
                        
                        $('#elink'+numero+''+i+'').selectpicker({
  				liveSearch: false
			});
                        
			$('#idtascensor'+numero+''+i+'').selectpicker({
  				liveSearch: false
			});
			$('#idtascensor'+numero+''+i+'').html(htmltipo);
			$('#idtascensor'+numero+''+i+'').selectpicker('refresh');

			$('#marca'+numero+''+i+'').selectpicker({
  				liveSearch: false
			});
			$('#marca'+numero+''+i+'').html(htmlmarca);
			$('#marca'+numero+''+i+'').selectpicker('refresh');

			$('#modelo'+numero+''+i+'').selectpicker({
  				liveSearch: true
			});

		}
}


function addformconcli(numero){
		console.log("Agregando Formulario cliente");
		$('#contactocli'+numero+'').empty();
		for (var i = 0; i < $('#ncon_cli'+numero+'').val() ; i++) {
                        console.log("Agregando Formulario cliente "+i);
			var formclicon =    '<div id="contacto'+numero+''+i+'" name="contacto'+numero+''+i+'">'+
						'<h5>Contacto: '+(i+1)+' - Edificio: '+(numero+1)+'</h5>'+
						'<div class="col-md-3 col-sm-12 col-xs-12 form-group">'+
						'<label for="tipocon'+numero+''+i+'">Tipo de contacto <span class="required">*</span></label>'+
						'<select class="form-control selectpicker" data-live-search="true" id="tipocon'+numero+''+i+'" name="tipocon'+numero+''+i+'" required="required">'+
                                                '<option value="Comercial">Comercial</option>'+
                                                '<option value="Cobranza">Cobranza</option>'+
                                                '</select>'+
						'</div>'+
						'<div class="col-md-3 col-sm-12 col-xs-12 form-group">'+
						'<label for="nombre_concli'+numero+''+i+'">Nombre y apellido<span class="required">*</span></label>'+
						'<input type="text" id="nombre_concli'+numero+''+i+'" name="nombre_concli'+numero+''+i+'" required="required" class="form-control">'+
						'</div>'+
						'<div class="col-md-3 col-sm-12 col-xs-12 form-group">'+
						'<label for="numero_concli'+numero+''+i+'">Numero de contacto<span class="required">*</span></label>'+
						'<input type="text" id="numero_concli'+numero+''+i+'" name="numero_concli'+numero+''+i+'" required="required" class="form-control">'+
						'</div>'+
						'<div class="col-md-3 col-sm-12 col-xs-12 form-group">'+
						'<label for="email_concli'+numero+''+i+'">Email<span class="required">*</span></label>'+
						'<input type="text" id="email_concli'+numero+''+i+'" name="email_concli'+numero+''+i+'" required="required" class="form-control">'+
						'</div>'+
                                            '</div>';

			$('#contactocli'+numero+'').append(formclicon);
                        
                        $('#idtcontacto'+numero+''+i+'').selectpicker({
  				liveSearch: false
			});
                        
                        $('#tipocon'+numero+''+i+'').selectpicker({
  				liveSearch: false
			});
		}
}



function addformconedi(numero){
		console.log("Agregando Formulario edificio");
		$('#contactoedi'+numero+'').empty();
		for (var i = 0; i < $('#ncon_edi'+numero+'').val() ; i++) {
                        console.log("Agregando Formulario edificio "+i);
			var formedicon =    '<div id="contactoedi'+numero+''+i+'" name="contactoedi'+numero+''+i+'">'+
						'<h5>Contacto: '+(i+1)+' - Edificio: '+(numero+1)+'</h5>'+
						'<div class="col-md-4 col-sm-12 col-xs-12 form-group">'+
						'<label for="nombre_conedi'+numero+''+i+'">Nombre y apellido<span class="required">*</span></label>'+
						'<input type="text" id="nombre_conedi'+numero+''+i+'" name="nombre_conedi'+numero+''+i+'" required="required" class="form-control">'+
						'</div>'+
						'<div class="col-md-4 col-sm-12 col-xs-12 form-group">'+
						'<label for="numero_conedi'+numero+''+i+'">Numero de contacto<span class="required">*</span></label>'+
						'<input type="text" id="numero_conedi'+numero+''+i+'" name="numero_conedi'+numero+''+i+'" required="required" class="form-control">'+
						'</div>'+
						'<div class="col-md-4 col-sm-12 col-xs-12 form-group">'+
						'<label for="email_conedi'+numero+''+i+'">Email<span class="required">*</span></label>'+
						'<input type="text" id="email_conedi'+numero+''+i+'" name="email_conedi'+numero+''+i+'" required="required" class="form-control">'+
						'</div>'+
                                            '</div>';

			$('#contactoedi'+numero+'').append(formedicon);
                        
		}
}




function listar(){
	tabla=$('#tblcontratos').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons:[
			'copyHtml5',
			'print',
			'excelHtml5',
			'csvHtml5',
			'pdf'
		],
		"ajax":{
			url:'../ajax/contrato.php?op=listarcontrato',
			type:"get",
			dataType:"json",
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 15, //Paginacion 10 items
		"order" : [[1 , "desc"]] //Ordenar en base a la columna 0 descendente
	}).DataTable();
}


function editarbd(){
	$("#btnGuardar").prop("disabled", true);
	var formData = new FormData($("#formulariocontrato")[0]);
	$.ajax({
		url:'../ajax/contrato.php?op=editar',
		type:"POST",
		data:formData,
		contentType: false,
		processData:false,

		success: function(datos){
			bootbox.alert(datos);
			mostarform(false);
			tabla.ajax.reload();
		}
	});
	limpiar();
}

function guardaryeditar(){
	$("#btnGuardar").prop("disabled", true);
	var formData = new FormData($("#formulario")[0]);
	$.ajax({
		url:'../ajax/cliente.php?op=guardaryeditar',
		type:"POST",
		data:formData,
		contentType: false,
		processData:false,

		success: function(datos){
			bootbox.alert(datos);
			mostarform(false);
			tabla.ajax.reload();
		}
	});
	limpiar();
}

function mostar(idcontrato){
        $("#formeditarcontrato").hide();
        $("#listadocontratos").hide();
        $("#formulariocontratos").hide();
        $("#op_agregar").show();
        $("#op_listar").show();
        $("#tabcontrato").show();
                
        $.post("../ajax/contrato.php?op=mostrar_contrato",{idcontrato:idcontrato}, function(r){
            data = JSON.parse(r);
            $("#tabncontrato").html("Contrato N°: "+data.dcontrato.ncontrato);
            $("#tabdireccion").html(data.dcontrato.calle+" "+data.dcontrato.numero);
            $("#tabregion").html(data.dcontrato.region);
            $("#tabprovincia").html(data.dcontrato.provincia);
            $("#tabcomuna").html(data.dcontrato.comuna);
            $("#tabtcontrato").html(data.dcontrato.tipo);
            $("#tabnexterno").html(data.dcontrato.nexterno);
            $("#tabnreferencia").html(data.dcontrato.nreferencia);
            $("#tabubicacion").html(data.dcontrato.ubicacion); 
            $("#tabperiocidad").html(data.dcontrato.periocidad);
            $("#tabfirma").html(data.dcontrato.firma);
            $("#tabinicio").html(data.dcontrato.inicio);
            $("#tabfin").html(data.dcontrato.fin);
            $("#tabobservaciones").html(data.dcontrato.observaciones);         
            $("#tabnrazon").html(data.nclientes.nclientes);
            $("#tabnedificios").html(data.nedificios.nedificios);
            $("#tabnascensores").html(data.nascensores.nascensores);
            $("#tabncentros").html(data.ncentros.ncentros);
            
	});
        
        tabclientes=$('#tbltabclientes').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons:[
			'copyHtml5',
			'print',
			'excelHtml5',
			'csvHtml5',
			'pdf'
		],
		"ajax":{
			url:'../ajax/contrato.php?op=clientes_contrato',
			type:"POST",                       
			dataType:"json",
                        data:{idcontrato:idcontrato},
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 5, //Paginacion 10 items
		"order" : [[1 , "desc"]] //Ordenar en base a la columna 0 descendente
	}).DataTable();
        
        tabedificios=$('#tbltabedificios').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons:[
			'copyHtml5',
			'print',
			'excelHtml5',
			'csvHtml5',
			'pdf'
		],
		"ajax":{
			url:'../ajax/contrato.php?op=edificio_contrato',
			type:"POST",                       
			dataType:"json",
                        data:{idcontrato:idcontrato},
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 5, //Paginacion 10 items
		"order" : [[1 , "desc"]] //Ordenar en base a la columna 0 descendente
	}).DataTable();
        
        tabascensores=$('#tbltabascensores').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons:[
			'copyHtml5',
			'print',
			'excelHtml5',
			'csvHtml5',
			'pdf'
		],
		"ajax":{
			url:'../ajax/contrato.php?op=ascensor_contrato',
			type:"POST",                       
			dataType:"json",
                        data:{idcontrato:idcontrato},
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 5, //Paginacion 10 items
		"order" : [[1 , "desc"]] //Ordenar en base a la columna 0 descendente
	}).DataTable();

        $.post("../ajax/contrato.php?op=centrosc_contrato",{idcontrato:idcontrato}, function(data){
            $("#tabcentroscosto").html(data);
	});
}

function editar(idcontrato){
    $.post("../ajax/tcontrato.php?op=selecttcontrato", function(r){
                    $("#ftcontrato").html(r);
                    $("#ftcontrato").selectpicker('refresh');
                });

    $.post("../ajax/periocidad.php?op=selectperiocidad", function(r){
            $("#fidperiocidad").html(r);
            $("#fidperiocidad").selectpicker('refresh');
    });
    
    $.post("../ajax/regiones.php?op=selectRegiones", function(r){
		$("#fidregiones").html(r);
		$("#fidregiones").selectpicker('refresh');
	});
    
    $.post("../ajax/contrato.php?op=formeditar",{idcontrato:idcontrato}, function(data,status){
                $("#listadocontratos").hide();
                $("#formulariocontratos").hide();
                $("#tabcontrato").hide();               
                $("#op_agregar").show();
                $("#op_listar").show();
                $("#formeditarcontrato").show();

                data = JSON.parse(data);
		$("#fidcontrato").val(data.idcontrato);	
                $("#ftcontrato").val(data.tcontrato);            
		$("#ftcontrato").selectpicker('refresh');                
		$("#fncontrato").val(data.ncontrato);
                $("#fnexterno").val(data.nexterno);
		$("#fnreferencia").val(data.nreferencia);
		$("#fubicacion").val(data.ubicacion);
                $("#fidperiocidad").val(data.idperiocidad);
                $("#fcalle").val(data.calle);
		$("#fnumero").val(data.numero);
                $("#foficina").val(data.oficina);
                $("#ffecha").val(data.fecha);
                $("#ffecha_ini").val(data.fecha_ini);
                $("#ffecha_fin").val(data.fecha_fin);
                $("#fobservaciones").val(data.observaciones);
                $("#fidregiones").val(data.idregiones);                
		$("#fidregiones").selectpicker('refresh');
		$.get("../ajax/provincia.php?op=selectProvincia",{id:data.idregiones}, function(r){
			$("#fidprovincias").html(r);
			$("#fidprovincias").val(data.idprovincia);
			$("#fidprovincias").selectpicker('refresh');
		});
		$.get("../ajax/comunas.php?op=selectComunas",{id:data.idprovincia}, function(r){
			$("#fidcomunas").html(r);
			$("#fidcomunas").val(data.idcomuna);
			$("#fidcomunas").selectpicker('refresh');
		});
                 
	});
}


/*
function desactivar(idequipo){

	bootbox.confirm("Esta seguro que quiere inhabilitar el avion?", function(result){
		if(result){
			$.post("../ajax/equipo.php?op=desactivar",{idequipo:idequipo}, function(e){
				bootbox.alert(e);
				tabla.ajax.reload();
			})	
		}
	})
}

function activar(idequipo){

	bootbox.confirm("Esta seguro que quiere habilitar el avion?", function(result){
		if(result){
			$.post("../ajax/equipo.php?op=activar",{idequipo:idequipo}, function(e){
				bootbox.alert(e);
				tabla.ajax.reload();
			})	
		}
	})
}
*/

init();