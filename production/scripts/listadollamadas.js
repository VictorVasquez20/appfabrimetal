var tablalistadollamadasmes;
var meses = {"1":"ENERO","2":"FEBRERO","3":"MARZO","4":"ABRIL","5":"MAYO","6":"JUNIO","7":"JULIO","8":"AGOSTO","9":"SEPTIEMBRE","10":"OCTUBRE","11":"NOVIEMBRE","12":"DICIEMBRE",}
  
//funcion que se ejecuta iniciando
function init() {
    
    $.post("../ajax/listadollamadas.php?op=selectaniollamadasemergencia", function(r){
		$("#anio").html(r);
		$("#anio").selectpicker('refresh');
	});

    $("#anio").on("change", function(e){
                  
            $.get("../ajax/listadollamadas.php?op=selectmesllamadasemergencia",
            {'anio':$("#anio").val()}, function(r){

                    $("#mes").html(r);
                    $("#mes").selectpicker('refresh');
            });
    });    
    
    $('#form_filtros').on("submit", function(event){
		event.preventDefault();
                $("#div_listallamadas").show();
                tablallamadasemergenciames();
	});
        
}

function tablallamadasemergenciames(){
   
    var f= new Date();
    var anio,mes;
  
    if($("#anio").val() == null || $("#anio").val() == ''){
        anio = f.getFullYear(); 
    }else{
        anio = $("#anio").val() 
    } 
    
     if($("#mes").val() == null || $("#mes").val() == ''){
        mes = parseInt(f.getMonth())+1; 
    }else{
        mes = $("#mes").val() 
    }
    
    $("#titulo_llamadasemergenciames").html("Listado (Año: "+anio+" Mes: "+meses[mes]+")");
    
    tablalistadollamadasmes=$('#tbllistadollamadas').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip', 
		buttons:[
                        'copyHtml5',
			'print',
                        'csvHtml5',
		],
		"ajax":{
			url:'../ajax/listadollamadas.php?op=listarllamadasemergenciames',
			type:"POST",
			dataType:"json",
                        data:{"anio":anio, 'mes': mes},
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
                 "iDisplayLength": 20, //Paginacion 
                 "order" : [[0 , "desc"]] //Ordenar en base a la columna 0 descendente
	}).DataTable();
        
}


init();


