var tabla;

//funcion que se ejecuta iniciando
function init(){
    mostarform(false);
    listar();
    $('#formulario').on("submit", function(event){
        event.preventDefault();
        guardaryeditar();
    });
    
    $.post("../ajax/areainfo.php?op=selectAreainfo", function(r){
        $("#idareainfo").html(r);
        $("#idareainfo").selectpicker('refresh');
    });
}

function mostarform(flag){

    if(flag){
        $("#tblacontacto").hide();
        $("#listadocontacto").hide();
        $("#formulariocontacto").show();
        $("#op_actualizar").hide();
        $("#op_listar").show();
        $("#btnGuardar").prop("disabled", false);
        limpiar();
    }else{
        $("#tblacontacto").show();
        $("#formulariocontacto").hide();
        $("#listadocontacto").show();
        $("#op_actualizar").show();
        $("#op_listar").hide();
    }

}

function cancelarform(){
    mostarform(false);
}

function listar(){
    tabla=$('#tblacontacto').dataTable({
            "aProcessing":true,
            "aServerSide": true,
            dom: 'Bfrtip',
            buttons:[
                    'copyHtml5',
                    'print',
                    'excelHtml5',
                    'csvHtml5',
                    'pdf'
            ],
            "ajax":{
                    url:'../ajax/contactoinfo.php?op=listar',
                    type:"get",
                    dataType:"json",
                    error: function(e){
                            console.log(e.responseText);
                    }
            },
            "bDestroy": true,
            "iDisplayLength": 15, //Paginacion 10 items
            "order" : [[1 , "desc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}


function guardaryeditar(){
	$("#btnGuardar").prop("disabled", true);
	var formData = new FormData($("#formulario")[0]);
	$.ajax({
		url:'../ajax/contactoinfo.php?op=guardaryeditar',
		type:"POST",
		data:formData,
		contentType: false,
		processData:false,

		success: function(datos){
			bootbox.alert(datos);
			mostarform(false);
			tabla.ajax.reload();
                        limpiar();
		}
	});
}


function limpiar(){
    $("#idcontacto").val(0);
    $("#nombre").val("");
    $("#correo").val("");
    $("#idareainfo").val(0);
    $("#idareainfo").selectpicker("refresh");
}

function editar(idcontacto){
    $.post("../ajax/contactoinfo.php?op=mostrar",{idcontacto: idcontacto}, function(data){
        data = JSON.parse(data);
        mostarform(true);
        $("#idcontacto").val(data.idcontacto);
        $("#nombre").val(data.nombre);
        $("#correo").val(data.correo);
        $("#idareainfo").val(data.idareainfo);
        $("#idareainfo").selectpicker("refresh");
        
    });
}

init();


