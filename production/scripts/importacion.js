var tabla;

//funcion que se ejecuta iniciando
function init() {
    mostarform(false);
    listar();

    $("#formulario").on("submit", function (e) {
        guardaryeditar(e);
    });
}


// Otras funciones
function limpiar() {
    $("#idimportacion").val("");
    $("#codigo").val("");
    $("#nef").val("");
    $("#proveedor").val("");
    $("#despacho").val("");
    $("#llegada").val("");
}

function mostarform(flag) {
    limpiar();
    if (flag) {
        $("#listadoimportacion").hide();
        $("#formularioimportacion").show();
        $("#op_agregar").hide();
        $("#op_listar").show();
        $("#btnGuardar").prop("disabled", false);

    } else {
        $("#listadoimportacion").show();
        $("#formularioimportacion").hide();
        $("#op_agregar").show();
        $("#op_listar").hide();
    }
}

function cancelarform() {
    limpiar();
    mostarform(false);
}

function listar() {
    tabla = $('#tblimportacion').dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Bfrtip',
        buttons: Botones,
        "language": Español,
        "ajax": {
            url: '../ajax/importacion.php?op=listar',
            type: "get",
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 10, //Paginacion 10 items
        "order": [[1, "desc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}

function guardaryeditar(e) {
    e.preventDefault();
    $("#btnGuardar").prop("disabled", true);
    var formData = new FormData($("#formulario")[0]);
    $.ajax({
        url: '../ajax/importacion.php?op=guardaryeditar',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,

        success: function (datos) {
            //bootbox.alert(datos);
            if (datos != '0') {
                $("#btnGuardar").prop("disabled", false);
                new PNotify({
                    title: 'Correcto!',
                    text: 'Importación guardada con exito.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
            } else {
                $("#btnGuardar").prop("disabled", true);
                new PNotify({
                    title: 'Error!',
                    text: 'Importación no guardada.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
            mostarform(false);
            tabla.ajax.reload();
        }
    });
    limpiar();
}

function mostrar(idimportacion) {
    $.post("../ajax/importacion.php?op=mostrar", {idimportacion: idimportacion}, function (data, status) {
        data = JSON.parse(data);
        mostarform(true);
        
        $("#idimportacion").val(data.idimportacion);
        $("#codigo").val(data.codigo);
        $("#nef").val(data.nef);
        $("#proveedor").val(data.proveedor);
        $("#despacho").val(formatofecha(data.dispatch_time));
        $("#llegada").val(formatofecha(data.arrival_time));
        
        if(data.dispatch_time || data.arrival_time){
            $("#btnGuardar").prop("disabled", true);
        }
    });
}

function despacho(idimportacion) {

    bootbox.confirm("Va a registrar la hora de despacho, ¿esta seguro?", function (result) {
        if (result) {
            var formData = new FormData();

            formData.append("idimportacion", idimportacion);
            formData.append("arrival_time", 0);
            formData.append("dispatch_time", 1);

            $.ajax({
                url: '../ajax/importacion.php?op=editartime',
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,

                success: function (datos) {
                    if (datos != '0') {
                        $("#btnGuardar").prop("disabled", false);
                        new PNotify({
                            title: 'Correcto!',
                            text: 'Hora de Despacho guardada con exito.',
                            type: 'success',
                            styling: 'bootstrap3'
                        });
                    } else {
                        $("#btnGuardar").prop("disabled", true);
                        new PNotify({
                            title: 'Error!',
                            text: 'Hora de despacho no guardada.',
                            type: 'error',
                            styling: 'bootstrap3'
                        });
                    }
                    tabla.ajax.reload();
                }
            });
        }
    });
}

function llegada(idimportacion) {
    bootbox.confirm("Va a registrar la hora de llegada, ¿esta seguro?", function (result) {
        if (result) {
            var formData = new FormData();

            formData.append("idimportacion", idimportacion);
            formData.append("arrival_time", 1);
            formData.append("dispatch_time", 0);

            $.ajax({
                url: '../ajax/importacion.php?op=editartime',
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,

                success: function (datos) {
                    if (datos != '0') {
                        $("#btnGuardar").prop("disabled", false);
                        new PNotify({
                            title: 'Correcto!',
                            text: 'Hora de Llegada guardada con exito.',
                            type: 'success',
                            styling: 'bootstrap3'
                        });
                    } else {
                        $("#btnGuardar").prop("disabled", true);
                        new PNotify({
                            title: 'Error!',
                            text: 'Hora de Llegada no guardada.',
                            type: 'error',
                            styling: 'bootstrap3'
                        });
                    }
                    tabla.ajax.reload();
                }
            });
        }
    });
}

function desactivar(idcentrocosto) {

    bootbox.confirm("Esta seguro que quiere inhabilitar este centro de costo??", function (result) {
        if (result) {
            $.post("../ajax/centrocosto.php?op=desactivar", {idcentrocosto: idcentrocosto}, function (e) {
                bootbox.alert(e);
                tabla.ajax.reload();
            })
        }
    })
}

function activar(idcentrocosto) {

    bootbox.confirm("Esta seguro que quiere habilitar este centro de costo?", function (result) {
        if (result) {
            $.post("../ajax/centrocosto.php?op=activar", {idcentrocosto: idcentrocosto}, function (e) {
                bootbox.alert(e);
                tabla.ajax.reload();
            })
        }
    })
}

init();


