var tabla, nomb = [], cantidad = [], porcentajes = [], nombhorizontal = [];
var supervisor = [], visitas = [];

function init() {
    cargarestados();

    tabla = $("#visitas").DataTable({
        "paging": false,
        "info": false,
        "bFilter": false,
        "bAutoWidth": false,
        "ordering": false
    });

    cargarvisitas();

    setTimeout(slide, 180000);
    
}

function slide(){    
    $(location).attr("href", "slidecontabilidad.php");
}


var theme = {
    color: [
        '#5cb85c', '#34495E', '#BDC3C7', '#8abb6f', '#3498DB', '#26B99A', '#759c6a', '#bfd3b7'
    ]};

function cargarestados() {

    $.post("../ajax/dashinstalaciones.php?op=instalacionesxestado", function (data, status) {
        data = JSON.parse(data);
        t = data.length - 1;
        html = '<div class="col-md-6 col-sm-6 col-xs-6 tile_stats_count">'
                + '    <div class="row top_tiles text-center">'
                + '        <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">'
                + '            <div class="tile-stats text-success">'
                + '                <div class="count">' + data[0]["proyectos"] + '</div>'
                + '                <h3>TOTAL PROYECTOS EN EJECUCION</h3> <p>&nbsp;</p>'
                + '            </div>'
                + '        </div>'
                + '    </div>'
                + '</div>';

        html += '<div class="col-md-6 col-sm-6 col-xs-6 tile_stats_count">'
                + '    <div class="row top_tiles text-center">'
                + '        <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">'
                + '            <div class="tile-stats text-success">'
                + '                <div class="count">' + data[t]["cantidad"] + '</div>'
                + '                <h3  class="text-success">TOTAL PROYECTOS TERMINADOS</h3> <p>&nbsp;</p>'
                + '            </div>'
                + '        </div>'
                + '    </div>'
                + '</div>';
        
                
        
        //nombhorizontal.push('TOTAL PROYECTOS EN EJECUCION');
        //porcentajes.push(100);
        for (i = 0; i < data.length - 1; i++) {
            porc = parseFloat((parseFloat(data[i]['cantidad']) / parseFloat(data[i]['proyectos'])) * 100).toFixed(0);

            html += '<div class="col-md-2 col-sm-2 col-xs-6 tile_stats_count">'
                    + '    <span class="count_top"><b>' + data[i]["estado"] + '</b></span>'
                    //+ '    <h2>' + data[i]["cantidad"] + ' (' + porc + '%)</h2>'
                    + '    <div class="count">' + data[i]["cantidad"] + ' <span>(' + porc + '%)</span></div>'
                    //+ '    <div class="count"><span>cantidad - </span>' + data[i]["cantidad"] + '</div>'
                    //+ '    <span class="text-success">porcentaje: ' + porc + '%</span>'
                    + '</div>'

            //grafico piramide
            nomb.push(data[i]['estado']);

            //grafico area
            cantidad.push(data[i]["cantidad"]);

            //horizontal
            porcentajes.push(porc);
            nombhorizontal.push(data[i]['estado']);
        }

        $("#tiles").empty();
        $("#tiles").append(html);


        if ($('#grafico').length) {

            var echartBar = echarts.init(document.getElementById('grafico'), theme);

            echartBar.setOption({
                title: {
                    text: 'Cantidad de proyectos por estado',
                    subtext: 'Proyectos Instalacion'
                },
                tooltip: {
                    trigger: 'axis'
                },
                grid: {
                    left: '4%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                legend: {
                    data: nomb
                },
                toolbox: {
                    show: false
                },
                calculable: false,
                xAxis: [{
                        type: 'category',
                        data: nomb
                    }],
                yAxis: [{
                        type: 'value'
                    }],
                series: [{
                        name: 'cantidad',
                        type: 'bar',
                        data: cantidad,
                        markPoint: {
                            data: [{
                                    type: 'max',
                                    name: ''
                                }, {
                                    type: 'min',
                                    name: ''
                                }]
                        },
                        markLine: {
                            data: [{
                                    type: 'average',
                                    name: ''
                                }]
                        }
                    }]
            });

        }

        if ($('#echart').length) {

            var echartBar = echarts.init(document.getElementById('echart'), theme);

            echartBar.setOption({
                title: {
                    text: 'Porcentaje de proyectos por estado',
                    subtext: 'Proyectos Instalacion'
                },
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    x: 100
                },
                grid: {
                    left: '0%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                calculable: true,
                xAxis: [{
                        type: 'value',
                        boundaryGap: [0, 0]
                    }],
                yAxis: [{
                        type: 'category',
                        data: nombhorizontal.reverse()
                    }],
                series: [{
                        name: 'porcentaje',
                        type: 'bar',
                        data: porcentajes.reverse()
                    }]
            });

        }

    });
}

function cargarvisitas() {

    $.post("../ajax/dashinstalaciones.php?op=visitasxobra", function (data, status) {
        data = JSON.parse(data);
        tabla.rows().remove().draw();

        for (i = 0; i < data.length; i++) {
            tabla.row.add([
                data[i]['sup'],
                data[i]['proyectos'],
                data[i]['visitas'],
                data[i]['procentaje'],
                data[i]['terminados']
            ]).draw(false);

            supervisor.push(data[i]['sup']);
            visitas.push(data[i]['visitas']);
        }

        if ($('#supvisitas').length) {

            var ctx = document.getElementById("supvisitas");
            var lineChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: supervisor,
                    datasets: [{
                            label: "visitas",
                            backgroundColor: "rgba(92, 184, 92, 0.31)",
                            borderColor: "rgba(92, 184, 92, 0.7)",
                            pointBorderColor: "rgba(92, 184, 92, 0.7)",
                            pointBackgroundColor: "rgba(92, 184, 92, 0.7)",
                            pointHoverBackgroundColor: "#fff",
                            pointHoverBorderColor: "rgba(220,220,220,1)",
                            pointBorderWidth: 0,
                            data: visitas
                        }]
                },
            });

        }

    });
}

init();