var tabla;


//funcion que se ejecuta iniciando
function init(){
	mostarform(false);
	listar();

	$('[data-toggle="tooltip"]').tooltip(); 

	$('#formulario').on("submit", function(event){
		event.preventDefault();
		guardarids();
	});
}


// Otras funciones
function limpiar(){
	console.log("Limpiar");
}

function mostarform(flag){
	limpiar();
	if(flag){
		$("#listadosolicitudesid").hide();
		$("#formulariosolicitudesid").show();
		$("#op_agregar").hide();
		$("#op_listar").show();
	}else{
		$("#listadosolicitudesid").show();
		$("#formulariosolicitudesid").hide();
		$("#op_agregar").show();
		$("#op_listar").hide();
	}

}

function cancelarform(){
	limpiar();
	mostarform(false);
}


function addformasc(contrato, nascensores){
		console.log("Listando formulario de codigo para "+nascensores+" ascensores del contrato"+contrato+"");
                $('#ascensoressoliid').empty();
                $.get("../ajax/ascensor.php?op=solid_edifid",{id:contrato}, function(r){
                    data = JSON.parse(r);
                    $('#ascensoressoliid').append('<input type="hidden" id="nascensores" name="nascensores" value="'+data.nasensores+'" class="form-control">');
                    for (var i = 0; i < data.ascensores.length ; i++) {
			var myvar = '<div class="row">'+
                                    '<div class="col-md-5 col-sm-12 col-xs-12 form-group">'+
                                    '<label for="edificio">Edificio</label>'+
                                    '<input type="hidden" id="idascensor" name="idascensor[]" value="'+data.ascensores[i][0]+'" class="form-control">'+
                                    '<input type="text" id="edificio" name="edificio[]" value="'+data.ascensores[i][1]+'" class="form-control" readonly="readonly">'+
                                    '</div>'+
                                    '<div class="col-md-1 col-sm-12 col-xs-12 form-group">'+
                                    '<label for="region">Region</label>'+
                                    '<input type="text" id="region" name="region[]" value="'+data.ascensores[i][2]+'" class="form-control" readonly="readonly">'+
                                    '</div>'+
                                    '<div class="col-md-2 col-sm-12 col-xs-12 form-group">'+
                                    '<label for="marca">Marca</label>'+
                                    '<input type="text" id="marca" name="marca[]" value="'+data.ascensores[i][3]+'" class="form-control" readonly="readonly">'+
                                    '</div>'+
                                    '<div class="col-md-2 col-sm-12 col-xs-12 form-group">'+
                                    '<label for="modelo">Modelo</label>'+
                                    '<input type="text" id="modelo" name="modelo[]" value="'+data.ascensores[i][4]+'" class="form-control" readonly="readonly">'+
                                    '</div>'+
                                    '<div class="col-md-2 col-sm-12 col-xs-12 form-group">'+
                                    '<label for="codigo">Codigo</label>'+
                                    '<input type="text" id="codigo" name="codigo[]" class="form-control">'+
                                    '</div>'+
                                    '</div>';

			$('#ascensoressoliid').append(myvar);
                    }        
		});
                
                mostarform(true);
}



function listar(){
	tabla=$('#tblsolicitudesid').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons:[
			'copyHtml5',
			'print',
			'excelHtml5',
			'csvHtml5',
			'pdf'
		],
		"ajax":{
			url:'../ajax/contrato.php?op=listarsoid',
			type:"get",
			dataType:"json",
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 10, //Paginacion 10 items
		"order" : [[1 , "desc"]] //Ordenar en base a la columna 0 descendente
	}).DataTable();
}


function guardarids(){
	$("#btnGuardar").prop("disabled", true);
	var formData = new FormData($("#formulario")[0]);
	$.ajax({
		url:'../ajax/ascensor.php?op=InsertarIds',
		type:"POST",
		data:formData,
		contentType: false,
		processData:false,

		success: function(datos){
			bootbox.alert(datos);
			mostarform(false);
                        $("#btnGuardar").prop("disabled", false);
			tabla.ajax.reload();
		}
	});
}

function guardaryeditar(){
	$("#btnGuardar").prop("disabled", true);
	console.log("Entro a guardar");
	var formData = new FormData($("#formulario")[0]);
	$.ajax({
		url:'../ajax/contrato.php?op=guardaryeditar',
		type:"POST",
		data:formData,
		contentType: false,
		processData:false,

		success: function(datos){
			bootbox.alert(datos);
			mostarform(false);
			tabla.ajax.reload();
		}
	});
	limpiar();
}


init();