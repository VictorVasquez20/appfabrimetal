var tabla;
var count = 1;
//funcion que se ejecuta iniciando
function init() {
	cargartiles();
	GraficoPresupuestosPorMes();
	setInterval(cargartiles, 1800000);
	setInterval(GraficoPresupuestosPorMes, 1800000);
}

function cargartiles() {
	$.post("../ajax/detenidos.php?op=ContarDatos", function(data, status) {
		data = JSON.parse(data);
		//Actualizamos valores
		$("#total_detenidos").html(data.detenidos.total);
		$("#en_evaluacion").html(data.evaluacion);
		$("#cotizados").html(data.cotizados);
		$("#aprobados").html(data.aprobados);
	});
}

function GraficoPresupuestosPorMes() {
	var fecha = new Date();
	var anio_busq;
	anio_busq = fecha.getFullYear();
	$("#titulo_grafico_lineal").html("PRESUPUESTOS DEL AÑO: " + anio_busq);

	$.post("../ajax/detenidos.php?op=GraficoDetenidosPorMes", { 'anio_busq': anio_busq },function(data, status) {
		data = JSON.parse(data);
		arrIn = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
	
		for (i = 0; i < data.ingreso.length; i++) {
			arrIn[parseInt(data.ingreso[i]['mes']) - 1] = data.ingreso[i]['cantidad'];;
		}

		if ($('#mybarChart').length) {
			var ctx = document.getElementById("mybarChart");
			Chart.pluginService.register({
				beforeRender: function (chart) {
					if (chart.config.options.showAllTooltips) {
						chart.pluginTooltips = [];
						chart.config.data.datasets.forEach(function (dataset, i) {
							chart.getDatasetMeta(i).data.forEach(function (sector, j) {
								chart.pluginTooltips.push(new Chart.Tooltip({
									_chart: chart.chart,
									_chartInstance: chart,
									_data: chart.data,
									_options: chart.options,
									_active: [sector]
								}, chart));
							});
						});
						chart.options.tooltips.enabled = false;
					}
				},
				afterDraw: function (chart, easing) {
					if (chart.config.options.showAllTooltips) {
						if (!chart.allTooltipsOnce) {
							if (easing !== 1){
								return;
							}
							chart.allTooltipsOnce = true;
						}
						chart.options.tooltips.enabled = true;
						Chart.helpers.each(chart.pluginTooltips, function (tooltip) {
							tooltip.initialize();
							tooltip.update();
							tooltip.pivot();
							tooltip.transition(easing).draw();
						});
						chart.options.tooltips.enabled = false;
					}
				}
			});
			lineChart = new Chart(ctx, {
				type: 'bar',
				data: {
					labels: ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"],
					datasets: [{
						label: "Cant.",
						backgroundColor: "rgba(38, 185, 154, 0.31)",
						borderColor: "rgba(38, 185, 154, 0.7)",
						pointBorderColor: "rgba(38, 185, 154, 0.7)",
						pointBackgroundColor: "rgba(38, 185, 154, 0.7)",
						pointHoverBackgroundColor: "#fff",
						pointHoverBorderColor: "rgba(220,220,220,1)",
						pointBorderWidth: 1,
						data: arrIn
					},]
				},
				options: {
					legend: {
						display: false
					},
					animation: {
						duration: 0
					},
					showAllTooltips: true,
					scales: {
						xAxes: [{
							display: true,
							scaleLabel: {
								display: true,
								labelString: 'Mes'
							}
						}],
						yAxes: [{
							display: true,
							scaleLabel: {
								display: true,
								labelString: 'Cantidad de GSE'
							}
						}]
					}
				}
			});
		}
	});
}
init();