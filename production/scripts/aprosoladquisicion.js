var tabla, tabladet, vertabladet;
var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

/***
 * 
 * DATO IMPORTANTE!!!!
 * La mayotia de los servicion estan en el AJAX de "soladquisicion.php"
 * existen vistas y JS de los otros modelos pero no los AJAX, por lo que todos se manejan con ese archivo.
 * 
 */

function init() {
    mostrarform(false);
    listar();

    //select centro costo
    $.post("../ajax/centrocosto.php?op=selectcentro", (r) => {
        $("#ccosto").html(r);
        $("#ccosto").selectpicker('refresh');
    });
    //select monedas
    $.post("../ajax/moneda.php?op=selecttodasmonedas", (r) => {
        $("#moneda").html(r);
        $("#moneda").selectpicker('refresh');
    });
    //select proveedores
    $.post("../ajax/proveedor.php?op=selectproveedor", (r) => {
        $("#proveedor").html(r);
        $("#proveedor").selectpicker('refresh');
    });
    //condiciones de pago 
    $.post("../ajax/condicionpago.php?op=selectcondicion", (r) => {
        $("#condpago").html(r);
        $("#condpago").selectpicker('refresh');
    });

    //selectcategoria
    $.post("../ajax/categoria_productoAjax.php?op=selectcategoria", (r) => {
        $("#det_categoria").html(r);
        $("#det_categoria").selectpicker('refresh');
    });

    $("#formulario").on('submit', (e) => {
        e.preventDefault();
        grabarencabezado();
    });

    $("#formaprueba").on('submit', (e) => {
        e.preventDefault();
        enviar();
    });

    $("#formcotizacion").on('submit', (e) => {
        e.preventDefault();
        subir();
    });


    tabladet = $('#detalle').dataTable({
        "responsive": true,
        "aProcessing": true,
        "aServerSide": true,
        "scrollX": false,
        dom: 'Bfrtip',
        buttons: Botones,
        "language": Español
    });
    
    vertabladet = $("#ver_detalle").DataTable({
        "responsive": true,
        "aProcessing": true,
        "aServerSide": true,
        "scrollX": false,
        dom: 'Bfrtip',
        buttons: Botones,
        "language": Español
    });
}

function mostrarform(flag) {
    //limpiar();
    if (flag) {
        $("#listadsolicitudes").hide();
        $("#formsolicitudes").show();
        $("#op_agregar").hide();
        $("#op_listar").show();
    } else {

        $("#formsolicitudes").hide();
        $("#listadsolicitudes").show();
        $("#op_agregar").show();
        $("#op_listar").hide();
    }

}

function listar() {
    tabla = $('#tblguias').dataTable({
        "responsive": true,
        "aProcessing": true,
        "aServerSide": true,
        "scrollX": false,
        dom: 'Bfrtip',
        buttons: Botones,
        "language": Español,
        "ajax": {
            url: '../ajax/soladquisicion.php?op=listaraprueba',
            type: "POST",
            dataType: "json",
            error: (e) => {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 20, //Paginacion 10 items
        "order": [[2, "desc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}

/**
 * Obtiene los datos de la solicitud para su revsion
 * @param {type} idsolicitud
 * @return {undefined}
 */
function editar(idsolicitud) {
    $.post("../ajax/soladquisicion.php?op=ver", {idsolicitud: idsolicitud}, (data, status) => {
        data = JSON.parse(data);
        mostrarform(true);

        //TAB 1
        $("#ver_numero").empty();
        $("#ver_total").empty();
        $("#ver_solicita").empty();
        $("#ver_ccosto").empty();
        $("#ver_condpago").empty();
        $("#ver_fecha").empty();
        $("#ver_proveedor").empty();
        $("#ver_moneda").empty();

        $("#ver_numero").append(data.idsolicitud);
        
        $("#ver_solicita").append(data.usuario);
        $("#ver_ccosto").append(data.ccosto);
        $("#ver_condpago").append(data.condpago);
        $("#ver_fecha").append(data.fecha);
        $("#ver_proveedor").append(data.rut + ' / ' + data.razonsocial);
        $("#ver_moneda").append(data.strmoneda);


        $("#idsolicitud").val(idsolicitud);
        $("#fase").val(data.fase);
        $("#completaadquisicion").val(data.completasolicitud);
        $("#fasefinal").val(data.fasefinal);

        if (data.completasolicitud == 1) {
            $("#formulariocotizaciones").show();
        } else {
            $("#formulariocotizaciones").hide();
        }

        
        
        
        vertabladet.rows().remove().draw();
        var total = 0, neto = 0, iva = 0, subtotal = 0;
        $.post("../ajax/detsoladquisicion.php?op=listar", {idsolicitud: idsolicitud}, function (data, status) {
            data = JSON.parse(data);
            for (i = 0; i < data.aaData.length; i++) {
                vertabladet.row.add([
                    data.aaData[i]["0"],
                    data.aaData[i]["1"],
                    data.aaData[i]["2"],
                    data.aaData[i]["3"],
                    data.aaData[i]["4"]
                ]).draw(false);
                
                subtotal += parseFloat(data.aaData[i]["3"]);
            }
            $("#totdet").empty();
            $("#totdet").html("<b>" + new Intl.NumberFormat("de-DE").format(subtotal) + "</b>");
            
            $("#neto").empty();
            $("#neto").html("<b>" + new Intl.NumberFormat("de-DE").format(subtotal) + "</b>");
            
            
            iva = (subtotal*19)/100;
            $("#iva").empty();
            $("#iva").html("<b>" + new Intl.NumberFormat("de-DE").format(iva) + "</b>");
            
            $("#total").empty();
            $("#total").html("<b>" + new Intl.NumberFormat("de-DE").format(subtotal + iva) + "</b>");
            
            $("#ver_total").append(new Intl.NumberFormat("de-DE").format(subtotal + iva));
            
        });
        

        //TAB 2
        //OBSERVACIONES
        $("#ver_observaciones").empty();
        html1 = "";
        html = '<li>'
                + '     <img src="../files/usuarios/noimg.jpg" class="avatar" alt="Avatar">'
                + '      <div class="message_wrapper">'
                + '          <h4 class="heading">' + data.usuario + '</h4>'
                + '          <blockquote class="message">' + data.observacion + '</blockquote><br>'
                + '          <p>' + formatofecha(data.created_time) + '</p>'
                + '      </div>'
                + '  </li>'
        //$("#ver_observaciones").append(html);

        $.post("../ajax/soladquisicion.php?op=listarobservacion", {idsolicitud: idsolicitud}, (dataObs, status) => {
            dataObs = JSON.parse(dataObs);
            for (i = 0; i < dataObs.length; i++) {
                html1 += '<li>'
                        + '     <img src="../files/usuarios/noimg.jpg" class="avatar" alt="Avatar">'
                        + '      <div class="message_wrapper">'
                        + '          <h4 class="heading">' + dataObs[i]['nombre'] + '</h4>'
                        + '          <blockquote class="message">' + dataObs[i]['observacion'] + '</blockquote><br>'
                        + '          <p>' + formatofecha(dataObs[i]['created_time']) + '</p>'
                        + '      </div>'
                        + '  </li>'

            }
            $("#ver_observaciones").append(html + html1);
        });




        //TAB 3
        archivos = "";
        //COTIZACIONES
        vercotizaciones(idsolicitud);

    });
}

function cancelarform() {
    limpiar();
    limpiardet();
    mostrarform(false);
}

function limpiar() {
    $("#idsolicitud").val(0);
    $("#estado").val(0);
    $("#fecha").val("");
    $("#ccosto").val("");
    $("#ccosto").selectpicker('refresh');
    $("#proveedor").val("");
    $("#proveedor").selectpicker('refresh');
    $("#moneda").val("");
    $("#condpago").val("");
    $("#prioridad").val("");
    $("#observacion").val("");
    $("#anterior").val("");
    $("#archivo").val("");
    $("#archivos").empty();
}


/**
 * Envia la solicitud para continuar la revision o convertirse en orden de compra
 * a su vez notifica a quien deba revisar la solicitud
 * @return {Boolean}
 */
function enviar() {
    var observacion = $("#observacion").val();
    var idsolicitud = $("#idsolicitud").val();
    var fase = $("#fase").val();
    var estado = 1;
    var completa = $("#completaadquisicion").val();
    var archivos = $("#archivos").val();
    var fasefinal = $("#fasefinal").val();
    var mensaje = "";

    //Si el usuario que revisa es el que debe completar y aun no adjunta las cotizaciones necesarias se muestra una alerta.
    if (completa == 1) {
        if (archivos < 3) {
            bootbox.alert("Debe Completar la solicitud de Adquisicion. Falta subir cotizaciones.");
            return false;
        }
    }


    //si la fase actual es igual a la final entonces se cambia el estado de la solicitud a aceptada, de lo contrario continua con la revision
    if (fase == fasefinal) {
        mensaje = "Va a finalizar la solicitud. Esta se convertira en Orden de compra, ¿esta seguro? ";
        estado = 2;
    } else {
        mensaje = "Va a aprobar esta solicitud para que siga el proceso de revisiones. ¿esta seguro?";
        fase = (parseInt(fase) + 1)
    }

    bootbox.confirm(mensaje, (result) => {
        if (result) {
            //GRABO LA OBSERVACION 
            $.post("../ajax/soladquisicion.php?op=guardaryeditarobservacion", {idsolicitud: idsolicitud, observacion: observacion}, (data, status) => {
                data = JSON.parse(data);

                //SI LA OBSERVACION SE GRABO CORRECAMENTE AVANZO LA FASE DE REVISION
                if (data) {
                    $.post("../ajax/soladquisicion.php?op=avanzar", {idsolicitud: idsolicitud, estado: estado, fase: fase}, (data2, status) => {
                        data2 = JSON.parse(data2);

                        if (data2 != 0) {
                            new PNotify({
                                title: 'Exito!',
                                text: 'Solicitud aprobada.',
                                type: 'success',
                                styling: 'bootstrap3'
                            });

                            if (estado = 2) {
                                //envio correo de notificacion de RECHAZO
                                $.post("../ajax/soladquisicion.php?op=notificacionapruebarechaza&texto=APROBADA", {idsolicitud: idsolicitud}, (datos, status) => {
                                });
                            } else {
                                //envio correo de notificacion
                                $.post("../ajax/soladquisicion.php?op=notificacion", {idsolicitud: idsolicitud}, (datos, status) => {
                                });
                            }
                        } else {
                            new PNotify({
                                title: 'Error!',
                                text: 'Hubo un error, intente de nuevo mas tarde.',
                                type: 'error',
                                styling: 'bootstrap3'
                            });
                        }
                        mostrarform(false);
                        tabla.ajax.reload();
                    });
                }

            });
        }
    });
}

/**
 * Rechaza la solicitud y le cambia el estado
 * @return {Boolean}
 */
function rechazar() {
    var observacion = $("#observacion").val();

    //se debe ingresar una observacion para el rechazo
    if (observacion == "") {
        new PNotify({
            title: 'Error!',
            text: 'Debe escribir una observación con el motivo del rechazo.',
            type: 'error',
            styling: 'bootstrap3'
        });
        return false;
    }

    var idsolicitud = $("#idsolicitud").val();
    var fase = $("#fase").val();
    var estado = 3;

    bootbox.confirm("Va a rechazar esta solicitud de adquisición, esta sera devuelta como rechazada al solicitante, ¿esta seguro?", (result) => {
        if (result) {
            //GRABO LA OBSERVACION 
            $.post("../ajax/soladquisicion.php?op=guardaryeditarobservacion", {idsolicitud: idsolicitud, observacion: observacion}, (data, status) => {
                data = JSON.parse(data);

                //SI LA OBSERVACION SE GRABO CORRECAMENTE AVANZO LA FASE DE REVISION
                if (data) {
                    $.post("../ajax/soladquisicion.php?op=avanzar", {idsolicitud: idsolicitud, estado: estado, fase: fase}, (data2, status) => {
                        data2 = JSON.parse(data2);

                        if (data2 != 0) {
                            new PNotify({
                                title: 'Exito!',
                                text: 'Solicitud rechazada.',
                                type: 'success',
                                styling: 'bootstrap3'
                            });

                            //envio correo de notificacion de RECHAZO
                            $.post("../ajax/soladquisicion.php?op=notificacionapruebarechaza&texto=RECHAZADA", {idsolicitud: idsolicitud}, (datos, status) => {
                            });

                        } else {
                            new PNotify({
                                title: 'Error!',
                                text: 'Hubo un error, intente de nuevo mas tarde.',
                                type: 'error',
                                styling: 'bootstrap3'
                            });
                        }
                        mostrarform(false);
                        tabla.ajax.reload();
                    });
                }

            });
        }
    });
}


/* COTIZACIONES */

/**
 * Se suben las cotizaciones para completar la solicitud
 * @return {Boolean}
 */
function subir() {
    var idsolicitud = $("#idsolicitud").val();
    p = $("#proveedor").val();

    if ($("#proveedor").val() == 0 || $("#proveedor").val() == "" || $("#proveedor").val() == null) {
        new PNotify({
            title: 'Error!',
            text: 'Debe seleccionar un proveedor. Si no existe pongase en contacto con el Dpto. de informática.',
            type: 'error',
            styling: 'bootstrap3'
        });

        $("#proveedor").addClass('parsley-error');
        return false;
    }

    if ($("#moneda").val() == 0 || $("#moneda").val() == "" || $("#moneda").val() == null) {
        new PNotify({
            title: 'Error!',
            text: 'Debe seleccionar una moneda. Si no existe pongase en contacto con el Dpto. de informática.',
            type: 'error',
            styling: 'bootstrap3'
        });
        $("#moneda").addClass('parsley-error');
        return false;
    }

    if ($("#condpago").val() == 0 || $("#condpago").val() == "" || $("#condpago").val() == null) {
        new PNotify({
            title: 'Error!',
            text: 'Debe seleccionar una Condición de pago. Si no existe pongase en contacto con el Dpto. de informática.',
            type: 'error',
            styling: 'bootstrap3'
        });
        $("#condpago").addClass('parsley-error');
        return false;
    }

    if ($("#archivo").val() == 0 || $("#archivo").val() == "") {
        new PNotify({
            title: 'Error!',
            text: 'Debe seleccionar al menos un archivo.',
            type: 'error',
            styling: 'bootstrap3'
        });
        $("#archivo").addClass('parsley-error');
        return false;
    }


    var formData = new FormData($("#formcotizacion")[0]);
    formData.append("idsolicitud", idsolicitud);

    $.ajax({
        url: '../ajax/soladquisicion.php?op=subircotizacion',
        type: "POST",
        data: formData,
        async: false,
        cache: false,
        timeout: 30000,
        contentType: false,
        processData: false,
        success: (datos) => {
            if (!isNaN(datos) && datos != 0 && datos != "") {
                new PNotify({
                    title: 'Exito!',
                    text: 'Cotización guardada con exito.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
                $("#idsolicitud").val(datos);
            } else {
                new PNotify({
                    title: 'Error!',
                    text: 'Hubo un error al registar. ' + datos,
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
            limpiarcotizacion();
            vercotizaciones(idsolicitud);
            //limpiar();
            //mostrarform(false);
            //tabla.ajax.reload();
        }
    });
}

function limpiarcotizacion() {
    $("#archivo").val("");
    $("#anterior").val("");
    $("#idcotizacion").val("");
    $("#moneda").val("");
    $("#moneda").selectpicker("refresh");
    $("#condpago").val("");
    $("#condpago").selectpicker("refresh");
    $("#proveedor").val("");
    $("#proveedor").selectpicker("refresh");
}

/**
 * Muestra las cotizaciones que se han subido a la solicitud de adquisicion.
 * @param {type} idsolicitud
 * @return {undefined}
 */
function vercotizaciones(idsolicitud) {
    archivos = "";
    //COTIZACIONES
    $.post("../ajax/soladquisicion.php?op=listarcotizacion", {idsolicitud: idsolicitud}, (data, status) => {
        data = JSON.parse(data);
        for (i = 0; i < data.length; i++) {
            if (data[i]['sel'] != "") {
                archivos += '<div class="col-md-4 col-sm-6 col-xs-12">'
                        + '<div class="pricing ui-ribbon-container">'
                        + '  <div class="ui-ribbon-wrapper">'
                        + '    <div class="ui-ribbon">'
                        + '       <i class="glyphicon glyphicon-star-empty"></i>'
                        + '    </div>'
                        + '  </div>'
                        + '  <div class="title">'
                        + '    <h2>archivo ' + (i + 1) + '</h2>'
                        + '    <h1>COTIZACIÓN ' + (i + 1) + '</h1>'
                        + '    <span>seleccionado</span>'
                        + '  </div>'
                        + '  <div class="x_content">'
                        + '    <div class="">'
                        + '      <div class="pricing_features">'
                        + '        <ul class="list-unstyled text-left">'
                        + '          <li><i class="fa fa-check text-success"></i> Proveedor: <br><strong>' + data[i]['razonsocial'] + '</strong></li>'
                        + '              <li><i class="fa fa-check text-success"></i> Rut: <br><strong>' + data[i]['rut'] + '</strong></li>'
                        + '              <li><i class="fa fa-check text-success"></i> Moneda: <br><strong>' + data[i]['moneda'] + '</strong></li>'
                        + '              <li><i class="fa fa-check text-success"></i> Condicion de pago: <br><strong>' + data[i]['condpago'] + '</strong></li>'
                        + '              <li><i class="fa fa-file-pdf-o text-danger"></i> - <a target="_blank" href="../files/cotizacion/' + data[i]['idsolicitud'] + '/' + data[i]['archivo'] + '">Descargar cotizacion</a></li>'
                        + '        </ul>'
                        + '      </div>'
                        + '    </div>'
                        + ' </div>'
                        + '</div>'
                        + '</div>';

            } else {
                archivos += '<div class="col-md-4 col-sm-6 col-xs-12">'
                        + '   <div class="pricing">'
                        + '     <div class="title">'
                        + '    <h2>archivo ' + (i + 1) + '</h2>'
                        + '    <h1>COTIZACIÓN ' + (i + 1) + '</h1>'
                        + '  </div>'
                        + '  <div class="x_content">'
                        + ' <div class="">'
                        + '      <div class="pricing_features">'
                        + '        <ul class="list-unstyled text-left">'
                        + '          <li><i class="fa fa-check text-success"></i> Proveedor: <br><strong>' + data[i]['razonsocial'] + '</strong></li>'
                        + '              <li><i class="fa fa-check text-success"></i> Rut: <br><strong>' + data[i]['rut'] + '</strong></li>'
                        + '              <li><i class="fa fa-check text-success"></i> Moneda: <br><strong>' + data[i]['moneda'] + '</strong></li>'
                        + '              <li><i class="fa fa-check text-success"></i> Condicion de pago: <br><strong>' + data[i]['condpago'] + '</strong></li>'
                        + '              <li><i class="fa fa-file-pdf-o text-danger"></i> - <a target="_blank" href="../files/cotizacion/' + data[i]['idsolicitud'] + '/' + data[i]['archivo'] + '">Descargar cotizacion</a></li>'
                        + '        </ul>'
                        + '      </div>'
                        + '    </div>'
                        + '    <div class="pricing_footer">'
                        + '      <a href="javascript:cambiacotizacion('+ data[i]["idcotizacion"] +', '+ idsolicitud +');" class="btn btn-success btn-block" role="button"><span>Sleccionar cotización</span></a>'
                        + '    </div>'
                        + '  </div>'
                        + '</div>'
                        + '</div>'
            }

        }

        $("#nroarchivos").empty();
        if (data.length < 3) {
            $("#nroarchivos").append(
                    '<div class="icheckbox_flat-green" style="position: relative;">'
                    + '    <input type="checkbox" class="flat" style="position: absolute; opacity: 0;" >'
                    + '    <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>'
                    + '</div> <span id="nroarchivos"></span> Faltan ' + (3 - data.length) + ' Cotizaciones');
        } else {
            $("#nroarchivos").append(
                    '<div class="icheckbox_flat-green" style="position: relative;">'
                    + '    <input type="checkbox" class="flat" style="position: absolute; opacity: 0;" checked="true" >'
                    + '    <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>'
                    + '</div> <span id="nroarchivos"></span> Archivos correctos');
            $('input.flat').iCheck({checkboxClass: 'icheckbox_flat-green', radioClass: 'iradio_flat-green'});


        }
        $("#archivos").val(data.length);
        $("#ver_archivos").empty();
        $("#ver_archivos").append(archivos);
    });
}


function cambiacotizacion(idcotizacion, idsolicitud) {
    $.post("../ajax/soladquisicion.php?op=cambiacotizacion", {idcotizacion: idcotizacion}, (data, status) => {
        data = JSON.parse(data);
        if (data != 0) {
            new PNotify({
                title: 'Exito!',
                text: 'Cotización seleccionada.',
                type: 'success',
                styling: 'bootstrap3'
            });
            editar(idsolicitud);
        } else {
            new PNotify({
                title: 'Error!',
                text: 'Hubo un error, intente de nuevo mas tarde.',
                type: 'error',
                styling: 'bootstrap3'
            });
        }

    });
}

init();
