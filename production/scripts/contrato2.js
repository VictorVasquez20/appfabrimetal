var tabla, tabcc, tabedificios, tabascensores;
var htmlre, htmlseg, htmltcli, htmltipo, htmlmarca;
var tblascensores;

//funcion que se ejecuta iniciando
function init() {
    mostarform(false);
    listar();

    $('[data-toggle="tooltip"]').tooltip();
    
    $("#rut").focusout(function(){
        rut = $("#rut").val();
        razon = getrazonsocial(rut);
        if(razon === "**"){
            $("#razon").val("");
            $("#razon").attr("readOnly", false);
        }else if(razon === "***"){
            new PNotify({
                title: 'Error!',
                text: 'Rut incorrecto.',
                type: 'error',
                styling: 'bootstrap3'
            });
            $("#rut").focus();
            $("#razon").val("");
            $("#razon").attr("readOnly", true);
        }else{
            $("#razon").val(razon);
            $("#razon").attr("readOnly", true);
        }
    });
    
    
    $("#ncontrato").focusout(function () {
        if ($("#ncontrato").val().search('_') == -1) {
            if ($("#ncontrato").val().length == 15) {
                $.post("../ajax/contratoinstalacion.php?op=validarncontrato", {ncontrato: $("#ncontrato").val()}, function (r) {
                    if (r == '0') {
                        $("#btnGuardar").prop("disabled", false);
                        new PNotify({
                            title: 'Correcto!',
                            text: 'Numero de contrato correcto.',
                            type: 'success',
                            styling: 'bootstrap3'
                        });
                    } else {
                        $("#btnGuardar").prop("disabled", true);
                        new PNotify({
                            title: 'Error!',
                            text: 'Contrato ya existe.',
                            type: 'error',
                            styling: 'bootstrap3'
                        });
                    }
                });

            } else {
                $("#btnGuardar").prop("disabled", true);
                new PNotify({
                    title: 'Error!',
                    text: 'Longitud invalida.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
        } else {
                $("#btnGuardar").prop("disabled", true);
                new PNotify({
                    title: 'Error!',
                    text: 'Debe completar el numero.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
        }
    });

    $('#formulario').on("submit", function (event) {
        event.preventDefault();
        guardar();
    });
    
    $("#formulario1").on("submit", function (event){
        event.preventDefault();
        agregarAscensor();
    });

    $('#formulariocontrato').on("submit", function (event) {
        event.preventDefault();
        editarbd();
    });

    $('#formcliente').on("submit", function (event) {
        event.preventDefault();
        guardarcliente();
    });

    $('#formedificio').on("submit", function (event) {
        event.preventDefault();
        guardaredificio();
    });

    $('#formeditarcontrato').on("submit", function (event) {
        event.preventDefault();
        gaurdareditarcontrato();
    });
    
    
    tblascensores = $("#tblasc").DataTable({
        "paging": false,
        "info": false,
        "bFilter": false,
        "bAutoWidth": false,
        "ordering": false
    });
    
    $("#idedificio").on("change", function(){
        ve = $("#idcontrato").val();
        if($("#idcontrato").val() !== 0 && $("#idcontrato").val() !== null){
            if($("#idedificio").val() !== 0){
                $("#ascensores").show();
                //document.getElementById("ascensores").style.display = 'inline';
                mostrarAscensor($("#idedificio").val(), $("#idcontrato").val());
                $.post("../ajax/edificio.php?op=getregion",{idedificio: $("#idedificio").val()}, function(data){
                    data = JSON.parse(data);
                    $("#region").val(data.idregiones);
                });
                
            }else{
                $("#ascensores").hide();
                //document.getElementById("ascensores").style.display = 'none';
            }
        }else{
            $("#idedificio").val(0);
            $("#idedificio").selectpicker("refresh");
            bootbox.alert("Debe seleccionar un Contrato");
        }
    });
    
    $.post("../ajax/contrato.php?op=selectcontrato", function (r) {
        $("#idcontrato").html(r);
        $("#idcontrato").selectpicker('refresh');
    });

    $.post("../ajax/tcontrato.php?op=selecttcontrato", function (r) {
        $("#tcontrato").html(r);
        $("#tcontrato").selectpicker('refresh');
    });

    $.post("../ajax/periocidad.php?op=selectperiocidad", function (r) {
        $("#idperiocidad").html(r);
        $("#idperiocidad").selectpicker('refresh');
    });

    $.post("../ajax/edificio.php?op=selectedificio", function (r) {
        $("#idedificio").html(r);
        $("#idedificio").selectpicker('refresh');
    });

    $.post("../ajax/cliente.php?op=selectcliente", function (r) {
        $("#idcliente").html(r);
        $("#idcliente").selectpicker('refresh');
    });

    $.post("../ajax/regiones.php?op=selectRegiones", function (r) {
        $("#idregionescli").html(r);
        $("#idregionescli").selectpicker('refresh');
        $("#idregionesedi").html(r);
        $("#idregionesedi").selectpicker('refresh');
        $("#conidregiones").html(r);
        $("#conidregiones").selectpicker('refresh');
    });

    $("#idregionescli").on("change", function (e) {
        $.get("../ajax/comunas.php?op=selectComunasReg", {id: $("#idregionescli").val()}, function (r) {
            $("#idcomunascli").html(r);
            $("#idcomunascli").selectpicker('refresh');
        });
    });

    $("#idregionesedi").on("change", function (e) {
        $.get("../ajax/comunas.php?op=selectComunasReg", {id: $("#idregionesedi").val()}, function (r) {
            $("#idcomunasedi").html(r);
            $("#idcomunasedi").selectpicker('refresh');
        });
    });

    $.post("../ajax/tsegmento.php?op=selecttsegmento", function (r) {
        $("#idtsegmento").html(r);
        $("#idtsegmento").selectpicker('refresh');
    });

    $.post("../ajax/tcliente.php?op=selecttcliente", function (r) {
        $("#idtcliente").html(r);
        $("#idtcliente").selectpicker('refresh');
    });

    $.post("../ajax/tascensor.php?op=selecttascensor", function (r) {
        htmltipo = r;
        $("#idtascensor").html(r);
        $("#idtascensor").selectpicker('refresh');
    });

    $.post("../ajax/marca.php?op=selectmarca", function (r) {
        htmlmarca = r;
        $("#marca").html(r);
        $("#marca").selectpicker('refresh');
    });
    
    $("#marca").on('change', function(){
        $.get("../ajax/modelo.php?op=selectmodelo", {id: $('#marca').val()}, function (r) {
            $('#modelo').html(r);
            $('#modelo').selectpicker('refresh');
        });
    });
    

    $("#ncon_edi").on("change", function (e) {
        $('#contactoedi').empty();
        for (var i = 0; i < $('#ncon_edi').val(); i++) {
            var formedicon = '<div id="contactoedi' + i + '" name="contactoedi' + i + '">' +
                    '<h5>Contacto: ' + (i + 1) + '</h5>' +
                    '<div class="col-md-12 col-sm-12 col-xs-12 form-group">' +
                    '<label for="nombre_conedi' + i + '">Nombre y apellido<span class="required">*</span></label>' +
                    '<input type="text" id="nombre_conedi' + i + '" name="nombre_conedi' + i + '" required="required" class="form-control">' +
                    '</div>' +
                    '<div class="col-md-6 col-sm-12 col-xs-12 form-group">' +
                    '<label for="numero_conedi' + i + '">Numero de contacto<span class="required">*</span></label>' +
                    '<input type="text" id="numero_conedi' + i + '" name="numero_conedi' + i + '" required="required" class="form-control">' +
                    '</div>' +
                    '<div class="col-md-6 col-sm-12 col-xs-12 form-group">' +
                    '<label for="email_conedi' + i + '">Email<span class="required">*</span></label>' +
                    '<input type="text" id="email_conedi' + i + '" name="email_conedi' + i + '" required="required" class="form-control">' +
                    '</div>' +
                    '</div>';
            $('#contactoedi').append(formedicon);

        }
    });

    $("#nascensores").on("change", function (e) {
        if ($("#idedificio").val() != null) {
            $('#ascensores').empty();
            for (var i = 0; i < $('#nascensores').val(); i++) {
                var myvar = '<div id="ascensor' + i + '" name="ascensor' + i + '">' +
                        '<h4><b>Ascensor: ' + (i + 1) + '</b></h4>' +
                        '<div class="col-md-3 col-sm-12 col-xs-12 form-group">' +
                        '<label for="idtascensor' + i + '">Tipo de Ascensor <span class="required">*</span></label>' +
                        '<select class="form-control selectpicker" data-live-search="true" id="idtascensor' + i + '" name="idtascensor' + i + '" required="required"></select>' +
                        '</div>' +
                        '<div class="col-md-3 col-sm-12 col-xs-12 form-group">' +
                        '<label for="marca' + i + '">Marca <span class="required">*</span></label>' +
                        '<div class="row">' +
                        '<div class="col-md-10 col-sm-12 col-xs-12 form-group">' +
                        '<select class="form-control selectpicker" data-live-search="true" id="marca' + i + '" name="marca' + i + '" required="required" onchange="selectmodelo(' + i + ')">' +
                        '</select>' +
                        '</div>' +
                        '<div class="col-md-2 col-sm-12 col-xs-12 form-group">' +
                        '<button class="btn btn-info btn-sm" type="button" onclick="actmarca(' + i + ')"><i class="fa fa-refresh"></i></button>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-md-3 col-sm-12 col-xs-12 form-group">' +
                        '<label for="modelo' + i + '">Modelo <span class="required">*</span></label>' +
                        '<div class="row">' +
                        '<div class="col-md-10 col-sm-12 col-xs-12 form-group">' +
                        '<select class="form-control selectpicker" data-live-search="true" id="modelo' + i + '" name="modelo' + i + '" required="required">' +
                        '</select>' +
                        '</div>' +
                        '<div class="col-md-2 col-sm-12 col-xs-12 form-group">' +
                        '<button class="btn btn-info btn-sm" type="button" onclick="actmodelo(' + i + ')"><i class="fa fa-refresh"></i></button>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-md-3 col-sm-12 col-xs-12 form-group">' +
                        '<label for="ken' + i + '">Codigo FM</label>' +
                        '<input type="text" id="codigo' + i + '" name="codigo' + i + '" class="form-control" onfocusout="VerCodigo(' + i + ')">' +
                        '</div><div class="clearfix"></div>' +
                        '<div class="col-md-3 col-sm-12 col-xs-12 form-group">' +
                        '<label for="ken' + i + '">Codigo Cliente</label>' +
                        '<input type="text" id="codigocli' + i + '" name="codigocli' + i + '" class="form-control">' +
                        '</div>' +
                        '<div class="col-md-3 col-sm-12 col-xs-12 form-group">' +
                        '<label for="ken' + i + '">Codigo Fabricante</label>' +
                        '<input type="text" id="ken' + i + '" name="ken' + i + '" class="form-control">' +
                        '</div>' +
                        '<div class="col-md-3 col-sm-12 col-xs-12 form-group">' +
                        '<label for="ken' + i + '">Ubicacion</label>' +
                        '<input type="text" id="ubicacionasc' + i + '" name="ubicacionasc' + i + '" class="form-control">' +
                        '</div>' +
                        '<div class="col-md-3 col-sm-12 col-xs-12 form-group">' +
                        '<label for="paradas' + i + '">Paradas</label>' +
                        '<input type="text" id="paradas' + i + '" name="paradas' + i + '" class="form-control">' +
                        '</div>' +
                        '<div class="col-md-3 col-sm-12 col-xs-12 form-group">' +
                        '<label for="capkg' + i + '">Capacidad (Kg)</label>' +
                        '<input type="text" id="capkg' + i + '" name="capkg' + i + '" class="form-control">' +
                        '</div>' +
                        '<div class="col-md-3 col-sm-12 col-xs-12 form-group">' +
                        '<label for="capper' + i + '">Capacidad Personas </label>' +
                        '<input type="text" id="capper' + i + '" name="capper' + i + '" class="form-control">' +
                        '</div>' +
                        '<div class="col-md-3 col-sm-12 col-xs-12 form-group">' +
                        '<label for="velocidad' + i + '">Velocidad </label>' +
                        '<input type="text" id="velocidad' + i + '" name="velocidad' + i + '" class="form-control">' +
                        '</div>' +
                        '<div class="col-md-3 col-sm-12 col-xs-12 form-group">' +
                        '<label for="pservicio' + i + '">Puesta Servicio</label>' +
                        '<input type="date" id="pservicio' + i + '" name="pservicio' + i + '" class="form-control"/>' +
                        '</div>' +
                        '<div class="col-md-3 col-sm-12 col-xs-12 form-group">' +
                        '<label for="gtecnica' + i + '">Garantia Tecnica</label>' +
                        '<input type="date" id="gtecnica' + i + '" name="gtecnica' + i + '" class="form-control"/>' +
                        '</div>' +
                        '<div class="col-md-3 col-sm-12 col-xs-12 form-group">' +
                        '<label for="dcs' + i + '">Dcs <span class="required">*</span></label>' +
                        '<select class="form-control selectpicker" data-live-search="true" id="dcs' + i + '" name="dcs' + i + '" required="required">' +
                        '<option value="" selected disabled>Dcs</option>' +
                        '<option value="1">SI</option>' +
                        '<option value="0">NO</option>' +
                        '</select>' +
                        '</div>' +
                        '<div class="col-md-3 col-sm-12 col-xs-12 form-group">' +
                        '<label for="elink' + i + '">Elink <span class="required">*</span></label>' +
                        '<select class="form-control selectpicker" data-live-search="true" id="elink' + i + '" name="elink' + i + '" required="required">' +
                        '<option value="" selected disabled>Elink</option>' +
                        '<option value="1">SI</option>' +
                        '<option value="0">NO</option>' +
                        '</select>' +
                        '</div>' +
                        '</div>' +
                        '<div class="clearfix"></div><div class="ln_solid"></div>';

                $('#ascensores').append(myvar);
                
                $('#codigo'+i+'').inputmask({"mask": "FM999999"}); //specifying options

                $('#dcs' + i + '').selectpicker({
                    liveSearch: false
                });

                $('#elink' + i + '').selectpicker({
                    liveSearch: false
                });

                $('#idtascensor' + i + '').selectpicker({
                    liveSearch: true
                });

                $('#idtascensor' + i + '').html(htmltipo);
                $('#idtascensor' + i + '').selectpicker('refresh');

                $('#marca' + i + '').selectpicker({
                    liveSearch: true
                });
                $('#marca' + i + '').html(htmlmarca);
                $('#marca' + i + '').selectpicker('refresh');

                $('#modelo' + i + '').selectpicker({
                    liveSearch: true
                });

            }
        } else {
            new PNotify({
                title: 'Error!',
                text: 'Debe seleccionar un edificio primero.',
                type: 'error',
                styling: 'bootstrap3'
            });
            $("#nascensores").val("");
            $("#nascensores").selectpicker('refresh');
        }
    });


}


function actualizar(){
    
    $.post("../ajax/marca.php?op=selectmarca", function (r) {
        htmlmarca = r;
        $("#marca").html(r);
        $("#marca").selectpicker('refresh');
    });
    
    $.get("../ajax/modelo.php?op=selectmodelo", {id: $('#marca').val()}, function (r) {
        $('#modelo').html(r);
        $('#modelo').selectpicker('refresh');
    });
    
}

//VALIDO EL KEN TENGA EL FORMATO CORRECTO Y QUE NO EXISTA EN LA BASE DE DATOS
$("#ken").focusout(function () {
    var valor = $("#ken").val();

    if (valor.length <= 8) {
        if (valor.includes('_')) {
            $("#addascensor").attr('disabled', true);
            new PNotify({
                title: 'Error!',
                text: 'El KEN debe tener 8 caracteres.',
                type: 'error',
                styling: 'bootstrap3'
            });

        } else {
            $.post("../ajax/ascensor.php?op=VerificarKEN", {ken: $("#ken").val()}, function (data) {
                data = JSON.parse(data);
                if (data == 1) {
                    new PNotify({
                        title: 'Error!',
                        text: 'El KEN ingresado ya existe!',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                    $("#addascensor").attr('disabled', true);
                } else {
                    $("#addascensor").attr('disabled', false);
                }
            });
        }
    } else {
        $.post("../ajax/ascensor.php?op=VerificarKEN", {ken: $("#ken").val()}, function (data) {
            data = JSON.parse(data);
            if (data == 1) {
                new PNotify({
                    title: 'Error!',
                    text: 'El codigo KEN ya existe!',
                    type: 'error',
                    styling: 'bootstrap3'
                });
                $("#addascensor").attr('disabled', true);
            } else {
                $("#addascensor").attr('disabled', false);
            }
        });
    }
});


// Otras funciones
function limpiar() {

    $("#idcliente").val("");
    $("#idcliente").selectpicker('refresh');
    $("#tcontrato").val("");
    $("#tcontrato").selectpicker('refresh');
    $("#ncontrato").val("");
    $("#nexterno").val("");
    $("#nreferencia").val("");
    $("#idperiocidad").val("");
    $("#idperiocidad").selectpicker('refresh');
    $("#ubicacion").val("");
    $("#fecha").val("");
    $("#fecha_ini").val("");
    $("#fecha_fin").val("");
    $("#nedificios").val("");
    $("#nedificios").selectpicker('refresh');
    $("#idperiocidad").val("");
    $("#idperiocidad").selectpicker('refresh');
    $("#observaciones").val("");
    $("#idedificio").val("");
    $("#idedificio").selectpicker('refresh');
    $("#nascensores").val("");
    $("#nascensores").selectpicker('refresh');
    //$('#ascensores').empty();

    //MODAL CLIENTE
    $("#razon").val("");
    $("#rut").val("");
    $("#callecli").val("");
    $("#numerocli").val("");
    $("#oficinacli").val("");
    $("#idregionescli").val("");
    $("#idregionescli").selectpicker('refresh');
    $("#idcomunascli").empty();
    $("#idcomunascli").val("");
    $("#idcomunascli").selectpicker('refresh');
    $("#idtcliente").val("");
    $("#idtcliente").selectpicker('refresh');

    //MODAL EDIFICIO
    $("#nombreedi").val("");
    $("#calleedi").val("");
    $("#numeroedi").val("");
    $("#idregionesedi").val("");
    $("#idregionesedi").selectpicker('refresh');
    $("#idcomunasedi").empty();
    $("#idcomunasedi").val("");
    $("#idcomunasedi").selectpicker('refresh');
    $("#idtsegmento").val("");
    $("#idtsegmento").selectpicker('refresh');
    $("#corcorreo").val("");
    $("#corcorreo").selectpicker('refresh');
    $("#residente").val("");
    $("#residente").selectpicker('refresh');
    $("#ncon_edi").val("");
    $("#ncon_edi").selectpicker('refresh');
    $("#contactoedi").empty();
}

function limpiarcliente() {
    $("#razon").val("");
    $("#rut").val("");
    $("#callecli").val("");
    $("#numerocli").val("");
    $("#oficinacli").val("");
    $("#idregionescli").val("");
    $("#idregionescli").selectpicker('refresh');
    $("#idcomunascli").empty();
    $("#idcomunascli").val("");
    $("#idcomunascli").selectpicker('refresh');
    $("#idtcliente").val("");
    $("#idtcliente").selectpicker('refresh');
}

function limpiaredificio() {
    $("#nombreedi").val("");
    $("#calleedi").val("");
    $("#numeroedi").val("");
    $("#idregionesedi").val("");
    $("#idregionesedi").selectpicker('refresh');
    $("#idcomunasedi").empty();
    $("#idcomunasedi").val("");
    $("#idcomunasedi").selectpicker('refresh');
    $("#idtsegmento").val("");
    $("#idtsegmento").selectpicker('refresh');
    $("#corcorreo").val("");
    $("#corcorreo").selectpicker('refresh');
    $("#residente").val("");
    $("#residente").selectpicker('refresh');
    $("#ncon_edi").val("");
    $("#ncon_edi").selectpicker('refresh');
    $("#contactoedi").empty();
}

function mostarform(flag) {
    if (flag) {
        $("#formeditarcontrato").hide();
        $("#tabcontrato").hide();
        $("#listadocontratos").hide();
        $("#formulariocontratos").show();
        $("#formclientes").hide();
        $("#formedificios").hide();
        $("#op_agregar").hide();
        $("#op_listar").show();
        $("#btnGuardar").prop("disabled", false);

    } else {
        $("#formeditarcontrato").hide();
        $("#tabcontrato").hide();
        $("#formulariocontratos").hide();
        $("#listadocontratos").show();
        $("#op_agregar").show();
        $("#op_listar").hide();
    }

}


function cancelarform() {
    mostarform(false);
    //document.getElementById("ascensores").style.display = 'none';
    $("#ascensores").hide();
    limpiar();
}

function listar() {
    tabla = $('#tblcontratos').dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'print',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "ajax": {
            url: '../ajax/contrato.php?op=listarcontrato',
            type: "get",
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 15, //Paginacion 10 items
        "order": [[1, "desc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}


function editarbd() {
    $("#btnGuardar").prop("disabled", true);
    var formData = new FormData($("#formulariocontrato")[0]);
    $.ajax({
        url: '../ajax/contrato.php?op=editar',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,

        success: function (datos) {
            bootbox.alert(datos);
            mostarform(false);
            tabla.ajax.reload();
        }
    });
    limpiar();
}

function guardar() {
    $("#btnGuardar").prop("disabled", true);
    var formData = new FormData($("#formulario")[0]);
    $.ajax({
        url: '../ajax/contrato.php?op=guardar',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,

        success: function (datos) {
            if (datos !== 0){
                new PNotify({
                    title: 'Registrado!',
                    text: 'El edificio fue registrado con exito.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
                $.post("../ajax/contrato.php?op=selectcontrato", function (r) {
                    $("#idcontrato").html(r);
                    $("#idcontrato").val(datos);
                    $("#idcontrato").selectpicker('refresh');
                });
                $('.modal-body').css('opacity', '');
                $('#modalContrato').modal('toggle');
            }else{
                new PNotify({
                    title: 'Error!',
                    text: 'Hubo un error al registar. ' + datos ,
                    type: 'error',
                    styling: 'bootstrap3'
                });
                $('.modal-body').css('opacity', '');
                $('#modalContrato').modal('toggle');
            }
        }
    });
    limpiar();
}

function guardarcliente() {
    var formData = new FormData($("#formcliente")[0]);
    $.ajax({
        url: '../ajax/cliente.php?op=guardar',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        beforeSend: function () {
            $("#btnGuardarCli").prop("disabled", true);
            $('.modal-body').css('opacity', '.5');
        },
        success: function (datos) {
            if (!isNaN(datos) || datos != 0) {
                new PNotify({
                    title: 'Registrado!',
                    text: 'El cliente fue registrado con exito.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
                $("#btnGuardarCli").prop("disabled", false);
                $('.modal-body').css('opacity', '');
                $('#modalForm').modal('toggle');
                $.post("../ajax/cliente.php?op=selectcliente", function (r) {
                    $("#idcliente").html(r);
                    $("#idcliente").val(datos);
                    $("#idcliente").selectpicker('refresh');
                });
            } else {
                new PNotify({
                    title: 'Error!',
                    text: 'Hubo un error al registar.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
        }
    });
    limpiarcliente();
}

function guardaredificio() {
    var formData = new FormData($("#formedificio")[0]);
    $.ajax({
        url: '../ajax/edificio.php?op=guardar',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        beforeSend: function () {
            $("#btnGuardarEdi").prop("disabled", true);
            $('.modal-body').css('opacity', '.5');
        },
        success: function (datos) {
            if (datos == 'OK') {
                new PNotify({
                    title: 'Registrado!',
                    text: 'El edificio fue registrado con exito.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
                $("#btnGuardarEdi").prop("disabled", false);
                $('.modal-body').css('opacity', '');
                $('#modalEdificio').modal('toggle');
                $.post("../ajax/edificio.php?op=selectedificio", function (r) {
                    $("#idedificio").html(r);
                    $("#idedificio").selectpicker('refresh');
                });
            } else if (!isNaN(datos) || datos == 0) {
                new PNotify({
                    title: 'Registrado!',
                    text: 'El edificio y sus contactos fueron registrados con exito.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
                $("#btnGuardarEdi").prop("disabled", false);
                $('.modal-body').css('opacity', '');
                $('#modalEdificio').modal('toggle');
                $.post("../ajax/edificio.php?op=selectedificio", function (r) {
                    $("#idedificio").html(r);
                    $("#idedificio").selectpicker('refresh');
                });
            } else if (datos == 'NOK') {
                new PNotify({
                    title: 'Error!',
                    text: 'Hubo un error al registar.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
                $("#btnGuardarEdi").prop("disabled", false);
                $('.modal-body').css('opacity', '');
                $('#modalEdificio').modal('toggle');
            } else {
                new PNotify({
                    title: 'Advertencia!',
                    text: 'Respuesta desconocia del servicio, verificar con administrador del sistema',
                    styling: 'bootstrap3'
                });
                $("#btnGuardarEdi").prop("disabled", false);
                $('.modal-body').css('opacity', '');
                $('#modalEdificio').modal('toggle');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 0) {
                new PNotify({
                    title: 'Error!',
                    text: 'No pudo conectar al servicio.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            } else if (jqXHR.status == 404) {
                new PNotify({
                    title: 'Error!',
                    text: 'Recurso no encontrado [404].',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            } else if (jqXHR.status == 500) {
                new PNotify({
                    title: 'Error!',
                    text: 'Error en el servidor [500].',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            } else if (textStatus === 'parsererror') {
                new PNotify({
                    title: 'Error!',
                    text: 'Objeto Incorrecto.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            } else if (textStatus === 'timeout') {
                new PNotify({
                    title: 'Error!',
                    text: 'Tiempo de espera agotado.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            } else if (textStatus === 'abort') {
                new PNotify({
                    title: 'Error!',
                    text: 'Solicitud abortada.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            } else {
                new PNotify({
                    title: 'Error!',
                    text: 'Error Desconocido: ' + jqXHR.responseText,
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
            $("#btnGuardarEdi").prop("disabled", false);
            $('.modal-body').css('opacity', '');
            $('#modalEdificio').modal('toggle');
        }
    });
    limpiaredificio();
}

function mostar(idcontrato) {
    $("#formeditarcontrato").hide();
    $("#listadocontratos").hide();
    $("#formulariocontratos").hide();
    $("#op_agregar").show();
    $("#op_listar").show();
    $("#tabcontrato").show();

    $.post("../ajax/contrato.php?op=mostrar_contrato", {idcontrato: idcontrato}, function (r) {
        data = JSON.parse(r);
        console.log(data);
        $("#tabncontrato").html("Contrato N°: " + data.dcontrato.ncontrato);
        $("#tabtcontrato").html(data.dcontrato.nombre);
        $("#tabnexterno").html(data.dcontrato.nexterno);
        $("#tabnreferencia").html(data.dcontrato.nreferencia);
        $("#tabubicacion").html(data.dcontrato.ubicacion);
        
        $("#tabperiocidad").html(data.dcontrato.periocidad);
        $("#tabfirma").html(data.dcontrato.firma);
        $("#tabinicio").html(data.dcontrato.inicio);
        $("#tabfin").html(data.dcontrato.fin);
        
        $("#tabregion").html(data.dcontrato.region);
        $("#tabrazon").html(data.dcliente.razon_social);
        $("#tabrut").html(data.dcliente.rut);
        $("#tabdireccion").html(data.dcliente.calle +" "+ data.dcliente.numero);
        
        $("#tabobservaciones").html(data.dcontrato.observaciones);

    });


    tabedificios = $('#tbltabedificios').dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'print',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "ajax": {
            url: '../ajax/contrato.php?op=edificio_contrato',
            type: "POST",
            dataType: "json",
            data: {idcontrato: idcontrato},
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 5, //Paginacion 10 items
        "order": [[1, "desc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();

    tabascensores = $('#tbltabascensores').dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'print',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "ajax": {
            url: '../ajax/contrato.php?op=ascensor_contrato',
            type: "POST",
            dataType: "json",
            data: {idcontrato: idcontrato},
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 5, //Paginacion 10 items
        "order": [[1, "desc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
    
    tabcc = $('#tbltabcentros').dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'print',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "ajax": {
            url: '../ajax/contrato.php?op=cc_contrato',
            type: "POST",
            dataType: "json",
            data: {idcontrato: idcontrato},
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 5, //Paginacion 10 items
        "order": [[1, "desc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();

    $.post("../ajax/contrato.php?op=centrosc_contrato", {idcontrato: idcontrato}, function (data) {
        $("#tabcentroscosto").html(data);
    });
}

function selectmodelo(ascensor) {

    $.get("../ajax/modelo.php?op=selectmodelo", {id: $('#marca' + ascensor + '').val()}, function (r) {
        $('#modelo' + ascensor + '').html(r);
        $('#modelo' + ascensor + '').selectpicker('refresh');
    });
}

function actmarca(ascensor) {

    $.get("../ajax/marca.php?op=selectmarca", function (r) {
        $('#marca' + ascensor + '').html(r);
        $('#marca' + ascensor + '').selectpicker('refresh');
    });
}

function actmodelo(ascensor) {
    $.get("../ajax/modelo.php?op=selectmodelo", {id: $('#marca' + ascensor + '').val()}, function (r) {
        $('#modelo' + ascensor + '').html(r);
        $('#modelo' + ascensor + '').selectpicker('refresh');
    });
}

function VerCodigo(ascensor) {
    if ($('#codigo' + ascensor + '').val().search('_') == -1) {
    if ($('#codigo' + ascensor + '').val().length == 8) {
        $.post("../ajax/ascensor.php?op=VerificarCodigo", {codigo: $('#codigo' + ascensor + '').val()}, function (rsp) {
            if (rsp == '0') {
                $("#btnGuardar").prop("disabled", false);
                new PNotify({
                    title: 'Correcto!',
                    text: 'Codigo FM correcto.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
            } else {
                $("#btnGuardar").prop("disabled", true);
                new PNotify({
                    title: 'Error!',
                    text: 'Codigo FM ya existe.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
        });

    } else if($('#codigo' + ascensor + '').val().length > 1){
        $("#btnGuardar").prop("disabled", true);
        new PNotify({
            title: 'Error!',
            text: 'Longitud invalida.',
            type: 'error',
            styling: 'bootstrap3'
        });
    }
}else{
    $("#btnGuardar").prop("disabled", true);
        new PNotify({
            title: 'Error!',
            text: 'Debe completar el codigo.',
            type: 'error',
            styling: 'bootstrap3'
        });
}
}

function editar2(idcontrato) {
    $.post("../ajax/tcontrato.php?op=selecttcontrato", function (r) {
        $("#ftcontrato").html(r);
        $("#ftcontrato").selectpicker('refresh');
    });

    $.post("../ajax/periocidad.php?op=selectperiocidad", function (r) {
        $("#fidperiocidad").html(r);
        $("#fidperiocidad").selectpicker('refresh');
    });

    $.post("../ajax/regiones.php?op=selectRegiones", function (r) {
        $("#fidregiones").html(r);
        $("#fidregiones").selectpicker('refresh');
    });

    $.post("../ajax/contrato.php?op=formeditar", {idcontrato: idcontrato}, function (data, status) {
        $("#listadocontratos").hide();
        $("#formulariocontratos").hide();
        $("#tabcontrato").hide();
        $("#op_agregar").show();
        $("#op_listar").show();
        $("#formeditarcontrato").show();

        data = JSON.parse(data);
        $("#fidcontrato").val(data.idcontrato);
        $("#ftcontrato").val(data.tcontrato);
        $("#ftcontrato").selectpicker('refresh');
        $("#fncontrato").val(data.ncontrato);
        $("#fnexterno").val(data.nexterno);
        $("#fnreferencia").val(data.nreferencia);
        $("#fubicacion").val(data.ubicacion);
        $("#fidperiocidad").val(data.idperiocidad);
        $("#fcalle").val(data.calle);
        $("#fnumero").val(data.numero);
        $("#foficina").val(data.oficina);
        $("#ffecha").val(data.fecha);
        $("#ffecha_ini").val(data.fecha_ini);
        $("#ffecha_fin").val(data.fecha_fin);
        $("#fobservaciones").val(data.observaciones);
        $("#fidregiones").val(data.idregiones);
        $("#fidregiones").selectpicker('refresh');
        $.get("../ajax/provincia.php?op=selectProvincia", {id: data.idregiones}, function (r) {
            $("#fidprovincias").html(r);
            $("#fidprovincias").val(data.idprovincia);
            $("#fidprovincias").selectpicker('refresh');
        });
        $.get("../ajax/comunas.php?op=selectComunas", {id: data.idprovincia}, function (r) {
            $("#fidcomunas").html(r);
            $("#fidcomunas").val(data.idcomuna);
            $("#fidcomunas").selectpicker('refresh');
        });

    });
}

function editar(idcontrato){
    $.post("../ajax/ascensor.php?op=getedificio", {idcontrato: idcontrato}, function (data, status) {
        data = JSON.parse(data);
        
        mostarform(true);
        $("#ascensores").show();
        //document.getElementById("ascensores").style.display = 'inline';
        mostrarAscensor(data.idedificio, idcontrato);
        $("#idcontrato").val(idcontrato);
        $("#idcontrato").selectpicker('refresh');
        $("#idedificio").val(data.idedificio);
        $("#idedificio").selectpicker('refresh');        
    });
}

function agregarAscensor() {
    var region = $("#region").val();
    var formData = new FormData();
    var idedificio = $("#idedificio").val();
    var idcontrato = $("#idcontrato").val();
    
    var corr = 0;
    
    if ($("#codigo").val() == "" || $("#codigo").val() == "0"){
        corr = getcorrelativo(region);
    }else{
        corr = $("#codigo").val();
    }
    
    if(corr === 0){
        return false;
    }    
    formData.append("codigo", corr);
    formData.append("idtascensor", $("#idtascensor").val());
    formData.append("marca", $("#marca").val());
    formData.append("modelo", $("#modelo").val());
    formData.append("codigocli", $("#codigocli").val());
    formData.append("ken", $("#ken").val());
    formData.append("ubicacion", $("#ubicacionasc").val());
    formData.append("paradas", $("#paradas").val());
    formData.append("capkg", $("#capkg").val());
    formData.append("capper", $("#capper").val());
    formData.append("velocidad", $("#velocidad").val());
    formData.append("pservicio", $("#pservicio").val());
    formData.append("gtecnica", $("#gtecnica").val());
    formData.append("dcs", null);
    formData.append("elink", null);
    formData.append("idventa", null);
    formData.append("idascensor", 0);
    formData.append("valoruf", 0);
    formData.append("valorclp", 0);
    formData.append("idedificio", idedificio);
    formData.append("montosi", 0);
    formData.append("montosn", 0);
    
    formData.append("idcontrato", idcontrato);
    
    $.ajax({
        url: '../ajax/ascensor.php?op=guardaryeditar',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function (datos) {
            if (datos !== '0') {
                $("#btnGuardar").prop("disabled", false);
                new PNotify({
                    title: 'Correcto!',
                    text: 'Ascensor guardado con exito.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
                mostrarAscensor(idedificio, idcontrato);
            } else {
                $("#btnGuardar").prop("disabled", true);
                new PNotify({
                    title: 'Error!',
                    text: 'Ascensor no guardado, verifique que no haya sido ingresado anteriormente.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
                setcorrelativo(region); 
            }
        },
        error: function (datos) {
            text = datos;
        }
    });

    limpiarascensor();
}

function delasc(idascensor) {
    $.post("../ajax/ascensor.php?op=eliminar", {idascensor: idascensor}, function (data, status) {
        if (data !== 0) {
            new PNotify({
                title: 'Correcto!',
                text: 'Ascensor eliminado.',
                type: 'success',
                styling: 'bootstrap3'
            });
            mostrarAscensor($("#idedificio").val(), $("#idcontrato").val());
        } else {
            new PNotify({
                title: 'Error!',
                text: 'Ascensor no eliminado',
                type: 'error',
                styling: 'bootstrap3'
            });
        }
    });
}

function limpiarascensor(){
    $("#idtascensor").val(0);
    $("#idtascensor").selectpicker('refresh');
    $("#marca").val(0);
    $("#marca").selectpicker('refresh');
    $("#modelo").val(0);
    $("#modelo").selectpicker('refresh');
    $("#codigo").val("");
    $("#codigocli").val("");
    $("#ken").val("");
    $("#ubicacionasc").val("");
    $("#paradas").val("");
    $("#capkg").val("");
    $("#capper").val("");
    $("#velocidad").val("");
    $("#pservicio").val("");
    $("#gtecnica").val("");
    $("#dcs").val(0);
    $("#dcs").selectpicker('refresh');
    $("#elink").val(0);
    $("#elink").selectpicker('refresh');
}

function mostrarAscensor(idedificio, idcontrato) {
    tblascensores.rows().remove().draw();
    $.post("../ajax/ascensor.php?op=listarxedificio", {idedificio: idedificio, idcontrato: idcontrato}, function (data4, status) {
        data4 = JSON.parse(data4);
        for (i = 0; i < data4.length; i++) {
            tblascensores.row.add([
                data4[i]["0"],
                data4[i]["1"],
                data4[i]["2"],
                data4[i]["3"],
                data4[i]["4"],
                data4[i]["5"],
                data4[i]["6"],
                data4[i]["7"]
            ]).draw(false);

        }
    });
}

function getcorr(){
    var correlativo = getcorrelativo(3);
    console.log(correlativo);
}

function editarcontrato(idcontrato){
    var formData = new FormData();
    formData.append('idcontrato',idcontrato);
    $.post("../ajax/regiones.php?op=selectRegiones", function (r) {
        $("#editaridregiones").html(r);
        $("#editaridregiones").selectpicker('refresh');
    });
    $.post("../ajax/tcontrato.php?op=selecttcontrato", function (r) {
        $("#editartcontrato").html(r);
        $("#editartcontrato").selectpicker('refresh');
    })
    $.post("../ajax/periocidad.php?op=selectperiocidad", function (r) {
        $("#editaridperiocidad").html(r);
        $("#editaridperiocidad").selectpicker('refresh');
    });
    $.ajax({
        url: '../ajax/contrato.php?op=editarcontrato',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function (datos) {
            var obj = $.parseJSON(datos);
            $("#editarnombrecliente").val(obj.cliente);
            $("#editaridcliente").val(obj.idcliente);
            $("#editaridcontrato").val(obj.idcontrato);
            $("#editaridregiones option[value="+obj.idregiones+"]").attr("selected",true);
            $("#editartcontrato option[value="+obj.tcontrato+"]").attr("selected",true);
            $("#editarncontrato").val(obj.ncontrato);
            $("#editarnexterno").val(obj.nexterno);
            $("#editarubicacion").val(obj.ubicacion);
            $("#editarfecha").val(obj.fechainicial);
            $("#editarfecha_ini").val(obj.fechaini);
            $("#editarfecha_fin").val(obj.fechafin);
            $("#editarnreferencia").val(obj.nreferencia);

            $('#editarrautomatica').append($('<option>', {value: '',text: 'Renovacion Automatica?'}).attr("disabled",true));
            $('#editarrautomatica').append($('<option>', {value: 0,text: 'NO'}));
            $('#editarrautomatica').append($('<option>', {value: 1,text: 'SI'}));
            $("#editaridperiocidad option[value="+obj.idperiocidad+"]").attr("selected",true);
            $("#editarrautomatica option[value="+obj.rautomatica+"]").attr("selected",true);
            $("#editarobservaciones").val(obj.observaciones);
            $('#editaridregiones').selectpicker('refresh')
            $("#editartcontrato").selectpicker('refresh');
            $('#editaridperiocidad').selectpicker('refresh');
            $('#editarrautomatica').selectpicker('refresh');
            //console.log(obj);  
        },
    });
    $('#modalEditarContrato').on('show.bs.modal', function (e) {
        $('#editarrautomatica').selectpicker('refresh');
    }).modal('show')
}

function limpiareditarcontrato(){
    $("#editarnombrecliente").val('');
    $("#editaridcliente").val('');
    $("#editaridregiones").val('');
    $('#editaridregiones').selectpicker('refresh')
    $("#editartcontrato").val('');
    $("#editartcontrato").selectpicker('refresh');
    $("#editarncontrato").val('');
    $("#editarnexterno").val('');
    $("#editarubicacion").val('');
    $("#editarfecha").val('');
    $("#editarfecha_ini").val('');
    $("#editarfecha_fin").val('');
    $("#editaridperiocidad").val('');
    $('#editarrautomatica').empty();
    $('#editarrautomatica').selectpicker('refresh');
    $("#editarobservaciones").val('');
}

function guardareditarcontrato() {
    var formData = new FormData();
    formData.append("idcontrato",$("#editaridcontrato").val());
    formData.append("idcliente",$("#editaridcliente").val());
    formData.append("idregiones",$("#editaridregiones").val());
    formData.append("tcontrato",$("#editartcontrato").val());
    formData.append("ncontrato",$("#editarncontrato").val());
    formData.append("nexterno",$("#editarnexterno").val());
    formData.append("ubicacion",$("#editarubicacion").val());
    formData.append("fecha",$("#editarfecha").val());
    formData.append("fecha_ini",$("#editarfecha_ini").val());
    formData.append("fecha_fin",$("#editarfecha_fin").val());
    formData.append("idperiocidad",$("#editaridperiocidad").val());
    formData.append("rautomatica",$("#editarrautomatica").val());
    formData.append("observaciones",$("#editarobservaciones").val());

    $.ajax({
        url: '../ajax/contrato.php?op=guardareditar',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function (datos) {
            bootbox.alert(datos);
            $("#modalEditarContrato").modal('toggle')
            tabla.ajax.reload();
        }
    });
    limpiareditarcontrato();
}

$('#modalEditarContrato').on('hidden.bs.modal', function (e) {
    $('#editarrautomatica option:selected').removeAttr('selected');
    limpiareditarcontrato();
})

init();