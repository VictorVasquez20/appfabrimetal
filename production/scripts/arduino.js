var tabla;
var refnormal=0,reffalla=0;

//funcion que se ejecuta iniciando
function init(){
	Verificar();
	CargarDatos();
	ListarAlerta();
	
	//Verifica los tiempos de normalidad de los arduinos
	setInterval("Verificar()", 10000);
	
	//Mantiene actulizado el listado de arduinos en falla
	setInterval("Actualizar()", 10000);
}

function ListarAlerta(){
		console.log("Carga Listado");
		GenerarListado();
		$("#Listado").show();
}



function GenerarListado(){
	console.log("Listado Arduinos en falla");
	tabla=$('#tblarduinos').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons:[
			'copyHtml5',
			'print',
			'excelHtml5',
			'csvHtml5',
			'pdf'
		],
		"ajax":{
			url:'../ajax/arduino.php?op=desactivados',
			type:"get",
			dataType:"json",
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 15, //Paginacion 10 items
		"order" : [[0 , "asc"]] //Ordenar en base a la columna 0 descendente
	}).DataTable();
	
}


function CargarDatos(){
	var normal=0,falla=0;
	console.log("Cargando Datos");
	$.post("../ajax/arduino.php?op=ContarEstado", function(data,status){
		data = JSON.parse(data);
        for (var i=0; i<data.aaData.length; i++){
        	if(data.aaData[i] == 1){
        		normal++;
        	}else{
        		falla++;
        	}
        }
        //Actualizamos valores
        $("#num_normal").html(normal);
        $("#num_falla").html(falla);
        
        //Verificamos clases de indicadores
        //Normal
        if(refnormal < normal && refnormal !=0){      	
        	$("#color_normal").attr('class', 'green');
        	$("#icon_normal").attr('class', 'fa fa-sort-asc');    	
        }else if(refnormal > normal && refnormal !=0){
        	$("#color_normal").attr('class', 'red');
        	$("#icon_normal").attr('class', 'fa fa-sort-desc'); 
        }else if(refnormal == 0){
        	$("#color_normal").attr('class', 'gray');
        	$("#icon_normal").attr('class', 'fa fa-sort'); 
        }
        
        //Falla
        if(reffalla < falla && reffalla !=0){     	
        	$("#color_falla").attr('class', 'green');
        	$("#icon_falla").attr('class', 'fa fa-sort-asc');    	
        }else if(reffalla > falla && reffalla !=0){
        	$("#color_falla").attr('class', 'red');
        	$("#icon_falla").attr('class', 'fa fa-sort-desc'); 
        }else if(reffalla == 0){
        	$("#color_falla").attr('class', 'gray');
        	$("#icon_falla").attr('class', 'fa fa-sort'); 
        }
         
        //Calculamos porcentajes
        var por_normal = parseFloat((normal/data.Totalestados)*100).toFixed(2);
        var por_falla = parseFloat((falla/data.Totalestados)*100).toFixed(2);

        //Actualizamos valorres de porcentajes en el DOM
        $("#pornormal").html(por_normal);
        $("#porfalla").html(por_falla);
      
        refnormal=normal;
        reffalla=falla;
        
	})
}

function ActualizarListado(){
	tabla.ajax.reload(null, false);
}

function Verificar(){
	console.log("Verificando estados");
	$.post("../ajax/arduino.php?op=verificar", function(data,status){
		console.log(data);
		})	
}


function Actualizar(){
	CargarDatos();
	ActualizarListado();
}

init();