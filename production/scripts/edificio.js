var tabla, tabcontactos;

//funcion que se ejecuta iniciando
function init(){
	mostarform(false);
	listar();

	$('[data-toggle="tooltip"]').tooltip(); 

	$('#formulario').on("submit", function(event){
		event.preventDefault();
		guardaryeditar();
	});
       
	$.post("../ajax/regiones.php?op=selectRegiones", function(r){
		$("#idregiones").html(r);
		$("#idregiones").selectpicker('refresh');
	});

	$("#idregiones").on("change", function(e){
		$.get("../ajax/provincia.php?op=selectProvincia",{id:$("#idregiones").val()}, function(r){
			$("#idprovincias").html(r);
			$("#idprovincias").selectpicker('refresh');
		});
	});

	$("#idprovincias").on("change", function(e){
		$.get("../ajax/comunas.php?op=selectComunas",{id:$("#idprovincias").val()}, function(r){
			$("#idcomunas").html(r);
			$("#idcomunas").selectpicker('refresh');
		});
	});
        
        
        $.post("../ajax/tsegmento.php?op=selecttsegmento", function(r){
                $("#idtsegmento").html(r);
                $("#idtsegmento").selectpicker('refresh');
	});
        
        
        

}


// Otras funciones
function limpiar(){
	$("#idedificio").val("");	
	$("#nombre").val("");
	$("#calle").val("");
	$("#oficina").val("");
        $("#idregiones").val("");
	$("#idregiones").selectpicker('refresh');
	$("#idprovincias").val("");
	$("#idprovincias").selectpicker('refresh');
	$("#idcomunas").val("");
	$("#idcomunas").selectpicker('refresh');
        $("#idtsegmento").val("");
        $("#idtsegmento").selectpicker('refresh');
        $("#residente").val("");
        $("#residente").selectpicker('refresh');
        $("#coordinacion").val("");
        $("#coordinacion").selectpicker('refresh');
}

function mostarform(flag){
	limpiar();
	if(flag){
		$("#tabedificio").hide();
		$("#listadoedificio").hide();
		$("#formularioedificio").show();
		$("#op_actualizar").hide();
		$("#op_listar").show();
                $("#btnGuardar").prop("disabled", false);
                $("#titulo_pagina").html("Editar Edificio");
	}else{
		$("#tabedificio").hide();
                $("#formularioedificio").hide();
		$("#listadoedificio").show();
		$("#op_actualizar").show();
		$("#op_listar").hide(); 
                $("#titulo_pagina").html("Edificios en Cartera");
	}

}

function cancelarform(){
	limpiar();
	mostarform(false);
}


function listar(){
	tabla=$('#tbledificios').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons:[
			'copyHtml5',
			'print',
			'excelHtml5',
			'csvHtml5',
			'pdf'
		],
		"ajax":{
			url:'../ajax/edificio.php?op=listaredificio',
			type:"get",
			dataType:"json",
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 15, //Paginacion 10 items
		"order" : [[1 , "desc"]] //Ordenar en base a la columna 0 descendente
	}).DataTable();
}


function guardaryeditar(){
	$("#btnGuardar").prop("disabled", true);
	var formData = new FormData($("#formulario")[0]);
	$.ajax({
		url:'../ajax/edificio.php?op=editar',
		type:"POST",
		data:formData,
		contentType: false,
		processData:false,

		success: function(datos){
			bootbox.alert(datos);
			mostarform(false);
			tabla.ajax.reload();
		}
	});
	limpiar();
}

function mostrar(idedificio){

        $("#idedificio_contacto").val(idedificio);     
	$("#listadoedificio").hide();
	$("#formularioedificio").hide();
	$("#op_actualizar").hide();
	$("#op_listar").show();
	$("#tabedificio").show();
        $("#titulo_pagina").html("Mostrar Edificio");

	$.post("../ajax/edificio.php?op=mostrar",{idedificio:idedificio}, function(data){
		data = JSON.parse(data);
                console.log(data);
		$("#tabnombre").html(data.nombre);
                $("#tabsegmento").html(data.segmento);
                $("#tabdireccion").html(data.calle+" "+data.numero);
                $("#taboficina").html(data.oficina);
                $("#tabregion").html(data.region);
                $("#tabprovincia").html(data.provincia);
                $("#tabcomuna").html(data.comuna);              
	});
        
        $.post("../ajax/edificio.php?op=contratos_edificio",{idedificio:idedificio}, function(data){
            $("#tabcontratos").html(data);
	});
        
        
        $.post("../ajax/edificio.php?op=ascensor_edificio",{idedificio:idedificio}, function(data){
            $("#tabascensores").html(data);
	});
        
       
         tabcontactos=$('#tbltabcontactos').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons:[					
			'excelHtml5',
			'pdf'
		],
		"ajax":{
			url:'../ajax/contacto.php?op=listar_contactos',
			type:"POST",                       
			dataType:"json",
                        data:{ "idedificio_contacto":idedificio },
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 5, //Paginacion 5 items
		"order" : [[1 , "desc"]] //Ordenar en base a la columna 0 descendente
	}).DataTable();
        
       
    
//        
//        $.post("../ajax/contrato.php?op=ascensor_contrato",{idcontrato:idcontrato}, function(data){
//            $("#tabascensores").html(data);
//	});
//        
//        $.post("../ajax/contrato.php?op=centrosc_contrato",{idcontrato:idcontrato}, function(data){
//            $("#tabcentroscosto").html(data);
//	});
	
}

function editar(idedificio){
    $.post("../ajax/edificio.php?op=formeditar",{idedificio:idedificio}, function(data,status){
		data = JSON.parse(data);
		mostarform(true);
		$("#idedificio").val(data.idedificio);	
		$("#nombre").val(data.nombre);
		$("#calle").val(data.calle);
		$("#numero").val(data.numero);
                $("#oficina").val(data.oficina);
                $("#idregiones").val(data.idregiones);                
		$("#idregiones").selectpicker('refresh');
		$.get("../ajax/provincia.php?op=selectProvincia",{id:data.idregiones}, function(r){
			$("#idprovincias").html(r);
			$("#idprovincias").val(data.idprovincias);
			$("#idprovincias").selectpicker('refresh');
		});
		$.get("../ajax/comunas.php?op=selectComunas",{id:data.idprovincias}, function(r){
			$("#idcomunas").html(r);
			$("#idcomunas").val(data.idcomunas);
			$("#idcomunas").selectpicker('refresh');
		});
                $("#idtsegmento").val(data.idtsegmento);
                $("#idtsegmento").selectpicker('refresh');
                $("#residente").val(data.residente);
                $("#residente").selectpicker('refresh');
                $("#coordinacion").val(data.coordinacion);
                $("#coordinacion").selectpicker('refresh');
                
	});
}



init();