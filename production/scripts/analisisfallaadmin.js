var tabla;

//funcion que se ejecuta iniciando
function init() {
    listar();

    $('#formAprobar').on("submit", function(event){
		event.preventDefault();
		guardar();
	});
}

function guardar(){
    //$("#enviar").prop("disabled", true);
	var formData = new FormData($("#formAprobar")[0]);
	$.ajax({
		url:'../ajax/analisisfalla.php?op=guardarSup',
		type:"POST",
		data:formData,
		contentType: false,
		processData:false,

		success: function(datos){
			bootbox.alert(datos);
            $('#aprobar').modal('hide');
            listar();	
		}
	});
	//limpiar();
}

function listar(){

    tabla = $('#tblanalisis').dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Bfrtip',
        buttons: Botones,
        "language": Español,
        "ajax": {
            url: '../ajax/analisisfalla.php?op=listaranalisis',
            type: "get",
            data: {},
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 20, //Paginacion 10 items
        "order": [[1, "desc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}

function detalle(falla,req){
    $("#descripcionfalla").val(falla);
    $("#descrequerimiento").val(req);
    $("#detalle-modal").click();
}

function aprobar(id){
    $("#idanalisisfalla").val(id);    
    $("#aprobar-modal").click();
}

function rechazar(id) {
    //$("#enviar").prop("disabled", true);
    bootbox.confirm("¿Desea rechazar la Solicitud de Análisis de Falla N° " + id + "?", function (result) {
        if (result) {
            $.ajax({
                url:'../ajax/analisisfalla.php?op=rechazar&idanalisisfalla='+id,
                type:"GET",
                // data:formData,
                contentType: false,
                processData:false,
        
                success: function(datos){
                    bootbox.alert(datos);
                    listar();
                }
            });
        }
    });
}

function MostrarImagenes(idfalla){
    var formData = new FormData();
    formData.append('id', idfalla);
    formData.append('tabla', 'analisisfalla');
    $.ajax({
        url: '../ajax/docsasociados.php?op=mostrarimagenesAnalisis',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function(datos) {
            data = JSON.parse(datos);
            $("#divimagenes").html(data.imagenes);
            $('#modalImagenes').modal('show');
        }
    });
}

init();