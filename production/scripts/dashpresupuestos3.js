var refdata, arrIn, arrOut;
var ctx, mybarChart, lineChart;

function init() {
	cargartiles();
	GraficoPresupuestosPorMes();
	GraficoReparacionesPorMes();

	setInterval(cargartiles, 1800000);
	setInterval(GraficoPresupuestosPorMes, 1800000);
	setInterval(GraficoReparacionesPorMes, 1800000);
}

function cargartiles() {
	$.post("../ajax/dashPresupuestos2.php?op=ContarDatos", function(data, status) {
		data = JSON.parse(data);

		var porcenSolicitados = Math.round((parseInt(data.solicitados.solicitados) * 100) / parseInt(data.presupuestos.total));
		var porcenInformacion = Math.round((parseInt(data.informacion.informacion) * 100) / parseInt(data.presupuestos.total));
		var porcenInformados = Math.round((parseInt(data.informados.informados) * 100) / parseInt(data.presupuestos.total));
		var porcenProcesados = Math.round((parseInt(data.procesados.procesados) * 100) / parseInt(data.presupuestos.total));
		var porcenEnviados = Math.round((parseInt(data.enviados.enviados) * 100) / parseInt(data.presupuestos.total));
		var porcenAceptados = Math.round((parseInt(data.aceptados.aceptados) * 100) / parseInt(data.presupuestos.total));


		//Actualizamos valores
		$("#total_presupuestos").html(data.presupuestos.total);
		$("#en_proceso").html(data.proceso.proceso);
		$("#enviados").html(data.enviados.enviados);
		$("#aceptados").html(data.aceptados.aceptados);

		$("#total_pendientes").html(data.reparaciones.total);
		$("#planificadas").html(data.planificadas.planificadas);
		$("#finalizadas").html(data.finalizadas.finalizadas);
		$("#facturacion").html(data.facturacion.facturacion);

		$("#num_solicitados").html(data.solicitados.solicitados + ' (' + porcenSolicitados + '%)');
		$("#num_informacion").html(data.informacion.informacion + ' (' + porcenInformacion + '%)');
		$("#num_informados").html(data.informados.informados + ' (' + porcenInformados + '%)');
		$("#num_procesados").html(data.procesados.procesados + ' (' + porcenProcesados + '%)');
		$("#num_enviados").html(data.enviados.enviados + ' (' + porcenEnviados + '%)');
		$("#num_aceptados").html(data.aceptados.aceptados + ' (' + porcenAceptados + '%)');

		$("#barra_solicitados").css("width", porcenSolicitados + "%");
		$("#barra_informacion").css("width", porcenInformacion + "%");
		$("#barra_informados").css("width", porcenInformados + "%");
		$("#barra_procesados").css("width", porcenProcesados + "%");
		$("#barra_enviados").css("width", porcenEnviados + "%");
		$("#barra_aceptados").css("width", porcenAceptados + "%");
	});
}

function GraficoPresupuestosPorMes() {

	var fecha = new Date();
	var anio_busq;
	anio_busq = fecha.getFullYear();
	$("#titulo_grafico_lineal").html("PRESUPUESTOS DEL AÑO: " + anio_busq);

	$.post("../ajax/dashPresupuestos2.php?op=GraficoPresupuestosPorMes", { 'anio_busq': anio_busq }, function(data, status) {
		data = JSON.parse(data);
		arrIn = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
		arrOut = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

		for (i = 0; i < data.ingreso.length; i++) {
			arrIn[parseInt(data.ingreso[i]['mes']) - 1] = data.ingreso[i]['cantidad'];;
		}
		for (i = 0; i < data.salida.length; i++) {
			arrOut[parseInt(data.salida[i]['mes']) - 1] = data.salida[i]['cantidad'];;
		}

		if ($('#lineChart1').length) {
			var ctx = document.getElementById("lineChart1");
			lineChart = new Chart(ctx, {
				type: 'line',
				data: {
					labels: ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"],
					datasets: [{
						label: "Solicitudes",
						backgroundColor: "rgba(150, 141, 0, 0.31)",
						borderColor: "rgba(150, 141, 0, 0.7)",
						pointBorderColor: "rgba(150, 141, 0, 0.7)",
						pointBackgroundColor: "rgba(150, 141, 0, 0.7)",
						pointHoverBackgroundColor: "#fff",
						pointHoverBorderColor: "rgba(220,220,220,1)",
						pointBorderWidth: 1,
						data: arrIn
					},
					{
						label: "Aceptados",
						backgroundColor: "rgba(0,120,150, 0.31)",
						borderColor: "rgba(0,120,150, 0.7)",
						pointBorderColor: "rgba(0,120,150, 0.7)",
						pointBackgroundColor: "rgba(0,120,150, 0.7)",
						pointHoverBackgroundColor: "#fff",
						pointHoverBorderColor: "rgba(220,220,220,1)",
						pointBorderWidth: 1,
						data: arrOut
					},]
				},
				options: {
					legend: {
						display: false
					},
					scales: {
						xAxes: [{
							display: true,
							scaleLabel: {
	    						display: true,
	    						labelString: 'Mes'
							}
						}],
						yAxes: [{
							display: true,
							scaleLabel: {
	    						display: true,
	    						labelString: 'Cantidad de Presupuestos'
							}
						}]
					}
				}
			});
		}
	});
}

function GraficoReparacionesPorMes() {

	var fecha = new Date();
	var anio_busq;
	anio_busq = fecha.getFullYear();
	$("#titulo_grafico_reparacion").html("REPARACIONES DEL AÑO: " + anio_busq);

	$.post("../ajax/dashPresupuestos2.php?op=GraficoReparacionesPorMes", { 'anio_busq': anio_busq },function(data, status) {
		data = JSON.parse(data);
		arrIn = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
		arrOut = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

		for (i = 0; i < data.ingreso.length; i++) {
			arrIn[parseInt(data.ingreso[i]['mes']) - 1] = data.ingreso[i]['cantidad'];;
		}
		for (i = 0; i < data.salida.length; i++) {
			arrOut[parseInt(data.salida[i]['mes']) - 1] = data.salida[i]['cantidad'];;
		}

		if ($('#lineChart2').length) {
			var ctx = document.getElementById("lineChart2");
			lineChart = new Chart(ctx, {
				type: 'line',
				data: {
					labels: ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"],
					datasets: [{
						label: "Solicitudes",
						backgroundColor: "rgba(150, 141, 0, 0.31)",
						borderColor: "rgba(150, 141, 0, 0.7)",
						pointBorderColor: "rgba(150, 141, 0, 0.7)",
						pointBackgroundColor: "rgba(150, 141, 0, 0.7)",
						pointHoverBackgroundColor: "#fff",
						pointHoverBorderColor: "rgba(220,220,220,1)",
						pointBorderWidth: 1,
						data: arrIn
					},
					{
						label: "Finalizados",
						backgroundColor: "rgba(0,120,150, 0.31)",
						borderColor: "rgba(0,120,150, 0.7)",
						pointBorderColor: "rgba(0,120,150, 0.7)",
						pointBackgroundColor: "rgba(0,120,150, 0.7)",
						pointHoverBackgroundColor: "#fff",
						pointHoverBorderColor: "rgba(220,220,220,1)",
						pointBorderWidth: 1,
						data: arrOut
					},]
				},
				options: {
					legend: {
						display: false
					},
					scales: {
						xAxes: [{
							display: true,
							scaleLabel: {
								display: true,
								labelString: 'Mes'
							}
						}],
						yAxes: [{
							display: true,
							scaleLabel: {
    							display: true,
    							labelString: 'Cantidad de Reparaciones'
							}
						}]
					}
				}
			});
		}
	});
}
init();