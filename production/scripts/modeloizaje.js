var tabla;

//funcion que se ejecuta iniciando
function init() {
    mostrarform(false);
    listar();
    
    $.post("../ajax/marca_izaje.php?op=selectmarca", function(e){
        $("#idmarca").html(e);
        $("#idmarca").selectpicker('refresh');
    });
    
    $("#formulario").on("submit", function (e) {
        guardaryeditar(e);
    });
    
    
}


// Otras funciones
function limpiar() {
    $("#idmodelo").val("");
    $("#idmarca").val("");
    $("#idmarca").selectpicker('refresh');
    $("#nombre").val("");

}

function mostrarform(flag) {

    limpiar();
    if (flag) {
        $("#listadomodeloizaje").hide();
        $("#formulariomodeloizaje").show();
        $("#op_agregar").hide();
        $("#op_listar").show();
        $("#btnGuardar").prop("disabled", false);

    } else {
        $("#listadomodeloizaje").show();
        $("#formulariomodeloizaje").hide();
        $("#op_agregar").show();
        $("#op_listar").hide();
    }

}

function cancelarform() {
    limpiar();
    mostrarform(false);
}

function listar() {
    tabla = $('#tblmodeloizaje').dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Bfrtip',
        buttons: Botones,
        "language": Español,
        "ajax": {
            url: '../ajax/modelo_izaje.php?op=listar',
            type: "get",
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 10, //Paginacion 10 items
        "order": [[0, "desc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}

function guardaryeditar(e) {
    e.preventDefault();
    $("#btnGuardar").prop("disabled", true);
    var formData = new FormData($("#formulario")[0]);

    $.ajax({
        url: '../ajax/modelo_izaje.php?op=guardaryeditar',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,

        success: function (datos) {
            if(datos != 0){
                new PNotify({
                    title: 'Correcto!',
                    text: 'guardada con exito.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
            }else{
                new PNotify({
                    title: 'Error!',
                    text: 'no guardada.' + datos,
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
            mostrarform(false);
            tabla.ajax.reload();
        }
    });
    limpiar();
}

function mostrar(idmodelo) {
    $.post("../ajax/modelo_izaje.php?op=mostrar", {idmodelo: idmodelo}, function (data, status) {
        data = JSON.parse(data);
        mostrarform(true);
        
        $("#idmodelo").val(data.idmodelo);
        $("#idmarca").val(data.idmarca);
        $("#idmarca").selectpicker('refresh');
        $("#nombre").val(data.nombre);
    });
}

init();


