var tabla;

function init(){
  
     $.post("../ajax/edificio.php?op=selectedificio", function (r) {
        $("#edificio").html(r);
        $("#edificio").selectpicker('refresh');
    });


    $("#edificio").on("change", function (e) {
        $.post("../ajax/ascensor.php?op=selectasc", {idedificio: $("#edificio").val()}, function (r) {
            $("#ascensor").html(r);
            $("#ascensor").selectpicker('refresh');
        });
    });
    
        $("#formGestion").on("submit", function (e) {
            guardarFormGestion(e);
        });
 
 
        //evento cuando se abre el modal.    
        $('#ModalGestion').on('show.bs.modal', function (event) {
            
            var button = $(event.relatedTarget);

            $("#uniqueid").val(button.data('uniqueid'));
            $("#src").val(button.data('src'));
            $("#dst").val(button.data('dst'));
            $("#calldate").val(button.data('calldate'));
            $("#estado").val(button.data('estado'));
            
            $("#text_src").html(button.data('src'));
            $("#text_dst").html(button.data('dst'));
            $("#text_calldate").html(button.data('calldate'));
            
            if(button.data('estado')==1){
                $("#tituloModal").html("Gestion de Llamada");
                $("#form_ticket").show();
                $("#form_rechazo").hide();
                $("#edificio").prop('required',true);
                $("#ascensor").prop('required',true);
                $("#observacion").prop('required',false);
                
            }else if(button.data('estado')==2){
                $("#tituloModal").html("Rechazo de Llamada");
                $("#form_ticket").hide();
                $("#form_rechazo").show();
                $("#edificio").prop('required',false);
                $("#ascensor").prop('required',false);
                $("#observacion").prop('required',true);
            }
            
            $("#observacion").val("");
            
            //limpiar datos del formulario de ticket
            $("#edificio").val("");
            $("#edificio").selectpicker('refresh');
            $("#ascensor").val("");
            $("#ascensor").selectpicker('refresh');
            $("#descripcion").val(""); 
            $("#nombre").val(""); 
            $("#correo").val("");
            $("#telefono").val("");
            
          });
 
        listar();
        
        setInterval("refrescar()", 150000);
        
}
 
function listar(){
    
	tabla=$('#tbl_gestionllamadas').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons:[			
			'excelHtml5',			
			'pdf'
		],
		"ajax":{
			url:'../ajax/llamadas.php?op=ListarLlamadas',
			type:"get",
			dataType:"json",
                        timeout: 100000,
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 10, //Paginacion 10 items
		"order" : [[1 , "desc"]] //Ordenar en base a la columna 0 descendente
	}).DataTable();
}


function guardarFormGestion(e) {

    e.preventDefault();

            $.ajax({     
                data: $("#formGestion").serialize(),
                type: "POST",
                url: "../ajax/llamadas.php?op=guardarGestion",
                beforeSend: function () {
                    $("#btnGuardarModalGestion").prop("disabled", true);
                    $('.modal-body').css('opacity', '.5');
                }
            })
            .done(function (data, textStatus, jqXHR) {
                if (console && console.log) {
                    console.log("La solicitud se ha completado correctamente.");
                }
                tabla.ajax.reload();
                bootbox.alert(data);
     
                $("#btnGuardarModalGestion").prop("disabled", false);
                $('.modal-body').css('opacity', '');
                $('#ModalGestion').modal('toggle');
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                if (console && console.log) {
                    console.log("La solicitud a fallado: " + textStatus);
                }
            });

}
 
function refrescar(){
    tabla.ajax.reload();
    //alert('refrescar tabla');
}

init();