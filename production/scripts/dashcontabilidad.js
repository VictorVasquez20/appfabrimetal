var arrmeses = ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"];
var lineChart, mybarChart , mybarChart2;

function init(){
    grafico();
    cargadatos();
    cargaguias();
    $(document).ready(function(){
         new PNotify({
            title: 'INFORMACIÓN!',
            text: 'Proyecto iniciado a partir de mayo de 2019.',
            type: 'info',
            styling: 'bootstrap3'
        });
    });    
    
    setInterval('ActializarGraficos()', 300000);
    
}

function actualiza(){
    
}

function cargadatos() {
    $.post("../ajax/dashcontabilidad.php?op=indicadores", function (data, status) {
        data = JSON.parse(data);
        //Actualizamos valores
        
        var sumtotal = 0, summant = 0, sumrep = 0,sumemerge = 0;
        var sumtotalfac = 0, summantfac = 0, sumrepfac = 0,sumemergefac = 0;
        
        for (i = 0; i < data.length; i++) {
            
            if(data[i]["1"] == 1 || data[i]["1"] == 4 | data[i]["1"] == 5 | data[i]["1"] == 6){
                //reparacion
                sumrep = parseFloat(sumrep) + parseFloat(data[i]["2"]);
                sumrepfac = parseFloat(sumrepfac) + parseFloat(data[i]["3"]);
            }else if(data[i]["1"] == 2){
                //emergencia
                sumemerge = parseFloat(sumemerge) + parseFloat(data[i]["2"]);
                sumemergefac = parseFloat(sumemergefac) + parseFloat(data[i]["3"]);
            }else if(data[i]["1"] == 3){
                //mantencion
                summant = parseFloat(summant) + parseFloat(data[i]["2"]);
                summantfac = parseFloat(summantfac) + parseFloat(data[i]["3"]);
            }
            
            sumtotal = parseFloat(sumtotal) + parseFloat(data[i]["2"]);
            sumtotalfac = parseFloat(sumtotalfac) + parseFloat(data[i]["3"]);
            
        }
        //TOTAL
        $("#tot").empty();
        $("#tot").html(sumtotal);
        
        $("#totfact").empty();
        $("#totfact").html(sumtotalfac);
        
        $("#totporc").empty();
        if(sumtotal != 0){
            $("#totporc").html(parseFloat((sumtotalfac / sumtotal) * 100).toFixed(2) + "%");
        }else{
            $("#totporc").html("0%");
        }
        
        
        
        //MANTENCION
        $("#mant").empty();
        $("#mant").html(summant);
        
        $("#mantfact").empty();
        $("#mantfact").html(summantfac);
        
        $("#mantporc").empty();
        if(summant != 0){
            $("#mantporc").html(parseFloat((summantfac / summant) * 100).toFixed(2) + "%");
        }else{
            $("#mantporc").html("0%");
        }
        
        
        //REPARACION
        $("#rep").empty();
        $("#rep").html(sumrep);
        
        $("#repfact").empty();
        $("#repfact").html(sumrepfac);
        
        $("#repporc").empty();
        if(sumrep != 0){
            $("#repporc").html(parseFloat((sumrepfac / sumrep) * 100).toFixed(2) + "%");
        }else{
            $("#repporc").html("0%");
        }
        
        
        //EMERGENCIA
        $("#emer").empty();
        $("#emer").html(sumemerge);
        
        $("#emerfact").empty();
        $("#emerfact").html(sumemergefac);
        
        $("#emerporc").empty();
        if(sumemerge != 0){
            $("#emerporc").html(parseFloat((sumemergefac / sumemerge) * 100).toFixed(2) + "%");
        }else{
            $("#emerporc").html("0%");
        }
        
        
    });
}

function cargaguias() {
    $.post("../ajax/dashgse.php?op=datostodos", {"idtservicios": '1,2,3,4,5,6'}, function (data, status) {
        data = JSON.parse(data);
        //Actualizamos valores
        
        var gdia = 0, gproceso = 0, gcompleta = 0, gcerrada = 0, gcerradaSF = 0;
        
        for(i = 0; i < data.length; i++){
            gdia += parseInt(data[i]['2']);
            gproceso += parseInt(data[i]['3']);
            gcompleta += parseInt(data[i]['4']);
            gcerrada += parseInt(data[i]['5']);
            gcerradaSF += parseInt(data[i]['8']);
        }
        
        $("#mdia").empty();
        $("#mdia").html(gdia);

        //$("#mmes").empty();
        //$("#mmes").html(data.guiames);

        $("#mproceso").empty();
        $("#mproceso").html(gproceso);

        $("#mcompleto").empty();
        $("#mcompleto").html(gcompleta);

        $("#mcerrado").empty();
        $("#mcerrado").html(gcerrada);
        
        $("#mcerradosf").empty();
        $("#mcerradosf").html(gcerradaSF);
    });
}

function grafico() {
    $.post("../ajax/dashcontabilidad.php?op=grafico", function (data, status) {
        data = JSON.parse(data);
        arrmatn = [0,0,0,0,0,0,0,0,0,0,0,0,0];
        arrmant2 = [0,0,0,0,0,0,0,0,0,0,0,0,0];
        
        arrrep = [0,0,0,0,0,0,0,0,0,0,0,0,0];
        arrrep2 = [0,0,0,0,0,0,0,0,0,0,0,0,0];
        
        arremer = [0,0,0,0,0,0,0,0,0,0,0,0,0];
        arremer2 = [0,0,0,0,0,0,0,0,0,0,0,0,0];
        
        var fecha = new Date();
        mesact = fecha.getMonth();
        
        messtr = arrmeses[mesact];
        
        $("#mesact").empty();
        $("#mesact").html(" (mes " + messtr + ")");
        
        for (i = 0; i < data.length; i++) {
            
            if(data[i]["4"] == 1 || data[i]["4"] == 4 | data[i]["4"] == 5 | data[i]["4"] == 6){
                //reparacion
                arrrep[arrmeses.indexOf(data[i]['1'].substr(0,3))] = data[i]['2'];  
                arrrep2[arrmeses.indexOf(data[i]['1'].substr(0,3))] = data[i]['3'];
            }else if(data[i]["4"] == 2){
                //emergencia
                arremer[arrmeses.indexOf(data[i]['1'].substr(0,3))] = data[i]['2'];  
                arremer2[arrmeses.indexOf(data[i]['1'].substr(0,3))] = data[i]['3'];
            }else if(data[i]["4"] == 3){
                //mantencion
                arrmatn[arrmeses.indexOf(data[i]['1'].substr(0,3))] = data[i]['2'];  
                arrmant2[arrmeses.indexOf(data[i]['1'].substr(0,3))] = data[i]['3'];
            }
        }
        
        if ($('#grafico').length) {
            var ctx = document.getElementById("grafico");
            var lineChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: arrmeses,
                    datasets: [{
                            label: "Guias emitidas",
                            backgroundColor: "rgba(52,152,219, 0.31)",
                            borderColor: "rgba(52,152,219, 0.7)",
                            pointBorderColor: "rgba(52,152,219, 0.7)",
                            pointBackgroundColor: "rgba(52,152,219, 0.7)",
                            pointHoverBackgroundColor: "#fff",
                            pointHoverBorderColor: "rgba(220,220,220,1)",
                            pointBorderWidth: 1,
                            data: arrmatn
                        },
                        {
                            label: "Guias facturadas",
                            backgroundColor: "rgba(3, 88, 106, 0.3)",
                            borderColor: "rgba(3, 88, 106, 0.70)",
                            pointBorderColor: "rgba(3, 88, 106, 0.70)",
                            pointBackgroundColor: "rgba(3, 88, 106, 0.70)",
                            pointHoverBackgroundColor: "#fff",
                            pointHoverBorderColor: "rgba(151,187,205,1)",
                            pointBorderWidth: 1,
                            data: arrmant2
                        }]
                },
                options: {
                    legend: {
                        display: false
                    }
                }
            });

        }
                
        if ($('#reparacion').length) {
            var ctx = document.getElementById("reparacion");
            var mybarChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: arrmeses,
                    datasets: [{
                            label: "guias emitidas",
                            backgroundColor: "#3498DB",
                            data: arrrep
                        },{
                            label: "guias facturadas",
                            backgroundColor: "#455C73",
                            data: arrrep2
                        }]
                },
                options: {
                    legend: {
                        display: false
                    },
                    scales: {
                        yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                    }
                }
            });
        }
        
        if ($('#emergencia').length) {
            var ctx = document.getElementById("emergencia");
            var mybarChart2 = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: arrmeses,
                    datasets: [{
                            label: "guias emitidas",
                            backgroundColor: "#3498DB",
                            data: arremer
                        },{
                            label: "guias facturadas",
                            backgroundColor: "#455C73",
                            data: arremer2
                        }]
                },
                options: {
                    legend: {
                        display: false
                    },
                    scales: {
                        yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                    }
                }
            });
        }
    });
}


function ActializarGraficos() {
    cargadatos();
    cargaguias();
    $.post("../ajax/dashcontabilidad.php?op=grafico", function (data, status) {
        data = JSON.parse(data);
        arrmatn = [0,0,0,0,0,0,0,0,0,0,0,0,0];
        arrmant2 = [0,0,0,0,0,0,0,0,0,0,0,0,0];
        
        arrrep = [0,0,0,0,0,0,0,0,0,0,0,0,0];
        arrrep2 = [0,0,0,0,0,0,0,0,0,0,0,0,0];
        
        arremer = [0,0,0,0,0,0,0,0,0,0,0,0,0];
        arremer2 = [0,0,0,0,0,0,0,0,0,0,0,0,0];
    
        for (i = 0; i < data.length; i++) {
            
            if(data[i]["4"] == 1 || data[i]["4"] == 4 | data[i]["4"] == 5 | data[i]["4"] == 6){
                //reparacion
                arrrep[arrmeses.indexOf(data[i]['1'].substr(0,3))] = data[i]['2'];  
                arrrep2[arrmeses.indexOf(data[i]['1'].substr(0,3))] = data[i]['3'];
            }else if(data[i]["4"] == 2){
                //emergencia
                arremer[arrmeses.indexOf(data[i]['1'].substr(0,3))] = data[i]['2'];  
                arremer2[arrmeses.indexOf(data[i]['1'].substr(0,3))] = data[i]['3'];
            }else if(data[i]["4"] == 3){
                //mantencion
                arrmatn[arrmeses.indexOf(data[i]['1'].substr(0,3))] = data[i]['2'];  
                arrmant2[arrmeses.indexOf(data[i]['1'].substr(0,3))] = data[i]['3'];
            }
        }
        
        //mantencion
        lineChart.data.datasets[0]['data'] = arrmatn;
        lineChart.data.datasets[1]['data'] = arrmant2;
        lineChart.update();
        
        //reparacion
        mybarChart.data.datasets[0]['data'] = arrrep;
        mybarChart.data.datasets[1]['data'] = arrrep2;
        mybarChart.update();
        
        //emergencia
        mybarChart2.data.datasets[0]['data'] = arremer;
        mybarChart2.data.datasets[1]['data'] = arremer2;
        mybarChart2.update();
        
    });
}


init();