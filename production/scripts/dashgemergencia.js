var tabla;

//funcion que se ejecuta iniciando
function init() {
    mostarform();
    listar();
    setInterval("Actualizar()", 10000);
    $('[data-toggle="tooltip"]').tooltip();
}



function mostarform() {
        $("#listadoguias").show();
}

function cancelarform() {
    mostarform(false);
}


function listar() {
    tabla = $('#tblguias').dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Bfrtip',
        buttons: [
        ],
        "ajax": {
            url: '../ajax/servicio.php?op=dashemergencia',
            type: "get",
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 20, //Paginacion 10 items
        "order": [[0, "desc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}



function Actualizar() {
    tabla.ajax.reload();
}


init();