function init() {
	calendario();
}

function calendario() {
	var vistaDefecto, tituloDias;
	if (window.matchMedia("(orientation: portrait)").matches) {
		vistaDefecto = 'listWeek'; // valor por defecto para moviles
		tituloDias = 'ddd';
	}

	if (window.matchMedia("(orientation: landscape)").matches) {
		vistaDefecto = 'listWeek'; // valor por defecto para escritorio
		tituloDias = 'ddd D-MMM';
	}
	
	var dt = new Date();
	var time = (dt.getHours() - 1) + ":00:00";
	var date = new Date(),
	d = date.getDate(),
	m = date.getMonth(),
	y = date.getFullYear(),
	started,
	categoryClass;

	var calendar = $('#calendario').fullCalendar({
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'listDay,listWeek,listMonth'
		},
		defaultView: vistaDefecto,
		locale: 'es',
		buttonText: {
			prev: "Ant",
			next: "Sig",
			today: "Hoy",
			month: "Mes",
			week: "Semana",
			day: "Día",
			listDay: "Día",
			listWeek: "Semana",
			listMonth: "Mes",
		},
		noEventsMessage:"No hay nada para mostrar",
		closeText: "Cerrar",
		prevText: "&#x3C;Ant",
		nextText: "Sig&#x3E;",
		currentText: "Hoy",
		monthNames: ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"],
		monthNamesShort: ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"],
		dayNames: ["domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado"],
		dayNamesShort: ["dom", "lun", "mar", "mié", "jue", "vie", "sáb"],
		dayNamesMin: ["D", "L", "M", "X", "J", "V", "S"],
		weekHeader: "Sm",
		dateFormat: "dd/mm/yy",
		firstDay: 1,
		isRTL: !1,
		showMonthAfterYear: !1,
		yearSuffix: "",
		allDaySlot: false,
		eventLimitText: "más",
		displayEventTime: false,
		views: {
			month: {
				columnFormat: 'ddd'
			},
			week: {
				columnFormat: tituloDias,
				titleFormat: 'D MMMM, YYYY',
			},
			day: {
				columnFormat: 'dddd D',
				titleFormat: 'D MMMM, YYYY',
			}
		},
		businessHours: {
			start: '08:00', // hora final
			end: '17:30', // hora inicial
			dow: [1, 2, 3, 4, 5] // dias de semana, 0=Domingo
		},
		hiddenDays: [6, 0],
		timeFormat: 'H(:mm)',
		scrollTime: time, //empieza el scroll desde 1 hora antes a la actual. Ejemplo sin son las 15:30 el scroll empieza desde las 14:00
		slotDuration: '00:15:00', // cuadros cada 15 minutos
		slotLabelFormat: "HH:mm", //muestra la hora en formato HH:mm        
		editable: false,
		selectable: false,
		eventLimit: false,
		selectHelper: false,
		events: "../ajax/reparacioncalendario.php?op=eventos",
		eventClick: function(calEvent, jsEvent, view) {
			mostrar(calEvent.url);
			return false;
		},
		eventRender: function(eventObj, $element) {
			$element.popover({
				title: eventObj.title,
				content: eventObj.description,
				trigger: 'hover',
				placement: 'top',
				container: 'body'
			});
		},
	});
}

function limpiar() {
	$("#codAscensor").val("");
	$("#numPresupuesto").val("");
	$("#reqTime").val("");
	$("#ediNombre").val("");
	$("#edfDireccion").val("");
}


function mostrar(dir) {
	$.ajax({ 
		url: dir,
		type: 'get',
		contentType: false,
		processData: false,
		cache: false,
		beforeSend: function(){ 
			$("#extraModal").modal();
		},
		success: function(data){ 
			var obj = $.parseJSON(data);
			$("#codAscensor").val(obj.codigo);
			$("#numPresupuesto").val(obj.npresupuesto);
			$("#reqTime").val(new Date(Date.parse(obj.inicio_reparacion.split(" ")[0])).format("d-m-Y"));
			$("#ediNombre").val(obj.nomEdificio);
			$("#edfDireccion").val(obj.direccion);
		}
	});
}

$('#extraModal').on('hidden.bs.modal', function () {
	limpiar();
})

init();