var tabla;


//funcion que se ejecuta iniciando
function init() {
    mostarform(false);
    listar();

    $('[data-toggle="tooltip"]').tooltip();

    $("#formulario").on("submit", function (e) {
        generarticket(e);
    })

    $.post("../ajax/edificio.php?op=selectedificio", function (r) {
        $("#idedificio").html(r);
        $("#idedificio").selectpicker('refresh');
    });


    $("#idedificio").on("change", function (e) {
        $.post("../ajax/ascensor.php?op=selectasc", {idedificio: $("#idedificio").val()}, function (r) {
            $("#idascensor").html(r);
            $("#idascensor").selectpicker('refresh');
        });
    });

    $.post("../ajax/tecnico.php?op=selecttecnico", function (r) {
        $("#tecnico").html(r);
        $("#tecnico").selectpicker('refresh');
    });

    $("#formulariotecnico").on('submit', function (e) {
        e.preventDefault();
        asignatecnico();
    });
    
    setInterval("reload()", 120000);
}

function reload(){
     tabla.ajax.reload();
}
// Otras funciones
function limpiar() {
    $("#idedificio").val("");
    $("#idedificio").selectpicker('refresh');
    $("#idascensor").val("");
    $("#idascensor").selectpicker('refresh');
    $("#descripcion").val("");
    $("#nombre").val("");
    $("#apellido").val("");
    $("#email").val("");
    $("#telefono").val("");
}

function mostarform(flag) {
    limpiar();
    if (flag) {
        $("#listadotickets").hide();
        $("#formulariotickets").show();
        $("#op_agregar").hide();
        $("#op_listar").show();
        $("#btnGuardar").prop("disabled", false);
    } else {
        $("#listadotickets").show();
        $("#formulariotickets").hide();
        $("#op_agregar").show();
        $("#op_listar").hide();
    }
}

function cancelarform() {
    limpiar();
    mostarform(false);
}

function listar() {
    tabla = $('#tbltickets').dataTable({
        "responsive": true,
        "aProcessing": true,
        "aServerSide": true,
        "scrollX": false,
        dom: 'Bfrtip',
        buttons: Botones,
        "language": Español,
        "ajax": {
            url: '../ajax/ticket.php?op=listar',
            type: "get",
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 15, //Paginacion 10 items
        "order": [[1, "desc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}

function generarticket(e) {
    e.preventDefault();
    $("#btnGuardar").prop("disabled", true);
    var formData = new FormData($("#formulario")[0]);
    $.ajax({
        url: '../ajax/ticket.php?op=registrar',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,

        success: function (datos) {
            bootbox.alert(datos);
            mostarform(false);
            tabla.ajax.reload();
        }
    });
    limpiar();
}

function mostrar(idticket) {
    $("#agregatecnico-modal").click();
    $.post("../ajax/ticket.php?op=mostrar", {idticket: idticket}, function (data) {
        data = JSON.parse(data);
        $("#idticket_text").html(data.idticket);
        $("#nombre_text").html(data.nombre);
        $("#telefono_text").html(data.telefono);
        $("#email_text").html(data.email);
        $("#edificio_text").html(data.edificio);
        $("#calle_text").html(data.calle);
        $("#numero_text").html(data.numero);
        $("#comuna_nombre_text").html(data.comuna_nombre);
        $("#codigocli_text").html(data.codigocli);
        $("#descripcion_text").html(data.descripcion);
        $("#fecha_text").html(data.fecha);
        $("#hora_text").html(data.hora);
  
        $("#ticket").val(idticket);

        $("#tecnico").val(data.idtecnico);
        $("#tecnico").selectpicker('refresh');
    });
}

function asignatecnico() {
    $("#btnGuardar").prop("disabled", true);
    var formData = new FormData();

    formData.append("idticket", $("#ticket").val());
    formData.append("idtecnico", $("#tecnico").val());
    formData.append("estado", 1);

    $.ajax({
        url: '../ajax/ticket.php?op=asignatecnico',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,

        success: function (datos) {
            console.log(datos);
            if (datos) {
                
                $(".close").click();
                 new PNotify({
                    title: 'Correcto!',
                    text: 'Técnico asignado correctamente.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
                
                $.post("../ajax/ticket.php?op=email", 
                    {idticket: $("#ticket").val(), idtecnico: $("#tecnico").val()}, 
                    function (data2) {
                   
                    });
                    
            } else {
                new PNotify({
                    title: 'Error!',
                    text: 'Tecnico no asignado. intentelo de nuevo mas tarde.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
            tabla.ajax.reload();
        }
    });
}

init();