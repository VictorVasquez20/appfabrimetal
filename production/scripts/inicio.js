/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function init(){
    indicadorsoladquisicion();
}

function indicadorsoladquisicion(){
    html = "";
    //COTIZACIONES
    $.post("../ajax/soladquisicion.php?op=indicadores", (data, status) => {
        data = JSON.parse(data);
        if(data.length != 0){
            for (i = 0; i < data.length; i++) {
                switch(data[i]['estado']){
                    case '0':
                        html+= '<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">'
                            +  '     <div class="tile-stats">'
                            +  '            <div class="icon"><i class="fa fa-file"></i></div>'
                            +  '            <div class="count" id="bor">' + data[i]['cont'] + '</div>'
                            +  '            <a href="soladquisiciones.php?search=borrador"><h3>En Borrador</h3></a>'
                            +  '        </div>'
                            +  '    </div>';

                        break;    
                    case '1':
                        html+= '<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">'
                            +  '<div class="tile-stats">'
                            +  '    <div class="icon"><i style="color:#FFD119;" class="fa fa-caret-square-o-right"></i></div>'
                            +  '    <div class="count" id="rev">' + data[i]['cont'] + '</div>'
                            +  '    <a href="soladquisiciones.php?search=REVISIÓN"><h3 style="color:#FFD119;">En Revisión</h3></a>'
                            +  '</div>'
                            +  '</div>';
                        break;
                    case '2':
                        html+= '<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">'
                            +  '<div class="tile-stats">'
                            +  '    <div class="icon"><i style="color:#060;" class="fa fa-check-square-o"></i></div>'
                            +  '    <div class="count" id="fin">' + data[i]['cont'] + '</div>'
                            +  '    <a href="soladquisiciones.php?search=ACEPTADO"><h3 style="color:#060;">Aceptadas</h3></a>'
                            +  '</div>'
                            +  '</div>';
                        break;
                    case '3':
                        html+= '<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">'
                            +  '<div class="tile-stats">'
                            +  '    <div class="icon"><i style="color:#E74C3C;" class="fa fa-times-circle-o"></i></div>'
                            +  '    <div class="count" id="rech">' + data[i]['cont'] + '</div>'
                            +  '    <a href="soladquisiciones.php?search=rechazada"><h3 style="color:#E74C3C;">Rechazadas</h3></a>'
                            +  '</div>'
                            +  '</div>';
                        break;
                    default:
                        html+= '<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">'
                            +  '    <div class="tile-stats">'
                            +  '        <div class="icon"><i style="color:#066;" class="fa fa-send-o"></i></div>'
                            +  '        <div class="count" id="env">' + data[i]['cont'] + '</div>'
                            +  '        <a href="aprosoladquisicion.php"><h3 style="color:#066;">Por Revisar</h3></a>'
                            +  '    </div>'
                            +  '</div>';
                        break;
                }
            }
            
            $("#indicadores").show();
            $("#contadores").empty();
            $("#contadores").append(html);
            
        }else{
            $("#indicadores").hide();
        }
        
    });
}

init();

