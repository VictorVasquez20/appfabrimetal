var tabla;

//funcion que se ejecuta iniciando
function init() {
    mostrarform(false);
    listar();
    $("#formulario").on("submit", function (e) {
        e.preventDefault();
        guardaryeditar();
    });
}


// Otras funciones
function limpiar() {
    $("#idcondicionpago").val("");
    $("#nombre").val("");
    $("#condicion").prop("checked", false);
}

function mostrarform(flag) {
    limpiar();
    if (flag) {
        $("#listadocondicion").hide();
        $("#formulariocondicion").show();
        $("#op_agregar").hide();
        $("#op_listar").show();
        $("#btnGuardar").prop("disabled", false);

    } else {
        $("#listadocondicion").show();
        $("#formulariocondicion").hide();
        $("#op_agregar").show();
        $("#op_listar").hide();
    }

}

function cancelarform() {
    limpiar();
    mostrarform(false);
}

function listar() {
    tabla = $('#tblcondicion').dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Bfrtip',
        buttons: Botones,
        "language": Español,
        "ajax": {
            url: '../ajax/condicionpago.php?op=listar',
            type: "get",
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 10, //Paginacion 10 items
        "order": [[1, "desc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}

function guardaryeditar() {
    $("#btnGuardar").prop("disabled", true);
    var formData = new FormData($("#formulario")[0]);
    $.ajax({
        url: '../ajax/condicionpago.php?op=guardaryeditar',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,

        success: function (datos) {
            //bootbox.alert(datos);
            if (datos != '0') {
                $("#btnGuardar").prop("disabled", false);
                new PNotify({
                    title: 'Correcto!',
                    text: 'Categoria guardada con exito.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
            } else {
                $("#btnGuardar").prop("disabled", true);
                new PNotify({
                    title: 'Error!',
                    text: 'Categoria no guardada.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
            mostrarform(false);
            tabla.ajax.reload();
        }
    });
    limpiar();
}

function mostrar(idcondicionpago){
    $.post("../ajax/condicionpago.php?op=mostrar",{"idcondicionpago" : idcondicionpago}, function(data){
        data = JSON.parse(data);
        mostrarform(true);

        $("#idcondicionpago").val(data.idcondicionpago);
        $("#nombre").val(data.nombre);
        
        if(data.condicion == 1){
            $("#condicion").attr("checked",true);
            $('.js-switch').trigger('click');
        }else{
            $("#condicion").attr("checked",false);
        }		
    });
}

init();