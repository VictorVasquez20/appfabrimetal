var tabcontactos;

function init_contacto(){
           
       $('#formcontacto').on("submit", function(event){
		event.preventDefault();
		guardarContacto();
	});
               
        $('#modalFormContacto').on('show.bs.modal', function (event) {
            
         if($(event.relatedTarget).data('idcontacto') !=""){ // si esta editando el contacto.
             
            $.post( "../ajax/contacto.php?op=mostrarContacto", 
                { idcontacto: $(event.relatedTarget).data('idcontacto') }, 
                function( data ) {            
                    $("#idcontacto").val(data.idcontacto);
                    $("#nombre_contacto").val(data.nombre);
                    $("#cargo_contacto").val(data.cargo);
                    $("#email_contacto").val(data.email);
                    $("#telefonomovil_contacto").val(data.telefonomovil);
                    $("#telefonoresi_contacto").val(data.telefonoresi);
                  }, "json");
              }     
              
        });
           
    
        $('#modalFormContacto').on('hidden.bs.modal', function (e) {
            limpiarformContacto();
        });
        
}

function desactivar_contacto(idcontacto){

	bootbox.confirm("Esta seguro que quiere eliminar el contacto?", function(result){
		if(result){
			$.post("../ajax/contacto.php?op=desactivar_contacto",{idcontacto:idcontacto}, function(e){
                            bootbox.alert(e);
                            tabcontactos.ajax.reload();
			})	
		}
	})
}

function activar_contacto(idcontacto){

	bootbox.confirm("Esta seguro que quiere activar el contacto?", function(result){
		if(result){
			$.post("../ajax/contacto.php?op=activar_contacto",{idcontacto:idcontacto}, function(e){
				bootbox.alert(e);
				tabcontactos.ajax.reload();
			});	
		}
	});
}


function limpiarformContacto(){
    
        $("#idcontacto").val("");
        $("#nombre_contacto").val("");
        $("#cargo_contacto").val("");
        $("#email_contacto").val("");
        $("#telefonomovil_contacto").val("");
        $("#telefonoresi_contacto").val("");
        
}

function guardarContacto() {
    
    var formData = new FormData($("#formcontacto")[0]);
    
    $.ajax({
        url: '../ajax/contacto.php?op=guardaryeditar',
        type: "POST",
        data: formData,
        dataType:"json",
        contentType: false,
        processData: false,
        beforeSend: function () {
            
            $("#btnGuardarContacto").prop("disabled", true);
            
            $('.modal-body').css('opacity', '.5');
            
        },
        success: function (datos) {
            
            if (parseInt(datos.estatus) == 1) {
                
                new PNotify({
                    title: 'Notificacion',
                    text: datos.mensaje,
                    type: 'success',
                    styling: 'bootstrap3'
                });
                
                $("#btnGuardarContacto").prop("disabled", false);
                
                $('.modal-body').css('opacity', '');
                
                $('#modalFormContacto').modal('toggle');
                
                tabcontactos.ajax.reload();
                
            } else if (parseInt(datos.estatus) == 0) {
                
                new PNotify({
                    title: 'Error!',
                    text: datos.mensaje,
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
        }
    });
    
    limpiarformContacto();
    
}

init_contacto();

