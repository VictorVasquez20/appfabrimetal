var refdata, refman;
var ctx, mybarChart;
var ctxmant, mybarMant;
var tabla;


//funcion que se ejecuta iniciando
function init() {
    tabla = $("#ascensoresestado").DataTable({
        "paging": true,
        "info": false,
        "bFilter": true,
        "bAutoWidth": false,
        "ordering": false
    });

    $("#hiden").on("click", function () {
        ListarAlerta(false);
    });

    CargarGuias();
    CargarEstado();
    //GraficoGuias();
    GraficoMant();
    ListarAlerta(false);
    setInterval("Actualizar()", 60000);
}

function ListarAlerta(flag) {
    if (flag) {
        $("#Charts").hide();
        $("#datos").show();
    } else {
        $("#Charts").show();
        $("#datos").hide();
    }

}

function listarAlertaestado(estado) {
    $.post("../ajax/servicio.php?op=listarxestado", {"estado": estado}, function (data, status) {
        data = JSON.parse(data);
        //Actualizamos valores
        tabla.rows().remove().draw();
        ListarAlerta(true);
        for (i = 0; i < data.length; i++) {
            tabla.row.add([
                data[i]['0'],
                data[i]['1'],
                data[i]['2'],
                data[i]['5'],
                data[i]['3'],
                data[i]['4']
            ]).draw(false);
            ;
        }
    });
}

function GraficoGuias() {

    $.post("../ajax/servicio.php?op=GM", function (data, status) {
        data = JSON.parse(data);
        refdata = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

        for (var i = 0; i < data.aaData.length; i++) {
            refdata[data.aaData[i][0] - 1] = data.aaData[i][1];
        }

        ctx = document.getElementById("mybarChart");
        ctx.height = 125;
        mybarChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                datasets: [{
                        label: 'Servicios',
                        backgroundColor: "#26B99A",
                        data: refdata
                    }]
            },

            options: {
                scales: {
                    yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                }
            }
        });
    });

}

function GraficoMant() {
    $.post("../ajax/servicio.php?op=graficomantencion", function (data, status) {
        data = JSON.parse(data);
        arrcm = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; //completa
        arrcsf = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; //sinfirma
        arrpr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; //proceso

        for (var i = 0; i < data.length; i++) {

            if (data[i]["2"] === "CM") {
                arrcm[data[i]["1"] - 1] = data[i]["0"];
            } else if (data[i]["2"] === "CSF") {
                arrcsf[data[i]["1"] - 1] = data[i]["0"];
            } else {
                arrpr[data[i]["1"] - 1] = data[i]["0"];
            }
        }

        var ctx = document.getElementById("mybarMant");
        mybarChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                datasets: [
                    {label: 'Completos',
                        backgroundColor: "#26B99A",
                        data: arrcm
                    },
                    {
                        label: 'Sin Firma',
                        backgroundColor: "#455C73",
                        data: arrcsf
                    },
                    {
                        label: 'Proceso',
                        backgroundColor: "#BDC3C7",
                        data: arrpr
                    }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: true,
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                }
            }
        });

    });
}

function ActualizaMant(){
     $.post("../ajax/servicio.php?op=graficomantencion", function (data, status) {
        data = JSON.parse(data);
        arrcm = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; //completa
        arrcsf = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; //sinfirma
        arrpr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; //proceso

        for (var i = 0; i < data.length; i++) {

            if (data[i]["2"] === "CM") {
                arrcm[data[i]["1"] - 1] = data[i]["0"];
            } else if (data[i]["2"] === "CSF") {
                arrcsf[data[i]["1"] - 1] = data[i]["0"];
            } else {
                arrpr[data[i]["1"] - 1] = data[i]["0"];
            }
        }
        
        mybarChart.data.datasets[0]['data'] = arrcm;
        mybarChart.data.datasets[1]['data'] = arrcsf;
        mybarChart.data.datasets[2]['data'] = arrpr;
        mybarChart.update();

        
    });
}

function GraficoMantencion() {

    $.post("../ajax/servicio.php?op=GMM", function (data, status) {
        data = JSON.parse(data);
        refman = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

        for (var i = 0; i < data.aaData.length; i++) {
            refman[data.aaData[i][0] - 1] = data.aaData[i][1];
        }
        
        ctxmant = document.getElementById("mybarMant");
        ctxmant.height = 50;
        mybarMant = new Chart(ctxmant, {
            type: 'bar',
            data: {
                labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                datasets: [{
                        label: 'Servicios',
                        backgroundColor: "#26B99A",
                        data: refman
                    },
                    {
                        label: 'Detenidos',
                        backgroundColor: "blue",
                        data: refman
                    },
                    {
                        label: 'Detenidos',
                        backgroundColor: "yellow",
                        data: refman
                    }
                ]
            },

            options: {
                scales: {
                    yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                }
            }
        });
    });

}



function ActializarGuias() {

    $.post("../ajax/servicio.php?op=GM", function (data, status) {
        data = JSON.parse(data);

        var updata = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        for (var i = 0; i < data.aaData.length; i++) {
            updata[data.aaData[i][0] - 1] = data.aaData[i][1];
        }

        for (var i = 0; i < updata.length; i++) {
            if (refdata[i] != updata[i]) {
                refdata[i] = updata[i];
                console.log("Consiguio diferencia");
                console.log(refdata);
                mybarChart.data.datasets[0]['data'] = refdata;
                mybarChart.update();
            }

        }
    });
}

function ActializarMantencion() {

    $.post("../ajax/servicio.php?op=GMM", function (data, status) {
        data = JSON.parse(data);

        var updata = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        for (var i = 0; i < data.aaData.length; i++) {
            updata[data.aaData[i][0] - 1] = data.aaData[i][1];
        }

        for (var i = 0; i < updata.length; i++) {
            if (refman[i] != updata[i]) {
                refman[i] = updata[i];
                console.log("Consiguio diferencia");
                console.log(refman);
                mybarMant.data.datasets[0]['data'] = refman;
                mybarMant.update();
            }

        }
    });
}


function CargarGuias() {

    $.post("../ajax/servicio.php?op=DG", function (data, status) {
        data = JSON.parse(data);
        //Actualizamos valores
        $("#ser_pro").html(data.proceso.guias);
        $("#ser_com").html(data.completadas.guias);
        $("#ser_firma").html(data.penfirma.guias);
    });
}

function CargarEstado() {

    $.post("../ajax/servicio.php?op=EE", function (data, status) {
        data = JSON.parse(data);
        //Actualizamos valores
        var refes = [0, 0, 0, 0, 0, 0, 0, 0];

        for (var i = 0; i < data.aaData.length; i++) {
            refes[data.aaData[i][0] - 1] = data.aaData[i][1];
        }

        $("#num_normal").html(refes[0]);
        $("#num_detenido").html(refes[6]);
        $("#num_observacion").html(refes[7]);
        $("#num_emergencia").html(refes[5]);
        $("#num_mantencion").html(refes[1]);
        $("#num_reparacion").html(refes[2]);
        $("#num_mayor").html(refes[3]);
        $("#num_ingenieria").html(refes[4]);

    });

}

function Actualizar() {
    //CargarGuias();
    //CargarEstado();
    //ActializarGuias();
    //ActializarMantencion();
    //CargarGuias();
    CargarEstado();
    //GraficoGuias();
    //GraficoMant();
    ActualizaMant();
}

init();