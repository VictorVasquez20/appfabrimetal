var tabla;

//funcion que se ejecuta iniciando
function init() {
    mostarform();
    listar();
    setInterval("Actualizar()", 120000);
}



function mostarform() {
        $("#listadoaten").show();
}


function listar() {
    tabla = $('#tblaten').dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Bfrtip',
        buttons: [
        ],
        "ajax": {
            url: '../ajax/solatencion.php?op=pantalla',
            type: "get",
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 15, //Paginacion 10 items
        "order": [[1, "asc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}

function Actualizar() {
    tabla.ajax.reload();
}


init();