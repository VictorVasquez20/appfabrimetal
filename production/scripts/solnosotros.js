var tabla;

//funcion que se ejecuta iniciando
function init(){
    
    mostarform(false);
    listar();

    $('#formulario').on("submit", function(event){
            event.preventDefault();
            guardaryeditar();
    });
}


// Otras funciones
function limpiar(){
    $("#nombre").val("");
}

function mostarform(flag){
	limpiar();
	if(flag){
		$("#listadocontratospagina").hide();
		$("#formulariocontratospagina").show();
		$("#op_actualizar").hide();
		$("#op_listar").show();
                $("#btnGuardar").prop("disabled", false);
	}else{
                $("#formulariocontratospagina").hide();
		$("#listadocontratospagina").show();
		$("#op_actualizar").show();
		$("#op_listar").hide();
	}

}

function cancelarform(){
	limpiar();
	mostarform(false);
}


function listar(){
    tabla=$('#tblcontratospagina').dataTable({
            "aProcessing":true,
            "aServerSide": true,
            dom: 'Bfrtip',
            buttons:[
                    'copyHtml5',
                    'print',
                    'excelHtml5',
                    'csvHtml5',
                    'pdf'
            ],
            "ajax":{
                    url:'../ajax/solnosotros.php?op=listar',
                    type:"get",
                    dataType:"json",
                    error: function(e){
                            console.log(e.responseText);
                    }
            },
            "bDestroy": true,
            "iDisplayLength": 15, //Paginacion 10 items
            "order" : [[1 , "desc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}

function guardaryeditar(){
	$("#btnGuardar").prop("disabled", true);
	var formData = new FormData($("#formulario")[0]);
	$.ajax({
		url:'../ajax/solnosotros.php?op=editar',
		type:"POST",
		data:formData,
		contentType: false,
		processData:false,

		success: function(datos){
			bootbox.alert(datos);
			mostarform(false);
			tabla.ajax.reload();
		}
	});
	limpiar();
}

function editar(idsolnosotros, procesado){
    
    var formData = new FormData();
    formData.append("idsolnosotros", idsolnosotros);
    formData.append("procesado", procesado == 0 ? 1 : 0);
    $.ajax({
            url:'../ajax/solnosotros.php?op=editar',
            type:"POST",
            data:formData,
            contentType: false,
            processData:false,

            success: function(datos){
                    bootbox.alert(datos);
                    mostarform(false);
                    tabla.ajax.reload();
            }
    });
    
}
init();