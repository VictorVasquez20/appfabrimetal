var tabla;

//funcion que se ejecuta iniciando
function init(){
	mostarform(false);
	listar();

	$("#formulario").on("submit", function(e){
		guardaryeditar(e);
	})

	$("#imagenmuestra").hide();
}


// Otras funciones
function limpiar(){

	$("#idpavion").val("");	
	$("#nombre").val("");
	$("#apellido").val("");
	$("#tipo_documento").val("");
	$("#tipo_documento").selectpicker('refresh');
	$("#num_documento").val("");
	$("#fecha_nac").val("");
	$("#direccion").val("");
	$("#telefono").val("");	
	$("#email").val("");
	$("#imagenmuestra").attr("src","");
	$("#imagenactual").val("");
	$("#imagen").val("");

}

function mostarform(flag){

	limpiar();
	if(flag){
		$("#listadopavion").hide();
		$("#formulariopavion").show();
		$("#op_agregar").hide();
		$("#op_listar").show();
		$("#btnGuardar").prop("disabled", false);

	}else{
		$("#listadopavion").show();
		$("#formulariopavion").hide();
		$("#op_agregar").show();
		$("#op_listar").hide();
	}

}

function cancelarform(){
	limpiar();
	mostarform(false);
}

function listar(){
	tabla=$('#tblpavion').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons:[
			'copyHtml5',
			'print',
			'excelHtml5',
			'csvHtml5',
			'pdf'
		],
		"ajax":{
			url:'../ajax/pavion.php?op=listar',
			type:"get",
			dataType:"json",
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 10, //Paginacion 10 items
		"order" : [[1 , "desc"]] //Ordenar en base a la columna 0 descendente
	}).DataTable();
}

function guardaryeditar(e){
	e.preventDefault();
	$("#btnGuardar").prop("disabled", true);
	var formData = new FormData($("#formulario")[0]);
	$.ajax({
		url:'../ajax/pavion.php?op=guardaryeditar',
		type:"POST",
		data:formData,
		contentType: false,
		processData:false,

		success: function(datos){
			bootbox.alert(datos);
			mostarform(false);
			tabla.ajax.reload();
		}
	});
	limpiar();
}

function mostar(idpavion){
	$.post("../ajax/pavion.php?op=mostar",{idpavion:idpavion}, function(data,status){
		data = JSON.parse(data);
		mostarform(true);
	
		$("#idpavion").val(data.idpropietario_avion);
		$("#nombre").val(data.nombre);
		$("#apellido").val(data.apellido);
		$("#tipo_documento").val(data.tipo_documento);
		$("#tipo_documento").selectpicker('refresh');
		$("#num_documento").val(data.num_documento);
		$("#fecha_nac").val(data.fecha_nac);	
		$("#direccion").val(data.direccion);
		$("#telefono").val(data.telefono);	
		$("#email").val(data.email);
		$("#imagenmuestra").show();
		if(data.imagen == null){
			$("#imagenmuestra").attr("src","../files/pavion/noimg.jpg");
		}else{
			$("#imagenmuestra").attr("src","../files/pavion/"+data.imagen);
		}
		$("#imagenactual").val(data.imagen);

	})
}

function desactivar(idpavion){

	bootbox.confirm("Esta seguro que quiere inhabilitar el cliente?", function(result){
		if(result){
			$.post("../ajax/pavion.php?op=desactivar",{idpavion:idpavion}, function(e){
				bootbox.alert(e);
				tabla.ajax.reload();
			})	
		}
	})
}

function activar(idpavion){

	bootbox.confirm("Esta seguro que quiere habilitar el cliente?", function(result){
		if(result){
			$.post("../ajax/pavion.php?op=activar",{idpavion:idpavion}, function(e){
				bootbox.alert(e);
				tabla.ajax.reload();
			})	
		}
	})
}


init();