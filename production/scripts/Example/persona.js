var tabla;

//funcion que se ejecuta iniciando
function init(){
	mostarform(false);
	listar();

	$("#formulario").on("submit", function(e){
		guardaryeditar(e);
	})

	$("#imagenmuestra").hide();
}


// Otras funciones
function limpiar(){

	$("#idpersona").val("");	
	$("#nombre").val("");
	$("#apellido").val("");
	$("#tipo_documento").val("");
	$("#num_documento").val("");
	$("#fecha_nac").val("");
	$("#direccion").val("");
	$("#telefono").val("");	
	$("#email").val("");
	$("#imagenmuestra").attr("src","");
	$("#imagenactual").val("");
	$("#imagen").val("");

}

function mostarform(flag){

	limpiar();
	if(flag){
		$("#listadopersonas").hide();
		$("#formulariopersonas").show();
		$("#op_agregar").hide();
		$("#op_listar").show();
		$("#btnGuardar").prop("disabled", false);

	}else{
		$("#listadopersonas").show();
		$("#formulariopersonas").hide();
		$("#op_agregar").show();
		$("#op_listar").hide();
	}

}

function cancelarform(){
	limpiar();
	mostarform(false);
}

function listar(){
	tabla=$('#tblpersonas').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons:[
			'copyHtml5',
			'print',
			'excelHtml5',
			'csvHtml5',
			'pdf'
		],
		"ajax":{
			url:'../ajax/persona.php?op=listar',
			type:"get",
			dataType:"json",
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 10, //Paginacion 10 items
		"order" : [[1 , "desc"]] //Ordenar en base a la columna 0 descendente
	}).DataTable();
}

function guardaryeditar(e){
	e.preventDefault();
	$("#btnGuardar").prop("disabled", true);
	var formData = new FormData($("#formulario")[0]);
	$.ajax({
		url:'../ajax/persona.php?op=guardaryeditar',
		type:"POST",
		data:formData,
		contentType: false,
		processData:false,

		success: function(datos){
			bootbox.alert(datos);
			mostarform(false);
			tabla.ajax.reload();
		}
	});
	limpiar();
}

function mostar(idpersona){
	$.post("../ajax/persona.php?op=mostar",{idpersona:idpersona}, function(data,status){
		data = JSON.parse(data);
		mostarform(true);
	
		$("#idpersona").val(data.idpersona);	
		$("#nombre").val(data.nombre);
		$("#apellido").val(data.apellido);
		$("#tipo_documento").val(data.tipo_documento);
		$("#tipo_documento").selectpicker('refresh');
		$("#num_documento").val(data.num_documento);
		$("#fecha_nac").val(data.fecha_nac);	
		$("#direccion").val(data.direccion);
		$("#telefono").val(data.telefono);	
		$("#email").val(data.email);
		$("#imagenmuestra").show();
		if(data.imagen == null){
			$("#imagenmuestra").attr("src","../files/paracaidista/noimg.jpg");
		}else{
			$("#imagenmuestra").attr("src","../files/paracaidista/"+data.imagen);
		}
		$("#imagenactual").val(data.imagen);

	})
}

function desactivar(idpersona){

	bootbox.confirm("Esta seguro que quiere inhabilitar el paracaidista?", function(result){
		if(result){
			$.post("../ajax/persona.php?op=desactivar",{idpersona:idpersona}, function(e){
				bootbox.alert(e);
				tabla.ajax.reload();
			})	
		}
	})
}

function activar(idpersona){

	bootbox.confirm("Esta seguro que quiere habilitar el paracaidista?", function(result){
		if(result){
			$.post("../ajax/persona.php?op=activar",{idpersona:idpersona}, function(e){
				bootbox.alert(e);
				tabla.ajax.reload();
			})	
		}
	})
}


init();