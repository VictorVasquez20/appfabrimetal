var tabla;

//funcion que se ejecuta iniciando
function init(){
	mostarform(false);
	listar();

	$("#formulario").on("submit", function(e){
		guardaryeditar(e);
	})

	$.post("../ajax/venta.php?op=selectCliente", function(r){
		$("#idcliente").html(r);
		$("#idcliente").selectpicker('refresh');
	});

	//$("#imagenmuestra").hide();
}


// Otras funciones
function limpiar(){

	$("#idventa").val("");
	$("#iduser").val("");
	$("#idcliente").val("");
	$("#idcliente").selectpicker('refresh');	
	$("#tipo").val("");
	$("#serie").val("");
	$("#numero").val("");
	$("#impuesto").val("");
	$("#total_venta").val("");
	$(".filas").remove();
	$("#total").html("CLP. 0");
	$("#estado").val("");

	//Fecha actual
	var now = new Date();
	var day = ("0" + now.getDate()).slice(-2);
	var month = ("0" + (now.getMonth()+1)).slice(-2);
	var today = now.getFullYear()+"-"+(month)+"-"+(day);
	$("#fecha").val(today);



}

function mostarform(flag){

	limpiar();
	if(flag){
		$("#listadoventas").hide();
		$("#formularioventas").show();
		$("#op_agregar").hide();
		$("#op_listar").show();
		//$("#btnGuardar").prop("disabled", false);
		listarSalto();

		$("#btnGuardar").hide();
		$("#btnLimpiar").show();
		$("#btnCancelar").show();
		detalles=0;
		$("#btnAgregarArt").show();


	}else{
		$("#listadoventas").show();
		$("#formularioventas").hide();
		$("#op_agregar").show();
		$("#op_listar").hide();
	}

}

function cancelarform(){
	limpiar();
	mostarform(false);
}

function listar(){
	tabla=$('#tblventas').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons:[
			'copyHtml5',
			'print',
			'excelHtml5',
			'csvHtml5',
			'pdf'
		],
		"ajax":{
			url:'../ajax/venta.php?op=listar',
			type:"get",
			dataType:"json",
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 10, //Paginacion 10 items
		"order" : [[1 , "desc"]] //Ordenar en base a la columna 0 descendente
	}).DataTable();
}


function listarSalto(){
	tabla=$('#tblsaltos').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		"language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
		dom: 'Bfrtip',
		buttons:[
			
		],
		"ajax":{
			url:'../ajax/venta.php?op=listarSalto',
			type:"get",
			dataType:"json",
			error: function(e){
				console.log(e.responseText);
			}
		},
		"searching": false,
		"paging": false,
		"info": false,
		"bDestroy": true,
		"iDisplayLength": 5, //Paginacion 5 items
		"order" : [[1 , "desc"]] //Ordenar en base a la columna 0 descendente
	}).DataTable();
}

function guardaryeditar(e){
	e.preventDefault();
	//$("#btnGuardar").prop("disabled", true);
	var formData = new FormData($("#formulario")[0]);
	$.ajax({
		url:'../ajax/venta.php?op=guardaryeditar',
		type:"POST",
		data:formData,
		contentType: false,
		processData:false,

		success: function(datos){
			bootbox.alert(datos);
			mostarform(false);
			listar();
		}
	});
	limpiar();
}

function mostar(idventa){
	$.post("../ajax/venta.php?op=mostar",{idventa:idventa}, function(data,status){
		data = JSON.parse(data);
		mostarform(true);
	
		$("#idventa").val(data.idventa);
		$("#idcliente").val(data.idcliente);
		$("#idcliente").selectpicker('refresh');
		$("#fecha").val(data.fecha);
		$("#tipo").val(data.tipo);
		$("#tipo").selectpicker('refresh');
		$("#serie").val(data.serie);
		$("#numero").val(data.numero);

		//Ocultar botones y solo mostar cancelar
		$("#btnGuardar").hide();
		$("#btnLimpiar").hide();
		$("#btnCancelar").show();
		$("#btnAgregarArt").hide();

	});

	$.post("../ajax/venta.php?op=listarDetalle&id="+idventa, function(r){
		console.log(r);
		$('#detalle').html(r);
	})
}

function anular(idventa){

	bootbox.confirm("Esta seguro en marcar venta como cancelada?", function(result){
		if(result){
			$.post("../ajax/venta.php?op=anular",{idventa:idventa}, function(e){
				bootbox.alert(e);
				//tabla.ajax.reload();
				listar();
			})	
		}
	})
}

function pagado(idventa){

	bootbox.confirm("Esta seguro en marcar venta como pagada?", function(result){
		if(result){
			$.post("../ajax/venta.php?op=pagado",{idventa:idventa}, function(e){
				bootbox.alert(e);
				//tabla.ajax.reload();
				listar();
			})	
		}
	})
}

//Variables para detalle de venta
var impuesto=19;
var cont=0;
var detalles=0;

$("#btnGuardar").hide();

function agregarDetalle(idsalto,precio, salto){
	var cantidad=1;
	var descuento=1;

	if(idsalto !=""){
		var subtotal = (precio * cantidad) / descuento;
		var fila='<tr class"filas" id="fila'+cont+'">'+
		'<td><button type="button" class="btn btn-danger btn-xs" onclick="eliminarDetalle('+cont+')">X</button></td>'+
		'<td><input type="hidden" name="idsalto[]" id="idsalto[]" value="'+idsalto+'">'+salto+'</td>'+
		'<td><input type="number" name="cantidad[]" id="cantidad[]" value="'+cantidad+'"></td>'+
		'<td><input type="number" name="precio[]" id="precio[]" value="'+precio+'"></td>'+
		'<td><input type="number" step="any" name="descuento[]" id="descuento[]" value="'+descuento+'"></td>'+
		'<td><span name="subtotal" id="subotal'+cont+'">'+subtotal+'</span></td>'+
		'<td><button type="button" class="btn btn-info btn-xs" onclick="modificarSubtotales()"><i class="fa fa-refresh"></i></button></td>'+
		'</tr>';
		cont++;
		detalles++;
		$('#detalle').append(fila);
		modificarSubtotales();
	}
	else{
		alert("Error al ingresar salto")
	}
}

function modificarSubtotales(){
		var cant = document.getElementsByName("cantidad[]");
		var prec = document.getElementsByName("precio[]");
		var desc = document.getElementsByName("descuento[]");
		var sub = document.getElementsByName("subtotal");

		for (var i=0; i<cant.length; i++){
			var inpC=cant[i];
			var inpP=prec[i];
			var inpD=desc[i];
			var inpS=sub[i];

			inpS.value= (inpC.value * inpP.value) / inpD.value;
			inpS.value=Number((inpS.value).toFixed(0))
			document.getElementsByName("subtotal")[i].innerHTML = inpS.value;
		}

		calcularTotales();
	}

function calcularTotales(){

	var sub = document.getElementsByName("subtotal");
	var total = 0.0;
	
	
	for(var i=0 ; i < sub.length ; i++){

		total = total + document.getElementsByName("subtotal")[i].value;
	}

	total=Number((total).toFixed(0))
	$("#total").html("CLP. " + total);
	$("#total_venta").val(total);
	evaluar();

}

function evaluar(){
	if (detalles > 0){
		$("#btnGuardar").show();
		//$("#guardar").show();
	}else{
		$("#btnGuardar").hide();
		cont=0;
	}
}

function eliminarDetalle(indice){
	$("#fila"+indice).remove();
	calcularTotales();
	detalles = detalles-1;
	evaluar();
}


init();