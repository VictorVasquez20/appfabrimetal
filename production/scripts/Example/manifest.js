var tabla;

//funcion que se ejecuta iniciando
function init(){
	mostarform(false);
	listar();

	$("#formulario").on("submit", function(e){
		guardaryeditar(e);
	})

	$.post("../ajax/avion.php?op=selectAvion", function(r){
		$("#idavion").html(r);
		$("#idavion").selectpicker('refresh');
	});

	$.post("../ajax/piloto.php?op=selectPiloto", function(r){
		$("#idpiloto").html(r);
		$("#idpiloto").selectpicker('refresh');
	});

}


// Otras funciones
function limpiar(){

	$("#idmanifest").val("");
	$("#idvuelo").val("");
	$("#idpiloto").val("");
	$("#idpiloto").selectpicker('refresh');
	$("#idavion").val("");
	$("#idavion").selectpicker('refresh');
	$("#altura").val("");
	$("#fecha").val("");
}


function mostarform(flag){

	limpiar();
	if(flag){
		$("#listadopases").hide();
		$("#formulariopases").show();
		$("#op_agregar").hide();
		$("#op_listar").show();
		$("#btnGuardar").prop("disabled", false);

	}else{
		$("#listadopases").show();
		$("#formulariopases").hide();
		$("#op_agregar").show();
		$("#op_listar").hide();
	}

}

function cancelarform(){
	limpiar();
	mostarform(false);
}

function listar(){
	tabla=$('#tblpases').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons:[
			'copyHtml5',
			'print',
			'excelHtml5',
			'csvHtml5',
			'pdf'
		],
		"ajax":{
			url:'../ajax/manifest.php?op=listar',
			type:"get",
			dataType:"json",
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 10, //Paginacion 10 items
		"order" : [[0 , "desc"]] //Ordenar en base a la columna 0 descendente
	}).DataTable();
}

function guardaryeditar(e){
	e.preventDefault();
	$("#btnGuardar").prop("disabled", true);
	console.log($("#formulario")[0]);
	var formData = new FormData($("#formulario")[0]);
	
	$.ajax({
		url:'../ajax/manifest.php?op=guardaryeditar',
		type:"POST",
		data:formData,
		contentType: false,
		processData:false,

		success: function(datos){
			bootbox.alert(datos);
			mostarform(false);
			tabla.ajax.reload();
		}
	});
	limpiar();
}

function mostar(idmanifest){
	$.post("../ajax/manifest.php?op=mostar",{idmanifest:idmanifest}, function(data,status){
		data = JSON.parse(data);
		mostarform(true);

		$("#idmanifest").val(data.idmanifest);
		$("#idvuelo").val(data.idvuelo);
		$("#idpiloto").val(data.idpiloto);
		$("#idpiloto").selectpicker('refresh');
		$("#idavion").val(data.idavion);
		$("#idavion").selectpicker('refresh');
		$("#altura").val(data.altura);	
		$("#fecha").val(data.fecha);
		
	})
}

function eliminar(idmanifest, idvuelo){

	bootbox.confirm("Esta seguro que quiere eliminar el pase?", function(result){
		if(result){
			$.post("../ajax/manifest.php?op=eliminar",{idmanifest:idmanifest, idvuelo:idvuelo}, function(e){
				bootbox.alert(e);
				tabla.ajax.reload();
			})	
		}
	})
}


init();