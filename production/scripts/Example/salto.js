var tabla;

//funcion que se ejecuta iniciando
function init(){
	mostarform(false);
	listar();

	$("#formulario").on("submit", function(e){
		guardaryeditar(e);
	})

	$.post("../ajax/salto.php?op=selectTiposalto", function(r){
		console.log(r);
		$("#idtipo_salto").html(r);
		$("#idtipo_salto").selectpicker('refresh');
	});

	$("#imagenmuestra").hide();
}


// Otras funciones
function limpiar(){

	$("#idsalto").val("");
	$("#idtipo_salto").val("");
	$("#idtipo_salto").selectpicker('refresh');
	$("#codigo").val("");
	$("#nombre").val("");
	$("#descripcion").val("");
	$("#precio").val("");
	$("#imagenmuestra").attr("src","");
	$("#imagenactual").val("");
	$("#print").hide();


}

function mostarform(flag){

	limpiar();
	if(flag){
		$("#listadosaltos").hide();
		$("#formulariosaltos").show();
		$("#op_agregar").hide();
		$("#op_listar").show();
		$("#btnGuardar").prop("disabled", false);

	}else{
		$("#listadosaltos").show();
		$("#formulariosaltos").hide();
		$("#op_agregar").show();
		$("#op_listar").hide();
	}

}

function cancelarform(){
	limpiar();
	mostarform(false);
}

function listar(){
	tabla=$('#tbllistado').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons:[
			'copyHtml5',
			'print',
			'excelHtml5',
			'csvHtml5',
			'pdf'
		],
		"ajax":{
			url:'../ajax/salto.php?op=listar',
			type:"get",
			dataType:"json",
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 10, //Paginacion 10 items
		"order" : [[0 , "desc"]] //Ordenar en base a la columna 0 descendente
	}).DataTable();
}

function guardaryeditar(e){
	e.preventDefault();
	$("#btnGuardar").prop("disabled", true);
	console.log($("#formulario")[0]);
	var formData = new FormData($("#formulario")[0]);
	
	$.ajax({
		url:'../ajax/salto.php?op=guardaryeditar',
		type:"POST",
		data:formData,
		contentType: false,
		processData:false,

		success: function(datos){
			bootbox.alert(datos);
			mostarform(false);
			tabla.ajax.reload();
		}
	});
	limpiar();
}

function mostar(idsalto){
	$.post("../ajax/salto.php?op=mostar",{idsalto:idsalto}, function(data,status){
		data = JSON.parse(data);
		mostarform(true);

		$("#idsalto").val(data.idsalto);
		$("#idtipo_salto").val(data.idtipo_salto);
		$("#idtipo_salto").selectpicker('refresh');
		$("#codigo").val(data.codigo);	
		$("#nombre").val(data.nombre);
		$("#descripcion").val(data.descripcion);
		$("#imagenmuestra").show();
		$("#imagenmuestra").attr("src","../files/saltos/"+data.imagen);
		$("#imagenactual").val(data.imagen);
		$("#precio").val(data.precio);
		generarbarcode();

	})
}

function desactivar(idsalto){

	bootbox.confirm("Esta seguro que quiere desactivar el salto?", function(result){
		if(result){
			$.post("../ajax/salto.php?op=desactivar",{idsalto:idsalto}, function(e){
				bootbox.alert(e);
				tabla.ajax.reload();
			})	
		}
	})
}

function activar(idsalto){

	bootbox.confirm("Esta seguro que quiere activar el salto?", function(result){
		if(result){
			$.post("../ajax/salto.php?op=activar",{idsalto:idsalto}, function(e){
				bootbox.alert(e);
				tabla.ajax.reload();
			})	
		}
	})
}

function generarbarcode(){
	codigo=$("#codigo").val();
	console.log(codigo);
	JsBarcode("#barcode", codigo);
	$("#print").show();

}

function imprimir(){
	console.log("Imprimiendo Barcode");
	$("#print").printArea();
}

init();