var tabla;

//funcion que se ejecuta iniciando
function init(){
	mostarform(false);
	listar();

	$("#formulario").on("submit", function(e){
		guardaryeditar(e);
	})

	$.post("../ajax/pavion.php?op=selectpavion", function(r){
		$("#idpavion").html(r);
		$("#idpavion").selectpicker('refresh');
	});

}


// Otras funciones
function limpiar(){

	$("#idavion").val("");
	$("#idpavion").val("");
	$("#idpavion").selectpicker('refresh');	
	$("#marca").val("");
	$("#modelo").val("");
	$("#ano").val("");
	$("#serial").val("");

}

function mostarform(flag){

	limpiar();
	if(flag){
		$("#listadoaviones").hide();
		$("#formularioaviones").show();
		$("#op_agregar").hide();
		$("#op_listar").show();
		$("#btnGuardar").prop("disabled", false);

	}else{
		$("#listadoaviones").show();
		$("#formularioaviones").hide();
		$("#op_agregar").show();
		$("#op_listar").hide();
	}

}

function cancelarform(){
	limpiar();
	mostarform(false);
}

function listar(){
	tabla=$('#tblaviones').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons:[
			'copyHtml5',
			'print',
			'excelHtml5',
			'csvHtml5',
			'pdf'
		],
		"ajax":{
			url:'../ajax/avion.php?op=listar',
			type:"get",
			dataType:"json",
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 10, //Paginacion 10 items
		"order" : [[1 , "desc"]] //Ordenar en base a la columna 0 descendente
	}).DataTable();
}

function guardaryeditar(e){
	e.preventDefault();
	$("#btnGuardar").prop("disabled", true);
	var formData = new FormData($("#formulario")[0]);
	$.ajax({
		url:'../ajax/avion.php?op=guardaryeditar',
		type:"POST",
		data:formData,
		contentType: false,
		processData:false,

		success: function(datos){
			bootbox.alert(datos);
			mostarform(false);
			tabla.ajax.reload();
		}
	});
	limpiar();
}

function mostar(idavion){
	$.post("../ajax/avion.php?op=mostar",{idavion:idavion}, function(data,status){
		data = JSON.parse(data);
		mostarform(true);
	
		$("#idavion").val(data.idavion);
		$("#idpavion").val(data.idpropietario_avion);
		$("#idpavion").selectpicker('refresh');
		$("#marca").val(data.marca);
		$("#modelo").val(data.modelo);
		$("#ano").val(data.ano);
		$("#serial").val(data.placa);

	})
}

function desactivar(idavion){

	bootbox.confirm("Esta seguro que quiere inhabilitar el avion?", function(result){
		if(result){
			$.post("../ajax/avion.php?op=desactivar",{idavion:idavion}, function(e){
				bootbox.alert(e);
				tabla.ajax.reload();
			})	
		}
	})
}

function activar(idavion){

	bootbox.confirm("Esta seguro que quiere habilitar el avion?", function(result){
		if(result){
			$.post("../ajax/avion.php?op=activar",{idavion:idavion}, function(e){
				bootbox.alert(e);
				tabla.ajax.reload();
			})	
		}
	})
}


init();