var tabla;

//funcion que se ejecuta iniciando
function init(){
	(false);
	listar();

	$("#formulario").on("submit", function(e){
		guardaryeditar(e);
	})

	$("#imagenmuestra").hide();
}


// Inserta la alerta generada por el arduino, enviamos parametro por GET
function insertar(e){
	e.preventDefault();
	$("#btnGuardar").prop("disabled", true);
	var formData = new FormData($("#formulario")[0]);
	$.ajax({
		url:'../ajax/instructor.php?op=guardaryeditar',
		type:"POST",
		data:formData,
		contentType: false,
		processData:false,

		success: function(datos){
			bootbox.alert(datos);
			mostarform(false);
			tabla.ajax.reload();
		}
	});
	limpiar();
}

function mostar(idinstructor){
	$.post("../ajax/instructor.php?op=mostar",{idinstructor:idinstructor}, function(data,status){
		data = JSON.parse(data);
		mostarform(true);
	
		$("#idinstructor").val(data.idinstructor);	
		$("#nombre").val(data.nombre);
		$("#apellido").val(data.apellido);
		$("#tipo_documento").val(data.tipo_documento);
		$("#tipo_documento").selectpicker('refresh');
		$("#num_documento").val(data.num_documento);
		$("#fecha_nac").val(data.fecha_nac);	
		$("#direccion").val(data.direccion);
		$("#telefono").val(data.telefono);	
		$("#email").val(data.email);
		$("#licencia").val(data.licencia);
		$("#imagenmuestra").show();
		if(data.imagen == null){
			$("#imagenmuestra").attr("src","../files/instructores/noimg.jpg");
		}else{
			$("#imagenmuestra").attr("src","../files/instructores/"+data.imagen);
		}
		$("#imagenactual").val(data.imagen);

	})
}

function desactivar(idinstructor){

	bootbox.confirm("Esta seguro que quiere inhabilitar el instructor?", function(result){
		if(result){
			$.post("../ajax/instructor.php?op=desactivar",{idinstructor:idinstructor}, function(e){
				bootbox.alert(e);
				tabla.ajax.reload();
			})	
		}
	})
}

function activar(idinstructor){

	bootbox.confirm("Esta seguro que quiere habilitar el instructor?", function(result){
		if(result){
			$.post("../ajax/instructor.php?op=activar",{idinstructor:idinstructor}, function(e){
				bootbox.alert(e);
				tabla.ajax.reload();
			})	
		}
	})
}


init();