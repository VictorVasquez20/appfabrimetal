var tabla;

//funcion que se ejecuta iniciando
function init(){
	console.log("Inicio JS");
	mostarform(false);
	listar();

	$("#formulario").on("submit", function(e){
		guardaryeditar(e);
	})

	$.post("../ajax/manifest.php?op=selectmanifest", function(r){
		$("#idmanifest").html(r);
		$("#idmanifest").selectpicker('refresh');
	});

}


// Otras funciones
function limpiar(){

	$("#idventa").val("");
	$("#idventa").selectpicker('refresh');
	$("#idsalto").val("");
	$("#idsalto").selectpicker('refresh');
	$("#idequipo").val("");
	$("#idequipo").selectpicker('refresh');
	$("#idpersona").val("");
	$("#idpersona").selectpicker('refresh');
	$("#idpacker").val("");
	$("#idpacker").selectpicker('refresh');
	$("#idinstructor").val("");
	$("#idinstructor").selectpicker('refresh');
	$("#idcamara").val("");
	$("#idcamara").selectpicker('refresh');


}

function mostarform(flag){

	limpiar();
	if(flag){
		$("#listadoplanificacion").hide();
		$("#formularioplanificacion").show();
		$("#plani_manifest").hide();
		$("#op_agregar").hide();
		$("#op_listar").show();
		$("#btnGuardar").prop("disabled", false);

	}else{
		$("#listadoplanificacion").show();
		$("#formularioplanificacion").hide();
		$("#op_agregar").show();
		$("#op_listar").hide();
	}

}

function cancelarform(){
	limpiar();
	mostarform(false);
}

function listar(){
	tabla=$('#tblplanificacion').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons:[
			'copyHtml5',
			'print',
			'excelHtml5',
			'csvHtml5',
			'pdf'
		],
		"ajax":{
			url:'../ajax/planificacion.php?op=listar',
			type:"get",
			dataType:"json",
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 10, //Paginacion 10 items
		"order" : [[1 , "desc"]] //Ordenar en base a la columna 0 descendente
	}).DataTable();
}

function guardaryeditar(e){
	e.preventDefault();
	var formData = new FormData($("#formulario")[0]);
	console.log(formData);
	$.ajax({
		url:'../ajax/planificacion.php?op=guardaryeditar',
		type:"POST",
		data:formData,
		contentType: false,
		processData:false,

		success: function(datos){
			bootbox.alert(datos);
			tabla.ajax.reload();
			listartabla();

		}
	});
	limpiar();
}

function listartabla(){
	$.post("../ajax/planificacion.php?op=listarmanifest",{idmanifest:$("#idmanifest").val()}, function(r){		
		$("#plani").html(r);
		$("#plani_manifest").show();
	});

	$.post("../ajax/venta.php?op=selectventa", function(r){
		$("#idventa").html(r);
		$("#idventa").selectpicker('refresh');
	});
	$.post("../ajax/salto.php?op=selectsalto", function(r){
		$("#idsalto").html(r);
		$("#idsalto").selectpicker('refresh');
	});
	$.post("../ajax/equipo.php?op=selectequipo", function(r){
		$("#idequipo").html(r);
		$("#idequipo").selectpicker('refresh');
	});
	$.post("../ajax/persona.php?op=selectpersona", function(r){
		$("#idpersona").html(r);
		$("#idpersona").selectpicker('refresh');
	});
	$.post("../ajax/packer.php?op=selectpacker", function(r){
		$("#idpacker").html(r);
		$("#idpacker").selectpicker('refresh');
	});
	$.post("../ajax/instructor.php?op=selectinstructor", function(r){
		$("#idinstructor").html(r);
		$("#idinstructor").selectpicker('refresh');
	});
	$.post("../ajax/camara.php?op=selectcamara", function(r){
		$("#idcamara").html(r);
		$("#idcamara").selectpicker('refresh');
	});
}


function eliminar_tabla(idplanificacion){

	bootbox.confirm("Esta seguro que quiere eliminar la planificacion?", function(result){
		if(result){
			$.post("../ajax/planificacion.php?op=eliminar",{idplanificacion:idplanificacion}, function(e){
				bootbox.alert(e);
				listartabla();
			})	
		}
	})
}

function eliminar(idplanificacion){
	bootbox.confirm("Esta seguro que quiere eliminar la planificacion?", function(result){
		if(result){
			$.post("../ajax/planificacion.php?op=eliminar",{idplanificacion:idplanificacion}, function(e){
				bootbox.alert(e);
				tabla.ajax.reload();
			})	
		}
	})
}


init();