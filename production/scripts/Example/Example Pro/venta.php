<?php 
ob_start();
session_start();

if(!isset($_SESSION["nombre"])){
  header("Location:login.php");
}else{

require 'header.php';

if( $_SESSION['ventas']==1)
{

 ?>
        
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Ventas</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a id="op_agregar" onclick="mostarform(true)">Agregar</a>
                          </li>
                          <li><a id="op_listar" onclick="mostarform(false)">Listar</a>
                          </li>
                        </ul>
                      </li>                     
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div id="listadoventas" class="x_content">

                    <table id="tblventas" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Opciones</th>
                          <th>Cliente</th>
                          <th>Tipo</th>
                          <th>Numero</th>
                          <th>Fecha</th>
                          <th>Usuario
                          <th>Condicion</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>

                  <div id="formularioventas" class="x_content">
                    <br />

                    <div class="col-md-12 center-margin">
	                  <form class="form-horizontal form-label-left" id="formulario" name="formulario">
	                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
	                      <label>Cliente</label>
	                      <input type="hidden" id="idventa" name="idventa" class="form-control">
	                      <select class="form-control selectpicker" data-live-search="true" id="idcliente" name="idcliente" required="required">
                          </select>
	                    </div>
	                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
	                    	<label>Fecha</label>
                            <input type='date' id="fecha" name="fecha" class="form-control"/>
	                 	</div>	                   
	                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
	                    	<label>Tipo de Venta</label>
	                      	<select class="form-control selectpicker" data-live-search="false" id="tipo" name="tipo" required="required">
                                <option value="Directa">Directa</option>
                                <option value="En linea">En linea</option>
                                <option value="Partner">Partner</option>
                            </select>
	                  	</div>	            
	                 	<div class="col-md-3 col-sm-12 col-xs-12 form-group">
	                    	<label>Serie de boleta</label>
	                      	<input type="text" class="form-control" name="serie" id="serie">
	                  	</div>
	                  	<div class="col-md-3 col-sm-12 col-xs-12 form-group">
	                    	<label>Numero de boleta</label>
	                      	<input type="text" class="form-control" name="numero" id="numero">
	                 	</div>

	                 	<div class="col-md-12 col-sm-12 col-xs-12 form-group">
	                      <label>Detalle Venta</label>
	                      <br />	                   
	                      <a data-toggle="modal" href="#myModal">
	                      	 <button id="btnAgregarArt" type="button" class="btn btn-primary btn-xs"> 
	                      	 <span class="fa fa-plus"></span>
	                      	 Agregar Saltos</button>
	                      </a>
	                    </div>

	                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
	                      <table id="detalle" name="detalle" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" >
	                      	<thead style="background-color: #e5e5e5">
	                      		<th>Opciones</th>
	                      		<th>Salto</th>
	                      		<th>Cantidad</th>
	                      		<th>Precio</th>
	                      		<th>Descuento</th>
	                      		<th>Subtotal</th>
	                      	</thead>
	                      	<tfoot>
	                      		<th></th>
	                      		<th></th>
	                      		<th></th>
	                      		<th></th>
	                      		<th><h4>Total</h4></th>
	                      		<th><h4 id="total">(CLP) 0.00</h4><input type="hidden" name="total_venta" id="total_venta"></th>
	                      	</tfoot>
	                      	<tbody>
	                      		
	                      	</tbody>	
	                      </table>	                     
	                    </div>	 	 

	                 	
	                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button class="btn btn-primary" type="button" id="btnCancelar" onclick="cancelarform()">Cancelar</button>
                          <button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiar()">Limpiar</button>
                          <button class="btn btn-success" type="submit" id="btnGuardar">Agregar</button>
                        </div>
                      </div>
	                  </form>
	                </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- /Modal Saltos -->

        <div class="modal fade" id="myModal" name="myModal" role="dialog" aria-LabelLedby="myModalLabel" aria-hidden="true">
		    <div class="modal-dialog modal-lg">
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">Seleccione un salto</h4>
		        </div>
		        <div class="modal-body">
				         <table id="tblsaltos" class="table table-striped table-bordered dt-responsive nowrap" style="width: 100%">
			        				<thead>
			        					<th>Opciones</th>
			        					<th>Codigo</th>
			        					<th>Nombre</th>
			        					<th>Precio</th>
			        				</thead>
			        				<tbody>
			        					
			        				</tbody>
	        			</table>      
		        </div>
		        <div class="modal-footer">
		          
		        </div>
		      </div>
		    </div>
		 </div>

        <!-- /Modal Saltos -->
<?php 
}else{
  require 'nopermiso.php';
}
require 'footer.php';
?>
<script>
    $('#myDatepicker2').datetimepicker({
        format: 'DD/MM/YYYY'
    });
</script>
<script type="text/javascript" src="scripts/venta.js"></script>
<?php 
}
ob_end_flush();
?>