<?php 
ob_start();
session_start();

if(!isset($_SESSION["nombre"])){
  header("Location:login.php");
}else{

require 'header.php';

if($_SESSION['mantenimiento']==1 & $_SESSION['man_ventas']==1)
{

 ?>
        
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Saltos</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a id="op_agregar" onclick="mostarform(true)">Agregar</a>
                          </li>
                          <li><a id="op_listar" onclick="mostarform(false)">Listar</a>
                          </li>
                        </ul>
                      </li>                     
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div id="listadosaltos" class="x_content">

                    <table id="tbllistado" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Opciones</th>
                          <th>Tipo</th>
                          <th>Nombre</th>
                          <th>Codigo</th>
                          <th>Precio</th>
                          <th>Condicion</th>
                          <th>Imagen</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>

                  <div id="formulariosaltos" class="x_content">
                    <br />
                    <form id="formulario" name="formulario" class="form-horizontal form-label-left input_mask">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombre">Codigo <span class="required">*</span>
                        </label>

                          <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="input-group">
                            <input type="hidden" id="idsalto" name="idsalto">
                            <input type="text" id="codigo" name="codigo" required="required" class="form-control col-md-7 col-xs-12" maxlength="45" aria-label="Text input with dropdown button">
                            <div class="input-group-btn">
                              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li><a onclick="generarbarcode()">Generar</a>
                                </li>
                                <li><a onclick="imprimir()">Imprimir</a>
                                </li>                              
                              </ul>
                            </div>
                            </div>
                          </div>
                          </div>
                        <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">               
                          <div id="print" name="print">
                            <svg id="barcode" class="col-md-6 col-xs-12 col-md-offset-6"></svg>
                          </div>
                        </div>                      
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombre">Nombre <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="nombre" name="nombre" required="required" class="form-control col-md-7 col-xs-12" maxlength="45">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombre">Descripcion <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="descripcion" name="descripcion" required="required" class="form-control col-md-7 col-xs-12" maxlength="45">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Tipo de salto <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select class="form-control selectpicker" data-live-search="true" id="idtipo_salto" name="idtipo_salto" required="required">
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="descripcion">Imagen de referencia <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="imagen" name="imagen" type="file" class="custom-file-input col-md-7 col-xs-12">
                          <input id="imagenactual" name="imagenactual" type="hidden" class="custom-file-input col-md-7 col-xs-12">
                          <img src="" width="150px" height="120px" id="imagenmuestra" name="imagenmuestra">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="precio">Precio <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="number" id="precio" name="precio" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button class="btn btn-primary" type="button" id="btnCancelar" onclick="cancelarform()">Cancelar</button>
                          <button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiar()">Limpiar</button>
                          <button class="btn btn-success" type="submit" id="btnGuardar">Agregar</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php 
}else{
  require 'nopermiso.php';
}
require 'footer.php';
?>
<script type="text/javascript" src="../public/build/js/JsBarcode.all.min.js"></script>
<script type="text/javascript" src="../public/build/js/jquery.PrintArea.js"></script>
<script type="text/javascript" src="scripts/salto.js"></script>

<?php 
}
ob_end_flush();
 ?>