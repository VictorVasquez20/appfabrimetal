<?php 
ob_start();
session_start();

if(!isset($_SESSION["nombre"])){
  header("Location:login.php");
}else{

require 'header.php';

if($_SESSION['reportes']==1)
{

 ?> 
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="row">
              <div class="col-md-4">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Top Vendedor <small>Ventas</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <article class="media event">
                      <a class="pull-left date">
                        <p class="month">Foto</p>
                      </a>
                      <div class="media-body">
                        <a class="title" href="#">Omar Maldonado</a>
                        <p>8 Ventas</p>
                      </div>
                    </article>
                    <article class="media event">
                      <a class="pull-left date">
                        <p class="month">Foto</p>
                      </a>
                      <div class="media-body">
                        <a class="title" href="#">Jaime Obreque</a>
                        <p>8 Ventas</p>
                      </div>
                    </article>
                    <article class="media event">
                      <a class="pull-left date">
                        <p class="month">Foto</p>
                      </a>
                      <div class="media-body">
                        <a class="title" href="#">Luis Pirela</a>
                        <p>6 Ventas</p>
                      </div>
                    </article>
                    <article class="media event">
                      <a class="pull-left date">
                        <p class="month">Foto</p>
                      </a>
                      <div class="media-body">
                        <a class="title" href="#">Luis Maldonado</a>
                        <p>3 Ventas</p>
                      </div>
                    </article>                 
                  </div>
                </div>
              </div>

              <div class="col-md-4">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Top Promotor <small>Contactos</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                                    <div class="x_content">
                    <article class="media event">
                      <a class="pull-left date">
                        <p class="month">Foto</p>
                      </a>
                      <div class="media-body">
                        <a class="title" href="#">Omar Maldonado</a>
                        <p>8 Contactos</p>
                      </div>
                    </article>
                    <article class="media event">
                      <a class="pull-left date">
                        <p class="month">Foto</p>
                      </a>
                      <div class="media-body">
                        <a class="title" href="#">Jaime Obreque</a>
                        <p>8 Contactos</p>
                      </div>
                    </article>
                    <article class="media event">
                      <a class="pull-left date">
                        <p class="month">Foto</p>
                      </a>
                      <div class="media-body">
                        <a class="title" href="#">Luis Pirela</a>
                        <p>6 Contactos</p>
                      </div>
                    </article>
                    <article class="media event">
                      <a class="pull-left date">
                        <p class="month">Foto</p>
                      </a>
                      <div class="media-body">
                        <a class="title" href="#">Luis Maldonado</a>
                        <p>3 Contactos</p>
                      </div>
                    </article>                 
                  </div>
                </div>
              </div>

              <div class="col-md-4">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Top Medio <small>Contactos</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                                    <div class="x_content">
                    <article class="media event">
                      <a class="pull-left date">
                        <p class="month">Foto</p>
                      </a>
                      <div class="media-body">
                        <a class="title" href="#">Facebook</a>
                        <p>97 Contactos</p>
                      </div>
                    </article>
                    <article class="media event">
                      <a class="pull-left date">
                        <p class="month">Foto</p>
                      </a>
                      <div class="media-body">
                        <a class="title" href="#">Instagram</a>
                        <p>85 Contactos</p>
                      </div>
                    </article>
                    <article class="media event">
                      <a class="pull-left date">
                        <p class="month">Foto</p>
                      </a>
                      <div class="media-body">
                        <a class="title" href="#">Twitter</a>
                        <p>47 Contactos</p>
                      </div>
                    </article>
                    <article class="media event">
                      <a class="pull-left date">
                        <p class="month">Foto</p>
                      </a>
                      <div class="media-body">
                        <a class="title" href="#">Personal</a>
                        <p>25 Saltos</p>
                      </div>
                    </article>                 
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

<?php 
}else{
  require 'nopermiso.php';
}
require 'footer.php';
?>

 <!-- jQuery Sparklines -->
 <script src="../public/build/js/jquery.sparkline.min.js"></script>

 <?php }
ob_end_flush();
 ?>