<?php 
ob_start();
session_start();

if(!isset($_SESSION["nombre"])){
  header("Location:login.php");
}else{

require 'header.php';

if($_SESSION['manifest']==1)
{

 ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Seleccionar Pase</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">      
                      <button class="btn btn-default" type="button">Generar</button>
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_content">
                    <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                      <h3>Primer Pase 22/07/2017 09:00 am - Avion Cessna 210 -Altura 13.000 Ft </h3>
                      </div>

                      <div class="clearfix"></div>

                      <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                        <div class="well profile_view">
                          <div class="col-sm-12">
                            <h4 class="brief"><i>Salto Tandem</i></h4>
                            <div class="left col-xs-7">
                              <h2><strong>Alexis Sanchez</strong></h2>
                              <p><strong>Equipo: </strong> Tandem 235 </p>
                              <ul class="list-unstyled">
                                <li><strong>Instructor: </strong>Jaime Obreque </li>
                                <li><strong>Camara: </strong>Luis Maldonado</li>
                                <li><strong>Packer: </strong>Luis Pirela</li>
                              </ul>
                            </div>
                            <div class="right col-xs-5 text-center">
                              <img src="../files/paracaidista/123456.jpg" alt="" class="img-circle img-responsive">
                            </div>
                          </div>
                          <div class="col-xs-12 bottom text-center">
                            <div class="col-xs-12 col-sm-6 emphasis">
                              <p class="ratings">
                                <a><strong>Saltos en Zona: </strong>20</a>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>

                     <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                        <div class="well profile_view">
                          <div class="col-sm-12">
                            <h4 class="brief"><i>Salto Tandem</i></h4>
                            <div class="left col-xs-7">
                              <h2><strong>Alexis Sanchez</strong></h2>
                              <p><strong>Equipo: </strong> Tandem 235 </p>
                              <ul class="list-unstyled">
                                <li><strong>Instructor: </strong>Jaime Obreque </li>
                                <li><strong>Camara: </strong>Luis Maldonado</li>
                                <li><strong>Packer: </strong>Luis Pirela</li>
                              </ul>
                            </div>
                            <div class="right col-xs-5 text-center">
                              <img src="../files/paracaidista/123456.jpg" alt="" class="img-circle img-responsive">
                            </div>
                          </div>
                          <div class="col-xs-12 bottom text-center">
                            <div class="col-xs-12 col-sm-6 emphasis">
                              <p class="ratings">
                                <a><strong>Saltos en Zona: </strong>20</a>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                        <div class="well profile_view">
                          <div class="col-sm-12">
                            <h4 class="brief"><i>Salto Tandem</i></h4>
                            <div class="left col-xs-7">
                              <h2><strong>Alexis Sanchez</strong></h2>
                              <p><strong>Equipo: </strong> Tandem 235 </p>
                              <ul class="list-unstyled">
                                <li><strong>Instructor: </strong>Jaime Obreque </li>
                                <li><strong>Camara: </strong>Luis Maldonado</li>
                                <li><strong>Packer: </strong>Luis Pirela</li>
                              </ul>
                            </div>
                            <div class="right col-xs-5 text-center">
                              <img src="../files/paracaidista/123456.jpg" alt="" class="img-circle img-responsive">
                            </div>
                          </div>
                          <div class="col-xs-12 bottom text-center">
                            <div class="col-xs-12 col-sm-6 emphasis">
                              <p class="ratings">
                                <a><strong>Saltos en Zona: </strong>20</a>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                        <div class="well profile_view">
                          <div class="col-sm-12">
                            <h4 class="brief"><i>Salto Tandem</i></h4>
                            <div class="left col-xs-7">
                              <h2><strong>Alexis Sanchez</strong></h2>
                              <p><strong>Equipo: </strong> Tandem 235 </p>
                              <ul class="list-unstyled">
                                <li><strong>Instructor: </strong>Jaime Obreque </li>
                                <li><strong>Camara: </strong>Luis Maldonado</li>
                                <li><strong>Packer: </strong>Luis Pirela</li>
                              </ul>
                            </div>
                            <div class="right col-xs-5 text-center">
                              <img src="../files/paracaidista/123456.jpg" alt="" class="img-circle img-responsive">
                            </div>
                          </div>
                          <div class="col-xs-12 bottom text-center">
                            <div class="col-xs-12 col-sm-6 emphasis">
                              <p class="ratings">
                                <a><strong>Saltos en Zona: </strong>20</a>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                      
                      <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                        <div class="well profile_view">
                          <div class="col-sm-12">
                            <h4 class="brief"><i>Salto Tandem</i></h4>
                            <div class="left col-xs-7">
                              <h2><strong>Alexis Sanchez</strong></h2>
                              <p><strong>Equipo: </strong> Tandem 235 </p>
                              <ul class="list-unstyled">
                                <li><strong>Instructor: </strong>Jaime Obreque </li>
                                <li><strong>Camara: </strong>Luis Maldonado</li>
                                <li><strong>Packer: </strong>Luis Pirela</li>
                              </ul>
                            </div>
                            <div class="right col-xs-5 text-center">
                              <img src="../files/paracaidista/123456.jpg" alt="" class="img-circle img-responsive">
                            </div>
                          </div>
                          <div class="col-xs-12 bottom text-center">
                            <div class="col-xs-12 col-sm-6 emphasis">
                              <p class="ratings">
                                <a><strong>Saltos en Zona: </strong>20</a>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                        <div class="well profile_view">
                          <div class="col-sm-12">
                            <h4 class="brief"><i>Salto Tandem</i></h4>
                            <div class="left col-xs-7">
                              <h2><strong>Alexis Sanchez</strong></h2>
                              <p><strong>Equipo: </strong> Tandem 235 </p>
                              <ul class="list-unstyled">
                                <li><strong>Instructor: </strong>Jaime Obreque </li>
                                <li><strong>Camara: </strong>Luis Maldonado</li>
                                <li><strong>Packer: </strong>Luis Pirela</li>
                              </ul>
                            </div>
                            <div class="right col-xs-5 text-center">
                              <img src="../files/paracaidista/123456.jpg" alt="" class="img-circle img-responsive">
                            </div>
                          </div>
                          <div class="col-xs-12 bottom text-center">
                            <div class="col-xs-12 col-sm-6 emphasis">
                              <p class="ratings">
                                <a><strong>Saltos en Zona: </strong>20</a>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                        <div class="well profile_view">
                          <div class="col-sm-12">
                            <h4 class="brief"><i>Salto Tandem</i></h4>
                            <div class="left col-xs-7">
                              <h2><strong>Alexis Sanchez</strong></h2>
                              <p><strong>Equipo: </strong> Tandem 235 </p>
                              <ul class="list-unstyled">
                                <li><strong>Instructor: </strong>Jaime Obreque </li>
                                <li><strong>Camara: </strong>Luis Maldonado</li>
                                <li><strong>Packer: </strong>Luis Pirela</li>
                              </ul>
                            </div>
                            <div class="right col-xs-5 text-center">
                              <img src="../files/paracaidista/123456.jpg" alt="" class="img-circle img-responsive">
                            </div>
                          </div>
                          <div class="col-xs-12 bottom text-center">
                            <div class="col-xs-12 col-sm-6 emphasis">
                              <p class="ratings">
                                <a><strong>Saltos en Zona: </strong>20</a>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                        <div class="well profile_view">
                          <div class="col-sm-12">
                            <h4 class="brief"><i>Salto Tandem</i></h4>
                            <div class="left col-xs-7">
                              <h2><strong>Alexis Sanchez</strong></h2>
                              <p><strong>Equipo: </strong> Tandem 235 </p>
                              <ul class="list-unstyled">
                                <li><strong>Instructor: </strong>Jaime Obreque </li>
                                <li><strong>Camara: </strong>Luis Maldonado</li>
                                <li><strong>Packer: </strong>Luis Pirela</li>
                              </ul>
                            </div>
                            <div class="right col-xs-5 text-center">
                              <img src="../files/paracaidista/123456.jpg" alt="" class="img-circle img-responsive">
                            </div>
                          </div>
                          <div class="col-xs-12 bottom text-center">
                            <div class="col-xs-12 col-sm-6 emphasis">
                              <p class="ratings">
                                <a><strong>Saltos en Zona: </strong>20</a>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                        <div class="well profile_view">
                          <div class="col-sm-12">
                            <h4 class="brief"><i>Salto Tandem</i></h4>
                            <div class="left col-xs-7">
                              <h2><strong>Alexis Sanchez</strong></h2>
                              <p><strong>Equipo: </strong> Tandem 235 </p>
                              <ul class="list-unstyled">
                                <li><strong>Instructor: </strong>Jaime Obreque </li>
                                <li><strong>Camara: </strong>Luis Maldonado</li>
                                <li><strong>Packer: </strong>Luis Pirela</li>
                              </ul>
                            </div>
                            <div class="right col-xs-5 text-center">
                              <img src="../files/paracaidista/123456.jpg" alt="" class="img-circle img-responsive">
                            </div>
                          </div>
                          <div class="col-xs-12 bottom text-center">
                            <div class="col-xs-12 col-sm-6 emphasis">
                              <p class="ratings">
                                <a><strong>Saltos en Zona: </strong>20</a>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php 
}else{
  require 'nopermiso.php';
}
require 'footer.php';
}
ob_end_flush();
 ?>