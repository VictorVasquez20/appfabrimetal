<?php 
ob_start();
session_start();

if(!isset($_SESSION["nombre"])){
  header("Location:login.php");
}else{

require 'header.php';

if( $_SESSION['manifest']==1 )
{

 ?>
        
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Planificacion</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a id="op_agregar" onclick="mostarform(true)">Agregar</a>
                          </li>
                          <li><a id="op_listar" onclick="mostarform(false)">Listar</a>
                          </li>
                        </ul>
                      </li>                     
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                  <div id="listadoplanificacion" class="x_content">
                    <table id="tblplanificacion" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th></th>
                          <th>Fecha</th>
                          <th>Salto</th>
                          <th>Equipo</th>
                          <th>Paracaidista</th>
                          <th>Packer</th>
                          <th>Instructor</th>
                          <th>Camara</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>

                  <div id="formularioplanificacion" class="x_content">
                    <form id="formulario" name="formulario" class="form-horizontal form-label-left">
                      <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <select class="form-control selectpicker" onchange="listartabla()" data-live-search="true" id="idmanifest" name="idmanifest" required="required">
                                <option value="" selected disabled>Seleccione pase</option>
                            </select>
                      </div>

                      <div class="clearfix"></div>
                      <div class="ln_solid"></div>

                      <div class="col-md-12 col-sm-12 col-xs-12 form-group" id="plani_manifest">
                        <table id="detalle" name="detalle" class="table table-striped table-bordered table-responsive" cellspacing="0" width="100%" >
                          <thead style="background-color: #e5e5e5">  
                            <th>Venta</th>
                            <th>Salto</th>
                            <th>Equipo</th>
                            <th>Paracaidista</th>
                            <th>Packer</th>
                            <th>Instructor</th>
                            <th>Camara</th>
                            <th></th>
                          </thead>
                          <tfoot>
                            <th>
                              <select class="form-control selectpicker" data-live-search="false" id="idventa" name="idventa" required="required">
                              <option value="" selected disabled>Venta</option>
                              </select>
                            </th>
                            <th>
                              <select class="form-control selectpicker" data-live-search="false" id="idsalto" name="idsalto" required="required">
                              <option value="" selected disabled>Salto</option>
                              </select>
                            </th>
                            <th>
                              <select class="form-control selectpicker" data-live-search="false" id="idequipo" name="idequipo">
                                <option value="" selected disabled>Equipo</option>
                                <option value="SE">Sin Equipo</option>
                              </select>
                            </th>
                            <th>
                              <select class="form-control selectpicker" data-live-search="false" id="idpersona" name="idpersona" required="required">
                              <option value="" selected disabled>Persona</option>
                              </select>
                            </th>
                            <th>
                              <select class="form-control selectpicker" data-live-search="false" id="idpacker" name="idpacker" >
                                <option value="" selected disabled>Packer</option>
                                <option value="SP">Sin Packer</option>
                              </select>
                            </th>
                            <th>
                              <select class="form-control selectpicker" data-live-search="false" id="idinstructor" name="idinstructor">
                                <option value="" selected disabled>Instructor</option>
                                <option value="SI">Sin Instructor</option>
                              </select>
                            </th>
                            <th>
                              <select class="form-control selectpicker" data-live-search="false" id="idcamara" name="idcamara" >
                                <option value="" selected disabled>Camara</option>
                                <option value="SI">Sin Camara</option>
                              </select>
                            </th>
                            <th>
                            <button class="btn btn-success btn-xs" type="submit" id="btnGuardar"><i class="fa fa-plus"></i>
                            </button>
                            </th>
                          </tfoot>
                          </form>
                          <tbody id="plani" name="plani">
                            
                          </tbody>  
                        </table>                       
                      </div>    
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php 
}else{
  require 'nopermiso.php';
}
require 'footer.php';
?>
<script type="text/javascript" src="scripts/planificacion.js"></script>
<?php 
}
ob_end_flush();
?>