<?php 
ob_start();
session_start();

if(!isset($_SESSION["nombre"])){
  header("Location:login.php");
}else{

require 'header.php';

if($_SESSION['manifest']==1)
{

 ?>
        
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Pases</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a id="op_agregar" onclick="mostarform(true)">Agregar</a>
                          </li>
                          <li><a id="op_listar" onclick="mostarform(false)">Listar</a>
                          </li>
                        </ul>
                      </li>                     
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div id="listadopases" class="x_content">

                    <table id="tblpases" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Opciones</th>
                          <th>Fecha</th>
                          <th>Piloto</th>
                          <th>Avion</th>
                          <th>Altura</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>

                  <div id="formulariopases" class="x_content">
                    <br />

                    <div class="col-md-12 center-margin">
	                  <form class="form-horizontal form-label-left" id="formulario" name="formulario">
	                    <div class="col-md-6 col-sm-12 col-xs-12 form-group">
	                      <label>Piloto</label>
	                      <input type="hidden" id="idmanifest" name="idmanifest" class="form-control">
	                      <input type="hidden" id="idvuelo" name="idvuelo" class="form-control">
	                      <select class="form-control selectpicker" data-live-search="false" id="idpiloto" name="idpiloto" required="required">
                          </select>
	                    </div>
	                     <div class="col-md-6 col-sm-12 col-xs-12 form-group">
	                      <label>Avion</label>
	                      <select class="form-control selectpicker" data-live-search="false" id="idavion" name="idavion" required="required">
                          </select>
	                    </div>
	                    <div class="col-md-6 col-sm-12 col-xs-12 form-group">
	                    	<label>Altura</label>
	                      	<input type="text" class="form-control" name="altura" id="altura">
	                 	</div>	                   
	                    <div class="col-md-6 col-sm-12 col-xs-12 form-group">
	                    	<label>Fecha</label>
	                      	<input type="datetime-local" id="fecha" name="fecha" class="form-control" placeholder="Fecha Pase" />
	                  	</div>	           
	                  	<div class="clearfix"></div>
	                 	<div class="ln_solid"></div>        
	                    <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <button class="btn btn-primary" type="button" id="btnCancelar" onclick="cancelarform()">Cancelar</button>
                          <button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiar()">Limpiar</button>
                          <button class="btn btn-success" type="submit" id="btnGuardar">Agregar</button>
                        </div>
                      </div>
	                  </form>
	                </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

<?php 
}else{
  require 'nopermiso.php';
}
require 'footer.php';
?>
<script type="text/javascript" src="scripts/manifest.js"></script>
<?php 
}
ob_end_flush();
?>