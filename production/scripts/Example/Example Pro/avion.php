<?php 
ob_start();
session_start();

if(!isset($_SESSION["nombre"])){
  header("Location:login.php");
}else{

require 'header.php';

if($_SESSION['mantenimiento']==1 & $_SESSION['man_zona']==1)
{

 ?>
        
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Aviones</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a id="op_agregar" onclick="mostarform(true)">Agregar</a>
                          </li>
                          <li><a id="op_listar" onclick="mostarform(false)">Listar</a>
                          </li>
                        </ul>
                      </li>                     
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div id="listadoaviones" class="x_content">

                    <table id="tblaviones" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Opciones</th>
                          <th>Avion</th>
                          <th>Placa</th>
                          <th>Propietario</th>
                          <th>Condicion</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>

                  <div id="formularioaviones" class="x_content">
                    <br />

                    <div class="col-md-12 center-margin">
	                  <form class="form-horizontal form-label-left" id="formulario" name="formulario">
	                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
	                      <label>Propietario</label>
	                      <input type="hidden" id="idavion" name="idavion" class="form-control">
	                      <select class="form-control selectpicker" data-live-search="true" id="idpavion" name="idpavion" required="required">
                          </select>
	                    </div>
	                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
	                    	<label>Marca</label>
	                      	<input type="text" class="form-control" name="marca" id="marca">
	                 	</div>	                   
	                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
	                    	<label>Modelo</label>
	                      	<input type="text" class="form-control" name="modelo" id="modelo">
	                  	</div>	            
	                 	<div class="col-md-3 col-sm-12 col-xs-12 form-group">
	                    	<label>Año</label>
	                      	<input type="text" class="form-control" name="ano" id="ano">
	                  	</div>
	                  	<div class="col-md-3 col-sm-12 col-xs-12 form-group">
	                    	<label>Placa</label>
	                      	<input type="text" class="form-control" name="serial" id="serial">
	                 	</div>        
	                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button class="btn btn-primary" type="button" id="btnCancelar" onclick="cancelarform()">Cancelar</button>
                          <button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiar()">Limpiar</button>
                          <button class="btn btn-success" type="submit" id="btnGuardar">Agregar</button>
                        </div>
                      </div>
	                  </form>
	                </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

<?php 
}else{
  require 'nopermiso.php';
}
require 'footer.php';
?>
<script type="text/javascript" src="scripts/avion.js"></script>
<?php 
}
ob_end_flush();
?>