var tabla;

//funcion que se ejecuta iniciando
function init(){
	console.log("Inicio JS");
	mostarform(false);
	listar();

	$("#formulario").on("submit", function(e){
		guardaryeditar(e);
	})

	$("#imagenmuestra").hide();
}


// Otras funciones
function limpiar(){

	$("#idpiloto").val("");	
	$("#nombre").val("");
	$("#apellido").val("");
	$("#tipo_documento").val("");
	$("#num_documento").val("");
	$("#fecha_nac").val("");
	$("#direccion").val("");
	$("#telefono").val("");	
	$("#email").val("");
	$("#licencia").val("");
	$("#imagenmuestra").attr("src","");
	$("#imagenactual").val("");
	$("#imagen").val("");

}

function mostarform(flag){

	limpiar();
	if(flag){
		$("#listadopilotos").hide();
		$("#formulariopilotos").show();
		$("#op_agregar").hide();
		$("#op_listar").show();
		$("#btnGuardar").prop("disabled", false);

	}else{
		$("#listadopilotos").show();
		$("#formulariopilotos").hide();
		$("#op_agregar").show();
		$("#op_listar").hide();
	}

}

function cancelarform(){
	limpiar();
	mostarform(false);
}

function listar(){
	tabla=$('#tbllistado').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons:[
			'copyHtml5',
			'print',
			'excelHtml5',
			'csvHtml5',
			'pdf'
		],
		"ajax":{
			url:'../ajax/piloto.php?op=listar',
			type:"get",
			dataType:"json",
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 10, //Paginacion 10 items
		"order" : [[1 , "desc"]] //Ordenar en base a la columna 0 descendente
	}).DataTable();
}

function guardaryeditar(e){
	e.preventDefault();
	$("#btnGuardar").prop("disabled", true);
	var formData = new FormData($("#formulario")[0]);
	$.ajax({
		url:'../ajax/piloto.php?op=guardaryeditar',
		type:"POST",
		data:formData,
		contentType: false,
		processData:false,

		success: function(datos){
			bootbox.alert(datos);
			mostarform(false);
			tabla.ajax.reload();
		}
	});
	limpiar();
}

function mostar(idpiloto){
	$.post("../ajax/piloto.php?op=mostar",{idpiloto:idpiloto}, function(data,status){
		data = JSON.parse(data);
		mostarform(true);
	
		$("#idpiloto").val(data.idpiloto);	
		$("#nombre").val(data.nombre);
		$("#apellido").val(data.apellido);
		$("#tipo_documento").val(data.tipo_documento);
		$("#tipo_documento").selectpicker('refresh');
		$("#num_documento").val(data.num_documento);
		$("#fecha_nac").val(data.fecha_nac);	
		$("#direccion").val(data.direccion);
		$("#telefono").val(data.telefono);	
		$("#email").val(data.email);
		$("#licencia").val(data.licencia);
		$("#imagenmuestra").show();
		$("#imagenmuestra").attr("src","../files/pilotos/"+data.imagen);
		$("#imagenactual").val(data.imagen);

	})
}

function desactivar(idpiloto){

	bootbox.confirm("Esta seguro que quiere inhabilitar el piloto?", function(result){
		if(result){
			$.post("../ajax/piloto.php?op=desactivar",{idpiloto:idpiloto}, function(e){
				bootbox.alert(e);
				tabla.ajax.reload();
			})	
		}
	})
}

function activar(idpiloto){

	bootbox.confirm("Esta seguro que quiere habilitar el piloto?", function(result){
		if(result){
			$.post("../ajax/piloto.php?op=activar",{idpiloto:idpiloto}, function(e){
				bootbox.alert(e);
				tabla.ajax.reload();
			})	
		}
	})
}


init();