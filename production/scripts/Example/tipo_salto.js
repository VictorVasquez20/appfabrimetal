var tabla;

//funcion que se ejecuta iniciando
function init(){
	mostarform(false);
	listar();

	$("#formulario").on("submit", function(e){
		guardaryeditar(e);
	})
}


// Otras funciones
function limpiar(){

	$("#idtiposalto").val("");	
	$("#nombre").val("");
	$("#descripcion").val("");

}

function mostarform(flag){

	limpiar();
	if(flag){
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#op_agregar").hide();
		$("#op_listar").show();
		$("#btnGuardar").prop("disabled", false);

	}else{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#op_agregar").show();
		$("#op_listar").hide();
	}

}

function cancelarform(){
	limpiar();
	mostarform(false);
}

function listar(){
	tabla=$('#tbllistado').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons:[
			'copyHtml5',
			'print',
			'excelHtml5',
			'csvHtml5',
			'pdf'
		],
		"ajax":{
			url:'../ajax/tiposalto.php?op=listar',
			type:"get",
			dataType:"json",
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 10, //Paginacion 5 items
		"order" : [[0 , "desc"]] //Ordenar en base a la columna 0 descendente
	}).DataTable();
}

function guardaryeditar(e){
	e.preventDefault();
	$("#btnGuardar").prop("disabled", true);
	console.log($("#formulario")[0]);
	var formData = new FormData($("#formulario")[0]);
	
	$.ajax({
		url:'../ajax/tiposalto.php?op=guardaryeditar',
		type:"POST",
		data:formData,
		contentType: false,
		processData:false,

		success: function(datos){
			bootbox.alert(datos);
			mostarform(false);
			tabla.ajax.reload();
		}
	});
	limpiar();
}

function mostar(idtiposalto){
	$.post("../ajax/tiposalto.php?op=mostar",{idtipo_salto:idtiposalto}, function(data,status){
		data = JSON.parse(data);
		mostarform(true);
		$("#idtipo_salto").val(data.idtipo_salto);	
		$("#nombre").val(data.nombre);
		$("#descripcion").val(data.descripcion);

	})
}

function desactivar(idtiposalto){

	bootbox.confirm("Esta seguro que quiere desactivar el tipo de salto?", function(result){
		if(result){
			$.post("../ajax/tiposalto.php?op=desactivar",{idtipo_salto:idtiposalto}, function(e){
				bootbox.alert(e);
				tabla.ajax.reload();
			})	
		}
	})
}

function activar(idtiposalto){

	bootbox.confirm("Esta seguro que quiere activar el tipo de salto?", function(result){
		if(result){
			$.post("../ajax/tiposalto.php?op=activar",{idtipo_salto:idtiposalto}, function(e){
				bootbox.alert(e);
				tabla.ajax.reload();
			})	
		}
	})
}


init();