var skycons = new Skycons({"color": "gray"});
//funcion que se ejecuta iniciando
function init(){

	//Datos meteorologicos
	tiempo();

	//Generamos Pases - tabs
	pases();

	//Generamos Detalle pase - Contenedor tabs
	detalle();

	//Llenamos en contenedor perfil
	perfil();

	//Mantiene actulizada la pagina
	setInterval( "actualizar()", 150000 );

}

function actualizar(){
	manifest();
	skycons.remove("icono");
	tiempo();
}

function manifest(){
	//Generamos Pases - tabs
	pases();

	//Generamos Detalle pase - Contenedor tabs
	detalle();

	//Llenamos en contenedor perfil
	perfil();
}


function tiempo(){
		$.post("//api.openweathermap.org/data/2.5/weather?q=Casablanca,Cl&APPID=1a028a40ae8c9a6937315ea17b8fa1d5&lang=es", function(data,status){
			var now = new Date();
			var day = ("0" + now.getDate()).slice(-2);
			var month = ("0" + (now.getMonth()+1)).slice(-2);
			var today = now.getFullYear()+"-"+(month)+"-"+(day);
			$("#fecha").html(today);

			var hora = ("0" + now.getHours()).slice(-2);
			var minuto = ("0" + (now.getMinutes()+1)).slice(-2);
			var segundo = ("0" + (now.getSeconds()+1)).slice(-2);
			var time = hora+" : "+minuto+" : "+segundo;
			$("#hora").html(time);

			$("#ciudad").html(data.name);
			$("#pais").html(data.sys.country);
			$("#tiempo").html((data.weather[0].description).toUpperCase());
			var celsius = Math.round((data.main.temp-273));
			$("#temperatura").html(celsius);
			var velocidad = Math.round((data.wind.speed*3.6));
			$("#velocidad").html(velocidad);
			$("#grados").html(data.wind.deg);
			var direccion = "";
			if((data.wind.deg >= 0 && data.wind.deg <= 22.5) || (data.wind.deg >= 337.5 && data.wind.deg <= 362) ){
				direccion = "N"
				$("#direccion").html(direccion);
			}else if(data.wind.deg >= 22.6 && data.wind.deg <= 67.5){
				direccion = "NE"
				$("#direccion").html(direccion);
			}else if(data.wind.deg >= 67.6 && data.wind.deg <= 112.5){
				direccion = "E"
				$("#direccion").html(direccion);
			}else if(data.wind.deg >= 112.6 && data.wind.deg <= 157.5){
				direccion = "SE"
				$("#direccion").html(direccion);
			}else if(data.wind.deg >= 157.6 && data.wind.deg <= 202.5){
				direccion = "S"
				$("#direccion").html(direccion);
			}else if(data.wind.deg >= 202.6 && data.wind.deg <= 247.5){
				direccion = "SO"
				$("#direccion").html(direccion);
			}else if(data.wind.deg >= 247.6 && data.wind.deg <= 292.5){
				direccion = "O"
				$("#direccion").html(direccion);
			}else{
				direccion = "NO"
				$("#direccion").html(direccion);
			}
			$("#nubosidad").html(data.clouds.all);
			$("#visibilidad").html(data.visibility);

			
			if(data.weather[0].icon == "01d"){
				skycons.add("icono", Skycons.CLEAR_DAY);
			}else if(data.weather[0].icon == "01n"){
				skycons.add("icono", Skycons.CLEAR_NIGHT);
			}else if(data.weather[0].icon == "02d"){
				skycons.add("icono", Skycons.PARTLY_CLOUDY_DAY);
			}else if(data.weather[0].icon == "02n"){
				skycons.add("icono", Skycons.PARTLY_CLOUDY_NIGHT);
			}else if(data.weather[0].icon == "03d" || data.weather[0].icon == "03n" || data.weather[0].icon == "04d" || data.weather[0].icon == "04n"){
				skycons.add("icono", Skycons.CLOUDY);
			}else if(data.weather[0].icon == "10d" || data.weather[0].icon == "10n" || data.weather[0].icon == "09d" || data.weather[0].icon == "09n"){
				skycons.add("icono", Skycons.RAIN);
			}else if(data.weather[0].icon == "11d" || data.weather[0].icon == "11n"){
				skycons.add("icono", Skycons.SLEET);
			}else if(data.weather[0].icon == "13d" || data.weather[0].icon == "13n"){
				skycons.add("icono", Skycons.SNOW);
			}else{
				skycons.add("icono", Skycons.FOG);
			}
			
			
  		
  			skycons.play();

	})
}

function pases(){
	$.post("../ajax/pase.php?op=listarPase", function(r){		
		$("#pases").html(r);
	});
}

function detalle(){
	$.post("../ajax/pase.php?op=listarVuelo", function(r){		
		$("#tabdetalle").html(r);
	});
}

function perfil(){
		$.post("../ajax/pase.php?op=listarPlanificaion", function(data,status){
		data = JSON.parse(data);
		console.log(data);
		console.log(data.tdata.length);

		for (var i=0; i<data.tdata.length; i++){
			var str1 = "#profile";
			var res = str1.concat(data.tdata[i][0]);
			console.log(res);
			console.log(data.tdata[i][1]);
			$( res ).append(data.tdata[i][1]);			
			//$(res).html(data.tdata[i][1]);
		}


	})
}



init();