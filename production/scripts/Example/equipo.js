var tabla;

//funcion que se ejecuta iniciando
function init(){
	mostarform(false);
	listar();

	$("#formulario").on("submit", function(e){
		guardaryeditar(e);
	})

	$.post("../ajax/pequipo.php?op=selectpequipo", function(r){
		$("#idpequipo").html(r);
		$("#idpequipo").selectpicker('refresh');
	});

}


// Otras funciones
function limpiar(){

	$("#idequipo").val("");
	$("#idpequipo").val("");
	$("#idpequipo").selectpicker('refresh');	
	$("#marca").val("");
	$("#modelo").val("");
	$("#ano").val("");
	$("#codigo").val("");

}

function mostarform(flag){

	limpiar();
	if(flag){
		$("#listadoequipos").hide();
		$("#formularioequipos").show();
		$("#op_agregar").hide();
		$("#op_listar").show();
		$("#btnGuardar").prop("disabled", false);

	}else{
		$("#listadoequipos").show();
		$("#formularioequipos").hide();
		$("#op_agregar").show();
		$("#op_listar").hide();
	}

}

function cancelarform(){
	limpiar();
	mostarform(false);
}

function listar(){
	tabla=$('#tblequipos').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons:[
			'copyHtml5',
			'print',
			'excelHtml5',
			'csvHtml5',
			'pdf'
		],
		"ajax":{
			url:'../ajax/equipo.php?op=listar',
			type:"get",
			dataType:"json",
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 10, //Paginacion 10 items
		"order" : [[1 , "desc"]] //Ordenar en base a la columna 0 descendente
	}).DataTable();
}

function guardaryeditar(e){
	e.preventDefault();
	$("#btnGuardar").prop("disabled", true);
	var formData = new FormData($("#formulario")[0]);
	$.ajax({
		url:'../ajax/equipo.php?op=guardaryeditar',
		type:"POST",
		data:formData,
		contentType: false,
		processData:false,

		success: function(datos){
			bootbox.alert(datos);
			mostarform(false);
			tabla.ajax.reload();
		}
	});
	limpiar();
}

function mostar(idequipo){
	$.post("../ajax/equipo.php?op=mostar",{idequipo:idequipo}, function(data,status){
		data = JSON.parse(data);
		mostarform(true);
	
		$("#idequipo").val(data.idequipo);
		$("#idpequipo").val(data.idpropietario_equipo);
		$("#idpequipo").selectpicker('refresh');
		$("#marca").val(data.marca);
		$("#modelo").val(data.modelo);
		$("#ano").val(data.ano);
		$("#codigo").val(data.codigo);

	})
}

function desactivar(idequipo){

	bootbox.confirm("Esta seguro que quiere inhabilitar el avion?", function(result){
		if(result){
			$.post("../ajax/equipo.php?op=desactivar",{idequipo:idequipo}, function(e){
				bootbox.alert(e);
				tabla.ajax.reload();
			})	
		}
	})
}

function activar(idequipo){

	bootbox.confirm("Esta seguro que quiere habilitar el avion?", function(result){
		if(result){
			$.post("../ajax/equipo.php?op=activar",{idequipo:idequipo}, function(e){
				bootbox.alert(e);
				tabla.ajax.reload();
			})	
		}
	})
}


init();