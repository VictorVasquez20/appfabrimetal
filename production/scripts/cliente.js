var tabla, tabascensores, tabedificios, tabcontratos, tabcontactos;

//funcion que se ejecuta iniciando
function init(){
	mostarform(false);
	listar();

	$('[data-toggle="tooltip"]').tooltip(); 

	$('#formulario').on("submit", function(event){
		event.preventDefault();
		guardaryeditar();
	});
       
	$.post("../ajax/regiones.php?op=selectRegiones", function(r){
		$("#idregiones").html(r);
		$("#idregiones").selectpicker('refresh');
	});

	$("#idregiones").on("change", function(e){
		$.get("../ajax/provincia.php?op=selectProvincia",{id:$("#idregiones").val()}, function(r){
			$("#idprovincias").html(r);
			$("#idprovincias").selectpicker('refresh');
		});
	});

	$("#idprovincias").on("change", function(e){
		$.get("../ajax/comunas.php?op=selectComunas",{id:$("#idprovincias").val()}, function(r){
			$("#idcomunas").html(r);
			$("#idcomunas").selectpicker('refresh');
		});
	});
        
        
        $.post("../ajax/tcliente.php?op=selecttcliente", function(r){
                $("#idtcliente").html(r);
                $("#idtcliente").selectpicker('refresh');
	});
        

}


// Otras funciones
function limpiar(){
	$("#idcliente").val("");	
	$("#razon").val("");
        $("#rut").val("");
	$("#calle").val("");
        $("#numero").val("");
	$("#oficina").val("");
        $("#idregiones").val("");
	$("#idregiones").selectpicker('refresh');
	$("#idprovincias").val("");
	$("#idprovincias").selectpicker('refresh');
	$("#idcomunas").val("");
	$("#idcomunas").selectpicker('refresh');
        $("#idtcliente").val("");
        $("#idtcliente").selectpicker('refresh');
}

function mostarform(flag){
	limpiar();
	if(flag){
		$("#tabcliente").hide();
		$("#listadoclientes").hide();
		$("#formularioclientesid").show();
		$("#op_actualizar").hide();
		$("#op_listar").show();
                $("#titulo_pagina").html("Editar Clientes");
	}else{
		$("#tabcliente").hide();
                $("#formularioclientesid").hide();
		$("#listadoclientes").show();
		$("#op_actualizar").show();
		$("#op_listar").hide();
                $("#titulo_pagina").html("Clientes en Cartera");
	}

}

function cancelarform(){
	limpiar();
	mostarform(false);
}


function listar(){
	tabla=$('#tblclientes').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons:[
			'copyHtml5',
			'print',
			'excelHtml5',
			'csvHtml5',
			'pdf'
		],
		"ajax":{
			url:'../ajax/cliente.php?op=listarclientes',
			type:"get",
			dataType:"json",
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 15, //Paginacion 10 items
		"order" : [[1 , "asc"]] //Ordenar en base a la columna 0 descendente
	}).DataTable();
}


function guardaryeditar(){
	$("#btnGuardar").prop("disabled", true);
	var formData = new FormData($("#formulario")[0]);
	$.ajax({
		url:'../ajax/cliente.php?op=editar',
		type:"POST",
		data:formData,
		contentType: false,
		processData:false,

		success: function(datos){
			bootbox.alert(datos);
			mostarform(false);
			tabla.ajax.reload();
		}
	});
	limpiar();
}

function mostrar(idcliente){

	$("#listadoclientes").hide();
	$("#formulariocliente").hide();
	$("#op_actualizar").hide();
	$("#op_listar").show();
	$("#tabcliente").show();
        $("#titulo_pagina").html("Mostrar Cliente");
        
	$.post("../ajax/cliente.php?op=mostrar",{idcliente:idcliente}, function(data){
		data = JSON.parse(data);
                $("#tabrazonti").html(data.razon_social);
		$("#tabrazon").html(data.razon_social);
                $("#tabrut").html(data.rut);
                $("#tabtipo").html(data.tipo);
                $("#tabregion").html(data.region);
                $("#tabprovincia").html(data.provincia);
                $("#tabcomuna").html(data.comuna);              
	});
        
        
        
        
        tabcontratos=$('#tbltabcontratos').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons:[
			'copyHtml5',
			'print',
			'excelHtml5',
			'csvHtml5',
			'pdf'
		],
		"ajax":{
			url:'../ajax/cliente.php?op=contratos_cliente',
			type:"POST",                       
			dataType:"json",
                        data:{idcliente:idcliente},
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 7, //Paginacion 10 items
		"order" : [[1 , "desc"]] //Ordenar en base a la columna 0 descendente
	}).DataTable();
        
        
        tabedificios=$('#tbltabedificios').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons:[
			'copyHtml5',
			'print',
			'excelHtml5',
			'csvHtml5',
			'pdf'
		],
		"ajax":{
			url:'../ajax/cliente.php?op=edificios_cliente',
			type:"POST",                       
			dataType:"json",
                        data:{idcliente:idcliente},
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 7, //Paginacion 10 items
		"order" : [[1 , "desc"]] //Ordenar en base a la columna 0 descendente
	}).DataTable();
        
        tabascensores=$('#tbltabascensores').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons:[
			'copyHtml5',
			'print',
			'excelHtml5',
			'csvHtml5',
			'pdf'
		],
		"ajax":{
			url:'../ajax/cliente.php?op=ascensores_cliente',
			type:"POST",                       
			dataType:"json",
                        data:{idcliente:idcliente},
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 7, 
		"order" : [[1 , "desc"]]
	}).DataTable();
        
        tabcontactos=$('#tbltabcontactos').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons:[
			'copyHtml5',
			'print',
			'excelHtml5',
			'csvHtml5',
			'pdf'
		],
		"ajax":{
			url:'../ajax/cliente.php?op=contactos_cliente',
			type:"POST",                       
			dataType:"json",
                        data:{idcliente:idcliente},
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 7, 
		"order" : [[1 , "desc"]]
	}).DataTable();
}

function editar(idcliente){
    $.post("../ajax/cliente.php?op=formeditar",{idcliente:idcliente}, function(data,status){
		data = JSON.parse(data);
		mostarform(true);
		$("#idcliente").val(data.idcliente);	
		$("#razon").val(data.razon_social);
                $("#rut").val(data.rut);
		$("#calle").val(data.calle);
		$("#numero").val(data.numero);
                $("#oficina").val(data.oficina);      
                $("#idregiones").val(data.idregiones);                
		$("#idregiones").selectpicker('refresh');
		$.get("../ajax/provincia.php?op=selectProvincia",{id:data.idregiones}, function(r){
			$("#idprovincias").html(r);
			$("#idprovincias").val(data.idprovincias);
			$("#idprovincias").selectpicker('refresh');
		});
		$.get("../ajax/comunas.php?op=selectComunas",{id:data.idprovincias}, function(r){
			$("#idcomunas").html(r);
			$("#idcomunas").val(data.idcomunas);
			$("#idcomunas").selectpicker('refresh');
		});
                
                $.post("../ajax/tcliente.php?op=selecttcliente", function(r){
                        $("#idtcliente").html(r);
			$("#idtcliente").val(data.idtcliente);
			$("#idtcliente").selectpicker('refresh');
                });
                
	});
}



init();