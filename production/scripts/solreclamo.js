var tabla;

function init(){
    listar();
}

function listar(){
    
	tabla=$('#tblreclamo').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons:[
			'copyHtml5',
			'print',			
			'csvHtml5'			
		],
		"ajax":{
			url:'../ajax/solreclamo.php?op=listar',
			type:"get",
			dataType:"json",
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 10,
		"ordering" : false
	}).DataTable();
}

function procesado(idsolreclamo){

	bootbox.confirm("ESTA SEGURO QUE QUIERE CAMBIAR EL ESTADO DE SOLICITUD A PROCESADA?", 
        
        function(result){
            
        if(result){
            $.post("../ajax/solreclamo.php?op=procesar",{"idsolreclamo":idsolreclamo}, 
            function(data){
                    if(parseInt(data.estatus)=== 1){ 
                            new PNotify({
                             title: 'Correcto!',
                             text: data.mensaje,
                             type: 'success',
                             styling: 'bootstrap3'
                            });                                                        
                    }else if(parseInt(data.estatus) === 0){
                        new PNotify({
                         title: 'Error!',
                         text: data.mensaje,
                         type: 'error',
                         styling: 'bootstrap3'
                     });
                 } 
                    tabla.ajax.reload();
            },"json");	
		}
	});
}

function descartar(idsolreclamo){
    
        bootbox.prompt("Indique el motivo por el que Descarta la Solicitud.",function(result){                 
            if(result){
                
                $.post("../ajax/solreclamo.php?op=descartar",
                {"idsolreclamo":idsolreclamo,"motivo":result}, 
                function(data){
                        if(parseInt(data.estatus)=== 1){ 
                                new PNotify({
                                 title: 'Correcto!',
                                 text: data.mensaje,
                                 type: 'success',
                                 styling: 'bootstrap3'
                                });                                                        
                        }else if(parseInt(data.estatus) === 0){
                            new PNotify({
                             title: 'Error!',
                             text: data.mensaje,
                             type: 'error',
                             styling: 'bootstrap3'
                         });
                     } 
                    tabla.ajax.reload();
                },"json");	

            }          
    });
    
}

function actualizar() {
    tabla.ajax.reload();
}


init();