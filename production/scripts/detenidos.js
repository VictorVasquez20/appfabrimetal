var tabla;
var count = 1;
//funcion que se ejecuta iniciando
function init() {
    cargartiles();
    GraficoPresupuestosPorMes();
    listar();
}

function cargartiles() {
    $.post("../ajax/detenidos.php?op=ContarDatos", function(data, status) {
        data = JSON.parse(data);
        //Actualizamos valores
        $("#total_detenidos").html(data.detenidos.total);
        $("#en_evaluacion").html(data.evaluacion);
        $("#cotizados").html(data.cotizados);
        $("#aprobados").html(data.aprobados);

    });
}

function GraficoPresupuestosPorMes() {

    var fecha = new Date();
    var anio_busq;

    anio_busq = fecha.getFullYear();

    $("#titulo_grafico_lineal").html("PRESUPUESTOS DEL AÑO: " + anio_busq);

    $.post("../ajax/detenidos.php?op=GraficoDetenidosPorMes", { 'anio_busq': anio_busq },
        function(data, status) {
            data = JSON.parse(data);

            arrIn = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

            for (i = 0; i < data.ingreso.length; i++) {
                arrIn[parseInt(data.ingreso[i]['mes']) - 1] = data.ingreso[i]['cantidad'];;
            }

            if ($('#mybarChart').length) {
                var ctx = document.getElementById("mybarChart");
                lineChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"],
                        datasets: [{
                            label: "Guías de servicio",
                            backgroundColor: "rgba(38, 185, 154, 0.31)",
                            borderColor: "rgba(38, 185, 154, 0.7)",
                            pointBorderColor: "rgba(38, 185, 154, 0.7)",
                            pointBackgroundColor: "rgba(38, 185, 154, 0.7)",
                            pointHoverBackgroundColor: "#fff",
                            pointHoverBorderColor: "rgba(220,220,220,1)",
                            pointBorderWidth: 1,
                            data: arrIn
                        }, ]
                    },
                    options: {
                        legend: {
                            display: false
                        },
                        scales: {
                            xAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Mes'
                                }
                            }],
                            yAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Cantidad de GSE'
                                }
                            }]
                        }
                    }
                });

            }
        });
}

function listar() {

    tabla = $('#tbldetenidos').dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Bfrtip',
        buttons: Botones,
        "language": Español,
        "ajax": {
            url: '../ajax/detenidos.php?op=listar',
            type: "get",
            dataType: "json",
            error: function(e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 200, //Paginacion 10 items
        "order": [
                [5, "asc"]
            ] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}

init();