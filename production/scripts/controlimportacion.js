var tabla;
var tabla2;
var tabla3;

//funcion que se ejecuta iniciando
function init() {
    mostarform(false);
    $.post("../ajax/controlimportacion.php?op=listaranios", function (r) {
        $("#listadoanio").html(r);
        $("#listadoanio").selectpicker('refresh');
    });
    listar();
    // mostrarcomentarios('1');
    // $('#modalComent').modal('toggle');

    $("#formulario").on("submit", function (e) {
        guardaryeditar(e);
    });
}


// Otras funciones
function limpiar(guardado) {
    if(guardado == 0){
        $("#ctli_numref").val('');
        $("#ctli_fechapedido").val('');
        $("#ctli_codkm").val('');
        $("#ctli_descripcion").val('');
        $("#ctli_codst").val('');
        $("#ctli_codst_fecha").val('');
        $("#ctli_cantidad").val('');
        $("#ctli_numpres").val('');
        $("#ctli_solicitante").val('');
        $("#ctli_observacion").val('');
    }else if (guardado == 1) {
        $("#ctli_codkm").val('');
        $("#ctli_descripcion").val('');
        $("#ctli_codst").val('');
        $("#ctli_codst_fecha").val('');
        $("#ctli_cantidad").val('');
        $("#ctli_numpres").val('');
        $("#ctli_solicitante").val('');
        $("#ctli_observacion").val('');
    }
    else {
        $("#ctli_codkm").val('');
        $("#ctli_descripcion").val('');
        $("#ctli_cantidad").val('');
        $("#ctli_numpres").val('');
    }
}

function mostarform(flag) {
    limpiar(0);
    if (flag) {
        $("#listadoimportacion").hide();
        $("#formularioimportacion").show();
        $("#op_agregar").hide();
        $("#op_listar").show();
        $("#agregar").show();
        tabla.ajax.reload();
        $("#listagregado").html('<table id="tblimportacionporcodigo" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%"><thead><tr><th> </th><th> ST</th><th> ST Fecha</th><th> KM</th><th> Descripción</th><th> Cantidad</th><th> N° de Presupuesto</th><th> Solicitante</th><th> Observación</th><th> Status</th></tr></thead><tbody></tbody></table>')
        $("#btnGuardar").prop("disabled", false);
        $("#ctli_numref").removeAttr('readonly');
        $("#ctli_fechapedido").removeAttr('readonly');
        $("#filtros").hide();
    } else {
        $("#filtros").show();
        $("#agregar").hide();
        $("#listadoimportacion").show();
        $("#formularioimportacion").hide();
        $("#op_agregar").show();
        $("#op_listar").hide();
    }
}

function cancelarform() {
    limpiar(0);
    mostarform(false);
}

function cancelaredicion() {
    limpiar(1);

    $('#btnGuardar').show();
    $('#btnEditar').hide();
}

function listar() {
    var mes = $('#listadomes option:selected').val();
    var anio = $('#listadoanio option:selected').val();
    tabla = $('#tblimportacion').dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Bfrtip',
        buttons: [
            {
                text:'<i class="fa fa-file-excel-o" aria-hidden="true"></i> Exportar Listado',
                action: function (e, dt, node, config) {
                    $.ajax({
                        "url": "../ajax/controlimportacion.php?op=listarcsv&mes="+mes+"&anio="+anio,
                        "data": dt.ajax.params(),
                        "success": function(res, status, xhr) {
                            var csvData = new Blob([res], {type: 'text/csv;charset=utf-8;'});
                            var csvURL = window.URL.createObjectURL(csvData);
                            var tempLink = document.createElement('a');
                            tempLink.href = csvURL;
                            var d = new Date();
                            var nombre = "Control Importacion "+d.getFullYear() + "-" + (d.getMonth()+1) + "-" + d.getDate()+ " " + d.getHours() + "_" + d.getMinutes() + "_" + d.getSeconds();
                            tempLink.setAttribute('download', nombre+'.csv');
                            tempLink.click();
                        }
                    });
                }
            },
        ],//Botones,
        "language": Español,
        "ajax": {
            url: '../ajax/controlimportacion.php?op=listar',
            type: "get",
            data: { mes: mes, anio: anio},
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "columnDefs": [
            {
                "targets": [ 3 ],
                "visible": false,
                "searchable": true
            },
        ],
        "columns": [
            null,
            null,
            {"render": function(data, type) {
                var date =  new Date(data);
                var dia =(date.getDate()<10 ? '0' : '')+date.getDate();
                var mes = ((date.getMonth()+1)<10 ? '0' : '')+(date.getMonth()+1);
                var anio = date.getFullYear();
                return '<span style="display:none;">'+anio+mes+dia+'</span>'+dia+"/"+mes+"/"+anio;
            }},
            null
        ],
        "bDestroy": true,
        "iDisplayLength": 10, //Paginacion 10 items
        "order": [[1, "desc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}

function guardar() {
    var formData = new FormData($("#formulario")[0]);
    var valido = true;
    var mensaje = '';

    $('#formulario input').each(function() {
        var $this = $(this);
        if(!$this.val()) {
            var inputName = $this.attr('name');
            if(inputName != 'ctli_codst_fecha'){
                valido = false;
            }
            console.log(inputName);
            mensaje = 'Existen campos vacios!';
        }
    });
    if(valido){
        $("#ctli_numref").attr('readonly',true);
        $("#ctli_fechapedido").attr('readonly',true);
        var codigo = $("#ctli_numref").val();
        $.ajax({
            url: '../ajax/controlimportacion.php?op=guardar',
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            success: function (datos) {
                if (datos !== 0){
                    new PNotify({
                        title: 'Registrado!',
                        text: 'Registro con exito.',
                        type: 'success',
                        styling: 'bootstrap3'
                    });
                    $("#listagregado").show();
                    tabla2 = $('#tblimportacionporcodigo').dataTable({
                        "aProcessing": true,
                        "aServerSide": true,
                        dom: 'Bfrtip',
                        buttons:[
                        ],
                        "language": Español,
                        "ajax": {
                            url: '../ajax/controlimportacion.php?op=listarporcodigo&codigo='+codigo,
                            type: "get",
                            dataType: "json",
                            error: function (e) {
                                console.log(e.responseText);
                            }
                        },
                        "bDestroy": true,
                        "iDisplayLength": 10, //Paginacion 10 items
                        "order": [[1, "desc"]] //Ordenar en base a la columna 0 descendente
                    }).DataTable();
                    tabla.ajax.reload();
                    limpiar(2);
                }else{
                    new PNotify({
                        title: 'Error!',
                        text: 'Hubo un error al registar. ',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                }
            }
        });
    }else{
        new PNotify({            
            text: mensaje,
            type: 'warning',
            styling: 'bootstrap3'
        });
    }
    // limpiar();
}

function editaritem(id){
	$.ajax({
		url: '../ajax/controlimportacion.php?op=mostraritem&codigo='+id,
		type: "get",
		contentType: false,
		processData: false,
		success: function (datos) {
			var obj = $.parseJSON(datos);
			$("#id").val(obj.ctli_id);
			$("#ctli_codkm").val(obj.ctli_codkm);
			$("#ctli_descripcion").val(obj.ctli_descripcion);
			$("#ctli_codst").val(obj.ctli_codst);
            $("#ctli_codst_fecha").val(obj.ctli_codst_fecha);
			$("#ctli_cantidad").val(obj.ctli_cantidad);
			$("#ctli_numpres").val(obj.ctli_numpres);
			$("#ctli_solicitante").val(obj.ctli_solicitante);
			$("#ctli_observacion").val(obj.ctli_observacion);
			$("#btnEditar").show();
			$("#btnGuardar").hide();
			return false;
			/*if (datos !== 0){
				$("#listagregado").show();
			}else{
				new PNotify({
					title: 'Error!',
					text: 'Hubo un error al cargar la información. ',
					type: 'error',
					styling: 'bootstrap3'
				});
			}*/
		}
	});
}
function guardareditar() {
    var formData = new FormData($("#formulario")[0]);
    var valido = true;
    var mensaje = '';

    $('#formulario input').each(function() {
        var $this = $(this);
        if(!$this.val()) {
            var inputName = $this.attr('name');
            valido = false;
            mensaje = 'Existen campos vacios!';
        }
    });
    if(valido){
        $("#ctli_numref").attr('readonly',true);
        $("#ctli_fechapedido").attr('readonly',true);
        var codigo = $("#ctli_numref").val();
        $.ajax({
            url: '../ajax/controlimportacion.php?op=guardareditar',
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            success: function (datos) {
            	var obj = $.parseJSON(datos);
            	if(obj.status == 'success'){
                    new PNotify({
                        title: 'Editado!',
                        text: 'Edición con exito.',
                        type: 'success',
                        styling: 'bootstrap3'
                    });
                    tabla2 = $('#tblimportacionporcodigo').dataTable({
                        "aProcessing": true,
                        "aServerSide": true,
                        dom: 'Bfrtip',
                        buttons:[
                        ],
                        "language": Español,
                        "ajax": {
                            url: '../ajax/controlimportacion.php?op=listarporcodigo&codigo='+codigo,
                            type: "get",
                            dataType: "json",
                            error: function (e) {
                                console.log(e.responseText);
                            }
                        },
                        "bDestroy": true,
                        "iDisplayLength": 10, //Paginacion 10 items
                        "order": [[1, "desc"]] //Ordenar en base a la columna 0 descendente
                    }).DataTable();
                    tabla.ajax.reload();
                    $("#btnEditar").hide();
					$("#btnGuardar").show();
                    limpiar(1);
                }else{
                    new PNotify({
                        title: 'Error!',
                        text: 'Hubo un error al registar. ',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                }
            }
        });
    }else{
        new PNotify({            
            text: mensaje,
            type: 'warning',
            styling: 'bootstrap3'
        });
    }
    limpiar();
}

function mostrar(codigo,fecha){
    mostarform(true);
    $("#ctli_numref").val(codigo);
    $("#ctli_fechapedido").val(fecha);
    $("#ctli_numref").attr('readonly',true);
    $("#ctli_fechapedido").attr('readonly',true);
    // $("#agregar").hide();
    tabla2 = $('#tblimportacionporcodigo').dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Bfrtip',
        buttons:[
        ],
        "language": Español,
        "ajax": {
            url: '../ajax/controlimportacion.php?op=listarporcodigo&codigo='+codigo,
            type: "get",
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 10, //Paginacion 10 items
        "order": [[8, "asc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}

function mostrarfechas(codigo, codkm){
    $('#idcom').val(codigo); //asigno valor al campo oculto del formulario en el modal
    $('#codkm').text(codkm);
    $.ajax({
		url: '../ajax/controlimportacion.php?op=mostrarfechas&codigo='+codigo,
		type: "get",
		contentType: false,
		processData: false,
		success: function (datos) {
			var obj = $.parseJSON(datos);
			if(obj.ctli_fechaestdesp !== ""){
				$("#formfechas #ctli_fechaestdesp").val(obj.ctli_fechaestdesp);
			}
			if(obj.ctli_fechadesp !== ""){
				$("#formfechas #ctli_fechadesp").val(obj.ctli_fechadesp);
			}
			if(obj.ctli_fechaestllegada !== ""){
				$("#formfechas #ctli_fechaestllegada").val(obj.ctli_fechaestllegada);
			}
			if(obj.ctli_fechallegada !== ""){
				$("#formfechas #ctli_fechallegada").val(obj.ctli_fechallegada);
			}
		}
	});
    $('#modalFechas').modal('toggle');
}

function guardarFechas(){
	var formData = new FormData($("#formfechas")[0]);
	var codigo = $("#ctli_numref").val();
	$.ajax({
		url: '../ajax/controlimportacion.php?op=guardarfechas',
		type: "POST",
		data: formData,
		contentType: false,
		processData: false,
		success: function (datos) {
			var obj = $.parseJSON(datos);
			if(obj.status == 'success'){
				new PNotify({
					title: 'Registro Exitoso!',
					text: 'Las fechas fueron registradas con exito.',
					type: 'success',
					styling: 'bootstrap3'
				});
				tabla2 = $('#tblimportacionporcodigo').dataTable({
					"aProcessing": true,
					"aServerSide": true,
					dom: 'Bfrtip',
					buttons:[],
					"language": Español,
					"ajax": {
						url: '../ajax/controlimportacion.php?op=listarporcodigo&codigo='+codigo,
						type: "get",
						dataType: "json",
						error: function (e) {
							console.log(e.responseText);
						}
					},
					"bDestroy": true,
					"iDisplayLength": 10, //Paginacion 10 items
					"order": [[1, "desc"]] //Ordenar en base a la columna 0 descendente
				}).DataTable();
				tabla.ajax.reload();
				$("#btnEditar").hide();
				$("#btnGuardar").show();
				limpiar(2);
				$('#modalFechas').modal('hide');
			}else{
				new PNotify({
					title: 'Error!',
					text: 'Hubo un error al registar. ',
					type: 'error',
					styling: 'bootstrap3'
				});
			}
		}
	});
}

function mostrarcomentarios(codigo, codkm){
    $('#idcom2').val(codigo); //asigno valor al campo oculto del formulario en el modal
    $('#codkm2').text(codkm);

    tabla3 = $('#tblcomentarios').dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Bfrtip',
        "autoWidth": false,
        buttons:[
        ],
        "language": Español,
        "ajax": {
            url: '../ajax/controlimportacion.php?op=listarcomentarios&codigo='+codigo,
            type: "get",
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        columnDefs: [
                {
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap width-98'>" + data + "</div>";
                    },
                    targets: 1
                },
                // { "width": "25%", "targets": 0 }
                { "width": "18%", "targets": 0 },
                { "width": "15%", "targets": 1 },
             ],
        "bDestroy": true,
        "iDisplayLength": 5, //Paginacion 5 items
        "order": [[0, "desc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();

    $('#modalComent').modal('toggle');
}

function cambiarstatus(codigo) {
    bootbox.confirm("¿Desea dar por terminado el item de importacion?", function(result) {
        if(result){
            var dataform = {
                idcom: codigo,
            }

            $.ajax({
                url: '../ajax/controlimportacion.php?op=cambiarstatus',
                type: "POST",
                data: dataform,
                success: function (datos) {
                    if (datos !== 0){
                        new PNotify({
                            title: 'Registrado!',
                            text: 'El status ha cambiado con exito.',
                            type: 'success',
                            styling: 'bootstrap3'
                        });

                        tabla2.ajax.reload();
                    }else{
                        new PNotify({
                            title: 'Error!',
                            text: 'Hubo un error al cambiar el status. ',
                            type: 'error',
                            styling: 'bootstrap3'
                        });
                    }
                }
            });
        }
    });
}

$('body').on('submit', '#formcomentario', function(e) {
    e.preventDefault();
    var formData = new FormData($(this)[0]);
    // alert("Hello");

    $.ajax({
        url: '../ajax/controlimportacion.php?op=guardarcomentario',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function (datos) {
            if (datos !== 0){
                new PNotify({
                    title: 'Registrado!',
                    text: 'Comentario registrado con exito.',
                    type: 'success',
                    styling: 'bootstrap3'
                });

                $('#comentario').val('');
                tabla3.ajax.reload();
            }else{
                new PNotify({
                    title: 'Error!',
                    text: 'Hubo un error al registar. ',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
        }
    });
});
$('#modalFechas').on('hidden.bs.modal', function () {
	$("#formfechas #ctli_fechaestdesp").val('');
	$("#formfechas #ctli_fechadesp").val('');
	$("#formfechas #ctli_fechaestllegada").val('');
	$("#formfechas #ctli_fechallegada").val('');
});

$(document).ready(function(){
    $("#listadoanio").change(function(){ 
        /*$.post("../ajax/presupuesto.php?op=listaredificio",{idsupervisor:$(this).children("option:selected").val()}).done(function (r) {
            $("#listaedificio").html(r);
            $("#listaedificio").selectpicker('refresh');
        });*/
        listar();//alert($(this).children("option:selected").val());

    });
    $("#listadomes").change(function(){ 
        /*$.post("../ajax/presupuesto.php?op=listaredificio",{idsupervisor:$(this).children("option:selected").val()}).done(function (r) {
            $("#listaedificio").html(r);
            $("#listaedificio").selectpicker('refresh');
        });*/
        listar();//alert($(this).children("option:selected").val());

    });


});

init();