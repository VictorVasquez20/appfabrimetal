var tabla;

//funcion que se ejecuta iniciando
function init() {
    mostrarform(false);
    listar();

    $("#formulario").on("submit", function (e) {
        guardaryeditar(e);
    })
}


// Otras funciones
function limpiar() {
    $("#idmarca").val("");
    $("#nombre").val("");

}

function mostrarform(flag) {

    limpiar();
    if (flag) {
        $("#listadoizaje").hide();
        $("#formularioizaje").show();
        $("#op_agregar").hide();
        $("#op_listar").show();
        $("#btnGuardar").prop("disabled", false);

    } else {
        $("#listadoizaje").show();
        $("#formularioizaje").hide();
        $("#op_agregar").show();
        $("#op_listar").hide();
    }

}

function cancelarform() {
    limpiar();
    mostrarform(false);
}

function listar() {
    tabla = $('#tblizaje').dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Bfrtip',
        buttons: Botones,
        "language": Español,
        "ajax": {
            url: '../ajax/marca_izaje.php?op=listar',
            type: "get",
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 10, //Paginacion 10 items
        "order": [[0, "desc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}

function guardaryeditar(e) {
    e.preventDefault();
    $("#btnGuardar").prop("disabled", true);
    var formData = new FormData($("#formulario")[0]);

    $.ajax({
        url: '../ajax/marca_izaje.php?op=guardaryeditar',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,

        success: function (datos) {
            if(datos != 0){
                new PNotify({
                    title: 'Correcto!',
                    text: 'guardada con exito.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
            }else{
                new PNotify({
                    title: 'Error!',
                    text: 'no guardada.' + datos,
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
            mostrarform(false);
            tabla.ajax.reload();
        }
    });
    limpiar();
}

function mostrar(idmarca) {
    $.post("../ajax/marca_izaje.php?op=mostrar", {idmarca: idmarca}, function (data, status) {
        data = JSON.parse(data);
        mostrarform(true);
        
        $("#idmarca").val(data.idmarca);
        $("#nombre").val(data.nombre);
    });
}

init();