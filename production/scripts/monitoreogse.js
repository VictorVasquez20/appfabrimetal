//Var Globales
var markers = [];
var refdata = [];
var map;
var refnuevos = [];


var detalle = {
    1: {
        icon: '../public/api/normal.png',
        info: 'NORMAL'
    },
    2: {
        icon: '../public/api/mantenido.png',
        info: 'MANTENCION'
    },
    3: {
        icon: '../public/api/reparacion.png',
        info: 'REPARACION'
    },
    4: {
        icon: '../public/api/repmayor.png',
        info: 'REPARACION MAYOR'
    },
    5: {
        icon: '../public/api/repingenieria.png',
        info: 'INGENIERIA'
    },
    6: {
        icon: '../public/api/emergencia.png',
        info: 'EMERGENCIA'
    },
    7: {
        icon: '../public/api/detenido.png',
        info: 'DETENIDO'
    },
    8: {
        icon: '../public/api/observacion.png',
        info: 'DETENIDO'
    },
    9: {
        icon: '../public/api/ticketmer.png',
        info: 'TICKET EMERGANCIA'
    }
};

//funcion que se ejecuta iniciando
function init() {

    //Cargamos Alertas al iniciar aplicacion
    alertas();

    //Mantiene actulizado el mapa con nuevos Markers
    setInterval("VerificarCambios()", 10000);

}

function alertas() {
    $.post("../ajax/monitoreogse.php?op=listarAlertagse", function (data, status) {
        data = JSON.parse(data);
        console.log(data);
        for (var i = 0; i < data.aaData.length; i++) {
            //Configuracion del marcador
            console.log(detalle);
            console.log(data.aaData[i][4]);
            var content = '<div id="iw-container">' +
                    '<div class="iw-title">' + detalle[data.aaData[i][4]].info + '</div>' +
                    '<div class="iw-content">' +
                    '<div class="iw-subTitle">Equipo</div>' +
                    '<p><b>' + data.aaData[i][1] + '</b><br>' +
                    ' ' + data.aaData[i][3] + ' - ' + data.aaData[i][4] + ' <br></p>' +
                    '<div class="iw-subTitle">Ubicacion</div>' +
                    '<p>Edificio: ' + data.aaData[i][7] + '<br>' +
                    'Direccion: ' + data.aaData[i][8] + '<br>' +
                    '' + data.aaData[i][9] + '<br></p>' +
                    '<div class="iw-subTitle">Contacto</div>' +
                    '<p>' + data.aaData[i][11] + '<br>' +
                    'Telefono: ' + data.aaData[i][13] + ' <br> Correo Electronico: ' + data.aaData[i][12] + '</p>' +
                    '</div>' +
                    '<div class="iw-bottom-gradient"></div>' +
                    '</div>';
            var MarLatLng = {lat: parseFloat(data.aaData[i][5]), lng: parseFloat(data.aaData[i][6])};
            var markerOptions = {
                position: MarLatLng,
                icon: detalle[data.aaData[i][4]].icon,
                map: map,
                title: "Codigo: " + data.aaData[i][1]
            };
            var marker = new google.maps.Marker(markerOptions);
            VentanaInfo(marker, content);
            //Almaceno marca en el array
            var newArray = new Array(data.aaData[i][0], marker);
            markers.push(newArray);
        }
        console.log("Marcas actuales");
        console.log(markers);
        refdata = data;
    });
}


function LimpiarMarker(pos) {
    markers[pos][1].setMap(null);
}

function LimpiarAnimacion(pos) {
    markers[pos][1].setAnimation(null);
}

function BorrarMarkers(eliminar) {
    console.log("Marcas antes de eliminar");
    console.log(markers);
    
    console.log("Alerta que debe eliminar");
    console.log(eliminar);

    for (var i = 0; i < eliminar.length; i++) {
        for (var o = 0; o < markers.length; o++) {
            if (eliminar[i][0] == markers[o][0]) {
                console.log("Elimina marca con la id: " + markers[o][0]);
                LimpiarMarker(o);
                markers.splice(o, 1);
            }
        }
    }
    
    console.log("Marcas despues de eliminar");
    console.log(markers);
    
}


function AnimacionMarkers(animados) {
    console.log("Original");
    console.log(markers);
    console.log("Elimina la animacion del ascensor");
    console.log(animados);
    
    for (var i = 0; i < animados.length; i++) {
        for (var o = 0; o < markers.length; o++) {
            if (animados[i][0] == markers[o][0]) {
                console.log("Elimina animacion del ascensor: " + markers[o][0]);
                LimpiarAnimacion(o);
            }
        }
    }
}

function AgregarMarkers(nuevos) {
    
    console.log("Marcas antes de agregar");
    console.log(markers);
    
    console.log("Alerta que debe agregar");
    console.log(nuevos);
    
    for (var i = 0; i < nuevos.length; i++) {
        //Configuracion del marcador
        var content = '<div id="iw-container">' +
                '<div class="iw-title">' + detalle[nuevos[i][4]].info + '</div>' +
                '<div class="iw-content">' +
                '<div class="iw-subTitle">Equipo</div>' +
                '<p><b>' + nuevos[i][1] + '</b><br>' +
                ' ' + nuevos[i][3] + ' - ' + nuevos[i][4] + ' <br></p>' +
                '<div class="iw-subTitle">Ubicacion</div>' +
                '<p>Edificio: ' + nuevos[i][7] + '<br>' +
                'Direccion: ' + nuevos[i][8] + '<br>' +
                '' + nuevos[i][9] + '<br></p>' +
                '<div class="iw-subTitle">Contacto</div>' +
                '<p>' + nuevos[i][11] + '<br>' +
                'Telefono: ' + nuevos[i][13] + ' <br> Correo Electronico: ' + nuevos[i][12] + '</p>' +
                '</div>' +
                '<div class="iw-bottom-gradient"></div>' +
                '</div>';
        var MarLatLng = {lat: parseFloat(nuevos[i][5]), lng: parseFloat(nuevos[i][6])};
        var markerOptions = {
            position: MarLatLng,
            icon: detalle[nuevos[i][4]].icon,
            animation: google.maps.Animation.BOUNCE,
            map: map,
            title: "Codigo: " + nuevos[i][1]
        };

        var marker = new google.maps.Marker(markerOptions);
        VentanaInfo(marker, content);
        //Almaceno marca en el array
        var newArray = new Array(nuevos[i][0], marker);
        markers.push(newArray);
    }
    console.log("Marcas despues de agregar");
    console.log(markers);
}

//Verifica animaciones
function VerificarAnimaciones() {
    if (refnuevos.length > 0) {
        AnimacionMarkers(refnuevos);
    }
    refnuevos = [];
}


//Funcion que verifica nuevos cambios de estado de Ascensores
function VerificarCambios() {
    //Actualiza animacion si entro una alerta nueva en la verificacion anterior
    VerificarAnimaciones();
    console.log("VERIFICANDO CAMBIOS");
    var nuevos = [];
    var eliminados = [];
    $.post("../ajax/monitoreogse.php?op=listarAlertagse", function (data, status) {
        data = JSON.parse(data);
        //Recorrido de arreglos para buscar nuevas alertas para ingresar
        var pos = 0;
        $.each(data.aaData, function (index, value) {
            $.each(refdata.aaData, function (i, val) {
                if (value[0] == val[0]) {
                    if (value[4] != val[4]) {
                        nuevos.push(data.aaData[pos]);
                    } else {
                        return false;
                    }
                }
            });
            pos++;
        });

        //Recorrido de arreglos que permite verificar alertas que deben ser eliminadas
        var pos = 0;
        $.each(refdata.aaData, function (index, value) {
            $.each(data.aaData, function (i, val) {
                if (value[0] == val[0]) {
                    if (value[4] != val[4]) {
                        eliminados.push(refdata.aaData[pos]);
                    } else {
                        return false;
                    }
                }
            });
            pos++;
        });

        if (eliminados.length > 0) {
            console.log("Ingresaron alertas para eliminar");
            BorrarMarkers(eliminados);
        } else {
            console.log("No ingresaron alertas para eliminar");
        }

        if (nuevos.length > 0) {
            console.log("Ingresaron marcas nuevas");
            AgregarMarkers(nuevos);
            refnuevos = nuevos;
        } else {
            console.log("No ingresaron alertas nuevas");
        }

        console.log("Se establece arreglo de referencia");
        refdata = data;
    });
}


//Funcion que establece informacion de la ventana de la marca
function VentanaInfo(marker, contenido) {
    var infowindow = new google.maps.InfoWindow({
        content: contenido
    });
    marker.addListener('click', function () {
        infowindow.open(marker.get('map'), marker);
    });
}


init();