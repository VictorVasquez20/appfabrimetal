var tabla;
var refdata, refdataant;
var ctx, mybarChart;
var ctxant, mybarChartant;


//funcion que se ejecuta iniciando
function init() {
    $.post("../ajax/estado.php?op=selectano", function (r) {
        $("#ano").html(r);
        $("#ano").selectpicker('refresh');
    });
    
    //CargarDatos();
    //CargarGraficos();
    CargarGraficosXregion();
    cargartiles();
    //CargarGraficosAnt();
    //ListarAlerta(false, 0);
    //setInterval("Actualizar()", 10000);
}

function ListarAlerta(flag, alerta) {

    if (flag) {
        console.log("Carga Listado");
        $("#Charts").hide();
        GenerarListado(alerta);
        $("#Listado").show();
    } else {
        console.log("Carga Graficos");
        $("#Charts").show();
        ActializarGraficos();
        $("#Listado").hide();
    }

}

function CargarGraficos() {

    $.post("../ajax/estado.php?op=DatosGrafico", function (data, status) {
        data = JSON.parse(data);
        refdata = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

        for (var i = 0; i < data.aaData.length; i++) {
            refdata[data.aaData[i][0] - 1] = data.aaData[i][1];
        }

        ctx = document.getElementById("mybarChart");
        ctx.height = 125;
        mybarChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                datasets: [{
                        label: 'Contratos Año Actual',
                        backgroundColor: "#26B99A",
                        data: refdata
                    }]
            },

            options: {
                scales: {
                    yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                }
            }
        });
    });

}

function CargarGraficosAnt() {

    $.post("../ajax/estado.php?op=DatosGraficoAnt", function (data, status) {
        data = JSON.parse(data);
        refdataant = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

        for (var i = 0; i < data.aaData.length; i++) {
            refdataant[data.aaData[i][0] - 1] = data.aaData[i][1];
        }

        ctxant = document.getElementById("mybarChartant");
        ctxant.height = 125;
        mybarChartant = new Chart(ctxant, {
            type: 'bar',
            data: {
                labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                datasets: [{
                        label: 'Contratos Año Pasado',
                        backgroundColor: "#26B99A",
                        data: refdataant
                    }]
            },

            options: {
                scales: {
                    yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                }
            }
        });
    });

}

function ActializarGraficos() {

    $.post("../ajax/estado.php?op=DatosGrafico", function (data, status) {
        data = JSON.parse(data);

        var updata = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        for (var i = 0; i < data.aaData.length; i++) {
            updata[data.aaData[i][0] - 1] = data.aaData[i][1];
        }

        for (var i = 0; i < updata.length; i++) {
            if (refdata[i] != updata[i]) {
                refdata[i] = updata[i];
                console.log("Consiguio diferencia");
                console.log(refdata);
                mybarChart.data.datasets[0]['data']=refdata;
                mybarChart.update();
            }

        }
    });
}

function ActializarGraficosAnt() {

    $.post("../ajax/estado.php?op=DatosGraficoAnt", function (data, status) {
        data = JSON.parse(data);

        var updataant = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        for (var i = 0; i < data.aaData.length; i++) {
            updataant[data.aaData[i][0] - 1] = data.aaData[i][1];
        }

        for (var i = 0; i < updataant.length; i++) {
            if (refdataant[i] != updataant[i]) {
                refdataant[i] = updataant[i];
                console.log("Consiguio diferencia Año Anterior");
                console.log(refdataant);
                mybarChartant.data.datasets[0]['data']=refdataant;
                mybarChartant.update();
            }

        }
    });
}

function CargarDatos() {

    $.post("../ajax/estado.php?op=ContarDatos", function (data, status) {
        data = JSON.parse(data);
        //Actualizamos valores
        $("#num_contrato").html(data.contratos.contratos);
        $("#num_clientes").html(data.clientes.clientes);
        $("#num_edificios").html(data.edificios.edificios);
        $("#num_ascensores").html(data.ascensores.ascensores);


    });
}

function Actualizar() {
    CargarDatos();
    ActializarGraficos();
    ActializarGraficosAnt();
}


/**
 * NUEVOS
 */
function CargarGraficosXregion() {
    var f= new Date();
    var ano = 0;
    if($("#ano").val() == null){
        ano = f.getFullYear(); 
    }else{
        ano = $("#ano").val() 
    } 
    var mes = $("#mes").val();
    
    $.post("../ajax/estado.php?op=DatosGraficoxregion", {'ano': ano, 'mes': mes}, function (data, status) {
        data = JSON.parse(data);
        refdata = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        refdataout = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

        for (var i = 0; i < data.aaData.length; i++) {
            refdata[data.aaData[i][0] - 1] = data.aaData[i][1];
            refdataout[data.aaData[i][0] - 1] = data.aaData[i][2];
        }

        ctx = document.getElementById("mybarChart");
        ctx.height = 125;
        mybarChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["Tarapacá", "Antofagasta", "Atacama", "Coquimbo", "Valparaiso", "Libertador Bdo. Ohiggins", "Maule", "Biobío", "La araucanía", "Los Lagos", "Aisén", "Magallanes", "Metropolitana", "Los Ríos", "Arica"],
                datasets: [{
                        label: 'Entrada Ascensores del año ' + ano,
                        backgroundColor: "#26B99A",
                        data: refdata
                    },
                    {
                        label: 'salida Ascensores del año ' + ano,
                        backgroundColor: "#455C73",
                        data: refdataout
                    }]
            },
            
            options: {
                scales: {
                    yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                }
            }
        });
    });

}

function ActializarGraficosXregion() {
    var f= new Date();
    var ano = 0;
    if($("#ano").val() == null || $("#ano").val() == 0){
        ano = f.getFullYear(); 
    }else{
        ano = $("#ano").val() 
    } 
    var mes = $("#mes").val();
    
    $.post("../ajax/estado.php?op=DatosGraficoxregion",{'ano': ano, 'mes': mes}, function (data, status) {
        data = JSON.parse(data);

        var updata = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        for (var i = 0; i < data.aaData.length; i++) {
            updata[data.aaData[i][0] - 1] = data.aaData[i][1];
        }

        for (var i = 0; i < updata.length; i++) {
            if (refdata[i] != updata[i]) {
                refdata[i] = updata[i];
                console.log("Consiguio diferencia");
                console.log(refdata);
                mybarChart.data.datasets[0]['data']=refdata;
                mybarChart.update();
            }

        }
    });
}

function cargartiles(){
    $.post("../ajax/estado.php?op=ContarDatos", function (data, status) {
        data = JSON.parse(data);
        //Actualizamos valores
        $("#num_contrato").html(data.contratos.contratos);
        $("#num_clientes").html(data.clientes.clientes);
        $("#num_edificios").html(data.edificios.edificios);
        $("#num_ascensores").html(data.ascensores.ascensores);


    });
}
init();