/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var tabla;
function init(){
    mostrarform(false);
    listar();
    
    $("#formulario").on("submit", function(e){
        guardaryeditar(e);
    });
}

function mostrarform(flag){
    limpiar();
    if(flag){
        $("#listadocategorias").hide();
        $("#formulariocategorias").show();
        $("#op_agregar").hide();
        $("#op_listar").show();
        $("#btnGuardar").prop("disabled", false);

    }else{
        $("#listadocategorias").show();
        $("#formulariocategorias").hide();
        $("#op_agregar").show();
        $("#op_listar").hide();
    }
}

function limpiar(){
    $("#idcategoria").val("");
    $("#nombre").val("");
    $("#descripcion").val("");
}

function cancelarform(){
    limpiar();
    mostrarform(false);
}

function listar(){
    tabla=$('#tblcategorias').dataTable({
            "aProcessing":true,
            "aServerSide": true,
            dom: 'Bfrtip',
            buttons:Botones,
            "language":Español,
            "ajax":{
                    url:'../ajax/categoria_productoAjax.php?op=listar',
                    type:"get",
                    dataType:"json",
                    error: function(e){
                            console.log(e.responseText);
                    }
            },
            "bDestroy": true,
            "iDisplayLength": 10, //Paginacion 10 items
            "order" : [[0 , "desc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}

function guardaryeditar(e){
	e.preventDefault();
	$("#btnGuardar").prop("disabled", true);
	var formData = new FormData($("#formulario")[0]);
        
	$.ajax({
            url:'../ajax/categoria_productoAjax.php?op=guardaryeditar',
            type:"POST",
            data:formData,
            contentType: false,
            processData:false,
            
            success: function(datos){
                    bootbox.alert(datos);
                    mostrarform(false);
                    tabla.ajax.reload();
            },
            error: function(datos){
                    bootbox.alert(datos);
                    mostrarform(false);
                    tabla.ajax.reload();
            }
	});
        $("#btnGuardar").prop("disabled", false);
	limpiar();
}

function mostrar(idcategoria){
	$.post("../ajax/categoria_productoAjax.php?op=mostrar",{idcategoria:idcategoria}, function(data,status){
		data = JSON.parse(data);
		mostrarform(true);
	
		$("#idcategoria").val(data.idcategoria);
		$("#nombre").val(data.nombre);
                $("#descripcion").val(data.descripcion);
                
                if(data.vigencia == 1){
                    $("#vigencia").attr("checked", "checked");
                }else{
                    $("#vigencia").attr("checked", "");
                }
	});
}


init();

