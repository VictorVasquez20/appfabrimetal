var tabla_dirk = $('#tabla_dirk').DataTable();
var tabla_arduinos= $('#tabla_arduinos').DataTable();
var tabla_ascensores= $('#tabla_ascensores').DataTable();
var arrayFuncionesArduino = []; 

var equivalenciaIpMacArduinos = new Array();
equivalenciaIpMacArduinos["192, 168, 100, 150"]="0xAE, 0x52, 0xEC, 0xBD, 0x13, 0x9A";
equivalenciaIpMacArduinos["192, 168, 100, 151"]="0xE8, 0xE2, 0xC9, 0x22, 0x9E, 0xBA";
equivalenciaIpMacArduinos["192, 168, 100, 152"]="0x12, 0xBA, 0x0B, 0x9C, 0xAB, 0x84";
equivalenciaIpMacArduinos["192, 168, 100, 153"]="0x7E, 0xCD, 0xF5, 0x8A, 0xF2, 0x97";
equivalenciaIpMacArduinos["192, 168, 100, 154"]="0xF0, 0xFD, 0xAB, 0x1C, 0XA3, 0x94";
equivalenciaIpMacArduinos["192, 168, 100, 155"]="0xC0, 0x60, 0x7B, 0xAC, 0x7B, 0xA7";
equivalenciaIpMacArduinos["192, 168, 100, 156"]="0x04, 0x69, 0xE5, 0xB2, 0X8E, 0xDA";
equivalenciaIpMacArduinos["192, 168, 100, 157"]="0x2A, 0x5C, 0xEF, 0xC4, 0xC3, 0x43";

function init(){

    //lista dirk.
    listar_dirk();
    
    //lista select de dirk.
    selectDirk();
              
    //lista select edificio
    $.post("../ajax/edificio.php?op=selectedificio", function (r) {
        
        $("#idedificio").html(r);
        $("#idedificio").selectpicker('refresh');
        
        $("#edificio_dirk").html(r);
        $("#edificio_dirk").selectpicker('refresh');
  
    });
    
    mostrarform(false); //mostrar lista dirk.
    
    //eventos asociados a los formularios.
    $("#formModulo").on("submit", function(e){
	guardarFormModulo(e);
    });
    
    $("#formArduino").on("submit", function(e){
	guardarFormArduinos(e);
    });
 
    $("#formAscensores").on("submit", function(e){
	guardarFormAscensores(e);
    });
    
    //eventos al abrir los modales.
    $('#modalFormModulos').on('show.bs.modal', function (event) {
        limpiarFormModulos();     
    });
    
    $('#modalFormArduinos').on('shown.bs.modal', function (e) {
    
    limpiarFormArduinos(); 

    
    var totalFilastablaArduinos = tabla_arduinos.page.info().recordsDisplay;
   
    if(totalFilastablaArduinos >= 2 ){

       $('#modalFormArduinos').modal('hide');
         new PNotify({
             title: 'Error!',
             text: 'Solo se Permite Registrar 2 Arduinos por este Dirk',
             type: 'error',
             styling: 'bootstrap3'
         });  
    };
  
 });
 
    $('#modalFormAscensores').on('shown.bs.modal', function (e) {
    
    limpiarFormAscensores(); 
    
    //select de ascensores
    $.post("../ajax/ascensor.php?op=selectAscensoresSinDirk",
        {"idedificio":$("#idedificio").val() }, function (r) {        
            $("#idascensor").html(r);
            $("#idascensor").selectpicker('refresh');
    });
    
    //validar si se asignaron dirk a todos los ascensores.  
    $.post("../ajax/ascensor.php?op=validarAscensoresSinDirk",{"idedificio": $("#idedificio").val() }, 
        function(data){                   
            if(parseInt(data.estatus)===0)  {

              $('#modalFormAscensores').modal('hide');

                new PNotify({
                title: 'Error!',
                text: data.mensaje,
                type: 'error',
                styling: 'bootstrap3'
                });  
            }                        
        },"json");
 
 });
      
    $("#idedificio").on("change", function(e){
        
        bootbox.confirm("Esta seguro que quiere cambiar el edificio del dirk?", function(result){
            
            if(result){
                
                $.post("../ajax/ascensor.php?op=inhabilitarAscensoresDirk",
                        {"iddirk":$("#iddirk").val()},
                         function (data) {
                             
                               if(parseInt(data.estatus)=== 1){ 
                                  new PNotify({
                                   title: 'Correcto!',
                                   text: data.mensaje,
                                   type: 'success',
                                   styling: 'bootstrap3'
                                  });                                                        
                                }else if(parseInt(data.estatus) === 0){
                                  new PNotify({
                                   title: 'Error!',
                                   text: data.mensaje,
                                   type: 'error',
                                   styling: 'bootstrap3'
                               });
                             }
                             
                        listar_ascensores($("#iddirk").val(),$("#idedificio").val()); 
                    
                },"json");
                    
                $.post("../ajax/arduino.php?op=actualizarEdificioArduino",
                    {"iddirk":$("#iddirk").val(),"idedificio": $("#idedificio").val()},
                     function (data) { 
                        if(parseInt(data.estatus)=== 1){
                                  new PNotify({
                                   title: 'Correcto!',
                                   text: data.mensaje,
                                   type: 'success',
                                   styling: 'bootstrap3'
                               });
                        }else if(parseInt(data.estatus) === 0){
                                  new PNotify({
                                   title: 'Error!',
                                   text: data.mensaje,
                                   type: 'error',
                                   styling: 'bootstrap3'
                               });
                        }
                        
                    listar_arduinos($("#iddirk").val(),$("#idedificio").val());

                },"json");
                    
                $.post("../ajax/dirk.php?op=actualizarDirk", 
                    {"idedificio": $("#idedificio").val(),"iddirk":$("#iddirk").val() }, 
                    function(data){                                 
                        if(parseInt(data.estatus)=== 1){
                                  new PNotify({
                                   title: 'Correcto!',
                                   text: data.mensaje,
                                   type: 'success',
                                   styling: 'bootstrap3'
                               });
                               
                        tabla_dirk.ajax.reload(); // recarga la tabla de modulos.
                        
                        }else if(parseInt(data.estatus) === 0){
                                  new PNotify({
                                   title: 'Error!',
                                   text: data.mensaje,
                                   type: 'error',
                                   styling: 'bootstrap3'
                               });
                             }   
                    },"json");

            }
	});
    });
       
    $("#ip_arduino").on("change", function(e){
        $("#mac_text").html(equivalenciaIpMacArduinos[$(this).val()]);
        $("#mac_arduino").val(equivalenciaIpMacArduinos[$(this).val()]);
    });
    
//mascara a los campos de formulario.

 $("#codigo_arduino").inputmask("\\AR999999",{ "onincomplete": function(){ alert('Debe completar el campo Codigo'); } });

 $("#codigo_dirk").inputmask("\\DIRK999999",{ "onincomplete": function(){ alert('Debe completar el campo Codigo'); } });
 
 $("#latitud_ascensor").inputmask("-99.999999",{ "onincomplete": function(){ alert('Debe completar el campo Latitud'); } });
 
 $("#longitud_ascensor").inputmask("-99.999999",{ "onincomplete": function(){ alert('Debe completar el campo Longitud'); } });
 
}//fin init.

function limpiar(){
   $("#iddirk").val("");
   $("#dirk_text").html();	
   $("#idedificio").val("");
   $("#idedificio").selectpicker('refresh');
   tabla_arduinos.clear().draw();
   tabla_ascensores.clear().draw();

}

function limpiarFormModulos(){
    $("#codigo_dirk").val("");
    $("#ubicacion_dirk").val("");
    $("#edificio_dirk").val("");
    $("#edificio_dirk").selectpicker('refresh');
}

function limpiarFormArduinos(){
    $("#codigo_arduino").val("");
    $("#ip_arduino").val("");
    $("#ip_arduino").selectpicker('refresh');
    $("#mac_arduino").val("");
    $("#mac_text").html("");
    $("#funcion_arduino").val("");
    $("#funcion_arduino").selectpicker('refresh');
   
}

function limpiarFormAscensores(){
    $("#idascensor").val("");
    $("#idascensor").selectpicker('refresh');
    $("#latitud_ascensor").val("");
    $("#longitud_ascensor").val("");    
}

function mostrarform(flag){

	limpiar();
        
	if(flag){
		$("#listado_dirk").hide();
		$("#div_formulario").show();
		$("#op_agregar").hide();
		$("#op_listar").show();
                $("#titulo_pantalla").html("MODIFICAR DIRK");
	}else{
		$("#listado_dirk").show();
		$("#div_formulario").hide();
		$("#op_agregar").show();
		$("#op_listar").hide();
                $("#titulo_pantalla").html("LISTA DIRK");
	}

}

function listar_dirk(){
    
	tabla_dirk=$('#tabla_dirk').dataTable({
            
		"aProcessing":true,
                
		"aServerSide": true,
                
		dom: 'Bfrtip',
                
		buttons:[
			'copyHtml5',
			'print',
			'csvHtml5',		
		],
                
		"ajax":{
			url:'../ajax/dirk.php?op=listar',
			type:"get",
			dataType:"json",
			error: function(e){
				console.log(e.responseText);
			}
		},
                
		"bDestroy": true,
                
		"iDisplayLength": 10, //Paginacion 10 items
                
		"order" : [[1 , "desc"]] 
                
	}).DataTable();
}

function selectDirk(){
        
    $.post("../ajax/dirk.php?op=selectDirk", function(r){
        $("#iddirk").html(r);
        $("#iddirk").selectpicker('refresh');
    });
    
}

function mostrar(iddirk){
                    
    $.post("../ajax/dirk.php?op=mostrar",{"iddirk":iddirk}, function(data,status){
            data = JSON.parse(data);
            
            mostrarform(true);	
            
            $("#iddirk").val(iddirk);
            $("#dirk_text").html(data.codigo+" - "+data.ubicacion);
            $("#idedificio").val(data.idedificio);
            $("#idedificio").selectpicker('refresh'); 

            listar_arduinos(iddirk,data.idedificio);
            
            listar_ascensores(iddirk,data.idedificio);

    });
        

     
}

function listar_arduinos(iddirk,idedificio){
    
	tabla_arduinos=$('#tabla_arduinos').dataTable({
            
		"aProcessing":true,
                
		"aServerSide": true,
                
		dom: 'Bfrtip',
                
		buttons:[
			'copyHtml5',
			'print',
			'csvHtml5',		
		],
                
		"ajax":{
			"url":'../ajax/arduino.php?op=listarPorDirkEdificio',
			"type":"POST",
			"dataType":"json",
                        "data":{"iddirk":iddirk, "idedificio":idedificio},
			"error": function(e){
				console.log(e.responseText);
			},
                        "dataSrc": function ( json ) {
                            for ( var i=0, ien=json.aaData.length ; i<ien ; i++ ) {
                            //modificar la data que retorna el servicio
                            }
                            return json.aaData;
                          }             
		},
                
		"bDestroy": true,
                
		"iDisplayLength": 10, //Paginacion 10 items
                
		"order" : [[1 , "desc"]] 
                
	}).DataTable();
}

function listar_ascensores(iddirk,idedificio){
    
	tabla_ascensores=$('#tabla_ascensores').dataTable({
            
		"aProcessing":true,
                
		"aServerSide": true,
                
		dom: 'Bfrtip',
                
		buttons:[
			'copyHtml5',
			'print',
			'csvHtml5',		
		],
                
		"ajax":{
			url:'../ajax/ascensor.php?op=listadoAscensores',
			type:"POST",
			dataType:"json",
                        data:{"iddirk":iddirk, "idedificio":idedificio},
			error: function(e){
				console.log(e.responseText);
			}
		},
                
		"bDestroy": true,
                
		"iDisplayLength": 10, 
                
		"order" : [[1 , "desc"]] 
                
	}).DataTable();
}

 

function guardarFormModulo(e) {

    e.preventDefault();
    
    var formData = new FormData();
    
    formData.append("codigo",    $("#codigo_dirk").val());
    formData.append("ubicacion", $("#ubicacion_dirk").val());
    formData.append("idedificio",$("#edificio_dirk").val());
    
        $.ajax({            
            data: formData,
            type: "POST",
            dataType: "json",
            async: false,
            cache: false,
            timeout: 30000,
            contentType: false,
            processData: false,
            url: "../ajax/dirk.php?op=guardar",
            beforeSend: function () {
                $("#btnGuardarModulo").prop("disabled", true);
                $('.modal-body').css('opacity', '.5');
            }
        }).done(function (data, textStatus, jqXHR) {
                
                if(parseInt(data.estatus)=== 1){
                      new PNotify({
                       title: 'Correcto!',
                       text: data.mensaje,
                       type: 'success',
                       styling: 'bootstrap3'
                   });
                   
                selectDirk();
                tabla_dirk.ajax.reload();
                 
                 }else if(parseInt(data.estatus) === 0){
                     
                      new PNotify({
                       title: 'Error!',
                       text: data.mensaje,
                       type: 'error',
                       styling: 'bootstrap3'
                   });
                   
                 } 
                                               
                $("#btnGuardarModulo").prop("disabled", false);
                $('.modal-body').css('opacity', '');
                $('#modalFormModulos').modal('toggle');
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                if (console && console.log) {
                    console.log("La solicitud a fallado: " + textStatus);
                }

            });

}

function guardarFormArduinos(e) {

    e.preventDefault();
    
    var formData = new FormData();
    
    formData.append("codigo",    $("#codigo_arduino").val());
    formData.append("ip",        $("#ip_arduino").val());
    formData.append("mac",       $("#mac_arduino").val());
    formData.append("funcion",   $("#funcion_arduino").val());
    formData.append("iddirk",    $("#iddirk").val());
    formData.append("idedificio",$("#idedificio").val());
    
        $.ajax({            
            data: formData,
            type: "POST",
            dataType: "json",
            async: false,
            cache: false,
            timeout: 30000,
            contentType: false,
            processData: false,
            url: "../ajax/arduino.php?op=guardar",
            beforeSend: function () {
                $("#btnGuardarArduino").prop("disabled", true);
                $('.modal-body').css('opacity', '.5');
            }
            })
            .done(function (data, textStatus, jqXHR) {
                
                if(parseInt(data.estatus)=== 1){
                      new PNotify({
                       title: 'Correcto!',
                       text: data.mensaje,
                       type: 'success',
                       styling: 'bootstrap3'
                   });
                   
                  tabla_arduinos.ajax.reload(); 
                  
                 }else if(parseInt(data.estatus) === 0){
                     
                      new PNotify({
                       title: 'Error!',
                       text: data.mensaje,
                       type: 'error',
                       styling: 'bootstrap3'
                   });
                   
                 } 
                                  
                $("#btnGuardarArduino").prop("disabled", false);
                $('.modal-body').css('opacity', '');
                $('#modalFormArduinos').modal('toggle');
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                if (console && console.log) {
                    console.log("La solicitud a fallado: " + textStatus);
                }

            });

}

function guardarFormAscensores(e) {

    e.preventDefault();
    
    var formData = new FormData();
    
    formData.append("idascensor",$("#idascensor").val());
    formData.append("iddirk",    $("#iddirk").val());
    formData.append("lat",       $("#latitud_ascensor").val());
    formData.append("lon",       $("#longitud_ascensor").val());

        $.ajax({            
            data: formData,
            type: "POST",
            dataType: "json",
            async: false,
            cache: false,
            timeout: 30000,
            contentType: false,
            processData: false,
            url: "../ajax/ascensor.php?op=actualizar_ascensor",
            beforeSend: function () {
                $("#btnGuardarAscensores").prop("disabled", true);
                $('.modal-body').css('opacity', '.5');
            }
        })
            .done(function (data, textStatus, jqXHR) {
                
                   if(parseInt(data.estatus)=== 1){
                      new PNotify({
                       title: 'Correcto!',
                       text: data.mensaje,
                       type: 'success',
                       styling: 'bootstrap3'
                   });
                                 
                tabla_ascensores.ajax.reload();
 
                 }else if(parseInt(data.estatus) === 0){
                     
                      new PNotify({
                       title: 'Error!',
                       text: data.mensaje,
                       type: 'error',
                       styling: 'bootstrap3'
                   });
                   
                 } 
   
                $("#btnGuardarAscensores").prop("disabled", false);
                $('.modal-body').css('opacity', '');
                $('#modalFormAscensores').modal('toggle');
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                if (console && console.log) {
                    console.log("La solicitud a fallado: " + textStatus);
                }

            });

}

 function eliminarAscensorDirk(idascensor){

	bootbox.confirm("Esta seguro que quiere inhabilitar el Ascensor del Dirk?", function(result){            
            if(result){
                    $.post("../ajax/ascensor.php?op=inhabilitarAscensorDirk",{"idascensor":idascensor}, 
                    function(data){
                              
                              if(parseInt(data.estatus)=== 1){
                                  new PNotify({
                                   title: 'Correcto!',
                                   text: data.mensaje,
                                   type: 'success',
                                   styling: 'bootstrap3'
                               });
                               
                                 tabla_ascensores.ajax.reload();
                                 
                        }else if(parseInt(data.estatus) === 0){
                                  new PNotify({
                                   title: 'Error!',
                                   text: data.mensaje,
                                   type: 'error',
                                   styling: 'bootstrap3'
                               });
                        }
                          
                    },"json");
            }
	});
}

 function eliminarArduino(idarduino){

	bootbox.confirm("Esta seguro que quiere eliminar el arduino?", function(result){            
            if(result){
                    $.post("../ajax/arduino.php?op=eliminarArduino",{"idarduino":idarduino}, 
                    
                    function(data){
                              
                              if(parseInt(data.estatus)=== 1){
                                  new PNotify({
                                   title: 'Correcto!',
                                   text: data.mensaje,
                                   type: 'success',
                                   styling: 'bootstrap3'
                               });
                               
                                 tabla_arduinos.ajax.reload();
                                 
                        }else if(parseInt(data.estatus) === 0){
                                  new PNotify({
                                   title: 'Error!',
                                   text: data.mensaje,
                                   type: 'error',
                                   styling: 'bootstrap3'
                               });
                        }
                          
                    },"json");
            }
	});
}

 function ActualizarAscensorDirk(idascensor){
    
 }
 
init();