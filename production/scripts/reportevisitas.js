var tablaproyectos,tablavisitas,tablavisitasetapa;

function init(){
     
    $('#modalVisitas').on('show.bs.modal', function (event) { 
        
        var button = $(event.relatedTarget); 
        var idproyecto = button.data('idproyecto');
        var iduser =button.data('created_user');
        
        $.post("../ajax/proyecto.php?op=mostrarproyecto",{idproyecto:idproyecto}, function(resp,status){
		resp = JSON.parse(resp);
		$("#nombre_proyecto").html(resp.nombre);
                $("#codigo_proyecto").html(resp.codigo);
	});
        
        $.post("../ajax/usuario.php?op=mostar",{iduser:iduser}, function(data,status){
		data = JSON.parse(data);
		$("#nombre_tecnico").html(data.nombre+', '+data.apellido);
                var idrole = data.idrole;
                
                $.post("../ajax/role.php?op=mostar",{idrole:idrole}, function(response,status){
                    response = JSON.parse(response);
                    $("#cargo_tecnico").html(response.descripcion);
                });
	});
        
       visitasetapa(idproyecto,iduser);
  
    });
 
      $( "#reporte" ).change(function() {
         if(String($(this).val())=='listadoproyectos'){
             $('#listaproyectos').show();
             $('#visitaproyectos').hide();
         }
          if(String($(this).val())=='listadovisitas'){
             $('#visitaproyectos').show();
             $('#listaproyectos').hide();
         }
     });
     
     
    listaproyectos(1);

    visitaproyectos();

}

function listaproyectos(tipoproyecto) {
 
    tablaproyectos = $('#tbllistaproyectos').dataTable({
        
         initComplete: function () {
      
            this.api().columns([2,3,5]).every( function () {
                
                var column = this;
                var select = $('<select><option value="">TODOS</option></select>')
                    .appendTo( $(column.header()) )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
                
            } );
        },
        "responsive": true,
        "aProcessing": true,
        "aServerSide": true,
        "scrollX": false,
        dom: 'Bfrtip',
        orderCellsTop: true,
        buttons:[                 
                'copyHtml5',
		'print',
                'csvHtml5',
                'excel',
                'pdf'
		],
        "language": Español,
        "ajax": {
            url: '../ajax/reportevisitas.php?op=listaproyectos',
            type: "get",
            dataType: "json",
            data: {'tipoproyecto': tipoproyecto},
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 20, //Paginacion 10 items
    }).DataTable();
}

function visitaproyectos() {
 
    tablavisitas = $('#tblvisitaproyectos').dataTable({
        
        initComplete: function () {
            this.api().columns([2,3,4]).every( function () {
                var column = this;
                var select = $('<select><option value="">TODOS</option></select>')
                    .appendTo( $(column.header()) )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        },
        "responsive": true,
        "aProcessing": true,
        "aServerSide": true,
        "scrollX": false,
        dom: 'Bfrtip',
        orderCellsTop: true,
        buttons:[                 
                'copyHtml5',
		'print',
                'csvHtml5'
		],
        "language": Español,
        "ajax": {
            url: '../ajax/reportevisitas.php?op=visitasproyectos',
            type: "get",
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 20, //Paginacion 10 items,
    }).DataTable();
}

function visitasetapa(idproyecto,created_user){
  
      tablavisitasetapa = $('#tblvisitasetapa').dataTable({
        "responsive": true,
        "aProcessing": true,
        "aServerSide": true,
        "scrollX": false,
        dom: 'Bfrtip',
        orderCellsTop: true,
        "bPaginate": false, //hide pagination
        "bFilter": false, //hide Search bar
        "bInfo": false, // hide showing entries
        buttons:[                 
               'copyHtml5',
		'print',
                'csvHtml5'
		],
        "language": Español,
        "ajax": {
            url: '../ajax/reportevisitas.php?op=visitasetapa',
            type: "get",
            dataType: "json",
            data: {"idproyecto": idproyecto, "created_user" : created_user },
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 20, //Paginacion 10 items
    }).DataTable();
    
}

init();
