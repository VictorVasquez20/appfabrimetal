var tabla;

function init() {
    mostrarform(false);
    listar();
    //listaProductos();

    $.post("../ajax/bodegaAjax.php?op=selectbodega", function (r) {
        $("#bodega22").html(r);
        $("#bodega22").selectpicker('refresh');

        $("#bodega").html(r);
        $("#bodega").selectpicker('refresh');
        
        $("#bodegasselect").html(r);
        $("#bodegasselect").selectpicker('refresh');
    });
    $.post("../ajax/categoria_productoAjax.php?op=selectcategoria", function (r) {
        $("#categoria22").html(r);
        $("#categoria22").selectpicker('refresh');

        $("#categoria").html(r);
        $("#categoria").selectpicker('refresh');
    });

    $("#formulario").on("submit", function (e) {
        guardaryeditar(e);
    });
    
    $("#formbodega").on("submit", function (e) {
        e.preventDefault();
        
        var idproducto = $("#idproductover").val();
        var bodega = $("#bodegasselect").val();
        
        ver(idproducto, bodega);
    });
}


$("#codigo").on("focusout", function () {
    if ($("#codigo").val() != "") {
        $.post("../ajax/productoAjax.php?op=existe", {codigo: $("#codigo").val()}, function (data) {
            data = JSON.parse(data);
            if (data.existe > 0) {
                new PNotify({
                    title: 'OPS!',
                    text: 'Esta producto ya existe.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
                $("#btnGuardar").prop("disabled", true);
            } else {
                $("#btnGuardar").prop("disabled", false);
            }
        });
    }
});

$("#categoria22").change(function () {
    var cat = document.getElementById("categoria22");
    filtrar(cat.options[cat.selectedIndex].text);
});

$("#bodega22").change(function () {
    var bod = document.getElementById("bodega22");
    filtrar(bod.options[bod.selectedIndex].text);
});

$("#todos").click(function () {
    $("#categoria22").val(0);
    $("#categoria22").selectpicker('refresh');
    $("#bodega22").val(0);
    $("#bodega22").selectpicker('refresh');
});

function mostrarform(flag) {
    limpiar();
    if (flag) {
        $("#listadoproductos").hide();
        $("#formularioproductos").show();
        $("#op_agregar").hide();
        $("#op_listar").show();
        $("#btnGuardar").prop("disabled", false);
        $("#filtros").hide();

    } else {
        $("#listadoproductos").show();
        $("#formularioproductos").hide();
        $("#op_agregar").show();
        $("#op_listar").hide();
        $("#filtros").show();
    }
}

function limpiar() {
    $("#idbodega").val("");
    $("#nombre").val("");
    $("#codigo").val("");
    $("#precio").val(0);
    $("#codigobarras").val("");
    $("#descripcion").val("");
    $("#stockcritico").val(0);
    $("#categoria").val("");
    $("#categoria").selectpicker('refresh');

    if (document.getElementById("vigencia").checked) {
        $("#venta_contrato").attr('checked', false);
    }


}

function cancelarform() {
    limpiar();
    mostrarform(false);
}

function listar() {
    tabla = $('#tblproductos').dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Bfrtip',
        buttons: Botones,
        "language": Español,
        "ajax": {
            url: '../ajax/productoAjax.php?op=listar',
            type: "get",
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 10, //Paginacion 10 items
        "order": [[0, "desc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}

function stock(idproducto){
    $("#stock-modal").click();
    var html = "";
    var col = 6;
    $.get("../ajax/stockAjax.php?op=stock", {idproducto: idproducto}, function (data) {
        data = JSON.parse(data);
        cantidad = 0;
        if(data.length >= 3){col = 4;}else if(data.length == 1){col = 12;}
        for(i = 0; i < data.length; i++){
            if(i == 0){
                $("#pdt").empty();$("#cant").empty();
                $("#pdt").html(data[i]['0']);
                
            }
            html+=  '<div class="animated flipInY col-lg-' + col + ' col-md-' + col + ' col-sm-' + col + ' col-xs-12">'+
                    '    <div class="tile-stats">'+
                    '         <div class="icon"><i class="fa fa-dropbox"></i></div>'+
                    '         <div class="count">' + data[i]['2'] + '</div>'+
                    '         <h3>' + data[i]['3'] + '</h3>'+
                    '     </div>'+
                    ' </div>';
            
            cantidad += parseFloat(data[i]['2']);
            
        }
        $("#cant").html(cantidad);
        
        $("#tiles").empty();
        $("#tiles").append(html);
    });
}

function guardaryeditar(e) {
    e.preventDefault();
    $("#btnGuardar").prop("disabled", true);
    var formData = new FormData($("#formulario")[0]);

    $.ajax({
        url: '../ajax/productoAjax.php?op=guardaryeditar',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,

        success: function (datos) {
            if (datos != '0') {
                new PNotify({
                    title: 'Correcto!',
                    text: 'Producto guardado con exito.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
            } else {
                $("#btnGuardar").prop("disabled", true);
                new PNotify({
                    title: 'Error!',
                    text: 'Producto no guardado.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
            mostrarform(false);
            tabla.ajax.reload();
        },
        error: function (datos) {
            bootbox.alert(datos);
            mostrarform(false);
            tabla.ajax.reload();
        }
    });
    $("#btnGuardar").prop("disabled", false);
    limpiar();
}

function mostrar(idproducto) {
    $.post("../ajax/productoAjax.php?op=mostrar", {idproducto: idproducto}, function (data, status) {
        data = JSON.parse(data);
        mostrarform(true);

        $("#idproducto").val(data.idproducto);

        $.post("../ajax/bodegaAjax.php?op=selectbodega", function (r) {
            $("#bodega").html(r);
            //$("#bodega").val(data.bodega);
            $("#bodega").selectpicker('refresh');
        });

        $.post("../ajax/categoria_productoAjax.php?op=selectcategoria", function (r) {
            $("#categoria").html(r);
            $("#categoria").val(data.categoria);
            $("#categoria").selectpicker('refresh');
        });

        $("#stockcritico").val(data.stockcritico);
        $("#precio").val(data.precio);
        $("#nombre").val(data.nombre);
        $("#descripcion").val(data.descripcion);
        $("#codigo").val(data.codigo);
        $("#codigo").attr('readOnly', true);
        $("#codigobarras").val(data.codigobarras);

        if (data.vigencia == 1) {
            $("#vigencia").attr("checked", "checked");
        } else {
            $("#vigencia").attr("checked", "");
        }
    });
}

function listaProductos() {
    $.post("../ajax/productoAjax.php?op=listaVigentes", function (data, status) {
        data = JSON.parse(data);

        var html = "";

        $("#productos").empty();

        for (i = 0; i < data.length; i++) {

            html += '<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">'
                    + ' <a href="#" onclick="mostrar(' + data[i]["idproducto"] + ')">'
                    + '   <div class="tile-stats">'
                    + '       <div class="icon2"><i class="fa fa-archive"></i></div>'
                    + '       <div class="count">' + data[i]["precio"] + '</div>'
                    + '       <h3 style="color:black;">' + data[i]["nombre"] + '</h3>'
                    + '       <p>' + data[i]["codigo"] + '</p>'
                    + '   </div>'
                    + ' </a>'
                    + '</div>';

        }

        $("#productos").append(html);

    });
}

function filtrar(filtro) {
    $.fn.dataTable.ext.search.pop();

    if (filtro != "") {
        var allowFilter = ['tblproductos'];
        $.fn.dataTable.ext.search.push(
                function (settings, data, index, rowData, counter) {
                    if ($.inArray(settings.nTable.getAttribute('id'), allowFilter) == -1) {
                        return true;
                    }
                    var categoria = data[3];
                    var bodega = data[4];
                    if (categoria == filtro) {
                        return true;
                    }
                    if (bodega == filtro) {
                        return true;
                    }
                    return false;
                }
        );
    }
    tabla.draw();
}

//******** MANEJO DE STOCK

function modalbodega(idproducto){
    $("#bodega-modal").click();
    $("#idproductover").val(idproducto);
}

function ver(idproducto, bodega) {
    $(".close").click();
   
    
    $.post("../ajax/productoAjax.php?op=ver", {idproducto: idproducto, bodega: bodega}, function (data, status) {
        data = JSON.parse(data);
        verProducto(true);

        $.post("../ajax/centrocosto.php?op=selectcentro", {}, function (r) {
            $("#ccosto").html(r);
            $("#ccosto").selectpicker('refresh');
        });
        
        $("#bodega").val(bodega);
        
        $("#idproducto2").html(data.idproducto);
        $("#idproducto").val(data.idproducto);
        $("#bodega2").html(data.bodega);
        $("#categoria2").html(data.categoriaprod);
        $("#precio2").html(data.precio);
        $("#nombre2").html(data.nombre);
        $("#codigo2").html(data.codigo);
        $("#stock2").html(data.stock);
        $("#stockcritico2").html(data.stockcritico);
        $("#cantidadbodega").html(data.cantidad);

        $("#alertadiv").hide();
        if ((data.stock - data.stockcritico) <= 2 && data.stockcritico > 0) {
            $("#alertadiv").show();
        }

        listaMovimientosStock(data.idproducto, bodega);
    });
}

function limpiarStock() {
    $("#cantidadin").val("");
    $("#referenciain").val("");
    $("#cantidad").val("");
    $("#referencia").val("");
    $("#ccosto").val("");
    $("#solicitante").val("");
    $("#Autoriza").val("");
}

function listaMovimientosStock(idproducto, bodega) {
    tabla2 = $('#tblstock').dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Bfrtip',
        buttons: Botones,
        "language": Español,
        "ajax": {
            url: '../ajax/stockAjax.php?op=listar&idp=' + idproducto + '&bodega=' + bodega,
            type: "get",
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 5, //Paginacion 10 items
        "order": [[0, "desc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}

function cancelarver() {
    verProducto(false);
    tabla.ajax.reload();
}

function verProducto(flag) {
    limpiar();
    if (flag) {
        $("#listadoproductos").hide();
        $("#verProducto").show();
        $("#op_agregar").hide();
        $("#op_listar").show();
        $("#btnGuardar").prop("disabled", false);
        $("#filtros").hide();

    } else {
        $("#listadoproductos").show();
        $("#verProducto").hide();
        $("#op_agregar").show();
        $("#op_listar").hide();
        $("#filtros").show();
    }
}

function guardarStock(tipomov) {
    var formData = new FormData();
    var cantidad, referencia;

    if (tipomov == 1) {
        if ($("#cantidadin").val() == "" || $("#cantidadin").val() == 0) {
            bootbox.alert("Debe ingresar una cantidad valida.");
            return false;
        }

        if ($("#referenciain").val() == "") {
            bootbox.alert("Debe ingresar una referencia.");
            return false;
        }

        cantidad = $("#cantidadin").val();
        referencia = $("#referenciain").val();

        formData.append("cantidad", cantidad);
        formData.append("referencia", referencia);
        formData.append("tipomovimiento", tipomov);

    } else {
        if ($("#cantidad").val() == "" || $("#cantidad").val() == 0) {
            bootbox.alert("Debe ingresar una cantidad valida.");
            return false;
        }

        if ($("#referencia").val() == "") {
            bootbox.alert("Debe ingresar una referencia.");
            return false;
        }
        if ($("ccosto").val() == 0) {
            bootbox.alert("Debe Seleccionar un centro de costo.");
            return false;

        }
        if ($("solicitante").val() == "") {
            bootbox.alert("Debe ingresar quien solicitante.");
            return false;
        }
        if ($("Autoriza").val() == "") {
            bootbox.alert("Debe ingresar quien autoriza.");
            return false;
        }

        cantidad = $("#cantidad").val();
        referencia = $("#referencia").val();

        formData.append("cantidad", cantidad);
        formData.append("referencia", referencia);
        formData.append("centrocosto", $("#ccosto").val());
        formData.append("solicitante", $("#solicitante").val());
        formData.append("Autoriza", $("#Autoriza").val());
        formData.append("tipomovimiento", tipomov);


    }

    var idprod = $("#idproducto").val();

    formData.append("idproducto", $("#idproducto").val());
    
    bodega = $("#bodega").val();
    formData.append("idbodega", bodega);


    $.ajax({
        url: '../ajax/stockAjax.php?op=guardaryeditar',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,

        success: function (datos) {
            if (datos != '0') {
                new PNotify({
                    title: 'Correcto!',
                    text: 'Producto guardado con exito.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
            } else {
                $("#btnGuardar").prop("disabled", true);
                new PNotify({
                    title: 'Error!',
                    text: 'Producto no guardado.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
            ver(idprod, bodega);
        },
        error: function (datos) {
            bootbox.alert(datos);
            //mostrarform(false);
            //tabla.ajax.reload();
            ver(idprod, bodega);
        }
    });
    $("#btnGuardar").prop("disabled", false);
    limpiarStock();

}

function formatofecha(texto) {
    if (texto != "") {
        if (texto != null) {
            var fecha = texto.split(" ");
            var info = fecha[0].split('-');
            return info[2] + '/' + info[1] + '/' + info[0] + ' ' + fecha[1];
        }
    }
}
init();
