var tabla;
var tablaasc;

function init() {
    mostrarform(false);

    $.post("../ajax/tcontrato.php?op=selecttcontrato", function (r) {
        $("#tcontrato").html(r);
        $("#tcontrato").selectpicker('refresh');
    });
    
    $.post("../ajax/contrato.php?op=selectcontrato", function (r) {
        $("#idcontrato").html(r);
        $("#idcontrato").selectpicker('refresh');
    });

    $.post("../ajax/tsegmento.php?op=selecttsegmento", function (r) {
        $("#idtsegmento").html(r);
        $("#idtsegmento").selectpicker('refresh');
    });
    
    $.post("../ajax/periocidad.php?op=selectperiocidad", function (r) {
        $("#idperiocidad").html(r);
        $("#idperiocidad").selectpicker('refresh');
    });

    $.post("../ajax/edificio.php?op=selectedificio", function (r) {
        $("#idedificio").html(r);
        $("#idedificio").selectpicker('refresh');
    });

    $.post("../ajax/cliente.php?op=selectcliente", function (r) {
        $("#idcliente").html(r);
        $("#idcliente").selectpicker('refresh');
    });
    
    
    $("#rut").focusout(function(){
        rut = $("#rut").val();
        razon = getrazonsocial(rut);
        if(razon === "**"){
            $("#razon").val("");
            $("#razon").attr("readOnly", false);
        }else if(razon === "***"){
            new PNotify({
                title: 'Error!',
                text: 'Rut incorrecto.',
                type: 'error',
                styling: 'bootstrap3'
            });
            $("#rut").focus();
            $("#razon").val("");
            $("#razon").attr("readOnly", true);
        }else{
            $("#razon").val(razon);
            $("#razon").attr("readOnly", true);
        }
    });
    
    $.post("../ajax/regiones.php?op=selectRegiones", function (r) {
        $("#conidregiones").html(r);
        $("#conidregiones").selectpicker('refresh');
        $("#idregionescli").html(r);
        $("#idregionescli").selectpicker('refresh');
        $("#idregionesedi").html(r);
        $("#idregionesedi").selectpicker('refresh');
    });
    
    $("#conidregiones").on("change", function (e){
       $.get("../ajax/comunas.php?op=selectComunasReg", {id: $("#conidregiones").val()}, function (r) {
            $("#concomuna").html(r);
            $("#concomuna").selectpicker('refresh');
        });
    });

    $("#idregionescli").on("change", function (e) {
        $.get("../ajax/comunas.php?op=selectComunasReg", {id: $("#idregionescli").val()}, function (r) {
            $("#idcomunascli").html(r);
            $("#idcomunascli").selectpicker('refresh');
        });
    });

    $("#idregionesedi").on("change", function (e) {
        $.get("../ajax/comunas.php?op=selectComunasReg", {id: $("#idregionesedi").val()}, function (r) {
            $("#idcomunasedi").html(r);
            $("#idcomunasedi").selectpicker('refresh');
        });
    });

    $.post("../ajax/tcliente.php?op=selecttcliente", function (r) {
        $("#idtcliente").html(r);
        $("#idtcliente").selectpicker('refresh');
    });

    $("#ncon_edi").on("change", function (e) {
        $('#contactoedi').empty();
        for (var i = 0; i < $('#ncon_edi').val(); i++) {
            var formedicon = '<div id="contactoedi' + i + '" name="contactoedi' + i + '">' +
                    '<h5>Contacto: ' + (i + 1) + '</h5>' +
                    '<div class="col-md-12 col-sm-12 col-xs-12 form-group">' +
                    '<label for="nombre_conedi' + i + '">Nombre y apellido<span class="required">*</span></label>' +
                    '<input type="text" id="nombre_conedi' + i + '" name="nombre_conedi' + i + '" required="required" class="form-control">' +
                    '</div>' +
                    '<div class="col-md-6 col-sm-12 col-xs-12 form-group">' +
                    '<label for="numero_conedi' + i + '">Numero de contacto<span class="required">*</span></label>' +
                    '<input type="text" id="numero_conedi' + i + '" name="numero_conedi' + i + '" required="required" class="form-control">' +
                    '</div>' +
                    '<div class="col-md-6 col-sm-12 col-xs-12 form-group">' +
                    '<label for="email_conedi' + i + '">Email<span class="required">*</span></label>' +
                    '<input type="text" id="email_conedi' + i + '" name="email_conedi' + i + '" required="required" class="form-control">' +
                    '</div>' +
                    '</div>';
            $('#contactoedi').append(formedicon);
            $("#numero_conedi" + i).inputmask({"mask": "+56(9)9999-9999"});
        }
    });
    
    //VERIFICO QUE EL NUMERO DE CONTRATO NO SE REPITA
    $("#ncontrato").on("focusout", function () {
        if ($("#ncontrato").val() != "") {
            var valor = $("#ncontrato").val();
            if (valor.length <= 15) {
                if (valor.includes('_')) {
                    $("#btnGuardarcont").attr('disabled', true);
                    new PNotify({
                        title: 'Error!',
                        text: 'El N° contrato debe tener 15 caracteres, incluido "-" ',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                    $("#ncontrato").focus();
                } else {
                    $.post("../ajax/contratoinstalacion.php?op=validarncontrato", {ncontrato: $("#ncontrato").val()}, function (data) {
                        data = JSON.parse(data);
                        if (data == 1) {
                            new PNotify({
                                title: 'OPS!',
                                text: 'Esta numero de contrato ya existe.',
                                type: 'error',
                                styling: 'bootstrap3'
                            });
                            $("#btnGuardarcont").prop("disabled", true);
                        } else {
                            $("#btnGuardarcont").prop("disabled", false);
                        }
                    });
                }
            }
        }
    });
    
    tablaasc = $("#ascensorestbl").DataTable({
        "paging": false,
        "info": false,
        "bFilter": false,
        "bAutoWidth": false,
        "ordering": false
    });

    $('#formulario').on("submit", function (event) {
        event.preventDefault();
        guardar();
    });

    $('#formcliente').on("submit", function (event) {
        event.preventDefault();
        guardarcliente();
    });
    
    $('#formedificio').on("submit", function (event) {
        event.preventDefault();
        guardaredificio();
    });
    
    $('#formulariotodo').on("submit", function (event) {
        event.preventDefault();
        guardartodo();
    });
    

    listar();
}

function mostrarform(flag) {
    //limpiar();
    if (flag) {
        $("#listadoproyectos").hide();
        $("#formularicontratos").show();
        $("#titulo_pagina").html("AGREGAR CONTRATO INSTALACIÓN");
    } else {

        $("#listadoproyectos").show();
        $("#formularicontratos").hide();
        $("#titulo_pagina").html("CONTRATO INSTALACIÓN");
    }

}

function cancelarform() {
    mostrarform(false);
}

function limpiar() {
    
    $("#idcliente").val("");
    $("#idcliente").selectpicker('refresh');
    $("#tcontrato").val("");
    $("#tcontrato").selectpicker('refresh');
    $("#ncontrato").val("");
    $("#nexterno").val("");
    $("#nreferencia").val("");
    $("#idperiocidad").val("");
    $("#idperiocidad").selectpicker('refresh');
    $("#ubicacion").val("");
    $("#fecha").val("");
    $("#fecha_ini").val("");
    $("#fecha_fin").val("");
    $("#nedificios").val("");
    $("#nedificios").selectpicker('refresh');
    $("#idperiocidad").val("");
    $("#idperiocidad").selectpicker('refresh');
    $("#observaciones").val("");
    $("#idedificio").val("");
    $("#idedificio").selectpicker('refresh');
    $("#idcontrato").val("");
    $("#idcontrato").selectpicker('refresh');
    $("#nascensores").val("");
    $("#nascensores").selectpicker('refresh');
    $('#ascensores').empty();

    //MODAL CLIENTE
    $("#razon").val("");
    $("#rut").val("");
    $("#callecli").val("");
    $("#numerocli").val("");
    $("#oficinacli").val("");
    $("#idregionescli").val("");
    $("#idregionescli").selectpicker('refresh');
    $("#idcomunascli").empty();
    $("#idcomunascli").val("");
    $("#idcomunascli").selectpicker('refresh');
    $("#idtcliente").val("");
    $("#idtcliente").selectpicker('refresh');

    //MODAL EDIFICIO
    
    $('#usar').prop('checked', false);
    
    $("#nombreedi").val("");
    $("#calleedi").val("");
    $("#numeroedi").val("");
    $("#idregionesedi").val("");
    $("#idregionesedi").selectpicker('refresh');
    $("#idcomunasedi").empty();
    $("#idcomunasedi").val("");
    $("#idcomunasedi").selectpicker('refresh');
    $("#idtsegmento").val("");
    $("#idtsegmento").selectpicker('refresh');
    $("#corcorreo").val("");
    $("#corcorreo").selectpicker('refresh');
    $("#residente").val("");
    $("#residente").selectpicker('refresh');
    $("#ncon_edi").val("");
    $("#ncon_edi").selectpicker('refresh');
    $("#contactoedi").empty();
}

function limpiaredificio() {
    $("#nombreedi").val("");
    $("#calleedi").val("");
    $("#numeroedi").val("");
    $("#idregionesedi").val("");
    $("#idregionesedi").selectpicker('refresh');
    $("#idcomunasedi").empty();
    $("#idcomunasedi").val("");
    $("#idcomunasedi").selectpicker('refresh');
    $("#idtsegmento").val("");
    $("#idtsegmento").selectpicker('refresh');
    $("#corcorreo").val("");
    $("#corcorreo").selectpicker('refresh');
    $("#residente").val("");
    $("#residente").selectpicker('refresh');
    $("#ncon_edi").val("");
    $("#ncon_edi").selectpicker('refresh');
    $("#contactoedi").empty();
}

function limpiarcliente() {
    $("#razon").val("");
    $("#rut").val("");
    $("#callecli").val("");
    $("#numerocli").val("");
    $("#oficinacli").val("");
    $("#idregionescli").val("");
    $("#idregionescli").selectpicker('refresh');
    $("#idcomunascli").empty();
    $("#idcomunascli").val("");
    $("#idcomunascli").selectpicker('refresh');
    $("#idtcliente").val("");
    $("#idtcliente").selectpicker('refresh');
}

function listar() {
    tabla = $('#tblproyectos').dataTable({
        "responsive": true,
        "aProcessing": true,
        "aServerSide": true,
        "scrollX": false,
        dom: 'Bfrtip',
        buttons: Botones,
        "language": Español,
        "ajax": {
            url: '../ajax/contratoinstalacion.php?op=listar',
            type: "get",
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 20, //Paginacion 10 items
        "order": [[2, "desc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}

function mostrar(idproyecto) {
    html = "";
    $("#idproyecto").val(idproyecto);
    $.post("../ajax/contratoinstalacion.php?op=listascensor", {idproyecto: idproyecto}, function (data, status) {
        data = JSON.parse(data);
        mostrarform(true);
        for (i = 0; i < data.length; i++) {
            html += "<div class='x_panel' id='info_" + i + "'><div class='clearfix'></div>";
            html += "<div class='col-md-2 col-sm-12 col-xs-12'><label>Ascensor</label> <br>" + data[i]["1"] + "</div>";
            html += "<div class='col-md-3 col-sm-12 col-xs-12'><label>Puesta servicio</label> " + data[i]["2"] + "</div>";
            html += "<div class='col-md-3 col-sm-12 col-xs-12'><label>Garantia tecnica</label> " + data[i]["3"] + "</div>";
            html += "<div class='col-md-2 col-sm-12 col-xs-12'><label>Ubicacion</label> " + data[i]["4"] + "</div>";
            html += "<div class='col-md-2 col-sm-12 col-xs-12'><label>Codigo Cliente</label> " + data[i]["5"] + "</div>";
            html += "</div>";
        }
        $("#nascensores").val(data.length);
        $("#listaascensor").html(html);
    });
}

function ficha(idproyecto){
    $.post("../ajax/contratoinstalacion.php?op=revisar", {idproyecto: idproyecto}, function (data) {
        data = JSON.parse(data);
        
        $("#nombProy").empty();
        $("#created_time").empty();
        $("#estadoproy").empty();
        $("#codigo").empty();
        $("#closed_time").empty();
        $("#pm").empty();
        $("#supervisor").empty();
        $("#numerovisita").empty();
        $("#ascensores").empty();
        $("#mandante").empty();
        $("#ruts").empty();
        $("#direccion").empty();
        
        $("#idproyecto").val(data.idproyecto);
        $("#idestado").val(data.estado);
        $("#idventa").val(data.idventa);

        $("#direccion").append(!data.calle ? '' : data.calle + ' #'+  data.numero);
        $("#nombProy").append(data.nombre);
        $("#created_time").append(formatofechacorta(data.fecha));
        $("#estadoproy").append(data.estadonomb);
        $("#codigo").append(data.codigo);
        $("#closed_time").append(formatofechacorta(data.fechafin));
        
        $("#mandante").append(data.razon_social);
        $("#ruts").append(data.rut);
        
        
        if (data.idventa) {
            $.post("../ajax/ascensorAjax.php?op=listar", {idventa: data.idventa}, function (data3) {
                tablaasc.rows().remove().draw();
                data3 = JSON.parse(data3);
                for (i = 0; i < data3.length; i++) {
                    tablaasc.row.add([
                        '<b>'+ data3[i]["codigo"] +'<b>',
                        '<b>'+ data3[i]["strmarca"] +'<b>',
                        '<b>'+ data3[i]["strmodelo"] +'<b>',
                        '<b>'+ data3[i]["ken"] +'<b>',
                        '<b>'+ data3[i]["ubicacion"] +'<b>',
                        '<label style="color:' + data3[i]['color'] + ';">' + data3[i]['strestado'] + '</label>'
                    ]).draw(false);
                }
                //$("#ascensores").append(asce);
            });
        }
    });
}

function datosproyecto() {
    if ($('#usar').prop('checked')) {
        idproyecto = $("#idproyecto").val();
        $.post("../ajax/contratoinstalacion.php?op=mostrar", {idproyecto: idproyecto}, function (data, status) {
            data = JSON.parse(data);

            $("#nombreedi").val(data.nombre);
            $("#calleedi").val(data.calle);
            $("#numeroedi").val(data.numero);
            $("#idregionesedi").val(data.idregion);
            $("#idregionesedi").selectpicker('refresh');
            $.get("../ajax/comunas.php?op=selectComunasReg", {id: data.idregion}, function (r) {
                $("#idcomunasedi").html(r);
                $("#idcomunasedi").val(data.idcomuna);
                $("#idcomunasedi").selectpicker('refresh');
            });
        });
    } else {
        limpiaredificio();
    }
}

function guardar() {
    var formData = new FormData($("#formulario")[0]);
    $.ajax({
        url: '../ajax/contrato.php?op=guardar',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,

        success: function (datos) {
            if (datos !== 0){
                new PNotify({
                    title: 'Registrado!',
                    text: 'El edificio fue registrado con exito.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
                $.post("../ajax/contrato.php?op=selectcontrato", function (r) {
                    $("#idcontrato").html(r);
                    $("#idcontrato").val(datos);
                    $("#idcontrato").selectpicker('refresh');
                });
                $('.modal-body').css('opacity', '');
                $('#modalContrato').modal('toggle');
            }else{
                new PNotify({
                    title: 'Error!',
                    text: 'Hubo un error al registar. ' + datos ,
                    type: 'error',
                    styling: 'bootstrap3'
                });
                $('.modal-body').css('opacity', '');
                $('#modalContrato').modal('toggle');
            }
        }
    });
    limpiar();
    /*bootbox.confirm("Va a guardar este contrato, esto causara cambios en Eidifio y ascensores, ¿esta seguro?", function(result){
        if (result){
            $("#btnGuardar").prop("disabled", true);
            var formData = new FormData($("#formulario")[0]);
            $.ajax({
                url: '../ajax/contratoinstalacion.php?op=guardaryeditar',
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,

                success: function (datos) {            
                    bootbox.alert(datos);
                    mostrarform(false);
                    tabla.ajax.reload();
                }
            });
            limpiar();
        }
    });*/
    
}

function guardarcliente() {
    var formData = new FormData($("#formcliente")[0]);
    $.ajax({
        url: '../ajax/cliente.php?op=guardar',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        beforeSend: function () {
            $("#btnGuardarCli").prop("disabled", true);
            $('.modal-body').css('opacity', '.5');
        },
        success: function (datos) {
            if (!isNaN(datos) || datos != 0) {
                new PNotify({
                    title: 'Registrado!',
                    text: 'El cliente fue registrado con exito.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
                $("#btnGuardarCli").prop("disabled", false);
                $('.modal-body').css('opacity', '');
                $('#modalForm').modal('toggle');
                $.post("../ajax/cliente.php?op=selectcliente", function (r) {
                    $("#idcliente").html(r);
                    $("#idcliente").val(datos);
                    $("#idcliente").selectpicker('refresh');
                });
            } else {
                new PNotify({
                    title: 'Error!',
                    text: 'Hubo un error al registar.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
        }
    });
    limpiarcliente();
}

function guardaredificio() {
    var formData = new FormData($("#formedificio")[0]);
    $.ajax({
        url: '../ajax/edificio.php?op=guardar',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        beforeSend: function () {
            $("#btnGuardarEdi").prop("disabled", true);
            $('.modal-body').css('opacity', '.5');
        },
        success: function (datos) {
            if (datos == 'OK') {
                new PNotify({
                    title: 'Registrado!',
                    text: 'El edificio fue registrado con exito.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
                $("#btnGuardarEdi").prop("disabled", false);
                $('.modal-body').css('opacity', '');
                $('#modalEdificio').modal('toggle');
                $.post("../ajax/edificio.php?op=selectedificio", function (r) {
                    $("#idedificio").html(r);
                    $("#idedificio").selectpicker('refresh');
                });
            } else if (!isNaN(datos) || datos == 0) {
                new PNotify({
                    title: 'Registrado!',
                    text: 'El edificio y sus contactos fueron registrados con exito.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
                $("#btnGuardarEdi").prop("disabled", false);
                $('.modal-body').css('opacity', '');
                $('#modalEdificio').modal('toggle');
                $.post("../ajax/edificio.php?op=selectedificio", function (r) {
                    $("#idedificio").html(r);
                    $("#idedificio").val(datos);
                    $("#idedificio").selectpicker('refresh');
                });
            } else if (datos == 'NOK') {
                new PNotify({
                    title: 'Error!',
                    text: 'Hubo un error al registar.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
                $("#btnGuardarEdi").prop("disabled", false);
                $('.modal-body').css('opacity', '');
                $('#modalEdificio').modal('toggle');
            } else {
                new PNotify({
                    title: 'Advertencia!',
                    text: 'Respuesta desconocia del servicio, verificar con administrador del sistema',
                    styling: 'bootstrap3'
                });
                $("#btnGuardarEdi").prop("disabled", false);
                $('.modal-body').css('opacity', '');
                $('#modalEdificio').modal('toggle');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 0) {
                new PNotify({
                    title: 'Error!',
                    text: 'No pudo conectar al servicio.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            } else if (jqXHR.status == 404) {
                new PNotify({
                    title: 'Error!',
                    text: 'Recurso no encontrado [404].',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            } else if (jqXHR.status == 500) {
                new PNotify({
                    title: 'Error!',
                    text: 'Error en el servidor [500].',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            } else if (textStatus === 'parsererror') {
                new PNotify({
                    title: 'Error!',
                    text: 'Objeto Incorrecto.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            } else if (textStatus === 'timeout') {
                new PNotify({
                    title: 'Error!',
                    text: 'Tiempo de espera agotado.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            } else if (textStatus === 'abort') {
                new PNotify({
                    title: 'Error!',
                    text: 'Solicitud abortada.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            } else {
                new PNotify({
                    title: 'Error!',
                    text: 'Error Desconocido: ' + jqXHR.responseText,
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
            $("#btnGuardarEdi").prop("disabled", false);
            $('.modal-body').css('opacity', '');
            $('#modalEdificio').modal('toggle');
        }
    });
    limpiaredificio();
}


function guardartodo(){
    bootbox.confirm("Va a guardar este contrato, esto causara cambios en Eidifio y ascensores, ¿esta seguro?", function(result){
        if (result){
            var formData = new FormData($("#formulariotodo")[0]);
            $.ajax({
                url: '../ajax/contratoinstalacion.php?op=guardar',
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,

                success: function (datos) {            
                    bootbox.alert(datos);
                    mostrarform(false);
                    tabla.ajax.reload();
                }
            });
            limpiar();
        }
    });
}

init();
