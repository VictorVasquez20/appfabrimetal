var tabla;

//funcion que se ejecuta iniciando
function init() {
    canvas = document.getElementById('firmafi');
    if (canvas) {
        canvas.height = canvas.offsetHeight;
        canvas.width = canvas.offsetWidth;
        signaturePad = new SignaturePad(canvas, {
            backgroundColor: 'rgb(255, 255, 255)',
            penColor: 'rgb(0, 0, 0)'
        });
    }

    mostrarform(false);
    var url = window.location.pathname;
    var str = url.split("/");
    var lastUrl = str.pop();
    console.log(lastUrl);
    // console.log(str[3]);

    
    listar('1,2');
    

    $('[data-toggle="tooltip"]').tooltip();

    $("#formulario").on("submit", function (e) {
        visita(e);
    });

    $('body').on('submit', '#formnewvisita', function(e) {
        guardarvisita(e);
    });

}


// Otras funciones
function limpiar() {
    $("#idproyecto").val("");
    $("#idestado").val("");
    $("#observaciones").val("");
    $("#esetapa").prop("checked", false);
}


function mostrarform(flag) {
    limpiar();
    if (flag) {
        $("#listadoproyectos").hide();
        $("#formulariovisita").show();
        $("#op_actualizar").hide();
        $("#btnGuardar").prop("disabled", false);
    } else {
        $("#formulariovisita").hide();
        $("#otradata").hide();
        $("#listadoproyectos").show();
        $("#op_actualizar").show();
        $("#btnGuardar").prop("disabled", false);
    }

}

function revisarform(flag) {
    limpiar();
    if (flag) {
        $("#listadoproyectos").hide();
        $("#revisarestado").show();
        $("#op_actualizar").hide();
    } else {
        $("#revisarestado").hide();
        $("#listadoproyectos").show();
        $("#op_actualizar").show();
    }

}

function cancelarform() {
    limpiar();
    mostrarform(false);

}

function cancelarrevisar() {
    limpiar();
    revisarform(false);
}

function fijarfirma() {

    if (signaturePad.isEmpty()) {
        new PNotify({
            title: 'Error en la firma',
            text: 'La firma no puede estar vacia',
            type: 'error',
            styling: 'bootstrap3'
        });
    } else {
        const padfirma = signaturePad.toDataURL();
        if (padfirma) {
            $("#firmavali").val("Firma validada");
            $("#firmavali").addClass(' border border-success');
            $("#firma").val(padfirma);
            $("#firmapad").hide();
            $("#btnFirmar").show();
            return 1;
        } else {
            $("#firmavali").val("Error al validar");
            $("#firmavali").addClass(' border border-danger');
        }
    }

}

function borrarfirma() {
    signaturePad.clear();
    $("#firma").val('');
    $("#firmapad").show();
    $("#firmavali").val('');
    $("#btnFirmar").hide();
}

function mostrar(idascensor) {

    $.post("../ajax/visitasinstalacionesAjaxEquipo.php?op=mostrar", {idascensor1: idascensor}, function (data, status) {
        data = JSON.parse(data);
        mostrarform(true);

        $("#nombProy").empty();
        $("#created_time").empty();
        $("#estadoproy").empty();
        $("#codigo").empty();
        $("#ccnomb").empty();
        $("#pm").empty();
        $("#supervisor").empty();
        $("#numerovisita").empty();
        $("#ascensores").empty();

        $("#idproyecto").val(data.idproyecto);
        $("#idestado").val(data.estado);
        $("#idventa").val(data.idventa);


        $("#nombProy").append(data.nombre);
        $("#created_time").append(data.fecha);
        $("#estadoproy").append(data.estadonomb);
        $("#codigo").append(data.codigo);
        $("#ccnomb").append(data.ccnomb);
        $("#pm").append(data.pm);
        $("#supervisor").append(data.supervisor);
        $("#numerovisita").append(data.nrovisita);



        if (data.idventa && data.estado > 1) {

            $.post("../ajax/ascensorAjax.php?op=listarEquipos", {idascensor2: idascensor}, function (data3, status) {
                data3 = JSON.parse(data3);
                var asce = "";
                for (i = 0; i < data3.length; i++) {
                    asce += '<div class="product_price col-md-12 col-sm-12 col-xs-12">'
                            + '    <input type="hidden" id="idascensor' + data3[i]['idascensor'] + '" name="idascensor[]" class="form-control" required="Campo requerido" value="' + data3[i]['idascensor'] + '">'
                            + '    <input type="hidden" id="estadoins' + data3[i]['idascensor'] + '" name="estadoins" class="form-control" required="Campo requerido" value="' + data3[i]['estadoins'] + '">'
                            + '    <div class="row" id="ascensorestres" style="margin: 0 15px;"><div class="col-md-4 col-sm-12 border" style="height: 100px;"><br><label class="green">' + data3[i]['codigo'] + ' - ken: ' + data3[i]['ken'] + ' - ubicación: ' + data3[i]['ubicacion'] + '</label><br>'
                            + '    <label style="color:' + data3[i]['color'] + ';"> Etapa: ' + data3[i]['strestado']+ '</label></div></div> <hr>' 
                            + '    <div class="col-md-12 col-sm-12 col-xs-12 form-group">'
                            + '        <p>Observaciones de la visita:</p>'
                            + '        <textarea type="text" id="observaciones' + data3[i]['idascensor'] + '" style="text-transform:uppercase;" name="observaciones" class="resizable_textarea form-control" required="Campo requerido"></textarea>'
                            + '    </div>'
                            + '    <div class="col-md-12 col-sm-12 col-xs-12 form-group" id="">'
                            + '        <p><input type="checkbox" id="esetapa' + data3[i]['idascensor'] + '" name="esetapa" value="1" class="js-switch"/> '
                            + '        Marque si la etapa fue completada</p>'
                            + '    </div>'
                            + '    <div class="col-md-12 col-sm-12 col-xs-12 form-group" id="' + data3[i]['idascensor'] + '">'
                            + '        <label for="descripcion2">Imagenes Asociadas</label> '
                            + '        <div class="dropzone" id="myDropzone"></div>'
                            + '    </div>'
                            + '</div>';
                            var ascensor = data3[i]['idascensor'];
                            var comando = data3[i]['comando'];
                            var ubicacion = data3[i]['ubicacion'];
                            var idventa = data3[i]['idventa'];
                            var estadoins = data3[i]['estadoins'];
                }
                $("#ascensores").append(asce);
                InitDropzone(ascensor);


                // transformo el comando a numero
                if(comando == 'SIMPLEX'){
                    var limit = 1;
                }else if(comando== 'DUPLEX'){
                    var limit = 2;
                }else if(comando == 'TRIPLEX'){
                    var limit = 3;
                }else if(comando == 'CUADRUPLEX' || comando == 'QUADRUPLEX'){
                    var limit = 4;
                }else if(comando == 'PENTAPLEX'){
                    var limit = 5;
                }else if(comando == 'SEXTAPLEX'){
                    var limit = 6;
                }else{
                    var limit = 1;
                }

                if(comando != '' && comando != 'SIMPLEX' && comando != 'N/S'){
                    $.post("../ajax/ascensorAjax.php?op=buscarcomandoubicacion", {comando: comando, ubicacion: ubicacion, idventa: idventa, estadoins: estadoins}, function (data3, status) {
                        data3 = JSON.parse(data3);
                        console.log(data3);
                        console.log(ascensor);
                        data4 = data3;
                        for (i = 0; i < limit; i++) {
                            if(data3[i]['idascensor'] != ascensor){
                                var asce = "";
                                var sele = "";
                                sele += '<select class="form-control selectpicker" data-live-search="true" name="idascensor[]" id="idascensor'+i+'" required>';
                                sele += '<option value="" disabled selected="selected">Seleccione FM</option>';
                                for (o = 0; o < data4.length; o++) {
                                    if(data4[o]['idascensor'] != ascensor){
                                        sele += '<option data-ken="' + data4[o]['ken'] + '" data-ubicacion="' + data4[o]['ubicacion'] + '" data-codigo="' + data4[o]['codigo'] + '" data-color="' + data4[o]['color'] + '" data-strestado="' + data4[o]['strestado'] + '" value="'+data4[o]['idascensor']+'">'+data4[o]['codigo']+'</option>';
                                    }
                                }
                                sele += '</select>';
                                if(data3[i]['idascensor'] != ascensor){
                                    asce +=   '<div class="col-md-4 col-sm-12 border"  style="height: 100px;">'+sele+'<br><div id="data'+i+'"style="display:none"></div></div>' ;
                                }
                                asce += '<script>$("#idascensor'+i+'").on("change", function(e){var datos = $(this).find("option:selected");'
                                    +'var text = \'<br><label class="green">\'+datos.data("codigo")+\' - ken: \'+datos.data("ken")+\' - ubicación: \'+datos.data("ubicacion")+\'</label><br>'
                                    + '<label style="color:\'+datos.data("color")+\';"> Etapa: \'+datos.data("strestado")+\'</label>\';'
                                    +'console.log(datos.data("codigo"));$("#data'+i+'").html(text);$("#data'+i+'").show();});</script>';
                                $("#ascensorestres").append(asce);
                                $('#idascensor'+i).selectpicker('refresh');
                            }
                        }
                    });

                }
            });
        }

        if (data.estado == 1) {
            $("#completada").hide();
            $("#esetapa").trigger('click').attr("checked", "checked");
            var btn = document.getElementById("btnGuardar");
            btn.innerHTML = "Iniciar Proyecto";
        } else {
            $("#completada").show();
            $("#esetapa").trigger('click').attr("checked", "checked");
            $("#esetapa").trigger('click').removeAttr("checked");
            var btn = document.getElementById("btnGuardar");
            btn.innerHTML = "Agregar";
        }


    });


}

function InitDropzone(ascensor){
     var uploadOK = false;
      // Dropzone.autoDiscover = false;
      Dropzone.prototype.defaultOptions.dictDefaultMessage = "<b style=\"color:black\">ARRASTRA O SELECCIONA TUS ARCHIVOS AQUÍ</b>";
      Dropzone.prototype.defaultOptions.dictFallbackMessage = "Su navegador no admite la carga de archivos de arrastrar y soltar.";
      Dropzone.prototype.defaultOptions.dictFallbackText = "Utilice el formulario de reserva a continuación para cargar sus archivos como en los viejos tiempos.";
      Dropzone.prototype.defaultOptions.dictFileTooBig = "El archivo es demasiado grande ({{filesize}} MiB). Tamaño máximo de archivo: {{maxFilesize}} MiB.";
      Dropzone.prototype.defaultOptions.dictInvalidFileType = "No puedes subir archivos de este tipo.";
      Dropzone.prototype.defaultOptions.dictResponseError = "El servidor respondió con el código {{statusCode}}.";
      Dropzone.prototype.defaultOptions.dictCancelUpload = "Cancelar Subida";
      Dropzone.prototype.defaultOptions.dictCancelUploadConfirmation = "¿Estás seguro de que deseas cancelar esta carga?";
      Dropzone.prototype.defaultOptions.dictRemoveFile = "Quitar Archivo";
      Dropzone.prototype.defaultOptions.dictMaxFilesExceeded = "No puedes subir mas archivos.";
      var myDropzone = new Dropzone("#myDropzone", {
        url: '../ajax/visitasinstalacionesAjaxEquipo.php?op=visitayetapa',
        autoProcessQueue: false,
        uploadMultiple: true,
        parallelUploads: 100,
        acceptedFiles: 'image/*',
        addRemoveLinks: true,
        init: function() {
          dzClosure = this;
          
          document.getElementById("btnGuardar").addEventListener("click", function(e) {
            e.preventDefault();
            e.stopPropagation();
            if (dzClosure.files.length) {
                var lista = new Array();
                var contador= 0;
                var contadorselect = 0;
                $("#formulario").find('select').each(function() {
                    var elemento= this;
                    if($.inArray(elemento.value,lista) >=0){
                        //alert($("#"+elemento.id+" option:selected").text()+' se encuentra repetido');
                        contador = contador +1;
                        return false;
                    }else{
                        lista.push(elemento.value);
                    }
                    if(elemento.value === "" || $("#formulario").find('textarea').val() == ''){
                        contadorselect = contadorselect+1;
                    }
                });
                if(contador == 0 && contadorselect == 0){
                    dzClosure.processQueue();
                }else{
                    bootbox.alert('Existen FM repetidos o Campos vacios');
                    return false;
                }
                return false;
            } else {
                bootbox.alert('Debe subir a lo menos 1 foto');
                return false;
            }
          });

          //// Envia todo el fomulario con las imagenes incluidas:
          this.on("sendingmultiple", function(data, xhr, formData) {
            var formData2 = new FormData($('#formulario')[0]);
            var poData = jQuery($('#formulario')[0]).serializeArray();
            for (var i=0; i<poData.length; i++){
              formData.append(poData[i].name, poData[i].value);
            }
          });

          //// Envia todo el fomulario con las imagenes incluidas:
          this.on("success", function(data) {
            console.log('subido');
          });

          this.on("queuecomplete", function() {
             mostrarform(false);
             tabla.ajax.reload();
          });

        }
      })
    }

function visitaotro(idproyecto) {

    $.post("../ajax/visitasinstalacionesAjax.php?op=mostrar", {idproyecto: idproyecto}, function (data, status) {
        data = JSON.parse(data);
        mostrarform(true);

        $("#nombProy").empty();
        $("#created_time").empty();
        $("#estadoproy").empty();
        $("#codigo").empty();
        $("#ccnomb").empty();
        $("#pm").empty();
        $("#supervisor").empty();
        $("#numerovisita").empty();
        $("#ascensores").empty();

        $("#idproyecto").val(data.idproyecto);
        $("#idestado").val(data.estado);
        $("#idventa").val(data.idventa);


        $("#nombProy").append(data.nombre);
        $("#created_time").append(data.fecha);
        $("#estadoproy").append(data.estadonomb);
        $("#codigo").append(data.codigo);
        $("#ccnomb").append(data.ccnomb);
        $("#pm").append(data.pm);
        $("#supervisor").append(data.supervisor);
        $("#numerovisita").append(data.nrovisita);



        if (data.idventa && data.estado > 1) {

            $.post("../ajax/ascensorAjax.php?op=listar", {idventa: data.idventa}, function (data3, status) {
                data3 = JSON.parse(data3);
                var asce = "";
                for (i = 0; i < data3.length; i++) {
                    asce += '<div class="product_price col-md-6 col-sm-6 col-xs-12">'
                            + '    <input type="hidden" id="idascensor' + data3[i]['idascensor'] + '" name="idascensor[]" class="form-control" required="Campo requerido" value="' + data3[i]['idascensor'] + '">'
                            + '    <input type="hidden" id="estadoins' + data3[i]['idascensor'] + '" name="estadoins' + data3[i]['idascensor'] + '" class="form-control" required="Campo requerido" value="' + data3[i]['estadoins'] + '">'
                            + '    <label class="green">' + data3[i]['codigo'] + ' - ken: ' + data3[i]['ken'] + ' - ubicación: ' + data3[i]['ubicacion'] + '</label><br>'
                            + '    <label style="color:' + data3[i]['color'] + ';"> Etapa: ' + data3[i]['strestado'] + '</label>'
                            + '    <div class="col-md-12 col-sm-12 col-xs-12 form-group">'
                            + '        <p>Observaciones de la visita:</p>'
                            + '        <textarea type="text" id="observaciones' + data3[i]['idascensor'] + '" style="text-transform:uppercase;" name="observaciones' + data3[i]['idascensor'] + '" class="resizable_textarea form-control" required="Campo requerido"></textarea>'
                            + '    </div>'
                            + '</div>';
                }
                $("#ascensores").append(asce);
            });
        }



        $("#completada").show();
        $("#esetapa").trigger('click').attr("checked", "checked");
        $("#esetapa").trigger('click').removeAttr("checked");
        var btn = document.getElementById("btnGuardar");
        btn.innerHTML = "Agregar";



    });


}


function revisar(idproyecto) {

    $.post("../ajax/visitasinstalacionesAjax.php?op=mostrar", {idproyecto: idproyecto}, function (data, status) {
        data = JSON.parse(data);
        revisarform(true);

        $("#nombProy2").empty();
        $("#created_time2").empty();
        $("#estadoproy2").empty();
        $("#codigo2").empty();
        $("#ccnomb2").empty();
        $("#pm2").empty();
        $("#supervisor2").empty();
        $("#numerovisita2").empty();
        $("#ascensores2").empty();

        $("#idproyecto2").val(data.idproyecto);
        $("#idestado2").val(data.estado);
        $("#idventa2").val(data.idventa);

        if (data.estado == 202) {
            $("#btnGrabar2").hide();
        }

        $("#nombProy2").append(data.nombre);
        $("#created_time2").append(data.fecha);
        $("#estadoproy2").append(data.estadonomb + " - (Duración: " + data.dias + " dias)");
        $("#codigo2").append(data.codigo);
        $("#ccnomb2").append(data.ccnomb);
        $("#pm2").append(data.pm);
        $("#supervisor2").append(data.supervisor);
        $("#numerovisita2").append(data.nrovisita);



        if (data.idventa) {

            $.post("../ajax/ascensorAjax.php?op=listar", {idventa: data.idventa}, function (data3, status) {
                data3 = JSON.parse(data3);
                var asce = "";
                for (i = 0; i < data3.length; i++) {
                    asce += '<div class="product_price col-md-6 col-sm-6 col-xs-12">'
                            + '    <input type="hidden" id="idascensor' + data3[i]['idascensor'] + '" name="idascensor[]" class="form-control" required="Campo requerido" value="' + data3[i]['idascensor'] + '">'
                            + '    <input type="hidden" id="estadoins' + data3[i]['idascensor'] + '" name="estadoins' + data3[i]['idascensor'] + '" class="form-control" required="Campo requerido" value="' + data3[i]['estadoins'] + '">'
                            + '    <label class="green">' + data3[i]['codigo'] + ' - ken: ' + data3[i]['ken'] + ' - ubicación: ' + data3[i]['ubicacion'] + '</label><br>'
                            + '    <label style="color:' + data3[i]['color'] + ';"> Etapa: ' + data3[i]['strestado'] + '</label>'
                            + '</div>';
                }
                $("#ascensores2").append(asce);
            });
        }


    });


}

function listavisitas(idascensor) {

    $.post("../ajax/visitasinstalacionesAjaxEquipo.php?op=listavisitas", {idascensor: idascensor, idproyecto: 4, idvisita: 11}, function (data, status) {
        $("#otradata").html(data); //data es el html procesada de la plantilla
        revisarform(true);
        $("#revisarestado").hide();
        $("#otradata").show();
        return;
    });

}

function visitaequipo(idascensor) {

    $.post("../ajax/visitasinstalacionesAjaxEquipo.php?op=listavisitas", {idascensor: idascensor, idproyecto: 4, idvisita: 11}, function (data, status) {
        $("#otradata").html(data); //data es el html procesada de la plantilla
        revisarform(true);
        $("#revisarestado").hide();
        $("#otradata").show();
        return;

    });

}

function newvisitaequipo(idascensor, tipoencuesta) {

    $.post("../ajax/visitasinstalacionesAjaxEquipo.php?op=newvisitaequipo", {idascensor: idascensor, tipoencuesta: tipoencuesta}, function (data, status) {
        $("#otradata").html(data); //data es el html procesada de la plantilla
        revisarform(true);
        $("#revisarestado").hide();
        $("#otradata").show();
        return;
    });

}

function guardarvisita(e) {
    e.preventDefault();

    if (fijarfirma()) {
        $("#btnGuardar").prop("disabled", true);
        var formData = new FormData($("#formnewvisita")[0]);

        bootbox.confirm("¿Desea generar una nueva visita para este equipo?", function (result) {
            if (result) {
                $.ajax({
                    url: '../ajax/visitasinstalacionesAjaxEquipo.php?op=guardarvisitaequipo',
                    type: "POST",
                    data: formData,
                    contentType: false,
                    processData: false,

                    success: function (datos) {
                        bootbox.alert(datos);
                        mostrarform(false);
                        tabla.ajax.reload();
                    }
                });
            }
        });

        limpiar();
    }/* else {
        new PNotify({
            title: 'Campo requerido',
            text: 'La firma no esta validada.',
            type: 'error',
            styling: 'bootstrap3'
        });
    }*/

}

function listar(tipoproyecto) {
    tabla = $('#tblproyectos').dataTable({
        "responsive": true,
        "aProcessing": true,
        "aServerSide": true,
        "scrollX": false,
        dom: 'Bfrtip',
        buttons: Botones,
        "language": Español,
        "ajax": {
            url: '../ajax/visitasinstalacionesAjaxEquipo.php?op=listar',
            type: "get",
            dataType: "json",
            data: {'tipoproyecto': tipoproyecto},
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 20, //Paginacion 10 items
        "order": [[1, "asc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}

function listarmodernizacion(tipoproyecto) {
    tabla = $('#tblproyectos').dataTable({
        "responsive": true,
        "aProcessing": true,
        "aServerSide": true,
        "scrollX": false,
        dom: 'Bfrtip',
        buttons: Botones,
        "language": Español,
        "ajax": {
            url: '../ajax/visitasinstalacionesAjax.php?op=listarModernizacion',
            type: "get",
            dataType: "json",
            data: {'tipoproyecto': tipoproyecto},
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 20, //Paginacion 10 items
        "order": [[2, "desc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}

function visita(e) {
    e.preventDefault();
    $("#btnGuardar").prop("disabled", true);
    var formData = new FormData($("#formulario")[0]);

    //if( $('#esetapa').prop('checked')) {
    bootbox.confirm("Va a registrar las visitas a este proyecto, ¿esta seguro?", function (result) {
        if (result) {
            $.ajax({
                url: '../ajax/visitasinstalacionesAjaxEquipo.php?op=visitayetapa',
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,

                success: function (datos) {
                    bootbox.alert(datos);
                    mostrarform(false);
                    tabla.ajax.reload();
                }
            });
        }
    });

    limpiar();

}

function finalizar() {

    $("#btnGuardar").prop("disabled", true);
    var formData = new FormData($("#formulario2")[0]);

    bootbox.confirm("Va a avanzar a este proyecto, ¿esta seguro?", function (result) {
        if (result) {
            $.ajax({
                url: '../ajax/visitasinstalacionesAjax.php?op=finalizarmodernizacion',
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,

                success: function (datos) {
                    bootbox.alert(datos);
                    revisarform(false);
                    tabla.ajax.reload();
                }
            });
        }
    });

    limpiar();
}

function pdf(idventa) {
    var a = document.createElement('a');
    a.href = '../ajax/venta.php?op=PDFCorto&idventa=' + idventa;
    a.download = 'venta.pdf';
    a.click();
    delete a;
}

function pdfvisitas(idproyecto) {
    /*alert('dsds');
    return;*/
    var a = document.createElement('a');
    a.href = '../ajax/visitasinstalacionesAjax.php?op=PDF&idproyecto=' + idproyecto;
    a.download = 'proyecto.pdf';
    a.click();
    delete a;
}

function MostrarDocumentos(idventa,idproyecto,visita = null, sistema = null){
    var dibujar = '<div class="col-md-3 col-sm-12 col-xs-12 text-center" id="solicitud"><p style="padding:15px;"><span class="text-danger" onClick="MostrarPreview(\'reportepdf\','+idproyecto+')"><i class="fa fa-file-pdf-o fa-3x"></i></span><br><span><h4>Reporte PDF</h4></span></p></div>'
        +'<div class="col-md-3 col-sm-12 col-xs-12 text-center" id="ordencompra"><p style="padding:15px;"><span class="text-danger" onClick="MostrarPreview(\'memoventa\','+idventa+')"><i class="fa fa-file-pdf-o fa-3x"></i></span><br><span><h4>Memo de Venta</h4></span></p></div>';
    if(visita != ''){
        dibujar +='<div class="col-md-3 col-sm-12 col-xs-12 text-center" id="visita"><p style="padding:15px;"><span class="text-danger" onClick="MostrarPreview(\'visitaencuesta\',\''+visita+',1\')"><i class="fa fa-file-pdf-o fa-3x"></i></span><br><span><h4>INFORME DE VISITA</h4></span></p></div>';
    }
    if(sistema != ''){
        dibujar +='<div class="col-md-3 col-sm-12 col-xs-12 text-center" id="sistema"><p style="padding:15px;"><span class="text-danger" onClick="MostrarPreview(\'visitaencuesta\',\''+sistema+',2\')"><i class="fa fa-file-pdf-o fa-3x"></i></span><br><span><h4>F-PREV-23</h4></span></p></div>';
    }
    $("#dibujar").html(dibujar);
    $('#modalDocumentos').modal('show');
}

function MostrarPreview(tipo, id){
    if(tipo == 'reportepdf'){        
        var url = '../ajax/visitasinstalacionesAjax.php?op=PDF&idproyecto='+id;
        var object = '<object class="PDFdoc" width="100%" style="height: 45vw;" data="'+ url +' "></object>';
        // var object = '<object class="PDFdoc" width="100%" style="height: 45vw;" type="application/pdf" data="'+ url +' "></object>';
        $("#contenido").html(object);
    }else if(tipo == 'memoventa'){
        var url = '../ajax/venta.php?op=PDFCorto&idventa='+id;
        var object = '<object class="PDFdoc" width="100%" style="height: 45vw;" data="'+ url +' "></object>';
        $("#contenido").html(object);
    }else if(tipo == 'visitaencuesta'){
        var url = '../ajax/visitasinstalacionesAjaxEquipo.php?op=PDFVISITA&visita='+id;
        var object = '<object class="PDFdoc" width="100%" style="height: 45vw;" data="'+ url +' "></object>';
        $("#contenido").html(object);
    }
    $('#modalPreview').modal('show');
}

$('#modalDocumentos').on('hidden.bs.modal', function () {
    $("#dibujar").html('');
});
$('#modalPreview').on('hidden.bs.modal', function () {
    $("#contenido").html('');
});

init();