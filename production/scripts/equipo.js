var tabla;

//funcion que se ejecuta iniciando
function init(){
	mostarform(false);
	listar();

	$('[data-toggle="tooltip"]').tooltip(); 

	$('#formulario').on("submit", function(event){
		event.preventDefault();
		guardaryeditar();
	});
        
        $.post("../ajax/tascensor.php?op=selecttascensor", function(r){
		$("#idtascensor").html(r);
		$("#idtascensor").selectpicker('refresh');
	});

	$.post("../ajax/marca.php?op=selectmarca", function(r){
		$("#marca").html(r);
		$("#marca").selectpicker('refresh');
	});
       
	$("#marca").on("change", function(e){
		$.get("../ajax/modelo.php?op=selectmodelo",{id:$('#marca').val()}, function(r){
			$("#modelo").html(r);
			$("#modelo").selectpicker('refresh');
		});
	});
}



function mostarform(flag){

	if(flag){
		$("#tabascensor").hide();
		$("#listadoascensores").hide();
		$("#formularioascensor").show();
		$("#op_actualizar").hide();
		$("#op_listar").show();
                $("#btnGuardar").prop("disabled", false);
	}else{
		$("#tabascensor").hide();
                $("#formularioascensor").hide();
		$("#listadoascensores").show();
		$("#op_actualizar").show();
		$("#op_listar").hide();
	}

}

function cancelarform(){
	mostarform(false);
}


function listar(){
	tabla=$('#tblascensores').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons:[
			'copyHtml5',
			'print',
			'excelHtml5',
			'csvHtml5',
			'pdf'
		],
		"ajax":{
			url:'../ajax/equipo.php?op=listarequipo',
			type:"get",
			dataType:"json",
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 15, //Paginacion 10 items
		"order" : [[1 , "desc"]] //Ordenar en base a la columna 0 descendente
	}).DataTable();
}


function guardaryeditar(){
	$("#btnGuardar").prop("disabled", true);
	var formData = new FormData($("#formulario")[0]);
	$.ajax({
		url:'../ajax/ascensor.php?op=editar',
		type:"POST",
		data:formData,
		contentType: false,
		processData:false,

		success: function(datos){
			bootbox.alert(datos);
			mostarform(false);
			tabla.ajax.reload();
		}
	});
}

function mostrar(idascensor){

	$("#listadoascensores").hide();
	$("#formularioascensor").hide();
	$("#op_actualizar").hide();
	$("#op_listar").show();
	$("#tabascensor").show();

	$.post("../ajax/ascensor.php?op=mostrar",{idascensor:idascensor}, function(data){
		data = JSON.parse(data);
                console.log(data);
                $("#tabcodigoti").html("Identificador N°: "+data.codigo);
                $("#tabcodigo").html(data.codigo);
		$("#tabcontrato").html(data.contrato);
                $("#tabedificio").html(data.edificio);
                $("#tabcliente").html(data.cliente);
                $("#tabtipo").html(data.tipo);
                $("#tabmarca").html(data.marca);
                $("#tabmodelo").html(data.modelo);
                $("#tabken").html(data.ken);
                $("#tabpservicio").html(data.pservicio);
                $("#tabgtecnica").html(data.gtecnica);
                $("#tabvaloruf").html(data.valoruf);
                $("#tabvalorclp").html(data.valorclp);
                $("#tabcapkg").html(data.capkg);
                $("#tabcapper").html(data.capper);
                $("#tabvelocidad").html(data.velocidad);
                $("#tabdcs").html(data.dcs);
                $("#tabelink").html(data.elink);            
	});
        	
}

function editar(idascensor){
    $.post("../ajax/ascensor.php?op=formeditar",{idascensor:idascensor}, function(data,status){
		data = JSON.parse(data);
		mostarform(true);
		$("#idascensor").val(data.idascensor);
                $("#idtascensor").val(data.idtascensor);                
		$("#idtascensor").selectpicker('refresh');
                $("#marca").val(data.marca);                
		$("#marca").selectpicker('refresh');
                $.get("../ajax/modelo.php?op=selectmodelo",{id:data.marca}, function(r){
			$("#modelo").html(r);
			$("#modelo").val(data.modelo);
			$("#modelo").selectpicker('refresh');
		});
		$("#ken").val(data.ken);
		$("#pservicio").val(data.pservicio);
		$("#gtecnica").val(data.gtecnica);
                $("#valoruf").val(data.valoruf);
                $("#valorclp").val(data.valorclp);
                $("#paradas").val(data.paradas);
                $("#capkg").val(data.capkg);
                $("#capper").val(data.capper);
                $("#velocidad").val(data.velocidad);
                $("#dcs").val(data.dcs);                
		$("#dcs").selectpicker('refresh');
                $("#elink").val(data.elink);                
		$("#elink").selectpicker('refresh');
                
	});
}



init();