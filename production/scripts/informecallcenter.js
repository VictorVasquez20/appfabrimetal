var tablallamadasporhorario,tablapromediollamadas,data_contestadas,data_nocontestadas, mybarChart;
var meses = {"0":"TODOS", "1":"ENERO","2":"FEBRERO","3":"MARZO","4":"ABRIL","5":"MAYO","6":"JUNIO","7":"JULIO","8":"AGOSTO","9":"SEPTIEMBRE","10":"OCTUBRE","11":"NOVIEMBRE","12":"DICIEMBRE",}
    
//funcion que se ejecuta iniciando
function init() {
    
    $.post("../ajax/informecallcenter.php?op=selectaniollamadasemergencia", function(r){
		$("#anio").html(r);
		$("#anio").selectpicker('refresh');
	});

    $("#anio").on("change", function(e){
                  
            $.get("../ajax/informecallcenter.php?op=selectmesllamadasemergencia",
            {'anio':$("#anio").val()}, function(r){

                    $("#mes").html(r);
                    $("#mes").selectpicker('refresh');
            });
    });    
    
    $('#form_filtros').on("submit", function(event){
		event.preventDefault();
		actualizar();
	});
        
    graficocantidadllamadasporhora();
    tablacantidadllamadasporhorario();
    tablatiempopromediollamadas();
}

function actualizar(){
    actualizargraficocantidadllamadasporhora();
    tablacantidadllamadasporhorario();
    tablatiempopromediollamadas();    
}
 

function graficocantidadllamadasporhora() {
    
    var f= new Date();
    var anio,mes;
    
    if($("#anio").val() == null || $("#anio").val() == ''){
        anio = f.getFullYear(); 
    }else{
        anio = $("#anio").val() 
    } 
    
     if($("#mes").val() == null || $("#mes").val() == ''){
        mes = parseInt(f.getMonth())+1; 
    }else{
        mes = $("#mes").val() 
    }
    
     $("#titulo_graficocantidadllamadasporhora").html("TENDENCIA - LLAMADAS DE EMERGENCIA POR HORA<br>(Año: "+anio+" Mes: "+meses[mes]+")");
    
    $.post("../ajax/informecallcenter.php?op=cantidadllamadasporhora", 
        {'anio': anio, 'mes': mes}, function (data, status) {
          data = JSON.parse(data);
    
    //inicializar.    
    data_contestadas =[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    data_nocontestadas =[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];

       data_contestadas[0]  = data['contestada_0'];
       data_contestadas[1]  = data['contestada_1'];
       data_contestadas[2]  = data['contestada_2'];
       data_contestadas[3]  = data['contestada_3'];
       data_contestadas[4]  = data['contestada_4'];
       data_contestadas[5]  = data['contestada_5'];
       data_contestadas[6]  = data['contestada_6'];
       data_contestadas[7]  = data['contestada_7'];
       data_contestadas[8]  = data['contestada_8'];
       data_contestadas[9]  = data['contestada_9'];
       data_contestadas[10] = data['contestada_10'];
       data_contestadas[11] = data['contestada_11'];
       data_contestadas[12] = data['contestada_12'];
       data_contestadas[13] = data['contestada_13'];
       data_contestadas[14] = data['contestada_14'];
       data_contestadas[15] = data['contestada_15'];
       data_contestadas[16] = data['contestada_16'];
       data_contestadas[17] = data['contestada_17'];
       data_contestadas[18] = data['contestada_18'];
       data_contestadas[19] = data['contestada_19'];
       data_contestadas[20] = data['contestada_20'];
       data_contestadas[21] = data['contestada_21'];
       data_contestadas[22] = data['contestada_22'];
       data_contestadas[23] = data['contestada_23'];
       
       data_nocontestadas[0]  = data['nocontestada_0'];
       data_nocontestadas[1]  = data['nocontestada_1'];
       data_nocontestadas[2]  = data['nocontestada_2'];
       data_nocontestadas[3]  = data['nocontestada_3'];
       data_nocontestadas[4]  = data['nocontestada_4'];
       data_nocontestadas[5]  = data['nocontestada_5'];
       data_nocontestadas[6]  = data['nocontestada_6'];
       data_nocontestadas[7]  = data['nocontestada_7'];
       data_nocontestadas[8]  = data['nocontestada_8'];
       data_nocontestadas[9]  = data['nocontestada_9'];
       data_nocontestadas[10] = data['nocontestada_10'];
       data_nocontestadas[11] = data['nocontestada_11'];
       data_nocontestadas[12] = data['nocontestada_12'];
       data_nocontestadas[13] = data['nocontestada_13'];
       data_nocontestadas[14] = data['nocontestada_14'];
       data_nocontestadas[15] = data['nocontestada_15'];
       data_nocontestadas[16] = data['nocontestada_16'];
       data_nocontestadas[17] = data['nocontestada_17'];
       data_nocontestadas[18] = data['nocontestada_18'];
       data_nocontestadas[19] = data['nocontestada_19'];
       data_nocontestadas[20] = data['nocontestada_20'];
       data_nocontestadas[21] = data['nocontestada_21'];
       data_nocontestadas[22] = data['nocontestada_22'];
       data_nocontestadas[23] = data['nocontestada_23'];
       
        ctx = document.getElementById("mybarChart");
        
        ctx.height = 125;
        
        mybarChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["00:00:00 - 00:59:59",
                        "01:00:00 - 01:59:59",
                        "02:00:00 - 02:59:59",
                        "03:00:00 - 03:59:59",
                        "04:00:00 - 04:59:59",
                        "05:00:00 - 05:59:59",
                        "06:00:00 - 06:59:59",
                        "07:00:00 - 07:59:59",
                        "08:00:00 - 08:59:59",
                        "09:00:00 - 09:59:59",
                        "10:00:00 - 10:59:59",
                        "11:00:00 - 11:59:59",
                        "12:00:00 - 12:59:59",
                        "13:00:00 - 13:59:59",
                        "14:00:00 - 14:59:59",
                        "15:00:00 - 15:59:59",
                        "16:00:00 - 16:59:59",
                        "17:00:00 - 17:59:59",
                        "18:00:00 - 18:59:59",
                        "19:00:00 - 19:59:59",
                        "20:00:00 - 20:59:59",
                        "21:00:00 - 21:59:59",
                        "22:00:00 - 22:59:59",
                        "23:00:00 - 23:59:59",
                ],
                datasets: [{
                            label: 'contestadas: ',
                            backgroundColor: "#26B99A",
                            data: data_contestadas
                            },
                            {
                                label: 'No Contestadas: ',
                                backgroundColor: "#FF0000",
                                data: data_nocontestadas
                            }
                        ]
            },            
            options: {
                 scales: {
                                xAxes: [{
                                        display: true,
                                        scaleLabel: {
                                                display: true,
                                                labelString: 'Horas'
                                        }
                                }],
                                yAxes: [{
                                        display: true,
                                        scaleLabel: {
                                                display: true,
                                                labelString: 'Cantidad de Llamadas'
                                        }
                                }]
                        }
            }
        });
    });

}

function actualizargraficocantidadllamadasporhora() {
    
    var f= new Date();
    var anio,mes;

    if($("#anio").val() == null || $("#anio").val() == ''){
        anio = f.getFullYear(); 
    }else{
        anio = $("#anio").val() 
    } 
    
     if($("#mes").val() == null || $("#mes").val() == ''){
        mes = parseInt(f.getMonth())+1; 
    }else{
        mes = $("#mes").val() 
    }
    
    $("#titulo_graficocantidadllamadasporhora").html("TENDENCIA - LLAMADAS DE EMERGENCIA POR HORA<br>(Año: "+anio+" Mes: "+meses[mes]+")");
    
    mybarChart.clear();
    
    $.post("../ajax/informecallcenter.php?op=cantidadllamadasporhora", 
        {'anio': anio, 'mes': mes}, function (data, status) {
          data = JSON.parse(data);
    
    var arr_contestadasupd =[
        data['contestada_0'],
        data['contestada_1'],
        data['contestada_2'],
        data['contestada_3'],
        data['contestada_4'],
        data['contestada_5'],
        data['contestada_6'],
        data['contestada_7'],
        data['contestada_8'],
        data['contestada_9'],
        data['contestada_10'],
        data['contestada_11'],
        data['contestada_12'],
        data['contestada_13'],
        data['contestada_14'],
        data['contestada_15'],
        data['contestada_16'],
        data['contestada_17'],
        data['contestada_18'],
        data['contestada_19'],
        data['contestada_20'],
        data['contestada_21'],
        data['contestada_22'],
        data['contestada_23'],       
    ];
    
       var arr_nocontestadasupd =[
        data['nocontestada_0'],
        data['nocontestada_1'],
        data['nocontestada_2'],
        data['nocontestada_3'],
        data['nocontestada_4'],
        data['nocontestada_5'],
        data['nocontestada_6'],
        data['nocontestada_7'],
        data['nocontestada_8'],
        data['nocontestada_9'],
        data['nocontestada_10'],
        data['nocontestada_11'],
        data['nocontestada_12'],
        data['nocontestada_13'],
        data['nocontestada_14'],
        data['nocontestada_15'],
        data['nocontestada_16'],
        data['nocontestada_17'],
        data['nocontestada_18'],
        data['nocontestada_19'],
        data['nocontestada_20'],
        data['nocontestada_21'],
        data['nocontestada_22'],
        data['nocontestada_23'],       
    ];

           //comparar los datos de las llamadas contestadas

            var cant_difContestadas=0;
            
            for (var i = 0; i < 24; i++) {                
                if (parseInt(data_contestadas[i]) !== parseInt(arr_contestadasupd[i])) {
                    cant_difContestadas++;                                     
                }               
            }
            
            //comparar los datos de las llamadas no contestadas
       
            var cant_difNoContestadas=0;
            
            for (var i = 0; i < 24; i++) {                
                if (parseInt(data_nocontestadas[i]) !== parseInt(arr_nocontestadasupd[i])) {
                    cant_difNoContestadas++;                                     
                }               
            }
            
            //si consegui diferencias actualizo el grafico.
            if(cant_difContestadas>0){
                mybarChart.data.datasets[0]['data']=arr_contestadasupd;
            }
            
            if(cant_difNoContestadas>0){
                mybarChart.data.datasets[1]['data']=arr_nocontestadasupd;
            }
            
            if( cant_difContestadas >0 || cant_difNoContestadas > 0){
                
                mybarChart.update();
                
            }
            
    });

}

function tablacantidadllamadasporhorario(){
   
    var f= new Date();
    var anio,mes;
  
    if($("#anio").val() == null || $("#anio").val() == ''){
        anio = f.getFullYear(); 
    }else{
        anio = $("#anio").val() 
    } 
    
     if($("#mes").val() == null || $("#mes").val() == ''){
        mes = parseInt(f.getMonth())+1; 
    }else{
        mes = $("#mes").val() 
    }
    
    $("#titulo_cantidadllamadashorario").html("LLAMADAS DE EMERGENCIA POR HORARIO<br>(Año: "+anio+" Mes: "+meses[mes]+")");
    
    tablallamadasporhorario=$('#tblcantidadllamadasporhorario').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
                searching: false, 
                paging: false,
                info: false,
		buttons:[
                        'copyHtml5',
			'print',
                        'csvHtml5',
		],
		"ajax":{
			url:'../ajax/informecallcenter.php?op=cantidadllamadasporhorario',
			type:"POST",
			dataType:"json",
                        data:{"anio":anio, 'mes': mes},
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true
	}).DataTable();
        
}

function tablatiempopromediollamadas(){
    
    var f= new Date();
    var anio,mes;

    if($("#anio").val() == null || $("#anio").val() == ''){
        anio = f.getFullYear(); 
    }else{
        anio = $("#anio").val() 
    } 
    
     if($("#mes").val() == null || $("#mes").val() == ''){
        mes = parseInt(f.getMonth())+1; 
    }else{
        mes = $("#mes").val() 
    }
 
    $("#titulo_tiempopromediollamadas").html("PROMEDIO DE LAS LLAMADAS DE EMERGENCIA CONTESTADAS<br>(Año: "+anio+" Mes: "+meses[mes]+")");
        
        tablapromediollamadas=$('#tblpromediollamadas').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
                searching: false, 
                paging: false,
                info: false,
		buttons:[
                        'copyHtml5',
			'print',
                        'csvHtml5',
		],
		"ajax":{
			url:'../ajax/informecallcenter.php?op=tiempopromediollamadas',
			type:"POST",
			dataType:"json",
                        data:{"anio":anio, 'mes': mes},
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true
	}).DataTable();
}

init();
