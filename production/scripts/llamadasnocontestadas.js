var tabla;

function init(){
    
        $("#formGestion").on("submit", function (e) {
            guardarFormGestion(e);
        });
        
	mostrarform(false);
        
	listar();
        
        setInterval("refrescar()", 150000);
        
        //evento cuando se abre el modal.    
        $('#ModalGestion').on('show.bs.modal', function (event) {
            
            var button = $(event.relatedTarget);

            $("#uniqueid").val(button.data('uniqueid'));
            $("#src").val(button.data('src'));
            $("#dst").val(button.data('dst'));
            $("#calldate").val(button.data('calldate'));
            $("#estado").val(button.data('estado'));
            if(parseInt(button.data('estado'))==1){
               $("#tituloModal").html("Gestionar Llamada"); 
            }else if(parseInt(button.data('estado'))==0){
               $("#tituloModal").html("<strong><font color='red'>Rechazar Llamada</font></strong>"); 
            }   
            $("#text_src").html(button.data('src'));
            $("#text_dst").html(button.data('dst'));
            $("#text_calldate").html(button.data('calldate'));
              
          });
          
        //evento cuando se cierra el modal.
        $('#ModalGestion').on('hidden.bs.modal', function (e) {
            limpiarModal();
        });
        
}

function limpiarModal(){
    $("#uniqueid").val("");
    $("#src").val("");
    $("#dst").val("");
    $("#calldate").val("");
    $("#estado").val("");
    $("#observacion").val("");
    $("#text_src").html("");
    $("#text_dst").html("");
    $("#text_calldate").html("");
}

function mostrarform(flag){
       
	if(flag){
		$("#listadollamdasnocontestadas").hide();		
		$("#op_agregar").hide();
		$("#op_listar").show();

	}else{
		$("#listadollamdasnocontestadas").show();		
		$("#op_agregar").show();
		$("#op_listar").hide();
	}

}

function listar(){
    
	tabla=$('#tbl_llamadasnocontestadas').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons:[			
			'excelHtml5',			
			'pdf'
		],
		"ajax":{
			url:'../ajax/llamadas.php?op=Listarnocontestadas',
			type:"get",
			dataType:"json",
                        timeout: 100000,
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 10, //Paginacion 10 items
		"order" : [[1 , "desc"]] //Ordenar en base a la columna 0 descendente
	}).DataTable();
}


function guardarFormGestion(e) {

    e.preventDefault();

            $.ajax({
                // En data puedes utilizar un objeto JSON, un array o un query string
                data: $("#formGestion").serialize(),
                type: "POST",
                url: "../ajax/llamadas.php?op=guardarGestionLlamada",
                beforeSend: function () {
                    $("#btnGuardarModalGestion").prop("disabled", true);
                    $('.modal-body').css('opacity', '.5');
                }
            })
            .done(function (data, textStatus, jqXHR) {
                if (console && console.log) {
                    console.log("La solicitud se ha completado correctamente.");
                }
                tabla.ajax.reload();
                bootbox.alert(data);
                limpiarModal();
                $("#btnGuardarModalGestion").prop("disabled", false);
                $('.modal-body').css('opacity', '');
                $('#ModalGestion').modal('toggle');
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                if (console && console.log) {
                    console.log("La solicitud a fallado: " + textStatus);
                }

            });

}

function refrescar(){
    tabla.ajax.reload();
    //alert('refrescar tabla');
}

init();