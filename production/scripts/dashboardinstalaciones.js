var tabla;
var ctx;

function init() {

    $.post("../ajax/pmIns.php?op=selectPm", function (r) {
        $("#idpm").html(r);
        $("#idpm").selectpicker('refresh');
    });

    $("#idpm").change(function () {

        $(".resultados").show();



        estadosProyectosPm($(this).val());

        visitasProyectoPm($(this).val());

    });

    tabla = $("#resumenVisitaSupervisor").DataTable({
        "paging": false,
        "info": false,
        "bFilter": false,
        "bAutoWidth": false,
        "ordering": false
    });

}

function estadosProyectosPm(idpm) {

    var ArrayLabels = [];
    var ArrayValores = [];
    var html = "";
    var totalProy = 0;
    var proyIniciar = 0;
    var proyIniciarPorcentaje = 0;
    var proyEjecucion = 0;
    var proyEjecucionPorcentaje = 0;
    var proyEntregados = 0;
    var proyEntregadosPorcentaje = 0;
    
    

    $.post("../ajax/dashInstalaciones.php?op=estadosProyectosPm", {"idpm": idpm}, function (data, status) {
        
        data = JSON.parse(data);
        console.log(data);
        for (i = 0; i < data.length; i++) {

            totalProy += parseInt(data[i]['cantidad']);

            if (parseInt(data[i]['idestadopro']) === 1) {
                proyIniciar = data[i]['cantidad'];
                proyIniciarPorcentaje = data[i]['porcentaje'];
            }

            if (parseInt(data[i]['idestadopro']) > 1 && parseInt(data[i]['idestadopro']) < 12) {
                proyEjecucion += parseInt(data[i]['cantidad']);
                proyEjecucionPorcentaje += parseFloat(data[i]['porcentaje']);
            }

            if (parseInt(data[i]['idestadopro']) === 12) {
                proyEntregados = data[i]['cantidad'];
                proyEntregadosPorcentaje = data[i]['porcentaje'];
            }
            
            if (parseInt(data[i]['idestadopro']) > 1 && parseInt(data[i]['idestadopro']) < 12) {

                ArrayLabels.push(data[i]['nombre_estado']);
                ArrayValores.push(data[i]['cantidad']);

                html += '<div class="animated flipInY col-md-4 col-sm-4 col-xs-4 tile_stats_count">'
                        + '<div class="left"></div>'
                        + '<div class="right"><span class="count_top"><i class="fa fa-check-circle"></i>&nbsp;' + data[i]['nombre_estado'] + ' </span>'
                        + '<div class="count">' + data[i]['cantidad'] + '</div>'
                        + '<span class="count_bottom"><i class="green"></i> &nbsp; </span>'
                        + '</div></div>';
            }
        }

        //resumen
        $("#totalProy").html(totalProy);
        $("#proyIniciar").html(proyIniciar);
        $("#proyIniciarPorcentaje").html(proyIniciarPorcentaje + '%');
        $("#proyEjecucion").html(proyEjecucion);
        $("#proyEjecucionPorcentaje").html(proyEjecucionPorcentaje.toFixed(2) + '%');
        $("#proyEntregados").html(proyEntregados);
        $("#proyEntregadosPorcentaje").html(proyEntregadosPorcentaje + '%');

        $("#div_estado_proyectos").empty();
        $("#div_estado_proyectos").html(html);

        $("#divGraficoProyectosEstado").empty();    
        $("#divGraficoProyectosEstado").append('<canvas id="graficoProyectosEstado" height="100%"></canvas>');

        ctx = document.getElementById('graficoProyectosEstado');

        var mybarChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels:  ArrayLabels,
                datasets: [{
                        label: 'Cantidad de Proyectos: ',
                        backgroundColor: "rgba(92, 184, 92, 0.31)",
                        data: ArrayValores
                    }]
            },

            options: {
                scales: {
                    yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                }
            }
        });
        
    });


}

function visitasProyectoPm(idpm) {

    cantidad_visitas = [];
    supervisores = [];

    $.post("../ajax/dashInstalaciones.php?op=visitasProyectoPm", {"idpm": idpm}, function (data, status) {
        data = JSON.parse(data);

        tabla.rows().remove().draw();

        for (i = 0; i < data.length; i++) {

            tabla.row.add([
                data[i]['nombre_supervisor'],
                data[i]['total_proyectos'],
                data[i]['cantidad_ascensores'],
                data[i]['cantidad_paradas'],
                '0',
                parseFloat(data[i]['visitas_proyectos']).toFixed(0),
                parseFloat(data[i]['visitas_ascensores']).toFixed(0),
                parseFloat(data[i]['visitas_proyectos'] / data[i]['total_proyectos']).toFixed(2),
                parseFloat(data[i]['visitas_ascensores'] / data[i]['cantidad_ascensores']).toFixed(2),
                parseFloat(data[i]['visitas_proyectos'] / data[i]['cantidad_meses']).toFixed(2)
            ]).draw(false);

            supervisores.push(data[i]['nombre_supervisor']);

            cantidad_visitas.push(data[i]['visitas_ascensores']);
        }

        $("#divVisitasAscensores").empty();    
        $("#divVisitasAscensores").append('<canvas id="canvasVisitasAscensores" height="50%"></canvas>');
 
        var ctx2 = document.getElementById('canvasVisitasAscensores');

            var lineChart = new Chart(ctx2, {
                type: 'line',
                data: {
                    labels: supervisores,
                    datasets: [{
                            label: "visitas",
                            backgroundColor: "rgba(92, 184, 92, 0.31)",
                            borderColor: "rgba(92, 184, 92, 0.7)",
                            pointBorderColor: "rgba(92, 184, 92, 0.7)",
                            pointBackgroundColor: "rgba(92, 184, 92, 0.7)",
                            pointHoverBackgroundColor: "#fff",
                            pointHoverBorderColor: "rgba(220,220,220,1)",
                            pointBorderWidth: 0,
                            data: cantidad_visitas
                        }]
                },
            });
 

    });
}

init();