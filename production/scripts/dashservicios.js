var refdata, refman;
var ctx, mybarChart;
var ctxmant, mybarMant;
var chart_gauge_elem, chart_gauge_meta;


//funcion que se ejecuta iniciando
function init() {
    CargarGuias();
    CargarEstado();
    GraficoGauge();
    GraficoMantencion();
    ListarAlerta(false);
    setInterval("Actualizar()", 60000);
}

function ListarAlerta(flag) {

    if (flag) {
        $("#Charts").hide();
    } else {
        $("#Charts").show();
    }

}


function GraficoGauge() {

    var chart_gauge_settings = {
        lines: 12,
        angle: 0,
        lineWidth: 0.4,
        pointer: {
            length: 0.75,
            strokeWidth: 0.042,
            color: '#1D212A'
        },
        limitMax: 'false',
        colorStart: '#1ABC9C',
        colorStop: '#1ABC9C',
        strokeColor: '#F0F3F3',
        generateGradient: true
    };
    
    chart_gauge_elem = document.getElementById('chart_gauge_meta');
    chart_gauge_meta = new Gauge(chart_gauge_elem).setOptions(chart_gauge_settings);
    if ($('#gauge-text').length) {
        chart_gauge_meta.maxValue = 1561;
        chart_gauge_meta.animationSpeed = 32;
        chart_gauge_meta.set(1200);
        chart_gauge_meta.setTextField(document.getElementById("gauge-text"));
    }

//    $.post("../ajax/servicio.php?op=GM", function (data, status) {
//        data = JSON.parse(data);
//        refdata = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
//
//        for (var i = 0; i < data.aaData.length; i++) {
//            refdata[data.aaData[i][0] - 1] = data.aaData[i][1];
//        }
//
//        ctx = document.getElementById("mybarChart");
//        ctx.height = 125;
//        mybarChart = new Chart(ctx, {
//            type: 'bar',
//            data: {
//                labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
//                datasets: [{
//                        label: 'Servicios',
//                        backgroundColor: "#26B99A",
//                        data: refdata
//                    }]
//            },
//
//            options: {
//                scales: {
//                    yAxes: [{
//                            ticks: {
//                                beginAtZero: true
//                            }
//                        }]
//                }
//            }
//        });
//    });

}


function GraficoMantencion() {

    $.post("../ajax/servicio.php?op=GMM", function (data, status) {
        data = JSON.parse(data);
        refman = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

        for (var i = 0; i < data.aaData.length; i++) {
            refman[data.aaData[i][0] - 1] = data.aaData[i][1];
        }

        ctxmant = document.getElementById("mybarMant");
        ctxmant.height = 125;
        mybarMant = new Chart(ctxmant, {
            type: 'bar',
            data: {
                labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                datasets: [{
                        label: 'Servicios',
                        backgroundColor: "#26B99A",
                        data: refman
                    }]
            },

            options: {
                scales: {
                    yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                }
            }
        });
    });

}



function ActializarGuias() {

    $.post("../ajax/servicio.php?op=GM", function (data, status) {
        data = JSON.parse(data);

        var updata = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        for (var i = 0; i < data.aaData.length; i++) {
            updata[data.aaData[i][0] - 1] = data.aaData[i][1];
        }

        for (var i = 0; i < updata.length; i++) {
            if (refdata[i] != updata[i]) {
                refdata[i] = updata[i];
                console.log("Consiguio diferencia");
                console.log(refdata);
                mybarChart.data.datasets[0]['data'] = refdata;
                mybarChart.update();
            }

        }
    });
}

function ActializarMantencion() {

    $.post("../ajax/servicio.php?op=GMM", function (data, status) {
        data = JSON.parse(data);

        var updata = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        for (var i = 0; i < data.aaData.length; i++) {
            updata[data.aaData[i][0] - 1] = data.aaData[i][1];
        }

        for (var i = 0; i < updata.length; i++) {
            if (refman[i] != updata[i]) {
                refman[i] = updata[i];
                console.log("Consiguio diferencia");
                console.log(refman);
                mybarMant.data.datasets[0]['data'] = refman;
                mybarMant.update();
            }

        }
    });
}


function CargarGuias() {

    $.post("../ajax/servicio.php?op=DG", function (data, status) {
        data = JSON.parse(data);
        //Actualizamos valores
        $("#ser_pro").html(data.proceso.guias);
        $("#ser_com").html(data.completadas.guias);
        $("#ser_firma").html(data.penfirma.guias);
    });
}

function CargarEstado() {

    $.post("../ajax/servicio.php?op=EE", function (data, status) {
        data = JSON.parse(data);
        //Actualizamos valores
        var refes = [0, 0, 0, 0, 0, 0, 0, 0];

        for (var i = 0; i < data.aaData.length; i++) {
            refes[data.aaData[i][0] - 1] = data.aaData[i][1];
        }

        $("#num_normal").html(refes[0]);
        $("#num_detenido").html(refes[6]);
        $("#num_observacion").html(refes[7]);
        $("#num_emergencia").html(refes[5]);
        $("#num_mantencion").html(refes[1]);
        $("#num_reparacion").html(refes[2]);
        $("#num_mayor").html(refes[3]);
        $("#num_ingenieria").html(refes[4]);

    });

}

function Actualizar() {
    CargarGuias();
    CargarEstado();
    ActializarGuias();
    ActializarMantencion();
}

init();