/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var tablareparacion;
var tablaemergencia;
var arrmeses = ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"];

function init() {
    tablareparacion = $("#tblreparaciones").DataTable({
        "paging": false,
        "info": false,
        "bFilter": false,
        "bAutoWidth": false,
        "ordering": false
    });

    tablaemergencia = $("#tblemergencia").DataTable({
        "paging": false,
        "info": false,
        "bFilter": false,
        "bAutoWidth": false,
        "ordering": false
    });
    cargamantencion();
    cargareparacion();
    cargaemergencia();
    graficoReparaciones();
    graficoEmergencias();
    graficoMantencion()
    setTimeout(slide, 180000);
    
}

function slide(){    
    $(location).attr("href", "slideinstalaciones.php");
}

function cargamantencion() {
    $.post("../ajax/dashgse.php?op=datos", {"idtservicio": 3}, function (data, status) {
        data = JSON.parse(data);
        //Actualizamos valores
        $("#encartera").empty();
        $("#encartera").html("EN CARTERA " + data.cartera);

        $("#mdia").empty();
        $("#mdia").html(data.guiadia);

        $("#mpordia").empty();
        $("#mpordia").html(((data.guiadia / data.cartera) * 100).toFixed(1) + "%");

        $("#mmes").empty();
        $("#mmes").html(data.guiames);

        $("#mpormes").empty();
        $("#mpormes").html(((data.guiames / data.cartera) * 100).toFixed(1) + "%");

        $("#mproceso").empty();
        $("#mproceso").html(data.guiaenproceso);

        $("#mcompleto").empty();
        $("#mcompleto").html(data.guiacompletada);

        $("#mcerrado").empty();
        $("#mcerrado").html(data.guiacerrada);
        
        $("#mcerradosf").empty();
        $("#mcerradosf").html(data.guiacerradaSF);
    });
}

function cargareparacion() {
    $.post("../ajax/dashgse.php?op=datostodos", {"idtservicios": '1,4,5,6'}, function (data, status) {
        data = JSON.parse(data);
        //Actualizamos valores
        tablareparacion.rows().remove().draw();
        var sumproceso = 0, sumcompleto = 0, sumcerrado = 0,sumcerradoSF = 0;
        for (i = 0; i < data.length; i++) {
            tablareparacion.row.add([
                data[i]['0'],
                data[i]['2'],
                data[i]['1']
            ]).draw(false);
            ;

            sumproceso = parseInt(sumproceso) + parseInt(data[i]['3']);
            sumcompleto = parseInt(sumcompleto) + parseInt(data[i]['4']);
            sumcerrado = parseInt(sumcerrado) + parseInt(data[i]['5']);
            sumcerradoSF = parseInt(sumcerradoSF) + parseInt(data[i]['8']);
        }

        $("#rproceso").empty();
        $("#rproceso").html(sumproceso);

        $("#rcompleto").empty();
        $("#rcompleto").html(sumcompleto);

        $("#rcerrado").empty();
        $("#rcerrado").html(sumcerrado);
        
        $("#rcerradoSF").empty();
        $("#rcerradoSF").html(sumcerradoSF);
    });
}

function cargaemergencia() {
    $.post("../ajax/dashgse.php?op=datosemergencia", {"idtservicios": '2', "estados": '1,2,3,4,5,6,7,8'}, function (data, status) {
        data = JSON.parse(data);
        //Actualizamos valores
        tablaemergencia.rows().remove().draw();
        var sumproceso = 0, sumcompleto = 0, sumcerrado = 0, sumcerradoSF = 0;
        for (i = 0; i < data.length; i++) {
            tablaemergencia.row.add([
                data[i]['0'],
                data[i]['2'],
                data[i]['1']
            ]).draw(false);
            ;

            sumproceso = parseInt(sumproceso) + parseInt(data[i]['3']);
            sumcompleto = parseInt(sumcompleto) + parseInt(data[i]['4']);
            sumcerrado = parseInt(sumcerrado) + parseInt(data[i]['5']);
            sumcerradoSF = parseInt(sumcerradoSF) + parseInt(data[i]['8']);
        }

        $("#eproceso").empty();
        $("#eproceso").html(sumproceso);

        $("#ecompleto").empty();
        $("#ecompleto").html(sumcompleto);

        $("#ecerrado").empty();
        $("#ecerrado").html(sumcerrado);
        
        $("#ecerradoSF").empty();
        $("#ecerradoSF").html(sumcerradoSF);
    });
}

function graficoReparaciones() {
    $.post("../ajax/dashgse.php?op=grafico", {"idtservicio": '1,4,5,6'}, function (data, status) {
        data = JSON.parse(data);
        arr = [0,0,0,0,0,0,0,0,0,0,0,0,0];
        meseNomb = [0,0,0,0,0,0,0,0,0,0,0,0];
        var fecha = new Date();
        mesact = fecha.getMonth() + 1 ;
        
        for(m = 0; m < 12; m++){
            meseNomb[m] = arrmeses[mesact];
            mesact ++;
            if(mesact == 12){
                mesact = 0;
            }
        }
        
        for (i = 0; i < data.length; i++) {
            arr[meseNomb.indexOf(data[i]['1'].substr(0,3))] = data[i]['3'];  ;
        }
        
        if ($('#reparaciones').length) {
            var ctx = document.getElementById("reparaciones");
            var mybarChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: meseNomb,
                    datasets: [{
                            backgroundColor: "#26B99A",
                            data: arr
                        }]
                },
                options: {
                    legend: {
                        display: false
                    },
                    scales: {
                        yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                    }
                }
            });
        }
    });
}

function graficoEmergencias() {
    $.post("../ajax/dashgse.php?op=grafico", {"idtservicio": '2'}, function (data, status) {
        data = JSON.parse(data);
        arr = [0,0,0,0,0,0,0,0,0,0,0,0,0];
        meseNomb = [0,0,0,0,0,0,0,0,0,0,0,0];
        var fecha = new Date();
        mesact = fecha.getMonth() + 1 ;
        
        for(m = 0; m < 12; m++){
            meseNomb[m] = arrmeses[mesact];
            mesact ++;
            if(mesact == 12){
                mesact = 0;
            }
        }
        
        for (i = 0; i < data.length; i++) {
            arr[meseNomb.indexOf(data[i]['1'].substr(0,3))] = data[i]['3'];  ;
        }
        
        if ($('#emergencias').length) {
            var ctx = document.getElementById("emergencias");
            var mybarChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: meseNomb,
                    datasets: [{
                            backgroundColor: "#26B99A",
                            data: arr
                        }]
                },
                options: {
                    legend: {
                        display: false
                    },
                    scales: {
                        yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                    }
                }
            });
        }
    });
}

function graficoMantencion() {
    $.post("../ajax/dashgse.php?op=grafico", {"idtservicio": '3'}, function (data, status) {
        data = JSON.parse(data);
        arr = [0,0,0,0,0,0,0,0,0,0,0,0,0];
        meseNomb = [0,0,0,0,0,0,0,0,0,0,0,0];
        var fecha = new Date();
        mesact = fecha.getMonth() + 1 ;
        
        for(m = 0; m < 12; m++){
            meseNomb[m] = arrmeses[mesact];
            mesact ++;
            if(mesact == 12){
                mesact = 0;
            }
        }
        
        for (i = 0; i < data.length; i++) {
            arr[meseNomb.indexOf(data[i]['1'].substr(0,3))] = data[i]['3'];  ;
        }
        
        if ($('#lineChart1').length) {
            var ctx = document.getElementById("lineChart1");
            var lineChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: meseNomb,
                    datasets: [{
                            label: "",
                            backgroundColor: "rgba(38, 185, 154, 0.31)",
                            borderColor: "rgba(38, 185, 154, 0.7)",
                            pointBorderColor: "rgba(38, 185, 154, 0.7)",
                            pointBackgroundColor: "rgba(38, 185, 154, 0.7)",
                            pointHoverBackgroundColor: "#fff",
                            pointHoverBorderColor: "rgba(220,220,220,1)",
                            pointBorderWidth: 1,
                            data: arr
                        }]
                },
                options: {
                    legend: {
                        display: false
                    }
                }
            });

        }
    });
}


init();