var tabla;
var tablaejecutivo;
var tablacompras;

//funcion que se ejecuta iniciando
function init() {
    mostrarform(false);
    mostrarficha(false);
    listar();
    $('[data-toggle="tooltip"]').tooltip();

    $.post("../ajax/categoria_proveedor.php?op=selectcategoria", function (r) {
        $("#idcategoria").html(r);
        $("#idcategoria").selectpicker('refresh');        
    });
    
    $.post("../ajax/categoria_proveedor.php?op=selectcategoriafiltro", function (r) {        
        $("#filcategoria").html(r);
        $("#filcategoria").selectpicker('refresh');
        
    });
    
    
    $.post("../ajax/regiones.php?op=selectRegiones", function (r) {
        $("#idregion").html(r);
        $("#idregion").selectpicker('refresh');
    });

    $("#idregion").on('change', function () {
        $.get("../ajax/comunas.php?op=selectComunasReg", {id: $("#idregion").val()}, function (r) {
            $("#idcomuna").html(r);
            $("#idcomuna").selectpicker('refresh');
        });
    });

    $.post("../ajax/condicionpago.php?op=selectcondicion", function (r) {
        $("#idcondicionpago").html(r);
        $("#idcondicionpago").selectpicker('refresh');
    });

    $.post("../ajax/proveedor.php?op=selectpresupuesto", function (r) {
        $("#presupuesto").html(r);
        $("#presupuesto").selectpicker('refresh');
    });


    $("#formulario").on("submit", function (e) {
        e.preventDefault();
        guardaryeditar();
    });

    $("#formulariocat").on("submit", function (e) {
        e.preventDefault();
        guardarcat();
    });

    $("#formulariocond").on("submit", function (e) {
        e.preventDefault();
        guardarcond();
    });

    $("#formejecutivo").on("submit", function (e) {
        e.preventDefault();
        guardarEjecutivo();
    });

    $("#formdocto").on("submit", function (e) {
        e.preventDefault();
        guardarDocumento();
    });

    $("#formcompra").on("submit", function (e) {
        e.preventDefault();
        guardarCompra();
    });

    $("#rut").focusout(function () {
        rut = $("#rut").val();
        razon = getrazonsocial(rut);
        if (razon === "**") {
            $("#razonsocial").val("");
            $("#razonsocial").attr("readOnly", false);
        } else if (razon === "***") {
            new PNotify({
                title: 'Error!',
                text: 'Rut incorrecto.',
                type: 'error',
                styling: 'bootstrap3'
            });
            $("#rut").focus();
            $("#razonsocial").val("");
            $("#razonsocial").attr("readOnly", true);
        } else {
            $("#razonsocial").val(razon);
            $("#razonsocial").attr("readOnly", true);
        }
    });
    $("#rut").focusout(function () {
        if (validarut($("#rut").val())) {
            new PNotify({
                title: 'Correcto!',
                text: 'Rut correcto.',
                type: 'success',
                styling: 'bootstrap3'
            });
        } else {
            new PNotify({
                title: 'Error!',
                text: 'rut incorrecto.',
                type: 'error',
                styling: 'bootstrap3'
            });
            $("#rut").focus();
        }
    });

    $('.starrr').starrr({
        change: function (e, value) {
            $("#evaluacion").val(value);
        }
    });
}

// Otras funciones
function limpiar() {
    $("#idproveedor").val("");
    $("#razonsocial").val("");
    $("#rut").val("");
    $("#idcategoria").val("");
    $("#idcategoria").selectpicker('refresh');
    $("#idregion").val("");
    $("#idregion").selectpicker('refresh');
    $("#idcomuna").val("");
    $("#idcomuna").selectpicker('refresh');
    $("#direccion").val("");
    $("#contacto").val("");
    $("#fono").val("");
    $("#email").val("");
    $("#idcondicionpago").val("");
    $("#idcondicionpago").selectpicker('refresh');
    $("#plazopago").val("");
    $("#observacion").val("");
    $("#servicioflete").prop("checked", false);
    $("#condicion").prop("checked", false);
}

function mostrarform(flag) {
    limpiar();
    if (flag) {
        $("#listadoproveedor").hide();
        $("#formularioproveedor").show();
        $("#op_agregar").hide();
        $("#op_listar").show();
        $("#btnGuardar").prop("disabled", false);

    } else {
        $("#listadoproveedor").show();
        $("#formularioproveedor").hide();
        $("#op_agregar").show();
        $("#op_listar").hide();
    }

}

function mostrarficha(flag) {
    if (flag) {
        $("#listadoproveedor").hide();
        $("#ficha").show();
        $("#op_agregar").hide();
        $("#op_listar").show();
        $("#btnGuardar").prop("disabled", false);

    } else {
        $("#listadoproveedor").show();
        $("#ficha").hide();
        $("#op_agregar").show();
        $("#op_listar").hide();
    }

}

function cancelarform() {
    limpiar();
    mostrarform(false);
}

function listar() {    
    tabla = $('#tblproveedor').dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Bfrtip',
        buttons: Botones,
        "language": Español,
        "ajax": {
            url: '../ajax/proveedor.php?op=listar',
            type: "get",
            data: {'idcategoria': $("#filcategoria").val()},
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 10, //Paginacion 10 items
        "order": [[1, "desc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}

function guardaryeditar() {
    $("#btnGuardar").prop("disabled", true);
    var formData = new FormData($("#formulario")[0]);
    $.ajax({
        url: '../ajax/proveedor.php?op=guardaryeditar',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,

        success: function (datos) {
            //bootbox.alert(datos);
            if (datos != '0') {
                $("#btnGuardar").prop("disabled", false);
                new PNotify({
                    title: 'Correcto!',
                    text: 'Proveedor guardado con exito.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
            } else {
                $("#btnGuardar").prop("disabled", true);
                new PNotify({
                    title: 'Error!',
                    text: 'Proveedor no guardada.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
            mostrarform(false);
            tabla.ajax.reload();
        }
    });
    limpiar();
}

function mostrar(idproveedor) {
    $.post("../ajax/proveedor.php?op=mostrar", {"idproveedor": idproveedor}, function (data) {
        data = JSON.parse(data);
        mostrarform(true);

        $("#idproveedor").val(data.idproveedor);
        $("#razonsocial").val(data.razonsocial);
        $("#rut").val(data.rut);
        $("#idcategoria").val(data.idcategoria);
        $("#idcategoria").selectpicker('refresh');
        $("#idregion").val(data.idregion);
        $("#idregion").selectpicker('refresh');
        
        $.get("../ajax/comunas.php?op=selectComunasReg", {id: $("#idregion").val()}, function (r) {
            $("#idcomuna").html(r);
            $("#idcomuna").val(data.idcomuna);
            $("#idcomuna").selectpicker('refresh');
        });
        
        //$("#idcomuna").selectpicker('refresh');
        $("#direccion").val(data.direccion);
        $("#contacto").val(data.contacto);
        $("#fono").val(data.fono);
        $("#email").val(data.email);
        $("#idcondicionpago").val(data.idcondicionpago);
        $("#idcondicionpago").selectpicker('refresh');
        $("#plazopago").val(data.plazopago);
        $("#observacion").val(data.observacion);

        if (data.servicioflete == 1) {
            $("#servicioflete").attr("checked", true);
            $('#servicioflete').trigger('click');
        } else {
            $("#servicioflete").attr("checked", false);
        }
        if (data.condicion == 1) {
            $("#condicion").attr("checked", true);
            $('#condicion').trigger('click');
        } else {
            $("#condicion").attr("checked", false);
        }
    });
}

function ficha(idproveedor) {
    $.post("../ajax/proveedor.php?op=ficha", {"idproveedor": idproveedor}, function (data) {
        data = JSON.parse(data);
        mostrarficha(true);
        $("#idproveedor1").val(data.idproveedor);
        $("#rut1").html(data.rut);
        $("#razonsocial1").html(data.razonsocial);
        $("#razonsocial2").html(data.razonsocial);
        $("#categoria1").html(data.categoriastr);
        $("#condicion1").html(data.condpagostr);
        $("#region1").html(data.regionstr);
        $("#comuna1").html(data.comunastr);
        $("#direccion1").html(data.direccion);
        $("#plazopago1").html(data.plazopago);
        $("#contacto1").html(data.contacto);
        $("#telefono1").html(data.fono);
        $("#email1").html(data.email);
        $("#flete1").html(data.servicioflete == 1 ? "SI" : "NO");
        $("#observacion1").html(data.observacion);

        listarejecutivo(data.idproveedor);
        listardocumentos(idproveedor);
        listarCompras(idproveedor);
    });

}

function guardarcat() {
    var formData = new FormData();
    formData.append("nombre", $("#nombrecat").val());
    formData.append("idcategoria", $("#idcategoriacat").val());
    formData.append("condicion", $("#condicioncat").val());

    $.ajax({
        url: '../ajax/categoria_proveedor.php?op=guardaryeditar',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,

        success: function (datos) {
            //bootbox.alert(datos);
            $(".close").trigger('click');
            if (datos != '0') {
                $("#btnGuardar").prop("disabled", false);
                new PNotify({
                    title: 'Correcto!',
                    text: 'Categoria guardada con exito.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
                $.post("../ajax/categoria_proveedor.php?op=selectcategoria", function (r) {
                    $("#idcategoria").html(r);
                    $("#idcategoria").val(datos);
                    $("#idcategoria").selectpicker('refresh');
                });
            } else {
                $("#btnGuardar").prop("disabled", true);
                new PNotify({
                    title: 'Error!',
                    text: 'Categoria no guardada.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
        }
    });

    $("#idcategoriacat").val("");
    $("#nombrecat").val("");
    $("#condicioncat").prop("checked", false);
}

function guardarcond() {
    var formData = new FormData();
    formData.append("nombre", $("#nombrecond").val());
    formData.append("idcategoria", $("#idcondicionpagocond").val());
    formData.append("condicion", $("#condicioncond").val());

    $.ajax({
        url: '../ajax/condicionpago.php?op=guardaryeditar',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,

        success: function (datos) {
            //bootbox.alert(datos);
            $(".close").trigger('click');
            if (datos != '0') {
                $("#btnGuardar").prop("disabled", false);
                new PNotify({
                    title: 'Correcto!',
                    text: 'Categoria guardada con exito.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
                $.post("../ajax/condicionpago.php?op=selectcondicion", function (r) {
                    $("#idcondicionpago").html(r);
                    $("#idcondicionpago").val(datos);
                    $("#idcondicionpago").selectpicker('refresh');
                });
            } else {
                $("#btnGuardar").prop("disabled", true);
                new PNotify({
                    title: 'Error!',
                    text: 'Categoria no guardada.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
        }
    });

    $("#idcondicionpagocond").val("");
    $("#nombrecond").val("");
    $("#condicioncond").prop("checked", false);
}

/*
 * EJECUTIVO COMERCIAL
 */
function listarejecutivo(idproveedor) {
    tablaejecutivo = $('#tblejecutivo').dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Bfrtip',
        buttons: Botones,
        "language": Español,
        "ajax": {
            url: '../ajax/ejecutivocomercial.php?op=listar',
            data: {'idproveedor': idproveedor},
            type: "get",
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 10, //Paginacion 10 items
        "order": [[1, "desc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}

function guardarEjecutivo() {
    var formData = new FormData();
    formData.append("idejecutivo", $("#ejecutivo_id").val());
    formData.append("nombre", $("#ejecutivo_nombre").val());
    formData.append("apellido", $("#ejecutivo_apellido").val());
    formData.append("email", $("#ejecutivo_email").val());
    formData.append("telefono", $("#ejecutivo_fono").val());
    formData.append("idproveedor", $("#idproveedor1").val());
    formData.append("condicion", 1);

    $.ajax({
        url: '../ajax/ejecutivocomercial.php?op=guardaryeditar',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,

        success: function (datos) {
            //bootbox.alert(datos);
            if (datos != '0') {
                $("#btnGuardar").prop("disabled", false);
                new PNotify({
                    title: 'Correcto!',
                    text: 'Ejecutivo guardado con exito.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
            } else {
                $("#btnGuardar").prop("disabled", true);
                new PNotify({
                    title: 'Error!',
                    text: 'Ejecutivo no guardad0.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
            $(".close").click();
            tablaejecutivo.ajax.reload();
            limpiarejecutivo();
        }
    });
}

function limpiarejecutivo() {
    $("#ejecutivo_id").val("");
    $("#ejecutivo_nombre").val("");
    $("#ejecutivo_apellido").val("");
    $("#ejecutivo_email").val("");
    $("#ejecutivo_fono").val("");
    $("#idproveedor1").val("");
}

function delejecutivo(idejecutivo) {
    var formData = new FormData();
    formData.append("idejecutivo", idejecutivo);
    $.ajax({
        url: '../ajax/ejecutivocomercial.php?op=eliminar',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,

        success: function (datos) {
            if (datos != '0') {
                new PNotify({
                    title: 'Correcto!',
                    text: 'Eliminado con exito.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
            } else {
                new PNotify({
                    title: 'Error!',
                    text: 'No eliminado',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
            tablaejecutivo.ajax.reload();
        }

    });
}

/*
 * DOCUMENTOS
 */

function listardocumentos(idproveedor) {

    $.post("../ajax/documento.php?op=listar", {"idproveedor": idproveedor}, function (data) {
        data = JSON.parse(data);
        $("#documentoslista").empty();
        for (i = 0; i < data.length; i++) {
            var nomb = "'" + data[i]["3"] + "'";
            $("#documentoslista").append(
                    '<li class="media event col-md-4">'
                    + '<a class="pull-left border-aero profile_thumb"><i class="fa fa-file-pdf-o red"></i></a>'
                    + '<div class="media-body">'
                    + '<a class="title" href="#">' + (data[i]["3"]) + '</a>'
                    + '<p>' + data[i]["1"] + ' </p>'
                    + '<p class="pull-right" onclick="deldocto(' + data[i]["0"] + ', ' + data[i]["2"] + ', ' + nomb + ')"><i class="fa fa-trash red"></i> <small>ELIMINAR</small></p>'
                    + '</div>'
                    + '</li>'
                    );
        }
    });
}

function guardarDocumento() {
    var formData = new FormData($("#formdocto")[0]);
    var idproveedor = $("#idproveedor1").val();
    formData.append("idproveedor", idproveedor);
    formData.append("condicion", 1);

    $.ajax({
        url: '../ajax/documento.php?op=guardaryeditar',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,

        success: function (datos) {
            //bootbox.alert(datos);
            if (isNaN(datos)) {
                new PNotify({
                    title: 'Error!',
                    text: 'Documento no guardado. ' + datos,
                    type: 'error',
                    styling: 'bootstrap3'
                });
            } else if (datos != '0') {
                $("#btnGuardar").prop("disabled", false);
                new PNotify({
                    title: 'Correcto!',
                    text: 'Documento guardado con exito.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
            } else {
                $("#btnGuardar").prop("disabled", true);
                new PNotify({
                    title: 'Error!',
                    text: 'Documento no guardada.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
            $(".close").click();
            limpiardocumento();
            listardocumentos(idproveedor);
        }
    });
}

function limpiardocumento() {
    $("#iddocumento").val("");
    $("#descripcion").val("");
    $("#docto").val("");
}

function deldocto(iddocumento, idproveedor, nombre) {
    bootbox.confirm("Va a eliminar este archivo, ¿esta seguro?", function (result) {
        if (result) {
            var formData = new FormData();
            formData.append("iddocumento", iddocumento);
            formData.append("idproveedor", idproveedor);
            formData.append("nombre", nombre);
            
            $.ajax({
                url: '../ajax/documento.php?op=eliminar',
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,

                success: function (datos) {
                    if (datos != '0') {
                        new PNotify({
                            title: 'Correcto!',
                            text: 'Eliminado con exito.',
                            type: 'success',
                            styling: 'bootstrap3'
                        });
                    } else {
                        new PNotify({
                            title: 'Error!',
                            text: 'No eliminado',
                            type: 'error',
                            styling: 'bootstrap3'
                        });
                    }
                    listardocumentos(idproveedor);
                }

            });
        }
    });

}

/*
 * COMPRAS
 */
function listarCompras(idproveedor) {
    tablacompras = $('#tblcompra').dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Bfrtip',
        buttons: Botones,
        "language": Español,
        "ajax": {
            url: '../ajax/compras.php?op=listar',
            data: {'idproveedor': idproveedor},
            type: "get",
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 10, //Paginacion 10 items
        "order": [[1, "desc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}

function guardarCompra() {
    var formData = new FormData();
    var idproveedor = $("#idproveedor1").val();
    formData.append("idcompra", $("#idcompra").val());
    formData.append("ordencompra", $("#ordencompra").val());
    formData.append("presupuesto", $("#presupuesto").val());
    formData.append("factura", $("#factura").val());
    formData.append("flete", $("#felete").val());
    formData.append("buy_time", $("#buy_time").val());
    formData.append("reception_time", $("#reception_time").val());
    formData.append("evaluacion", $("#evaluacion").val());
    formData.append("idproveedor", idproveedor);
    formData.append("condicion", 1);

    $.ajax({
        url: '../ajax/compras.php?op=guardaryeditar',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,

        success: function (datos) {
            //bootbox.alert(datos);
            if (datos != '0') {
                $("#btnGuardar").prop("disabled", false);
                new PNotify({
                    title: 'Correcto!',
                    text: 'Compra guardada con exito.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
            } else {
                $("#btnGuardar").prop("disabled", true);
                new PNotify({
                    title: 'Error!',
                    text: 'Compra no guardada.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
            $(".close").click();
            tablacompras.ajax.reload();
            limpiarcompra();
        }
    });
}

function limpiarcompra() {
    $("#idcompra").val("");
    $("#ordencompra").val("");
    $("#presupuesto").val("");
    $("#factura").val("");
    $("#felete").val("");
    $("#buy_time").val("");
    $("#reception_time").val("");
    $("#evaluacion").val("");
}

function delcompra(idcompra){
    bootbox.confirm("Va a eliminar esta compra ¿esta seguro?", function (result) {
        if (result) {
            var formData = new FormData();
            formData.append("idcompra", idcompra);
            
            $.ajax({
                url: '../ajax/compras.php?op=eliminar',
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,

                success: function (datos) {
                    if (datos != '0') {
                        new PNotify({
                            title: 'Correcto!',
                            text: 'Eliminado con exito.',
                            type: 'success',
                            styling: 'bootstrap3'
                        });
                    } else {
                        new PNotify({
                            title: 'Error!',
                            text: 'No eliminado',
                            type: 'error',
                            styling: 'bootstrap3'
                        });
                    }
                    tablacompras.ajax.reload();
                }

            });
        }
    });
}

init();

