var tabla;

//funcion que se ejecuta iniciando
function init() {
    mostarform(false);
    listar();

    $("#formulario").on("submit", function (e) {
        guardaryeditar(e);
    });

    $.post("../ajax/centrocosto.php?op=selecttipo", function (r) {
        $("#tcentrocosto").html(r);
        $("#tcentrocosto").selectpicker('refresh');
    });

}


// Otras funciones
function limpiar() {

    $("#idcentrocosto").val("");
    $("#tcentrocosto").val(0);
    $("#tcentrocosto").selectpicker('refresh');
    $("#codigo").val("");
    $("#codexterno").val("");
    $("#nombre").val("");
    $("#condicion").prop("checked", false);

}

function mostarform(flag) {
    limpiar();
    if (flag) {
        $("#listadocentros").hide();
        $("#formulariocentros").show();
        $("#op_agregar").hide();
        $("#op_listar").show();
        $("#btnGuardar").prop("disabled", false);

    } else {
        $("#listadocentros").show();
        $("#formulariocentros").hide();
        $("#op_agregar").show();
        $("#op_listar").hide();
    }
}

function cancelarform() {
    limpiar();
    mostarform(false);
}

function listar() {
    tabla = $('#tblcentros').dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Bfrtip',
        buttons: Botones,
        "language": Español,
        "ajax": {
            url: '../ajax/centrocosto.php?op=listarcentro',
            type: "get",
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 10, //Paginacion 10 items
        "order": [[1, "desc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}

function guardaryeditar(e) {
    e.preventDefault();
    $("#btnGuardar").prop("disabled", true);
    var formData = new FormData($("#formulario")[0]);
    $.ajax({
        url: '../ajax/centrocosto.php?op=cguardaryeditar',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,

        success: function (datos) {
            //bootbox.alert(datos);
            if (datos != '0') {
                $("#btnGuardar").prop("disabled", false);
                new PNotify({
                    title: 'Correcto!',
                    text: 'Centro de costo guardado con exito.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
            } else {
                $("#btnGuardar").prop("disabled", true);
                new PNotify({
                    title: 'Error!',
                    text: 'Centro de costo no guardado.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
            mostarform(false);
            tabla.ajax.reload();
        }
    });
    limpiar();
}

function mostar(idcentrocosto) {
    $.post("../ajax/centrocosto.php?op=mostar", {idcentrocosto: idcentrocosto}, function (data, status) {
        data = JSON.parse(data);
        mostarform(true);

        $("#idcentrocosto").val(data.idcentrocosto);
        $("#tcentrocosto").val(data.tcentrocosto);
        $("#tcentrocosto").selectpicker('refresh');
        $("#codigo").val(data.codigo);
        $("#codexterno").val(data.codexterno);
        $("#nombre").val(data.nombre);

        if (data.condicion == 1) {
            $("#condicion").val(1);
        } else {
            $("#condicion").val(0);
        }
    });
}

function desactivar(idcentrocosto) {
    bootbox.confirm("Esta seguro que quiere inhabilitar este centro de costo??", function (result) {
        if (result) {
            $.post("../ajax/centrocosto.php?op=desactivar", {idcentrocosto: idcentrocosto}, function (e) {
                new PNotify({
                    title: 'Correcto!',
                    text: e,
                    type: 'success',
                    styling: 'bootstrap3'
                });
                tabla.ajax.reload();
            });
        }
    });
}

function activar(idcentrocosto) {

    bootbox.confirm("Esta seguro que quiere habilitar este centro de costo?", function (result) {
        if (result) {
            $.post("../ajax/centrocosto.php?op=activar", {idcentrocosto: idcentrocosto}, function (e) {
                new PNotify({
                    title: 'Correcto!',
                    text: e,
                    type: 'success',
                    styling: 'bootstrap3'
                });
                tabla.ajax.reload();
            });
        }
    });
}

/*
 //funcion que se ejecuta iniciando
 function init(){
 mostarform(false);
 listar();
 
 $("#formulario").on("submit", function(e){
 guardaryeditar(e);
 })
 
 $.post("../ajax/centrocosto.php?op=selecttipo", function(r){
 $("#id_tipo_centro_costo").html(r);
 $("#id_tipo_centro_costo").selectpicker('refresh');
 });
 
 }
 
 // Otras funciones
 function limpiar(){
 
 $("#idcentro_costo").val("");
 $("#id_tipo_centro_costo").val("");
 $("#id_tipo_centro_costo").selectpicker('refresh');	
 $("#numero_centro").val("");
 $("#descripcion").val("");
 
 }
 
 function mostarform(flag){
 
 limpiar();
 if(flag){
 $("#listadocentros").hide();
 $("#formulariocentros").show();
 $("#op_agregar").hide();
 $("#op_listar").show();
 $("#btnGuardar").prop("disabled", false);
 
 }else{
 $("#listadocentros").show();
 $("#formulariocentros").hide();
 $("#op_agregar").show();
 $("#op_listar").hide();
 }
 
 }
 
 function cancelarform(){
 limpiar();
 mostarform(false);
 }
 
 function listar(){
 tabla=$('#tblcentros').dataTable({
 "aProcessing":true,
 "aServerSide": true,
 dom: 'Bfrtip',
 buttons:[
 'copyHtml5',
 'print',
 'excelHtml5',
 'csvHtml5',
 'pdf'
 ],
 "ajax":{
 url:'../ajax/centrocosto.php?op=listarcentro',
 type:"get",
 dataType:"json",
 error: function(e){
 console.log(e.responseText);
 }
 },
 "bDestroy": true,
 "iDisplayLength": 10, //Paginacion 10 items
 "order" : [[1 , "desc"]] //Ordenar en base a la columna 0 descendente
 }).DataTable();
 }
 
 function guardaryeditar(e){
 e.preventDefault();
 $("#btnGuardar").prop("disabled", true);
 var formData = new FormData($("#formulario")[0]);
 $.ajax({
 url:'../ajax/equipo.php?op=guardaryeditar',
 type:"POST",
 data:formData,
 contentType: false,
 processData:false,
 
 success: function(datos){
 bootbox.alert(datos);
 mostarform(false);
 tabla.ajax.reload();
 }
 });
 limpiar();
 }
 
 function mostar(idequipo){
 $.post("../ajax/equipo.php?op=mostar",{idequipo:idequipo}, function(data,status){
 data = JSON.parse(data);
 mostarform(true);
 
 $("#idequipo").val(data.idequipo);
 $("#idpequipo").val(data.idpropietario_equipo);
 $("#idpequipo").selectpicker('refresh');
 $("#marca").val(data.marca);
 $("#modelo").val(data.modelo);
 $("#ano").val(data.ano);
 $("#codigo").val(data.codigo);
 
 })
 }
 
 function desactivar(idequipo){
 
 bootbox.confirm("Esta seguro que quiere inhabilitar el avion?", function(result){
 if(result){
 $.post("../ajax/equipo.php?op=desactivar",{idequipo:idequipo}, function(e){
 bootbox.alert(e);
 tabla.ajax.reload();
 })	
 }
 })
 }
 
 function activar(idequipo){
 
 bootbox.confirm("Esta seguro que quiere habilitar el avion?", function(result){
 if(result){
 $.post("../ajax/equipo.php?op=activar",{idequipo:idequipo}, function(e){
 bootbox.alert(e);
 tabla.ajax.reload();
 })	
 }
 })
 }*/


init();