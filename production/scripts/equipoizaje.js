var tabla;

//funcion que se ejecuta iniciando
function init() {
    mostrarform(false);
    listar();
    
    $.post("../ajax/marca_izaje.php?op=selectmarca", function(e){
        $("#idmarca").html(e);
        $("#idmarca").selectpicker('refresh');
    });
    
    $("#idmarca").on('change', function(){
        $.post("../ajax/modelo_izaje.php?op=selectmodelo",{"idmarca": $("#idmarca").val()}, function(e){
            $("#idmodelo").html(e);
            $("#idmodelo").selectpicker('refresh');
        });
    });
    
    
    $("#formulario").on("submit", function (e) {
        guardaryeditar(e);
    });
}


// Otras funciones
function limpiar() {
    $("#numero").val("");
    $("#idequipo").val("");
    $("#serie").val("");
    $("#idmarca").val("");
    $("#idmarca").selectpicker('refresh');
    $("#idmodelo").val("");
    $("#idmodelo").selectpicker('refresh');
    $("#capacidad").val("");
    $("#velocidad").val("");
    $("#estado").val("");
    $("#estado").selectpicker('refresh');
}

function mostrarform(flag) {

    limpiar();
    if (flag) {
        $("#listadoequiposizaje").hide();
        $("#formularioequiposizaje").show();
        $("#op_agregar").hide();
        $("#op_listar").show();
        $("#btnGuardar").prop("disabled", false);

    } else {
        $("#listadoequiposizaje").show();
        $("#formularioequiposizaje").hide();
        $("#op_agregar").show();
        $("#op_listar").hide();
    }

}

function cancelarform() {
    limpiar();
    mostrarform(false);
}

function listar() {
    tabla = $('#tblequiposizaje').dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Bfrtip',
        buttons: Botones,
        "language": Español,
        "ajax": {
            url: '../ajax/equipos_izaje.php?op=listar',
            type: "get",
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 10, //Paginacion 10 items
        "order": [[0, "desc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}

function guardaryeditar(e) {
    e.preventDefault();
    $("#btnGuardar").prop("disabled", true);
    var formData = new FormData($("#formulario")[0]);

    $.ajax({
        url: '../ajax/equipos_izaje.php?op=guardaryeditar',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,

        success: function (datos) {
            if(datos != 0){
                new PNotify({
                    title: 'Correcto!',
                    text: 'guardada con exito.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
            }else{
                new PNotify({
                    title: 'Error!',
                    text: 'no guardada.' + datos,
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
            mostrarform(false);
            tabla.ajax.reload();
        }
    });
    limpiar();
}

function mostrar(idequipo) {
    $.post("../ajax/equipos_izaje.php?op=mostrar", {idequipo: idequipo}, function (data, status) {
        data = JSON.parse(data);
        mostrarform(true);
        
        $("#numero").val(data.numero);
        $("#idequipo").val(data.idequipo);
        $("#serie").val(data.serie);
        $("#idmarca").val(data.idmarca);
        $("#idmarca").selectpicker('refresh');
        
        $.post("../ajax/modelo_izaje.php?op=selectmodelo",{"idmarca": data.idmarca}, function(e){
            $("#idmodelo").html(e);
            $("#idmodelo").val(data.idmodelo);
            $("#idmodelo").selectpicker('refresh');
        });
        
        //$("#idmodelo").selectpicker('refresh');
        $("#capacidad").val(data.capacidad);
        $("#velocidad").val(data.velocidad);
        $("#estado").val(data.estado);
        $("#estado").selectpicker('refresh');
    });
}

init();