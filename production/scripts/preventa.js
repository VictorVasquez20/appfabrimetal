var tabla;
var tablaboleta;
var tablaNoSup;
var tablaSI;
var tablaSN;
var tablaAscensores;
var tblinstalacion;
var tablamandante;

var countboleta = 0;
var counAscensor = 0;
var countSI = 0;
var countSN = 0;
var countinstalacion = 0;
var countinfo = 0;

var sumSI = 0;
var sumSN = 0;
var sumSIAscensor = 0;
var sumSNAscensor = 0;

var finalizar = false;
var selectInstalaciones = "";

//funcion que se ejecuta iniciando
function init() {
    mostrarform(false);
    listar();

    $('[data-toggle="tooltip"]').tooltip();

    //VENTA
    $("#formularioVenta").on("submit", function (e) {
        grabaVenta(e);
    });
    //GARANTIA
    $("#formularioGarantia").on("submit", function (e) {
        e.preventDefault();
        //grabaGarantia(e);
        agregarGarantia();
    });
    //PROYECTO
    $("#formproyecto").on("submit", function (e) {
        grabaProyecto(e);
    });
    //ASCENSOR
    $("#formAscensor").on("submit", function (e) {
        e.preventDefault();
        agregarAscensor();
    });
    //IMPORTACION
    $("#formularioimp").on("submit", function (e) {
        agregaIMP(e);
    });
    //CENTRO COSTO
    $("#formularioCC").on("submit", function (e) {
        agregaCC(e);
    });
    //PAGOS
    $("#formpago").on("submit", function (e) {
        grabaPago(e);
    });
    //AGRRGA CLIENTE
    $('#formcliente').on("submit", function (event) {
        event.preventDefault();
        guardarcliente();
    });

    $("#rut").focusout(function () {
        rut = $("#rut").val();
        razon = getrazonsocial(rut);
        if (razon === "**") {
            $("#razon").val("");
            $("#razon").attr("readOnly", false);
        } else if (razon === "***") {
            new PNotify({
                title: 'Error!',
                text: 'Rut incorrecto.',
                type: 'error',
                styling: 'bootstrap3'
            });
            $("#rut").focus();
            $("#razon").val("");
            $("#razon").attr("readOnly", true);
        } else {
            $("#razon").val(razon);
            $("#razon").attr("readOnly", true);
        }
    });

    $('#venta_fecha').datetimepicker({format: 'DD-MM-YYYY'});
    $('#venta_fecedifica').datetimepicker({format: 'DD-MM-YYYY'});
    $('#venta_valganticipo').datetimepicker({format: 'DD-MM-YYYY'});
    $('#venta_valgcumplimiento').datetimepicker({format: 'DD-MM-YYYY'});
    $('#garantia_validez').datetimepicker({format: 'DD-MM-YYYY'});


    $.post("../ajax/mandante.php?op=selectmandante", function (r) {
        $("#venta_idmandante").html(r);
        $("#venta_idmandante").selectpicker('refresh');
    });

    //GARANTIAS
    $.get("../json/comercial.json", function (json) {
        desc = json.DescripcionBoleta;
        $("#garantia_descripcion").append("<option value='' selected disabled>Seleccione una opción</option>")
        for (i = 0; i < desc.length; i++) {
            $("#garantia_descripcion").append("<option value='" + desc[i].valor + "'>" + desc[i].nombre + "</option>");
        }
        $("#garantia_descripcion").selectpicker('refresh');
    });

    $.get("../json/comercial.json", function (json) {
        docto = json.DocumentoBoleta;
        $("#garantia_documento").append("<option value='' selected disabled>Seleccione una opción</option>")
        for (i = 0; i < docto.length; i++) {
            $("#garantia_documento").append("<option value='" + docto[i].valor + "'>" + docto[i].nombre + "</option>");
        }
        $("#garantia_documento").selectpicker('refresh');
    });

    $.get("../json/comercial.json", function (json) {
        pago = json.PagoBoleta;
        $("#garantia_tipo").append("<option value='' selected disabled>Seleccione una opción</option>")
        for (i = 0; i < pago.length; i++) {
            $("#garantia_tipo").append("<option value='" + pago[i].valor + "'>" + pago[i].nombre + "</option>");
        }
        $("#garantia_tipo").selectpicker('refresh');
    });

    //PROYECTO
    $.post("../ajax/regiones.php?op=selectRegiones", function (r) {
        $("#idregionescli").html(r);
        $("#idregionescli").selectpicker('refresh');

        $("#proyecto_region").html(r);
        $("#proyecto_region").selectpicker('refresh');
    });

    $("#idregionescli").on("change", function (e) {
        $.get("../ajax/comunas.php?op=selectComunasReg", {id: $("#idregionescli").val()}, function (r) {
            $("#idcomunascli").html(r);
            $("#idcomunascli").selectpicker('refresh');
        });
    });

    $("#proyecto_region").on("change", function (e) {
        $.get("../ajax/comunas.php?op=selectComunasReg", {id: $("#proyecto_region").val()}, function (r) {
            $("#proyecto_comuna").html(r);
            $("#proyecto_comuna").selectpicker('refresh');
        });
    });


    $.post("../ajax/tsegmento.php?op=selecttsegmento", function (r) {
        $("#proyecto_idtsegmento").html(r);
        $("#proyecto_idtsegmento").selectpicker('refresh');
    });

    $.post("../ajax/clasificacion_proyecto.php?op=selectclasificacionpro", function (r) {
        $("#proyecto_idtclasificacion").html(r);
        $("#proyecto_idtclasificacion").selectpicker('refresh');
    });

    //ASCENSORES
    $.get("../json/comercial.json", function (json) {
        comando = json.ComandoAscensores;
        $("#ascensor_comando").append("<option value='' selected disabled>Seleccione una opción</option>")
        for (i = 0; i < comando.length; i++) {
            $("#ascensor_comando").append("<option value='" + comando[i].valor + "'>" + comando[i].nombre + "</option>");
        }
        $("#ascensor_comando").selectpicker('refresh');
    });

    $.post("../ajax/tascensor.php?op=selecttascensor", function (r) {
        $("#ascensor_idtascensor").html(r);
        $("#ascensor_idtascensor").selectpicker('refresh');
    });

    $.post("../ajax/marca.php?op=selectmarca", function (r) {
        $("#ascensor_marca").html(r);
        $("#ascensor_marca").selectpicker('refresh');
    });

    $("#ascensor_marca").on("change", function (e) {
        $.get("../ajax/modelo.php?op=selectmodelo", {id: $('#ascensor_marca').val()}, function (r) {
            $("#ascensor_modelo").html(r);
            $("#ascensor_modelo").selectpicker('refresh');
        });
    });


    $.post("../ajax/moneda.php?op=selectmoneda", {idtformapago: 1}, function (r) {
        $("#venta_monedaSI").html(r);
        $("#venta_monedaSI").selectpicker('refresh');
    });
    $.post("../ajax/moneda.php?op=selectmoneda", {idtformapago: 2}, function (r) {
        $("#venta_monedaSN").html(r);
        $("#venta_monedaSN").selectpicker('refresh');
    });

    //VERIFICO QUE CDIGO DE VENTA NO SE REPITA
    $("#venta_codigo").on("focusout", function () {
        if ($("#venta_codigo").val() != "") {
            var valor = $("#venta_codigo").val();
            if (valor.length <= 6) {
                if (valor.includes('_')) {
                    $("#addascensor").attr('disabled', true);
                    new PNotify({
                        title: 'Error!',
                        text: 'El N° Venta debe tener 6 caracteres, incluido "/" ',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                    $("#btnGuardar").prop("disabled", true);
                } else {
                    $.post("../ajax/venta.php?op=validarventa", {codigo: $("#venta_codigo").val()}, function (data) {
                        data = JSON.parse(data);
                        if (data.existe > 0) {
                            new PNotify({
                                title: 'OPS!',
                                text: 'Esta venta ya existe.',
                                type: 'error',
                                styling: 'bootstrap3'
                            });
                            $("#btnGuardar").prop("disabled", true);
                        } else {
                            $("#btnGuardar").prop("disabled", false);
                        }
                    });
                }
            }
        }
    });

    $.post("../ajax/ascensor.php?op=selectascensores", function (r) {
        $("#listaAsc").html(r);
        $("#listaAsc").selectpicker('refresh');
    });

    $.post("../ajax/centrocosto.php?op=selectcentro", function (r) {
        $("#venta_cc").html(r);
        $("#venta_cc").selectpicker('refresh');
    });


    $.post("../ajax/centrocosto.php?op=selecttipo", function (r) {
        $("#tcentrocosto").html(r);
        $("#tcentrocosto").selectpicker('refresh');
    });

    $.post("../ajax/importacion.php?op=selectimportacion", function (r) {
        $("#venta_pedido").html(r);
        $("#venta_pedido").selectpicker('refresh');
    });



    //PAGOS
    $.get("../json/comercial.json", function (json) {
        pagos = json.pagointernacional;
        $("#si_desc").append("<option value='' selected disabled>Seleccione una opción</option>")
        for (i = 0; i < pagos.length; i++) {
            $("#si_desc").append("<option value='" + pagos[i].valor + "'>" + pagos[i].nombre + "</option>");
        }
        $("#si_desc").selectpicker('refresh');
    });

    $.get("../json/comercial.json", function (json) {
        pagos = json.EtapasInstalacion;
        $("#sn_desc").append("<option value='' selected disabled>Seleccione una opción</option>")
        for (i = 0; i < pagos.length; i++) {
            selectInstalaciones += "<option value='" + pagos[i].valor + "'>" + pagos[i].nombre + "</option>";
            $("#sn_desc").append("<option value='" + pagos[i].valor + "'>" + pagos[i].nombre + "</option>");
        }
        $("#sn_desc").selectpicker('refresh');
    });

    $("#nrep").on("change", function (e) {
        $('#replegal').empty();
        for (var i = 0; i < $('#nrep').val(); i++) {
            var formrep = '<div id="replegal' + i + '" name="replegal' + i + '">' +
                    '<h5>Representante: ' + (i + 1) + '</h5>' +
                    '<div class="col-md-12 col-sm-12 col-xs-12 form-group">' +
                    '<label for="nombre_rep' + i + '">Nombre y apellido<span class="required">*</span></label>' +
                    '<input type="text" id="nombre_rep' + i + '" name="nombre_rep' + i + '" required="required" class="form-control" style="text-transform:uppercase;">' +
                    '</div>' +
                    '<div class="col-md-12 col-sm-12 col-xs-12 form-group">' +
                    '<label for="numero_conedi' + i + '">Rut<span class="required">*</span></label>' +
                    '<input type="text" id="rut_rep' + i + '" name="rut_rep' + i + '" required="required" class="form-control"/>' +
                    '</div>' +
                    '<div class="col-md-6 col-sm-12 col-xs-12 form-group">' +
                    '<label for="email_conedi' + i + '">Correo electrónico</label>' +
                    '<input type="text" id="email_rep' + i + '" name="email_rep' + i + '" class="form-control" style="text-transform:uppercase;">' +
                    '</div>' +
                    '<div class="col-md-6 col-sm-12 col-xs-12 form-group">' +
                    '<label for="fono_rep' + i + '">Teléfono</label>' +
                    '<input type="text" id="fono_rep' + i + '" name="fono_rep' + i + '" class="form-control">' +
                    '</div>' +
                    '</div>';
            $('#replegal').append(formrep);
            $("#rut_rep" + i).inputmask({"mask": "99.999.999-*"});
            $("#fono_rep" + i).inputmask({"mask": "+56(9)9999-9999"});
        }
    });

    tablaboleta = $("#tblboletas").DataTable({
        "paging": false,
        "info": false,
        "bFilter": false,
        "bAutoWidth": false,
        "ordering": false
    });

    tablaAscensores = $("#tblascensores").DataTable({
        "paging": false,
        "info": false,
        "bFilter": false,
        "bAutoWidth": false,
        "ordering": false
    });
    tablaSI = $("#tblaSI").DataTable({
        "paging": false,
        "info": false,
        "bFilter": false,
        "bAutoWidth": false,
        "ordering": false
    });
    tablaSN = $("#tblaSN").DataTable({
        "paging": false,
        "info": false,
        "bFilter": false,
        "bAutoWidth": false,
        "ordering": false
    });

    tblinstalacion = $("#tblinstalacion").DataTable({
        "paging": false,
        "info": false,
        "bFilter": false,
        "bAutoWidth": false,
        "ordering": false
    });

    tablamandante = $("#informacionrepresentantes").DataTable({
        "paging": false,
        "info": false,
        "bFilter": false,
        "bAutoWidth": false,
        "ordering": false
    });
}

$("#venta_tipo").change(function () {
    if ($(this).val() !== 1) {
        $("#cartera").show();
    } else {
        $("#cartera").hide();
    }
});


//VALIDO QUE CODIGO FM TENGA EL FORMATO CORRECTO Y QUE NO EXISTA EN LA BASE DE DATOS
$("#ascensor_codigo").focusout(function () {
    var valor = $("#ascensor_codigo").val();

    if (valor.length <= 8) {
        if (valor.includes('_')) {
            $("#addascensor").attr('disabled', true);
            new PNotify({
                title: 'Error!',
                text: 'El codigo debe tener 8 caracteres, incluidas las siglas "FM"',
                type: 'error',
                styling: 'bootstrap3'
            });

        } else {
            $.post("../ajax/ascensor.php?op=VerificarCodigo", {codigo: $("#ascensor_codigo").val()}, function (data) {
                data = JSON.parse(data);
                if (data == 1) {
                    new PNotify({
                        title: 'Error!',
                        text: 'El codigo FM ingresado ya existe!',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                    $("#addascensor").attr('disabled', true);
                } else {
                    $("#addascensor").attr('disabled', false);
                }
            });
        }
    } else {
        $.post("../ajax/ascensor.php?op=VerificarCodigo", {codigo: $("#ascensor_codigo").val()}, function (data) {
            data = JSON.parse(data);
            if (data == 1) {
                new PNotify({
                    title: 'Error!',
                    text: 'El codigo FM ingresado ya existe!',
                    type: 'error',
                    styling: 'bootstrap3'
                });
                $("#addascensor").attr('disabled', true);
            } else {
                $("#addascensor").attr('disabled', false);
            }
        });
    }
});


//VALIDO EL KEN TENGA EL FORMATO CORRECTO Y QUE NO EXISTA EN LA BASE DE DATOS
$("#ascensor_ken").focusout(function () {
    var valor = $("#ascensor_ken").val();

    if (valor.length <= 8) {
        if (valor.includes('_')) {
            $("#addascensor").attr('disabled', true);
            new PNotify({
                title: 'Error!',
                text: 'El KEN debe tener 8 caracteres.',
                type: 'error',
                styling: 'bootstrap3'
            });

        } else {
            $.post("../ajax/ascensor.php?op=VerificarKEN", {ken: $("#ascensor_ken").val()}, function (data) {
                data = JSON.parse(data);
                if (data == 1) {
                    new PNotify({
                        title: 'Error!',
                        text: 'El KEN ingresado ya existe!',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                    $("#addascensor").attr('disabled', true);
                } else {
                    $("#addascensor").attr('disabled', false);
                }
            });
        }
    } else {
        $.post("../ajax/ascensor.php?op=VerificarKEN", {ken: $("#ascensor_ken").val()}, function (data) {
            data = JSON.parse(data);
            if (data == 1) {
                new PNotify({
                    title: 'Error!',
                    text: 'El codigo KEN ya existe!',
                    type: 'error',
                    styling: 'bootstrap3'
                });
                $("#addascensor").attr('disabled', true);
            } else {
                $("#addascensor").attr('disabled', false);
            }
        });
    }
});


$("#op_adicionales").change(function () {
    if ($(this).prop("checked")) {
        $("#adi").show();
    } else {
        $("#adi").hide();
    }
});

$("#op_garantia").change(function () {
    if ($(this).prop("checked")) {
        $("#venta_garantiaex").val(1);
        $("#meses_garantia").show();
    } else {
        $("#venta_garantiaex").val(0)
        $("#meses_garantia").hide();
    }
});

$("#op_mantencion").change(function () {
    if ($(this).prop("checked")) {
        $("#venta_mantencionpost").val(1);
        $("#meses_mantencion").show();
    } else {
        $("#venta_mantencionpost").val(0);
        $("#meses_mantencion").hide();
    }
});

$("#venta_cc").change(function () {
    $.post("../ajax/centrocosto.php?op=mostar", {idcentrocosto: $("#venta_cc").val()}, function (data) {
        data = JSON.parse(data);
        $("#proyecto_codigo").val(data.codigo);
    });
});

$("#venta_ganticipo").change(function () {
    if ($(this).prop("checked")) {
        $("#venta_tpganticipo").attr("disabled", false);
        $("#venta_tpganticipo").selectpicker('refresh');
        $("#venta_valganticipo").attr("disabled", false);
    } else {
        $("#venta_tpganticipo").attr("disabled", true);
        $("#venta_tpganticipo").selectpicker('refresh');
        $("#venta_valganticipo").attr("disabled", true);
    }
});

$("#venta_gcumplimiento").change(function () {
    if ($(this).prop("checked")) {
        $("#venta_tpgcumplimiento").attr("disabled", false);
        $("#venta_tpgcumplimiento").selectpicker('refresh');
        $("#venta_valgcumplimiento").attr("disabled", false);
    } else {
        $("#venta_tpgcumplimiento").attr("disabled", true);
        $("#venta_tpgcumplimiento").selectpicker('refresh');
        $("#venta_valgcumplimiento").attr("disabled", true);
    }
});

$("#venta_cartera").change(function () {
    if ($(this).prop("checked")) {
        $("#nuevo").hide();
        $("#cartera2").show();
    } else {
        $("#nuevo").show();
        $("#cartera2").hide();
    }
});

$("#venta_monedaSI").change(function () {
    $("#ascensor_monedaSI").val($(this).find('option:selected').text());
    $("#ascensor_monedaSI1").val($(this).find('option:selected').text());
});

$("#venta_monedaSN").change(function () {
    $("#ascensor_monedaSN").val($(this).find('option:selected').text());
    $("#ascensor_monedaSN1").val($(this).find('option:selected').text());
});

$("#instalacion_etapas").change(function () {
    etapa = $("#instalacion_etapas").val();
    ascensor = document.getElementsByName("ascensor[].idascensor");

    for (i = 1; i <= 4; i++) {
        $("#" + i).hide();
    }
    $("#instalacion_ascensor").empty();
    for (i = 1; i <= etapa; i++) {
        html = "<div class='form-group col-sm-3 well'><label>ETAPA " + i + "</label><br>";
        for (a = 0; a < ascensor.length; a++) {
            html += "<input type='checkbox' value='" + $("#ascensor_" + a + "__idascensor").val() + "' name='instalacion_ascensor" + a + "' id='instalacion_" + i + "__ascensor" + a + "' class='js-switch'> <label class='control-label'> " + $("#ascensor_" + a + "__codigo").val() + "</label><br>";
        }
        $("#instalacion_ascensor").append(html + "</div>");
        $("#" + i).show();
    }

});

//listado de representantes
$("#venta_idmandante").change(function () {
    $.post("../ajax/mandante.php?op=listarContacto", {idmandante: $("#venta_idmandante").val()}, function (data, status) {
        data = JSON.parse(data);
        tablamandante.rows().remove().draw();
        for (i = 0; i < data.length; i++) {
            tablamandante.row.add([
                data[i]["nombre"],
                data[i]["rut"],
                data[i]["email"],
                data[i]["telefono"]
            ]).draw(false);
        }
    });
});

function cancelarform() {
    bootbox.confirm("Va a Cancelar el ingreso de datos ¿esta seguro?", function (result) {
        if (result) {
            location.reload();
        }
    });

}

function limpiartodo() {
    $("#venta_tipo").val("");
    $("#venta_fecha").val("");
    $("#venta_cartera").val("");
    $("#venta_cc").val("");
    $("#venta_pedido").val("");
    $("#venta_montoSI").val("");
    $("#venta_monedaSI").val("");
    $("#venta_montoSN").val("");
    $("#venta_monedaSN").val("");
    $("#venta_contrato").val("");
    $("#venta_retencion").val("");
    $("#venta_multa").val("");
    $("#venta_ocompra").val("");
    $("#venta_fecedifica").val("");
    $("#venta_ganticipo").val("");
    $("#venta_tpganticipo").val("");
    $("#venta_valganticipo").val("");
    $("#venta_gcumplimiento").val("");
    $("#venta_tpgcumplimiento").val("");
    $("#venta_valgcumplimiento").val("");
    $("#venta_razonsocial").val("");
    $("#venta_rut").val("");
    $("#venta_representante").val("");
    $("#venta_rutrepresentante").val("");
    $("#venta_region").val("");
    $("#venta_comuna").val("");
    $("#venta_calle").val("");
    $("#venta_numero").val("");
    $("#venta_oficina").val("");
    $("#venta_adicionales").val("");
    limpiarascensor();

    tablaAscensores.rows().remove().draw();
    counAscensor = 0;

    tablaSI.rows().remove().draw();
    countSI = 0;
    $("#si_desc").val("");
    $("#si_desc").selectpicker("refresh");
    $("#si_porc").val("");

    tablaSN.rows().remove().draw();
    countSN = 0;
    $("#sn_desc").val("");
    $("#sn_desc").selectpicker("refresh");
    $("#sn_porc").val("");
}

function mostrarform(flag) {
    //limpiar();
    if (flag) {
        $("#listadoproyectos").hide();
        $("#formularioproyecto").show();
        $("#myTabContent").show();
        $("#op_actualizar").hide();
        $("#op_listar").show();
        $("#btnGuardar").prop("disabled", false);
    } else {

        $("#formularioproyecto").hide();
        $("#listadoproyectos").show();
        $("#myTabContent").hide();
        $("#op_actualizar").show();
        $("#op_listar").hide();
        $("#btnGuardar").prop("disabled", false);
    }

}

function listar() {
    tabla = $('#tblproyectos').dataTable({
        "responsive": true,
        "aProcessing": true,
        "aServerSide": true,
        "scrollX": false,
        dom: 'Bfrtip',
        buttons: Botones,
        "language": Español,
        "ajax": {
            url: '../ajax/venta.php?op=listarpreventa',
            type: "get",
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 20, //Paginacion 10 items
        "order": [[2, "desc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}


/** AGREGAR garantias **/

function agregarGarantia() {

    //countboleta = tablaboleta.rows().count();
    idboleta = 0;
    idventa = $("#venta_idventa").val();
    descripcion = $("#garantia_descripcion").val();
    documento = $("#garantia_documento").val();
    tipo = $("#garantia_tipo").val();
    validez = $("#garantia_validez").val();
    ejecutada = 0;
    banco = $("#garantia_banco").val() == "" ? "N/S" : $("#garantia_banco").val();
    //GRABO EN BBDD
    formData = new FormData();

    formData.append("idboleta", idboleta);
    formData.append("idventa", idventa);
    formData.append("descripcion", descripcion);
    formData.append("documento", documento);
    formData.append("tipo", tipo);
    formData.append("validez", validez);
    formData.append("ejecutada", ejecutada);
    formData.append("banco", banco);

    $.ajax({
        url: '../ajax/boleta.php?op=guardaryeditar',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,

        success: function (datos) {
            if (datos != '0') {
                new PNotify({
                    title: 'Correcto!',
                    text: 'Garantia guardada con exito.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
                mostrarBoleta(idventa);
            } else {
                $("#btnGuardar").prop("disabled", true);
                new PNotify({
                    title: 'Error!',
                    text: 'Garantia no guardados.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
        }
    });





    limpiarGarantia();
}

function limpiarGarantia() {
    $("#garantia_descripcion").val("");
    $("#garantia_descripcion").selectpicker('refresh');
    $("#garantia_documento").val("");
    $("#garantia_documento").selectpicker('refresh');
    $("#garantia_tipo").val("");
    $("#garantia_tipo").selectpicker('refresh');
    $("#garantia_validez").val("");
    $("#garantia_banco").val("");
}

function delGarantia(id, idventa) {
    formData = new FormData();
    formData.append("idboleta", id);
    $.ajax({
        url: '../ajax/boleta.php?op=eliminar',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,

        success: function (datos) {
            if (datos != '0') {
                new PNotify({
                    title: 'Correcto!',
                    text: 'Eliminada con exito.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
                mostrarBoleta(idventa);
            } else {
                new PNotify({
                    title: 'Error!',
                    text: 'No eliminada',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }

        }

    });


}

/** fin AGREGAR garantias **/

//###########################################################

/** AGREGAR IMPORTACION , CENTRO COSTO , MANDANTE**/

function agregaCC(e) {
    e.preventDefault();
    $("#agregacentrocosto").prop("disabled", true);
    var formData = new FormData($("#formularioCC")[0]);

    //if( $('#esetapa').prop('checked')) {
    bootbox.confirm("Va agrabar este centro de costo, ¿esta seguro?", function (result) {
        if (result) {
            $.ajax({
                url: '../ajax/centrocosto.php?op=cguardaryeditar',
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,

                success: function (datos) {
                    if (datos != '0') {
                        $("#btnGuardar").prop("disabled", false);
                        new PNotify({
                            title: 'Correcto!',
                            text: 'Centro de costo guardado con exito.',
                            type: 'success',
                            styling: 'bootstrap3'
                        });

                        $.post("../ajax/centrocosto.php?op=mostar", {idcentrocosto: datos}, function (data) {
                            data = JSON.parse(data);
                            $("#proyecto_codigo").val(data.codigo);
                        });
                    } else {
                        $("#btnGuardar").prop("disabled", true);
                        new PNotify({
                            title: 'Error!',
                            text: 'Centro de costo no guardado.',
                            type: 'error',
                            styling: 'bootstrap3'
                        });
                    }

                    $.post("../ajax/centrocosto.php?op=selectcentro", function (r) {
                        $("#venta_cc").html(r);
                        $("#venta_cc").val(datos);
                        $("#venta_cc").selectpicker('refresh');
                    });
                }
            });
        }
        $("#venta_cc").empty();
        $('.modal-body').css('opacity', '');
        $('#agregacc').modal('toggle');
    });
}

function agregaIMP(e) {
    e.preventDefault();
    $("#agregaImportacion").prop("disabled", true);
    var formData = new FormData($("#formularioimp")[0]);

    //if( $('#esetapa').prop('checked')) {
    bootbox.confirm("Va agrabar esta importación, ¿esta seguro?", function (result) {
        if (result) {
            $.ajax({
                url: '../ajax/importacion.php?op=guardaryeditar',
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,

                success: function (datos) {
                    if (datos != '0') {
                        $("#btnGuardar").prop("disabled", false);
                        new PNotify({
                            title: 'Correcto!',
                            text: 'Importación guardada con exito.',
                            type: 'success',
                            styling: 'bootstrap3'
                        });
                    } else {
                        $("#btnGuardar").prop("disabled", true);
                        new PNotify({
                            title: 'Error!',
                            text: 'Importación no guardada.',
                            type: 'error',
                            styling: 'bootstrap3'
                        });
                    }
                    $("#venta_pedido").empty();
                    $('.modal-body').css('opacity', '');
                    $('#agregaimp').modal('toggle');
                    $.post("../ajax/importacion.php?op=selectimportacion", function (r) {
                        $("#venta_pedido").html(r);
                        $("#venta_pedido").val(datos);
                        $("#venta_pedido").selectpicker('refresh');
                    });
                }
            });
        }
    });
}

function guardarcliente() {
    var formData = new FormData($("#formcliente")[0]);
    $.ajax({
        url: '../ajax/mandante.php?op=guardar',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        beforeSend: function () {
            $("#btnGuardarCli").prop("disabled", true);
            $('.modal-body').css('opacity', '.5');
        },
        success: function (datos) {
            if (!isNaN(datos) || datos == 0) {
                new PNotify({
                    title: 'Registrado!',
                    text: 'El mandante fue registrado con exito.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
                $("#btnGuardarCli").prop("disabled", false);
                $('.modal-body').css('opacity', '');
                $('#modalForm').modal('toggle');


            } else {
                new PNotify({
                    title: 'Error!',
                    text: 'Hubo un error al registar.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
            $.post("../ajax/mandante.php?op=selectmandante", function (r) {
                $("#venta_idmandante").html(r);
                $("#venta_idmandante").val(datos);
                $("#venta_idmandante").selectpicker('refresh');

                //listado de representantes
                $.post("../ajax/mandante.php?op=listarContacto", {idmandante: $("#venta_idmandante").val()}, function (data, status) {
                    data = JSON.parse(data);
                    tablamandante.rows().remove().draw();
                    for (i = 0; i < data.length; i++) {
                        tablamandante.row.add([
                            data[i]["nombre"],
                            data[i]["rut"],
                            data[i]["email"],
                            data[i]["telefono"]
                        ]).draw(false);
                    }
                });

            });

        }
    });
    limpiarcliente();
}
function limpiarcliente() {
    $("#razon").val("");
    $("#rut").val("");
    $("#callecli").val("");
    $("#numerocli").val("");
    $("#oficinacli").val("");
    $("#idregionescli").val("");
    $("#idregionescli").selectpicker('refresh');
    $("#idcomunascli").empty();
    $("#idcomunascli").val("");
    $("#idcomunascli").selectpicker('refresh');
    $("#idtcliente").val("");
    $("#idtcliente").selectpicker('refresh');
    $('#replegal').empty();
}
$("#rut").blur(function () {
    if (this.value != "") {
        if (!validarut(this.value)) {
            new PNotify({
                title: 'Error!',
                text: 'Rut incorrecto',
                type: 'error',
                styling: 'bootstrap3'
            });
            $("#rut").focus();
        }
    }
});
/** FIN AGREGAR IMPORTACION Y CENTRO COSTO **/

//###########################################################

/** FUNCIONES DE ASCENSOR */
function agregarAscensor() {
    /*
     * validacion de montos
     */
    var MSI = $("#venta_montoSI").val();
    var MSN = $("#venta_montoSN").val();
    var ascensor = document.getElementsByName("ascensor[]");
    var sumSI = 0, sumSN = 0;
    for (i = 0; i < ascensor.length; i++) {
        sumSI = sumSI + parseFloat($("#ascensor_" + i + "__montosi").val());
        sumSN = sumSN + parseFloat($("#ascensor_" + i + "__montosn").val());
    }
    sumSI = sumSI + parseFloat($("#ascensor_valorSI").val());
    sumSN = sumSN + parseFloat($("#ascensor_valorSN").val());

    if (MSI < sumSI) {
        //bootbox.alert("La suma de los montos de suministro importado de los ascensores no pude superar el monto de suministro importado de la venta.");
        new PNotify({
            title: 'Error!',
            text: 'La suma de los montos de suministro importado de los ascensores no pude superar el monto de suministro importado de la venta.!',
            type: 'error',
            styling: 'bootstrap3'
        });
        return false;
    }
    if (MSN < sumSN) {
        //bootbox.alert("La suma de los montos de suministro nacional de los ascensores no pude superar el monto de suministro nacional de la venta.");
        new PNotify({
            title: 'Error!',
            text: 'La suma de los montos de suministro nacional de los ascensores no pude superar el monto de suministro nacional de la venta.!',
            type: 'error',
            styling: 'bootstrap3'
        });
        return false;
    }
    /*
     * FIN validacion de montos
     */

    var counAscensor = tablaAscensores.rows().count();
    var tpnombAsc = $("#ascensor_idtascensor").find('option:selected').text();
    var nommarca = $("#ascensor_marca").find('option:selected').text();
    var idventa = $("#venta_idventa").val();
    var formData = new FormData();
    var text = "";
    formData.append("idventa", idventa);
    var corr = 0;

    if ($("#ascensor_codigo").val() == "" || $("#ascensor_codigo").val() == "0") {
        corr = getcorrelativo($("#proyecto_region").val());
        if (corr == 0) {
            new PNotify({
                title: 'Error!',
                text: 'Ocurrio un Error, Ascensor no guardado.',
                type: 'error',
                styling: 'bootstrap3'
            });
            return false;
        }
    } else {
        corr = $("#ascensor_codigo").val();
    }


    formData.append("codigo", corr);
    formData.append("idascensor", 0);
    formData.append("idtascensor", $("#ascensor_idtascensor").val());
    formData.append("marca", $("#ascensor_marca").val());
    formData.append("modelo", $("#ascensor_modelo").val());
    formData.append("ken", ($("#ascensor_ken").val() == "" ? "N/S" : $("#ascensor_ken").val()));
    formData.append("pservicio", '0000-00-00');
    formData.append("gtecnica", '0000-00-00');
    formData.append("valoruf", 0);
    formData.append("valorclp", 0);
    formData.append("comando", $("#ascensor_comando").val());
    formData.append("paradas", $("#ascensor_paradas").val());
    formData.append("accesos", $("#ascensor_accesos").val());
    formData.append("capkg", $("#ascensor_capkg").val());
    formData.append("capper", $("#ascensor_capper").val());
    formData.append("velocidad", $("#ascensor_velocidad").val());
    formData.append("dcs", 0);
    formData.append("elink", 0);
    formData.append("montosi", $("#ascensor_valorSI").val());
    formData.append("montosn", $("#ascensor_valorSN").val());
    formData.append("idedificio", null);
    formData.append("idcontrato", null);
    formData.append("ubicacion", "N/S");
    formData.append("codigocli", "N/S");

    $.ajax({
        url: '../ajax/ascensor.php?op=guardaryeditar',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function (datos) {
            if (datos != '0') {
                $("#btnGuardar").prop("disabled", false);
                new PNotify({
                    title: 'Correcto!',
                    text: 'Ascensor guardado con exito.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
                mostrarAscensor(idventa);
                limpiarascensor();
            } else {
                $("#btnGuardar").prop("disabled", true);
                new PNotify({
                    title: 'Error!',
                    text: 'Ascensor no guardado, verifique que no haya sido ingresado anteriormente.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
                setcorrelativo($("#proyecto_region").val());
            }
        },
        error: function (datos) {
            text = datos;
        }
    });


}

function agregarAscensorexiste() {
    Ascensor = $("#listaAsc").val();
    counAscensor = tablaAscensores.rows().count();
    idventa = $("#venta_idventa").val();

    if (Ascensor == "" || Ascensor == 0 || Ascensor == null) {
        bootbox.alert("Debe seleccionar un ascensor del listado");
        return false;
    }
    if ($("#ascensor_valorSI1").val() == "" || isNaN($("#ascensor_valorSI1").val())) {
        bootbox.alert("Debe ingresar un valor numérico para el suministro importado");
        $("#ascensor_valorSI1").val(0);
        return false;
    }
    if ($("#ascensor_valorSN1").val() == "" || isNaN($("#ascensor_valorSN1").val())) {
        bootbox.alert("Debe ingresar un valor numérico para el suministro nacional");
        $("#ascensor_valorSN1").val(0);
        return false;
    }

    /*
     * validacion de montos
     */
    var MSI = $("#venta_montoSI").val();
    var MSN = $("#venta_montoSN").val();
    var ascensor = document.getElementsByName("ascensor[]");
    var sumSI = 0, sumSN = 0;
    for (i = 0; i < ascensor.length; i++) {
        sumSI = sumSI + parseFloat($("#ascensor_" + i + "__montosi").val());
        sumSN = sumSN + parseFloat($("#ascensor_" + i + "__montosn").val());
    }
    sumSI = sumSI + parseFloat($("#ascensor_valorSI1").val());
    sumSN = sumSN + parseFloat($("#ascensor_valorSN1").val());

    if (MSI < sumSI) {
        bootbox.alert("La suma de los montos de suministro importado de los ascensores no pude superar el monto de suministro importado de la venta.");
        return false;
    }
    if (MSN < sumSN) {
        bootbox.alert("La suma de los montos de suministro nacional de los ascensores no pude superar el monto de suministro nacional de la venta.");
        return false;
    }
    /*
     * FIN validacion de montos
     */

    $.post("../ajax/ascensor.php?op=mostrar", {idascensor: Ascensor}, function (data) {
        data = JSON.parse(data);
        mostrarform(true);
        codigo = data.codigo;
        idascensor = data.idascensor;
        tpAscensor = data.idtascensor;
        tpnombAsc = data.tipo + ' - ' + data.codigo;
        marca = data.marca;
        nommarca = data.marcaNomb;
        modelo = data.modelo;
        ken = data.ken;
        pservicio = data.pservicio;
        gtecnica = data.gtecnica;
        vuf = data.valoruf;
        vclp = data.valorclp;
        paradas = data.paradas;
        capkg = data.capkg;
        caper = data.capper;
        vel = data.velocidad;
        dcs = data.dcs;
        elink = data.elink;
        valSI = $("#ascensor_valorSI1").val();
        valSN = $("#ascensor_valorSN1").val();



        formData = new FormData();
        var text = "";

        formData.append("idventa", idventa);

        formData.append("codigo", codigo);
        formData.append("idascensor", idascensor);
        formData.append("idtascensor", tpAscensor);
        formData.append("marca", marca);
        formData.append("modelo", modelo);
        formData.append("ken", ken);
        formData.append("pservicio", pservicio);
        formData.append("gtecnica", gtecnica);
        formData.append("valoruf", vuf);
        formData.append("valorclp", vclp);
        formData.append("paradas", paradas);
        formData.append("capkg", capkg);
        formData.append("capper", caper);
        formData.append("velocidad", vel);
        formData.append("dcs", dcs);
        formData.append("elink", elink);

        formData.append("montosi", valSI);
        formData.append("montosn", valSN);

        formData.append("idedificio", null);
        formData.append("idcontrato", null);
        formData.append("ubicacion", "PENDIENTE");
        formData.append("codigocli", "ASC " + (i + 1));

        $.ajax({
            url: '../ajax/ascensor.php?op=guardaryeditar',
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            success: function (datos) {
                if (datos != '0' || datos != '') {
                    $("#btnGuardar").prop("disabled", false);
                    new PNotify({
                        title: 'Correcto!',
                        text: 'Ascensor guardado con exito.',
                        type: 'success',
                        styling: 'bootstrap3'
                    });
                    mostrarAscensor(idventa);
                } else {
                    $("#btnGuardar").prop("disabled", true);
                    new PNotify({
                        title: 'Error!',
                        text: 'Ascensor no guardado, verifique que no haya sido ingresado anteriormente.',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                }
            },
            error: function (datos) {
                text = datos;
            }
        });
        limpiarascensor();
    });
}

function limpiarascensor() {
    $("#ascensor_codigo").val("");
    $("#ascensor_idascensor").val(0);
    $("#ascensor_idtascensor").val(0);
    $("#ascensor_idtascensor").selectpicker('refresh');

    $("#ascensor_comando").val("")
    $("#ascensor_comando").selectpicker('refresh');

    $("#ascensor_marca").val(0);
    $("#ascensor_marca").selectpicker('refresh');
    $("#ascensor_modelo").val(0);
    $("#ascensor_modelo").selectpicker('refresh');
    $("#ascensor_ken").val("");
    $("#ascensor_pservicio").val("");
    $("#ascensor_gtecnica").val("");
    $("#ascensor_valoruf").val("");
    $("#ascensor_valorclp").val("");
    $("#ascensor_paradas").val("");
    $("#ascensor_accesos").val("");
    $("#ascensor_capkg").val("");
    $("#ascensor_capper").val("");
    $("#ascensor_velocidad").val("");
    $("#ascensor_dcs").val(0);
    $("#ascensor_dcs").selectpicker('refresh');
    $("#ascensor_elink").val(0);
    $("#ascensor_elink").selectpicker('refresh');
    $("#ascensor_codigo").val("");
    $("#ascensor_valorSI").val("");
    $("#ascensor_valorSN").val("");
    $("#ascensor_valorSI1").val("");
    $("#ascensor_valorSN1").val("");
    $("#listaAsc").val(0);
    $("#listaAsc").selectpicker("refresh");
}

function delasc(idascensor) {
    formData = new FormData();
    idventa = $("#venta_idventa").val();
    //ELIMINO FORMAS DE PAGO (SI EXISTE)
    $.post("../ajax/formapago.php?op=eliminarxascensor", {idascensor: idascensor}, function (data, status) {
        sw = true;
    });
    //ELIMINO INFO CONTRACTUAL SI EXISTE
    $.post("../ajax/infoventa.php?op=eliminarxascensor", {idascensor: idascensor}, function (data, status) {
        sw = true;
    });
    //FINALMENTE ELIMINO ASCENSOR
    $.post("../ajax/ascensor.php?op=eliminar", {idascensor: idascensor}, function (data, status) {
        if (data != 0) {
            new PNotify({
                title: 'Correcto!',
                text: 'Ascensor eliminado.',
                type: 'success',
                styling: 'bootstrap3'
            });
            mostrarAscensor(idventa);
        } else {
            new PNotify({
                title: 'Error!',
                text: 'Ascensor no eliminado',
                type: 'error',
                styling: 'bootstrap3'
            });
        }

    });
}

/** FIN FUNCIONES DE ASCENSOR */

//###########################################################

/*** FUNCIONES SI Y SN **/

function agregarSI() {
    var secuencia = tablaSI.rows().count() + 1;
    var idventa = $("#venta_idventa").val();
    var formData = new FormData();
    var ascensor = document.getElementsByName("ascensor[].idascensor");
    var SI = document.getElementsByName("si[]");
    sw = 0;
    var sumsi = 0;
    for (i = 0; i < SI.length; i++) {
        sumsi = sumsi + parseFloat($("#si_" + i + "__porcetaje").val());
    }
    sumsi = sumsi + parseFloat($("#si_porc").val() / 100);

    if ($("#si_desc").val() == "" || $("#si_desc").val() == null) {
        new PNotify({
            title: 'Error!',
            text: 'Debe seleccionar una opcion de suministro internacional',
            type: 'error',
            styling: 'bootstrap3'
        });
        //bootbox.alert("la suma del posrcentaje de pagos no puede ser superior a 100%");
        return false;
    }

    if (sumsi > 1) {
        new PNotify({
            title: 'Error!',
            text: 'la suma del posrcentaje de pagos no puede ser superior a 100%.',
            type: 'error',
            styling: 'bootstrap3'
        });
        //bootbox.alert("la suma del posrcentaje de pagos no puede ser superior a 100%");
        return false;
    }

    cambiaSecuenciaSI(idventa);
    for (a = 0; a < ascensor.length; a++) {
        asc = $("#ascensor_" + a + "__idascensor").val();
        formData.append("idformapago", 0);
        formData.append("idtformapago", 1);
        formData.append("idascensor", asc);
        formData.append("descripcion", $("#si_desc").val());
        formData.append("secuencia", secuencia);
        formData.append("porcentaje", $("#si_porc").val());

        $.ajax({
            url: '../ajax/formapago.php?op=guardaryeditar',
            type: "POST",
            data: formData,
            async: false,
            cache: false,
            timeout: 30000,
            contentType: false,
            processData: false
        }).done(function (datos) {
            if ((a + 1) == ascensor.length) {
                new PNotify({
                    title: 'Correcto!',
                    text: 'Guardado.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
                mostrarSI(idventa);
            }
        }).fail(function (datos) {
            new PNotify({
                title: 'Error!',
                text: 'No guardado.',
                type: 'error',
                styling: 'bootstrap3'
            });
        });

    }
    limpiarSI();
}

function limpiarSI() {
    $("#si_desc").val("");
    $("#si_desc").selectpicker("refresh");
    $("#si_porc").val("");
}

function delSI(idventa, fp, secuencia) {

    $.post("../ajax/formapago.php?op=listarelimina", {"idventa": idventa, "idtformapago": fp, "secuencia": secuencia}, function (data, status) {
        data = JSON.parse(data);

        for (i = 0; i < data.length; i++) {
            formData = new FormData();
            formData.append("idformapago", data[i]["id"]);
            $.ajax({
                url: '../ajax/formapago.php?op=eliminar',
                type: "POST",
                data: formData,
                async: false,
                cache: false,
                timeout: 30000,
                contentType: false,
                processData: false
            }).done(function (datos) {
                if ((i + 1) == data.length) {
                    new PNotify({
                        title: 'Correcto!',
                        text: 'Eliminado.',
                        type: 'success',
                        styling: 'bootstrap3'
                    });
                    mostrarSI(idventa);
                }
            }).fail(function (datos) {
                new PNotify({
                    title: 'Error!',
                    text: 'No guardado.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            });
        }
    });

}

function cambiaSecuenciaSI(idventa) {
    var si = document.getElementsByName("si[]");
    //var idventa = $("#venta_idventa").val();
    for (i = 0; i < si.length; i++) {
        var sec1 = $("#si_" + i + "__secuencia").val();
        var sec2 = $("#si_" + i + "__secuencia2").val();

        if (sec1 !== sec2) {
            $.post("../ajax/formapago.php?op=listarelimina", {"idventa": idventa, "idtformapago": 1, "secuencia": sec1}, function (data, status) {
                data = JSON.parse(data);

                for (i = 0; i < data.length; i++) {
                    formData = new FormData();
                    formData.append("idformapago", data[i]["id"]);
                    formData.append("secuencia", sec2);

                    $.ajax({
                        url: '../ajax/formapago.php?op=editarSecuencia',
                        type: "POST",
                        data: formData,
                        async: false,
                        cache: false,
                        timeout: 30000,
                        contentType: false,
                        processData: false
                    }).done(function (datos) {
                        if ((i + 1) == data.length) {
                            mostrarSI(idventa);
                        }
                    });
                }
            });
        }
    }
}

//------------------------------------

function agregarSN() {
    var secuencia = tablaSN.rows().count() + 1;
    var idventa = $("#venta_idventa").val();
    var ascensor = document.getElementsByName("ascensor[].idascensor");
    var SN = document.getElementsByName("sn[]");

    var sumsn = 0;
    for (i = 0; i < SN.length; i++) {
        sumsn = sumsn + parseFloat($("#sn_" + i + "__porcetaje").val());
    }
    sumsn = sumsn + parseFloat($("#sn_porc").val() / 100);

    if ($("#sn_desc").val() == "" || $("#sn_desc").val() == null) {
        new PNotify({
            title: 'Error!',
            text: 'Debe seleccionar una opcion de suministro nacional',
            type: 'error',
            styling: 'bootstrap3'
        });
        //bootbox.alert("la suma del posrcentaje de pagos no puede ser superior a 100%");
        return false;
    }

    if (sumsn > 1) {
        new PNotify({
            title: 'Error!',
            text: 'la suma del posrcentaje de pagos no puede ser superior a 100%',
            type: 'error',
            styling: 'bootstrap3'
        });
        //bootbox.alert("la suma del posrcentaje de pagos no puede ser superior a 100%");
        return false;
    }

    cambiaSecuenciaSN(idventa);
    for (a = 0; a < ascensor.length; a++) {
        formData = new FormData();
        asc = $("#ascensor_" + a + "__idascensor").val();
        formData.append("idformapago", 0);
        formData.append("idtformapago", 2);
        formData.append("idascensor", asc);
        formData.append("descripcion", $("#sn_desc").val());
        formData.append("secuencia", secuencia);
        formData.append("porcentaje", $("#sn_porc").val());

        $.ajax({
            url: '../ajax/formapago.php?op=guardaryeditar',
            type: "POST",
            data: formData,
            async: false,
            cache: false,
            timeout: 30000,
            contentType: false,
            processData: false
        }).done(function (datos) {
            if ((a + 1) == ascensor.length) {
                new PNotify({
                    title: 'Correcto!',
                    text: 'Guardado.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
                mostrarSN(idventa);
            }
        }).fail(function (datos) {
            new PNotify({
                title: 'Error!',
                text: 'No guardado.',
                type: 'error',
                styling: 'bootstrap3'
            });
        });

    }
    limpiarSN();
}

function limpiarSN() {
    $("#sn_desc").val("");
    $("#sn_desc").selectpicker("refresh");
    $("#sn_porc").val("");
}

function delSN(idventa, fp, secuencia) {
    idventa = $("#venta_idventa").val();
    ascensor = document.getElementsByName("ascensor[].idascensor");
    $.post("../ajax/formapago.php?op=listarelimina", {"idventa": idventa, "idtformapago": fp, "secuencia": secuencia}, function (data, status) {
        data = JSON.parse(data);

        for (i = 0; i < data.length; i++) {
            formData = new FormData();
            var id = data[i]["id"];

            formData.append("idformapago", id);
            $.ajax({
                url: '../ajax/formapago.php?op=eliminar',
                type: "POST",
                data: formData,
                async: false,
                cache: false,
                timeout: 30000,
                contentType: false,
                processData: false
            }).done(function (datos) {
                if ((i + 1) == data.length) {
                    new PNotify({
                        title: 'Correcto!',
                        text: 'Eliminado.',
                        type: 'success',
                        styling: 'bootstrap3'
                    });
                    mostrarSN(idventa);
                }
            }).fail(function (datos) {
                new PNotify({
                    title: 'Error!',
                    text: 'No guardado.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            });
            ;
        }
    });
}

function cambiaSecuenciaSN(idventa) {
    var sn = document.getElementsByName("sn[]");
    //var idventa = $("#venta_idventa").val();
    for (i = 0; i < sn.length; i++) {
        var sec1 = $("#sn_" + i + "__secuencia").val();
        var sec2 = $("#sn_" + i + "__secuencia2").val();

        if (sec1 !== sec2) {
            $.post("../ajax/formapago.php?op=listarelimina", {"idventa": idventa, "idtformapago": 2, "secuencia": sec1}, function (data, status) {
                data = JSON.parse(data);

                for (i = 0; i < data.length; i++) {
                    formData = new FormData();
                    formData.append("idformapago", data[i]["id"]);
                    formData.append("secuencia", sec2);

                    $.ajax({
                        url: '../ajax/formapago.php?op=editarSecuencia',
                        type: "POST",
                        data: formData,
                        async: false,
                        cache: false,
                        timeout: 30000,
                        contentType: false,
                        processData: false
                    }).done(function (datos) {
                        if ((i + 1) == data.length) {
                            mostrarSN(idventa);
                        }
                    });
                }
            });
        }
    }
}
/*** FIN FUNCIONES SI Y SN **/

//###########################################################

/*** FUNCIONES FECHAS CONTRACTUAL **/

function agregarinfo() {
    SW = 0;
    info = document.getElementsByName("instalacion_descripcion[]");
    asc = document.getElementsByName("ascensor[]");
    idventa = $("#venta_idventa").val();
    o = 0;
    for (i = 0; i < info.length; i++) {

        for (a = 0; a < asc.length; a++) {

            if ($("#instalacion_" + i + "__descripcion").val() != "" && $("#instalacion_asc" + a + "_" + i).val() != "") {
                formData = new FormData();

                formData.append("idinforventa", $("#instalacion_idinfo" + a + "_" + i).val());
                formData.append("idascensor", $("#instalacion_idascensor" + a + "_" + i).val());
                formData.append("descripcion", $("#instalacion_" + i + "__descripcion").val());
                formData.append("fecha", $("#instalacion_asc" + a + "_" + i).val());

                $.ajax({
                    url: '../ajax/infoventa.php?op=guardaryeditar',
                    type: "POST",
                    data: formData,
                    async: false,
                    cache: false,
                    timeout: 30000,
                    contentType: false,
                    processData: false
                }).done(function (datos) {
                    console.log(datos);
                    if ((a + 1) == asc.length) {
                        new PNotify({
                            title: 'Correcto!',
                            text: 'Guardado.',
                            type: 'success',
                            styling: 'bootstrap3'
                        });
                        mostrarSI(idventa);
                    }
                }).fail(function (datos) {
                    console.log(datos);
                    new PNotify({
                        title: 'Error!',
                        text: 'No guardado.',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                });
                o++;
            } else {
                new PNotify({
                    title: 'Error!',
                    text: 'Debe seleccionar una Descripción.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
                return false;
            }
        }


    }
    mostrarInfo(idventa);


}

function delinfo(ids) {

    idventa = $("#venta_idventa").val();
    formData = new FormData();

    if (ids.includes("info_")) {
        $("#" + ids).remove();
        new PNotify({
            title: 'Correcto!',
            text: 'Eliminado.',
            type: 'success',
            styling: 'bootstrap3'
        });
    } else {

        formData.append("ids", ids);
        $.ajax({
            url: '../ajax/infoventa.php?op=eliminar',
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,

            success: function (datos) {
                if (datos != '0') {
                    new PNotify({
                        title: 'Correcto!',
                        text: 'Datos de venta guardados con exito.',
                        type: 'success',
                        styling: 'bootstrap3'
                    });

                } else {
                    $("#btnGuardar").prop("disabled", true);
                    new PNotify({
                        title: 'Error!',
                        text: 'Datos de venta no guardados.',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                }
            }
        });
        mostrarInfo(idventa);
    }
}

/*** FIN FUNCIONES FECHAS CONTRACTUAL **/

//###########################################################

/*** FUNCION MOSTRAR **/

function mostrarBoleta(idventa) {
    if (!idventa) {
        idventa = $("#venta_idventa").val();
    }
    limpiarGarantia();

    tablaboleta.rows().remove().draw();
    $.post("../ajax/boleta.php?op=listarventa", {idventa: idventa}, function (data, status) {
        data = JSON.parse(data);
        for (i = 0; i < data.length; i++) {
            tablaboleta.row.add([
                data[i]["0"],
                data[i]["1"],
                data[i]["2"],
                data[i]["3"],
                data[i]["4"],
                data[i]["5"]
            ]).draw(false);
        }
        if (i > 0) {
            cambiapestana(3);
        }

    });
}

function mostrarSI(idventa) {
    if (!idventa) {
        idventa = $("#venta_idventa").val();
    }
    $.post("../ajax/formapago.php?op=listarventaSI", {idventa: idventa}, function (data, status) {
        data = JSON.parse(data);
        tablaSI.rows().remove().draw();
        sumSI = 0;
        $("#sumSI").empty();


        for (i = 0; i < data.length; i++) {
            tablaSI.row.add([
                data[i]["1"],
                data[i]["0"],
                data[i]["2"]
            ]).draw(false);

            sumSI += data[i]["3"];
        }
        //cambiapestana(7);
        if (sumSI == 100) {
            $("#addsi").attr("disabled", true);
        } else {
            $("#addsi").attr("disabled", false);
        }

        $("#sumSI").append(sumSI);
        final();
    });

}

function mostrarSN(idventa) {
    if (!idventa) {
        idventa = $("#venta_idventa").val();
    }
    $.post("../ajax/formapago.php?op=listarventaSN", {idventa: idventa}, function (data, status) {
        data = JSON.parse(data);
        tablaSN.rows().remove().draw();
        sumSN = 0;
        $("#sumSN").empty();
        for (i = 0; i < data.length; i++) {
            tablaSN.row.add([
                data[i]["1"],
                data[i]["0"],
                data[i]["2"]
            ]).draw(false);
            sumSN += data[i]["3"];
        }
        //cambiapestana(7);
        if (sumSN == 100) {
            $("#addsn").attr("disabled", true);
        } else {
            $("#addsn").attr("disabled", false);
        }

        $("#sumSN").append(sumSN);
        final();
    });

}

function mostrarAscensor(idventa) {
    if (!idventa) {
        idventa = $("#venta_idventa").val();
    }
    sumSIAscensor = 0;
    sumSNAscensor = 0;
    tablaAscensores.rows().remove().draw();
    $.post("../ajax/ascensor.php?op=listarventa", {idventa: idventa}, function (data4, status) {
        data4 = JSON.parse(data4);

        $("#totSN").empty();
        $("#totSI").empty();

        for (i = 0; i < data4.length; i++) {
            tablaAscensores.row.add([
                data4[i]["0"],
                data4[i]["1"],
                data4[i]["2"],
                data4[i]["3"],
                data4[i]["4"],
                data4[i]["5"],
                data4[i]["6"],
                data4[i]["7"], //new Intl.NumberFormat("de-DE").format(data4[i]["7"]),
                data4[i]["8"], //new Intl.NumberFormat("de-DE").format(data4[i]["8"]),
                data4[i]["9"],
                data4[i]["10"],
                data4[i]["11"]
            ]).draw(false);

            sumSIAscensor += parseFloat(data4[i]["9"]);
            sumSNAscensor += parseFloat(data4[i]["10"]);
        }

        $("#totSN").html(sumSNAscensor.toFixed(2));
        $("#totSI").html(sumSIAscensor.toFixed(2));
        //$("#totSN").html(formatNumber.new(sumSNAscensor.toFixed(2))); //new Intl.NumberFormat("de-DE").format(sumSNAscensor));
        //$("#totSI").html(formatNumber.new(sumSIAscensor.toFixed(2))); //new Intl.NumberFormat("de-DE").format(sumSIAscensor));

        if (i > 0) {
            cambiapestana(5);
            cambiapestana(6);
            //cambiapestana(7);
        }

    });
}

function mostrarInfo(idventa) {
    if (!idventa) {
        idventa = $("#venta_idventa").val();
    }
    $("#fechasascensores").empty();
    countinstalacion = 0;
    $.post("../ajax/infoventa.php?op=listar", {idventa: idventa}, function (data, status) {
        data = JSON.parse(data);
        desc = "";
        html = "";
        del = "";
        for (i = 0; i < data.length; i++) {
            if (desc !== data[i]["2"]) {
                html += "<div class='x_panel' id='info_" + i + "'><div class='clearfix'></div>" +
                        "<div class='col-md-4 col-sm-12 col-xs-12'>" +
                        "<label>DESCRIPCION</label>" +
                        "<input type='text' readOnly='readOnly' name='instalacion_descripcion[]' id='instalacion_" + countinstalacion + "__descripcion' style='text-transform:uppercase;' class='form-control' value='" + data[i]["2"] + "'>" +
                        "</div>";
            }

            html += "<div class='col-md-2 col-sm-12 col-xs-12'>" +
                    "<label>Ascensor " + data[i]["5"] + "</label>" +
                    "<input type='hidden' name='instalacion_idascensor" + countinfo + "_" + countinstalacion + "' id='instalacion_idascensor" + countinfo + "_" + countinstalacion + "' value='" + data[i]["1"] + "'>" +
                    "<input type='hidden' name='instalacion_idinfo" + countinfo + "_" + countinstalacion + "' id='instalacion_idinfo" + countinfo + "_" + countinstalacion + "' value='" + data[i]["0"] + "'>" +
                    "<input type='date' name='instalacion_asc[]' id='instalacion_asc" + countinfo + "_" + countinstalacion + "' class='form-control' value='" + data[i]["3"] + "'>" +
                    "</div>";

            desc = data[i]["2"];
            del += data[i]["0"] + ",";
            countinfo += 1;
            if (i + 1 >= data.length) {
                html += "<div class='col-md-1 col-sm-12 col-xs-12'><label>&nbsp;</label><br>" +
                        "<button type='button' class='btn btn-round btn-danger' onclick=\"delinfo('" + del.substr(0, del.length - 1) + "');\"><i class='fa fa-trash'></i></button>" +
                        "</div>";
                html += "</div>";
                del = "";
                countinfo = 0;
                countinstalacion += 1;
            } else {
                if (desc !== data[i + 1]["2"]) {
                    html += "<div class='col-md-1 col-sm-12 col-xs-12'><label>&nbsp;</label><br>" +
                            "<button type='button' class='btn btn-round btn-danger' onclick=\"delinfo('" + del.substr(0, del.length - 1) + "');\"><i class='fa fa-trash'></i></button>" +
                            "</div>";
                    html += "</div>";
                    del = "";
                    countinfo = 0;
                    countinstalacion += 1;
                } else {

                }
            }
        }

        $("#fechasascensores").append(html);

        final();

    });
}

function addinfo() {

    html = "";
    asc = document.getElementsByName("ascensor[]");
    countinfo += 1;
    id = "info_" + countinfo;

    html = "<div class='x_panel' id='" + id + "'><div class='clearfix'></div>" +
            "<div class='col-md-4 col-sm-12 col-xs-12'>" +
            "<label>DESCRIPCION</label>";
    //"<input type='text' name='instalacion_descripcion[]' id='instalacion_" + countinstalacion + "__descripcion' style='text-transform:uppercase;' class='form-control'>" +
    selecthtml = "<select name='instalacion_descripcion[]' id='instalacion_" + countinstalacion + "__descripcion' class='form-control' required>";
    selecthtml += "<option value='' selected disabled>Seleccione una opción</option>";
    selecthtml += selectInstalaciones;
    selecthtml += "</select>";

    html += selecthtml + "</div>";
    //$("#fechasascensores").empty();


    for (a = 0; a < asc.length; a++) {
        html += "<div class='col-md-2 col-sm-12 col-xs-12'>" +
                "<label>Ascensor " + $("#ascensor_" + a + "__codigo").val() + "</label>" +
                "<input type='hidden' name='instalacion_idascensor" + a + "_" + countinstalacion + "' id='instalacion_idascensor" + a + "_" + countinstalacion + "' value='" + $("#ascensor_" + a + "__idascensor").val() + "'>" +
                "<input type='hidden' name='instalacion_idinfo" + a + "_" + countinstalacion + "' id='instalacion_idinfo" + a + "_" + countinstalacion + "' value='0'>" +
                "<input type='date' name='instalacion_asc[]' id='instalacion_asc" + a + "_" + countinstalacion + "' class='form-control'>" +
                "</div>";
        countinfo += 1;
    }
    html += "<div class='col-md-1 col-sm-12 col-xs-12'><label>&nbsp;</label><br>" +
            "<button type='button' class='btn btn-round btn-danger' onclick=\"delinfo('" + id + "');\"><i class='fa fa-trash'></i></button>" +
            "</div>";
    html += "</div>";
    $("#fechasascensores").append(html);
    countinstalacion += 1;
}

///********
function revisar() {
    limpiarRevisa();
    idventa = $("#venta_idventa").val();

    //PRIMERA PARTE
    $("#venta_codigo1").html("<h5><b>N° " + $("#venta_codigo").val() + "</b></h5>");
    $("#proyecto_nombre1").html("<h5><b>" + $("#proyecto_nombre").val().toUpperCase() + "</b></h5>");
    $("#venta_fecha1").html("<h5><b>" + $("#venta_fecha").val() + "</b></h5>");

    if ($("#venta_tipo").val() == 1) {
        $("#venta_tipo1").html("X");
        $("#venta_tipo2").empty();
        $("#venta_tipo3").empty();
    } else if ($("#venta_tipo").val() == 2) {
        $("#venta_tipo1").empty();
        $("#venta_tipo2").html("X");
        $("#venta_tipo3").empty();
    } else if ($("#venta_tipo").val() == 3) {
        $("#venta_tipo1").empty();
        $("#venta_tipo2").empty();
        $("#venta_tipo3").html("X");
    }
    $("#venta_tipo4").empty();


    var idimportacion = $("#venta_pedido").val();
    $.post("../ajax/importacion.php?op=mostrar", {idimportacion: idimportacion}, function (data, status) {
        data = JSON.parse(data);
        $("#imp_codigo").html(data.codigo);
        $("#imp_nef").html(data.nef);
        $("#imp_proveedor").html(data.proveedor);
    });

    $("#venta_cc1").html($("#proyecto_codigo").val());
    $("#venta_ken1").html("Pendiente");


    //DATOS PROYECTO
    $("#proyecto_nombre2").html($("#proyecto_nombre").val());
    $("#proyecto_direccion1").html($("#proyecto_calle").val().toLowerCase() + " #" + $("#proyecto_numero").val().toLowerCase());

    $("#contacto_ventas").html("");
    $("#contacto_ventas_email").html("Email: ");
    $("#contacto_ventas_fono").html("Fono: ");

    $("#contacto_obra").html("");
    $("#contacto_obra_email").html("Email: ");
    $("#contacto_obra_fono").html("Fono: ");


    //DATOS CLIENTE
    var idmandante = $("#venta_idmandante").val();
    $.post("../ajax/mandante.php?op=mostrar", {idmandante: idmandante}, function (data, status) {
        data = JSON.parse(data);
        $("#mandante_razon").html(data.razon_social.toLowerCase());
        $("#mandante_direccion").html(data.calle.toLowerCase() + " #" + data.numero.toLowerCase());
        $("#mandante_rut").html(data.rut);
    });
    $.post("../ajax/mandante.php?op=listarContacto", {idmandante: idmandante}, function (data, status) {
        data = JSON.parse(data);
        for (i = 0; i < data.length; i++) {
            $("#mandante_representante").html(data[i]["nombre"].toLowerCase());
            $("#mandante_respresentante_rut").html("Rut: " + data[i]["rut"].toLowerCase());
        }
    });

    ///INFORMACION CONTRACTUAL DE INSTALACION
    $.post("../ajax/infoventa.php?op=listarrevisar", {idventa: idventa}, function (data, status) {
        data = JSON.parse(data);
        desc = "";
        html = "";
        for (i = 0; i < data.length; i++) {
            if (desc !== data[i]["2"]) {
                html += "<tr class='item'><td colspan='3'>" + data[i]["2"].toLowerCase() + "</td> <td>";
            }
            html += formatofechacorta(data[i]["3"]) + " / ";
            desc = data[i]["2"];
            if (i + 1 >= data.length) {
                html = html.substr(0, (html.length - 2)).toLowerCase();
                html += "</td></tr>";
            } else {
                if (desc !== data[i + 1]["2"]) {
                    html = html.substr(0, (html.length - 2));
                    html += "</td></tr>";
                }
            }
        }

        $("#rev_info").append(html);
    });


    ///FORMA DE PAGO CONTRACTUAL
    $.post("../ajax/formapago.php?op=listarventaSI", {idventa: idventa}, function (data, status) {
        data = JSON.parse(data);
        val = data.length + 1;
        html = "";
        html = '<tr class="item">' +
                '<td rowspan="' + val + '" style="text-align: left;font-weight: bold; vertical-align: middle;">Parte Importada</td>' +
                '</tr>';

        for (i = 0; i < data.length; i++) {
            html += '<tr class="item">' +
                    '<td style="text-align: center;">' + data[i]["3"] + '%</td>' +
                    '<td style="text-align: left;"> ' + data[i]["1"] + '</td>' +
                    '</tr>';
        }


        $("#PI").append(html.toLowerCase());
    });

    $.post("../ajax/formapago.php?op=listarventaSN", {idventa: idventa}, function (data, status) {
        data = JSON.parse(data);
        val = data.length + 1;
        html = "";
        html = '<tr class="item">' +
                '<td rowspan="' + val + '" style="text-align: left;font-weight: bold; vertical-align: middle;">Parte Nacional</td>' +
                '</tr>';

        for (i = 0; i < data.length; i++) {
            html += '<tr class="item">' +
                    '<td style="text-align: center;">' + data[i]["3"] + '%</td>' +
                    '<td style="text-align: left;"> ' + data[i]["1"] + '</td>' +
                    '</tr>';
        }


        $("#PN").append(html.toLowerCase());
    });



    //BAJO PAGO CONTACTUAL
    $.post("../ajax/boleta.php?op=listarventa", {idventa: idventa}, function (data, status) {
        data = JSON.parse(data);
        for (i = 0; i < data.length; i++) {
            $("#garantia" + (i + 1)).html(data[i]["1"].toLowerCase());
            $("#descgarantia" + (i + 1)).html(data[i]["6"].toLowerCase());
            $("#tipo_garantia" + (i + 1)).html(data[i]["3"].toLowerCase());
            $("#validez_garantia" + (i + 1)).html(formatofechacorta(data[i]["4"]));
        }
    });



    $("#contrato").html(document.getElementById("venta_contrato").checked ? "SI" : "");
    $("#retenciones").html(document.getElementById("venta_retencion").checked ? "SI" : "");
    $("#multas").html(document.getElementById("venta_multa").checked ? "SI" : "");
    $("#ordencompra").html(document.getElementById("venta_ocompra").checked ? "SI" : "");
    $("#garantias_equipo").html("");
    $("#mantencion_equipo").html("");



    //ASENSORES
    asc = document.getElementsByName("ascensor[]");
    for (i = 0; i < asc.length; i++) {

        id = $("#ascensor_" + i + "__codigo").val();

        $("#ide").append("<td style='text-align: center;'>" + $("#ascensor_" + i + "__codigo").val() + "</td>");
        $("#ken").append("<td  style='text-align: center;'>" + $("#ascensor_" + i + "__ken").val().toLowerCase() + "</td>");
        //$("#cuadro").append("<td>"+" "+"</td>");
        $("#unidades").append("<td  style='text-align: center;'>" + "1" + "</td>");
        $("#kg").append("<td style='text-align: center;'>" + $("#ascensor_" + i + "__capkg").val() + "</td>");
        $("#vel").append("<td style='text-align: center;'>" + $("#ascensor_" + i + "__velocidad").val() + "</td>");
        $("#paradas").append("<td style='text-align: center;'>" + $("#ascensor_" + i + "__paradas").val() + "</td>");
    }

}

function limpiarRevisa() {
    $("#venta_codigo1").empty();
    $("#proyecto_nombre1").empty();
    $("#venta_fecha1").empty();

    $("#venta_tipo1").empty();
    $("#venta_tipo2").empty();
    $("#venta_tipo3").empty();
    $("#venta_tipo4").empty();

    $("#imp_codigo").empty();
    $("#imp_nef").empty();
    $("#imp_proveedor").empty();

    $("#venta_cc1").empty();
    $("#venta_ken1").empty();

    //DATOS PROYECTO
    $("#proyecto_nombre2").empty();
    $("#proyecto_direccion1").empty();

    $("#contacto_ventas").empty();
    $("#contacto_ventas_email").empty();
    $("#contacto_ventas_fono").empty();

    $("#contacto_obra").empty();
    $("#contacto_obra_email").empty();
    $("#contacto_obra_fono").empty();

    //DATOS CLIENTE
    $("#mandante_razon").empty();
    $("#mandante_direccion").empty();
    $("#mandante_rut").empty();
    $("#mandante_representante").empty();
    $("#mandante_respresentante_rut").empty();

    ///INFORMACION CONTRACTUAL DE INSTALACION
    $("#rev_info").empty();

    ///FORMA DE PAGO CONTRACTUAL
    $("#PI").empty();
    $("#PN").empty();

    //BAJO PAGO CONTACTUAL
    $("#garantia1").empty();
    $("#descgarantia1").empty();
    $("#tipo_garantia1").empty();
    $("#validez_garantia1").empty();
    $("#garantia2").empty();
    $("#descgarantia2").empty();
    $("#tipo_garantia2").empty();
    $("#validez_garantia2").empty();

    $("#contrato").empty();
    $("#retenciones").empty();
    $("#multas").empty();
    $("#ordencompra").empty();
    $("#garantias_equipo").empty();
    $("#mantencion_equipo").empty();

    //ASENSORES
    $("#ide").empty();
    $("#ide").append('<td style="text-align: left;">IDE</td>');
    $("#ken").empty();
    $("#ken").append('<td style="text-align: left;"># KEN</td>');
    //$("#cuadro").append("<td>"+" "+"</td>");
    $("#unidades").empty();
    $("#unidades").append('<td style="text-align: left;">Unidades</td>');
    $("#kg").empty();
    $("#kg").append('<td style="text-align: left;">Carga (KG)</td>');
    $("#vel").empty();
    $("#vel").append('<td style="text-align: left;">Velocidad (m/s)</td>');
    $("#paradas").empty();
    $("#paradas").append('<td style="text-align: left;">Paradas/Accesos</td>');
}

function printDiv(nombreDiv) {
    var contenido = document.getElementById(nombreDiv).innerHTML;
    var contenidoOriginal = document.body.innerHTML;

    document.body.innerHTML = contenido;

    window.print();

    document.body.innerHTML = contenidoOriginal;
}

///********
function mostrar(idventa) {

    //LLENA VENTA
    $.post("../ajax/venta.php?op=mostrar", {idventa: idventa}, function (data, status) {
        data = JSON.parse(data);
        mostrarform(true);

        $("#venta_idventa").val(data.idventa);
        $("#venta_tipo").val(data.idtventa);
        if (data.idtventa != 1) {
            $("#cartera").show();
        } else {
            $("#cartera").hide();
        }
        $("#venta_tipo").selectpicker('refresh');
        $("#venta_fecha").val(data.fecha);
        $("#venta_cc").val(data.idcentrocosto);
        $("#venta_cc").selectpicker('refresh');

        $.post("../ajax/centrocosto.php?op=mostar", {idcentrocosto: data.idcentrocosto}, function (datas) {
            datas = JSON.parse(datas);
            $("#proyecto_codigo").val(datas.codigo);
        });

        $("#venta_pedido").val(data.idimportacion);
        $("#venta_pedido").selectpicker('refresh');
        $("#venta_montoSI").val(data.montosi);
        $("#venta_monedaSI").val(data.idmonedasi);
        $("#ascensor_monedaSI").val($("#venta_monedaSI").find('option:selected').text());
        $("#ascensor_monedaSI1").val($("#venta_monedaSI").find('option:selected').text());
        $("#venta_monedaSI").selectpicker('refresh');
        $("#venta_montoSN").val(data.montosn);
        $("#venta_monedaSN").val(data.idmonedasn);
        $("#ascensor_monedaSN").val($("#venta_monedaSN").find('option:selected').text());
        $("#ascensor_monedaSN1").val($("#venta_monedaSN").find('option:selected').text());
        $("#venta_monedaSN").selectpicker('refresh');

        if (data.contrato == 1) {
            $("#venta_contrato").attr('checked', true);
        }
        if (data.retenciones == 1) {
            $("#venta_retencion").attr('checked', true);
        }
        if (data.multas == 1) {
            $("#venta_multa").attr('checked', true);
        }
        if (data.oc == 1) {
            $("#venta_ocompra").attr('checked', true);
        }
        $('input.flat').iCheck({checkboxClass: 'icheckbox_flat-green', radioClass: 'iradio_flat-green'});

        if (data.permiso == '00-00-0000') {
            $("#venta_fecedifica").val("");
        } else {
            $("#venta_fecedifica").val(data.permiso);
        }

        if (data.garantiaex == 1) {
            $("#op_garantia").click();
            $("#venta_garantiaex").val(data.garantiaex);
            $("#mes_garantia").val(data.mesesgarex);
        }
        if (data.mantencionpost == 1) {
            $("#op_mantencion").click();
            $("#venta_mantencionpost").val(data.mantencionpost);
            $("#mes_mantencion").val(data.mesesmantp);
        }




        $("#venta_idmandante").val(data.idmandante);
        $("#venta_idmandante").selectpicker('refresh');

        $.post("../ajax/mandante.php?op=listarContacto", {idmandante: data.idmandante}, function (data, status) {
            data = JSON.parse(data);
            tablamandante.rows().remove().draw();
            for (i = 0; i < data.length; i++) {
                tablamandante.row.add([
                    data[i]["nombre"],
                    data[i]["rut"],
                    data[i]["email"],
                    data[i]["telefono"]
                ]).draw(false);
            }
        });

        $("#venta_codigo").val(data.codigo);

        if (data.adicionales !== "") {
            $("#op_adicionales").prop("checked");
            $("#adi").show();
            $("#venta_adicionales").val(data.adicionales);
        }


        cambiapestana(2);
        cambiapestana(3);
    });

    //BOLETAS DE GARANTIA
    //mostrarBoleta(idventa);

    //PROYECTO
    $.post("../ajax/proyecto.php?op=mostrar", {idventa: idventa}, function (data, status) {
        data = JSON.parse(data);

        if (data) {

            $("#proyecto_nombre").val(data.nombre);
            $("#proyecto_tipo").val(data.tipoproyecto);
            $("#proyecto_tipo").selectpicker('refresh');
            $("#proyecto_region").val(data.idregion);
            $("#proyecto_region").selectpicker('refresh');

            $.get("../ajax/comunas.php?op=selectComunasReg", {id: data.idregion}, function (r) {
                $("#proyecto_comuna").html(r);
                $("#proyecto_comuna").val(data.idcomuna);
                $("#proyecto_comuna").selectpicker('refresh');
            });


            $("#proyecto_idtsegmento").val(data.idtsegmento);
            $("#proyecto_idtsegmento").selectpicker('refresh');
            $("#proyecto_idtclasificacion").val(data.idtclasificacion);
            $("#proyecto_idtclasificacion").selectpicker('refresh');

            //$("#proyecto_comuna").val(data.idcomuna);
            //$("#proyecto_comuna").selectpicker('refresh');
            $("#proyecto_calle").val(data.calle);
            $("#proyecto_numero").val(data.numero);
            $("#proyecto_idproyecto").val(data.idproyecto);
            $("#proyecto_codigo").val(data.codigo);

            $("#con_nom_obra").val(data.nom_obra);
            $("#con_rut_obra").val(data.rut_obra);
            $("#con_email_obra").val(data.email_obra);
            $("#con_telefono_obra").val(data.tele_obra);

            $("#con_nom_ven").val(data.nom_ven);
            $("#con_rut_ven").val(data.rut_ven);
            $("#con_email_ven").val(data.email_ven);
            $("#con_telefono_ven").val(data.tele_ven);

            cambiapestana(4);
        }
    });

    //ASCENSOR
    mostrarAscensor(idventa);

    //INFO
    //addinfo();
    mostrarInfo(idventa);

    //PAGO
    //mostrarSI(idventa);
    //mostrarSN(idventa);
}

/*** FIN FUNCION MOSTRAR **/

//###########################################################

/***GUARDA TODO EN LAS DIFERENTES TABLAS **/

function grabaVenta(e) {
    e.preventDefault();
    var formData = new FormData();

    //VALIDACIONES DE INFORMACION DE VENTA
    if ($("#venta_tipo").val() == "" || $("#venta_tipo").val() == 0) {
        bootbox.alert("Debe seleccionar un valor para TIPO VENTA");
        return false;
    }
    if ($("#venta_fecha").val() == "") {
        bootbox.alert("Debe ingresar un valor para la FECHA");
        return false;
    }
    //if ($("#venta_cartera").val("")){bootbox.alert("Debe ingresar un valor para");return false;}
    if ($("#venta_cc").val() == "" || $("#venta_cc").val() == 0) {
        bootbox.alert("Debe seleccionar un valor de CENTRO DE COSTO");
        return false;
    }
    if ($("#venta_pedido").val() == "" || $("#venta_pedido").val() == 0) {
        bootbox.alert("Debe seleccionar un valor de NRO. PEDIDO");
        return false;
    }
    if ($("#venta_montoSI").val() == "") {
        if (isNaN($("#venta_montoSI").val())) {
            bootbox.alert("Debe ingresar un monto valido para SUMINISTRO IMPORTADO");
            return false;
        }
    }
    if ($("#venta_monedaSI").val() == "" || $("#venta_monedaSI").val() == 0) {
        bootbox.alert("Debe seleccionar un valor de MONEDA");
        return false;
    }
    if ($("#venta_montoSN").val() == "") {
        if (isNaN($("#venta_montoSN").val())) {
            bootbox.alert("Debe ingresar un monto valido para SUMINISTRO NACIONAL");
            return false;
        }
    }
    if ($("#venta_monedaSN").val() == "" || $("#venta_monedaSN").val() == 0) {
        bootbox.alert("Debe ingresar un valor para MONEDA");
        return false;
    }

    if ($("#venta_idmandante").val() == "" || $("#venta_idmandante").val() == 0) {
        bootbox.alert("Debe seleccionar un valor para MANDANTE");
        return false;
    }


    //$idvendedor, $observaciones, 
    formData.append("idventa", $("#venta_idventa").val());
    formData.append("codigo", $("#venta_codigo").val());
    formData.append("idtventa", $("#venta_tipo").val());
    formData.append("idmandante", $("#venta_idmandante").val());
    formData.append("fecha", $("#venta_fecha").val());
    perm = ($("#venta_fecedifica").val() == "" ? '0000-00-00 00:00:00' : $("#venta_fecedifica").val());
    formData.append("permiso", perm);
    formData.append("idcentrocosto", $("#venta_cc").val());
    formData.append("idimportacion", $("#venta_pedido").val());
    formData.append("montosi", $("#venta_montoSI").val());
    formData.append("idmonedasi", $("#venta_monedaSI").val());
    formData.append("montosn", $("#venta_montoSN").val());
    formData.append("idmonedasn", $("#venta_monedaSN").val());

    if ($("#venta_mantencionpost").val() == 1) {
        if ($("#mes_mantencion").val() == "") {
            new PNotify({
                title: 'Ops!',
                text: 'Debe completar los meses de mantención.',
                type: 'info',
                styling: 'bootstrap3'
            });
            $("#mes_mantencion").focus();
            return false;
        }
    }
    formData.append("mantencionpost", $("#venta_mantencionpost").val());
    formData.append("mesesmantp", $("#mes_mantencion").val());


    if ($("#venta_garantiaex").val() == 1) {
        if ($("#mes_garantia").val() == "") {
            new PNotify({
                title: 'Ops!',
                text: 'Debe completar los meses de garantía.',
                type: 'info',
                styling: 'bootstrap3'
            });
            $("#mes_garantia").focus();
            return false;
        }
    }
    formData.append("garantiaex", $("#venta_garantiaex").val());
    formData.append("mesesgarex", $("#mes_garantia").val());

    formData.append("contrato", (document.getElementById("venta_contrato").checked ? 1 : 0));
    formData.append("retenciones", (document.getElementById("venta_retencion").checked ? 1 : 0));
    formData.append("multas", (document.getElementById("venta_multa").checked ? 1 : 0));
    formData.append("oc", (document.getElementById("venta_ocompra").checked ? 1 : 0));

    formData.append("adicionales", $("#venta_adicionales").val());


    formData.append("idvendedor", 1);
    formData.append("condicion", 1);


    bootbox.confirm("NO se encontraron errores. ¿Desea grabar esta venta?", function (result) {
        if (result) {
            //GRABA VENTA
            $.ajax({
                url: '../ajax/venta.php?op=guardaryeditar',
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,

                success: function (datos) {
                    if (datos != '0') {
                        $("#btnGuardar").prop("disabled", false);
                        new PNotify({
                            title: 'Correcto!',
                            text: 'Datos de venta guardados con exito.',
                            type: 'success',
                            styling: 'bootstrap3'
                        });
                        mostrar(datos);
                        cambiapestana(2);
                        cambiapestana(3);
                    } else {
                        $("#btnGuardar").prop("disabled", true);
                        new PNotify({
                            title: 'Error!',
                            text: 'Datos de venta no guardados.',
                            type: 'error',
                            styling: 'bootstrap3'
                        });
                    }
                },
                error: function (datos) {
                    bootbox.alert(datos);
                }
            });
        }
    });

}

function grabaProyecto(e) {
    e.preventDefault();
    var formData = new FormData();

    // $idsupervisor, $idpm, $codigo, $estado,  $created_user
    formData.append("idventa", $("#venta_idventa").val());
    formData.append("nombre", $("#proyecto_nombre").val());
    formData.append("tipoproyecto", $("#proyecto_tipo").val());
    formData.append("idregion", $("#proyecto_region").val());
    formData.append("idcomuna", $("#proyecto_comuna").val());
    formData.append("calle", $("#proyecto_calle").val());
    formData.append("numero", $("#proyecto_numero").val());
    formData.append("idproyecto", $("#proyecto_idproyecto").val());
    formData.append("idtsegmento", $("#proyecto_idtsegmento").val());
    formData.append("idtclasificacion", $("#proyecto_idtclasificacion").val());


    //INFORMACION CONTACTO VENTA
    formData.append("nombre_ven", $("#con_nom_ven").val());
    formData.append("rut_ven", $("#con_rut_ven").val());
    formData.append("email_ven", $("#con_email_ven").val());
    formData.append("telefono_ven", $("#con_telefono_ven").inputmask('unmaskedvalue'));

    //INFORMACION CONTACTO OBRA
    formData.append("nombre_obra", $("#con_nom_obra").val());
    formData.append("rut_obra", $("#con_rut_obra").val());
    formData.append("email_obra", $("#con_email_obra").val());
    formData.append("telefono_obra", $("#con_telefono_obra").inputmask('unmaskedvalue'));

    formData.append("idsupervisor", null);
    formData.append("idpm", null);
    formData.append("codigo", $("#proyecto_codigo").val());
    formData.append("estado", 1);
    formData.append("closed_user", null);
    formData.append("closed_time", null);


    bootbox.confirm("NO se encontraron errores. ¿Desea grabar este proyecto?", function (result) {
        if (result) {
            //GRABA VENTA
            $.ajax({
                url: '../ajax/proyecto.php?op=guardaryeditarNUEVO',
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,

                success: function (datos) {
                    if (datos != '0') {
                        $("#btnGuardar").prop("disabled", false);
                        new PNotify({
                            title: 'Correcto!',
                            text: 'Proyecto guardado con exito.',
                            type: 'success',
                            styling: 'bootstrap3'
                        });
                        cambiapestana(4);
                    } else {
                        $("#btnGuardar").prop("disabled", true);
                        new PNotify({
                            title: 'Error!',
                            text: 'Proyecto no guardado.',
                            type: 'error',
                            styling: 'bootstrap3'
                        });
                    }
                },
                error: function (datos) {
                    bootbox.alert(datos);
                }
            });
        }
    });
}

function grabaAscensores(e) {
    e.preventDefault();
    formData = new FormData();
    var sw = true;
    var a = document.getElementsByName("ascensor[]");
    var text = "";

    for (i = 0; i < a.length; i++) {

        formData.append("idventa", $("#venta_idventa").val());

        formData.append("codigo", $("#ascensor_" + i + "__codigo").val());
        formData.append("idascensor", $("#ascensor_" + i + "__idascensor").val());
        formData.append("idtascensor", $("#ascensor_" + i + "__idtascensor").val());
        formData.append("marca", $("#ascensor_" + i + "__marca").val());
        formData.append("modelo", $("#ascensor_" + i + "__modelo").val());
        formData.append("ken", $("#ascensor_" + i + "__ken").val());
        formData.append("pservicio", null);
        formData.append("gtecnica", null);
        formData.append("valoruf", $("#ascensor_" + i + "__valoruf").val());
        formData.append("valorclp", $("#ascensor_" + i + "__valorclp").val());
        formData.append("paradas", $("#ascensor_" + i + "__paradas").val());
        formData.append("capkg", $("#ascensor_" + i + "__capkg").val());
        formData.append("capper", $("#ascensor_" + i + "__capper").val());
        formData.append("velocidad", $("#ascensor_" + i + "__velocidad").val());
        formData.append("dcs", $("#ascensor_" + i + "__dcs").val());
        formData.append("elink", $("#ascensor_" + i + "__elink").val());

        formData.append("idedificio", null);
        formData.append("idcontrato", null);
        formData.append("ubicacion", "PENDIENTE");
        formData.append("codigocli", "ASC " + (i + 1));

        $.ajax({
            url: '../ajax/ascensor.php?op=guardaryeditar',
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            success: function (datos) {
                if (datos != '0') {
                    $("#btnGuardar").prop("disabled", false);
                    new PNotify({
                        title: 'Correcto!',
                        text: 'Ascensor guardado con exito.',
                        type: 'success',
                        styling: 'bootstrap3'
                    });
                    sw = true;
                } else {
                    $("#btnGuardar").prop("disabled", true);
                    new PNotify({
                        title: 'Error!',
                        text: 'Ascensor no guardado, verifique que no haya sido ingresado anteriormente.',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                }

            },
            error: function (datos) {
                text = datos;
            }
        });
    }

    if (sw == true) {
        cambiapestana(5);
    } else {
        bootbox.alert("ERROR");
    }
}

function grabaInfo(e) {
    e.preventDefault();
    formData = new FormData();
    var sw = true;
    var g = document.getElementsByName("info[]");

    for (i = 0; i < g.length; i++) {
        formData.append("idinfoventa", $("#info" + i + "__idinfoventa").val());
        formData.append("descripcion", $("#info" + i + "__descripcion").val());
        formData.append("fecha", $("#info_" + i + "__fecha").val());
        formData.append("tipo", $("#garantia_" + i + "__tipo").val());
        formData.append("validez", $("#garantia_" + i + "__validez").val());
        formData.append("ejecutada", $("#garantia_" + i + "__ejecutada").val());

        $.ajax({
            url: '../ajax/boleta.php?op=guardaryeditar',
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,

            success: function (datos) {
                sw = true;
            }
        });
    }

    if (sw == true) {
        bootbox.alert("Grabado exitosamente");
        cambiapestana(3);
    } else {
        bootbox.alert("ERROR");
    }
}

function enviararevision(idventa, estado) {
    var formData = new FormData();
    formData.append("idventa", idventa);
    formData.append("estado", estado);

    bootbox.confirm("Va a enviar este memo de ventas a revisión. ¿esta seguro?", function (result) {
        if (result) {
            $.ajax({
                url: '../ajax/venta.php?op=cambiarestado',
                type: "POST",
                data: formData,
                async: false,
                cache: false,
                timeout: 30000,
                contentType: false,
                processData: false
            }).done(function (datos) {
                if (datos != 0) {
                    new PNotify({
                        title: 'Correcto!',
                        text: 'Memo de Venta enviado a revision.',
                        type: 'success',
                        styling: 'bootstrap3'
                    });
                    listar();
                } else {
                    new PNotify({
                        title: 'Error!',
                        text: 'Memo no enviado, por favor intentar de nuevo mas tarde.',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                }
            });
        }
    });

}

function cambiapestana(numero) {
    idventa = $("#venta_idventa").val();
    switch (numero) {
        case 1:
            document.getElementById("step-11").style.backgroundColor = "#E5FFE3";
            $("#venta").empty();
            $("#venta").append('<i class="glyphicon glyphicon-ok"></i> VENTA');
            break;
        case 2:
            document.getElementById("step-22").style.backgroundColor = "#E5FFE3";
            $("#step-22").empty();
            $("#step-22").append('<a href="#step-2" id="garantia" role="tab" data-toggle="tab" aria-expanded="true" onclick="mostrarBoleta(' + idventa + ');"><i class="glyphicon glyphicon-ok"></i> GARANTIA</a>');
            break;

        case 3:
            document.getElementById("step-33").style.backgroundColor = "#E5FFE3";
            $("#step-33").empty();
            $("#step-33").append('<a href="#step-3" id="proyecto" role="tab" data-toggle="tab" aria-expanded="true"><i class="glyphicon glyphicon-ok"></i> PROYECTO</a>');
            break;

        case 4:
            document.getElementById("step-44").style.backgroundColor = "#E5FFE3";
            $("#step-44").empty();
            $("#step-44").append('<a href="#step-4" id="proyecto" role="tab" data-toggle="tab" aria-expanded="true" onclick="mostrarAscensor(' + idventa + ');"><i class="glyphicon glyphicon-ok"></i> ASCENSORES</a>');
            break;

        case 5:
            document.getElementById("step-55").style.backgroundColor = "#E5FFE3";
            $("#step-55").empty();
            $("#step-55").append('<a href="#step-5" id="proyecto" role="tab" data-toggle="tab" aria-expanded="true" onclick="mostrarInfo(' + idventa + ');"><i class="glyphicon glyphicon-ok"></i> INSTALACION</a>');
            break;

        case 6:
            document.getElementById("step-66").style.backgroundColor = "#E5FFE3";
            $("#step-66").empty();
            $("#step-66").append('<a href="#step-6" id="proyecto" role="tab" data-toggle="tab" aria-expanded="true" onclick="mostrarSI(' + idventa + '); mostrarSN(' + idventa + ');"><i class="glyphicon glyphicon-ok"></i> PAGOS</a>');
            break;

        case 7:
            document.getElementById("step-77").style.backgroundColor = "#E5FFE3";
            $("#step-77").empty();
            $("#step-77").append('<a href="#step-7" id="proyecto" role="tab" data-toggle="tab" aria-expanded="true" onclick="revisar();"><i class="glyphicon glyphicon-ok"></i> REVISIÓN</a>');
            break;

    }
}

/*** FIN GUARDA TODO EN LAS DIFERENTES TABLAS **/

//###########################################################


function final() {
    //INFO
    if (countinstalacion > 0) {
        finalizar = true
    } else {
        finalizar = false
    }

    //PAGOS
    if (sumSI == 100) {
        finalizar = true
    } else {
        finalizar = false
    }

    if (sumSN == 100) {
        finalizar = true
    } else {
        finalizar = false
    }

    if (finalizar == true) {
        $("#fin").attr("disabled", false);
    }
}

function volverallistado() {
    location.reload();
    new PNotify({
        title: 'Correcto!',
        text: 'Memo ventas finalizado correctamente.',
        type: 'success',
        styling: 'bootstrap3'
    });
}



function pdf(idventa) {

    $.post("../ajax/venta.php?op=PDF", {idventa: idventa}, function (data, status) {
        data = JSON.parse(data);
        console.log(data);
        var company_logo = {
            firma: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAALqnpUWHRSYXcgcHJvZmlsZSB0eXBlIGV4aWYAAHjarZhpchyxjoT/8xRzBG4gyONwjZgbzPHnA7skWbJk+02M2u7qroULkMhMtNv/89/H/Rd/KZXssmgtrRTPX265xc6H6l9/r2Pw+b7fv5Kfa+Hzefd+IXIqcUzPA/u5v3NePh7Q5/4wPp93Op9x6jPQc+FtwGQzRz4899VnoBRf58Pz3bXnuZ5/2c7z/8x4L8t4Xfr6PSvBWMJ4Kbq4U0ie9ztLYgWpps5ReU/Jbgqc70n4Zu/6fezc+8cvwXv/9CV2vj/n0+dQOF+eG8qXGD3ng3wfuxuhX1cUPmb+dMGvt/B+E7uz6jn7tbueC5Eq7tnU21buJ24knDndxwov5b/wWe+r8apscZIxm27wmi60EIn2CTms0MMJ+x5nmCwxxx2VY4wzpnuuJo0tzpuUbK9woqaWliNHMU2yljgd39cS7rztzjdDZeYVuDMGBrMs/vZy3538v7zeBzrHYhuCr++xYl3RMM0yLHP2zl0kJJwnpnLje1/uF9z4XxKbyKDcMFc22P14DTEkfGAr3Twn7hOfnX+VRtD1DECImFtYTEhkwJeQJJTgNUYNgThW8tNZeUw5DjIQROIK7pAb+ITk1Ghz84yGe2+U+DoNtZAISYWyqWSok6ycBfxormCoS5LsRKSISpUmvUBQRUopWoyjuibNKlpUtWrTXlPNVWqpWmtttbfYEhQmrTR1rbbWemfSztCdpzt39D7iSCMPGWXoqKONPoHPzFNmmTrrbLOvuNKi/FdZ6lZdbfUdNlDaecsuW3fdbfcD1k46+cgpR0897fT3rD1Z/Zy18CVzf85aeLJmGcv3Pv3IGqdV34YIRidiOSNjMQcyrpYBAB0tZ76GnKNlznLmW6QoJJK1IJacFSxjZDDvEOWE99x9ZO6PeXOS/6O8xZ8y5yx1/x+Zc5a6J3O/5+2brK1+FSXdBFkVWkx9OhAbN+zaY+2mSX8/NsJEXaxCdccuLa61U67gqMnMRkqkp8V8YmHb9WSibRSaahef0pr+sA2ebZxPfnUNzesmPHWjidFJ3QyZcyNJjFgShCeMmbtvYdZekNqWJW54MCpjAQxKOosp4y9H9/XEn49xe+ZdpG7Cqax4L16y53RnjsbJtWvogxXHlv08ILjtRPYzGt1viAjx+nGe1t3XYMo8YaXWfZ06FtmZYY0xPCGZxcK3xhpzrNYOYW92UyLu6op/TRj+YX+rE9As5hxGDYNxiNwC9SzC/ZjqRGCZsNjqbgI/Dd1NKCHJvsLIFhvcCKtZb4F4X1coZYJjXZnyCzJiORiRNQWZ07BHS0d6X8JVhYsHA2k15FCbsDgY6Kxik/5TetmrxAa8Yx9l28aYir3V5t/i/3F0z4f0rwAIEHQ/i2pYvjHbSRTdVnXapIIMgFG07gEC89hy2jzU3T5xaFhtnH1KGoXPm7Tl0aQsSvMVzwwdBPeGtBqB7xOdSMUXCh9miLDKqLLGJr5hwkcn2YyTooF+gN3sAikud5jh7EnodODj1p61ADxKaRAPw+8LPlgNm/PmnuO4MCCyJBjYCun/dP0eO8n2+XSJKin3Za5on4v98IAj//aUez6AKjBWKY5d2m8p+f0o2CzowAzZ0LpGcxs06frpYZbXTH7EqDOgxZWlQa/M+eQROl/mO1xCKhpxiG1kAIQHQznU5wXxjime3A5WOkbk+ZC1t5N3Ext7JJhzaSrYqOXwCRGQtaVlVsovGQsxbTLShoxXh1zXiavJ0N1fMYl4uS8V5f6JXTP5gQXLsMaFvYC1WnXuurRT/7Pv7aAitqQSKK16EqpR2haTOMGiowkJ73e0o1BkOY3dGGkOROIVxzdcuF9TSFxoj3zrMWdtxC6EkgPEXcRE/AjVugqiNahUiw5InBTXiawo10B0EEYSEaYRqXaPtCFTJGpSFroOvgo/n+NEGTMS3A2zpup+W73MZnzEO+jHCvT1z4X7zdFdD9FMdFKDXTSzcsMV5iDvbFz7VwY1DjRkk5mNtccaGPPlgqfGlBj+9pFEt7AzxbjP0AItxMFH3CSUPaCMszEPyOSE2CDe0+DDNFtF+MuaSH9Y8OCZK23REQpFNjrUMft5gPQVJ3B2y8G4KFyV6nQNe0/lUrk0oOv7Iv2paP9249UBIJ6PYnKisLOwkeV2Ug/SMKPAr/qTKftj0/81IKehq3Bd7nvhd/wZdFlQr1unQOuwq7a1mSwYA4Z6Zq9AZNIJnXloGeKyQZYfOa5R1eBCUZzasUO1p+ba3mEydQd49Y2DP6UYWPhhFpPK31Z2AnvmzQZLXvAdtAECHAqFcCVBAHgWpxFYnK8YmQSQvOSmg1iYQgDzFUcMWg883AHfzmTpzA1POFh1zuMnbBMhdzwdvgf/QGOG/FEqHQmYRj6okCKt0cgeJ9isLnOBqYBHKS5NigQIg86OVqygcXgLBvQwkIwJj2JfvarRt5ovC2WtZkinEkbAS5CT4mg8kawga1aNOOc1w8mYBWoEfa1P+uMfCdyO7suJOM03JYHHGzupYefJEiu8gO6c03dLe+HdjtFCBVuGB1XSD9ImbyUh8RRA6DoBDw4e/k1s4mBFVEyDsznRHiyrZgab1YMxx60S1+CX2SzVm3Dzj0Bh2mdDAhPlAvHXtHy9pmIngHrK3KWDyQGtb53jJDmm/dl6QWSKXnzXyIy4kOU7fcCxsrMSpZnYaK953LlikJcLgnSHNswC1ZJcbWsRbELfu5mBfbAJsQ6ZIVp24FL2vL2xLOAsN5LFNNu00ABar1668dWL/fUYzbhArt56GYh5l8py3Ue48JaE3G63UBIYm/KLUL/4hniqH1QyTi6xqMk1B++sKigzDg7VO2FKnLVSkR7jVitbo6Aosw4EgpHDdZ+rhoTM5XecfeCIDLPLFwXFK/b/2JvkW8ru6Q+qqf9rQLPUFtw01nVebATfVVRw8fRwVpjcvbHH2mbFJwyYZrjKA/XQf0gHUihqY4rVoIN5cyzmdcxMh0gbQvUgjlKAlezccKWNTiKPFF2hb0MrjSIXgxVoD7fHGLRvSPWNsU65thKZKJfkBl7vjHSNJZ4OTvIOgwULTtrOOCkiE3Pr3l/r1m6tH66zAmo2KBtEN4bAJIWxGRn7jXSskxxsBztaU4ECWYOV4Jqtw2ZuFzfoNrJUygi0pILDKJUOdPZysu+IqPWT87iw4dzcMCxIJmy5aGqpuZd8v6z5vooF0bGpm1DPLu3bNud263ereyn/TyokrUJjNroti+bgR6vspBiEM8VN85wQXGq8EWfrwqcxfriesX82yA/kzLOnyLomZtRMdCac0wPyGAwzitQ+1DWGRqMugmrCdplLjbkWpo09W7MIVPDZWM+TXqI9uXBbpzotPtkcdyUKxmi32F4T0iBc6oUwEEDzn/ajCYD8sMZg+aXPdK7YUmuyHyNcvhgAfdigG4bu487/ldbtLcM8qFDeKHeEuAVt7UaY0G6VCizx2UDhsu0edA0sAzbNMiOLWlAiDYrptlpTdw37N1psR/fhuwwW1+0f+4FnImgYOCrAftSMxZoctXLGkIyGd2/WC9Kr+xNjTNVNbkaLU8WdCbY/b9ZVTUkWfTMV+cT9UitB04HQW29DQVDcMxSgPMJ0gWnWut2+rBRovCMYozmEwQoKQIPQtdAi2y+1ecenjn/ru9wfTdZVjfsZikAoC/bYQrwg0/Zq1AmuNWA0fuOjz7HfU5AS4DH9/lkVIvaXoVn3yVcDrkC+WqlABCilak16r+AO954hF/upA6kG6kDvFbAfjID7a4v3qXsrhtmROQXG336E8JCld1YbhmH7oQSk+4b3iZP2R+Cv6e2XEDiuz9Za0E4VYxUn/RbO1Ix4RrJHPjIdBJ8IIAYvacEpWLVjA8dY5naWtfj4toVLgkckz4x1U8Jo7IflgDSbblndJVPKvKn9vPCrxlz4MHprTljGKgsp1jLBKZixd1r5qk3uZ9E6LAbo/y+ZDI6MQRnmYAAAEOBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+Cjx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IlhNUCBDb3JlIDQuNC4wLUV4aXYyIj4KIDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+CiAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgIHhtbG5zOmlwdGNFeHQ9Imh0dHA6Ly9pcHRjLm9yZy9zdGQvSXB0YzR4bXBFeHQvMjAwOC0wMi0yOS8iCiAgICB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIKICAgIHhtbG5zOnN0RXZ0PSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VFdmVudCMiCiAgICB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIKICAgIHhtbG5zOnBsdXM9Imh0dHA6Ly9ucy51c2VwbHVzLm9yZy9sZGYveG1wLzEuMC8iCiAgICB4bWxuczpHSU1QPSJodHRwOi8vd3d3LmdpbXAub3JnL3htcC8iCiAgICB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iCiAgICB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iCiAgIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6Q0MxRTdERTM2NkIzMTFFNkFCRjlGOTU5ODYyNkQ1N0MiCiAgIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NmM0OTBlMGYtNzBlZi00OTIwLTkxNzctMjNjZWVmMjFjZWJjIgogICB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6MDc1ZDQ1ZjctODliNy00YzY0LWE1NTctMjBhNmRiNmQ4YjRkIgogICBHSU1QOkFQST0iMi4wIgogICBHSU1QOlBsYXRmb3JtPSJMaW51eCIKICAgR0lNUDpUaW1lU3RhbXA9IjE1MzI5Njk2NDM5MDQ2MzMiCiAgIEdJTVA6VmVyc2lvbj0iMi4xMC4wIgogICBkYzpGb3JtYXQ9ImltYWdlL3BuZyIKICAgeG1wOkNyZWF0b3JUb29sPSJHSU1QIDIuMTAiPgogICA8aXB0Y0V4dDpMb2NhdGlvbkNyZWF0ZWQ+CiAgICA8cmRmOkJhZy8+CiAgIDwvaXB0Y0V4dDpMb2NhdGlvbkNyZWF0ZWQ+CiAgIDxpcHRjRXh0OkxvY2F0aW9uU2hvd24+CiAgICA8cmRmOkJhZy8+CiAgIDwvaXB0Y0V4dDpMb2NhdGlvblNob3duPgogICA8aXB0Y0V4dDpBcnR3b3JrT3JPYmplY3Q+CiAgICA8cmRmOkJhZy8+CiAgIDwvaXB0Y0V4dDpBcnR3b3JrT3JPYmplY3Q+CiAgIDxpcHRjRXh0OlJlZ2lzdHJ5SWQ+CiAgICA8cmRmOkJhZy8+CiAgIDwvaXB0Y0V4dDpSZWdpc3RyeUlkPgogICA8eG1wTU06SGlzdG9yeT4KICAgIDxyZGY6U2VxPgogICAgIDxyZGY6bGkKICAgICAgc3RFdnQ6YWN0aW9uPSJzYXZlZCIKICAgICAgc3RFdnQ6Y2hhbmdlZD0iLyIKICAgICAgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDpkYzI3YTlmZS1hYTBlLTRkM2ItOGU1Yi0zMjhiNDJkYTk1ZTIiCiAgICAgIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkdpbXAgMi4xMCAoTGludXgpIgogICAgICBzdEV2dDp3aGVuPSItMDQ6MDAiLz4KICAgICA8cmRmOmxpCiAgICAgIHN0RXZ0OmFjdGlvbj0ic2F2ZWQiCiAgICAgIHN0RXZ0OmNoYW5nZWQ9Ii8iCiAgICAgIHN0RXZ0Omluc3RhbmNlSUQ9InhtcC5paWQ6NGNiMGYzOGYtMWJhMC00MDdjLTgyODktMGE0ZGVlMDM4ZTY0IgogICAgICBzdEV2dDpzb2Z0d2FyZUFnZW50PSJHaW1wIDIuMTAgKExpbnV4KSIKICAgICAgc3RFdnQ6d2hlbj0iLTA0OjAwIi8+CiAgICA8L3JkZjpTZXE+CiAgIDwveG1wTU06SGlzdG9yeT4KICAgPHhtcE1NOkRlcml2ZWRGcm9tCiAgICBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkNDMUU3REUxNjZCMzExRTZBQkY5Rjk1OTg2MjZENTdDIgogICAgc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpDQzFFN0RFMDY2QjMxMUU2QUJGOUY5NTk4NjI2RDU3QyIvPgogICA8cGx1czpJbWFnZVN1cHBsaWVyPgogICAgPHJkZjpTZXEvPgogICA8L3BsdXM6SW1hZ2VTdXBwbGllcj4KICAgPHBsdXM6SW1hZ2VDcmVhdG9yPgogICAgPHJkZjpTZXEvPgogICA8L3BsdXM6SW1hZ2VDcmVhdG9yPgogICA8cGx1czpDb3B5cmlnaHRPd25lcj4KICAgIDxyZGY6U2VxLz4KICAgPC9wbHVzOkNvcHlyaWdodE93bmVyPgogICA8cGx1czpMaWNlbnNvcj4KICAgIDxyZGY6U2VxLz4KICAgPC9wbHVzOkxpY2Vuc29yPgogIDwvcmRmOkRlc2NyaXB0aW9uPgogPC9yZGY6UkRGPgo8L3g6eG1wbWV0YT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgIAo8P3hwYWNrZXQgZW5kPSJ3Ij8+HW1V7gAAAAZiS0dEAP8A/wD/oL2nkwAAAAlwSFlzAAAuIwAALiMBeKU/dgAAAAd0SU1FB+IHHhA2A5mNjyoAAAvlSURBVHja7V1dbBxXFf7OvfOzu17b4zrkv/aGpEJQUByJH6V5yEQVQm0eSAQVgqeVIEJVQTIPwBPqIB6rOq5aVAkQdiueKiobyUW8wLqQRgiK1hGoD1HLOmqEUFBZE1VJam+8POzdzfV4Zmd2997ZWdtHmofEszPnnu/ec8797rl3gD3Zkz0JFxoEJc/bj5lvfHT1zAiyx27j7mQGZuEeNiYfNIJQR33LbzIwb9zDxuoIsjdu427lvP3YW298dHVjD/Iu5CiNHx+CfYmB5jnYdQB1FRcHu85A80OwLx2l8eN7lm4vFzIwXwJQUQVAjKsi3nlhz2U1pMDBvkvAV2rYnIxx/wqAtRysN+9gHQAwBPvDPGWvWTBz79f/c6p5Yw4W7mD9LAAHwFTUgw2wG3Xg9fvYfBHA6m4bDa4F/tt2vZeB3rVhvAygGMegMWQKQDED8yUGerfdu4Vu7q4AAkApzBAG+J8BTHOwgm5FxDumxTvDwCntVGAKYUAwUMUE98Q9fdPPBPcYqNIGmMLAo3DG+IwNwAtqZA7WX4U7SpsUhW5BwHiiTQMpUyb4P4IyGwe5r6ZdeaHjthFjgr+jKKYlmbrRswFAVHOwntFmv4avV24ooXPV3x7RRqS/YwXECgPstTxsR+M7y9Lkb078nzLJw3YMsNdCYouTWhcVMMSrBNI66TLB5wM6wIqO0SLa4h8tldS5sFFkpwIULT/Cjoxpfu/FsJTVBF+zYSh3kY+wI2M2jGV/x3OQSw0oRQaqBcUMnT0nD7sQ5NsD0upF1W5laWIGBPzS955aGrLGYoRBtIFigl9FfFJxdZzyp1TrkIEZ1P5iv9zU2ZgGUQ6K1ZhIdkwm5mB9P4lOKWyTeACvdmAMZaBwMBc9MLwEKh0gx9EMilZ3HZRmVrswhgolHRN8Fb3T7tVhZNwEQEkkJS73YoheQBEBWtl6yBBsb2liRmVa7AelrBWJIdieit7ZDSgZmE9DwyIVA72dhamMOLRhPO8HXRcerkJDdArKFAdbg76Vw2oGprIJbBamfyS7yuOGJflu4cfLSYCyNDFDBCojgSXdDMxZRWzullgnbKcunhhgP99KTXDXzyHpAoWDzSK5NXZlfl/YqPVcG8asFldlgj8XRuypBiUP+4mEwVDqYgg0o/y5THIX1CDSnHZsqypQJml/t+l1r0G+9igrqOLgHJJWIRloRWluvY+GH49DgasAhUClfowOC3xaZfAdp/zjKqmVio/7j5owKgFlCNZ0f8AwljRlqCUfXd/76DhEY3HSVFWg9CNuVB3ktMyshe16HiUViSx7pUNqpdwno3Z9jSLnQqOMNGzY3SgZQfZLW2eb1pku+K6BAYWBLuvmm4QNW+8UNo4tC5Kyyz2QkIMASlklnxWRsS5L712I9aNjdHBMYVaQalAIVE24Cn5LXBa2jhQ5w6kq6D2pBSUHq5ggGBC2lJOW6BTbhvEn6QdzCtdQUgUKB/s1+iNzUpp9JY7hWkpnYZ5XvLCVClBEDW9f6qkC6KBwPYaReco3H1DOGqcBlP00egr9lZbbEjaXAr8kG7h/VsoIfq9BkTUA59DYeNMXIdCPb9X/V+4nGrJtZZsHKVuWAt63NerUl5FigC0jBTIE+5KU6QV3jizMUV8DdFdNJA1K9SjtO4R0yJQvVo9GrnskpFhioFjgqdnYKSRwnYRJ6e5p6eakhvaaCV7T/ZIcrBfWcX8xZYAsS97pC9sA4WAHpJtXk9DIAp/ewP3P6nyHAXbtDtY9pE9Wpdh9aBsgd7B+Urr5hm5tDtNDx2rYfFYzGHdr2CyK7C5tciPI9uxBbzVavMooctd0a3OrvnZ5E3VHIxi/qWHzG/1MsduJbGNbsn1kkElicqQjo0L69wEGJlGsj7n4C5qztxIGbXNmn0cIbBjPmOBru3SkhE8zBC3c+uPSxAzXqclBGhuWJ0liT+CuAmVpYob5bN7WZWmbGJ5gh4f+Xa/+kYGWRe3VSg2bLgO9ssvcVz22y2IgbTuAsjDn5d47RkPnmn8bQfYpjcE+VSPl0+zYx6OYkSRiyByCd8w+1xyyh2jsmEY6JU2guP0GZC7CWOWHKF9oxjQGCiyyJuC/OwSU9oD4sh2lgHCwX8Q11ggyLRJwGJknZBdmgs9nYY4y0Ns7AJQWIFbD9tukVe6Yh62svvUEO/xNdL5P42URjPEJ9vBYHvb3sjCLTbe2E0ARNg4v081s3fXj9QsMKbFYmaT9o22Sg0EHxZM64GLbGxBdWB0pJvjPejRWpA6DDAoBf2g7AHKwvg4FFdoxA3jktY+GCzHT6EEFpSKFiK8FBd6TiFueEj77JANsvlcw8rB/0uHcZtBA2VJuJWwfNIyoldE4lPtiJ2/YR8NxlmPLANyI/eZdbbgfJFCEbVvlrKE3ig0rTdSe74AkdGPMsKvNeYav/st/TkjX1S6DAorYpxm9SUgcztLRblSxuzSyocNifmGBOwb4v0Luq/RaSzwgoMjlVm0PxPHv9IkKrFOIV9Q8K43CxTaj46IivizNoBTQSbmVCX5Tyo+fbndvBmYkIAQqn2CHh8RoCt07aIIvKyYxUwmKfESIsHVHKWsct9VuctdqUBR4YqM9dgEocuITvbtAHFskp6CFCJ4qdMUv2zhxrZFYtPnqQQ+7tAYKFGFLOa7G7oQVKduKOhKiFMJHzUuB/1ftGiyviexkUHzHhHQ0+fY6mBeUEHz6syN6RbFdQ20YiZTppAAU/8kU8fnCSdp/QFbEodx3OgHEAGsqHedYpWJSXEU/QRE2bD1H2Lg7PsoEX12amKE4gBhgTereYdHHKlXbPHfHgLI0MUO+owm72ipYQIyzzq1GutrMlBalhv80RsPm0AdJGpSAs/AL3eoup8BVC9wJS3tN6ZAuseoXZ1uy1i0COVhTImHwRFtKDFRloNoocj9KAhRhs6qqTljwzbhfDANknIY/J0jGQgxeq26BryhyVwUALgebzsHyhAuN9WExG8ZJ3aAMNWymZHQAAIzthxbLubMreronfCXCGF8xSSwB8DhYsRNGVwDnigTAY6BFa/v5691sjS5odl+urwN4Kka+45uXXJeM6cqTOtpeKVIBcFkc1h+HVphykNvmYqChJEg+ck8VKAz05S2JFfBPny0cVUHwfBhZGMTxc7B5QckHrJuMHGm4GJoWPabkU7znywT/UIDZbhQ5GgJ9y4MIG8mHzailhrIwt7zAQe6ij4ZfFr26ILkaAJgm0IIhZWMKr7JYT/BsGEU0Pse3xdBhoBjgbh72FABXVIB4BPob1BTjeT4KaRYaxBGz6tgZBmvEll79/GpjFNFsFuYPAbjNha4oscAdDnYL0L+pNIKJ0LYxaWprpmS81+4UNgItxDT6PQAl8bUcj4EujCB7uldlSUGxhYJL+5Kwn5sqh/WAEWRdQ6JOCFQ2wX8HwBNMsKvx21ShxGdE/LkNoGSBLypwYYnRQnNxQbHAnf00WkAfJAfrB23o/pJI1z0AbtCpeT0G+mRZCGv7x7jKSOEXy4aRmRKZj9vNen03oFgw5vvR1qCyn1SCkjD31VcbbAPFAFsZoeyjuxSUVHRIx4bxuj8XH6fhc7sJFAt8IW3eYS4gB/eSOvEzCVmamEEGxuW0LCPEkaAyn/I45T856GCINgQRp9Np191F8Md8vWZ91iDJSX48S8GMQxUJ7edXFewXEHz4ZHGA8CiGfPQ+dfEilowg+62g0WLD+PtBGnsyrXofJOfJLMwg91QVbRpocRDOKVWMDherdOppghcRvto4hx02x3LDuCWxR2JOnGmbqIh3zlH4QlhpkGKFUmDwYOVtHkDxODv8MdUvF88sine0W43c8UD448tpxKPHKyKIemKJ1I1THNFchxe/8cQz4hQ/zKmg/gc6xohD8bs6TiMDo2rDuGLDuJKB0e36e1no4GBPtruTDMxX5X0qqi8T/GYG5qu63GIvQmkGKAuzcA+1syb4p9ZR+7wF49g6apOdPMOCcWMdtYoF4y8buP9OBsabd7GxmtY20yCOpKM0fvxm/YOHm//m4tiv+9iU73n/Zv2D9/b8zp70JP8HKuBBAtm3G18AAAAASUVORK5CYII=',
            srcf: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wgARCAAoAH8DAREAAhEBAxEB/8QAHAAAAgIDAQEAAAAAAAAAAAAAAAUEBwIDBgEI/8QAGgEAAgMBAQAAAAAAAAAAAAAAAAMCBAYFAf/aAAwDAQACEAMQAAABtDTZ/iuvzWKHLnpYocAuenrOZfuLK6MAAAAAACm9XnKy0PEbVnxWQYIdHZCG1XRULlz5PSc7foxGezEz99Vj4/rubbaVrNd93j173eRLVOMyDqpZ3rnHZD09uHK6OC2EVid0WYSVol6+pv3wZ8473G1TpeC9p2kVyq9p2gEVyrY/B7P0hgdmltV5ipa/ZaGJy8m5qulqaAAAAAAAAAAAAAAAB//EACUQAAICAgEDAwUAAAAAAAAAAAQFAwYBAgATFzUHFRYiMDQ2QP/aAAgBAQABBQK2WwtCx7jsuH35gLP3HZcZX5gGxW35gYx7jsuVi4mOmv2LGi+QWr470UEdf3clsaxHosMrE7TZUi6ESyuQFp6kOKNaHsmIl2Sth9tHE5BMDuaPT3mbeUBvP01c8hS7hJcAd0ZHjWpAGaISLtuFV0GjvChcW8CcDU5lELX1v13omfAo8LKPaHZ4NHjLaDqYeB5xE/Gl3gn0Ji56j+c45/L4984i85ygfsJYcZ0cleGzrIq0kyQminh3RaSz4Rx68FG1Eg/h/8QAOBEAAQIDBAUJBgcAAAAAAAAAAQIDAAQREiExUQUTQWFxEBQiNIGhwdHwFTKCkbHhIzBAQlKy8f/aAAgBAwEBPwGfn3ZV0IQBhHtmYyHf5w7pZ9CqADAd4rnHtmYyHf5w9pZ9t1SABcfW2GdLPuOpQQLz62x7ZmMh3+cSOkXZl7VrA/JnJXnc4EVp0fGOZ2ZfnDhpkKYwJQzK7RNEgJv+EQ9JANa9hdpMOSK39a8jYo3QxK2QzMVxULu37QzJocY17i7I4VjR6G0TYDarQplSJo2Wqk0vH1EKcKFfgmqdmJvsr7TgLr918NvuOKSon+XA4YUUd/yMGYWlZCb7/l0gL+ypGGG2BNrKjeAMzgBVYrjTYMseAjXqTYFcaZ1NcacNt1RiaCJZanWUuKxIryLcQ3PVWadDxh51uflgpSqLT3w262tCpV02ahN/wjyglmRl1oSu0peUCZEu08UnpWzd2iHJpqYSzYuNsXRo55KJazbANdsM36QtWgqo2QpQQCo7IQu0L8fXobo5yitPA7vOC8gf4Y5010b/AHsLjthMy2sFQwuz2whYcFpPJpnrA4eJ5Jn3xwT/AFHJNdYc4n6xK9Yb4j68mies9kONpdTZWLo5sj9nRG671XwFL41AraqfVPIb98OMJcQUYQZRKlBZJqKZbDUbM8oTLhIs1O7dTL71rgaiG2w2myP0X//EADkRAAECAwUCCgkFAQAAAAAAAAECAwAEEQUSITFBUWEQFCI0coGhsdHwExUyUnGCkcHhMDNAQvGy/9oACAECAQE/AbMsxmdZLjhOdMOrdHqCW95XZ4QxYku4mpUcyNNCRsj1BLe8rs8Il7El3WUOFRxAOnhExYku0ytwKOAJ08I9QS3vK7PCLRspmUY9Kgmvnd+jITnEZEuXa8unZHH781xVpNaZmuUKnkyjd0C8oqVQfMYYtFRe4tMouK74atFuWDLDmRSnHz3xMzl9T8rd9lBNer8xMT7jUwJZtu8aVzpFprdckSXUXTXbWJNN56gFcFb/AOpphAbCxR8UVrQAYXkU3A4nZpXCFSrbbbgAxw6sFZ1SPJTBlEKxoRsyx5JOHXQa50wMcUQEgEEnYKVJog7DtO3L4mHpVurigMr2OgpkKb9Mdd0TKEtPLbRkDTgbaW7ZtG015enRiXZcs2bKUoq2vZp5/O6HWXW1pnWU3rpUCPmV4wA/aM2hxbZQlGOMGUM09LhSTduDHqMNSb8qt/0mIuHHz3RasutybCvRqUmmn+GJjCy7lwpodc9uwQ2guLCBrC2FBVE45duUCTdV7NNNRr1xxZdAdu8duOEcTe5Qp7OeI8YVJOIArnjqMKUzNaDOFoU2q6rgsDmyul9hwSn7Z6Sv+jwSXNmuiO6J3mzvRPdwW3zQ/EQ06plV5GcCedwK+URt+o+n3hMyUilB/mR6q/DaDCJpSFX6V/Ap53wmdUhJQlIoa7dRQ6wZxRzSNa760rX6aUppDjhdVeP8L//EADsQAAEDAgMDCAgDCQAAAAAAAAECAxEEEgATIQUxQRAjMkJRYXGRFCIzdIGSstIVMMFAYnOCobGz0fD/2gAIAQEABj8CbYYbZWhTQXzgM7z392PYUvyq+7CUJZpiC02vVKusgKPHvx7Cl+VX3YqmEM0xQ06pAlKp0PjilYWzTBDrqUGEqnU+OPYUvyq+7Apn22EoKSZbSZ/v+Sinzsi2jzLrbuuf94/E6p5VPeYZZy5LnZx0wpxTqaWkZpmC4+vhzKcHaOzqwV1Kkwv1bVIxteup1XrarHU5EakTOnnuxsXamdOdWIbyrd3rHj/Lhe0amv8ARGkrsPMlf69+G00lX6a3kqJXllEHsg4kuFlGa0FLC7ITmJnXhpgmidL9Mo82464txN+W4TrMkaJ7ePHFEsupCFXIiLUuG9vo2rIO/t4KwkFxt5CQkuGwgsi9I9Yz2FRnTozuwohbVOibS68lRSkXvASJEdBPn4YoWytClKQzzSkkuOhQErCp4a8Or34pnnrcxxAWbBA15L33m2EHZ8XOKtHtMNuO1SKfaNGDKHVAZvbHjH/b8VOxqx4UiX2GHEPndOUjf8o/risYZrWq+rrRZzRlKRHj3nG3FNvNJqxtFaktKIlQuTOnnjYxpyltz8RbUtjrJMmT5nf34W36bS01RnEgVCuGnCRjO9KYq1OsXFVN0RpEbz2YcdUCQgTCd57hi9zm1ALvTvi0wrCszNTCljRlaujvOg3ajXCki9Vsza0s7iBppr8MUxvWE1EZSlNLAVO7WMORmZYShSTlLuXdduTEno8P0wHGzKT8ORj3cfUrkb93Y/xJ5Noe8OfUcbP94b+ociP4asBt4XtXBRQdyvHC0sldIhcyliEjUQeHHTyweddRJM2nqmLk/GPHsIwW8xxsG7oR1lBRG7ujww285UPuOJtkm0XWquEwMGH37haG1SJbCZiNP3iNZwGkSQJMq3kkyT5/sX//xAAnEAEBAAICAgEDAwUAAAAAAAABEQAhMUFRYRBxgZEwQNGxweHw8f/aAAgBAQABPyF60FfSfQ1p8bHVA96/Rmr42O8B7goXz1jvAeYCM89/GyW1uNH1f6PVe9lCkp/xn3zGQbWIdvHBd0weoQU6IavvevxUANC+AbPvvRpHjeWxajVUV2+j+M+h3zru239neaeP3oR1X4ZV6f4jQRvvGF4XJA1oindziShnJNB7wi66av6WPYFRg+3GnMZBG1hNtqTonIrhP6QM9EyYXW43oDBF+lB4pu3KpGJE69jFgK8Wfx8AogRo9K96cks6RsK47ik4dSJiYE1AF4ln4LpRmXmsy1Zs0hs7U1BzRnM+v5USKe/Gb0XSCEl2NPL2phUWEU022vPeIKkuWj6ohee8eI2a+g7XgM5e82dMs2DxoojMVBpnQMP7aG+cFNzvFUJHqu61/SoOhVqioFig9b4TIJ5MQUGqSqEl8s2SdyKEYiOxERHYn6M1fYsf6nxmquDDPYDssZ6OqZ0SAex5wgudlI2vRrUZfCNCqmjehmhot6YAoToSUjc8vMYqMOqyEOHkHLYQdTJ92kcw7uJowjVQh8qX7/sv/9oADAMBAAIAAwAAABDqKReSSSSarQuxL/TR0/wbR5xZZLQcbSySSSSSSST/xAAmEQEBAQACAgECBgMAAAAAAAABESEAMUFRYXGhEIGRsfDxIDBA/9oACAEDAQE/EHTBDo+08J65/R8CW2vT4F+pzn9HwkvoFHwpxJfAYPlDj+j4DgCLg3Pqv+nxTXZfI9nvnZds0Po9kHfHRS0r0NsvH6F+dw/IVsE7kT8v3wyMTeMtqHGoNo+Xep+dzndubHWvN/Z544hGdvT0/PrmjMbWvUf3+eHgGrZBJ0SZa3g2zdlNCMReliL03BQBA5oDKAp5VYMNRZgCuNIyrhdAJRA8Q4DGkBljUradKAdaUZRURKnSooCCDyMsWCGlmr19fwg+MVQ+/hgTuKHTZ8sydOSI8lFOPCmLc8PV0ozgksnkBJscxY+VMg8FSBBJU0zuJevn1xOmi4o1rPIrb5u7Tj4V3bxnij9+TvlV6mSdvgvfnnQSFfOGuc65AonyX6MQqQqGFnJpXuERYJYFhBenG6cqSslidlJBuazo1mXNqiBYKoFk3xfG9bxwVAqFNIISozEo76eBUzfCdMcYiJEfw+x/4Cj+S9ufyXp+HT9XEliin0RPuFPJjRTiWJoMkLEfFwxidkCq0ifhDIEMpbKQJg5QKH1OpJokmScYW7GKQmCB8C5VhF4lhoz0IDrzoNMOEkXtV7VVV6NVYAHQBD/i/8QAJhEBAQEAAwABAwMFAQAAAAAAAREhADFBURBhgaHw8SAwQHGRsf/aAAgBAgEBPxAxJFoBAXq+efynBPdMT0E/4N+/P5ThTFYjFQc4pikVigu8fynBevQak3/Q/s+6qizstsfjnRUrkI7JGph32xkUANjTt93ufbGv5QnSlNp+X/motKJEh4tvBSRPDPWekrzpt2DvGSZ+XnKk/wAPy+SefPMx9yMfNPyT7cbQqAhSJoI7IR3mLOYTsKEEYrU0vVCoruo8SwUMQtB6p1Ib2Q4pDwKVX4DjPBUSg4GiFoYKJohQCyQdNY7gFBgBob0VaRcZcDuWTOt7+iZj3ArPWcXc0iFdsveFjezDR4taesR2A31e5jEHn2VRitpKFqFOgHahxwxkgYNDeqMY/aiPBQsM3EgBfECfAMyPGZAmXvfYfpysimHdXXwVTrziKgoN6L6vgevhzpUcOFm+sfnUES8k6UWn7ho1jnedcHZDEo9i9hoMoXzeJsi2gdAisKQupY52Jw4ikJh2OwWIojDtOKBifnsoiYiaJiaZ9P1T+gD/ALT8OftPy+uKTkIPpcU+8pfLSMSwJ1G1x+QtWXxDSBSG67HtPpqERqOxvC8ETu9oGiN20RAJOAf9DU3irZ61NDFFMwNCMejWaEwWg4/mOEOgAAPsABa5qv8Ahf/EACIQAQEAAgIBBQADAAAAAAAAAAERACExQVEQMGFxkUDR8P/aAAgBAQABPxA58zUxBPp0Wrv0HPWvERIQ6GO4FV24OWNs4mSCMCwC9GDG2cTJLCFKJen0HLJqwNEKSb8ez/UF3Hd3t+m9FHyUDF+pWVx9Buj1e6FQWh0AatRiDYPWGitYhpQIUg3qHrNHXhBsYxWL8v8AMXP+ifnrdAi6FddFsghp5zmMMvaNisHQ49OKvjZl5UAUCC7xTzaxNCVrgGAZ9CKnzm1ookdETlgYhqObE9ANQmEnFmlAjHSW3VmHkdf5OAHJX7BB9LmC5Y9AldosqGCfAoi0Kgwk5g+MV4Le5dhABpWMBU5ep+NAqg6Nkt6cgsE92xiRBtQ0sZJRAmFYKAhGhSzdrhBnGUi2iCfejcqqyIAQFO5MQHHPuElwJX6QmP8At9DCnYU7Ch3lCBuzWoCwFUIbQW9V3O7Br+CpGGFbJ0UWMKTgoiuH2nNmGHsQCwBQk1akKqYM9wT0JjtpQqQHgYEICCJ7IaDWrf7Xhg/SooNcMTxqk1IsYYahmoDhhElGBnem+YW0XcOgOA2MRqtJSoTEEq5KKFUBJ0iKwCCTvG7W6VOoeUkdK+fURnQ5XgFUAh/C/9k=',
            src: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wgARCAA+ASsDAREAAhEBAxEB/8QAHAABAAIDAQEBAAAAAAAAAAAAAAMFBAYHAgEI/8QAGgEBAAMBAQEAAAAAAAAAAAAAAAECAwQGBf/aAAwDAQACEAMQAAAB7l43Kp+bUAADI6J6L7zYAAAAAAAAAAAAACt+fXHwAAAe7rf6lgAAAAAAAAAAAAANL8jlU/OqAABkbT0L3OwAAAAAAAAAAAAAGkeOyqPm1z+2c322wAExtgAAAAAAAAAAAAABW/Prj4JdUv0rAAfTPAAAAAAAAAAAAAANN8nlVfOjO7JzPY6gASm0H53pfDvTaL0wsNqO9N0W5pfO3pb4t5tnjUvtm+VLz79GtTQZjVF75XfTn1LQa59cy04/rnYzHQKX6kADRvG5U/zK2HdOd7fYACU24/MFL7LenR7U4Zlrtl66plrx3fC4pa5W6fplyLPTrGudtlpt8xpedvzdpN8p+rtcs7j66rt49e5eqq1x0qt7SY2oq712KluzIhynzSPsvWspJACUiJQREpDDCkMwnR5iYpj0mUiMKHyWWiaJ+THlMpHDHlAe0ZKZz//EACgQAAAGAAYCAgIDAAAAAAAAAAACAwQFBgEHEhMVFhBAFyYRNhQgJ//aAAgBAQABBQKUm1WLvs647OuOzrjs647OuOzrjs64b2JZZx7q2LPXqjhqjhqjhqjhqjhqjhqjgTFhq92Tg1Xzvq646uuOrrjq646uuOrrjq64b1tZFf3ZScXYu+0OgwsDh085lYcysOZWHMrDmVglLKnV91ZVoU+/HhJZkZTcajcajcajcajcajBRt+fdkoI7511ZUMa8o0d8KccKccKccKccKcJRB01BZZ6xKXD74K8jcjSdun5s1nx75hhWbS7k6REyd3mWH3zAGv0orQkz3tVN7IXeJauro9UcOsxZKLtDrMODbRNevMvMWqGm7pYGv3wRTe2YQsj8gRbCIXv01HHfyUDTWLy9SDOPsFliLG+n3iGYH9JWbXZPOzOxHzzh085hccwuOYXHMLjmFwlKrHVE1WW1pzM+GIIViqM6m2aVVtfrV8MQQr7Xr4iS1LFhiSifgiKhMqU8m4Zdm8onU38+dsvOVEmB7w2y9g2ks8w/1aGLV/4uihim2KNnI65fqeXf6bmsubrpMmITTbsvGdSjrNY20RfvmSBCubKL+UPIPuXBkiGx2ExgkQuO2UbZRtlG2UbZRoL42ibngiRE/Gynq4xmOMZjFBMyYMXA5dhMFSIQw2ibnGtMRxjMIt0m+Bi4HKQhUynSIp4OQqpVGaCw4xmCR7VM2wnuj//EADIRAAEDAwEFBAkFAAAAAAAAAAEAAhIDESEQIjFAQWETUbHwBBQjMDJCUHGRIFJygdH/2gAIAQMBAT8BLrKampqampqanx2FhYWFhYWFhY44tuoFQKgVAqBUCoFQ44usVMoOJP0TCwsfRC26ggy3uKlmhnXR1/lT7Na3vOlEXrdk/vQN9KY2qjXfKgZC6DS7AUvYT53AVVtnAM7kzaynEXZHcU+03NHI6M+Pa3ICSDpKi0udtIOlkKpdrZ9VU2WMI5k+H6S4hTKDiT7is63Zjzz0jmSqu9rDuGjGw9LZbnZC/PSjaVUt3Km6TQ4pzSRsI29V2f3BVsVm/wAf8Ttveqm+j55lVL9q+/fo2zdgKl8X9HwVLcfuUDCm9/RC9sosFRjh0RvUoUndT4aVHRpk81U3+ev55fn7cP04H//EADwRAAAEAgQKBggHAAAAAAAAAAABAgMEEQUSUtEQExYhMUFRYWKhFSMwcaLwICIyQFCBkbEUJDNCU8Hx/9oACAECAQE/AaRpVyEfxaUkf1HT79gud46ffsFzvHT79gud46ffsFzvGUD9gud4ygfsFzvHT79gud4h6ceW8lFQs5ltv9+eODrddVnvkJ0dweETo7g8InR3B4ROjuDwidHcHhE6O4PCJ0dweEJOArFVqT+Xv1IUM7GP41KiIZOP2yGTj9shk4/bIZOP2yGTj9shk4/bIZOP2yDFAPNOpcNZZjI/fqQph+EfxSCKUi23jKKKsp53iCpuIiYhDSklI++/4I65BpV1xpnvkMdR1pHINuwRrImzTPdL4JSFDORj+NSoiGTjv8hCDoNyGfS8ayzdg3NdY9mBMs9YIms17CB5g+cmycRrIGmrgcPM2pP7goqpyBqJGcxV/MG3qlMNKJSJr2hc0nLSJSbcUelIkZJIz14F+wVXTPkFKJJTMKTV0h9RJL1NwUmqcjDclKNGuQa9c3J6vRpGl4iEfxSCKWYZQxexPO8QVNRMREIaWRSPzt7BhMycPYeA1TSSQynqcZtM8ClGuEOeo/P2By1YHZ9SStP+hxNVRpIIWksywmt+KVWs3hjPDn33hHVlIgn9KI86iBSqlLAuausPWHvZ+Zfcg7pLuIVcY8hAOU8wS4bbqD3hEm3Xk+dWBtNdyR6A3/d0+7X9O/AbSFaSGKaskCbQWckifabvS39pv9H/xABBEAABAwICBgMNBwIHAAAAAAABAAIDBBEFEhMhMTIzkRBBoRQiIzRAUXGBk6KywdIGNUJSYWLRFSAkc3SChbHh/9oACAEBAAY/AjE1jHCw2rhR9q4UfauFH2rhR9q4UfauFH2rhR9qjjMcdnOA6/LvDaHP++11tpvdW2m91bab3VtpvdW2m91bab3VtpvdQymnzdVsvlxlbI1osBYrjRrjRrjRrjRrjRrjRrjRqN5lYQ1wPlxiYyMtsN4Lhxcj/KjicyMNceoFbrFusW6xbrFusTGlrLE28utM6EP/AH2ut+m5tQEb4C/qykXW9F2Lei7FvRdi3ouxb0XYhZ0V/V5cZWyNaLAWK4zOSjlMrSGnZZcRq4jVxGriNXEamOzjUb9EuF4RWQUzGQtf4ZrbcyF98Ybzj+lQvxDEKOegB8IIg255NUWE4JUwUxbBpZHTBtu1fe+Hc4/pVZXTOaa+lbKHHL+JouNSjq4cWoWxybBJowfhX3vh3OP6VUV4McWIQVPcxka24P62TXjF8Os4X16P6VJWTYnh80UIzujGTvh5ti+yTocsUWJHw8eW/mHzWJxTUndWFUzw1xib30Q86bXisbK1+5EziE+ay7nqKYUVG+mdLHA5vfW6nXTqqmxSijizluWUMafhX3xhvOP6ViArKylfiDh/hXtaMrfTqVRVzYhSaKBhkdlay9h/tUNbT4hS6GUXbnYwH4VUVeJyRTYjBE5xcwd6T+H5KKpZitAxkrcwa/Rg/CsMpsZraWppax5j8CG6j1bAqDCmub3HLSmRzcuvNc9fq/tMTAwtsNoW7FyP8qKJzY8rj1BbGclsZyWxnJbGclsZyTGkMsTbZ0VVLVmVsQpmvvEbH/pcau9o36VLBRume2R2c6Z11j9TWunZDBKIozEQPl+i41d7Rv0r7Y4H3xjZTulizbSMp/8AFH/UX4mKz8egy5PUuJjPuKpDmObC6uvFmFiW6taY4VFYyV7L3L2kA281kJMXpJ8Qwu48PSPtYfuC+xD6JuWkLjoxa1h3q+1TXNu0uaCCnYgyjGlOsMJuxp84C/44/NH+ruxEVec+LZctvWuJjPuLR4aZtHSgRkTjvv0WL/6WT4Vhn+X81FRx3L6ydkVghmmrc3X4Rv0qPFcOkqXy08zHO0rgQBf0LCMUqA/uc0APeC51ly3av2Q/lYZS4VE52mnbHN3Qz8JPVYqR/dBGXU2O53/yZdnrt6+jW1p9IXDbyVwxoPoW6OS3RyW6OS3RyW6OS3Ry6NJkbn2Zra+k5Ghtzc2G3oc7I3M4WJttC8Ug9mF4pB7MLRmNpj/KRq6C1wDmnaCmeDb3m7q3fQnOaxoc7aQNvRpMjc9rZra14rD7MLxSD2YREUTIwfyNsi1wuDtBQaxoa0bAEM7Q6xuLjZ0Fr2hzT1FDSQRvtqGZoNl4pB7MIObTRNcNhDAtLo26T89tfR//xAAoEAEAAgEDAwQCAwEBAAAAAAABABEhMUHxEFHRYXGR8ECBILHB4aH/2gAIAQEAAT8hAGUu95PecP5Th/KcP5Th/KcP5Th/KfX8ocktTLLXf84ztjz505GORjkY5GORjkY5GN12ZbXtX5zprkN4J9hn2GfYZ9hn2GfYZ9hinJoXmm/ziJJLS8nv0EVLpLc095wb5nBvmcG+Zwb5nBvmHkDVD39/zm9GZF/Z04QQdHxVTnY52OdjnY52GlmcU2v85uqoDtOSSwfpCzOEZwjOEZwjOEYhXDpT36VHfJFZnIznpCwKF5wYBDet5nyEYK6KWsVp3iY6cIJ+8AuIjHaGeZQjzWSGpdOEsdR+jwVm/tCkgG1szlz2WhrQL8Iww8jBbNQuTUxlvvL6u9+v/kwQaycO98Sy7mocJsNRZzn09N5T2XDE9FYzKkIgzbRM4Eb2wxZibVQtonY1XA9Sa04cM2lFf0hKwVpHuaJUbYtw6kUbSADMcW1O7b/ENca2XJ79Kg0lJUvT3nJPM5J5nJPM5J5nJPMrYGqXf36FX3DsANVdK1hs6LuqxQEvjjzUERtaA+elZwTrDaNdhuPwj3VGvfYyL0llVu2PGHQCvdM/dwywQgH7Je1zMvatlZKL09T0ey3lV/YimCaODYjeJdL39GYv0qLXDUitt6fT6LvWcD4zS9LUFZLo3T8Q3XnzIK+7VBgAXczdfNTZ6GFVwf4a3MMHeo276Ey4U79HJ/zGFAHrc7yyR21tsz2HBqZ9EJYHe4M4XAgBuD+Tve94Jxj6AELJarDtfXsN5i3d6KNbA++wvafY/wDJ9j/yHzbQr8JpDghQLEl78H/8JoFGwfc79F7OxbDtfaLlXXf/AIz7H/ky4EQL/ECAagWJMLRB0H6nYb3Nu501yjHY/qPuj6GOxc+x/wCRC2WOR+IuUpKKcffp/9oADAMBAAIAAwAAABCkkliySSSSSSSSSSSSSQ7bbYSSSSSSSSSSSSSSR2223ySSSSSSSSSSSSSRnySQSSSSSSSSSSSSSSQ6gACSSSSSSSSSSSSSSRhAAAQhIicNBfYZJXWSRCgACTj0aNFS6FOadIAGfrSSSSSeQFsQeNuTycT/xAAqEQEAAgAEBAUFAQEAAAAAAAABABEhMVGBEEFhoXGRscHwMEBQ0fEg4f/aAAgBAwEBPxB1UtpLaS2ktpLaS2ktpBKFffOqbZtm2bZtm2bZ01986snWnWnWnWnWnWnWghG/vnoOEYH8ItsZfTBvhX4R1dy+sRDf0BWGJey+xwtq2Nl+EpiZrsYez2qBbUd8SibNfy4d65NeUC8IuZosd6/faGJycZYpTT2Lir8vpKWmJjBopr180PDdlCpoM75fPmEVUxM9TE9Rqu8Qy1D39HhSzye+NPpf9t1RovkX7QgpyU8ogMuLsF12a2hGqhlKHAA74ds76Q8zCfAD/lyjhGB+ggPmPquABjNo8r/cYByC98vPHgTch7Efdmf19uUM8YtGsL29q3uVFzBdyXxWd/nmRvQ92NWaIxh/rq/K3lQDWHTFq8Ou98PDXpMz4Yp8BqwVuXu/5Ljz1j4xT5/w+s6AI+Q9XgBOTLT45ShQaDN05NwKsbyVdC0LlDKJR9Bxq+XHNt4W2PM4mBRArAgpiSitEcW3hnV8ot4vCi7go2QAymZXLhdZQwKMuCDnHGr5cP/EACoRAQABAwIEBgIDAQAAAAAAAAERACExQWEQkbHwUXGBodHxIMEwQFDh/9oACAECAQE/EFpwBluubJ+ckkkggmtSPsQjF10P7zLuexpm9b9W/Vv1b9W/Vv1b9T5NwiL50iLzOP7yAYgQzNiNCuzfiuzfiuzfiuzfiuzfiuzfiuzfiksDWNEfD+82XCXJXJ0HSvpqSLVhglh8V0/xIyLeltm9fYVbH0yn6Rf/ABEARAhHQivrGk9BzAPh/BGZsvg4Q2NLedTqcJ0+Y9KUF8KK3Qn3hzzvTQnUnnSwTQNnDMc+/wB0qvIxQgMkhzYPdoI/BjzhTlEc9oxClE+tjkL45zUESTiNfju1CeyQ7DAxyT/lDEmE9T9cMCzNyW88943EBzQPdpEGoHnejLasPVQX3mkmrlTyZE9L9D3KtWmR7dZ9jefwdqgVxW5slfRUG0rDAzh3fwApnqRwVDBPv9UcVmJ5GepHrwZM431TpDnWPb760zFqMNJZ81X1tUzcKcmkIYdPB6d6JRE28un/ADQBNTU9HxNh7d4tV/p0SvnF/O+PSKLUC0E8zTknPMzWGm7h4FSD1Z/X7q806VoIy6nOpJ2Dt5cHDSvnITy8aisLMgNzN5JwiMpiVISa7g19EfFJgh8irKGT8i0xrxwQY4QQ6HPFuy5rNIOalnxUWIOBaY1oAIOEsRSDZpVu1hnXgg5rL4uApii0xrw//8QAKBABAQACAQMDBAIDAQAAAAAAAREAITEQQVFh0fEgQHGBkaEwscHw/9oACAEBAAE/EB/zTs0eAfXp06dOnT8biMv3YApPLf3xMOELTNXlP8BhhhhhhizSvNH2N2yT74RMwNkOxnxHtz4j258R7c+I9ufEe3PiPbnxHtwroNWBQa9PvgWDUbo8E/roOQSA4JTqs7ePq379+/enmErBAz9vvj4MIeJq8p0eSFEYJn5P4+o88889mFANytSd798WmYkkTt07SAgkdE1fz9RRRRRSkdYsgGf10Lp5ZALX2hAhDJgBHXRkuhRpcM2nvGFmiwgAq7dRAuBYUcLMeIxgDYtAByOqh7Jbc2ecaSYrMEFq/Kt3Kkuu5A1BrVogASnB3lrBpWRBdTU8AjMWtUgGisIgjdFUtUI/MeQAsKrxoA8ElDLUASPuggKbaVLtZolZS4E1ATuOW6GNLEOCKq9EDtkVybLlQbQStNY4MA3VoGsGFMBJmdhJocaPfDlZr5kAXaGFbyR7sXqFtTtXCzCKmAloKCoi+MQu6nEgiIQBm2jqfQeezXKOwf66DFzdNwU6V+PH1SJEiRIPaTKIgZ+3RZbUMWos29ujE+pxlJJHEPFbt4jyyNzxLewBx6MUzHoFLwBeBDlrEleBk1yXY5752RLtG5HX31WCBuk30hoMMh3Q81iguwzVOcUiMiQ3NkRuae7gzYq0GjYIG74WvOaXSBWFDpEuu+NmCVVVX6T1oTTa2pxFDXJybyJkaO88j9YE4u/RWPG0WLgbVQErFCuExYSJu7wGCWmzxPVGNj/G3RueNwzRI0lCNoe954xiTkPhA4CLvp7Zl+PvLpNzQmuMKuobS1k77HLQyKQpHuYilMtP5c/8d/zHBxoZP3MN3Z9GfAvbPgXtnwL2z4F7ZXgdhAI/x0Ggllvmiz0vVtrs115gbdG3fSrCg/BJX0OuqBAVGQFIcAya/GAAAANAYjgRZIiI6R8YqV3ke0nZ2a1MewozT4gWPXoDSMFzd4LsurMehFUyvQgcEoImcKAuOJ8WSIiOkTthWLgz+AaDG2uSTXihpOyb6DR2Fn8K042Jq/hAMPQ6IB12I+4RKHKYa1h4E2bdXp//2Q==',
            w: 200,
            h: 40,
            wf: 130
        };

        var fontSizes = {
            HeadTitleFontSize: 18,
            Head2TitleFontSize: 16,
            TitleFontSize: 14,
            SubTitleFontSize: 12,
            NormalFontSize: 10,
            SmallFontSize: 8
        };

        var lineSpacing = {
            NormalSpacing: 12,
            DobleSpacing: 24,
            LineSpacing: 10,
            LetterSpace: 6
        };


        var doc = new jsPDF('p', 'pt', 'letter');

        var rightStartCol1 = 40;
        var rightStartCol2 = 530;

        var border = 3;
        var tabla = 0;


        var InitialstartX = 45;
        var startX = 40;
        var startbX = 43;
        var InitialstartY = 20;
        var startY = 20;
        var lines = 0;
        var space = 0;

        doc.addImage(company_logo.src, 'PNG', startX, startY, company_logo.w, company_logo.h);
        doc.addImage(company_logo.srcf, 'PNG', startX + 400, startY, company_logo.wf, company_logo.h);
        startY += lineSpacing.NormalSpacing + company_logo.h;

        startY += lineSpacing.NormalSpacing;

        doc.setFontSize(fontSizes.Head2TitleFontSize);
        doc.setFontType('bold');
        doc.myText("MEMO DE VENTA", {align: "center"}, 0, startY);

        startY += lineSpacing.NormalSpacing;

        doc.setDrawColor(0);
        doc.setFillColor(255, 255, 255);
        doc.roundedRect(startX, startY, 100, 20, 0, 0, 'FD');

        doc.setFontSize(fontSizes.TitleFontSize);
        doc.setFontType('bold');
        doc.text(InitialstartX, startY + 20 - border, 'N° ' + data.Datos.codigo);

        doc.setDrawColor(0);
        doc.setFillColor(170, 170, 170);
        doc.roundedRect(startX + 100, startY, 320, 20, 0, 0, 'FD');

        doc.setFontSize(fontSizes.TitleFontSize);
        doc.setFontType('bold');
        doc.setTextColor(255, 255, 255);
        doc.myText(data.Datos.proyecto, {align: "center"}, 0, startY + 20 - border);

        doc.setDrawColor(0);
        doc.setFillColor(255, 255, 255);
        doc.roundedRect(startX + 420, startY, 100, 20, 0, 0, 'FD');

        doc.setFontSize(fontSizes.TitleFontSize);
        doc.setFontType('bold');
        doc.setTextColor(0)
        doc.text(InitialstartX + 420, startY + 20 - border, data.Datos.fecha);

        //INFORMACION DE LA VENTA

        doc.setDrawColor(0);
        doc.setFillColor(230, 230, 230);
        doc.roundedRect(startX, startY += 20, 520, 20, 0, 0, 'FD');

        doc.setFontSize(fontSizes.SubTitleFontSize);
        doc.setFontType('bold');
        doc.setTextColor(0);
        doc.myText('INFORMACION DE LA VENTA', {align: "center"}, 0, startY + 20 - border);

        doc.setDrawColor(0);
        doc.setFillColor(255, 255, 255);
        doc.roundedRect(startX, startY += 20, 173, (lineSpacing.NormalSpacing * 4), 0, 0, 'FD');

        doc.setDrawColor(0);
        doc.setFillColor(255, 255, 255);
        doc.roundedRect(startX + 173, startY, 173, (lineSpacing.NormalSpacing * 4), 0, 0, 'FD');

        doc.setDrawColor(0);
        doc.setFillColor(255, 255, 255);
        doc.roundedRect(startX + 346, startY, 174, (lineSpacing.NormalSpacing * 4), 0, 0, 'FD');

        doc.setFontSize(fontSizes.SmallFontSize);
        doc.setFontType('bold');
        doc.text(startX + 86, startY += lineSpacing.NormalSpacing, "TIPO DE VENTA", 'center');
        doc.text(startX + 259, startY, "IMPORTACION", 'center');
        doc.text(startX + 432, startY, "CENTRO DE COSTO", 'center');
        doc.setFontType('normal');
        doc.text(startX + 86, startY += lineSpacing.NormalSpacing, data.Datos.descripcion, 'center');
        doc.text(startX + 259, startY, data.Datos.proveedor, 'center');
        doc.text(startX + 432, startY, data.Datos.ccnom, 'center');
        doc.text(startX + 86, startY += lineSpacing.NormalSpacing, '', 'center');
        doc.text(startX + 259, startY, data.Datos.codigoimp, 'center');
        doc.text(startX + 432, startY, data.Datos.cc, 'center');

        //INFORMACION DEL PROYECTO
        doc.setDrawColor(0);
        doc.setFillColor(230, 230, 230);
        doc.roundedRect(startX, startY += lineSpacing.NormalSpacing, 520, 20, 0, 0, 'FD');

        doc.setFontSize(fontSizes.SubTitleFontSize);
        doc.setFontType('bold');
        doc.setTextColor(0);
        doc.myText('INFORMACION DEL PROYECTO', {align: "center"}, 0, startY + 20 - border);

        doc.setDrawColor(0);
        doc.setFillColor(255, 255, 255);
        doc.roundedRect(startX, startY += 20, 520, (lineSpacing.NormalSpacing * 3), 0, 0, 'FD');

        doc.setFontSize(fontSizes.SmallFontSize);
        doc.setFontType('bold');
        doc.text(startbX, startY += lineSpacing.NormalSpacing, "NOMBRE: ");
        doc.setFontType('normal');
        doc.text(startbX + 40, startY, data.Datos.proyecto);

        doc.setFontType('bold');
        doc.text(startbX, startY += lineSpacing.NormalSpacing, "DIRECCION: ");
        doc.setFontType('normal');
        doc.text(startbX + 50, startY, data.Datos.direcpro);

        //CONTACTO DE VENTAS Y CONTACTO OBRA

        doc.setDrawColor(0);
        doc.setFillColor(230, 230, 230);
        doc.roundedRect(startX, startY += lineSpacing.NormalSpacing, 520, 10, 0, 0, 'FD');

        doc.setFontType('bold');
        doc.setFontSize(fontSizes.SmallFontSize);
        doc.text(startX + 130, startY + 10 - 1, 'CONTACTO DE VENTAS', 'center');
        doc.text(startX + 260 + 130, startY + 10 - 1, 'CONTACTO DE OBRA', 'center');

        doc.setDrawColor(0);
        doc.setFillColor(255, 255, 255);
        doc.roundedRect(startX, startY += 10, 260, (lineSpacing.NormalSpacing * 5), 0, 0, 'FD');

        doc.setDrawColor(0);
        doc.setFillColor(255, 255, 255);
        doc.roundedRect(startX + 260, startY, 260, (lineSpacing.NormalSpacing * 5), 0, 0, 'FD');

        doc.setFontSize(fontSizes.SmallFontSize);
        doc.setFontType('bold');
        doc.text(startbX, startY += lineSpacing.NormalSpacing, "CONTACTO: ");
        doc.text(startbX + 260, startY, "CONTACTO: ");
        doc.setFontType('normal');

        doc.text(startbX + 50, startY, data.Datos.nom_ven);
        doc.text(startbX + 50 + 260, startY, data.Datos.nom_obra);


        doc.setFontType('bold');
        doc.text(startbX, startY += lineSpacing.NormalSpacing, "RUT: ");
        doc.text(startbX + 260, startY, "RUT: ");
        doc.setFontType('normal');

        doc.text(startbX + 20, startY, data.Datos.rut_ven);
        doc.text(startbX + 20 + 260, startY, data.Datos.rut_obra);

        doc.setFontType('bold');
        doc.text(startbX, startY += lineSpacing.NormalSpacing, "EMAIL: ");
        doc.text(startbX + 260, startY, "EMAIL: ");
        doc.setFontType('normal');

        doc.text(startbX + 30, startY, data.Datos.email_ven);
        doc.text(startbX + 30 + 260, startY, data.Datos.email_obra);

        doc.setFontType('bold');
        doc.text(startbX, startY += lineSpacing.NormalSpacing, "TELEFONO: ");
        doc.text(startbX + 260, startY, "TELEFONO: ");
        doc.setFontType('normal');

        doc.text(startbX + 50, startY, data.Datos.tele_ven);
        doc.text(startbX + 50 + 260, startY, data.Datos.tele_obra);

        //INFORMACION DEL MANDANTE
        doc.setDrawColor(0);
        doc.setFillColor(230, 230, 230);
        doc.roundedRect(startX, startY += lineSpacing.NormalSpacing, 520, 20, 0, 0, 'FD');

        doc.setFontSize(fontSizes.SubTitleFontSize);
        doc.setFontType('bold');
        doc.setTextColor(0);
        doc.myText('INFORMACION DEL MANDANTE', {align: "center"}, 0, startY + 20 - border);

        doc.setDrawColor(0);
        doc.setFillColor(255, 255, 255);
        doc.roundedRect(startX, startY += 20, 520, (lineSpacing.NormalSpacing * 4), 0, 0, 'FD');

        doc.setFontSize(fontSizes.SmallFontSize);
        doc.setFontType('bold');
        doc.text(startbX, startY += lineSpacing.NormalSpacing, "RAZON SOCIAL: ");
        doc.setFontType('normal');
        doc.text(startbX + 65, startY, data.Datos.razon_social);

        doc.setFontType('bold');
        doc.text(startbX, startY += lineSpacing.NormalSpacing, "RUT: ");
        doc.setFontType('normal');
        doc.text(startbX + 20, startY, data.Datos.rut);

        doc.setFontType('bold');
        doc.text(startbX, startY += lineSpacing.NormalSpacing, "DIRECCION: ");
        doc.setFontType('normal');
        doc.text(startbX + 50, startY, data.Datos.direcman);

        //INFORMACION DEL CONTRACTUAL
        doc.setDrawColor(0);
        doc.setFillColor(230, 230, 230);
        doc.roundedRect(startX, startY += lineSpacing.NormalSpacing, 520, 20, 0, 0, 'FD');

        doc.setFontSize(fontSizes.SubTitleFontSize);
        doc.setFontType('bold');
        doc.setTextColor(0);
        doc.myText('INFORMACION CONTRACTUAL INSTALACION', {align: "center"}, 0, startY + 20 - border);

        doc.setDrawColor(0);
        doc.setFillColor(255, 255, 255);
        doc.roundedRect(startX, startY += 20, 220, (lineSpacing.NormalSpacing * (parseInt(data.Etapas) + 2)), 0, 0, 'FD');

        var infoStarty = startY;
        doc.setFontSize(fontSizes.SmallFontSize);
        switch (data.Info.length % data.Etapas) {
            case 2:
                doc.setDrawColor(0);
                doc.setFillColor(255, 255, 255);
                doc.roundedRect(startX + 220, infoStarty, 150, (lineSpacing.NormalSpacing * (parseInt(data.Etapas) + 1)), 0, 0, 'FD');
                doc.setDrawColor(0);
                doc.setFillColor(255, 255, 255);
                doc.roundedRect(startX + 220 + 150, infoStarty, 150, (lineSpacing.NormalSpacing * (parseInt(data.Etapas) + 1)), 0, 0, 'FD');

                break;

            case 3:
                doc.setDrawColor(0);
                doc.setFillColor(255, 255, 255);
                doc.roundedRect(startX + 220, infoStarty, 100, (lineSpacing.NormalSpacing * (parseInt(data.Etapas) + 1)), 0, 0, 'FD');
                doc.setDrawColor(0);
                doc.setFillColor(255, 255, 255);
                doc.roundedRect(startX + 220 + 100, infoStarty, 100, (lineSpacing.NormalSpacing * (parseInt(data.Etapas) + 1)), 0, 0, 'FD');
                doc.setDrawColor(0);
                doc.setFillColor(255, 255, 255);
                doc.roundedRect(startX + 220 + 100 + 100, infoStarty, 100, (lineSpacing.NormalSpacing * (parseInt(data.Etapas) + 1)), 0, 0, 'FD');
                break;

            default:
                doc.setDrawColor(0);
                doc.setFillColor(255, 255, 255);
                doc.roundedRect(startX + 220, infoStarty, 300, (lineSpacing.NormalSpacing * (parseInt(data.Etapas) + 2)), 0, 0, 'FD');
                for (var i = 0; i < data.Info.length; i++) {
                    doc.setFontType('bold');
                    doc.text(startX + 110, infoStarty += lineSpacing.NormalSpacing, data.Info[i][0], 'center');
                    doc.setFontType('normal');
                    doc.text(startX + 220 + 150, infoStarty, data.Info[i][1], 'center');
                }
                break;
        }

        //INFORMACION DEL CONTRACTUAL
        doc.setDrawColor(0);
        doc.setFillColor(230, 230, 230);
        doc.roundedRect(startX, startY += (lineSpacing.NormalSpacing * (parseInt(data.Etapas) + 2)), 520, 20, 0, 0, 'FD');

        doc.setFontSize(fontSizes.SubTitleFontSize);
        doc.setFontType('bold');
        doc.setTextColor(0);
        doc.myText('FORMA DE PAGO CONTRACTUAL', {align: "center"}, 0, startY + 20 - border);

        doc.setDrawColor(0);
        doc.setFillColor(230, 230, 230);
        doc.roundedRect(startX, startY += 20, 520, 10, 0, 0, 'FD');

        doc.setFontType('bold');
        doc.setFontSize(fontSizes.SmallFontSize);
        doc.text(startX + 130, startY + 10 - 1, 'PARTE IMPORTADA', 'center');
        doc.text(startX + 260 + 130, startY + 10 - 1, 'PARTE NACIONAL', 'center');

        if (parseInt(data.FSi.length) > parseInt(data.FSn.length)) {
            doc.setDrawColor(0);
            doc.setFillColor(255, 255, 255);
            doc.roundedRect(startX, startY += 10, 260, (lineSpacing.NormalSpacing * (parseInt(data.FSi.length) + 1)), 0, 0, 'FD');
            doc.setDrawColor(0);
            doc.setFillColor(255, 255, 255);
            doc.roundedRect(startX + 260, startY, 260, (lineSpacing.NormalSpacing * (parseInt(data.FSi.length) + 1)), 0, 0, 'FD');
            infoStarty = startY;
            for (var i = 0; i < data.FSi.length; i++) {
                doc.setFontType('normal');
                doc.text(startX + 130, infoStarty += lineSpacing.NormalSpacing, data.FSi[i][0] + ' (' + parseInt(data.FSi[i][1] * 100) + '%)', 'center');
            }
            for (var i = 0; i < data.FSn.length; i++) {
                doc.setFontType('normal');
                doc.text(startX + 260 + 130, startY += lineSpacing.NormalSpacing, data.FSn[i][0] + ' (' + parseInt(data.FSn[i][1] * 100) + '%)', 'center');
            }
            startY += lineSpacing.NormalSpacing;
        } else {
            doc.setDrawColor(0);
            doc.setFillColor(255, 255, 255);
            doc.roundedRect(startX, startY += 10, 260, (lineSpacing.NormalSpacing * (parseInt(data.FSn.length) + 1)), 0, 0, 'FD');
            doc.setDrawColor(0);
            doc.setFillColor(255, 255, 255);
            doc.roundedRect(startX + 260, startY, 260, (lineSpacing.NormalSpacing * (parseInt(data.FSn.length) + 1)), 0, 0, 'FD');
            infoStarty = startY;
            for (var i = 0; i < data.FSi.length; i++) {
                doc.setFontType('normal');
                doc.text(startX + 130, infoStarty += lineSpacing.NormalSpacing, data.FSi[i][0] + ' (' + parseInt(data.FSi[i][1] * 100) + '%)', 'center');
            }
            for (var i = 0; i < data.FSn.length; i++) {
                doc.setFontType('normal');
                doc.text(startX + 260 + 130, startY += lineSpacing.NormalSpacing, data.FSn[i][0] + ' (' + parseInt(data.FSn[i][1] * 100) + '%) ', 'center');
            }
            startY += lineSpacing.NormalSpacing;
        }

        //OTRO ACUERDOS CONTRACTUALES
        doc.setDrawColor(0);
        doc.setFillColor(255, 255, 255);
        doc.roundedRect(startX, startY, 130, 10, 0, 0, 'FD');
        doc.roundedRect(startX + 130, startY, 130, 10, 0, 0, 'FD');
        doc.roundedRect(startX + 260, startY, 130, 10, 0, 0, 'FD');
        doc.roundedRect(startX + 390, startY, 130, 10, 0, 0, 'FD');

        doc.setFontType('bold');
        doc.setFontSize(fontSizes.SmallFontSize);
        doc.text(startbX, startY + 10 - 1, 'CONTRATO:');
        doc.text(startbX + 130, startY + 10 - 1, 'RETENCIONES:');
        doc.text(startbX + 260, startY + 10 - 1, 'MULTAS:');
        doc.text(startbX + 390, startY + 10 - 1, 'ORDEN DE COMPRA:');
        doc.setFontType('normal');
        doc.text(startbX + 50, startY + 10 - 1, data.Datos.contrato);
        doc.text(startbX + 130 + 65, startY + 10 - 1, data.Datos.retenciones);
        doc.text(startbX + 260 + 40, startY + 10 - 1, data.Datos.multas);
        doc.text(startbX + 390 + 85, startY + 10 - 1, data.Datos.oc);

        doc.setDrawColor(0);
        doc.setFillColor(255, 255, 255);
        doc.roundedRect(startX, startY += 10, 260, 10, 0, 0, 'FD');
        doc.roundedRect(startX + 260, startY, 260, 10, 0, 0, 'FD');
        doc.setFontType('bold');
        startY += 10;
        doc.text(startbX, startY - 1, 'GARANTIA MAYOR A 12 MESES:');
        doc.text(startbX + 260, startY - 1, 'MANTENCION SIN COSTO POST ENTREGA:');
        doc.setFontType('normal');

        doc.text(startbX + 130, startY - 1, data.Datos.garan_ex);
        doc.text(startbX + 260 + 170, startY - 1, data.Datos.man_post);

        if (data.Boletas.length > 0) {

            doc.setDrawColor(0);
            doc.setFillColor(230, 230, 230);
            doc.roundedRect(startX, startY, 520, 10, 0, 0, 'FD');

            doc.setFontType('bold');
            doc.setFontSize(fontSizes.SmallFontSize);
            doc.text(startX + 260, startY + 10 - 1, 'GARANTIAS', 'center');

            startY += 10;
            console.log(data.Boletas.length);
            var salto = 0;

            for (var i = 0; i < data.Boletas.length; i++) {

                if (i % 3 == 0) {
                    //startY = +data.Equipos[i].length * lineSpacing.NormalSpacing;

                    if (i == 0) {
                        var startyb = startY;
                        console.log('inicio ' + startY);
                    } else if (salto == 1) {
                        var startyb = startY;
                        console.log('Salto de pagina ' + startY);
                    } else {
                        startY += (lineSpacing.NormalSpacing * 6);
                        console.log('salto de linea ' + startY);
                        var startyb = startY;
                    }

                    var startxb = 160;
                    var medio = 67;
                    doc.setDrawColor(0);
                    doc.setFillColor(230, 230, 230);
                    doc.roundedRect(startX, startyb, 120, lineSpacing.NormalSpacing * 6, 0, 0, 'FD');

                    doc.setFontType('bold');
                    doc.text(startX + 60, startyb += lineSpacing.NormalSpacing, 'EMISOR', 'center');
                    doc.text(startX + 60, startyb += lineSpacing.NormalSpacing, 'TIPO DE GARANTIA', 'center');
                    doc.text(startX + 60, startyb += lineSpacing.NormalSpacing, 'TIPO DOCUMENTO', 'center');
                    doc.text(startX + 60, startyb += lineSpacing.NormalSpacing, 'DURACION', 'center');
                    doc.text(startX + 60, startyb += lineSpacing.NormalSpacing, 'VENCIMIENTO', 'center');
                }

                infoStarty = startY;
                doc.setDrawColor(0);
                doc.setFillColor(255, 255, 255);
                doc.roundedRect(startxb, infoStarty, 133, (lineSpacing.NormalSpacing * (data.Boletas[i].length + 1)), 0, 0, 'FD');
                for (var o = 0; o < data.Boletas[i].length; o++) {
                    doc.setFontType('normal');
                    if (data.Boletas[i][o] == null) {
                        doc.text(startxb + medio, infoStarty += lineSpacing.NormalSpacing, 'SIN INFORMACION', 'center');
                    } else {
                        doc.text(startxb + medio, infoStarty += lineSpacing.NormalSpacing, data.Boletas[i][o], 'center');
                    }

                }
                startxb += 133;
            }

        }
        
        doc.setLineWidth(0.5);
        var firmay = 750;
        doc.line(startX + 100, firmay, startX + 250, firmay);
        doc.line(startX + 300, firmay, startX + 450, firmay);
        doc.text(startX + 175, firmay += lineSpacing.NormalSpacing, 'VENDEDOR', 'center');
        doc.text(startX + 375, firmay, 'GERENCIA', 'center');
        doc.text(startX + 175, firmay += lineSpacing.NormalSpacing, data.Datos.vendedor, 'center');
        doc.text(startX + 375, firmay, 'MAURICIO GIORDANO', 'center');


        //INICIO SEGUNDA PAGINA

        if (data.Equipos.length > 0) {

            doc.addPage();
            startY = 20;

            doc.addImage(company_logo.src, 'PNG', startX, startY, company_logo.w, company_logo.h);
            doc.addImage(company_logo.srcf, 'PNG', startX + 400, startY, company_logo.wf, company_logo.h);
            startY += lineSpacing.NormalSpacing + company_logo.h;

            startY += lineSpacing.NormalSpacing;

            doc.setFontSize(fontSizes.Head2TitleFontSize);
            doc.setFontType('bold');
            doc.myText("MEMO DE VENTA", {align: "center"}, 0, startY);

            startY += lineSpacing.NormalSpacing;

            doc.setDrawColor(0);
            doc.setFillColor(255, 255, 255);
            doc.roundedRect(startX, startY, 100, 20, 0, 0, 'FD');

            doc.setFontSize(fontSizes.TitleFontSize);
            doc.setFontType('bold');
            doc.text(InitialstartX, startY + 20 - border, 'N° ' + data.Datos.codigo);

            doc.setDrawColor(0);
            doc.setFillColor(170, 170, 170);
            doc.roundedRect(startX + 100, startY, 320, 20, 0, 0, 'FD');

            doc.setFontSize(fontSizes.TitleFontSize);
            doc.setFontType('bold');
            doc.setTextColor(255, 255, 255);
            doc.myText(data.Datos.proyecto, {align: "center"}, 0, startY + 20 - border);

            doc.setDrawColor(0);
            doc.setFillColor(255, 255, 255);
            doc.roundedRect(startX + 420, startY, 100, 20, 0, 0, 'FD');

            doc.setFontSize(fontSizes.TitleFontSize);
            doc.setFontType('bold');
            doc.setTextColor(0)
            doc.text(InitialstartX + 420, startY + 20 - border, data.Datos.fecha);

            //INFORMACION DE EQUIPOS

            doc.setDrawColor(0);
            doc.setFillColor(230, 230, 230);
            doc.roundedRect(startX, startY += 20, 520, 20, 0, 0, 'FD');

            doc.setFontSize(fontSizes.SubTitleFontSize);
            doc.setFontType('bold');
            doc.setTextColor(0);
            doc.myText('EQUIPOS', {align: "center"}, 0, startY + 20 - border);

            startY += 20;
            doc.setFontSize(fontSizes.SmallFontSize);

            console.log(data.Equipos.length);
            var salto = 0;

            for (var i = 0; i < data.Equipos.length; i++) {

                if (i % 2 == 0) {
                    //startY = +data.Equipos[i].length * lineSpacing.NormalSpacing;

                    if (i == 0) {
                        var startyb = startY;
                        console.log('inicio ' + startY);
                    } else if (salto == 1) {
                        var startyb = startY;
                        console.log('Salto de pagina ' + startY);
                    } else {
                        startY += (lineSpacing.NormalSpacing * 8);
                        console.log('salto de linea ' + startY);
                        var startyb = startY;
                    }

                    var startxb = 160;
                    var medio = 100;
                    doc.setDrawColor(0);
                    doc.setFillColor(230, 230, 230);
                    doc.roundedRect(startX, startyb, 120, lineSpacing.NormalSpacing * 8, 0, 0, 'FD');

                    doc.setFontType('bold');
                    doc.text(startX + 60, startyb += lineSpacing.NormalSpacing, 'TIPO DE EQUIPO', 'center');
                    doc.text(startX + 60, startyb += lineSpacing.NormalSpacing, 'MODELO/MARCA', 'center');
                    doc.text(startX + 60, startyb += lineSpacing.NormalSpacing, 'COMANDO', 'center');
                    doc.text(startX + 60, startyb += lineSpacing.NormalSpacing, 'CODIGO FM', 'center');
                    doc.text(startX + 60, startyb += lineSpacing.NormalSpacing, 'KEN', 'center');
                    doc.text(startX + 60, startyb += lineSpacing.NormalSpacing, 'PARADAS / ACCESOS', 'center');
                    doc.text(startX + 60, startyb += lineSpacing.NormalSpacing, 'CAP. PER / CAP. KG', 'center');
                }

                infoStarty = startY;
                doc.setDrawColor(0);
                doc.setFillColor(255, 255, 255);
                doc.roundedRect(startxb, infoStarty, 200, (lineSpacing.NormalSpacing * (data.Equipos[i].length + 1)), 0, 0, 'FD');
                for (var o = 0; o < data.Equipos[i].length; o++) {
                    doc.setFontType('normal');
                    if (data.Equipos[i][o] == null) {
                        doc.text(startxb + medio, infoStarty += lineSpacing.NormalSpacing, 'SIN INFORMACION', 'center');
                    } else {
                        doc.text(startxb + medio, infoStarty += lineSpacing.NormalSpacing, data.Equipos[i][o], 'center');
                    }

                }
                startxb += 200;

            }

        }
        
        doc.setLineWidth(0.5);
        var firmay = 750;
        doc.line(startX + 100, firmay, startX + 250, firmay);
        doc.line(startX + 300, firmay, startX + 450, firmay);
        doc.text(startX + 175, firmay += lineSpacing.NormalSpacing, 'VENDEDOR', 'center');
        doc.text(startX + 375, firmay, 'GERENCIA', 'center');
        doc.text(startX + 175, firmay += lineSpacing.NormalSpacing, data.Datos.vendedor, 'center');
        doc.text(startX + 375, firmay, 'MAURICIO GIORDANO', 'center');


        //INICIO TERCERA PAGINA

        doc.addPage();
        startY = 20;

        doc.addImage(company_logo.src, 'PNG', startX, startY, company_logo.w, company_logo.h);
        doc.addImage(company_logo.srcf, 'PNG', startX + 400, startY, company_logo.wf, company_logo.h);
        startY += lineSpacing.NormalSpacing + company_logo.h;

        startY += lineSpacing.NormalSpacing;

        doc.setFontSize(fontSizes.Head2TitleFontSize);
        doc.setFontType('bold');
        doc.myText("MEMO DE VENTA", {align: "center"}, 0, startY);

        startY += lineSpacing.NormalSpacing;

        doc.setDrawColor(0);
        doc.setFillColor(255, 255, 255);
        doc.roundedRect(startX, startY, 100, 20, 0, 0, 'FD');

        doc.setFontSize(fontSizes.TitleFontSize);
        doc.setFontType('bold');
        doc.text(InitialstartX, startY + 20 - border, 'N° ' + data.Datos.codigo);

        doc.setDrawColor(0);
        doc.setFillColor(170, 170, 170);
        doc.roundedRect(startX + 100, startY, 320, 20, 0, 0, 'FD');

        doc.setFontSize(fontSizes.TitleFontSize);
        doc.setFontType('bold');
        doc.setTextColor(255, 255, 255);
        doc.myText(data.Datos.proyecto, {align: "center"}, 0, startY + 20 - border);

        doc.setDrawColor(0);
        doc.setFillColor(255, 255, 255);
        doc.roundedRect(startX + 420, startY, 100, 20, 0, 0, 'FD');

        doc.setFontSize(fontSizes.TitleFontSize);
        doc.setFontType('bold');
        doc.setTextColor(0)
        doc.text(InitialstartX + 420, startY + 20 - border, data.Datos.fecha);

        //INFORMACION COMERCIAL

        doc.setDrawColor(0);
        doc.setFillColor(230, 230, 230);
        doc.roundedRect(startX, startY += 20, 520, 20, 0, 0, 'FD');

        doc.setFontSize(fontSizes.SubTitleFontSize);
        doc.setFontType('bold');
        doc.setTextColor(0);
        doc.myText('INFORMACION COMERCIAL', {align: "center"}, 0, startY + 20 - border);

        doc.setDrawColor(0);
        doc.setFillColor(230, 230, 230);
        doc.roundedRect(startX, startY += 20, 520, 10, 0, 0, 'FD');

        doc.setFontType('bold');
        doc.setFontSize(fontSizes.SmallFontSize);
        doc.text(startX + 130, startY + 10 - 1, 'MONTO SUMINISTRO IMPORTADO', 'center');
        doc.text(startX + 260 + 130, startY + 10 - 1, 'MONTO SUMINISTRO NACIONAL', 'center');

        doc.setDrawColor(0);
        doc.setFillColor(255, 255, 255);
        doc.roundedRect(startX, startY += 10, 260, (lineSpacing.NormalSpacing * 4), 0, 0, 'FD');

        doc.setDrawColor(0);
        doc.setFillColor(255, 255, 255);
        doc.roundedRect(startX + 260, startY, 260, (lineSpacing.NormalSpacing * 4), 0, 0, 'FD');

        doc.setFontSize(fontSizes.SmallFontSize);
        doc.setFontType('bold');
        doc.text(startbX, startY += lineSpacing.NormalSpacing, "MONEDA: ");
        doc.text(startbX + 260, startY, "MONEDA: ");
        doc.setFontType('normal');
        doc.text(startbX + 40, startY, data.MontoVen.nomsi + ' ' + data.MontoVen.simsi);
        doc.text(startbX + 40 + 260, startY, data.MontoVen.nomsn + ' ' + data.MontoVen.simsn);

        doc.setFontType('bold');
        doc.text(startbX, startY += lineSpacing.NormalSpacing, "MONTO: ");
        doc.text(startbX + 260, startY, "MONTO: ");
        doc.setFontType('normal');
        doc.text(startbX + 35, startY, data.MontoVen.montosi);
        doc.text(startbX + 35 + 260, startY, data.MontoVen.montosn);

        doc.setDrawColor(0);
        doc.setFillColor(230, 230, 230);
        doc.roundedRect(startX, startY += 20, 520, 20, 0, 0, 'FD');

        doc.setFontSize(fontSizes.SubTitleFontSize);
        doc.setFontType('bold');
        doc.setTextColor(0);
        doc.myText('EQUIPOS', {align: "center"}, 0, startY + 20 - border);

        doc.setDrawColor(0);
        doc.setFillColor(230, 230, 230);
        doc.roundedRect(startX, startY += 20, 520, 10, 0, 0, 'FD');

        doc.setFontType('bold');
        doc.setFontSize(fontSizes.SmallFontSize);
        doc.text(startX + 110, startY + 10 - 1, 'EQUIPO', 'center');
        doc.text(startX + 220 + 75, startY + 10 - 1, 'SUMINISTRO NACIONAL', 'center');
        doc.text(startX + 370 + 75, startY + 10 - 1, 'SUMINISTRO IMPORTADO', 'center');

        doc.setDrawColor(0);
        doc.setFillColor(255, 255, 255);
        doc.roundedRect(startX, startY += 10, 520, (lineSpacing.NormalSpacing * (parseInt(data.MontoAsc.length) + 1)), 0, 0, 'FD');

        for (var i = 0; i < data.MontoAsc.length; i++) {
            startY += lineSpacing.NormalSpacing;
            doc.text(startX + 110, startY + 10 - 1, data.MontoAsc[i][0] + ' / ' + data.MontoAsc[i][1], 'center');
            doc.text(startX + 220 + 75, startY + 10 - 1, data.MontoAsc[i][3] + ' / ' + data.MontoAsc[i][4], 'center');
            doc.text(startX + 370 + 75, startY + 10 - 1, data.MontoAsc[i][5] + ' / ' + data.MontoAsc[i][6], 'center');

        }

        doc.setLineWidth(0.5);
        var firmay = 750;
        doc.line(startX + 100, firmay, startX + 250, firmay);
        doc.line(startX + 300, firmay, startX + 450, firmay);
        doc.text(startX + 175, firmay += lineSpacing.NormalSpacing, 'VENDEDOR', 'center');
        doc.text(startX + 375, firmay, 'GERENCIA', 'center');
        doc.text(startX + 175, firmay += lineSpacing.NormalSpacing, data.Datos.vendedor, 'center');
        doc.text(startX + 375, firmay, 'MAURICIO GIORDANO', 'center');
        
        doc.save(data.Datos.codigoimp + ' - Memo de Venta.pdf');

    });
}


function pdfcorto(idventa) {

    $.post("../ajax/venta.php?op=PDFCorto", {idventa: idventa}, function (data, status) {
        data = JSON.parse(data);
        console.log(data);
        var company_logo = {
            firma: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAALqnpUWHRSYXcgcHJvZmlsZSB0eXBlIGV4aWYAAHjarZhpchyxjoT/8xRzBG4gyONwjZgbzPHnA7skWbJk+02M2u7qroULkMhMtNv/89/H/Rd/KZXssmgtrRTPX265xc6H6l9/r2Pw+b7fv5Kfa+Hzefd+IXIqcUzPA/u5v3NePh7Q5/4wPp93Op9x6jPQc+FtwGQzRz4899VnoBRf58Pz3bXnuZ5/2c7z/8x4L8t4Xfr6PSvBWMJ4Kbq4U0ie9ztLYgWpps5ReU/Jbgqc70n4Zu/6fezc+8cvwXv/9CV2vj/n0+dQOF+eG8qXGD3ng3wfuxuhX1cUPmb+dMGvt/B+E7uz6jn7tbueC5Eq7tnU21buJ24knDndxwov5b/wWe+r8apscZIxm27wmi60EIn2CTms0MMJ+x5nmCwxxx2VY4wzpnuuJo0tzpuUbK9woqaWliNHMU2yljgd39cS7rztzjdDZeYVuDMGBrMs/vZy3538v7zeBzrHYhuCr++xYl3RMM0yLHP2zl0kJJwnpnLje1/uF9z4XxKbyKDcMFc22P14DTEkfGAr3Twn7hOfnX+VRtD1DECImFtYTEhkwJeQJJTgNUYNgThW8tNZeUw5DjIQROIK7pAb+ITk1Ghz84yGe2+U+DoNtZAISYWyqWSok6ycBfxormCoS5LsRKSISpUmvUBQRUopWoyjuibNKlpUtWrTXlPNVWqpWmtttbfYEhQmrTR1rbbWemfSztCdpzt39D7iSCMPGWXoqKONPoHPzFNmmTrrbLOvuNKi/FdZ6lZdbfUdNlDaecsuW3fdbfcD1k46+cgpR0897fT3rD1Z/Zy18CVzf85aeLJmGcv3Pv3IGqdV34YIRidiOSNjMQcyrpYBAB0tZ76GnKNlznLmW6QoJJK1IJacFSxjZDDvEOWE99x9ZO6PeXOS/6O8xZ8y5yx1/x+Zc5a6J3O/5+2brK1+FSXdBFkVWkx9OhAbN+zaY+2mSX8/NsJEXaxCdccuLa61U67gqMnMRkqkp8V8YmHb9WSibRSaahef0pr+sA2ebZxPfnUNzesmPHWjidFJ3QyZcyNJjFgShCeMmbtvYdZekNqWJW54MCpjAQxKOosp4y9H9/XEn49xe+ZdpG7Cqax4L16y53RnjsbJtWvogxXHlv08ILjtRPYzGt1viAjx+nGe1t3XYMo8YaXWfZ06FtmZYY0xPCGZxcK3xhpzrNYOYW92UyLu6op/TRj+YX+rE9As5hxGDYNxiNwC9SzC/ZjqRGCZsNjqbgI/Dd1NKCHJvsLIFhvcCKtZb4F4X1coZYJjXZnyCzJiORiRNQWZ07BHS0d6X8JVhYsHA2k15FCbsDgY6Kxik/5TetmrxAa8Yx9l28aYir3V5t/i/3F0z4f0rwAIEHQ/i2pYvjHbSRTdVnXapIIMgFG07gEC89hy2jzU3T5xaFhtnH1KGoXPm7Tl0aQsSvMVzwwdBPeGtBqB7xOdSMUXCh9miLDKqLLGJr5hwkcn2YyTooF+gN3sAikud5jh7EnodODj1p61ADxKaRAPw+8LPlgNm/PmnuO4MCCyJBjYCun/dP0eO8n2+XSJKin3Za5on4v98IAj//aUez6AKjBWKY5d2m8p+f0o2CzowAzZ0LpGcxs06frpYZbXTH7EqDOgxZWlQa/M+eQROl/mO1xCKhpxiG1kAIQHQznU5wXxjime3A5WOkbk+ZC1t5N3Ext7JJhzaSrYqOXwCRGQtaVlVsovGQsxbTLShoxXh1zXiavJ0N1fMYl4uS8V5f6JXTP5gQXLsMaFvYC1WnXuurRT/7Pv7aAitqQSKK16EqpR2haTOMGiowkJ73e0o1BkOY3dGGkOROIVxzdcuF9TSFxoj3zrMWdtxC6EkgPEXcRE/AjVugqiNahUiw5InBTXiawo10B0EEYSEaYRqXaPtCFTJGpSFroOvgo/n+NEGTMS3A2zpup+W73MZnzEO+jHCvT1z4X7zdFdD9FMdFKDXTSzcsMV5iDvbFz7VwY1DjRkk5mNtccaGPPlgqfGlBj+9pFEt7AzxbjP0AItxMFH3CSUPaCMszEPyOSE2CDe0+DDNFtF+MuaSH9Y8OCZK23REQpFNjrUMft5gPQVJ3B2y8G4KFyV6nQNe0/lUrk0oOv7Iv2paP9249UBIJ6PYnKisLOwkeV2Ug/SMKPAr/qTKftj0/81IKehq3Bd7nvhd/wZdFlQr1unQOuwq7a1mSwYA4Z6Zq9AZNIJnXloGeKyQZYfOa5R1eBCUZzasUO1p+ba3mEydQd49Y2DP6UYWPhhFpPK31Z2AnvmzQZLXvAdtAECHAqFcCVBAHgWpxFYnK8YmQSQvOSmg1iYQgDzFUcMWg883AHfzmTpzA1POFh1zuMnbBMhdzwdvgf/QGOG/FEqHQmYRj6okCKt0cgeJ9isLnOBqYBHKS5NigQIg86OVqygcXgLBvQwkIwJj2JfvarRt5ovC2WtZkinEkbAS5CT4mg8kawga1aNOOc1w8mYBWoEfa1P+uMfCdyO7suJOM03JYHHGzupYefJEiu8gO6c03dLe+HdjtFCBVuGB1XSD9ImbyUh8RRA6DoBDw4e/k1s4mBFVEyDsznRHiyrZgab1YMxx60S1+CX2SzVm3Dzj0Bh2mdDAhPlAvHXtHy9pmIngHrK3KWDyQGtb53jJDmm/dl6QWSKXnzXyIy4kOU7fcCxsrMSpZnYaK953LlikJcLgnSHNswC1ZJcbWsRbELfu5mBfbAJsQ6ZIVp24FL2vL2xLOAsN5LFNNu00ABar1668dWL/fUYzbhArt56GYh5l8py3Ue48JaE3G63UBIYm/KLUL/4hniqH1QyTi6xqMk1B++sKigzDg7VO2FKnLVSkR7jVitbo6Aosw4EgpHDdZ+rhoTM5XecfeCIDLPLFwXFK/b/2JvkW8ru6Q+qqf9rQLPUFtw01nVebATfVVRw8fRwVpjcvbHH2mbFJwyYZrjKA/XQf0gHUihqY4rVoIN5cyzmdcxMh0gbQvUgjlKAlezccKWNTiKPFF2hb0MrjSIXgxVoD7fHGLRvSPWNsU65thKZKJfkBl7vjHSNJZ4OTvIOgwULTtrOOCkiE3Pr3l/r1m6tH66zAmo2KBtEN4bAJIWxGRn7jXSskxxsBztaU4ECWYOV4Jqtw2ZuFzfoNrJUygi0pILDKJUOdPZysu+IqPWT87iw4dzcMCxIJmy5aGqpuZd8v6z5vooF0bGpm1DPLu3bNud263ereyn/TyokrUJjNroti+bgR6vspBiEM8VN85wQXGq8EWfrwqcxfriesX82yA/kzLOnyLomZtRMdCac0wPyGAwzitQ+1DWGRqMugmrCdplLjbkWpo09W7MIVPDZWM+TXqI9uXBbpzotPtkcdyUKxmi32F4T0iBc6oUwEEDzn/ajCYD8sMZg+aXPdK7YUmuyHyNcvhgAfdigG4bu487/ldbtLcM8qFDeKHeEuAVt7UaY0G6VCizx2UDhsu0edA0sAzbNMiOLWlAiDYrptlpTdw37N1psR/fhuwwW1+0f+4FnImgYOCrAftSMxZoctXLGkIyGd2/WC9Kr+xNjTNVNbkaLU8WdCbY/b9ZVTUkWfTMV+cT9UitB04HQW29DQVDcMxSgPMJ0gWnWut2+rBRovCMYozmEwQoKQIPQtdAi2y+1ecenjn/ru9wfTdZVjfsZikAoC/bYQrwg0/Zq1AmuNWA0fuOjz7HfU5AS4DH9/lkVIvaXoVn3yVcDrkC+WqlABCilak16r+AO954hF/upA6kG6kDvFbAfjID7a4v3qXsrhtmROQXG336E8JCld1YbhmH7oQSk+4b3iZP2R+Cv6e2XEDiuz9Za0E4VYxUn/RbO1Ix4RrJHPjIdBJ8IIAYvacEpWLVjA8dY5naWtfj4toVLgkckz4x1U8Jo7IflgDSbblndJVPKvKn9vPCrxlz4MHprTljGKgsp1jLBKZixd1r5qk3uZ9E6LAbo/y+ZDI6MQRnmYAAAEOBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+Cjx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IlhNUCBDb3JlIDQuNC4wLUV4aXYyIj4KIDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+CiAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgIHhtbG5zOmlwdGNFeHQ9Imh0dHA6Ly9pcHRjLm9yZy9zdGQvSXB0YzR4bXBFeHQvMjAwOC0wMi0yOS8iCiAgICB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIKICAgIHhtbG5zOnN0RXZ0PSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VFdmVudCMiCiAgICB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIKICAgIHhtbG5zOnBsdXM9Imh0dHA6Ly9ucy51c2VwbHVzLm9yZy9sZGYveG1wLzEuMC8iCiAgICB4bWxuczpHSU1QPSJodHRwOi8vd3d3LmdpbXAub3JnL3htcC8iCiAgICB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iCiAgICB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iCiAgIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6Q0MxRTdERTM2NkIzMTFFNkFCRjlGOTU5ODYyNkQ1N0MiCiAgIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NmM0OTBlMGYtNzBlZi00OTIwLTkxNzctMjNjZWVmMjFjZWJjIgogICB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6MDc1ZDQ1ZjctODliNy00YzY0LWE1NTctMjBhNmRiNmQ4YjRkIgogICBHSU1QOkFQST0iMi4wIgogICBHSU1QOlBsYXRmb3JtPSJMaW51eCIKICAgR0lNUDpUaW1lU3RhbXA9IjE1MzI5Njk2NDM5MDQ2MzMiCiAgIEdJTVA6VmVyc2lvbj0iMi4xMC4wIgogICBkYzpGb3JtYXQ9ImltYWdlL3BuZyIKICAgeG1wOkNyZWF0b3JUb29sPSJHSU1QIDIuMTAiPgogICA8aXB0Y0V4dDpMb2NhdGlvbkNyZWF0ZWQ+CiAgICA8cmRmOkJhZy8+CiAgIDwvaXB0Y0V4dDpMb2NhdGlvbkNyZWF0ZWQ+CiAgIDxpcHRjRXh0OkxvY2F0aW9uU2hvd24+CiAgICA8cmRmOkJhZy8+CiAgIDwvaXB0Y0V4dDpMb2NhdGlvblNob3duPgogICA8aXB0Y0V4dDpBcnR3b3JrT3JPYmplY3Q+CiAgICA8cmRmOkJhZy8+CiAgIDwvaXB0Y0V4dDpBcnR3b3JrT3JPYmplY3Q+CiAgIDxpcHRjRXh0OlJlZ2lzdHJ5SWQ+CiAgICA8cmRmOkJhZy8+CiAgIDwvaXB0Y0V4dDpSZWdpc3RyeUlkPgogICA8eG1wTU06SGlzdG9yeT4KICAgIDxyZGY6U2VxPgogICAgIDxyZGY6bGkKICAgICAgc3RFdnQ6YWN0aW9uPSJzYXZlZCIKICAgICAgc3RFdnQ6Y2hhbmdlZD0iLyIKICAgICAgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDpkYzI3YTlmZS1hYTBlLTRkM2ItOGU1Yi0zMjhiNDJkYTk1ZTIiCiAgICAgIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkdpbXAgMi4xMCAoTGludXgpIgogICAgICBzdEV2dDp3aGVuPSItMDQ6MDAiLz4KICAgICA8cmRmOmxpCiAgICAgIHN0RXZ0OmFjdGlvbj0ic2F2ZWQiCiAgICAgIHN0RXZ0OmNoYW5nZWQ9Ii8iCiAgICAgIHN0RXZ0Omluc3RhbmNlSUQ9InhtcC5paWQ6NGNiMGYzOGYtMWJhMC00MDdjLTgyODktMGE0ZGVlMDM4ZTY0IgogICAgICBzdEV2dDpzb2Z0d2FyZUFnZW50PSJHaW1wIDIuMTAgKExpbnV4KSIKICAgICAgc3RFdnQ6d2hlbj0iLTA0OjAwIi8+CiAgICA8L3JkZjpTZXE+CiAgIDwveG1wTU06SGlzdG9yeT4KICAgPHhtcE1NOkRlcml2ZWRGcm9tCiAgICBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkNDMUU3REUxNjZCMzExRTZBQkY5Rjk1OTg2MjZENTdDIgogICAgc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpDQzFFN0RFMDY2QjMxMUU2QUJGOUY5NTk4NjI2RDU3QyIvPgogICA8cGx1czpJbWFnZVN1cHBsaWVyPgogICAgPHJkZjpTZXEvPgogICA8L3BsdXM6SW1hZ2VTdXBwbGllcj4KICAgPHBsdXM6SW1hZ2VDcmVhdG9yPgogICAgPHJkZjpTZXEvPgogICA8L3BsdXM6SW1hZ2VDcmVhdG9yPgogICA8cGx1czpDb3B5cmlnaHRPd25lcj4KICAgIDxyZGY6U2VxLz4KICAgPC9wbHVzOkNvcHlyaWdodE93bmVyPgogICA8cGx1czpMaWNlbnNvcj4KICAgIDxyZGY6U2VxLz4KICAgPC9wbHVzOkxpY2Vuc29yPgogIDwvcmRmOkRlc2NyaXB0aW9uPgogPC9yZGY6UkRGPgo8L3g6eG1wbWV0YT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgIAo8P3hwYWNrZXQgZW5kPSJ3Ij8+HW1V7gAAAAZiS0dEAP8A/wD/oL2nkwAAAAlwSFlzAAAuIwAALiMBeKU/dgAAAAd0SU1FB+IHHhA2A5mNjyoAAAvlSURBVHja7V1dbBxXFf7OvfOzu17b4zrkv/aGpEJQUByJH6V5yEQVQm0eSAQVgqeVIEJVQTIPwBPqIB6rOq5aVAkQdiueKiobyUW8wLqQRgiK1hGoD1HLOmqEUFBZE1VJam+8POzdzfV4Zmd2997ZWdtHmofEszPnnu/ec8797rl3gD3Zkz0JFxoEJc/bj5lvfHT1zAiyx27j7mQGZuEeNiYfNIJQR33LbzIwb9zDxuoIsjdu427lvP3YW298dHVjD/Iu5CiNHx+CfYmB5jnYdQB1FRcHu85A80OwLx2l8eN7lm4vFzIwXwJQUQVAjKsi3nlhz2U1pMDBvkvAV2rYnIxx/wqAtRysN+9gHQAwBPvDPGWvWTBz79f/c6p5Yw4W7mD9LAAHwFTUgw2wG3Xg9fvYfBHA6m4bDa4F/tt2vZeB3rVhvAygGMegMWQKQDED8yUGerfdu4Vu7q4AAkApzBAG+J8BTHOwgm5FxDumxTvDwCntVGAKYUAwUMUE98Q9fdPPBPcYqNIGmMLAo3DG+IwNwAtqZA7WX4U7SpsUhW5BwHiiTQMpUyb4P4IyGwe5r6ZdeaHjthFjgr+jKKYlmbrRswFAVHOwntFmv4avV24ooXPV3x7RRqS/YwXECgPstTxsR+M7y9Lkb078nzLJw3YMsNdCYouTWhcVMMSrBNI66TLB5wM6wIqO0SLa4h8tldS5sFFkpwIULT/Cjoxpfu/FsJTVBF+zYSh3kY+wI2M2jGV/x3OQSw0oRQaqBcUMnT0nD7sQ5NsD0upF1W5laWIGBPzS955aGrLGYoRBtIFigl9FfFJxdZzyp1TrkIEZ1P5iv9zU2ZgGUQ6K1ZhIdkwm5mB9P4lOKWyTeACvdmAMZaBwMBc9MLwEKh0gx9EMilZ3HZRmVrswhgolHRN8Fb3T7tVhZNwEQEkkJS73YoheQBEBWtl6yBBsb2liRmVa7AelrBWJIdieit7ZDSgZmE9DwyIVA72dhamMOLRhPO8HXRcerkJDdArKFAdbg76Vw2oGprIJbBamfyS7yuOGJflu4cfLSYCyNDFDBCojgSXdDMxZRWzullgnbKcunhhgP99KTXDXzyHpAoWDzSK5NXZlfl/YqPVcG8asFldlgj8XRuypBiUP+4mEwVDqYgg0o/y5THIX1CDSnHZsqypQJml/t+l1r0G+9igrqOLgHJJWIRloRWluvY+GH49DgasAhUClfowOC3xaZfAdp/zjKqmVio/7j5owKgFlCNZ0f8AwljRlqCUfXd/76DhEY3HSVFWg9CNuVB3ktMyshe16HiUViSx7pUNqpdwno3Z9jSLnQqOMNGzY3SgZQfZLW2eb1pku+K6BAYWBLuvmm4QNW+8UNo4tC5Kyyz2QkIMASlklnxWRsS5L712I9aNjdHBMYVaQalAIVE24Cn5LXBa2jhQ5w6kq6D2pBSUHq5ggGBC2lJOW6BTbhvEn6QdzCtdQUgUKB/s1+iNzUpp9JY7hWkpnYZ5XvLCVClBEDW9f6qkC6KBwPYaReco3H1DOGqcBlP00egr9lZbbEjaXAr8kG7h/VsoIfq9BkTUA59DYeNMXIdCPb9X/V+4nGrJtZZsHKVuWAt63NerUl5FigC0jBTIE+5KU6QV3jizMUV8DdFdNJA1K9SjtO4R0yJQvVo9GrnskpFhioFjgqdnYKSRwnYRJ6e5p6eakhvaaCV7T/ZIcrBfWcX8xZYAsS97pC9sA4WAHpJtXk9DIAp/ewP3P6nyHAXbtDtY9pE9Wpdh9aBsgd7B+Urr5hm5tDtNDx2rYfFYzGHdr2CyK7C5tciPI9uxBbzVavMooctd0a3OrvnZ5E3VHIxi/qWHzG/1MsduJbGNbsn1kkElicqQjo0L69wEGJlGsj7n4C5qztxIGbXNmn0cIbBjPmOBru3SkhE8zBC3c+uPSxAzXqclBGhuWJ0liT+CuAmVpYob5bN7WZWmbGJ5gh4f+Xa/+kYGWRe3VSg2bLgO9ssvcVz22y2IgbTuAsjDn5d47RkPnmn8bQfYpjcE+VSPl0+zYx6OYkSRiyByCd8w+1xyyh2jsmEY6JU2guP0GZC7CWOWHKF9oxjQGCiyyJuC/OwSU9oD4sh2lgHCwX8Q11ggyLRJwGJknZBdmgs9nYY4y0Ns7AJQWIFbD9tukVe6Yh62svvUEO/xNdL5P42URjPEJ9vBYHvb3sjCLTbe2E0ARNg4v081s3fXj9QsMKbFYmaT9o22Sg0EHxZM64GLbGxBdWB0pJvjPejRWpA6DDAoBf2g7AHKwvg4FFdoxA3jktY+GCzHT6EEFpSKFiK8FBd6TiFueEj77JANsvlcw8rB/0uHcZtBA2VJuJWwfNIyoldE4lPtiJ2/YR8NxlmPLANyI/eZdbbgfJFCEbVvlrKE3ig0rTdSe74AkdGPMsKvNeYav/st/TkjX1S6DAorYpxm9SUgcztLRblSxuzSyocNifmGBOwb4v0Luq/RaSzwgoMjlVm0PxPHv9IkKrFOIV9Q8K43CxTaj46IivizNoBTQSbmVCX5Tyo+fbndvBmYkIAQqn2CHh8RoCt07aIIvKyYxUwmKfESIsHVHKWsct9VuctdqUBR4YqM9dgEocuITvbtAHFskp6CFCJ4qdMUv2zhxrZFYtPnqQQ+7tAYKFGFLOa7G7oQVKduKOhKiFMJHzUuB/1ftGiyviexkUHzHhHQ0+fY6mBeUEHz6syN6RbFdQ20YiZTppAAU/8kU8fnCSdp/QFbEodx3OgHEAGsqHedYpWJSXEU/QRE2bD1H2Lg7PsoEX12amKE4gBhgTereYdHHKlXbPHfHgLI0MUO+owm72ipYQIyzzq1GutrMlBalhv80RsPm0AdJGpSAs/AL3eoup8BVC9wJS3tN6ZAuseoXZ1uy1i0COVhTImHwRFtKDFRloNoocj9KAhRhs6qqTljwzbhfDANknIY/J0jGQgxeq26BryhyVwUALgebzsHyhAuN9WExG8ZJ3aAMNWymZHQAAIzthxbLubMreronfCXCGF8xSSwB8DhYsRNGVwDnigTAY6BFa/v5691sjS5odl+urwN4Kka+45uXXJeM6cqTOtpeKVIBcFkc1h+HVphykNvmYqChJEg+ck8VKAz05S2JFfBPny0cVUHwfBhZGMTxc7B5QckHrJuMHGm4GJoWPabkU7znywT/UIDZbhQ5GgJ9y4MIG8mHzailhrIwt7zAQe6ij4ZfFr26ILkaAJgm0IIhZWMKr7JYT/BsGEU0Pse3xdBhoBjgbh72FABXVIB4BPob1BTjeT4KaRYaxBGz6tgZBmvEll79/GpjFNFsFuYPAbjNha4oscAdDnYL0L+pNIKJ0LYxaWprpmS81+4UNgItxDT6PQAl8bUcj4EujCB7uldlSUGxhYJL+5Kwn5sqh/WAEWRdQ6JOCFQ2wX8HwBNMsKvx21ShxGdE/LkNoGSBLypwYYnRQnNxQbHAnf00WkAfJAfrB23o/pJI1z0AbtCpeT0G+mRZCGv7x7jKSOEXy4aRmRKZj9vNen03oFgw5vvR1qCyn1SCkjD31VcbbAPFAFsZoeyjuxSUVHRIx4bxuj8XH6fhc7sJFAt8IW3eYS4gB/eSOvEzCVmamEEGxuW0LCPEkaAyn/I45T856GCINgQRp9Np191F8Md8vWZ91iDJSX48S8GMQxUJ7edXFewXEHz4ZHGA8CiGfPQ+dfEilowg+62g0WLD+PtBGnsyrXofJOfJLMwg91QVbRpocRDOKVWMDherdOppghcRvto4hx02x3LDuCWxR2JOnGmbqIh3zlH4QlhpkGKFUmDwYOVtHkDxODv8MdUvF88sine0W43c8UD448tpxKPHKyKIemKJ1I1THNFchxe/8cQz4hQ/zKmg/gc6xohD8bs6TiMDo2rDuGLDuJKB0e36e1no4GBPtruTDMxX5X0qqi8T/GYG5qu63GIvQmkGKAuzcA+1syb4p9ZR+7wF49g6apOdPMOCcWMdtYoF4y8buP9OBsabd7GxmtY20yCOpKM0fvxm/YOHm//m4tiv+9iU73n/Zv2D9/b8zp70JP8HKuBBAtm3G18AAAAASUVORK5CYII=',
            srcf: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wgARCAAoAH8DAREAAhEBAxEB/8QAHAAAAgIDAQEAAAAAAAAAAAAAAAUEBwIDBgEI/8QAGgEAAgMBAQAAAAAAAAAAAAAAAAMCBAYFAf/aAAwDAQACEAMQAAABtDTZ/iuvzWKHLnpYocAuenrOZfuLK6MAAAAAACm9XnKy0PEbVnxWQYIdHZCG1XRULlz5PSc7foxGezEz99Vj4/rubbaVrNd93j173eRLVOMyDqpZ3rnHZD09uHK6OC2EVid0WYSVol6+pv3wZ8473G1TpeC9p2kVyq9p2gEVyrY/B7P0hgdmltV5ipa/ZaGJy8m5qulqaAAAAAAAAAAAAAAAB//EACUQAAICAgEDAwUAAAAAAAAAAAQFAwYBAgATFzUHFRYiMDQ2QP/aAAgBAQABBQK2WwtCx7jsuH35gLP3HZcZX5gGxW35gYx7jsuVi4mOmv2LGi+QWr470UEdf3clsaxHosMrE7TZUi6ESyuQFp6kOKNaHsmIl2Sth9tHE5BMDuaPT3mbeUBvP01c8hS7hJcAd0ZHjWpAGaISLtuFV0GjvChcW8CcDU5lELX1v13omfAo8LKPaHZ4NHjLaDqYeB5xE/Gl3gn0Ji56j+c45/L4984i85ygfsJYcZ0cleGzrIq0kyQminh3RaSz4Rx68FG1Eg/h/8QAOBEAAQIDBAUJBgcAAAAAAAAAAQIDAAQREiExUQUTQWFxEBQiNIGhwdHwFTKCkbHhIzBAQlKy8f/aAAgBAwEBPwGfn3ZV0IQBhHtmYyHf5w7pZ9CqADAd4rnHtmYyHf5w9pZ9t1SABcfW2GdLPuOpQQLz62x7ZmMh3+cSOkXZl7VrA/JnJXnc4EVp0fGOZ2ZfnDhpkKYwJQzK7RNEgJv+EQ9JANa9hdpMOSK39a8jYo3QxK2QzMVxULu37QzJocY17i7I4VjR6G0TYDarQplSJo2Wqk0vH1EKcKFfgmqdmJvsr7TgLr918NvuOKSon+XA4YUUd/yMGYWlZCb7/l0gL+ypGGG2BNrKjeAMzgBVYrjTYMseAjXqTYFcaZ1NcacNt1RiaCJZanWUuKxIryLcQ3PVWadDxh51uflgpSqLT3w262tCpV02ahN/wjyglmRl1oSu0peUCZEu08UnpWzd2iHJpqYSzYuNsXRo55KJazbANdsM36QtWgqo2QpQQCo7IQu0L8fXobo5yitPA7vOC8gf4Y5010b/AHsLjthMy2sFQwuz2whYcFpPJpnrA4eJ5Jn3xwT/AFHJNdYc4n6xK9Yb4j68mies9kONpdTZWLo5sj9nRG671XwFL41AraqfVPIb98OMJcQUYQZRKlBZJqKZbDUbM8oTLhIs1O7dTL71rgaiG2w2myP0X//EADkRAAECAwUCCgkFAQAAAAAAAAECAwAEEQUSITFBUWEQFCI0coGhsdHwExUyUnGCkcHhMDNAQvGy/9oACAECAQE/AbMsxmdZLjhOdMOrdHqCW95XZ4QxYku4mpUcyNNCRsj1BLe8rs8Il7El3WUOFRxAOnhExYku0ytwKOAJ08I9QS3vK7PCLRspmUY9Kgmvnd+jITnEZEuXa8unZHH781xVpNaZmuUKnkyjd0C8oqVQfMYYtFRe4tMouK74atFuWDLDmRSnHz3xMzl9T8rd9lBNer8xMT7jUwJZtu8aVzpFprdckSXUXTXbWJNN56gFcFb/AOpphAbCxR8UVrQAYXkU3A4nZpXCFSrbbbgAxw6sFZ1SPJTBlEKxoRsyx5JOHXQa50wMcUQEgEEnYKVJog7DtO3L4mHpVurigMr2OgpkKb9Mdd0TKEtPLbRkDTgbaW7ZtG015enRiXZcs2bKUoq2vZp5/O6HWXW1pnWU3rpUCPmV4wA/aM2hxbZQlGOMGUM09LhSTduDHqMNSb8qt/0mIuHHz3RasutybCvRqUmmn+GJjCy7lwpodc9uwQ2guLCBrC2FBVE45duUCTdV7NNNRr1xxZdAdu8duOEcTe5Qp7OeI8YVJOIArnjqMKUzNaDOFoU2q6rgsDmyul9hwSn7Z6Sv+jwSXNmuiO6J3mzvRPdwW3zQ/EQ06plV5GcCedwK+URt+o+n3hMyUilB/mR6q/DaDCJpSFX6V/Ap53wmdUhJQlIoa7dRQ6wZxRzSNa760rX6aUppDjhdVeP8L//EADsQAAEDAgMDCAgDCQAAAAAAAAECAxEEEgATIQUxQRAjMkJRYXGRFCIzdIGSstIVMMFAYnOCobGz0fD/2gAIAQEABj8CbYYbZWhTQXzgM7z392PYUvyq+7CUJZpiC02vVKusgKPHvx7Cl+VX3YqmEM0xQ06pAlKp0PjilYWzTBDrqUGEqnU+OPYUvyq+7Apn22EoKSZbSZ/v+Sinzsi2jzLrbuuf94/E6p5VPeYZZy5LnZx0wpxTqaWkZpmC4+vhzKcHaOzqwV1Kkwv1bVIxteup1XrarHU5EakTOnnuxsXamdOdWIbyrd3rHj/Lhe0amv8ARGkrsPMlf69+G00lX6a3kqJXllEHsg4kuFlGa0FLC7ITmJnXhpgmidL9Mo82464txN+W4TrMkaJ7ePHFEsupCFXIiLUuG9vo2rIO/t4KwkFxt5CQkuGwgsi9I9Yz2FRnTozuwohbVOibS68lRSkXvASJEdBPn4YoWytClKQzzSkkuOhQErCp4a8Or34pnnrcxxAWbBA15L33m2EHZ8XOKtHtMNuO1SKfaNGDKHVAZvbHjH/b8VOxqx4UiX2GHEPndOUjf8o/risYZrWq+rrRZzRlKRHj3nG3FNvNJqxtFaktKIlQuTOnnjYxpyltz8RbUtjrJMmT5nf34W36bS01RnEgVCuGnCRjO9KYq1OsXFVN0RpEbz2YcdUCQgTCd57hi9zm1ALvTvi0wrCszNTCljRlaujvOg3ajXCki9Vsza0s7iBppr8MUxvWE1EZSlNLAVO7WMORmZYShSTlLuXdduTEno8P0wHGzKT8ORj3cfUrkb93Y/xJ5Noe8OfUcbP94b+ociP4asBt4XtXBRQdyvHC0sldIhcyliEjUQeHHTyweddRJM2nqmLk/GPHsIwW8xxsG7oR1lBRG7ujww285UPuOJtkm0XWquEwMGH37haG1SJbCZiNP3iNZwGkSQJMq3kkyT5/sX//xAAnEAEBAAICAgEDAwUAAAAAAAABEQAhMUFRYRBxgZEwQNGxweHw8f/aAAgBAQABPyF60FfSfQ1p8bHVA96/Rmr42O8B7goXz1jvAeYCM89/GyW1uNH1f6PVe9lCkp/xn3zGQbWIdvHBd0weoQU6IavvevxUANC+AbPvvRpHjeWxajVUV2+j+M+h3zru239neaeP3oR1X4ZV6f4jQRvvGF4XJA1oindziShnJNB7wi66av6WPYFRg+3GnMZBG1hNtqTonIrhP6QM9EyYXW43oDBF+lB4pu3KpGJE69jFgK8Wfx8AogRo9K96cks6RsK47ik4dSJiYE1AF4ln4LpRmXmsy1Zs0hs7U1BzRnM+v5USKe/Gb0XSCEl2NPL2phUWEU022vPeIKkuWj6ohee8eI2a+g7XgM5e82dMs2DxoojMVBpnQMP7aG+cFNzvFUJHqu61/SoOhVqioFig9b4TIJ5MQUGqSqEl8s2SdyKEYiOxERHYn6M1fYsf6nxmquDDPYDssZ6OqZ0SAex5wgudlI2vRrUZfCNCqmjehmhot6YAoToSUjc8vMYqMOqyEOHkHLYQdTJ92kcw7uJowjVQh8qX7/sv/9oADAMBAAIAAwAAABDqKReSSSSarQuxL/TR0/wbR5xZZLQcbSySSSSSSST/xAAmEQEBAQACAgECBgMAAAAAAAABESEAMUFRYXGhEIGRsfDxIDBA/9oACAEDAQE/EHTBDo+08J65/R8CW2vT4F+pzn9HwkvoFHwpxJfAYPlDj+j4DgCLg3Pqv+nxTXZfI9nvnZds0Po9kHfHRS0r0NsvH6F+dw/IVsE7kT8v3wyMTeMtqHGoNo+Xep+dzndubHWvN/Z544hGdvT0/PrmjMbWvUf3+eHgGrZBJ0SZa3g2zdlNCMReliL03BQBA5oDKAp5VYMNRZgCuNIyrhdAJRA8Q4DGkBljUradKAdaUZRURKnSooCCDyMsWCGlmr19fwg+MVQ+/hgTuKHTZ8sydOSI8lFOPCmLc8PV0ozgksnkBJscxY+VMg8FSBBJU0zuJevn1xOmi4o1rPIrb5u7Tj4V3bxnij9+TvlV6mSdvgvfnnQSFfOGuc65AonyX6MQqQqGFnJpXuERYJYFhBenG6cqSslidlJBuazo1mXNqiBYKoFk3xfG9bxwVAqFNIISozEo76eBUzfCdMcYiJEfw+x/4Cj+S9ufyXp+HT9XEliin0RPuFPJjRTiWJoMkLEfFwxidkCq0ifhDIEMpbKQJg5QKH1OpJokmScYW7GKQmCB8C5VhF4lhoz0IDrzoNMOEkXtV7VVV6NVYAHQBD/i/8QAJhEBAQEAAwABAwMFAQAAAAAAAREhADFBURBhgaHw8SAwQHGRsf/aAAgBAgEBPxAxJFoBAXq+efynBPdMT0E/4N+/P5ThTFYjFQc4pikVigu8fynBevQak3/Q/s+6qizstsfjnRUrkI7JGph32xkUANjTt93ufbGv5QnSlNp+X/motKJEh4tvBSRPDPWekrzpt2DvGSZ+XnKk/wAPy+SefPMx9yMfNPyT7cbQqAhSJoI7IR3mLOYTsKEEYrU0vVCoruo8SwUMQtB6p1Ib2Q4pDwKVX4DjPBUSg4GiFoYKJohQCyQdNY7gFBgBob0VaRcZcDuWTOt7+iZj3ArPWcXc0iFdsveFjezDR4taesR2A31e5jEHn2VRitpKFqFOgHahxwxkgYNDeqMY/aiPBQsM3EgBfECfAMyPGZAmXvfYfpysimHdXXwVTrziKgoN6L6vgevhzpUcOFm+sfnUES8k6UWn7ho1jnedcHZDEo9i9hoMoXzeJsi2gdAisKQupY52Jw4ikJh2OwWIojDtOKBifnsoiYiaJiaZ9P1T+gD/ALT8OftPy+uKTkIPpcU+8pfLSMSwJ1G1x+QtWXxDSBSG67HtPpqERqOxvC8ETu9oGiN20RAJOAf9DU3irZ61NDFFMwNCMejWaEwWg4/mOEOgAAPsABa5qv8Ahf/EACIQAQEAAgIBBQADAAAAAAAAAAERACExQVEQMGFxkUDR8P/aAAgBAQABPxA58zUxBPp0Wrv0HPWvERIQ6GO4FV24OWNs4mSCMCwC9GDG2cTJLCFKJen0HLJqwNEKSb8ez/UF3Hd3t+m9FHyUDF+pWVx9Buj1e6FQWh0AatRiDYPWGitYhpQIUg3qHrNHXhBsYxWL8v8AMXP+ifnrdAi6FddFsghp5zmMMvaNisHQ49OKvjZl5UAUCC7xTzaxNCVrgGAZ9CKnzm1ookdETlgYhqObE9ANQmEnFmlAjHSW3VmHkdf5OAHJX7BB9LmC5Y9AldosqGCfAoi0Kgwk5g+MV4Le5dhABpWMBU5ep+NAqg6Nkt6cgsE92xiRBtQ0sZJRAmFYKAhGhSzdrhBnGUi2iCfejcqqyIAQFO5MQHHPuElwJX6QmP8At9DCnYU7Ch3lCBuzWoCwFUIbQW9V3O7Br+CpGGFbJ0UWMKTgoiuH2nNmGHsQCwBQk1akKqYM9wT0JjtpQqQHgYEICCJ7IaDWrf7Xhg/SooNcMTxqk1IsYYahmoDhhElGBnem+YW0XcOgOA2MRqtJSoTEEq5KKFUBJ0iKwCCTvG7W6VOoeUkdK+fURnQ5XgFUAh/C/9k=',
            src: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wgARCAA+ASsDAREAAhEBAxEB/8QAHAABAAIDAQEBAAAAAAAAAAAAAAMFBAYHAgEI/8QAGgEBAAMBAQEAAAAAAAAAAAAAAAECAwQGBf/aAAwDAQACEAMQAAAB7l43Kp+bUAADI6J6L7zYAAAAAAAAAAAAACt+fXHwAAAe7rf6lgAAAAAAAAAAAAANL8jlU/OqAABkbT0L3OwAAAAAAAAAAAAAGkeOyqPm1z+2c322wAExtgAAAAAAAAAAAAABW/Prj4JdUv0rAAfTPAAAAAAAAAAAAAANN8nlVfOjO7JzPY6gASm0H53pfDvTaL0wsNqO9N0W5pfO3pb4t5tnjUvtm+VLz79GtTQZjVF75XfTn1LQa59cy04/rnYzHQKX6kADRvG5U/zK2HdOd7fYACU24/MFL7LenR7U4Zlrtl66plrx3fC4pa5W6fplyLPTrGudtlpt8xpedvzdpN8p+rtcs7j66rt49e5eqq1x0qt7SY2oq712KluzIhynzSPsvWspJACUiJQREpDDCkMwnR5iYpj0mUiMKHyWWiaJ+THlMpHDHlAe0ZKZz//EACgQAAAGAAYCAgIDAAAAAAAAAAACAwQFBgEHEhMVFhBAFyYRNhQgJ//aAAgBAQABBQKUm1WLvs647OuOzrjs647OuOzrjs64b2JZZx7q2LPXqjhqjhqjhqjhqjhqjhqjgTFhq92Tg1Xzvq646uuOrrjq646uuOrrjq64b1tZFf3ZScXYu+0OgwsDh085lYcysOZWHMrDmVglLKnV91ZVoU+/HhJZkZTcajcajcajcajcajBRt+fdkoI7511ZUMa8o0d8KccKccKccKccKcJRB01BZZ6xKXD74K8jcjSdun5s1nx75hhWbS7k6REyd3mWH3zAGv0orQkz3tVN7IXeJauro9UcOsxZKLtDrMODbRNevMvMWqGm7pYGv3wRTe2YQsj8gRbCIXv01HHfyUDTWLy9SDOPsFliLG+n3iGYH9JWbXZPOzOxHzzh085hccwuOYXHMLjmFwlKrHVE1WW1pzM+GIIViqM6m2aVVtfrV8MQQr7Xr4iS1LFhiSifgiKhMqU8m4Zdm8onU38+dsvOVEmB7w2y9g2ks8w/1aGLV/4uihim2KNnI65fqeXf6bmsubrpMmITTbsvGdSjrNY20RfvmSBCubKL+UPIPuXBkiGx2ExgkQuO2UbZRtlG2UbZRoL42ibngiRE/Gynq4xmOMZjFBMyYMXA5dhMFSIQw2ibnGtMRxjMIt0m+Bi4HKQhUynSIp4OQqpVGaCw4xmCR7VM2wnuj//EADIRAAEDAwEFBAkFAAAAAAAAAAEAAhIDESEQIjFAQWETUbHwBBQjMDJCUHGRIFJygdH/2gAIAQMBAT8BLrKampqampqanx2FhYWFhYWFhY44tuoFQKgVAqBUCoFQ44usVMoOJP0TCwsfRC26ggy3uKlmhnXR1/lT7Na3vOlEXrdk/vQN9KY2qjXfKgZC6DS7AUvYT53AVVtnAM7kzaynEXZHcU+03NHI6M+Pa3ICSDpKi0udtIOlkKpdrZ9VU2WMI5k+H6S4hTKDiT7is63Zjzz0jmSqu9rDuGjGw9LZbnZC/PSjaVUt3Km6TQ4pzSRsI29V2f3BVsVm/wAf8Ttveqm+j55lVL9q+/fo2zdgKl8X9HwVLcfuUDCm9/RC9sosFRjh0RvUoUndT4aVHRpk81U3+ev55fn7cP04H//EADwRAAAEAgQKBggHAAAAAAAAAAABAgMEEQUSUtEQExYhMUFRYWKhFSMwcaLwICIyQFCBkbEUJDNCU8Hx/9oACAECAQE/AaRpVyEfxaUkf1HT79gud46ffsFzvHT79gud46ffsFzvGUD9gud4ygfsFzvHT79gud4h6ceW8lFQs5ltv9+eODrddVnvkJ0dweETo7g8InR3B4ROjuDwidHcHhE6O4PCJ0dweEJOArFVqT+Xv1IUM7GP41KiIZOP2yGTj9shk4/bIZOP2yGTj9shk4/bIZOP2yDFAPNOpcNZZjI/fqQph+EfxSCKUi23jKKKsp53iCpuIiYhDSklI++/4I65BpV1xpnvkMdR1pHINuwRrImzTPdL4JSFDORj+NSoiGTjv8hCDoNyGfS8ayzdg3NdY9mBMs9YIms17CB5g+cmycRrIGmrgcPM2pP7goqpyBqJGcxV/MG3qlMNKJSJr2hc0nLSJSbcUelIkZJIz14F+wVXTPkFKJJTMKTV0h9RJL1NwUmqcjDclKNGuQa9c3J6vRpGl4iEfxSCKWYZQxexPO8QVNRMREIaWRSPzt7BhMycPYeA1TSSQynqcZtM8ClGuEOeo/P2By1YHZ9SStP+hxNVRpIIWksywmt+KVWs3hjPDn33hHVlIgn9KI86iBSqlLAuausPWHvZ+Zfcg7pLuIVcY8hAOU8wS4bbqD3hEm3Xk+dWBtNdyR6A3/d0+7X9O/AbSFaSGKaskCbQWckifabvS39pv9H/xABBEAABAwICBgMNBwIHAAAAAAABAAIDBBEFEhMhMTIzkRBBoRQiIzRAUXGBk6KywdIGNUJSYWLRFSAkc3SChbHh/9oACAEBAAY/AjE1jHCw2rhR9q4UfauFH2rhR9q4UfauFH2rhR9qjjMcdnOA6/LvDaHP++11tpvdW2m91bab3VtpvdW2m91bab3VtpvdQymnzdVsvlxlbI1osBYrjRrjRrjRrjRrjRrjRrjRqN5lYQ1wPlxiYyMtsN4Lhxcj/KjicyMNceoFbrFusW6xbrFusTGlrLE28utM6EP/AH2ut+m5tQEb4C/qykXW9F2Lei7FvRdi3ouxb0XYhZ0V/V5cZWyNaLAWK4zOSjlMrSGnZZcRq4jVxGriNXEamOzjUb9EuF4RWQUzGQtf4ZrbcyF98Ybzj+lQvxDEKOegB8IIg255NUWE4JUwUxbBpZHTBtu1fe+Hc4/pVZXTOaa+lbKHHL+JouNSjq4cWoWxybBJowfhX3vh3OP6VUV4McWIQVPcxka24P62TXjF8Os4X16P6VJWTYnh80UIzujGTvh5ti+yTocsUWJHw8eW/mHzWJxTUndWFUzw1xib30Q86bXisbK1+5EziE+ay7nqKYUVG+mdLHA5vfW6nXTqqmxSijizluWUMafhX3xhvOP6ViArKylfiDh/hXtaMrfTqVRVzYhSaKBhkdlay9h/tUNbT4hS6GUXbnYwH4VUVeJyRTYjBE5xcwd6T+H5KKpZitAxkrcwa/Rg/CsMpsZraWppax5j8CG6j1bAqDCmub3HLSmRzcuvNc9fq/tMTAwtsNoW7FyP8qKJzY8rj1BbGclsZyWxnJbGclsZyTGkMsTbZ0VVLVmVsQpmvvEbH/pcau9o36VLBRume2R2c6Z11j9TWunZDBKIozEQPl+i41d7Rv0r7Y4H3xjZTulizbSMp/8AFH/UX4mKz8egy5PUuJjPuKpDmObC6uvFmFiW6taY4VFYyV7L3L2kA281kJMXpJ8Qwu48PSPtYfuC+xD6JuWkLjoxa1h3q+1TXNu0uaCCnYgyjGlOsMJuxp84C/44/NH+ruxEVec+LZctvWuJjPuLR4aZtHSgRkTjvv0WL/6WT4Vhn+X81FRx3L6ydkVghmmrc3X4Rv0qPFcOkqXy08zHO0rgQBf0LCMUqA/uc0APeC51ly3av2Q/lYZS4VE52mnbHN3Qz8JPVYqR/dBGXU2O53/yZdnrt6+jW1p9IXDbyVwxoPoW6OS3RyW6OS3RyW6OS3Ry6NJkbn2Zra+k5Ghtzc2G3oc7I3M4WJttC8Ug9mF4pB7MLRmNpj/KRq6C1wDmnaCmeDb3m7q3fQnOaxoc7aQNvRpMjc9rZra14rD7MLxSD2YREUTIwfyNsi1wuDtBQaxoa0bAEM7Q6xuLjZ0Fr2hzT1FDSQRvtqGZoNl4pB7MIObTRNcNhDAtLo26T89tfR//xAAoEAEAAgEDAwQCAwEBAAAAAAABABEhMUHxEFHRYXGR8ECBILHB4aH/2gAIAQEAAT8hAGUu95PecP5Th/KcP5Th/KcP5Th/KfX8ocktTLLXf84ztjz505GORjkY5GORjkY5GN12ZbXtX5zprkN4J9hn2GfYZ9hn2GfYZ9hinJoXmm/ziJJLS8nv0EVLpLc095wb5nBvmcG+Zwb5nBvmHkDVD39/zm9GZF/Z04QQdHxVTnY52OdjnY52GlmcU2v85uqoDtOSSwfpCzOEZwjOEZwjOEYhXDpT36VHfJFZnIznpCwKF5wYBDet5nyEYK6KWsVp3iY6cIJ+8AuIjHaGeZQjzWSGpdOEsdR+jwVm/tCkgG1szlz2WhrQL8Iww8jBbNQuTUxlvvL6u9+v/kwQaycO98Sy7mocJsNRZzn09N5T2XDE9FYzKkIgzbRM4Eb2wxZibVQtonY1XA9Sa04cM2lFf0hKwVpHuaJUbYtw6kUbSADMcW1O7b/ENca2XJ79Kg0lJUvT3nJPM5J5nJPM5J5nJPMrYGqXf36FX3DsANVdK1hs6LuqxQEvjjzUERtaA+elZwTrDaNdhuPwj3VGvfYyL0llVu2PGHQCvdM/dwywQgH7Je1zMvatlZKL09T0ey3lV/YimCaODYjeJdL39GYv0qLXDUitt6fT6LvWcD4zS9LUFZLo3T8Q3XnzIK+7VBgAXczdfNTZ6GFVwf4a3MMHeo276Ey4U79HJ/zGFAHrc7yyR21tsz2HBqZ9EJYHe4M4XAgBuD+Tve94Jxj6AELJarDtfXsN5i3d6KNbA++wvafY/wDJ9j/yHzbQr8JpDghQLEl78H/8JoFGwfc79F7OxbDtfaLlXXf/AIz7H/ky4EQL/ECAagWJMLRB0H6nYb3Nu501yjHY/qPuj6GOxc+x/wCRC2WOR+IuUpKKcffp/9oADAMBAAIAAwAAABCkkliySSSSSSSSSSSSSQ7bbYSSSSSSSSSSSSSSR2223ySSSSSSSSSSSSSRnySQSSSSSSSSSSSSSSQ6gACSSSSSSSSSSSSSSRhAAAQhIicNBfYZJXWSRCgACTj0aNFS6FOadIAGfrSSSSSeQFsQeNuTycT/xAAqEQEAAgAEBAUFAQEAAAAAAAABABEhMVGBEEFhoXGRscHwMEBQ0fEg4f/aAAgBAwEBPxB1UtpLaS2ktpLaS2ktpBKFffOqbZtm2bZtm2bZ01986snWnWnWnWnWnWnWghG/vnoOEYH8ItsZfTBvhX4R1dy+sRDf0BWGJey+xwtq2Nl+EpiZrsYez2qBbUd8SibNfy4d65NeUC8IuZosd6/faGJycZYpTT2Lir8vpKWmJjBopr180PDdlCpoM75fPmEVUxM9TE9Rqu8Qy1D39HhSzye+NPpf9t1RovkX7QgpyU8ogMuLsF12a2hGqhlKHAA74ds76Q8zCfAD/lyjhGB+ggPmPquABjNo8r/cYByC98vPHgTch7Efdmf19uUM8YtGsL29q3uVFzBdyXxWd/nmRvQ92NWaIxh/rq/K3lQDWHTFq8Ou98PDXpMz4Yp8BqwVuXu/5Ljz1j4xT5/w+s6AI+Q9XgBOTLT45ShQaDN05NwKsbyVdC0LlDKJR9Bxq+XHNt4W2PM4mBRArAgpiSitEcW3hnV8ot4vCi7go2QAymZXLhdZQwKMuCDnHGr5cP/EACoRAQABAwIEBgIDAQAAAAAAAAERACExQWEQkbHwUXGBodHxIMEwQFDh/9oACAECAQE/EFpwBluubJ+ckkkggmtSPsQjF10P7zLuexpm9b9W/Vv1b9W/Vv1b9T5NwiL50iLzOP7yAYgQzNiNCuzfiuzfiuzfiuzfiuzfiuzfiuzfiksDWNEfD+82XCXJXJ0HSvpqSLVhglh8V0/xIyLeltm9fYVbH0yn6Rf/ABEARAhHQivrGk9BzAPh/BGZsvg4Q2NLedTqcJ0+Y9KUF8KK3Qn3hzzvTQnUnnSwTQNnDMc+/wB0qvIxQgMkhzYPdoI/BjzhTlEc9oxClE+tjkL45zUESTiNfju1CeyQ7DAxyT/lDEmE9T9cMCzNyW88943EBzQPdpEGoHnejLasPVQX3mkmrlTyZE9L9D3KtWmR7dZ9jefwdqgVxW5slfRUG0rDAzh3fwApnqRwVDBPv9UcVmJ5GepHrwZM431TpDnWPb760zFqMNJZ81X1tUzcKcmkIYdPB6d6JRE28un/ADQBNTU9HxNh7d4tV/p0SvnF/O+PSKLUC0E8zTknPMzWGm7h4FSD1Z/X7q806VoIy6nOpJ2Dt5cHDSvnITy8aisLMgNzN5JwiMpiVISa7g19EfFJgh8irKGT8i0xrxwQY4QQ6HPFuy5rNIOalnxUWIOBaY1oAIOEsRSDZpVu1hnXgg5rL4uApii0xrw//8QAKBABAQACAQMDBAIDAQAAAAAAAREAITEQQVFh0fEgQHGBkaEwscHw/9oACAEBAAE/EB/zTs0eAfXp06dOnT8biMv3YApPLf3xMOELTNXlP8BhhhhhhizSvNH2N2yT74RMwNkOxnxHtz4j258R7c+I9ufEe3PiPbnxHtwroNWBQa9PvgWDUbo8E/roOQSA4JTqs7ePq379+/enmErBAz9vvj4MIeJq8p0eSFEYJn5P4+o88889mFANytSd798WmYkkTt07SAgkdE1fz9RRRRRSkdYsgGf10Lp5ZALX2hAhDJgBHXRkuhRpcM2nvGFmiwgAq7dRAuBYUcLMeIxgDYtAByOqh7Jbc2ecaSYrMEFq/Kt3Kkuu5A1BrVogASnB3lrBpWRBdTU8AjMWtUgGisIgjdFUtUI/MeQAsKrxoA8ElDLUASPuggKbaVLtZolZS4E1ATuOW6GNLEOCKq9EDtkVybLlQbQStNY4MA3VoGsGFMBJmdhJocaPfDlZr5kAXaGFbyR7sXqFtTtXCzCKmAloKCoi+MQu6nEgiIQBm2jqfQeezXKOwf66DFzdNwU6V+PH1SJEiRIPaTKIgZ+3RZbUMWos29ujE+pxlJJHEPFbt4jyyNzxLewBx6MUzHoFLwBeBDlrEleBk1yXY5752RLtG5HX31WCBuk30hoMMh3Q81iguwzVOcUiMiQ3NkRuae7gzYq0GjYIG74WvOaXSBWFDpEuu+NmCVVVX6T1oTTa2pxFDXJybyJkaO88j9YE4u/RWPG0WLgbVQErFCuExYSJu7wGCWmzxPVGNj/G3RueNwzRI0lCNoe954xiTkPhA4CLvp7Zl+PvLpNzQmuMKuobS1k77HLQyKQpHuYilMtP5c/8d/zHBxoZP3MN3Z9GfAvbPgXtnwL2z4F7ZXgdhAI/x0Ggllvmiz0vVtrs115gbdG3fSrCg/BJX0OuqBAVGQFIcAya/GAAAANAYjgRZIiI6R8YqV3ke0nZ2a1MewozT4gWPXoDSMFzd4LsurMehFUyvQgcEoImcKAuOJ8WSIiOkTthWLgz+AaDG2uSTXihpOyb6DR2Fn8K042Jq/hAMPQ6IB12I+4RKHKYa1h4E2bdXp//2Q==',
            w: 200,
            h: 40,
            wf: 130
        };

        var fontSizes = {
            HeadTitleFontSize: 18,
            Head2TitleFontSize: 16,
            TitleFontSize: 14,
            SubTitleFontSize: 12,
            NormalFontSize: 10,
            SmallFontSize: 8
        };

        var lineSpacing = {
            NormalSpacing: 12,
            DobleSpacing: 24,
            LineSpacing: 10,
            LetterSpace: 6
        };


        var doc = new jsPDF('p', 'pt', 'letter');

        var rightStartCol1 = 40;
        var rightStartCol2 = 530;

        var border = 3;
        var tabla = 0;


        var InitialstartX = 45;
        var startX = 40;
        var startbX = 43;
        var InitialstartY = 20;
        var startY = 20;
        var lines = 0;
        var space = 0;

        doc.addImage(company_logo.src, 'PNG', startX, startY, company_logo.w, company_logo.h);
        doc.addImage(company_logo.srcf, 'PNG', startX + 400, startY, company_logo.wf, company_logo.h);
        startY += lineSpacing.NormalSpacing + company_logo.h;

        startY += lineSpacing.NormalSpacing;

        doc.setFontSize(fontSizes.Head2TitleFontSize);
        doc.setFontType('bold');
        doc.myText("MEMO DE VENTA", {align: "center"}, 0, startY);

        startY += lineSpacing.NormalSpacing;

        doc.setDrawColor(0);
        doc.setFillColor(255, 255, 255);
        doc.roundedRect(startX, startY, 100, 20, 0, 0, 'FD');

        doc.setFontSize(fontSizes.TitleFontSize);
        doc.setFontType('bold');
        doc.text(InitialstartX, startY + 20 - border, 'N° ' + data.Datos.codigo);

        doc.setDrawColor(0);
        doc.setFillColor(170, 170, 170);
        doc.roundedRect(startX + 100, startY, 320, 20, 0, 0, 'FD');

        doc.setFontSize(fontSizes.TitleFontSize);
        doc.setFontType('bold');
        doc.setTextColor(255, 255, 255);
        doc.myText(data.Datos.proyecto, {align: "center"}, 0, startY + 20 - border);

        doc.setDrawColor(0);
        doc.setFillColor(255, 255, 255);
        doc.roundedRect(startX + 420, startY, 100, 20, 0, 0, 'FD');

        doc.setFontSize(fontSizes.TitleFontSize);
        doc.setFontType('bold');
        doc.setTextColor(0)
        doc.text(InitialstartX + 420, startY + 20 - border, data.Datos.fecha);

        //INFORMACION DE LA VENTA

        doc.setDrawColor(0);
        doc.setFillColor(230, 230, 230);
        doc.roundedRect(startX, startY += 20, 520, 20, 0, 0, 'FD');

        doc.setFontSize(fontSizes.SubTitleFontSize);
        doc.setFontType('bold');
        doc.setTextColor(0);
        doc.myText('INFORMACION DE LA VENTA', {align: "center"}, 0, startY + 20 - border);

        doc.setDrawColor(0);
        doc.setFillColor(255, 255, 255);
        doc.roundedRect(startX, startY += 20, 173, (lineSpacing.NormalSpacing * 4), 0, 0, 'FD');

        doc.setDrawColor(0);
        doc.setFillColor(255, 255, 255);
        doc.roundedRect(startX + 173, startY, 173, (lineSpacing.NormalSpacing * 4), 0, 0, 'FD');

        doc.setDrawColor(0);
        doc.setFillColor(255, 255, 255);
        doc.roundedRect(startX + 346, startY, 174, (lineSpacing.NormalSpacing * 4), 0, 0, 'FD');

        doc.setFontSize(fontSizes.SmallFontSize);
        doc.setFontType('bold');
        doc.text(startX + 86, startY += lineSpacing.NormalSpacing, "TIPO DE VENTA", 'center');
        doc.text(startX + 259, startY, "IMPORTACION", 'center');
        doc.text(startX + 432, startY, "CENTRO DE COSTO", 'center');
        doc.setFontType('normal');
        doc.text(startX + 86, startY += lineSpacing.NormalSpacing, data.Datos.descripcion, 'center');
        doc.text(startX + 259, startY, data.Datos.proveedor, 'center');
        doc.text(startX + 432, startY, data.Datos.ccnom, 'center');
        doc.text(startX + 86, startY += lineSpacing.NormalSpacing, '', 'center');
        doc.text(startX + 259, startY, data.Datos.codigoimp, 'center');
        doc.text(startX + 432, startY, data.Datos.cc, 'center');

        //INFORMACION DEL PROYECTO
        doc.setDrawColor(0);
        doc.setFillColor(230, 230, 230);
        doc.roundedRect(startX, startY += lineSpacing.NormalSpacing, 520, 20, 0, 0, 'FD');

        doc.setFontSize(fontSizes.SubTitleFontSize);
        doc.setFontType('bold');
        doc.setTextColor(0);
        doc.myText('INFORMACION DEL PROYECTO', {align: "center"}, 0, startY + 20 - border);

        doc.setDrawColor(0);
        doc.setFillColor(255, 255, 255);
        doc.roundedRect(startX, startY += 20, 520, (lineSpacing.NormalSpacing * 3), 0, 0, 'FD');

        doc.setFontSize(fontSizes.SmallFontSize);
        doc.setFontType('bold');
        doc.text(startbX, startY += lineSpacing.NormalSpacing, "NOMBRE: ");
        doc.setFontType('normal');
        doc.text(startbX + 40, startY, data.Datos.proyecto);

        doc.setFontType('bold');
        doc.text(startbX, startY += lineSpacing.NormalSpacing, "DIRECCION: ");
        doc.setFontType('normal');
        doc.text(startbX + 50, startY, data.Datos.direcpro);

        //CONTACTO DE VENTAS Y CONTACTO OBRA

        doc.setDrawColor(0);
        doc.setFillColor(230, 230, 230);
        doc.roundedRect(startX, startY += lineSpacing.NormalSpacing, 520, 10, 0, 0, 'FD');

        doc.setFontType('bold');
        doc.setFontSize(fontSizes.SmallFontSize);
        doc.text(startX + 130, startY + 10 - 1, 'CONTACTO DE VENTAS', 'center');
        doc.text(startX + 260 + 130, startY + 10 - 1, 'CONTACTO DE OBRA', 'center');

        doc.setDrawColor(0);
        doc.setFillColor(255, 255, 255);
        doc.roundedRect(startX, startY += 10, 260, (lineSpacing.NormalSpacing * 5), 0, 0, 'FD');

        doc.setDrawColor(0);
        doc.setFillColor(255, 255, 255);
        doc.roundedRect(startX + 260, startY, 260, (lineSpacing.NormalSpacing * 5), 0, 0, 'FD');

        doc.setFontSize(fontSizes.SmallFontSize);
        doc.setFontType('bold');
        doc.text(startbX, startY += lineSpacing.NormalSpacing, "CONTACTO: ");
        doc.text(startbX + 260, startY, "CONTACTO: ");
        doc.setFontType('normal');

        doc.text(startbX + 50, startY, data.Datos.nom_ven);
        doc.text(startbX + 50 + 260, startY, data.Datos.nom_obra);

        doc.setFontType('bold');
        doc.text(startbX, startY += lineSpacing.NormalSpacing, "RUT: ");
        doc.text(startbX + 260, startY, "RUT: ");
        doc.setFontType('normal');

        doc.text(startbX + 20, startY, data.Datos.rut_ven);
        doc.text(startbX + 20 + 260, startY, data.Datos.rut_obra);


        doc.setFontType('bold');
        doc.text(startbX, startY += lineSpacing.NormalSpacing, "EMAIL: ");
        doc.text(startbX + 260, startY, "EMAIL: ");
        doc.setFontType('normal');

        doc.text(startbX + 30, startY, data.Datos.email_ven);
        doc.text(startbX + 30 + 260, startY, data.Datos.email_obra);

        doc.setFontType('bold');
        doc.text(startbX, startY += lineSpacing.NormalSpacing, "TELEFONO: ");
        doc.text(startbX + 260, startY, "TELEFONO: ");
        doc.setFontType('normal');

        doc.text(startbX + 50, startY, data.Datos.tele_ven);
        doc.text(startbX + 50 + 260, startY, data.Datos.tele_obra);


        //INFORMACION DEL MANDANTE
        doc.setDrawColor(0);
        doc.setFillColor(230, 230, 230);
        doc.roundedRect(startX, startY += lineSpacing.NormalSpacing, 520, 20, 0, 0, 'FD');

        doc.setFontSize(fontSizes.SubTitleFontSize);
        doc.setFontType('bold');
        doc.setTextColor(0);
        doc.myText('INFORMACION DEL MANDANTE', {align: "center"}, 0, startY + 20 - border);

        doc.setDrawColor(0);
        doc.setFillColor(255, 255, 255);
        doc.roundedRect(startX, startY += 20, 520, (lineSpacing.NormalSpacing * 4), 0, 0, 'FD');

        doc.setFontSize(fontSizes.SmallFontSize);
        doc.setFontType('bold');
        doc.text(startbX, startY += lineSpacing.NormalSpacing, "RAZON SOCIAL: ");
        doc.setFontType('normal');
        doc.text(startbX + 65, startY, data.Datos.razon_social);

        doc.setFontType('bold');
        doc.text(startbX, startY += lineSpacing.NormalSpacing, "RUT: ");
        doc.setFontType('normal');
        doc.text(startbX + 20, startY, data.Datos.rut);

        doc.setFontType('bold');
        doc.text(startbX, startY += lineSpacing.NormalSpacing, "DIRECCION: ");
        doc.setFontType('normal');
        doc.text(startbX + 50, startY, data.Datos.direcman);

        //INFORMACION DEL CONTRACTUAL
        doc.setDrawColor(0);
        doc.setFillColor(230, 230, 230);
        doc.roundedRect(startX, startY += lineSpacing.NormalSpacing, 520, 20, 0, 0, 'FD');

        doc.setFontSize(fontSizes.SubTitleFontSize);
        doc.setFontType('bold');
        doc.setTextColor(0);
        doc.myText('INFORMACION CONTRACTUAL INSTALACION', {align: "center"}, 0, startY + 20 - border);

        doc.setDrawColor(0);
        doc.setFillColor(255, 255, 255);
        doc.roundedRect(startX, startY += 20, 220, (lineSpacing.NormalSpacing * (parseInt(data.Etapas) + 2)), 0, 0, 'FD');

        var infoStarty = startY;
        doc.setFontSize(fontSizes.SmallFontSize);
        switch (data.Info.length % data.Etapas) {
            case 2:
                doc.setDrawColor(0);
                doc.setFillColor(255, 255, 255);
                doc.roundedRect(startX + 220, infoStarty, 150, (lineSpacing.NormalSpacing * (parseInt(data.Etapas) + 1)), 0, 0, 'FD');
                doc.setDrawColor(0);
                doc.setFillColor(255, 255, 255);
                doc.roundedRect(startX + 220 + 150, infoStarty, 150, (lineSpacing.NormalSpacing * (parseInt(data.Etapas) + 1)), 0, 0, 'FD');

                break;

            case 3:
                doc.setDrawColor(0);
                doc.setFillColor(255, 255, 255);
                doc.roundedRect(startX + 220, infoStarty, 100, (lineSpacing.NormalSpacing * (parseInt(data.Etapas) + 1)), 0, 0, 'FD');
                doc.setDrawColor(0);
                doc.setFillColor(255, 255, 255);
                doc.roundedRect(startX + 220 + 100, infoStarty, 100, (lineSpacing.NormalSpacing * (parseInt(data.Etapas) + 1)), 0, 0, 'FD');
                doc.setDrawColor(0);
                doc.setFillColor(255, 255, 255);
                doc.roundedRect(startX + 220 + 100 + 100, infoStarty, 100, (lineSpacing.NormalSpacing * (parseInt(data.Etapas) + 1)), 0, 0, 'FD');
                break;

            default:
                doc.setDrawColor(0);
                doc.setFillColor(255, 255, 255);
                doc.roundedRect(startX + 220, infoStarty, 300, (lineSpacing.NormalSpacing * (parseInt(data.Etapas) + 2)), 0, 0, 'FD');
                for (var i = 0; i < data.Info.length; i++) {
                    doc.setFontType('bold');
                    doc.text(startX + 110, infoStarty += lineSpacing.NormalSpacing, data.Info[i][0], 'center');
                    doc.setFontType('normal');
                    doc.text(startX + 220 + 150, infoStarty, data.Info[i][1], 'center');
                }
                break;
        }

        //INFORMACION DEL CONTRACTUAL
        doc.setDrawColor(0);
        doc.setFillColor(230, 230, 230);
        doc.roundedRect(startX, startY += (lineSpacing.NormalSpacing * (parseInt(data.Etapas) + 2)), 520, 20, 0, 0, 'FD');

        doc.setFontSize(fontSizes.SubTitleFontSize);
        doc.setFontType('bold');
        doc.setTextColor(0);
        doc.myText('FORMA DE PAGO CONTRACTUAL', {align: "center"}, 0, startY + 20 - border);

        doc.setDrawColor(0);
        doc.setFillColor(230, 230, 230);
        doc.roundedRect(startX, startY += 20, 520, 10, 0, 0, 'FD');

        doc.setFontType('bold');
        doc.setFontSize(fontSizes.SmallFontSize);
        doc.text(startX + 130, startY + 10 - 1, 'PARTE IMPORTADA', 'center');
        doc.text(startX + 260 + 130, startY + 10 - 1, 'PARTE NACIONAL', 'center');

        if (parseInt(data.FSi.length) > parseInt(data.FSn.length)) {
            doc.setDrawColor(0);
            doc.setFillColor(255, 255, 255);
            doc.roundedRect(startX, startY += 10, 260, (lineSpacing.NormalSpacing * (parseInt(data.FSi.length) + 1)), 0, 0, 'FD');
            doc.setDrawColor(0);
            doc.setFillColor(255, 255, 255);
            doc.roundedRect(startX + 260, startY, 260, (lineSpacing.NormalSpacing * (parseInt(data.FSi.length) + 1)), 0, 0, 'FD');
            infoStarty = startY;
            for (var i = 0; i < data.FSi.length; i++) {
                doc.setFontType('normal');
                doc.text(startX + 130, infoStarty += lineSpacing.NormalSpacing, data.FSi[i][0] + ' (' + parseInt(data.FSi[i][1] * 100) + '%)', 'center');
            }
            for (var i = 0; i < data.FSn.length; i++) {
                doc.setFontType('normal');
                doc.text(startX + 260 + 130, startY += lineSpacing.NormalSpacing, data.FSn[i][0] + ' (' + parseInt(data.FSn[i][1] * 100) + '%)', 'center');
            }
            startY += lineSpacing.NormalSpacing;
        } else {
            doc.setDrawColor(0);
            doc.setFillColor(255, 255, 255);
            doc.roundedRect(startX, startY += 10, 260, (lineSpacing.NormalSpacing * (parseInt(data.FSn.length) + 1)), 0, 0, 'FD');
            doc.setDrawColor(0);
            doc.setFillColor(255, 255, 255);
            doc.roundedRect(startX + 260, startY, 260, (lineSpacing.NormalSpacing * (parseInt(data.FSn.length) + 1)), 0, 0, 'FD');
            infoStarty = startY;
            for (var i = 0; i < data.FSi.length; i++) {
                doc.setFontType('normal');
                doc.text(startX + 130, infoStarty += lineSpacing.NormalSpacing, data.FSi[i][0] + ' (' + parseInt(data.FSi[i][1] * 100) + '%)', 'center');
            }
            for (var i = 0; i < data.FSn.length; i++) {
                doc.setFontType('normal');
                doc.text(startX + 260 + 130, startY += lineSpacing.NormalSpacing, data.FSn[i][0] + ' (' + parseInt(data.FSn[i][1] * 100) + '%) ', 'center');
            }
            startY += lineSpacing.NormalSpacing;
        }

        //OTRO ACUERDOS CONTRACTUALES
        doc.setDrawColor(0);
        doc.setFillColor(255, 255, 255);
        doc.roundedRect(startX, startY, 130, 10, 0, 0, 'FD');
        doc.roundedRect(startX + 130, startY, 130, 10, 0, 0, 'FD');
        doc.roundedRect(startX + 260, startY, 130, 10, 0, 0, 'FD');
        doc.roundedRect(startX + 390, startY, 130, 10, 0, 0, 'FD');

        doc.setFontType('bold');
        doc.setFontSize(fontSizes.SmallFontSize);
        doc.text(startbX, startY + 10 - 1, 'CONTRATO:');
        doc.text(startbX + 130, startY + 10 - 1, 'RETENCIONES:');
        doc.text(startbX + 260, startY + 10 - 1, 'MULTAS:');
        doc.text(startbX + 390, startY + 10 - 1, 'ORDEN DE COMPRA:');
        doc.setFontType('normal');
        doc.text(startbX + 50, startY + 10 - 1, data.Datos.contrato);
        doc.text(startbX + 130 + 65, startY + 10 - 1, data.Datos.retenciones);
        doc.text(startbX + 260 + 40, startY + 10 - 1, data.Datos.multas);
        doc.text(startbX + 390 + 85, startY + 10 - 1, data.Datos.oc);

        doc.setDrawColor(0);
        doc.setFillColor(255, 255, 255);
        doc.roundedRect(startX, startY += 10, 260, 10, 0, 0, 'FD');
        doc.roundedRect(startX + 260, startY, 260, 10, 0, 0, 'FD');
        doc.setFontType('bold');
        startY += 10;
        doc.text(startbX, startY - 1, 'GARANTIA MAYOR A 12 MESES:');
        doc.text(startbX + 260, startY - 1, 'MANTENCION SIN COSTO POST ENTREGA:');
        doc.setFontType('normal');

        doc.text(startbX + 130, startY - 1, data.Datos.garan_ex);
        doc.text(startbX + 260 + 170, startY - 1, data.Datos.man_post);

        if (data.Boletas.length > 0) {

            doc.setDrawColor(0);
            doc.setFillColor(230, 230, 230);
            doc.roundedRect(startX, startY, 520, 10, 0, 0, 'FD');

            doc.setFontType('bold');
            doc.setFontSize(fontSizes.SmallFontSize);
            doc.text(startX + 260, startY + 10 - 1, 'GARANTIAS', 'center');

            startY += 10;
            console.log(data.Boletas.length);
            var salto = 0;

            for (var i = 0; i < data.Boletas.length; i++) {

                if (i % 3 == 0) {
                    //startY = +data.Equipos[i].length * lineSpacing.NormalSpacing;

                    if (i == 0) {
                        var startyb = startY;
                        console.log('inicio ' + startY);
                    } else if (salto == 1) {
                        var startyb = startY;
                        console.log('Salto de pagina ' + startY);
                    } else {
                        startY += (lineSpacing.NormalSpacing * 6);
                        console.log('salto de linea ' + startY);
                        var startyb = startY;
                    }

                    var startxb = 160;
                    var medio = 67;
                    doc.setDrawColor(0);
                    doc.setFillColor(230, 230, 230);
                    doc.roundedRect(startX, startyb, 120, lineSpacing.NormalSpacing * 6, 0, 0, 'FD');

                    doc.setFontType('bold');
                    doc.text(startX + 60, startyb += lineSpacing.NormalSpacing, 'EMISOR', 'center');
                    doc.text(startX + 60, startyb += lineSpacing.NormalSpacing, 'TIPO DE GARANTIA', 'center');
                    doc.text(startX + 60, startyb += lineSpacing.NormalSpacing, 'TIPO DOCUMENTO', 'center');
                    doc.text(startX + 60, startyb += lineSpacing.NormalSpacing, 'DURACION', 'center');
                    doc.text(startX + 60, startyb += lineSpacing.NormalSpacing, 'VENCIMIENTO', 'center');
                }

                infoStarty = startY;
                doc.setDrawColor(0);
                doc.setFillColor(255, 255, 255);
                doc.roundedRect(startxb, infoStarty, 133, (lineSpacing.NormalSpacing * (data.Boletas[i].length + 1)), 0, 0, 'FD');
                for (var o = 0; o < data.Boletas[i].length; o++) {
                    doc.setFontType('normal');
                    if (data.Boletas[i][o] == null) {
                        doc.text(startxb + medio, infoStarty += lineSpacing.NormalSpacing, 'SIN INFORMACION', 'center');
                    } else {
                        doc.text(startxb + medio, infoStarty += lineSpacing.NormalSpacing, data.Boletas[i][o], 'center');
                    }

                }
                startxb += 133;

            }

        }
        
        doc.setLineWidth(0.5);
        var firmay = 750;
        doc.line(startX + 100, firmay, startX + 250, firmay);
        doc.line(startX + 300, firmay, startX + 450, firmay);
        doc.text(startX + 175, firmay += lineSpacing.NormalSpacing, 'VENDEDOR', 'center');
        doc.text(startX + 375, firmay, 'GERENCIA', 'center');
        doc.text(startX + 175, firmay += lineSpacing.NormalSpacing, data.Datos.vendedor, 'center');
        doc.text(startX + 375, firmay, 'MAURICIO GIORDANO', 'center');


        //INICIO SEGUNDA PAGINA

        if (data.Equipos.length > 0) {

            doc.addPage();
            startY = 20;

            doc.addImage(company_logo.src, 'PNG', startX, startY, company_logo.w, company_logo.h);
            doc.addImage(company_logo.srcf, 'PNG', startX + 400, startY, company_logo.wf, company_logo.h);
            startY += lineSpacing.NormalSpacing + company_logo.h;

            startY += lineSpacing.NormalSpacing;

            doc.setFontSize(fontSizes.Head2TitleFontSize);
            doc.setFontType('bold');
            doc.myText("MEMO DE VENTA", {align: "center"}, 0, startY);

            startY += lineSpacing.NormalSpacing;

            doc.setDrawColor(0);
            doc.setFillColor(255, 255, 255);
            doc.roundedRect(startX, startY, 100, 20, 0, 0, 'FD');

            doc.setFontSize(fontSizes.TitleFontSize);
            doc.setFontType('bold');
            doc.text(InitialstartX, startY + 20 - border, 'N° ' + data.Datos.codigo);

            doc.setDrawColor(0);
            doc.setFillColor(170, 170, 170);
            doc.roundedRect(startX + 100, startY, 320, 20, 0, 0, 'FD');

            doc.setFontSize(fontSizes.TitleFontSize);
            doc.setFontType('bold');
            doc.setTextColor(255, 255, 255);
            doc.myText(data.Datos.proyecto, {align: "center"}, 0, startY + 20 - border);

            doc.setDrawColor(0);
            doc.setFillColor(255, 255, 255);
            doc.roundedRect(startX + 420, startY, 100, 20, 0, 0, 'FD');

            doc.setFontSize(fontSizes.TitleFontSize);
            doc.setFontType('bold');
            doc.setTextColor(0)
            doc.text(InitialstartX + 420, startY + 20 - border, data.Datos.fecha);

            //INFORMACION DE EQUIPOS

            doc.setDrawColor(0);
            doc.setFillColor(230, 230, 230);
            doc.roundedRect(startX, startY += 20, 520, 20, 0, 0, 'FD');

            doc.setFontSize(fontSizes.SubTitleFontSize);
            doc.setFontType('bold');
            doc.setTextColor(0);
            doc.myText('EQUIPOS', {align: "center"}, 0, startY + 20 - border);

            startY += 20;
            doc.setFontSize(fontSizes.SmallFontSize);

            console.log(data.Equipos.length);
            var salto = 0;

            for (var i = 0; i < data.Equipos.length; i++) {

                if (i % 2 == 0) {
                    //startY = +data.Equipos[i].length * lineSpacing.NormalSpacing;

                    if (i == 0) {
                        var startyb = startY;
                        console.log('inicio ' + startY);
                    } else if (salto == 1) {
                        var startyb = startY;
                        console.log('Salto de pagina ' + startY);
                    } else {
                        startY += (lineSpacing.NormalSpacing * 8);
                        console.log('salto de linea ' + startY);
                        var startyb = startY;
                    }

                    var startxb = 160;
                    var medio = 100;
                    doc.setDrawColor(0);
                    doc.setFillColor(230, 230, 230);
                    doc.roundedRect(startX, startyb, 120, lineSpacing.NormalSpacing * 8, 0, 0, 'FD');

                    doc.setFontType('bold');
                    doc.text(startX + 60, startyb += lineSpacing.NormalSpacing, 'TIPO DE EQUIPO', 'center');
                    doc.text(startX + 60, startyb += lineSpacing.NormalSpacing, 'MODELO/MARCA', 'center');
                    doc.text(startX + 60, startyb += lineSpacing.NormalSpacing, 'COMANDO', 'center');
                    doc.text(startX + 60, startyb += lineSpacing.NormalSpacing, 'CODIGO FM', 'center');
                    doc.text(startX + 60, startyb += lineSpacing.NormalSpacing, 'KEN', 'center');
                    doc.text(startX + 60, startyb += lineSpacing.NormalSpacing, 'PARADAS / ACCESOS', 'center');
                    doc.text(startX + 60, startyb += lineSpacing.NormalSpacing, 'CAP. PER / CAP. KG', 'center');
                }

                infoStarty = startY;
                doc.setDrawColor(0);
                doc.setFillColor(255, 255, 255);
                doc.roundedRect(startxb, infoStarty, 200, (lineSpacing.NormalSpacing * (data.Equipos[i].length + 1)), 0, 0, 'FD');
                for (var o = 0; o < data.Equipos[i].length; o++) {
                    doc.setFontType('normal');
                    if (data.Equipos[i][o] == null) {
                        doc.text(startxb + medio, infoStarty += lineSpacing.NormalSpacing, 'SIN INFORMACION', 'center');
                    } else {
                        doc.text(startxb + medio, infoStarty += lineSpacing.NormalSpacing, data.Equipos[i][o], 'center');
                    }

                }
                startxb += 200;

            }

        }
        
        doc.setLineWidth(0.5);
        var firmay = 750;
        doc.line(startX + 100, firmay, startX + 250, firmay);
        doc.line(startX + 300, firmay, startX + 450, firmay);
        doc.text(startX + 175, firmay += lineSpacing.NormalSpacing, 'VENDEDOR', 'center');
        doc.text(startX + 375, firmay, 'GERENCIA', 'center');
        doc.text(startX + 175, firmay += lineSpacing.NormalSpacing, data.Datos.vendedor, 'center');
        doc.text(startX + 375, firmay, 'MAURICIO GIORDANO', 'center');

        doc.save(data.Datos.codigoimp + ' - Memo de Venta.pdf');

    });
}


var formatNumber = {
    separador: "", // separador para los miles
    sepDecimal: '.', // separador para los decimales
    formatear: function (num) {
        num += '';
        var splitStr = num.split('.');
        var splitLeft = splitStr[0];
        var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
        var regx = /(\d+)(\d{3})/;
        while (regx.test(splitLeft)) {
            splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
        }
        return this.simbol + splitLeft + splitRight;
    },
    new : function (num, simbol) {
        this.simbol = simbol || '';
        return this.formatear(num);
    }
};

init();

