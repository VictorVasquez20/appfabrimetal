var tabla;

function init(){
    
     $.post("../ajax/dashventa.php?op=selectanioventa", function (r) {
        $("#anio_busq").html(r);
        $("#anio_busq").selectpicker('refresh');        
    });
    
    $( "#anio_busq" ).change(function() {        
        $.post("../ajax/dashventa.php?op=selectmesventa",{"anio_busq": $(this).val() }, function (r) {
        $("#mes_busq").html(r);
        $("#mes_busq").selectpicker('refresh');
        });
    });
    
        $("#form_busqueda").on("submit", function(e){
            buscarVentas(e);
	});
        
    tabla = $("#tblproyecto").DataTable({
        "paging": true,
        "info": false,
        "bFilter": true,
        "bAutoWidth": true,
        "ordering": true,
        "iDisplayLength": 10,
    });
    
    listar(0,0);

}

function listar(anio_busq,mes_busq){
    
    $.post("../ajax/dashventa.php?op=listar",{ "anio_busq":anio_busq,"mes_busq":mes_busq}, 
        function (data, status) {
            data = JSON.parse(data);
            sumSI = 0, sumSN = 0, xfactSI = 0, xfactSN = 0, sumFactSI = 0, sumFactSN = 0;
            
            tabla.clear().draw();

            for (i = 0; i < data.length; i++) {

            si = isNaN(Math.round(((parseInt(data[i]['mntoFactSI']) / data[i]['montosi']) * 100))) ? 0 : Math.round(((parseInt(data[i]['mntoFactSI']) / data[i]['montosi']) * 100));
            sn = isNaN(Math.round(((parseInt(data[i]['mntoFactSN']) / data[i]['montosn']) * 100))) ? 0 : Math.round(((parseInt(data[i]['mntoFactSN']) / data[i]['montosn']) * 100));
            
            tabla.row.add([
                data[i]['pronomb'],
                new Intl.NumberFormat("de-DE").format(data[i]['montosi']),
                new Intl.NumberFormat("de-DE").format(data[i]['montosi'] - parseInt(data[i]['mntoFactSI'])),
                new Intl.NumberFormat("de-DE").format(data[i]['mntoFactSI']),
                '<td class="project_progress"><div class="progress progress_sm"><div class="progress-bar bg-green" role="progressbar" data-transitiongoal="'+ si + '" aria-valuenow="'+ si + '" style="width: '+ si + '%;"></div></div></td>',
                new Intl.NumberFormat("de-DE").format(data[i]['montosn']),
                new Intl.NumberFormat("de-DE").format(data[i]['montosn'] - parseInt(data[i]['mntoFactSN'])),
                new Intl.NumberFormat("de-DE").format(data[i]['mntoFactSN']),
                '<td class="project_progress"><div class="progress progress_sm"><div class="progress-bar bg-green" role="progressbar" data-transitiongoal="'+ sn + '" aria-valuenow="'+ sn + '" style="width: '+ sn + '%;"></div></div></td>'
            ]).draw(false);
            
            sumSI = parseInt(sumSI) +  parseInt(data[i]['montosi']);
            sumSN = parseInt(sumSN) +  parseInt(data[i]['montosn']);
            
            sumFactSI = parseInt(sumFactSI) +  parseInt(data[i]['mntoFactSI']);
            sumFactSN = parseInt(sumFactSN) +  parseInt(data[i]['mntoFactSN']);
        }
        $("#SN").html(new Intl.NumberFormat("de-DE").format(sumSN) + " UF");
        $("#factSN").html(new Intl.NumberFormat("de-DE").format(sumFactSN) + " UF");
        $("#xfactSN").html(new Intl.NumberFormat("de-DE").format(sumSN - sumFactSN) + " UF");
        $("#porcSN").html( ((sumFactSN / sumSN) * 100).toFixed(1) + "%" );
        
        $("#SI").html(new Intl.NumberFormat("de-DE").format(sumSI) + " $");
        $("#factSI").html(new Intl.NumberFormat("de-DE").format(sumFactSI) + " $");
        $("#xfactSI").html(new Intl.NumberFormat("de-DE").format(sumSI - sumFactSI) + " $");
        $("#porcSI").html( ((sumFactSI / sumSI) * 100).toFixed(1) + "%" );
    });
}

function buscarVentas(e){
    e.preventDefault();
    listar($("#anio_busq").val(),$("#mes_busq").val());
}

init();

