var tabla;

//funcion que se ejecuta iniciando
function init() {
    mostarform(true);
    cargaObras();

    $("#formulariofin").on("submit", function (e) {
        e.preventDefault();
        guardaryeditar();
    });

    $.post("../ajax/centrocosto.php?op=selecttipo", function (r) {
        $("#tcentrocosto").html(r);
        $("#tcentrocosto").selectpicker('refresh');
    });

    $.post("../ajax/analisisfalla.php?op=responsables", function(r){
        $("#responsable").html(r);
        $("#responsable").selectpicker('refresh');
    });

}

function cargaObras(){
    $.post("../ajax/analisisfalla.php?op=cargaObra", function (r) {
        $("#obra").html(r);
        $("#obra").selectpicker('refresh');
    });
}


// Otras funciones
function limpiar() {

    $("#obra").val(0);
    $("#obra").selectpicker('refresh');
    $("#tipofalla").val(0);
    $("#tipofalla").selectpicker('refresh');
    $("#descripcionFalla").val("");
    $("#imputable").val(0);
    $("#imputable").selectpicker('refresh');
    $("#requerimiento").val(0);
    $("#requerimiento").selectpicker('refresh');
    $("#descRequerimiento").val("");
    $("#responsable").val(0);
    $("#responsable").selectpicker('refresh');

}

function mostarform(flag) {
    limpiar();
    if (flag) {
        $("#listadocentros").hide();
        $("#formulariocentros").show();
        $("#op_agregar").hide();
        $("#op_listar").show();
        $("#btnGuardar").prop("disabled", false);

    } else {
        $("#listadocentros").show();
        $("#formulariocentros").hide();
        $("#op_agregar").show();
        $("#op_listar").hide();
    }
}

function cancelarform() {
    limpiar();
    mostarform(false);
}

function guardaryeditar() {
    
    $("#btnGuardar").prop("disabled", true);
    var formData = new FormData($("#formulariofin")[0]);
    $.ajax({
        //url: '../ajax/centrocosto.php?op=cguardaryeditar',
        url: '../ajax/analisisfalla.php?op=guardarAnalisis',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,

        success: function (datos) {
            if (datos != '0') {
                $("#btnGuardar").prop("disabled", false);
                new PNotify({
                    title: 'Correcto!',
                    text: 'Análisis enviado con exito.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
            } else {
                $("#btnGuardar").prop("disabled", true);
                new PNotify({
                    title: 'Error!',
                    text: 'Análisis no pudo ser guardado.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
        }
    });
    limpiar();
}

function mostar(idcentrocosto) {
    $.post("../ajax/centrocosto.php?op=mostar", {idcentrocosto: idcentrocosto}, function (data, status) {
        data = JSON.parse(data);
        mostarform(true);

        $("#idcentrocosto").val(data.idcentrocosto);
        $("#tcentrocosto").val(data.tcentrocosto);
        $("#tcentrocosto").selectpicker('refresh');
        $("#codigo").val(data.codigo);
        $("#codexterno").val(data.codexterno);
        $("#nombre").val(data.nombre);

        if (data.condicion == 1) {
            $("#condicion").val(1);
        } else {
            $("#condicion").val(0);
        }
    });
}

init();