<?php ob_start(); session_start(); ?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>App Fabrimetal</title>

		<link rel="icon" href="../public/favicon.ico" type="image/x-icon" />

		<!-- Bootstrap -->
		<link href="../public/build/css/bootstrap.min.css" rel="stylesheet">
		<!-- Font Awesome -->
		<link href="../public/build/css/font-awesome.min.css" rel="stylesheet">
		<!-- Custom Theme Style -->
		<link href="../public/build/css/custom.css" rel="stylesheet">
	</head>
	<body class="nav-md">
		<div class="container body">
			<div class="main_container">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
								<h2>Ascensores Detenidos</h2>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<div class="row top_tiles" style="text-align: center;">
									<div class="animated flipInY col-lg-3 col-md-6 col-sm-12 col-xs-12">
										<div class="tile-stats">
											<div class="count" id="total_detenidos"></div>
											<h3>Total</h3>
										</div>
									</div>
									<div class="animated flipInY col-lg-3 col-md-6 col-sm-12 col-xs-12">
										<div class="tile-stats">
											<div class="count" id="en_evaluacion"></div>
											<h3>En Evaluación</h3>
										</div>
									</div>
									<div class="animated flipInY col-lg-3 col-md-6 col-sm-12 col-xs-12">
										<div class="tile-stats">
											<div class="count" id="cotizados"></div>
											<h3>Cotizados</h3>
										</div>
									</div>
									<div class="animated flipInY col-lg-3 col-md-6 col-sm-12 col-xs-12">
										<div class="tile-stats">
											<div class="count" id="aprobados"></div>
											<h3>Aprobados</h3>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12" style="overflow:hidden;">
							<div class="x_panel">
								<div class="x_title">
									<h3 id="">Guías de servicio finalizadas con el equipo detenido</h3> 
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<div class="panel">
										<canvas height="80%" id="mybarChart"></canvas>
									</div>
								</div>  
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!-- jQuery -->
		<script src="../public/build/js/jquery.min.js"></script>
		<!-- Bootstrap -->
		<script src="../public//build/js/bootstrap.min.js"></script>
		<!-- Chart.js -->
		<!-- <script src="../public/build/js/Chart.min.js"></script> -->
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.0/Chart.bundle.min.js"></script>

		<!-- ECharts -->
		<script src="../public/build/js/echarts.min.js"></script>


		<!-- Custom Theme Scripts -->
		<script src="../public/build/js/custom.js"></script>
		<script src="../public/build/js/utiles.js"></script>
		<script type="text/javascript" src="scripts/detenidos2.js"></script>
	</body>
</html>
<?php ob_end_flush(); ?>