<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {

    require 'header.php';

    if ($_SESSION['administrador'] == 1 || $_SESSION['AsigProyectos'] == 1 || $_SESSION['Icontratos'] == 1) {
        ?>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2 id="titulo_pagina"></h2>
                                <div class="clearfix"></div>
                            </div>
                            <div id="listadoproyectos" class="x_content">

                                                                                                                                                                    <!--<table id="tblproyectos" class="table table-striped border border-gray-dark projects dt-responsive" cellspacing="0" width="100%">-->
                                <table id="tblproyectos" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>OPCIONES</th>
                                            <th>PROYECTO</th>
                                            <th>CODIGO</th>
                                            <th>MANDANTE</th>
                                            <th>DIRECCION</th>  
                                            <th>N° ASC</th>
                                            <th>N° ASC <br> ENTREGADOS</th>
                                            <th>N° ASC <br> SIN CONTRATO</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>

                            <div id="formularicontratos" style="display: none;" class="x_content">
                                <form id="formulariotodo" name="formulariotodo" method="post" class="form-horizontal form-label-left">
                                    <h4><b>DATOS DEL CONTRATO</b></h4>  
                                    <div class="row"> 
                                        <div class="col-md-11 col-sm-12 col-xs-10 form-group">
                                            <select class="form-control selectpicker" data-live-search="true" id="idcontrato" name="idcontrato" required="required"></select>
                                        </div>
                                        <div class="col-md-1 col-sm-12 col-xs-12 form-group">
                                            <button class="btn btn-info btn-sm" type="button" data-toggle="modal" data-target="#modalContrato"><i class="fa fa-plus-circle"></i></button>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="ln_solid"></div> 
                                    <h4><b>DATOS DEL EDIFICIO Y EQUIPOS</b></h4>  
                                    <div class="row"> 
                                        <div class="col-md-11 col-sm-12 col-xs-10 form-group">
                                            <select class="form-control selectpicker" data-live-search="true" id="idedificio" name="idedificio" required="required"></select>
                                            <input type="hidden" id="region" name="region">
                                        </div>
                                        <div class="col-md-1 col-sm-12 col-xs-12 form-group">
                                            <button class="btn btn-info btn-sm" type="button" data-toggle="modal" data-target="#modalEdificio"><i class="fa fa-plus-circle"></i></button>
                                        </div>
                                        <div class="clearfix"></div><div class="ln_solid"></div>
                                    </div>
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2>ASCENSORES</h2>
                                            <div class="clearfix"></div>
                                        </div>
                                        <input type="hidden" id="nascensores" name="nascensores" class="form-control">
                                        <div class="x_content"  id="listaascensor">

                                        </div>
                                    </div>

                                    <div class="modal-footer">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <button class="btn btn-primary" type="button" id="btnCancelar"  onclick="cancelarform()">Cancelar</button>
                                            <button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiar()">Limpiar</button>
                                            <button class="btn btn-success" type="submit" id="btnGuardar">Agregar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal -->
           

            <div class="modal fade" id="revisar" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Close</span>
                            </button>
                            <h4 class="modal-title text-success" id="nombProy"></h4>
                        </div>


                        <!-- Modal Body -->
                        <div class="modal-body">

                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th style="width:20%">Dirección</th>
                                        <td class="green" colspan="3" id="direccion"></td>
                                    </tr>
                                    <tr>
                                        <th style="width:20%">Etapa Actual:</th>
                                        <td style="width:30%" class="green" id="estadoproy"></td>
                                        <th style="width:20%">Codigo</th>
                                        <td style="width:30%" class="green" id="codigo"></td>
                                    </tr>
                                    <tr>
                                        <th style="width:20%">Fecha inicial:</th>
                                        <td style="width:30%" class="green" id="created_time"></td>
                                        <th style="width:20%">Fecha Final</th>
                                        <td style="width:30%" class="green" id="closed_time"></td>
                                    </tr>
                                    <tr>
                                        <th style="width:20%">Mandante</th>
                                        <td class="green" colspan="3" id="mandante"></td>
                                    </tr>
                                    <tr>
                                        <th style="width:20%">Rut</th>
                                        <td class="green" colspan="3" id="ruts"></td>
                                    </tr>
                                </tbody>
                            </table>
                            <hr>
                            <div class="title" style="text-align: center;"><h4>Ascensores</h4></div>
                            <table id="ascensorestbl" class="table table-striped table-bordered" style="text-align: center;">
                                <thead>
                                    <tr>
                                        <th style="text-align: center;">CODIGO</th>
                                        <th style="text-align: center;">MARCA</th>
                                        <th style="text-align: center;">MODELO</th>
                                        <th style="text-align: center;">KEN</th>
                                        <th style="text-align: center;">UBICACIÓN</th>
                                        <th style="text-align: center;">ESTADO</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                        <!-- Modal Footer -->
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                        </div>

                    </div>
                </div>
            </div>

            <div class="modal fade" id="modalContrato" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel">AGREGAR CONTRATO</h4>
                        </div>

                        <form id="formulario" name="formulario" method="post" class="form-horizontal form-label-left">
                            <div class="modal-body">
                                <h4><b>DATOS DEL CLIENTE</b></h4>  
                                <div class="col-md-11 col-sm-12 col-xs-10 form-group">
                                    <select class="form-control selectpicker" data-live-search="true" id="idcliente" name="idcliente" required="required"></select>
                                </div>
                                <div class="col-md-1 col-sm-12 col-xs-12 form-group">
                                    <button class="btn btn-info btn-sm" type="button" data-toggle="modal" data-target="#modalForm"><i class="fa fa-plus-circle"></i></button>
                                </div>
                                <div class="clearfix"></div>
                                <div class="ln_solid"></div>
                                <h4><b>DATOS DEL CONTRATO</b></h4>  
                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <label>Region <span class="required">*</span></label>
                                    <select class="form-control selectpicker" data-live-search="true" id="conidregiones" name="conidregiones" required="Campo requerido"></select>
                                </div>

                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <label for="tcontrato">Tipo de contrato <span class="required">*</span></label>
                                    <select class="form-control selectpicker" data-live-search="true" id="tcontrato" name="tcontrato" required="required"></select>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                    <label for="ncontrato">Numero de contrato <span class="required">*</span></label>
                                    <input type="text" id="ncontrato" name="ncontrato" required="required" class="form-control" data-inputmask="'mask' : 'AA-99-A-AA-9999'">
                                </div>

                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                    <label for="nexterno">Id. contrato cliente</label>
                                    <input type="text" id="nexterno" name="nexterno" class="form-control">
                                </div>

                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                    <label for="ubicacion">Ubicacion</label>
                                    <input type="text" id="ubicacion" name="ubicacion" class="form-control">
                                </div>  
                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                    <label for="fecha">Fecha firma de contrato <span class="required">*</span></label>
                                    <input type='date' id="fecha" name="fecha" class="form-control" required="required"/>
                                </div>

                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                    <label for="fecha">Fecha inicio de contrato <span class="required">*</span></label>
                                    <input type='date' id="fecha_ini" name="fecha_ini" class="form-control" required="required"/>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                    <label for="fecha">Fecha fin de contrato (Blanco Indefinido)</label>
                                    <input type='date' id="fecha_fin" name="fecha_fin" class="form-control"/>
                                </div>

                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                    <label for="nreferencia">Tipo Entrada</label>
                                    <!--<input id="nreferencia" class="form-control" type="text" name="nreferencia">-->
                                    <select id="nreferencia" name="nreferencia" class="selectpicker form-control">
                                        <option value="" disabled="" selected="">Sleccione una opción</option>
                                        <option value="NEB KONE">NEB KONE</option>
                                    </select>
                                </div>

                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                    <label for="idperiocidad">Periocidad <span class="required">*</span></label>
                                    <select class="form-control selectpicker" data-live-search="true" id="idperiocidad" name="idperiocidad" required="required"></select>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                    <label for="rautomatica">Renovacion Automatica <span class="required">*</span></label>
                                    <select class="form-control selectpicker" data-live-search="false" id="rautomatica" name="rautomatica" required="required">
                                        <option value="" selected disabled>Renovacion Automatica?</option>
                                        <option value="0">NO</option>
                                        <option value="1">SI</option>
                                    </select>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label for="observaciones">Observaciones adicionales del contrato</label>
                                    <textarea type="text" id="observaciones" name="observaciones" class="resizable_textarea form-control"></textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <!-- Modal Footer -->
                            <div class="modal-footer">
                                <button type="reset" onclick="limpiarcontrato()" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                <button type="submit" class="btn btn-primary submitBtn" id="btnGuardarcont">Agregar</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modalEdificio" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Close</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">AGREGAR EDIFICIO</h4>
                        </div>

                        <form role="form" id="formedificio" name="formedificio">
                            <!-- Modal Body -->
                            <div class="modal-body">
                                <p class="statusMsg"></p>

                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label>Nombre del edificio <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="nombreedi" id="nombreedi" required="Campo requerido">
                                </div>
                                <div class="col-md-10 col-sm-12 col-xs-12 form-group">
                                    <label>Calle o Avenida <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="calleedi" id="calleedi" required="Campo requerido">
                                </div>
                                <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                                    <label>Numero <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="numeroedi" id="numeroedi" required="Campo requerido">
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <label>Region <span class="required">*</span></label>
                                    <select class="form-control selectpicker" data-live-search="true" id="idregionesedi" name="idregionesedi" required="Campo requerido">
                                    </select>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <label>Comuna <span class="required">*</span></label>
                                    <select class="form-control selectpicker" data-live-search="true" id="idcomunasedi" name="idcomunasedi" required="Campo requerido">
                                    </select>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label>Segmento <span class="required">*</span></label>
                                    <select class="form-control selectpicker" data-live-search="true" id="idtsegmento" name="idtsegmento" required="Campo requerido">
                                    </select>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label>Coordinacion por correo? <span class="required">*</span></label>
                                    <select class="form-control selectpicker" data-live-search="true" id="corcorreo" name="corcorreo" required="Campo requerido">
                                        <option value="" selected disabled>Coordinacion</option>
                                        <option value="1">SI</option>
                                        <option value="0">NO</option>
                                    </select>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label>Residente? <span class="required">*</span></label>
                                    <select class="form-control selectpicker" data-live-search="true" id="residente" name="residente" required="Campo requerido">
                                        <option value="" selected disabled>Residente</option>
                                        <option value="1">SI</option>
                                        <option value="0">NO</option>
                                    </select>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label>Nro de contactos <span class="required">*</span></label>
                                    <select class="form-control selectpicker" data-live-search="true" id="ncon_edi" name="ncon_edi" required="Campo requerido">
                                        <option value="" selected disabled>N° Contactos edificio</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                    </select>
                                </div>
                                <div id="contactoedi" name="contactoedi">
                                </div>                                             
                            </div>
                            <div class="clearfix"></div>

                            <!-- Modal Footer -->
                            <div class="modal-footer">
                                <button type="reset" onclick="limpiaredificio()" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                <button type="submit" class="btn btn-primary submitBtn" id="btnGuardarEdi">Agregar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            
            <div class="modal fade" id="modalForm" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Close</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">AGREGAR CLIENTE</h4>
                        </div>

                        <form role="form" id="formcliente" name="formcliente">
                            <!-- Modal Body -->
                            <div class="modal-body">
                                <p class="statusMsg"></p>

                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label>RUT <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="rut" id="rut" required="Campo requerido" data-inputmask="'mask' : '[*9.][999.999]-[*]'">
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label>Razon Social <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="razon" id="razon" required="Campo requerido" readonly="">
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label>Tipo <span class="required">*</span></label>
                                    <select class="form-control selectpicker" data-live-search="true" id="idtcliente" name="idtcliente" required="Campo requerido">
                                    </select>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <label>Avenida o Calle <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="callecli" id="callecli" required="Campo requerido">
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                    <label>Numero <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="numerocli" id="numerocli" required="Campo requerido">
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                    <label>Oficina</label>
                                    <input type="text" class="form-control" name="oficinacli" id="oficinacli">
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <label>Region <span class="required">*</span></label>
                                    <select class="form-control selectpicker" data-live-search="true" id="idregionescli" name="idregionescli" required="Campo requerido">
                                    </select>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <label>Comuna <span class="required">*</span></label>
                                    <select class="form-control selectpicker" data-live-search="true" id="idcomunascli" name="idcomunascli" required="Campo requerido">
                                    </select>
                                </div>                                              
                            </div>
                            <div class="clearfix"></div>

                            <!-- Modal Footer -->
                            <div class="modal-footer">
                                <button type="reset" onclick="limpiarcliente()" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                <button type="submit" class="btn btn-primary submitBtn" id="btnGuardarCli">Agregar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php
    } else {
        require 'nopermiso.php';
    }
    require 'footer.php';
    ?>
    <script type="text/javascript" src="scripts/contratoinstalacion.js"></script>
    <?php
}
ob_end_flush();

