<?php
ob_start();
?>

<html lang="es">
    <head>
        <base href="/appfabrimetal/public/">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>App Fabrimetal</title>

        <link rel="icon" href="../public/favicon.ico" type="image/x-icon" />


        <!-- Bootstrap -->
        <link href="../public/build/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="../public/build/css/font-awesome.min.css" rel="stylesheet">
        
        <!-- Custom Theme Style -->
        <!-- <link href="../public/build/css/custom.min.css" rel="stylesheet"> -->

        <!-- PNotify -->
        <link href="../public/build/css/pnotify.css" rel="stylesheet">
        <link href="../public/build/css/pnotify.buttons.css" rel="stylesheet">
        <link href="../public/build/css/pnotify.nonblock.css" rel="stylesheet">

        <!-- Datatables -->
        <link href="../public/build/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="../public/build/css/buttons.bootstrap.min.css" rel="stylesheet">
        <link href="../public/build/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
        <link href="../public/build/css/responsive.bootstrap.min.css" rel="stylesheet">
        <link href="../public/build/css/scroller.bootstrap.min.css" rel="stylesheet">

        <style>
        /*@import url('https://fonts.googleapis.com/css2?family=Montserrat&display=swap');*/
        </style>

        <style>
        /*@import url('https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400&display=swap');*/
        </style>

        <style>
        /*@import url('https://fonts.googleapis.com/css2?family=Roboto&display=swap');*/
        </style>

        <style>
        /*@import url('https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap');*/
        </style>

        <style>
        /*@import url('https://fonts.googleapis.com/css2?family=Roboto:wght@100&display=swap');*/
        </style>

        <style>
        /*@import url('https://fonts.googleapis.com/css2?family=Playfair+Display&display=swap');*/
        </style>

        <style>
        @import url('https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap');
        </style>

        <style>
        /*@import url('https://fonts.googleapis.com/css2?family=Lato:wght@100&family=Open+Sans:wght@300&family=Playfair+Display&display=swap');*/
        </style>

        <style>
        /*@import url('https://fonts.googleapis.com/css2?family=Ubuntu:wght@300&display=swap');*/
        </style>

        <style>
            * {
                font-size: 13px;
                /*font-family: 'Ubuntu', sans-serif, Verdana, Arial;*/
                /*font-family: 'Lato', sans-serif, Verdana, Arial;*/
                font-family: 'Open Sans', sans-serif, Verdana, Arial;
                /*font-family: 'Playfair Display', 'Roboto', Verdana, Arial;*/
                /*font-family: Gotham, 'Roboto', Verdana, Arial;*/
                /*font-family: 'Roboto', Verdana, Arial;*/
                /*font-family: Montserrat, Verdana, Arial;*/
                font-weight: bold;
            }
            body {
                font-size: 2em;
                /*margin:  0;
                padding:  0;*/
            }
            .contenedor {
                display: flex;
                flex-direction: column;
                align-items: center;
                /*Justify-content: space-between;*/
            }

            .titulo {
                font-size:  2em;
            }

            form {
                margin-block-end: 0em;
            }

            .rowRed {
                background-color: #FFE959;
                /*color: #FFFFFF;*/
            }

            .rowNormal {
                background-color: #FFFFFF;
                color: #000000;
            }

            /*estilos para que el comentario en el listado se ajuste a la celda*/
            /*se coloca este codigo js en la configuracion del datatable*/
            /*columnDefs: [
                {
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap width-98'>" + data + "</div>";
                    },
                    targets: 1 //el indice segun el orden del campo a ajustar
                }
             ]*/
            .text-wrap{
                white-space:normal;
            }
            .width-98{
                width:98%;
            }

            /*estilo para generar scroll en el modal*/
            .modal-body {
                max-height: calc(100vh - 150px);
                overflow-y: auto;
            }
        </style>
    </head>
    <body>
        <div class="container-fluid" style="border: 1px solid black; background-color: #5A748D; color: #FFFFFF; height: 48px; padding: 5px">
            <div class="row">
                <div class="col-md-2"><img src="../production/informes/img/fm-logo-negro.png" height="30" alt=""></div>
                <div class="col-md-5 titulo">CONTROL DE IMPORTACIÓN DE REPUESTOS</div>
                <div class="col-md-5">
                    <form class="form-inline" id="form_filtros2">
                        <div class="form-group">
                            <label for="">Año</label>
                            <select class="form-control"  id="anio" name="anio" required="required"></select> 
                          </div>
                          <div class="form-group">
                            <label for="">Mes</label>
                          <select class="form-control"  id="mes" name="mes" required="required"></select>
                          </div>
                          <button type="submit" class="btn btn-primary">Buscar</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- <hr> -->
        <div class="container body" style="width: 100%; height: 100%; text-align: center;">
            <div class="main_container">
    <div class="right_col" role="main">
        <div class="">
            <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div id="listaimportacion" class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h4 id="titulo_listaimportacion"></h4>
                                        <div class="clearfix"></div>
                                    </div>
                                           <div class="x_content">
                                                <table id="tbllistaimportacion2" class="table table-bordered dt-responsive" cellspacing="0" width="100%">
                                                <thead>
                                                  <tr>
                                                    <th></th>
                                                    <th>ST</th>
                                                    <th>ST Fecha</th>
                                                    <th>REF</th>
                                                    <th>Fec Ped</th>
                                                    <th>KM</th>
                                                    <th>Desc.</th>
                                                    <th>Cant.</th>
                                                    <th>Pres.</th>
                                                    <th>Sol.</th>
                                                    <th>Obs.</th>
                                                    <th>ETD</th>
                                                    <th>Desp</th>
                                                    <th>Est Lleg</th>
                                                    <th>Lleg</th>
                                                  </tr>
                                                </thead>
                                                <tbody></tbody>
                                              </table>
                                            <div style="height: 20px;"></div>
                                      </div> 
                                </div>
                            </div>
                        </div>                                                                 
                </div>
            </div>
        </div>
    </div>
        
</div>
        </div>


        <div class="modal fade" id="modalComent" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">COMENTARIOS DE LA IMPORTACION (<span id="codkm2"></span>)</h4>
                    </div>

                    <form role="form" id="formcomentario" name="formcomentario">
                        <input type="hidden" id="idcom2" name="idcom" value="">
                        <!-- Modal Body -->
                        <div class="modal-body">
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                <div class="x_panel" id="listacomentarios">
                                    <table id="tblcomentarios" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th> Fecha</th>
                                                <th> Usuario</th>
                                                <th> Comentario</th>
                                                <!-- <th> Observación</th> -->
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
        <!-- jQuery -->
        <script src="../public/build/js/jquery.min.js"></script>
        <script src="../public/build/js/jquery.blink2.js"></script>
        <!-- Bootstrap -->
        <script src="../public/build/js/bootstrap.min.js"></script>
        <!-- DateJS -->
        <script src="../public/build/js/date.js"></script>
        <!-- bootstrap-daterangepicker -->
        <script src="../public/build/js/moment.min.js"></script>
        <script src="../public/build/js/daterangepicker.js"></script>

        <!-- PNotify -->
        <script src="../public/build/js/pnotify.js"></script>
        <script src="../public/build/js/pnotify.buttons.js"></script>
        <script src="../public/build/js/pnotify.nonblock.js"></script>

        <!-- Datatables -->
        <script src="../public/build/js/jquery.dataTables.min.js"></script>
        <script src="../public/build/js/dataTables.bootstrap.min.js"></script>
        <script src="../public/build/js/dataTables.buttons.min.js"></script>
        <script src="../public/build/js/buttons.bootstrap.min.js"></script>
        <script src="../public/build/js/buttons.flash.min.js"></script>
        <script src="../public/build/js/buttons.html5.min.js"></script>
        <script src="../public/build/js/buttons.print.min.js"></script>
        <script src="../public/build/js/dataTables.fixedHeader.min.js"></script>
        <script src="../public/build/js/dataTables.keyTable.min.js"></script>
        <script src="../public/build/js/dataTables.responsive.min.js"></script>
        <script src="../public/build/js/responsive.bootstrap.js"></script>
        <script src="../public/build/js/dataTables.scroller.min.js"></script>
        <script src="../public/build/js/jszip.min.js"></script>
        <script src="../public/build/js/pdfmake.min.js"></script>
        <script src="../public/build/js/vfs_fonts.js"></script>


        <!-- Custom Theme Scripts -->
        <script src="../public/build/js/custom.js"></script>
        <script src="../public/build/js/utiles.js"></script>
        <script type="text/javascript" src="../production/scripts/pantallaimportacion.js"></script>
    </body>
</html>        
<?php
ob_end_flush();
?>