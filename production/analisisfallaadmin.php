<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {

    require 'header.php';

    if ($_SESSION['administrador'] == 1) { ?>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Análisis de Falla</h2>
                                <div class="clearfix"></div>
                            </div>
                            <div id="listadoanalisis" class="x_content">

                                <table id="tblanalisis" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>OPCIONES</th>
                                            <th>OBRA</th>
                                            <th>TIPO FALLA</th>
                                            <th>IMPUTABLE</th>
                                            <th>REQUERIMIENTO</th>
                                            <th>RESPONSABLE</th>
                                            <th>FECHA</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal 1-->
        <div class="modal fade" id="detalle" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">DETALLE ANÁLISIS</h4>
                    </div>
                    <form id="formprocesarservicio" name="formprocesarservicio" class="form form-horizontal" enctype="multipart/form-data">
                        <div class="modal-body">
                            <div class="form-group">
                                
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                                    <label class="control-label">DESCRIPCIÓN FALLA</label>
                                    <textarea name="descripcionfalla" id="descripcionfalla" class="form-control" required="" style="text-transform:uppercase;" readonly></textarea>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                                    <label class="control-label">DESCRIPCIÓN REQUERIMIENTO</label>
                                    <textarea name="descrequerimiento" id="descrequerimiento" class="form-control" required="" style="text-transform:uppercase;" readonly></textarea>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Modal 2-->
        <div class="modal fade" id="aprobar" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">APROBAR ANÁLISIS</h4>
                    </div>
                    <form id="formAprobar" name="formAprobar" class="form form-horizontal" enctype="multipart/form-data">
                        <div class="modal-body">
                            <div class="form-group">
                                
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                                    <label class="control-label">OBSERVACIONES</label>
                                    <input type="hidden" id="idanalisisfalla" name="idanalisisfalla" class="form-control" value="">
                                    <textarea name="obs_ji" id="obs_ji" class="form-control" required="" style="text-transform:uppercase;"></textarea>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                                    <label class="control-label">DESTINATARIOS CORREO (separados por ";")</label>
                                    <input type="text" id="destinatarios" name="destinatarios" class="form-control" >
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default antoclose" data-dismiss="modal">CANCELAR</button>
                            <button type="submit" id="enviar" class="btn btn-primary">ENVIAR</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modalPreview" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" id="contenido">

                </div>
            </div>
        </div>

        <hidden id="detalle-modal" name="detalle-modal" data-toggle="modal" data-target="#detalle"></hidden>
        <hidden id="aprobar-modal" name="aprobar-modal" data-toggle="modal" data-target="#aprobar"></hidden>

        <div id="modalImagenes" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="min-width:60%;">
                <form id="Motivoform" class="form-horizontal calender" role="form" enctype="multipart/form-data">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" id="myModalLabel">IMAGENES ASOCIADAS</h4>
                        </div>
                        <div class="modal-body">
                            <div id="testmodal" style="padding: 5px 10px;">
                                <div class="form-group">
                                <div id="divimagenes" ></div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default antoclose" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    <?php
    } else {
        require 'nopermiso.php';
    }
    require 'footer.php';
    ?>
    <script src="../public/build/js/libs/png_support/png.js"></script>
    <script src="../public/build/js/libs/png_support/zlib.js"></script>
    <script src="../public/build/js/jspdf.debug.js"></script>
    <script src="../public/build/js/jspdf.plugin.autotable.js"></script>
    <script src="../public/build/js/jsPDFcenter.js"></script>
    <script src="../public/build/js/SimpleTableCellEditor.js"></script>
    <script type="text/javascript" src="scripts/analisisfallaadmin.js"></script>

    <script>
        $(document).ready(function() {

            editor = new SimpleTableCellEditor("tblguias");
            editor.SetEditableClass("editMe");

            $('#tblguias').on("cell:edited", function(event) {
                console.log(`'${event.oldValue}' changed to '${event.newValue}'`);
            });

        });
    </script>

<?php
}
ob_end_flush();
?>