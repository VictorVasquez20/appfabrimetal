<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {

    require 'header.php';

    if ($_SESSION['administrador'] == 1 || $_SESSION['Instalaciones'] == 1) {
        ?>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>REPORTES</h2>
                                <div class="clearfix"></div>
                            </div>
                            <div id="" class="x_content">
                               <div class="col-md-12 col-sm-12 col-xs-12">
                                   <select class="form-control selectpicker"  id="reporte" name="reporte" >
                                       <option value="" disabled="disabled" selected="selected"><-- SELECCIONE EL REPORTE--></option>
                                       <option value="listadoproyectos"><-- LISTADO DE PROYECTOS --></option>
                                       <option value="listadovisitas"><-- LISTADO DE VISITAS POR PROYECTO --></option>
                                   </select>                           
                               </div>     
                            </div>                            
                        </div> 
                                                                            
                <div id="listaproyectos" class="row" style="display: none" >
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>LISTADO DE PROYECTOS </h2>     
                                 <div class="clearfix"></div>                                  
                            </div>
                            <div id="" class="x_content">
                                         <table id="tbllistaproyectos" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>PROYECTO</th>
                                                    <th>CODIGO</th>
                                                    <th>MANAGER</th>    
                                                    <th>SUPERVISOR</th>
                                                    <th>PROGRESO</th>
                                                    <th>ESTADO</th>
                                                    <th>DIAS DESDE COMIENZO</th>
                                                    <th>FECHA <br> ACTUALIZACIÓN</th>
                                                    <th>DIAS EN EL ESTADO</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>         
                                        </table>       
                                    </div>
                               </div>
                           </div>
                        </div>        
                        
                <div id="visitaproyectos" class="row" style="display: none">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title"> 
                                 <h2>LISTADO DE VISITAS POR PROYECTO </h2>  
                                 <div class="clearfix"></div>
                            </div>
                            <div id="" class="x_content">
                                         <table id="tblvisitaproyectos" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>PROYECTO</th>
                                                    <th>CODIGO</th>  
                                                    <th>ESTADO</th>
                                                    <th>CARGO</th>
                                                    <th>NOMBRE</th>
                                                    <th>VISITAS</th>
                                                    <th>VER DETALLE</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                            
                                        </table>       
                                    </div>
                               </div>
                           </div>
                        </div>      
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

<div class="modal fade" id="modalVisitas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
       <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
            </button>
            <h4 class="modal-title" id="titulo_modal">Visitas por Estado de Proyecto</h4>
        </div>      
            <div class="modal-body">
                    <form class="form-horizontal">
                    <div  class="row">  
                        <div class="col-xs-6 col-sm-6">                        
                            <div class="form-group">
                              <label class="col-sm-4 control-label">Proyecto:</label>
                              <div class="col-sm-8">
                                  <p class="form-control-static" id="nombre_proyecto"></p>
                              </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6"> 
                            <div class="form-group">
                                <label class="col-sm-4 control-label">C&oacute;digo:</label>
                              <div class="col-sm-8">
                                  <p class="form-control-static" id="codigo_proyecto"></p>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div  class="row"> 
                        <div class="col-xs-6 col-sm-6"> 
                            <div class="form-group">
                              <label class="col-sm-4 control-label">Nombre:</label>
                              <div class="col-sm-8">
                                  <p class="form-control-static" id="nombre_tecnico"></p>
                              </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6"> 
                            <div class="form-group">
                              <label class="col-sm-4 control-label">Cargo:</label>
                              <div class="col-sm-8">
                                  <p class="form-control-static" id="cargo_tecnico"></p>
                              </div>
                            </div>
                        </div>
                    </div>
                  </form>
               <table id="tblvisitasetapa" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>ESTADO</th>
                                                    <th>VISITAS</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>                                            
                                        </table>
            </div>
            <div class="modal-footer"></div>                        
    </div>
  </div>
</div>   
    
        
        <?php
    } else {
        require 'nopermiso.php';
    }
    require 'footer.php';
    ?>
    <script src="../public/build/js/jspdf.min.js"></script>
    <script src="../public/build/js/jspdf.plugin.autotable.js"></script>
    <script src="../public/build/js/jsPDFcenter.js"></script>
    <script src="../public/build/js/nprogress.js"></script>
    <script src="../public/build/js/bootstrap-progressbar.min.js"></script>
    <script type="text/javascript" src="scripts/reportevisitas.js"></script>
    <?php
}
ob_end_flush();
?>