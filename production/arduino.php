<?php
ob_start();
session_start();

if(!isset($_SESSION["nombre"])){
  header("Location:login.php");
}else{

require 'header.php';


 ?>
        <!-- Contenido -->
        <div class="right_col" role="main">
        
          <!-- Datos actulidad en fallas -->
          <div class="row tile_count">
            <div class="col-md-6 col-sm-6 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-check"></i> Arduino Normal </span>
              <div class="count"><a href="#" id="num_normal"></a></div>
              <span class="count_bottom"><i id="color_normal" class=""><i id="icon_normal" class=""></i></i><a id="pornormal"></a> % Total</span>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-exclamation-triangle"></i> Arduino Falla</span>
              <div class="count"><a href="#" id="num_falla"</a></div>
              <span class="count_bottom"><i id="color_falla" class="green"><i id="icon_falla" class="fa fa-sort-asc"></i></i><a id="porfalla"></a> % Total</span>
            </div>
          </div>

          <br />
          
          <!-- Contenido Listar alertas especificas -->
          <div id="Listado" class="row">         
			       <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Arduinos en falla</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div id="listadoarduinos" class="x_content">

                    <table id="tblarduinos" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                      		<th>CODIGO</th>
                          	<th>FUNCION</th>
                          	<th>DIRK</th>
                          	<th>UBICAICON</th>
                          	<th>EDIFICIO</th>
                          	<th>ACTUALIZACION</th>
                                <th>ESTADO</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
                  
          </div>
          <!-- /Fin Listar alertas especificas -->
          
        </div>
        <!-- /Fin Contenido -->

<?php 
require 'footer.php';
?>
<script type="text/javascript" src="scripts/arduino.js"></script>

<?php
}
ob_end_flush();
?>
