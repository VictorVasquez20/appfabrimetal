<?php ob_start(); session_start();?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>App Fabrimetal</title>

		<link rel="icon" href="../public/favicon.ico" type="image/x-icon" />

		<!-- Bootstrap -->
		<link href="../public/build/css/bootstrap.min.css" rel="stylesheet">
		<!-- Font Awesome -->
		<link href="../public/build/css/font-awesome.min.css" rel="stylesheet">
		<!-- Custom Theme Style -->
		<link href="../public/build/css/custom.css" rel="stylesheet">
	</head>

	<body class="nav-md">
		<div class="container body">
			<div class="main_container">
				<div class="row">
					<div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-6 px-0">
						<div class="x_panel">
							<div class="x_title">
								<h3>PRESUPUESTOS</h3>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<div class="row top_tiles" style="text-align: center;">
									<div class="animated flipInY col-lg-3 col-md-6 col-sm-12 col-xs-12">
										<div class="tile-stats">
											<div class="count" id="total_presupuestos"></div>
											<h3>Activos</h3>
										</div>
									</div>
									<div class="animated flipInY col-lg-3 col-md-6 col-sm-12 col-xs-12">
										<div class="tile-stats">
											<div class="count" id="en_proceso"></div>
											<h3>En Proceso</h3>
										</div>
									</div>
									<div class="animated flipInY col-lg-3 col-md-6 col-sm-12 col-xs-12">
										<div class="tile-stats">
											<div class="count" id="enviados"></div>
											<h3>Enviados</h3>
										</div>
									</div>
									<div class="animated flipInY col-lg-3 col-md-6 col-sm-12 col-xs-12">
										<div class="tile-stats">
											<div class="count" id="aceptados"></div>
											<h3>Aceptados</h3>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-6 px-0">
						<div class="x_panel">
							<div class="x_title">
								<h3>REPARACIONES</h3>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<div class="row top_tiles" style="text-align: center;">
									<div class="animated flipInY col-lg-3 col-md-6 col-sm-12 col-xs-12">
										<div class="tile-stats">
											<div class="count" id="total_pendientes"></div>
											<h3>Por Planificar</h3>
										</div>
									</div>
									<div class="animated flipInY col-lg-3 col-md-6 col-sm-12 col-xs-12">
										<div class="tile-stats">
											<div class="count" id="planificadas"></div>
											<h3>Planificadas</h3>
										</div>
									</div>
									<div class="animated flipInY col-lg-3 col-md-6 col-sm-12 col-xs-12">
										<div class="tile-stats">
											<div class="count" id="finalizadas"></div>
											<h3>Finalizadas</h3>
										</div>
									</div>
									<div class="animated flipInY col-lg-3 col-md-6 col-sm-12 col-xs-12">
										<div class="tile-stats">
											<div class="count" id="facturacion"></div>
											<h3>Facturación</h3>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12 px-0">
						<div class="x_panel">
							<div class="x_title">
								<h3 id="titulo_grafico_lineal">PRESUPUESTOS ACEPTADOS DEL AÑO</h3>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<div class="col-md-12 col-sm-12 col-xs-12" style="overflow:hidden;">
									<div class="row tile_count ">
										<div class="col-md-12 col-sm-12 col-xs-12" style="overflow:hidden;">
											<div class="col-md-12" style="overflow:hidden;">
												<canvas height="40%" id="lineChart1"></canvas>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12 px-0">
						<div class="x_panel">
							<div class="x_title">
								<h3 id="titulo_grafico_reparacion">REPARACIONES DEL AÑO</h3>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<div class="col-md-12 col-sm-12 col-xs-12" style="overflow:hidden;">
									<div class="row tile_count ">
										<div class="col-md-12 col-sm-12 col-xs-12" style="overflow:hidden;">
											<div class="col-md-12" style="overflow:hidden;">
												<canvas height="40%" id="lineChart2"></canvas>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- jQuery -->
		<script src="../public/build/js/jquery.min.js"></script>
		<!-- Chart.js -->
		<script src="../public/build/js/Chart.min.js"></script>
		<!-- ECharts -->
		<script src="../public/build/js/echarts.min.js"></script>
		<!-- Custom Theme Scripts -->
		<script src="../public/build/js/custom.js"></script>
		<script src="../public/build/js/utiles.js"></script>
		<script type="text/javascript" src="scripts/dashpresupuestos3.js"></script>
	</body>

</html>
<?php ob_end_flush(); ?>