<?php
require 'header.php';
?>
<div class="right_col" role="main">
    <div class="">

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Curriculum vitae - pagina web</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-tooltip="tooltip" title="Operaciones" role="button" aria-expanded="false"><i class="fa fa-cog"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a id="op_actualizar"><i class="fa fa-refresh"></i> Actualizar</a>
                                    </li>
                                    <li><a id="op_listar" onclick="mostarform(false)"><i class="fa fa-list-alt"></i> Listar</a>
                                    </li>
                                </ul>
                            </li>                     
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div id="listadocontratospagina" class="x_content">

                        <table id="tblcontratospagina" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Opciones</th>
                                    <th>Nombre</th>
                                    <th>Telefono</th>
                                    <th>Email</th>                          
                                    <th>Area</th>
                                    <th>Procesado</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>


                    <div id="formulariocontratospagina" class="x_content">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 <?php
 require 'footer.php';
 ?>
<script type="text/javascript" src="scripts/solnosotros.js"></script>