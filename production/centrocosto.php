<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {

    require 'header.php';

    if ($_SESSION['administrador'] == 1 || $_SESSION['Contabilidad'] == 1) {
        ?>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>CENTRO DE COSTO</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-cog"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a id="op_agregar" onclick="mostarform(true)">Agregar</a>
                                            </li>
                                            <li><a id="op_listar" onclick="mostarform(false)">Listar</a>
                                            </li>
                                        </ul>
                                    </li>                     
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div id="listadocentros" class="x_content">

                                <table id="tblcentros" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Opciones</th>
                                            <th>Codigo</th>
                                            <th>Nombre</th>
                                            <th>Area</th>
                                            <th>Condicion</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>

                            <div id="formulariocentros" class="x_content">
                                <br />
                                <div class="col-md-12 center-margin">
                                    <form class="form-horizontal form-label-left" id="formulario" name="formulario">
                                        <div class="form-group">
                                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Codigo</label>
                                            <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                                                <input type="hidden" id="idcentrocosto" name="idcentrocosto" class="form-control">
                                                <input type="text" class="form-control col-md-4" name="codigo" id="codigo">
                                            </div>

                                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Codigo externo</label>
                                            <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control col-md-4" name="codexterno" id="codexterno">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Nombre</label>
                                            <div class="col-md-10 col-sm-10 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control" name="nombre" id="nombre">
                                            </div>
                                        </div>	
                                        <div class="form-group">
                                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Area</label>
                                            <div class="col-md-10 col-sm-10 col-xs-12 form-group">
                                                <select class="form-control" data-live-search="true" id="tcentrocosto" name="tcentrocosto" required="required"></select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <input type="hidden" class="form-control" name="condicion" id="condicion">
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="ln_solid"></div> 

                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <button class="btn btn-primary" type="button" id="btnCancelar" onclick="cancelarform()">Cancelar</button>
                                                <button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiar()">Limpiar</button>
                                                <button class="btn btn-success" type="submit" id="btnGuardar">Agregar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- /page content -->
        <?php
    } else {
        require 'nopermiso.php';
    }
    require 'footer.php';
    ?>
    <script type="text/javascript" src="scripts/centrocosto.js"></script>
    <?php
}
ob_end_flush();
?>