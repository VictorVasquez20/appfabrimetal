<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {

    require 'header.php';


    if ($_SESSION['administrador'] == 1) {
        ?>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Fase de revisión por tipo centro de costo</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-cog"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a id="op_listar" onclick="mostrarform(false)">Listar</a></li>
                                        </ul>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div id="listadccosto" class="x_content">
                                <table id="tblccosto" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Opciones</th>
                                            <th>Nombre</th>
                                            <th>Fases</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>

                            <div id="formularioccosto" class="x_content" style="display: none;">
                                <br />
                                <form id="formulario" name="formulario" class="form-horizontal form-label-left">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h3>Orden de revisión de solicitudes de adquisición</h3>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-4 col-sm-6 col-xs-12">
                                                <label>Usuario</label>
                                                <select id="iduser" name="iduser" class="form-control selectpicker" data-live-search="true" required=""></select>
                                                <input type="hidden" name="idtcentrocosto" id="idtcentrocosto" >
                                            </div>
                                            <div class="col-md-2 col-sm-6 col-xs-12">
                                                <label>Orden</label>
                                                <input id="orden" name="orden" class="form-control" required="" value="" readonly="">
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <br>
                                                <label>Completa Solicitud  </label>
                                                <input type="checkbox" value="1" name="completasolicitud" id="completasolicitud" class="flat">
                                                <p>Seleccionar esta opción solo para el usuario encargado de adquisiciones o quien este encargado de cotizar.</p>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12 text-right">
                                                <br>
                                                <button class="btn btn-success" type="submit" id="btnGuardar">Agregar</button>
                                                <button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiar()">Limpiar</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h3>Orden de usuarios por revisión</h3>
                                    </div>
                                    <table id="tblfases" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th style="width: 15%;">ORDEN</th>
                                                <th style="width: 60%;">USUARIO</th>
                                                <th style="width: 15%;">COMPLETA SOLICITUD</th>
                                                <th style="width: 10%;">#</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                    
                                    <div class="ln_solid"></div>
                                    <div class="modal-footer">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <button class="btn btn-primary" type="button" id="btnCancelar" onclick="cancelarform()">Volver</button>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
        <?php
    } else {
        require 'nopermiso.php';
    }
    require 'footer.php';
    ?>


    <script type="text/javascript" src="scripts/faseaprobacion.js"></script>

    <?php
}
ob_end_flush();
