<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {

    require 'header.php';

    if ($_SESSION['administrador'] == 1) {
        ?>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>ANÁLISIS DE FALLA - INSTALACIONES</h2>
                                <div class="clearfix"></div>
                            </div>

                            <div id="finservicio" class="x_content">
                                <br />
                                <div class="clearfix"></div>
                                <form id="formulariofin" name="formulariofin" method="post" class="form-horizontal form-label-left" action="../ajax/uploadpres.php" enctype="multipart/form-data">   
                                    <input type="hidden" id="file01" name="file01" class="form-control" value="">
                                    <input type="hidden" id="file02" name="file02" class="form-control" value="">
                                    <input type="hidden" id="file03" name="file03" class="form-control" value=""> 
                                    <input type="hidden" id="attachments" name="attachments" class="form-control" value=""> 
                                    <input type="hidden" id="dTime" name="dTime" class="form-control" value="<?php echo date('YmdHis'); ?>">
                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label>Obra</label>
                                        <input type="hidden" class="form-control" disabled="disabled" name="servicecallID" id="servicecallID">
                                        <input type="hidden" class="form-control" disabled="disabled" name="customerCode" id="customerCode">
                                        <input type="hidden" class="form-control" disabled="disabled" name="itemcode" id="itemcode">
                                        <select class="form-control selectpicker" data-live-search="true" id="obra" name="obra" required="">
                                                </select>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label>TIPO DE FALLA</label>
                                        <select class="form-control selectpicker" data-live-search="true" id="tipofalla" name="tipofalla" required="">
                                                    <option value="" selected disabled>Seleccione Tipo de Falla</option>
                                                    <option value="MECANICA">MECÁNICA</option>
                                                    <option value="ELECTRICA">ELÉCTRICA</option>
                                                    <option value="ELECTRONICA">ELCTRÓNICA</option>
                                                    <option value="LOGISTICA">LOGÍSTICA</option>
                                                    <option value="SUMINISTRO">SUMINISTRO - KONE</option>
                                                    <option value="ESCOTILLA">ESCOTILLA - CIVIL</option>
                                                    <option value="PERSONAL">DE PERSONAL</option>
                                                    <option value="SUPERVISION">SUPERVISIÓN</option>
                                                </select>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label>DESCRIPCION DE LA FALLA</label>
                                        <input type="text" class="form-control col-md-4" name="descripcionFalla" id="descripcionFalla">
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label>CARGO IMPUTABLE A</label>
                                        <select class="form-control selectpicker" data-live-search="true" id="imputable" name="imputable" required="">
                                                    <option value="" selected disabled>Seleccione</option>
                                                    <option value="INSTALACIONES">INSTALACIONES</option>
                                                    <option value="LOGISITCA">LOGÍSTICA</option>
                                                    <option value="COMERCIAL">COMERCIAL</option>
                                                    <option value="CLIENTE">CLIENTE</option>
                                                    <option value="GARANTIA">GARANTÍA DE EQUIPOS - KONE</option>
                                                </select>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label>REQUERIMIENTO</label>
                                        <select class="form-control selectpicker" data-live-search="true" id="requerimiento" name="requerimiento" required="">
                                                    <option value="" selected disabled>Seleccione Requerimiento</option>
                                                    <option value="SOLCOMPONENTE">SOLICITUD DE COMPONENTE</option>
                                                    <option value="REPCOMPONENTE">REPARACIÓN DE COMPONENTE</option>
                                                    <option value="SOLPERSONAL">SOLICITUD DE PERSONAL</option>
                                                    <option value="EXTERNO">CONTRATACIÓN DE EXTERNO</option>
                                                    <option value="REINSTALACION">RE-INSTALACIÓN</option>
                                                    <option value="OTRO">OTRO</option>
                                                </select>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label>DESCRIPCION DEL REQUERIMIENTO</label>
                                        <input type="text" class="form-control col-md-4" name="descRequerimiento" id="descRequerimiento">
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label>RESPONSABLE<span class="required">*</span></label>
                                        <select class="form-control selectpicker" data-live-search="true" id="responsable" name="responsable">
                                                </select>
                                    </div>  
                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group" id="zonaUpload" >
                                        <label for="descripcion2">Imagenes (máx. 3)</label>
                                        <div class="dropzone" id="myDropzone"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 left-margin">
                                            
                                            <button class="btn btn-success" type="submit" id="btnFinalizar">Enviar</button>
                                        </div>
                                    </div>

                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- /page content -->
        <?php
    } else {
        require 'nopermiso.php';
    }
    require 'footer.php';
    ?>
    <link rel="stylesheet" type="text/css" href="../public/build/css/dropzone.css">
    <link rel="stylesheet" type="text/css" href="../public/build/css/component-dropzone.css">
    <script type="text/javascript" src="../public/build/js/instascan.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
    <script src="../public/build/js/dropzone.js" type="text/javascript"></script>
    <script type="text/javascript" src="scripts/analisisfalla.js"></script>
    <script>
      var uploadOK = false;
      // Dropzone.autoDiscover = false;
      Dropzone.prototype.defaultOptions.dictDefaultMessage = "<b style=\"color:black\">ARRASTRA O SELECCIONA TUS ARCHIVOS AQUÍ</b>";
      Dropzone.prototype.defaultOptions.dictFallbackMessage = "Su navegador no admite la carga de archivos de arrastrar y soltar.";
      Dropzone.prototype.defaultOptions.dictFallbackText = "Utilice el formulario de reserva a continuación para cargar sus archivos como en los viejos tiempos.";
      Dropzone.prototype.defaultOptions.dictFileTooBig = "El archivo es demasiado grande ({{filesize}} MiB). Tamaño máximo de archivo: {{maxFilesize}} MiB.";
      Dropzone.prototype.defaultOptions.dictInvalidFileType = "No puedes subir archivos de este tipo.";
      Dropzone.prototype.defaultOptions.dictResponseError = "El servidor respondió con el código {{statusCode}}.";
      Dropzone.prototype.defaultOptions.dictCancelUpload = "Cancelar Subida";
      Dropzone.prototype.defaultOptions.dictCancelUploadConfirmation = "¿Estás seguro de que deseas cancelar esta carga?";
      Dropzone.prototype.defaultOptions.dictRemoveFile = "Quitar Archivo";
      Dropzone.prototype.defaultOptions.dictMaxFilesExceeded = "No puedes subir mas archivos.";
      // Dropzone.autoDiscover = false;
      Dropzone.options.myDropzone= {
        url: $('#formulariofin').attr('action'),
        autoProcessQueue: false,
        uploadMultiple: true,
        parallelUploads: 3,
        maxFiles: 3,
        // autoQueue: true,
        // autoProcessQueue: true,
        //maxFilesize: 10,
        acceptedFiles: 'image/*',
        addRemoveLinks: true,
        init: function() {
          dzClosure = this;
          
          document.getElementById("btnFinalizar").addEventListener("click", function(e) {
            e.preventDefault();
            e.stopPropagation();
            
                if(dzClosure.files.length){
                    dzClosure.processQueue();
                    if (uploadOK)
                        $('form#formulariofin').submit();

                }
                else{
                    if (!$("#formulariofin")[0].checkValidity())
                        $("#formulariofin")[0].reportValidity();
                    else
                    {
                        if (!$("#file01").val() && !$("#file02").val() && !$("#file03").val())
                            {alert('Debe subir al menos una imagen.');}
                        else
                            {
                                alert("aasd");
                                guardaryeditar();}
                    }
                }
            
          });

          //// Envia todo el fomulario con las imagenes incluidas:
          this.on("sendingmultiple", function(data, xhr, formData) {
            var formData2 = new FormData($('#formulariofin')[0]);
            var poData = jQuery($('#formulariofin')[0]).serializeArray();
            for (var i=0; i<poData.length; i++){
              formData.append(poData[i].name, poData[i].value);
            }
          });

          //// Envia todo el fomulario con las imagenes incluidas:
          // this.on("success", function(data, xhr, formData) {
          this.on("success", function(data,xhr, formData) {
            //console.log('CADA UNO');
            //console.log(xhr);
            obj = JSON.parse(xhr);
            console.log(obj.id);
            $("#attachments").val(obj.id);
            //console.log(data.upload.filename);
            if (!$('#file01').val()) {
                $('#file01').val(data.upload.filename);
                return;
            }
            if (!$('#file02').val()) {
                $('#file02').val(data.upload.filename);
                return;
            }
            if (!$('#file03').val()) {
                $('#file03').val(data.upload.filename);
                return;
            }
            
          });

          //// cuando se termino de procesar todas las imagenes
          // this.on("queuecomplete", function(data, xhr, formData) {
          this.on("queuecomplete", function(xhr) {
            // alert('TODO SUBIDO');
            uploadOK = true;
            console.log(xhr);
            $('form#formulariofin').submit();
          });

          //Si se elimina el archivo por dropzone se deja en blanco
          //la variable oculta que contenia el nombre del archivo
          this.on("removedfile", function(file) {
            var name = file.name;
            if ($('#file01').val() == name)
                $('#file01').val('');
            if ($('#file02').val() == name)
                $('#file02').val('');
            if ($('#file03').val() == name)
                $('#file03').val('');
          });
        }
      }
    </script>
    <?php
}
ob_end_flush();
?>



