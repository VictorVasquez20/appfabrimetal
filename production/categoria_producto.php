<?php
ob_start();
session_start();

if(!isset($_SESSION["nombre"])){
    header("Location:login.php");
}else{

require 'header.php';

if( $_SESSION['administrador']==1)
{

 ?>
        
<!-- page content -->
<div class="right_col" role="main">
    <div class="">

        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Categoria de Productos</h2>
                        <div class="clearfix"></div>
                       <button id="op_agregar" name="op_agregar" onclick="mostrarform(true)" class="btn btn-primary">Agregar</button>
                    </div>
                    <div id="listadocategorias" class="x_content">

                        <table id="tblcategorias" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Opciones</th>
                                    <th>Nombre</th>
                                    <th>Descripcion</th>
                                    <th>Vigencia</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                    <div id="formulariocategorias" style="display:none;" class="x_content">
                        <br />
                        <form id="formulario" name="formulario" class="form-horizontal form-label-left input_mask">
                            
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12">Nombre</label>
                                <div class="col-md-10 col-sm-10 col-xs-12">
                                    <input type="hidden" name="idcategoria" id="idcategoria">
                                    <input type="text" class="form-control has-feedback-left" style="text-transform:uppercase;" id="nombre" name="nombre" placeholder="Nombre">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12">Descripcion</label>
                                <div class="col-md-10 col-sm-10 col-xs-12">
                                    <input type="text" class="form-control has-feedback-left" style="text-transform:uppercase;" id="descripcion" name="descripcion" placeholder="Nombres">  
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12">Vigencia</label>
                                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                                    <div class="">
                                        <label>
                                            <input name="vigencia" id="vigencia" type="checkbox" class="icheckbox_flat-green" value="1"/>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <button class="btn btn-primary" type="button" id="btnCancelar" onclick="cancelarform()">Cancelar</button>
                              <button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiar()">Limpiar</button>
                              <button class="btn btn-success" type="submit" id="btnGuardar">Guardar</button>
                            </div>
                          </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
        <!-- /page content -->
<?php 
}else{
  require 'nopermiso.php';
}
require 'footer.php';
?>
<script>
    $('#myDatepicker2').datetimepicker({
        format: 'DD/MM/YYYY'
    });
</script>
<script type="text/javascript" src="scripts/categoria_producto.js"></script>
<?php 
}
ob_end_flush();
