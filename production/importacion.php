<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {

    require 'header.php';

    if ($_SESSION['administrador'] == 1 || $_SESSION['Comex'] == 1) {
        ?>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>IMPORTACIÓN</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-cog"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a id="op_agregar" onclick="mostarform(true)">Agregar</a>
                                            </li>
                                            <li><a id="op_listar" onclick="mostarform(false)">Listar</a>
                                            </li>
                                        </ul>
                                    </li>                     
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div id="listadoimportacion" class="x_content">

                                <table id="tblimportacion" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Opciones</th>
                                            <th>Codigo</th>
                                            <th>Nef</th>
                                            <th>proveedor</th>
                                            <th>Despacho</th>
                                            <th>llegada</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>

                            <div id="formularioimportacion" class="x_content" style="display: false;">
                                <br />
                                <div class="col-md-12 center-margin">
                                    <form class="form-horizontal form-label-left" id="formulario" name="formulario">
                                        <div class="form-group">
                                            <label class="control-label col-md-2 col-sm-2 col-xs-12">CODIGO</label>
                                            <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                                                <input type="hidden" id="idimportacion" name="idimportacion" class="form-control">
                                                <input type="text" class="form-control col-md-4" name="codigo" id="codigo" required>
                                            </div>

                                            <label class="control-label col-md-2 col-sm-2 col-xs-12">NEF</label>
                                            <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control col-md-4" name="nef" id="nef" required="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-2 col-sm-2 col-xs-12">PROVEEDOR</label>
                                            <div class="col-md-10 col-sm-10 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control" name="proveedor" id="proveedor" style='text-transform:uppercase;' required>
                                            </div>
                                        </div>	       
                                        
                                        <div class="form-group">
                                            <label class="control-label col-md-2 col-sm-2 col-xs-12">DESPACHO</label>
                                            <div class="col-md-4 col-sm-4 col-xs-4 form-group has-feedback">
                                                <input type="text" class="form-control" name="despacho" id="despacho" readonly="">
                                            </div>
                                            
                                            <label class="control-label col-md-2 col-sm-2 col-xs-12">LLEGADA</label>
                                            <div class="col-md-4 col-sm-4 col-xs-4 form-group has-feedback">
                                                <input type="text" class="form-control" name="llegada" id="llegada" readonly="">
                                            </div>
                                        </div>	
                                        
                                        <div class="clearfix"></div>
                                        <div class="ln_solid"></div> 

                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <button class="btn btn-primary" type="button" id="btnCancelar" onclick="cancelarform()">Cancelar</button>
                                                <button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiar()">Limpiar</button>
                                                <button class="btn btn-success" type="submit" id="btnGuardar">Agregar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- /page content -->
        <?php
    } else {
        require 'nopermiso.php';
    }
    require 'footer.php';
    ?>
    <script type="text/javascript" src="scripts/importacion.js"></script>
    <?php
}
ob_end_flush();
