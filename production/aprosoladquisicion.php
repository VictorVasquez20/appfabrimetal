<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {
    require 'header.php';
    ?>

    <div class="right_col" role="main">
        <div class="">
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>APROBACIÓN SOLICITUD DE ADQUISICIONES</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-tooltip="tooltip" title="Operaciones" role="button" aria-expanded="false"><i class="fa fa-cog"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <!--<li><a id="op_agregar" onclick="mostrarform(true)"><i class="fa fa-list-alt"></i> Agregar</a></li>-->
                                        <li><a id="op_listar" onclick="mostrarform(false)"><i class="fa fa-list-alt"></i> Listar</a></li>
                                    </ul>
                                </li>                     
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div id="listadsolicitudes" class="x_content">
                            <table id="tblguias" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>C. COSTO</th>
                                        <th>NRO.</th>
                                        <th>FECHA</th>
                                        <th>PROVEEDOR</th>
                                        <th>ESTADO</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>

                        <div id="formsolicitudes" class="x_content" style="display: none ;">

                            <div class="x_content">
                                <div class="col-xs-2">
                                    <ul class="nav nav-tabs tabs-left">
                                        <li class="active"><a href="#home" data-toggle="tab">Revisión Solicitud</a></li>
                                        <li><a href="#profile" data-toggle="tab">Observaciones</a></li>
                                        <li><a href="#settings" data-toggle="tab">Archivos Adjuntos</a></li>
                                        <li>
                                            <p>
                                            <ul class="to_do">
                                                <li class="check-mark" id="nroarchivos"></li>
                                            </ul>
                                            </p>
                                        </li>
                                    </ul>
                                </div>

                                <div class="col-xs-10">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="home">
                                            <ul class="stats-overview">
                                                <li>
                                                    <span class="name"> N° Solicitud </span>
                                                    <span class="value text-success" id="ver_numero"> </span>
                                                </li>
                                                <li>
                                                    <span class="name"> Total </span>
                                                    <span class="value text-success" id="ver_total"> </span>
                                                </li>
                                                <li class="hidden-phone">
                                                    <span class="name"> Prioridad </span>
                                                    <span class="value text-success"><span class="label label-success">Normal</span></span>
                                                </li>
                                            </ul>
                                            <div class="x_panel">
                                                <div class="col-sm-6 invoice-col">
                                                    <ul class="bs-glyphicons-list">
                                                        <li>
                                                            <h5><b>Solicitante: </b></h5><p id="ver_solicita" name="tabcontrato">Aaron Zuñiga</p>
                                                        </li> 
                                                        <li>
                                                            <h5><b>Centro de costo: </b></h5><p id="ver_ccosto" name="tabedificio">KM81881 - CENTRO COSTO</p>
                                                        </li> 
                                                        <li>
                                                            <h5><b>Condicion de pago: </b></h5><p id="ver_condpago" name="tabtipo">30 días</p>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-sm-6 invoice-col">
                                                    <ul class="bs-glyphicons-list">
                                                        <li>
                                                            <h5><b>Fecha Solicitud: </b></h5><p id="ver_fecha" name="tabcodigo">17/07/2019</p>
                                                        </li>
                                                        <li>
                                                            <h5><b>Proveedor: </b></h5><p id="ver_proveedor" name="tabcliente">PC FACTORY</p>
                                                        </li>
                                                        <li>
                                                            <h5><b>Moneda: </b></h5><p id="ver_moneda" name="tabmarca">Euro</p>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="x_panel">
                                                <div class="x_title">DETALLE</div>
                                                <table id="ver_detalle" class="table table-bordered" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>PRODUCTO</th>
                                                            <th>CANTIDAD</th>
                                                            <th>VALOR</th>
                                                            <th>TOTAL</th>
                                                            <th>OBSERVACIÓN</th>
                                                            <!--<th>#</th>-->
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td colspan="4" class="text-right"><b>SUBTOTAL</b></td>
                                                            <td id="totdet"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4" class="text-right"><b>NETO</b></td>
                                                            <td id="neto"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4" class="text-right"><b>IVA 19%</b></td>
                                                            <td id="iva"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4" class="text-right"><b>TOTAL</b></td>
                                                            <td id="total"></td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                            <div class="x_panel well">
                                                <form class="form-horizontal form-label-left" id="formaprueba" name="formaprueba">
                                                    <div class="x_content well">
                                                        <div class="x_title"><h3>FORMULARIO REVISIÓN</h3></div>
                                                        <div class="form-group">
                                                            <label>Observación</label>
                                                            <textarea id="observacion" name="observacion" class="form-control" required="Capo requerido"></textarea>
                                                            <input type="hidden" name="idsolicitud" id="idsolicitud">
                                                            <input type="hidden" name="fase" id="fase">
                                                            <input type="hidden" name="archivos" id="archivos">
                                                            <input type="hidden" name="completaadquisicion" id="completaadquisicion">
                                                            <input type="hidden" name="fasefinal" id="fasefinal">
                                                        </div>
                                                        <div class="modal-footer">
                                                            <div class="col-md-12 col-sm-12 col-xs-12 ">
                                                                <button class="btn btn-danger" type="button" id="btnLimpiar" onclick="rechazar();">Rechazar</button>
                                                                <button class="btn btn-success" type="submit" id="btnGuardar">Aprobar</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>

                                        </div>
                                        <div class="tab-pane" id="profile">
                                            <ul class="messages" id="ver_observaciones">
                                            </ul>
                                        </div>
                                        <div class="tab-pane" id="settings">
                                            <div class="x_panel">
                                                <div class="x_title">ARCHIVOS ADJUTNOS</div>
                                                <div class="x_panel" id="formulariocotizaciones">
                                                    <div class="x_title">Adjuntar nueva cotización</div>
                                                    <form class="form-horizontal form-label-left" id="formcotizacion" name="formcotizacion">
                                                        
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <label class="control-label">PROVEEDOR<span class="required">*</span></label>
                                                            <select class="selectpicker select2_single form-control" id="proveedor" name="proveedor" data-live-search="true" required="required"></select>
                                                        </div>
                                                        
                                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                                            <label class="control-label">MONEDA</label>
                                                            <select class="form-control selectpicker" id="moneda" name="moneda" data-live-search="true" required=""></select>
                                                        </div>
                                                        <div class="col-md-3 col-sm-3 col-xs-12 form-group">
                                                            <div class="col-md-11 col-sm-11 col-xs-12 form-group">
                                                                <label class="control-label">CONDICION DE PAGO</label>
                                                                <select class="selectpicker form-control" id="condpago" name="condpago" data-live-search="true" required="required"></select>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <label class="control-label">COTIZACIÓN<span class="required">*</span></label>
                                                            <input type="file" class="file-input" id="archivo" name="archivo">
                                                            <input type="hidden" class="form-control" id="anterior" name="anterior">
                                                            <input type="hidden" class="form-control" id="idcotizacion" name="idcotizacion">
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12 ">
                                                             <br>
                                                            <button class="btn btn-info" type="submit" id="btnGuardar2">Subir cotización</button>
                                                        </div>
                                                    </form>
                                                </div>
                                                
                                                                                                
                                                <div class="attachment">
                                                    <ul id="ver_archivos">
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="modal-footer">
                                    <div class="col-md-12 col-sm-12 col-xs-12 ">
                                        <button class="btn btn-primary" type="button" id="btnCancelar" onclick="mostrarform(false); mostrarvista(false);">Volver</button>

                                        <!--<button class="btn btn-round btn-success" type="button" id="">ENVIAR A REVISIÓN</button>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php require 'footer.php'; ?>

    <script type="text/javascript" src="scripts/aprosoladquisicion.js"></script>

    <?php
}
ob_end_flush();
