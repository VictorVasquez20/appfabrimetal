<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {

    require 'header.php';

    if ($_SESSION['administrador'] == 1 || $_SESSION['GReparacion'] == 1 || $_SESSION['AReparacion'] == 1 ||  $_SESSION['GEServicios'] == 1) {
?>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>REPARACIONES</h2>
                                <a href="reparacioncalendario.php" class="navbar-right panel_toolbox btn btn-info"><i class="fa fa-calendar"></i> Calendario</a>
                                <div class="clearfix"></div>
                            </div>
                            <div id="listadorep" class="x_content">

                                <table id="tblrep" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>SOLICITUD</th>
                                            <th>PRESUPUESTO</th>
                                            <th>EDIFICIO</th>                                            
                                            <th>EQUIPO</th>
                                            <th>ESTADO EQUIPO</th>
                                            <th>TIPO</th>
                                            <th>ACTUALIZADO</th>
                                            <th>ESTADO</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="modalPendientes" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">
                                                <span aria-hidden="true">&times;</span>
                                                <span class="sr-only">Close</span>
                                            </button>
                                            <h4 class="modal-title" id="myModalLabel">SOLICITUD PENDIENTE</h4>
                                        </div>

                                        <form role="form" id="formpendiente" name="formpendiente">
                                            <!-- Modal Body -->
                                            <div class="modal-body">
                                                <p class="statusMsg"></p>
                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <input type="hidden" id="idreparacion" name="idreparacion">
                                                    <label>MOTIVO <span class="required">*</span></label>
                                                    <select class="form-control selectpicker" data-live-search="true" id="idMotivoPendiente" name="idMotivoPendiente" required="Campo requerido">
                                                    </select>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <label>COMENTARIOS<span class="required">*</span></label>
                                                    <textarea type="text" id="comentarioPendiente" name="comentarioPendiente" class="resizable_textarea form-control" style="text-transform: uppercase;" required="Campo requerido"></textarea>
                                                </div>

                                            </div>
                                            <div class="clearfix"></div>

                                            <!-- Modal Footer -->
                                            <div class="modal-footer">
                                                <button type="reset" onclick="limpiarpendiente()" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                <button type="submit" class="btn btn-primary submitBtn" id="btnGuardarPendiente">Guardar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="modalPlanificacion" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">
                                                <span aria-hidden="true">&times;</span>
                                                <span class="sr-only">Close</span>
                                            </button>
                                            <h4 class="modal-title" id="myModalLabel">SOLICITUD A PLANIFICAR</h4>
                                        </div>

                                        <form role="form" id="formProgra" name="formProgra">
                                            <!-- Modal Body -->
                                            <div class="modal-body">
                                                <p class="statusMsg"></p>
                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <input type="hidden" id="idreparacionProgra" name="idreparacionProgra">
                                                    <label>RESPONSABLE <span class="required">*</span></label>
                                                    <select class="form-control selectpicker" data-live-search="true" id="idResponsable" name="idResponsable" required="Campo requerido">
                                                    </select>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <label>PRIORIDAD <span class="required">*</span></label>
                                                    <select class="form-control selectpicker" data-live-search="true" id="idPrioridad" name="idPrioridad" required="Campo requerido">
                                                        <option value="" disabled selected>SELECCIONE</option>
                                                        <option value="1">ALTA</option>
                                                        <option value="2">MEDIA</option>
                                                        <option value="3">BAJA</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <label>FECHA PLANIFICADA<span class="required">*</span></label>
                                                    <input class="form-control selectpicker" type="date" id="inicioProgra" name="inicioProgra">
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <label>COMENTARIO<span class="required">*</span></label>
                                                    <textarea type="text" id="comentarioProgra" name="comentarioProgra" class="resizable_textarea form-control" style="text-transform: uppercase;" required="Campo requerido"></textarea>
                                                </div>

                                            </div>
                                            <div class="clearfix"></div>

                                            <!-- Modal Footer -->
                                            <div class="modal-footer">
                                                <button type="reset" onclick="limpiarprogra()" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                <button type="submit" class="btn btn-primary submitBtn" id="btnGuardarProgra">Guardar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="modalEjecutado" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">
                                                <span aria-hidden="true">&times;</span>
                                                <span class="sr-only">Close</span>
                                            </button>
                                            <h4 class="modal-title" id="myModalLabel">SOLICITUD EJECUTADA</h4>
                                        </div>

                                        <form role="form" id="formEjecuta" name="formEjecuta">
                                            <!-- Modal Body -->
                                            <div class="modal-body">
                                                <p class="statusMsg"></p>
                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <input type="hidden" id="idreparacionEjec" name="idreparacionEjec">
                                                    <label>¿REPARACIÓN COMPLETA? <span class="required">*</span></label>
                                                    <select class="form-control selectpicker" data-live-search="true" id="idcompleto" name="idcompleto" required="Campo requerido">
                                                        <option value="" selected disabled>SELECCIONE</option>
                                                        <option value="1">SI</option>
                                                        <option value="0">NO</option>
                                                    </select>
                                                </div>
                                                <div id="divcomentario" name="divcomentario" class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <label>COMENTARIO<span class="required">*</span></label>
                                                    <textarea type="text" id="comentarioEjec" name="comentarioEjec" class="resizable_textarea form-control" style="text-transform: uppercase;" required="Campo requerido"></textarea>
                                                </div>

                                            </div>
                                            <div class="clearfix"></div>

                                            <!-- Modal Footer -->
                                            <div class="modal-footer">
                                                <button type="reset" onclick="limpiarejec()" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                <button type="submit" class="btn btn-primary submitBtn" id="btnGuardarEjec">Guardar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="modalTipo" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">
                                                <span aria-hidden="true">&times;</span>
                                                <span class="sr-only">Close</span>
                                            </button>
                                            <h4 class="modal-title" id="myModalLabel">TIPO DE REPARACIÓN</h4>
                                        </div>

                                        <form role="form" id="formTipo" name="formTipo">
                                            <!-- Modal Body -->
                                            <div class="modal-body">
                                                <p class="statusMsg"></p>
                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <input type="hidden" id="idreparacionTipo" name="idreparacionTipo">
                                                    <label>INGRESE TIPO DE REPARACIÓN <span class="required">*</span></label>
                                                    <select class="form-control selectpicker" data-live-search="true" id="idTipoRep" name="idTipoRep" required="Campo requerido">
                                                        <option value="" selected disabled>SELECCIONE</option>
                                                        <option value="1">MENOR</option>
                                                        <option value="2">MAYOR</option>
                                                        <option value="3">MIXTA</option>
                                                    </select>
                                                </div>

                                            </div>
                                            <div class="clearfix"></div>

                                            <!-- Modal Footer -->
                                            <div class="modal-footer">
                                                <button type="reset" onclick="limpiarejec()" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                <button type="submit" class="btn btn-primary submitBtn" id="btnGuardarTipo">Guardar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="modalFact" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">
                                                <span aria-hidden="true">&times;</span>
                                                <span class="sr-only">Close</span>
                                            </button>
                                            <h4 class="modal-title" id="myModalLabel">SOLICITUD ENVIADA A FACTURACIÓN</h4>
                                        </div>

                                        <form role="form" id="formFact" name="formFact">
                                            <!-- Modal Body -->
                                            <div class="modal-body">
                                                <p class="statusMsg"></p>
                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <input type="hidden" id="idreparacionFact" name="idreparacionFact">
                                                    <label>¿FACTURACIÓN COMPLETA? <span class="required">*</span></label>
                                                    <select class="form-control selectpicker" data-live-search="true" id="idFact" name="idFact" required="Campo requerido">
                                                        <option value="" selected disabled>SELECCIONE</option>
                                                        <option value="6">SI</option>
                                                        <option value="5">NO</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <label>COMENTARIO<span class="required">*</span></label>
                                                    <textarea type="text" id="comentarioFact" name="comentarioFact" class="resizable_textarea form-control" style="text-transform: uppercase;" required="Campo requerido"></textarea>
                                                </div>

                                            </div>
                                            <div class="clearfix"></div>

                                            <!-- Modal Footer -->
                                            <div class="modal-footer">
                                                <button type="reset" onclick="limpiarFact()" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                <button type="submit" class="btn btn-primary submitBtn" id="btnGuardarFact">Guardar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="modalHistorico" role="dialog" style="min-width:800px !important;">
                                <div class="modal-dialog" style="min-width:1000px !important;">
                                    <div class="modal-content" >
                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">
                                                <span aria-hidden="true">&times;</span>
                                                <span class="sr-only">Close</span>
                                            </button>
                                            <h4 class="modal-title" id="myModalLabel">HISTORIAL DE SOLICITUD</h4>
                                        </div>

                                        <form role="form" id="formhistorico" name="formhistorico">
                                            <!-- Modal Body -->
                                            <div class="modal-body">
                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <table id="tblhist" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th>USUARIO</th>
                                                                <th>FECHA ACTUALIZACIÓN</th>
                                                                <th>ESTADO</th>
                                                                <th>COMENTARIO</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="modalPlan" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">
                                                <span aria-hidden="true">&times;</span>
                                                <span class="sr-only">Close</span>
                                            </button>
                                            <h4 class="modal-title" id="myModalLabel">NUEVA SOLICITUD</h4>
                                        </div>

                                        <form role="form" id="formPlan" name="formPlan">
                                            <!-- Modal Body -->
                                            <div class="modal-body">
                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <input type="hidden" id="idreparacionPlan" name="idreparacionPlan">
                                                    <p>AL MOMENTO DE GUARDAR, ESTA SOLICITUD PASARÁ AL ESTADO "POR PLANIFICAR"</p>
                                                    <label>COMENTARIO<span class="required">*</span></label>
                                                    <textarea type="text" id="comentarioPlan" name="comentarioPlan" class="resizable_textarea form-control" style="text-transform: uppercase;" required="Campo requerido"></textarea>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <!-- Modal Footer -->
                                            <div class="modal-footer">
                                                <button type="reset" onclick="limpiarplan()" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                <button type="submit" class="btn btn-primary submitBtn" id="btnGuardarPlan">Guardar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="modal fade" id="modalDocumentos" role="dialog">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span class="sr-only">Close</span></button>
                                            <h4 class="modal-title text-center" id="myModalLabel">DOCUMENTOS</h4>
                                        </div>

                                        <div class="row p-5" id="dibujar">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal fade" id="modalPreview" role="dialog">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content" id="contenido">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- /page content -->

        <!-- /page content -->
    <?php
    } else {
        require 'nopermiso.php';
    }
    require 'footer.php';
    ?>
    <script src="../public/build/js/libs/png_support/png.js"></script>
    <script src="../public/build/js/libs/png_support/zlib.js"></script>
    <script src="../public/build/js/jspdf.debug.js"></script>
    <script src="../public/build/js/jspdf.plugin.autotable.js"></script>
    <script src="../public/build/js/jsPDFcenter.js"></script>
    <script type="text/javascript" src="scripts/reparacion.js"></script>

    <style type="text/css">
        

        #tblhist td {
            white-space: inherit;
        }
    </style>
<?php
}
ob_end_flush();
?>