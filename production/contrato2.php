<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {

    require 'header.php';

    if ($_SESSION['administrador'] == 1 || $_SESSION['Contratos'] == 1) {
        ?>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Contratos</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-tooltip="tooltip" title="Operaciones" role="button" aria-expanded="false"><i class="fa fa-cog"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a id="op_agregar" onclick="mostarform(true)"><i class="fa fa-plus"></i> Agregar</a>
                                            </li>
                                            <li><a id="op_listar" onclick="mostarform(false)"><i class="fa fa-list-alt"></i> Listar</a>
                                            </li>
                                        </ul>
                                    </li>                     
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div id="listadocontratos" class="x_content">

                                <table id="tblcontratos" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>OPCIONES</th>
                                            <th>ID CONTRATO</th>
                                            <th>FECHA</th>
                                            <th>TIPO</th>
                                            <th>CLIENTE</th>
                                            <th>UBICACION</th>                                                                                               
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>


                            <div id="formulariocontratos" class="x_content">
                                <br />
                                <div class="clearfix"></div>
                                <form id="formulario1" name="formulario1" method="post" class="form-horizontal form-label-left">
                                    <h4><b>DATOS DEL CONTRATO</b></h4>  
                                    <div class="row"> 
                                        <div class="col-md-11 col-sm-12 col-xs-10 form-group">
                                            <select class="form-control selectpicker" data-live-search="true" id="idcontrato" name="idcontrato" required="required"></select>
                                        </div>
                                        <div class="col-md-1 col-sm-12 col-xs-12 form-group">
                                            <button class="btn btn-info btn-sm" type="button" data-toggle="modal" data-target="#modalContrato"><i class="fa fa-plus-circle"></i></button>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="ln_solid"></div> 
                                    <h4><b>DATOS DEL EDIFICIO Y EQUIPOS</b></h4>  
                                    <div class="row"> 
                                        <div class="col-md-11 col-sm-12 col-xs-10 form-group">
                                            <select class="form-control selectpicker" data-live-search="true" id="idedificio" name="idedificio" required="required"></select>
                                            <input type="hidden" id="region" name="region">
                                        </div>
                                        <div class="col-md-1 col-sm-12 col-xs-12 form-group">
                                            <button class="btn btn-info btn-sm" type="button" data-toggle="modal" data-target="#modalEdificio"><i class="fa fa-plus-circle"></i></button>
                                        </div>
                                        <div class="clearfix"></div><div class="ln_solid"></div>
                                        <div class="col-md-12 col-sm-12 col-xs-10 form-group" id="ascensores" name="ascensores" style="display: none;">
                                            <div class="x_panel">
                                                <div id="" name="">
                                                    <h4><b>Ascensor:</b></h4>
                                                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                                        <input type="hidden" name="idascensor" id="idascensor">
                                                        <label for="idtascensor">Tipo de Ascensor <span class="required">*</span></label>
                                                        <select class="form-control selectpicker" data-live-search="true" id="idtascensor" name="idtascensor" required="requerido"></select>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                                        <label for="marca">Marca <span class="required">*</span></label>
                                                        <div class="row">
                                                            <div class="col-md-10 col-sm-12 col-xs-12 form-group">
                                                                <select class="form-control selectpicker" data-live-search="true" id="marca" name="marca" required="" onchange="selectmodelo()"></select>
                                                            </div>
                                                            <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                                                                <button class="btn btn-warning btn-sm" type="button" onclick="actualizar();"><i class="fa fa-refresh"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                                        <label for="modelo">Modelo <span class="required">*</span></label>
                                                        <div class="row">
                                                            <div class="col-md-10 col-sm-12 col-xs-12 form-group">
                                                                <select class="form-control selectpicker" data-live-search="true" id="modelo" name="modelo" required="">
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                                                                <button class="btn btn-warning btn-sm" type="button" onclick="actualizar();"><i class="fa fa-refresh"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                                        <label for="modelo">Codigo <span class="required">(si queda vacio toma uno automático)</span></label>
                                                        <input type="text" id="codigo" name="codigo" class="form-control" onblur="VerCodigo('');" data-inputmask="'mask' : 'FM999999'" >
                                                    </div>

                                                    <div class="clearfix"></div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                                        <label for="ken">Codigo Cliente</label>
                                                        <input type="text" id="codigocli" name="codigocli" class="form-control">
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                                        <label for="ken">Codigo Fabricante</label>
                                                        <input type="text" id="ken" name="ken" class="form-control" data-inputmask="'mask': '99999999'">
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                                        <label for="ken">Ubicacion</label>
                                                        <input type="text" id="ubicacionasc" name="ubicacionasc" class="form-control">
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                                        <label for="paradas">Paradas</label>
                                                        <input type="text" id="paradas" name="paradas" class="form-control" required="">
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                                        <label for="capkg">Capacidad (Kg)</label>
                                                        <input type="text" id="capkg" name="capkg" class="form-control" required="">
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                                        <label for="capper">Capacidad Personas </label>
                                                        <input type="text" id="capper" name="capper" class="form-control" required="">
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                                        <label for="velocidad">Velocidad </label>
                                                        <input type="text" id="velocidad" name="velocidad" class="form-control" required="" data-inputmask="'mask': '9,99'">
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                                        <label for="pservicio">Puesta Servicio</label>
                                                        <input type="date" id="pservicio" name="pservicio" class="form-control"/>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                                        <label for="gtecnica">Garantia Tecnica</label>
                                                        <input type="date" id="gtecnica" name="gtecnica" class="form-control"/>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                                        <input type="hidden" class="form-control" id="dcs" name="dcs" value="">
                                                        
                                                        <!--<label for="dcs">Dcs <span class="required">*</span></label>
                                                        <select class="form-control selectpicker" data-live-search="true" id="dcs" name="dcs" required="required">
                                                            <option value="" selected disabled>Dcs</option>
                                                            <option value="1">SI</option>
                                                            <option value="0">NO</option>
                                                        </select>-->
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                                        <input type="hidden" class="form-control" id="elink" name="elink" value="">
                                                        
                                                        <!--<label for="elink">Elink <span class="required">*</span></label>
                                                        <select class="form-control selectpicker" data-live-search="true" id="elink" name="elink" required="required">
                                                            <option value="" selected disabled>Elink</option>
                                                            <option value="1">SI</option>
                                                            <option value="0">NO</option>
                                                        </select>-->
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                                        <label>&nbsp;</label><br>
                                                        <button type="submit" name="addasc" id="addasc" class="btn btn-success"><i class="fa fa-plus-circle"></i> Agregar</button>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div><div class="ln_solid"></div>

                                                <table class="table table-bordered" id="tblasc" name="tblasc">
                                                    <thead>
                                                        <tr>
                                                            <th>NUM</th>
                                                            <th>TIPO</th>
                                                            <th>MARCA</th>
                                                            <th>CODIGO</th>
                                                            <th>PARADAS</th>
                                                            <th>CAPACIDAD (KG)</th>
                                                            <th>VELOCIDAD</th>
                                                            <th>#</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>

                                            </div>
                                        </div>

                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 left-margin">
                                            <button class="btn btn-primary" type="button" id="btnCancelar"  onclick="cancelarform()">Volver</button>
                                            <!--<button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiar()">Limpiar</button>
                                            <button class="btn btn-success" type="submit" id="btnGuardar">Agregar</button>-->
                                        </div>
                                    </div>

                                </form>
                            </div>

                            <div id="formeditarcontrato" class="x_content">
                                <br />
                                <div class="clearfix"></div>
                                <form id="formulariocontrato" name="formulariocontrato" method="post" class="form-horizontal form-label-left">
                                    <h4><b>Datos del Contrato</b></h4>  
                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label for="ftcontrato">Tipo de contrato <span class="required">*</span></label>
                                            <input type="hidden" id="fidcontrato" name="fidcontrato" class="form-control">
                                            <select class="form-control selectpicker" data-live-search="true" id="ftcontrato" name="ftcontrato" required="required"></select>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label for="ncontrato">Numero de contrato <span class="required">*</span></label>
                                            <input type="text" id="fncontrato" name="fncontrato" required="required" class="form-control">
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label for="nexterno">Identicacion contrato cliente</label>
                                            <input type="text" id="fnexterno" name="fnexterno" class="form-control">
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label for="nreferencia">Referencia tipo de contrato</label>
                                            <input id="fnreferencia" class="form-control" type="text" name="fnreferencia">
                                        </div>

                                        <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                                            <label for="idperiocidad">Periocidad <span class="required">*</span></label>
                                            <select class="form-control selectpicker" data-live-search="true" id="fidperiocidad" name="fidperiocidad" required="required"></select>
                                        </div>

                                        <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                                            <label for="ubicacion">Archivador</label>
                                            <input type="text" id="fubicacion" name="fubicacion" class="form-control">
                                        </div>

                                        <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                            <label>Avenida<span class="required">*</span></label>
                                            <input type="text" class="form-control" name="fcalle" id="fcalle" required="Campo requerido">
                                        </div>
                                        <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                                            <label>Numero <span class="required">*</span></label>
                                            <input type="text" class="form-control" name="fnumero" id="fnumero" required="Campo requerido">
                                        </div>
                                        <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                                            <label>Oficina</label>
                                            <input type="text" class="form-control" name="foficina" id="foficina">
                                        </div>

                                        <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                            <label>Region <span class="required">*</span></label>
                                            <select class="form-control selectpicker" data-live-search="true" id="fidregiones" name="fidregiones" required="Campo requerido">
                                            </select>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                            <label>Provincia <span class="required">*</span></label>
                                            <select class="form-control selectpicker" data-live-search="true" id="fidprovincias" name="fidprovincias" required="Campo requerido">
                                            </select>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                            <label>Comuna <span class="required">*</span></label>
                                            <select class="form-control selectpicker" data-live-search="true" id="fidcomunas" name="fidcomunas" required="Campo requerido">
                                            </select>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label for="fecha">Fecha firma de contrato <span class="required">*</span></label>
                                            <input type='date' id="ffecha" name="ffecha" class="form-control" required="required"/>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label for="fecha">Fecha inicio de contrato</label>
                                            <input type='date' id="ffecha_ini" name="ffecha_ini" class="form-control"/>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label for="fecha">Fecha fin de contrato (Blanco Indefinido)</label>
                                            <input type='date' id="ffecha_fin" name="ffecha_fin" class="form-control"/>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                            <label for="rautomatica">Renovacion Automatica <span class="required">*</span></label>
                                                <!--<input type="text" id="nedificios" name="nedificios" required="required" class="form-control">-->
                                            <select class="form-control selectpicker" data-live-search="false" id="nedificios" name="nedificios" required="required">
                                                <option value="" selected disabled>Renovacion Automatica?</option>
                                                <option value="0">NO</option>
                                                <option value="1">SI</option>
                                            </select>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                            <label for="observaciones">Observaciones adicionales del contrato</label>
                                            <textarea type="text" id="fobservaciones" name="fobservaciones" class="resizable_textarea form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 left-margin">
                                            <button class="btn btn-primary" type="button" id="btnCancelar"  onclick="cancelarform()">Cancelar</button>
                                            <button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiar()">Limpiar</button>
                                            <button class="btn btn-success" type="submit" id="btnGuardar">Agregar</button>
                                        </div>
                                    </div>

                                </form>
                            </div>

                            <div id="tabcontrato" class="x_content">

                                <div class="col-md-12 col-sm-12 col-xs-12" style="border:0px solid #e5e5e5;">
                                    <h1 class="prod_title" id="tabncontrato" name="tabncontrato"></h1>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <ul class="">
                                            <li>
                                                <h5><b>Tipo contrato: </b></h5><p id="tabtcontrato" name="tabtcontrato"></p>
                                            </li>
                                            <li>
                                                <h5><b>Codigo Cliente: </b></h5><p id="tabnexterno" name="tabexterno"></p>
                                            </li>
                                            <li>
                                                <h5><b>Referencia Contrato: </b></h5><p id="tabnreferencia" name="tabnreferencia"></p>
                                            </li>
                                            <li>
                                                <h5><b>Ubicacion Contrato: </b></h5><p id="tabubicacion" name="tabnubicacion"></p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <div class="">
                                            <ul class="">
                                                <li>
                                                    <h5><b>Periocidad: </b></h5><p id="tabperiocidad" name="tabperiocidad"></p>
                                                </li>
                                                <li>
                                                    <h5><b>Fecha de firma: </b></h5><p id="tabfirma" name="tabfirma"></p>
                                                </li>
                                                <li>
                                                    <h5><b>Inicio del contrato: </b></h5><p id="tabinicio" name="tabinicio"></p>
                                                </li>
                                                <li>
                                                    <h5><b>Fin del contrato: </b></h5><p id="tabfin" name="tabfin"></p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <ul class="">
                                            <li>
                                                <h5><b>Region: </b></h5><p id="tabregion" name="tabregion"></p>
                                            </li>
                                            <li>
                                                <h5><b>Razon Social: </b></h5><p id="tabrazon" name="tabrazon"></p>
                                            </li>
                                            <li>
                                                <h5><b>RUT : </b></h5><p id="tabrut" name="tabrut"></p>
                                            </li>
                                            <li>
                                                <h5><b>Direccion: </b></h5><p id="tabdireccion" name="tabdireccion"></p>
                                        </ul>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="ln_solid"></div>


                                    <div class="col-md-12">

                                        <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#tab_content1" role="tab" id="home-tab"  data-toggle="tab" aria-expanded="true">EDIFICIOS</a>
                                                </li>
                                                <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab"  data-toggle="tab" aria-expanded="false">ASCENSORES</a>
                                                </li>
                                                <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">CC</a>
                                                </li>
                                                <li role="presentation" class=""><a href="#tab_content4" role="tab" id="profile-tab3" data-toggle="tab" aria-expanded="false">OBSERVACIONES</a>
                                                </li>
                                            </ul>
                                            <div id="myTabContent" class="tab-content">
                                                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                                                    <table id="tbltabedificios" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th>OPCIONES</th>
                                                                <th>DIRECCION</th>
                                                                <th>SEGMENTO</th>
                                                                <th>REGION</th>
                                                                <th>COMUNA</th>                                                            
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                                                    <table id="tbltabascensores" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th>OPCIONES</th>
                                                                <th>CODIGO</th>
                                                                <th>CODIGO CLIENTE</th>
                                                                <th>KEN</th>
                                                                <th>MARCA</th>
                                                                <th>MODELO</th>
                                                                <th>EDIFICIO</th>
                                                                <th>REGION</th>                                                            
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>        
                                                </div>
                                                <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                                                    <table id="tbltabcentros" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th>OPCIONES</th>
                                                                <th>CODIGO</th>
                                                                <th>NOMBRE</th>                                                          
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>                                             
                                                </div>
                                                <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="profile-tab">
                                                    <p id="tabobservaciones" name="tabobservaciones"></p>
                                                </div>                     
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <!-- Formulario modificar ascensor -->




                            <!-- Modal -->
                            <!--<div class="modal fade" id="modalContrato" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span class="sr-only">Close</span></button>
                                            <h4 class="modal-title" id="myModalLabel">AGREGAR CONTRATO</h4>
                                        </div>

                                        <form id="formulario" name="formulario" method="post" class="form-horizontal form-label-left">
                                            <div class="modal-body">
                                                <h4><b>DATOS DEL CLIENTE</b></h4>  
                                                <div class="col-md-11 col-sm-12 col-xs-10 form-group">
                                                    <select class="form-control selectpicker" data-live-search="true" id="idcliente" name="idcliente" required="required"></select>
                                                </div>
                                                <div class="col-md-1 col-sm-12 col-xs-12 form-group">
                                                    <button class="btn btn-info btn-sm" type="button" data-toggle="modal" data-target="#modalForm"><i class="fa fa-plus-circle"></i></button>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="ln_solid"></div>
                                                <h4><b>DATOS DEL CONTRATO</b></h4>  
                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <label>Region <span class="required">*</span></label>
                                                    <select class="form-control selectpicker" data-live-search="true" id="conidregiones" name="conidregiones" required="Campo requerido"></select>
                                                </div>

                                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                                    <label for="tcontrato">Tipo de contrato <span class="required">*</span></label>
                                                    <select class="form-control selectpicker" data-live-search="true" id="tcontrato" name="tcontrato" required="required"></select>
                                                </div>
                                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                                    <label for="ncontrato">Numero de contrato <span class="required">*</span></label>
                                                    <input type="text" id="ncontrato" name="ncontrato" required="required" class="form-control" data-inputmask="'mask' : 'AA-99-A-AA-9999'">
                                                </div>

                                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                                    <label for="nexterno">Id. contrato cliente</label>
                                                    <input type="text" id="nexterno" name="nexterno" class="form-control">
                                                </div>
                                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                                    <label for="nreferencia">Referencia tipo de contrato</label>
                                                    <input id="nreferencia" class="form-control" type="text" name="nreferencia">
                                                </div>

                                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                                    <label for="ubicacion">Ubicacion</label>
                                                    <input type="text" id="ubicacion" name="ubicacion" class="form-control">
                                                </div>   
                                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                                    <label for="fecha">Fecha firma de contrato <span class="required">*</span></label>
                                                    <input type='date' id="fecha" name="fecha" class="form-control" required="required"/>
                                                </div>

                                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                                    <label for="fecha">Fecha inicio de contrato <span class="required">*</span></label>
                                                    <input type='date' id="fecha_ini" name="fecha_ini" class="form-control" required="required"/>
                                                </div>
                                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                                    <label for="fecha">Fecha fin de contrato (Blanco Indefinido)</label>
                                                    <input type='date' id="fecha_fin" name="fecha_fin" class="form-control"/>
                                                </div>

                                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                                    <label for="idperiocidad">Periocidad <span class="required">*</span></label>
                                                    <select class="form-control selectpicker" data-live-search="true" id="idperiocidad" name="idperiocidad" required="required"></select>
                                                </div>
                                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                                    <label for="rautomatica">Renovacion Automatica <span class="required">*</span></label>
                                                    <select class="form-control selectpicker" data-live-search="false" id="rautomatica" name="rautomatica" required="required">
                                                        <option value="" selected disabled>Renovacion Automatica?</option>
                                                        <option value="0">NO</option>
                                                        <option value="1">SI</option>
                                                    </select>
                                                </div>

                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <label for="observaciones">Observaciones adicionales del contrato</label>
                                                    <textarea type="text" id="observaciones" name="observaciones" class="resizable_textarea form-control"></textarea>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="modal-footer">
                                                <button type="reset" onclick="limpiarcontrato()" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary submitBtn" id="btnGuardarcont">Agregar</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>-->

                            <div class="modal fade" id="modalContrato" role="dialog">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span class="sr-only">Close</span></button>
                                            <h4 class="modal-title" id="myModalLabel">AGREGAR CONTRATO</h4>
                                        </div>

                                        <form id="formulario" name="formulario" method="post" class="form-horizontal form-label-left">
                                            <div class="modal-body">
                                                <h4><b>DATOS DEL CLIENTE</b></h4>  
                                                <div class="col-md-11 col-sm-12 col-xs-10 form-group">
                                                    <select class="form-control selectpicker" data-live-search="true" id="idcliente" name="idcliente" required="required"></select>
                                                </div>
                                                <div class="col-md-1 col-sm-12 col-xs-12 form-group">
                                                    <button class="btn btn-info btn-sm" type="button" data-toggle="modal" data-target="#modalForm"><i class="fa fa-plus-circle"></i></button>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="ln_solid"></div>
                                                <h4><b>DATOS DEL CONTRATO</b></h4>  
                                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                                    <label>Region <span class="required">*</span></label>
                                                    <select class="form-control selectpicker" data-live-search="true" id="conidregiones" name="conidregiones" required="Campo requerido"></select>
                                                </div>

                                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                                    <label for="tcontrato">Tipo de contrato <span class="required">*</span></label>
                                                    <select class="form-control selectpicker" data-live-search="true" id="tcontrato" name="tcontrato" required="required"></select>
                                                </div>
                                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label for="ncontrato">Numero de contrato <span class="required">*</span></label>
                                                    <input type="text" id="ncontrato" name="ncontrato" required="required" class="form-control" data-inputmask="'mask' : 'AA-99-A-AA-9999'">
                                                </div>

                                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label for="nexterno">Id. contrato cliente</label>
                                                    <input type="text" id="nexterno" name="nexterno" class="form-control">
                                                </div>

                                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label for="ubicacion">Ubicacion</label>
                                                    <input type="text" id="ubicacion" name="ubicacion" class="form-control">
                                                </div>  
                                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label for="fecha">Fecha firma de contrato <span class="required">*</span></label>
                                                    <input type='date' id="fecha" name="fecha" class="form-control" required="required"/>
                                                </div>

                                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label for="fecha">Fecha inicio de contrato <span class="required">*</span></label>
                                                    <input type='date' id="fecha_ini" name="fecha_ini" class="form-control" required="required"/>
                                                </div>
                                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label for="fecha">Fecha fin de contrato (Blanco Indefinido)</label>
                                                    <input type='date' id="fecha_fin" name="fecha_fin" class="form-control"/>
                                                </div>

                                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label for="nreferencia">Tipo Entrada</label>
                                                    <!--<input id="nreferencia" class="form-control" type="text" name="nreferencia">-->
                                                    <select id="nreferencia" name="nreferencia" class="selectpicker form-control">
                                                        <option value="" disabled="" selected="">Sleccione una opción</option>
                                                        <option value="RECUP Kone">RECUPERADOS KONE</option>
                                                        <option value="MM">MULTIMARCA</option>
                                                    </select>
                                                </div>

                                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label for="idperiocidad">Periocidad <span class="required">*</span></label>
                                                    <select class="form-control selectpicker" data-live-search="true" id="idperiocidad" name="idperiocidad" required="required"></select>
                                                </div>
                                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label for="rautomatica">Renovacion Automatica <span class="required">*</span></label>
                                                    <select class="form-control selectpicker" data-live-search="false" id="rautomatica" name="rautomatica" required="required">
                                                        <option value="" selected disabled>Renovacion Automatica?</option>
                                                        <option value="0">NO</option>
                                                        <option value="1">SI</option>
                                                    </select>
                                                </div>

                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <label for="observaciones">Observaciones adicionales del contrato</label>
                                                    <textarea type="text" id="observaciones" name="observaciones" class="resizable_textarea form-control"></textarea>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <!-- Modal Footer -->
                                            <div class="modal-footer">
                                                <button type="reset" onclick="limpiarcontrato()" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                <button type="submit" class="btn btn-primary submitBtn" id="btnGuardarcont">Agregar</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="modalForm" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">
                                                <span aria-hidden="true">&times;</span>
                                                <span class="sr-only">Close</span>
                                            </button>
                                            <h4 class="modal-title" id="myModalLabel">AGREGAR CLIENTE</h4>
                                        </div>

                                        <form role="form" id="formcliente" name="formcliente">
                                            <!-- Modal Body -->
                                            <div class="modal-body">
                                                <p class="statusMsg"></p>
                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <label>RUT <span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="rut" id="rut" required="Campo requerido" data-inputmask="'mask' : '[*9.][999.999]-[*]'">
                                                </div>

                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <label>Razon Social <span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="razon" id="razon" required="Campo requerido" readonly="">
                                                </div>

                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <label>Tipo <span class="required">*</span></label>
                                                    <select class="form-control selectpicker" data-live-search="true" id="idtcliente" name="idtcliente" required="Campo requerido">
                                                    </select>
                                                </div>
                                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                                    <label>Avenida o Calle <span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="callecli" id="callecli" required="Campo requerido">
                                                </div>
                                                <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                                    <label>Numero <span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="numerocli" id="numerocli" required="Campo requerido">
                                                </div>
                                                <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                                    <label>Oficina</label>
                                                    <input type="text" class="form-control" name="oficinacli" id="oficinacli">
                                                </div>
                                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                                    <label>Region <span class="required">*</span></label>
                                                    <select class="form-control selectpicker" data-live-search="true" id="idregionescli" name="idregionescli" required="Campo requerido">
                                                    </select>
                                                </div>
                                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                                    <label>Comuna <span class="required">*</span></label>
                                                    <select class="form-control selectpicker" data-live-search="true" id="idcomunascli" name="idcomunascli" required="Campo requerido">
                                                    </select>
                                                </div>                                              
                                            </div>
                                            <div class="clearfix"></div>

                                            <!-- Modal Footer -->
                                            <div class="modal-footer">
                                                <button type="reset" onclick="limpiarcliente()" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary submitBtn" id="btnGuardarCli">Agregar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>    

                            <!-- Modal -->
                            <div class="modal fade" id="modalEdificio" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">
                                                <span aria-hidden="true">&times;</span>
                                                <span class="sr-only">Close</span>
                                            </button>
                                            <h4 class="modal-title" id="myModalLabel">AGREGAR EDIFICIO</h4>
                                        </div>

                                        <form role="form" id="formedificio" name="formedificio">
                                            <!-- Modal Body -->
                                            <div class="modal-body">
                                                <p class="statusMsg"></p>

                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <label>Nombre del edificio <span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="nombreedi" id="nombreedi" required="Campo requerido">
                                                </div>
                                                <div class="col-md-10 col-sm-12 col-xs-12 form-group">
                                                    <label>Calle o Avenida <span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="calleedi" id="calleedi" required="Campo requerido">
                                                </div>
                                                <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                                                    <label>Numero <span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="numeroedi" id="numeroedi" required="Campo requerido">
                                                </div>
                                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                                    <label>Region <span class="required">*</span></label>
                                                    <select class="form-control selectpicker" data-live-search="true" id="idregionesedi" name="idregionesedi" required="Campo requerido">
                                                    </select>
                                                </div>
                                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                                    <label>Comuna <span class="required">*</span></label>
                                                    <select class="form-control selectpicker" data-live-search="true" id="idcomunasedi" name="idcomunasedi" required="Campo requerido">
                                                    </select>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <label>Segmento <span class="required">*</span></label>
                                                    <select class="form-control selectpicker" data-live-search="true" id="idtsegmento" name="idtsegmento" required="Campo requerido">
                                                    </select>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <label>Coordinacion por correo? <span class="required">*</span></label>
                                                    <select class="form-control selectpicker" data-live-search="true" id="corcorreo" name="corcorreo" required="Campo requerido">
                                                        <option value="" selected disabled>Coordinacion</option>
                                                        <option value="1">SI</option>
                                                        <option value="0">NO</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <label>Residente? <span class="required">*</span></label>
                                                    <select class="form-control selectpicker" data-live-search="true" id="residente" name="residente" required="Campo requerido">
                                                        <option value="" selected disabled>Residente</option>
                                                        <option value="1">SI</option>
                                                        <option value="0">NO</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <label>Nro de contactos <span class="required">*</span></label>
                                                    <select class="form-control selectpicker" data-live-search="true" id="ncon_edi" name="ncon_edi" required="Campo requerido">
                                                        <option value="" selected disabled>N° Contactos edificio</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                    </select>
                                                </div>
                                                <div id="contactoedi" name="contactoedi">
                                                </div>                                             
                                            </div>
                                            <div class="clearfix"></div>

                                            <!-- Modal Footer -->
                                            <div class="modal-footer">
                                                <button type="reset" onclick="limpiaredificio()" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary submitBtn" id="btnGuardarEdi">Agregar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <!-- Modal Editar Contrato -->

                            <div class="modal fade" id="modalEditarContrato" role="dialog">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span class="sr-only">Close</span></button>
                                            <h4 class="modal-title" id="myModalLabel">EDITAR CONTRATO</h4>
                                        </div>

                                        <form id="formeditarcontrato" name="formulario" method="post" class="form-horizontal form-label-left">
                                            <div class="modal-body">
                                                <h4><b>DATOS DEL CLIENTE</b></h4>  
                                                <div class="col-md-11 col-sm-12 col-xs-10 form-group">
                                                    <input type="text" readonly name="nombrecliente" id="editarnombrecliente" class="form-control">
                                                    <input type="hidden" name="idcliente" id="editaridcliente">
                                                    <input type="hidden" name="idcontrato" id="editaridcontrato">
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="ln_solid"></div>
                                                <h4><b>DATOS DEL CONTRATO</b></h4>  
                                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                                    <label>Region <span class="required">*</span></label>
                                                    <select class="form-control selectpicker" data-live-search="true" id="editaridregiones" name="conidregiones" disabled required="Campo requerido"></select>
                                                </div>

                                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                                    <label for="tcontrato">Tipo de contrato <span class="required">*</span></label>
                                                    <select class="form-control selectpicker" data-live-search="true" id="editartcontrato" name="tcontrato" required="required"></select>
                                                </div>
                                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label for="ncontrato">Numero de contrato <span class="required">*</span></label>
                                                    <input type="text" id="editarncontrato" name="ncontrato" required="required" class="form-control" data-inputmask="'mask' : 'AA-99-A-AA-9999'">
                                                </div>

                                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label for="nexterno">Id. contrato cliente</label>
                                                    <input type="text" disabled id="editarnexterno" name="nexterno" class="form-control">
                                                </div>

                                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label for="ubicacion">Ubicacion</label>
                                                    <input type="text" id="editarubicacion" disabled name="ubicacion" class="form-control">
                                                </div>  
                                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label for="fecha">Fecha firma de contrato <span class="required">*</span></label>
                                                    <input type='date' id="editarfecha" disabled name="fecha" class="form-control" required="required"/>
                                                </div>

                                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label for="fecha">Fecha inicio de contrato <span class="required">*</span></label>
                                                    <input type='date' id="editarfecha_ini" disabled name="fecha_ini" class="form-control" required="required"/>
                                                </div>
                                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label for="fecha">Fecha fin de contrato (Blanco Indefinido)</label>
                                                    <input type='date' id="editarfecha_fin"  name="fecha_fin" class="form-control"/>
                                                </div>

                                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label for="nreferencia">Tipo Entrada</label>
                                                    <input id="editarnreferencia" class="form-control" type="text"  disabled name="nreferencia">
                                                    <!-- <select id="nreferencia" name="nreferencia" class="selectpicker form-control">
                                                        <option value="" disabled="" selected="">Sleccione una opción</option>
                                                        <option value="RECUP Kone">RECUPERADOS KONE</option>
                                                        <option value="MM">MULTIMARCA</option>
                                                    </select> -->
                                                </div>

                                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label for="idperiocidad">Periocidad <span class="required">*</span></label>
                                                    <select class="form-control selectpicker" disabled id="editaridperiocidad" name="idperiocidad" required="required"></select>
                                                </div>
                                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label for="rautomatica">Renovacion Automatica <span class="required">*</span></label>
                                                    <select class="form-control selectpicker"id="editarrautomatica" name="rautomatica" required="required">
                                                        
                                                    </select>
                                                </div>

                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <label for="observaciones">Observaciones adicionales del contrato</label>
                                                    <textarea type="text" id="editarobservaciones" name="observaciones" class="resizable_textarea form-control"></textarea>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <!-- Modal Footer -->
                                            <div class="modal-footer">
                                                <button type="reset" onclick="limpiareditarcontrato()" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                <button type="button" onclick="guardareditarcontrato()"class="btn btn-primary submitBtn" id="btnGuardarcont">Editar</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /page content -->

            <!-- /page content -->
            <?php
        } else {
            require 'nopermiso.php';
        }
        require 'footer.php';
        ?>
        <script type="text/javascript" src="scripts/contrato2.js"></script>
        <?php
    }
    ob_end_flush();
    ?>