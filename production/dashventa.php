<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>App Fabrimetal </title>
        
        <link rel="icon" href="../public/favicon.ico" type="image/x-icon" />
        <!-- Bootstrap -->
        <link href="../public/build/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="../public/build/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="../public/build/css/nprogress.css" rel="stylesheet">
        <!-- iCheck -->
        <link href="../public/build/css/green.css" rel="stylesheet">
        <!-- Datatables -->
        <link href="../public/build/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="../public/build/css/buttons.bootstrap.min.css" rel="stylesheet">
        <link href="../public/build/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
        <link href="../public/build/css/responsive.bootstrap.min.css" rel="stylesheet">
        <link href="../public/build/css/scroller.bootstrap.min.css" rel="stylesheet">
        <!-- Custom Theme Style -->
        <link href="../public/build/css/custom.min.css" rel="stylesheet">
        
        <!-- bootstrap-select -->
        <link href="../public/build/css/bootstrap-select.min.css" rel="stylesheet">
 
    </head>
    <body class="">
        <div class="container body" style="height: 100%; text-align: center;">
            <div class="main_container">
                <div class="right_col" role="main">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_title">
                               
                                    <div class="well" >
                                        <form class="form-inline" id="form_busqueda">                                            
                                                <div class="col-md-2 col-md-offset-3">
                                                    <label for="anio_busq">Año</label>
                                                    <select name="anio_busq" id="anio_busq" class="form-control selectpicker" ></select>                                        
                                                </div>
                                              <div class="col-md-2">                                                     
                                                    <label for="mes_busq">Mes</label>
                                                    <select name="mes_busq" id="mes_busq" class="form-control selectpicker">
                                                      <option value="0" selected="selected">TODOS</option>
                                                    </select>  
                                                </div>
                                                <div class="col-md-2">
                                                    <br>
                                                      <button type="submit" class="btn btn-success">Buscar</button>
                                                </div>                                                                                                                                                          
                                        </form>
                                    </div>
                              
                                    <!--<h2>PROYECTOS</h2>-->
                                    <div class="col-md-12 col-sm-12 col-xs-12" style="overflow:hidden;">
                                        <div class="row tile_count product_price" style=" text-align: center;">
                                            <div class="col-md-3 col-sm-6 col-xs-12 tile_stats_count">
                                                <span class="count_top"><i class="fa fa-clock-o"></i> SUMINISTRO IMPORTADO</span>
                                                <div class="count green" id="SI"></div>
                                                <span class="count_bottom"><i class=""><i class="fa fa-sort-asc"></i>N° </i> SUM. IMP. - HOY</span>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12 tile_stats_count">
                                                <span class="count_top"><i class="fa fa-clock-o"></i> POR FACTURAR </span>
                                                <div class="count green" id="xfactSI"></div>
                                                <span class="count_bottom"><i class=""><i class="fa fa-sort-asc"></i>% </i> SUM. IMP. - HOY</span>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12 tile_stats_count">
                                                <span class="count_top"><i class="fa fa-clock-o"></i> FACTURADO </span>
                                                <div class="count green" id="factSI"></div>
                                                <span class="count_bottom"><i class=""><i class="fa fa-sort-asc"></i>% </i> SUM. IMP. - HOY</span>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12 tile_stats_count">
                                                <span class="count_top"><i class="fa fa-clock-o"></i> PORCENTAJE FACTURADO </span>
                                                <div class="count green" id="porcSI"></div>
                                                <span class="count_bottom"><i class=""><i class="fa fa-sort-asc"></i>% </i> SUM. IMP. - HOY</span>
                                            </div>
                                        </div>
                                        <div class="row tile_count product_price"  style="text-align: center;">
                                            <div class="col-md-3 col-sm-3 col-xs-12 tile_stats_count">
                                                <span class="count_top"><i class="fa fa-clock-o"></i> SUMINISTRO NACIONAL</span>
                                                <div class="count green" id="SN"></div>
                                                <span class="count_bottom"><i class=""><i class="fa fa-sort-asc"></i>N° </i> SUM. NAC. - HOY</span>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12 tile_stats_count">
                                                <span class="count_top"><i class="fa fa-clock-o"></i> POR FACTURAR </span>
                                                <div class="count green" id="xfactSN"></div>
                                                <span class="count_bottom"><i class=""><i class="fa fa-sort-asc"></i>% </i> SUM. NAC. - HOY</span>
                                            </div>
                                            <div class="col-md-3 col-sm-3 col-xs-12 tile_stats_count">
                                                <span class="count_top"><i class="fa fa-clock-o"></i> FACTURADO</span>
                                                <div class="count green" id="factSN"></div>
                                                <span class="count_bottom"><i class=""><i class="fa fa-sort-asc"></i>% </i> SSUM. NAC. - HOY</span>
                                            </div>              
                                            <div class="col-md-3 col-sm-6 col-xs-12 tile_stats_count">
                                                <span class="count_top"><i class="fa fa-clock-o"></i> PORCENTAJE FACTURADO </span>
                                                <div class="count green" id="porcSN"></div>
                                                <span class="count_bottom"><i class=""><i class="fa fa-sort-asc"></i>% </i> SUM. NAC. - HOY</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <table class="table table-bordered projects dt-responsive" cellspacing="0" width="100%"id="tblproyecto">
                                        <thead>
                                            <tr>
                                                <th style="width: 20%;"></th>
                                                <th style="width: 40%; text-align: center;" colspan="4">SUMINISTRO IMPORTADO</th>
                                                <th style="width: 40%; text-align: center;" colspan="5">SUMINISTRO NACIONAL</th>
                                            </tr>
                                            <tr>
                                                <th style="width: 20%; font-size: 13px;">PROYECTO</th>
                                                <th style="width: 10%; font-size: 13px;">TOTAL ($)</th>
                                                <th style="width: 10%; font-size: 13px;">X FACTURAR ($)</th>
                                                <th style="width: 10%; font-size: 13px;">FACTURADO ($)</th>
                                                <th style="width: 10%; font-size: 13px;">% FACTURADO</th>
                                                
                                                <th style="width: 10%; font-size: 13px;">TOTAL (UF)</th>
                                                <th style="width: 10%; font-size: 13px;">X FACTURAR (UF)</th>
                                                <th style="width: 10%; font-size: 13px;">FACTURADO (UF)</th>
                                                <th style="width: 10%; font-size: 13px;">% FACTURADO</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    <!-- end project list -->

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <script src="../public/build/js/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="../public/build/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="../public/build/js/fastclick.js"></script>
            
        <!-- bootstrap-select -->
        <script src="../public/build/js/bootstrap-select.min.js"></script>
 
        <!-- Datatables -->
        <script src="../public/build/js/jquery.dataTables.min.js"></script>
        <script src="../public/build/js/dataTables.bootstrap.min.js"></script>
        <script src="../public/build/js/dataTables.buttons.min.js"></script>
        <script src="../public/build/js/buttons.bootstrap.min.js"></script>
        <script src="../public/build/js/buttons.flash.min.js"></script>
        <script src="../public/build/js/buttons.html5.min.js"></script>
        <script src="../public/build/js/buttons.print.min.js"></script>
        <script src="../public/build/js/dataTables.fixedHeader.min.js"></script>
        <script src="../public/build/js/dataTables.keyTable.min.js"></script>
        <script src="../public/build/js/dataTables.responsive.min.js"></script>
        <script src="../public/build/js/responsive.bootstrap.js"></script>
        <script src="../public/build/js/dataTables.scroller.min.js"></script>
        
        <!-- Custom Theme Scripts -->
        <script src="../public/build/js/custom.js"></script>
        <script src="../production/scripts/dashventa.js"></script>
        <!-- NProgress -->
        <script src="../public/build/js/nprogress.js"></script>
        <!-- bootstrap-progressbar -->
        <script src="../public/build/js/bootstrap-progressbar.min.js"></script>

    </body>
</html>



