<?php
ob_start();
session_start();

if(!isset($_SESSION["nombre"])){
  header("Location:login.php");
}else{

require 'header.php';


 ?>
        <!-- Contenido -->
        <div class="right_col" role="main">
        
          <!-- Datos actulidad en fallas -->
          <div class="row tile_count">
            <div class="col-md-3 col-sm-6 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-check"></i> LLAMADAS RECIBIDAS DIA </span>
              <div class="count"><a href="#" id="num_llamadas"></a></div>            
            </div>
              <div class="col-md-3 col-sm-6 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-check"></i> LLAMADAS RECIBIDAS MES </span>
              <div class="count"><a href="#" id="num_llamadasmes"></a></div>            
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-exclamation-triangle"></i> LLAMADAS CONTESTADAS DIA </span>
              <div class="count"><a href="#" id="num_contestadas"></a></div>              
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-exclamation-triangle"></i> LLAMADAS CONTESTADAS MES </span>
              <div class="count"><a href="#" id="num_contestadasmes"></a></div>              
            </div>       
          </div>

          <div class="row tile_count">            
            <div class="col-md-3 col-sm-6 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-exclamation-triangle"></i> LLAMADAS NO CONTESTADAS DIA </span>
              <div class="count"><a href="#" id="num_nocontestadas"></a></div>  
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-exclamation-triangle"></i> LLAMADAS NO CONTESTADAS MES </span>
              <div class="count"><a href="#" id="num_nocontestadasmes"></a></div>  
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-exclamation-triangle"></i> INDICE DE EFICIENCIA DIA </span>
              <div class="count"><a href="#" id="indice"></a></div>  
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-exclamation-triangle"></i> INDICE DE EFICIENCIA MES </span>
              <div class="count"><a href="#" id="indicemes"></a></div>  
            </div>       
          </div>


          <div class="row tile_count">         
            <div class="col-md-3 col-sm-6 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-exclamation-triangle"></i> PROM. DE TIEMPO EN CONTESTAR DIA </span>
              <div class="count"><a href="#" id="promcontestar"></a></div>  
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-exclamation-triangle"></i> PROM. DE TIEMPO EN CONTESTAR MES </span>
              <div class="count"><a href="#" id="promcontestarmes"></a></div>  
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-exclamation-triangle"></i> PROM. DE TIEMPO EN LLAMADA DIA </span>
              <div class="count"><a href="#" id="promllamada"></a></div>  
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-exclamation-triangle"></i> PROM. DE TIEMPO EN LLAMADA MES </span>
              <div class="count"><a href="#" id="promllamadames"></a></div>  
            </div>
          </div>

          <br />

		  <!-- Contenido Graficos -->
          <div id="Charts" class="row">         
              <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Llamadas en el Año</h2>                   
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <canvas id="mybarChart"></canvas>
                  </div>
                </div>
              </div> 

              <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Eficiencia Diaria</h2>                  
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div id="echart_gauge" style="height:300px;"></div>
                  </div>
                </div>
              </div>

              <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Eficiencia Mensual</h2>                  
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div id="echart_gaugemes" style="height:300px;"></div>
                  </div>
                </div>
              </div>
          </div>


          <!-- /Fin contenido graficos -->
          
        </div>
        <!-- /Fin Contenido -->

<?php 
require 'footer.php';
?>
<script type="text/javascript" src="scripts/llamadas.js"></script>

<?php
}
ob_end_flush();
?>
