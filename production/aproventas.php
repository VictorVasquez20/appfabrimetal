<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {

    require 'header.php';

    if ($_SESSION['administrador'] == 1 || $_SESSION['Aprobacion'] == 1) {
        ?>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>APROBACION MEMOS DE VENTAS</h2>
                                <div class="clearfix"></div>
                            </div>
                            <div id="listadoventas" class="x_content">

                                                                                                                <!--<table id="tblproyectos" class="table table-striped border border-gray-dark projects dt-responsive" cellspacing="0" width="100%">-->
                                <table id="tblventas" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>PROYECTO</th>
                                            <th>PEDIDO</th>
                                            <th>MANDANTE</th>
                                            <th>FECHA</th>
                                            <th>VENDEDOR</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <?php
    } else {
        require 'nopermiso.php';
    }
    require 'footer.php';
    ?>
        <script src="../public/build/js/libs/png_support/png.js"></script>
        <script src="../public/build/js/libs/png_support/zlib.js"></script>
        <script src="../public/build/js/jspdf.debug.js"></script>
        <script src="../public/build/js/jspdf.plugin.autotable.js"></script>
        <script src="../public/build/js/jsPDFcenter.js"></script>

    <script type="text/javascript" src="scripts/aproventas.js"></script>
    <?php
}
ob_end_flush();

