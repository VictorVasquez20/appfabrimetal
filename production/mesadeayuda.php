<?php 
	ob_start();
	session_start();
	
	if(!isset($_SESSION["nombre"])){
		header("Location:login.php");
	}else{
		require 'header.php';
		if( isset($_SESSION['nombre']) && isset($_SESSION['email'])){
?>

			<!-- page content -->
			<div class="right_col" role="main">
				<?php 
					//echo '<pre>';print_r($_SESSION);echo '</pre>';
					//$variables="&nombre=".$_SESSION['nombre']." ".$_SESSION['apellido']."&email=".$_SESSION['email'];
					$variables="q=".$_SESSION['email']."&what=email&category=0&owner=0&dt=&limit=10&more2=1&nombre=".$_SESSION['nombre']." ".$_SESSION['apellido'];
				?>
				<div class="row">
					
					<div class="col-md-12 col-sm-12 col-xs-12">
						<?php  /*<iframe style="width: 100%;height: 100vh;position: relative;" src="http://localhost/hesk322/index.php?a=iframe<?php echo $variables; ?>" title="Mesa de Ayuda" frameborder="0" allowfullscreen></iframe>*/ ?>
						<iframe style="width: 100%;height: 100vh;position: relative;" src="https://www.fabrimetaltesting.cl/mesadeayuda/tickets_list.php?<?php echo $variables; ?>" title="Mesa de Ayuda" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
			</div>
			<!-- /page content -->
<?php 
		}else{
			require 'nopermiso.php';
		}
		require 'footer.php'; 
	}
	ob_end_flush();
?>
