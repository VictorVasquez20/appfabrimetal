<?php
ob_start();
session_start();
require 'header.php';
?>
<div class="right_col" role="main">
    <div class="row">
        <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2 id="mes"></h2> <h3>GUÍAS DE SERVICIO <span id="mesact"></span></h3> 
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row tile_count text-center">
                        <div class="col-md-3 col-sm-6 col-xs-12 tile_stats_count">
                            <h4>TERMINADAS</h4>
                            <div class="count" id="gsetotal">0</div>
                            <span class="count_top"><i class="fa fa-clock-o"></i> #GSE TOTAL</span>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12 tile_stats_count">
                            <h4>MANTENCIÓN</h4>
                            <div class="count" id="totmant">0 <span>(100%)</span> </div>
                            <span class="count_top"><i class="fa fa-clock-o"></i> #GSE TOTAL TERMINADAS </span>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12 tile_stats_count">
                            <h4>REPARACIÓN</h4>
                            <div class="count" id="totrep">0 <span>(100%)</span></div>
                            <span class="count_top"><i class="fa fa-clock-o"></i> #GSE TOTAL TERMINADAS </span>
                        </div>   
                        <div class="col-md-3 col-sm-6 col-xs-12 tile_stats_count">
                            <h4>EMERGENCIAS</h4>
                            <div class="count" id="toteme">0 <span>(100%)</span></div>
                            <span class="count_top"><i class="fa fa-clock-o"></i> #GSE TOTAL TERMINADAS </span>
                        </div>
                    </div>
                </div>
            </div>



            <div class="x_panel">
                <div class="x_title">
                    <h3>MANTENCIÓN</h3> 
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="col-md-7 col-sm-12 col-xs-12" style="overflow:hidden;">
                        <div class="row tile_count text-center">
                            <div class="col-md-4 col-sm-6 col-xs-12 tile_stats_count">
                                <span class="count_top"><i class="fa fa-clock-o"></i> #GSE TOTAL MANTENCIÓN</span>
                                <div class="count" id="totmant2"></div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12 tile_stats_count">
                                <span class="count_top"><i class="fa fa-clock-o"></i> FACTURADAS</span>
                                <div class="count" id="mantfact"> </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12 tile_stats_count">
                                <span class="count_top"><i class="fa fa-clock-o"></i> PORCENTAJE (%)</span>
                                <div class="count" id="mantporc"></div>
                            </div>                                   
                        </div>
                        <div class="row tile_count ">
                            <div class="col-md-12 col-sm-12 col-xs-12" style="overflow:hidden;">
                                <div class="col-md-12" style="overflow:hidden;">
                                    <canvas height="100%" id="grafico"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-5 col-sm-12 col-xs-12" style="overflow:hidden;">
                        <div class="row top_tiles text-center">
                            <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="tile-stats">
                                    <div class="count" id="totmant3"></div>
                                    <h3>GSE Mantención</h3>
                                    <p>Terminadas en el mes</p>
                                </div>

                            </div>
                        </div>
                        <div class="row top_tiles text-center">
                            <div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="tile-stats">
                                    <div class="count" id="mdia"></div>
                                    <h3>Terminadas</h3>
                                    <p>GSE terminadas en el día</p>
                                </div>
                            </div>
                            <div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="tile-stats">
                                    <div class="count" id="mproceso"></div>
                                    <h3>En proceso</h3>
                                    <p>GSE en proceso en el día</p>
                                </div>
                            </div>
                        </div>
                        <div class="row top_tiles text-center">
                            <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="tile-stats">
                                    <div class="count" id="mfirmadas">0</div>
                                    <h3>Firmadas</h3>
                                    <p>GSE terminadas y firmadas e el mes</p>
                                </div>

                            </div>
                        </div>
                        <div class="row top_tiles text-center">
                            <div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="tile-stats">
                                    <div class="count" id="mcerrado"></div>
                                    <h3>Pendiente de firma</h3>
                                    <p>GSE terminadas pendiente de firma</p>
                                </div>
                            </div>
                            <div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="tile-stats">
                                    <div class="count" id="mcerradosf"></div>
                                    <h3>No requiere firma</h3>
                                    <p>GSE terminadas que no requieren firma</p>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

            <div class="x_panel">
                <div class="x_title">
                    <h3>REPARACIÓN</h3> 
                    <div class="clearfix"></div>
                </div>
                <div class="x_content text-center">
                    <div class="col-md-7 col-sm-7 col-xs-12" style="overflow:hidden;">
                        <div class="row tile_count ">
                            <div class="col-md-4 col-sm-6 col-xs-12 tile_stats_count">
                                <span class="count_top"><i class="fa fa-clock-o"></i> #GSE TOTAL REPARACIÓN</span>
                                <div class="count" id="totrep2"></div>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12 tile_stats_count" style="color: #3498DB;">
                                <span class="count_top"><i class="fa fa-clock-o"></i> REPARACION</span>
                                <div class="count" id="trep">0 </div>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12 tile_stats_count" style="color: #455C73;">
                                <span class="count_top"><i class="fa fa-clock-o"></i> REPARACION MAYOR</span>
                                <div class="count" id="trepm">0</div>
                            </div>  
                            <div class="col-md-2 col-sm-2 col-xs-12 tile_stats_count" style="color: #5cb85c;">
                                <span class="count_top"><i class="fa fa-clock-o"></i> NORMALIZACION</span>
                                <div class="count" id="trepn">0 </div>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12 tile_stats_count" style="color: #f0ad4e;">
                                <span class="count_top"><i class="fa fa-clock-o"></i> INGENIERIA</span>
                                <div class="count" id="trepi">0</div>
                            </div>  
                        </div>
                        <div class="row tile_count ">
                            <div class="col-md-12 col-sm-12 col-xs-12" style="overflow:hidden;">
                                <div class="col-md-12" style="overflow:hidden;">
                                    <canvas height="100%" id="reparacion"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-5 col-sm-12 col-xs-12" style="overflow:hidden;">
                        <div class="row top_tiles text-center">
                            <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="tile-stats">
                                    <div class="count" id="totrep3"></div>
                                    <h3>GSE Reparación</h3>
                                    <p>Terminadas en el mes</p>
                                </div>

                            </div>
                        </div>
                        <div class="row top_tiles text-center">
                            <div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="tile-stats">
                                    <div class="count" id="rdia"></div>
                                    <h3>Terminadas</h3>
                                    <p>GSE terminadas en el día</p>
                                </div>
                            </div>
                            <div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="tile-stats">
                                    <div class="count" id="rproceso"></div>
                                    <h3>En proceso</h3>
                                    <p>GSE en proceso en el día</p>
                                </div>
                            </div>
                        </div>
                        <div class="row top_tiles text-center">
                            <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="tile-stats">
                                    <div class="count" id="rfirmadas">0</div>
                                    <h3>Firmadas</h3>
                                    <p>GSE terminadas y firmadas e el mes</p>
                                </div>

                            </div>
                        </div>
                        <div class="row top_tiles text-center">
                            <div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="tile-stats">
                                    <div class="count" id="rcerrado"></div>
                                    <h3>Pendiente de firma</h3>
                                    <p>GSE terminadas pendiente de firma</p>
                                </div>
                            </div>
                            <div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="tile-stats">
                                    <div class="count" id="rcerradosf"></div>
                                    <h3>No requiere firma</h3>
                                    <p>GSE terminadas que no requieren firma</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="x_panel">
                <div class="x_title">
                    <h3>EMERGENCIA</h3> 
                    <div class="clearfix"></div>
                </div>
                <div class="x_content text-center">
                    <div class="col-md-7 col-sm-7 col-xs-7" style="overflow:hidden;">
                        <div class="row tile_count ">
                            <div class="col-md-3 col-sm-6 col-xs-12 tile_stats_count">
                                <span class="count_top"><i class="fa fa-clock-o"></i> #GSE TOTAL EMERGENCIA</span>
                                <div class="count" id="toteme2">0</div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12 tile_stats_count" style="color: #3498DB;">
                                <span class="count_top"><i class="fa fa-clock-o"></i> NORMAL</span>
                                <div class="count" id="temen">0 </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12 tile_stats_count" style="color: #455C73;">
                                <span class="count_top"><i class="fa fa-clock-o"></i> DETENIDO</span>
                                <div class="count" id="temed"></div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12 tile_stats_count" style="color: #5cb85c;">
                                <span class="count_top"><i class="fa fa-clock-o"></i> OBSERVACIÓN</span>
                                <div class="count" id="temeo">0</div>
                            </div>
                        </div>                                 

                        <div class="row tile_count ">
                            <div class="col-md-12 col-sm-12 col-xs-12" style="overflow:hidden;">
                                <div class="col-md-12" style="overflow:hidden;">
                                    <canvas height="100%" id="emergencia"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-5 col-sm-12 col-xs-12" style="overflow:hidden;">
                        <div class="row top_tiles text-center">
                            <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="tile-stats">
                                    <div class="count" id="toteme3"></div>
                                    <h3>GSE Emergencia</h3>
                                    <p>Terminadas en el mes</p>
                                </div>

                            </div>
                        </div>
                        <div class="row top_tiles text-center">
                            <div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="tile-stats">
                                    <div class="count" id="edia"></div>
                                    <h3>Terminadas</h3>
                                    <p>GSE terminadas en el día</p>
                                </div>
                            </div>
                            <div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="tile-stats">
                                    <div class="count" id="eproceso"></div>
                                    <h3>En proceso</h3>
                                    <p>GSE en proceso en el día</p>
                                </div>
                            </div>
                        </div>
                        <div class="row top_tiles text-center">
                            <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="tile-stats">
                                    <div class="count" id="efirmadas">0</div>
                                    <h3>Firmadas</h3>
                                    <p>GSE terminadas y firmadas e el mes</p>
                                </div>

                            </div>
                        </div>
                        <div class="row top_tiles text-center">
                            <div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="tile-stats">
                                    <div class="count" id="ecerrado"></div>
                                    <h3>Pendiente de firma</h3>
                                    <p>GSE terminadas pendiente de firma</p>
                                </div>
                            </div>
                            <div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="tile-stats">
                                    <div class="count" id="ecerradosf"></div>
                                    <h3>No requiere firma</h3>
                                    <p>GSE terminadas que no requieren firma</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
</div>

<?php
require 'footer.php';
?>
<script src="../production/scripts/dashguiasservicio.js"></script>