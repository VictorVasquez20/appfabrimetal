<?php
session_start();

if (!isset($_SESSION["nombre"])) {
    
    header("Location:login.php");
    
} else {

    require 'header.php';

    if ($_SESSION['administrador'] == 1 || $_SESSION['Contratos'] == 1) {
        ?>

        <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <div class="x_panel">
                            <div class="x_title">
                                <h2 id="titulo_pantalla"></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-tooltip="tooltip" title="Operaciones" role="button" aria-expanded="false"><i class="fa fa-cog"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a id="op_agregar" data-toggle="modal" data-target="#modalFormModulos" ><i class="fa fa-plus"></i> Agregar</a>
                                            </li>
                                            <li><a id="op_listar" onclick="mostrarform(false)"><i class="fa fa-list-alt"></i> Listar</a>
                                            </li>
                                        </ul>
                                    </li>                     
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            
                            <div id="listado_dirk" class="x_content">

                                <table id="tabla_dirk" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>OPCIONES</th>
                                            <th>CODIGO</th> 
                                            <th>UBICACION</th>
                                            <th>EDIFICIO</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            
                            <div id="div_formulario" >
                                <form class="form-horizontal">
                                    <input type="hidden" name="iddirk" id="iddirk">
                                    <div class="form-group">
                                        <label  class="col-sm-2 control-label">Dirk</label>
                                      <div class="col-sm-10 input-group">
                                          <p class="form-control-static" id="dirk_text"></p>                                       
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label for="idedificio" class="col-sm-2 control-label">Edificio</label>
                                      <div class="col-sm-10 input-group">
                                        <select class="form-control selectpicker" data-live-search="true" id="idedificio" name="idedificio" ></select>                                     
                                      </div>
                                    </div>
                                  </form>
                                
                                    <div class="x_panel" >                                        
                                        <div class="x_title">
                                            <h2>Arduinos</h2>
                                            <ul class="nav navbar-right panel_toolbox">
                                                <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle"  data-toggle="modal" data-target="#modalFormArduinos"  title="Agregar" role="button" >
                                                        <i class="fa fa-plus-circle"></i>
                                                    </a>
                                                </li>                     
                                            </ul>
                                                <div class="clearfix"></div>
                                        </div>
                                        <div  class="x_content">
                                             <table class="table" id="tabla_arduinos">
                                                <thead>
                                                     <tr>
                                                         <th>OPCIONES</th>
                                                         <th>CODIGO</th> 
                                                         <th>IP</th>
                                                         <th>MAC</th>
                                                         <th>FUNCION</th>
                                                     </tr>
                                                 </thead>
                                                 <tbody></tbody>
                                             </table>
                                        </div>
                                    </div>
                         
                                <div class="x_panel" >  
                                    <div class="x_title">
                                    <h2>Ascensores</h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="modal" data-target="#modalFormAscensores" title="Agregar" role="button" >
                                                <i class="fa fa-plus-circle"></i>
                                            </a>                                           
                                        </li>                     
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div  class="x_content">
                                        <table class="table" id="tabla_ascensores">
                                        <thead>
                                            <tr>
                                                <th>OPCIONES</th>
                                                <th>CODIGO</th> 
                                                <th>CODIGO CLIENTE</th>
                                                <th>LATITUD</th>
                                                <th>LONGITUD</th>                                            
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                        </table>
                                    </div>                                    
                                </div> 
                                
                            </div>
                        </div>                                            
                    </div>
                </div>
            </div>
         </div>
    
    <div class="modal fade" id="modalFormModulos" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">           
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">AGREGAR DIRK</h4>
                </div>

                <form class="form-horizontal" id="formModulo" name="formModulo">

                    <div class="modal-body">                                             
                        <div class="form-group">
                            <label for="codigo_dirk" class="col-sm-2 control-label">C&oacute;digo</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="codigo_dirk" name="codigo_dirk" placeholder="Codigo" required="required" style="text-transform: uppercase"   >
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="ubicacion_dirk" class="col-sm-2 control-label">Ubicaci&oacute;n</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="ubicacion_dirk" name="ubicacion_dirk" placeholder="Ubicacion" required="required" style="text-transform: uppercase">
                            </div>
                        </div>  
                        
                        <div class="form-group">
                            <label for="edificio_dirk" class="col-sm-2 control-label">Edificio</label>
                            <div class="col-sm-10">
                                <select class="form-control selectpicker" data-live-search="true" id="edificio_dirk" name="edificio_dirk" required="required"></select>
                            </div>
                        </div>  
                    </div>
                    <div class="clearfix"></div>

                    <!-- Modal Footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="btnGuardarModulo">Agregar</button>
                    </div>
                </form>
            </div>
        </div>
    </div> 
        
    <div class="modal fade" id="modalFormArduinos" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">           
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">AGREGAR ARDUINO</h4>
                </div>

                <form class="form-horizontal" id="formArduino" name="formArduino">
                    <input type="hidden" name="mac_arduino" id="mac_arduino" >
                    <div class="modal-body">                                             
                        <div class="form-group">
                             <label for="codigo_arduino" class="col-sm-2 control-label">C&oacute;digo</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="codigo_arduino" name="codigo_arduino" placeholder="Codigo" required="required" style="text-transform: uppercase" >
                            </div>
                        </div>   
                        <div class="form-group">
                            <label for="ip_arduino" class="col-sm-2 control-label">Ip</label>
                            <div class="col-sm-10">
                                <select class="form-control selectpicker" required="required" name="ip_arduino" id="ip_arduino">
                                    <option value="" selected="selected" disabled="disabled"><--Seleccione--></option>
                                    <option value="192, 168, 100, 150" >192, 168, 100, 150</option>                                    
                                    <option value="192, 168, 100, 151" >192, 168, 100, 151</option>
                                    <option value="192, 168, 100, 152" >192, 168, 100, 152</option>
                                    <option value="192, 168, 100, 153" >192, 168, 100, 153</option>
                                    <option value="192, 168, 100, 154" >192, 168, 100, 154</option>
                                    <option value="192, 168, 100, 155" >192, 168, 100, 155</option>
                                    <option value="192, 168, 100, 156" >192, 168, 100, 156</option>
                                    <option value="192, 168, 100, 157" >192, 168, 100, 157</option>                                    
                                </select>                                
                            </div>
                        </div> 
                        <div class="form-group">
                            <label   class="col-sm-2 control-label">Mac</label>
                            <div class="col-sm-10">
                                <p class="form-control-static" id="mac_text"></p>                      
                            </div>
                        </div> 
                        <div class="form-group">
                            <label for="funcion_arduino" class="col-sm-2 control-label">Funci&oacute;n</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="funcion_arduino" id="funcion_arduino" required="required" >
                                    <option value="" disabled="disabled" selected="selected"><-- Seleccione--></option>
                                    <option value="MONITOREO">MONITOREO</option>
                                    <option value="MOVIMIENTO">MOVIMIENTO</option>
                                </select>
                            </div>
                          </div> 
                    </div>
                    <div class="clearfix"></div>

                    <!-- Modal Footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="btnGuardarArduino">Agregar</button>
                    </div>
                </form>
            </div>
        </div>
    </div> 
     
    <div class="modal fade" id="modalFormAscensores" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">           
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">AGREGAR ASCENSORES</h4>
                </div>

                <form class="form-horizontal" id="formAscensores" name="formAscensores">  
                     
                    <div class="modal-body">                                               
                        <div class="form-group">
                            <label for="idascensor" class="col-sm-2 control-label">C&oacute;digo</label>
                            <div class="col-sm-10">
                                <select class="form-control selectpicker" data-live-search="true" name="idascensor" id="idascensor" required="required" ></select>
                            </div>
                          </div> 
                        
                        <div class="form-group">
                             <label for="latitud_ascensor" class="col-sm-2 control-label">Latitud</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="latitud_ascensor" name="latitud_ascensor" placeholder="Latitud" required="required" >
                            </div>
                        </div>  
                        
                        <div class="form-group">
                            <label for="longitud_ascensor" class="col-sm-2 control-label">Longitud</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="longitud_ascensor" name="longitud_ascensor" placeholder="Longitud" required="required" >
                            </div>
                        </div> 

                    </div>
                    <div class="clearfix"></div>

                    <!-- Modal Footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="btnGuardarAscensores">Agregar</button>
                    </div>
                </form>
            </div>
        </div>
    </div> 
        
  <?php  } else {
            require 'nopermiso.php';
        }
        require 'footer.php';
        ?>
        <script type="text/javascript" src="scripts/dirk.js"></script>
        <?php
        
    }

    ?>
