<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {

    require 'header.php';

    if ($_SESSION['administrador'] == 1 || $_SESSION['Tickets'] == 1) {
        ?>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Tickets de Emergencia</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-tooltip="tooltip" title="Operaciones" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-cog"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a id="op_agregar" onclick="mostarform(true)">Agregar</a>
                                            </li>
                                            <li><a id="op_listar" onclick="mostarform(false)">Listar</a>
                                            </li>
                                        </ul>
                                    </li>                     
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div id="listadotickets" class="x_content">

                                <table id="tbltickets" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>N°</th>
                                            <th>EDIFICIO</th>
                                            <th>DIRECCION</th>
                                            <th>EQUIPO</th>                                       
                                            <th>CODIGO FM</th>
                                            <th>CODIGO CLIENTE</th>
                                            <th>ID LLAMADA</th>
                                            <th>TECNICO</th>
                                            <th>ACTUALIZACION</th>
                                            <th>ESTADO</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>

                            <div id="formulariotickets" class="x_content">
                                <br />

                                <div class="col-md-12 center-margin">
                                    <form class="form-horizontal form-label-left" id="formulario" name="formulario">
                                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                            <h4><b>EDIFICIO</b></h4>
                                            <input type="hidden" id="idticket" name="idticket" class="form-control">
                                            <select class="form-control selectpicker" data-live-search="true" id="idedificio" name="idedificio" required="required">
                                                <option value="" selected disabled>Seleccione Edificio</option>
                                            </select>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12 form-group" id="ascensor">
                                            <h4><b>ASCENSOR</b></h4>
                                            <select class="form-control selectpicker" data-live-search="true" id="idascensor" name="idascensor" required="required">
                                                <option value="" selected disabled>Seleccione Ascensor</option>
                                            </select>
                                        </div>
                                        <h4><b>INFORMACION DE LA EMERGENCIA</b></h4>
                                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                            <label for="descripcion">Descripcion de la emergencia</label>
                                            <textarea type="text" id="descripcion" name="descripcion" class="resizable_textarea form-control"></textarea>
                                        </div>
                                        <h4><b>INFORMACION DEL CONTACTO</b></h4>
                                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                            <label>Nombre Y Apellidos</label>
                                            <input type="text" class="form-control" name="nombre" id="nombre">
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                            <label>Correo Electronico</label>
                                            <input type="email" class="form-control" name="email" id="email">
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                            <label>Telefono</label>
                                            <input type="text" class="form-control" name="telefono" id="telefono">
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="ln_solid"></div> 
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <button class="btn btn-primary" type="button" id="btnCancelar" onclick="cancelarform()">Cancelar</button>
                                                <button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiar()">Limpiar</button>
                                                <button class="btn btn-success" type="submit" id="btnGuardar">Agregar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <div class="modal fade bs-example-modal-sm" id="agregatecnico" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">ASIGNAR TÉCNICO</h4>
                    </div>
                    <form id="formulariotecnico" name="formulariotecnico" class="form form-horizontal">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group form-group-sm">
                                        <label class="col-sm-6 control-label">N&uacute;mero de Ticket</label>
                                        <div class="col-sm-6">
                                            <p class="form-control-static" id="idticket_text"></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group form-group-sm">
                                        <label class="col-sm-6 control-label">Nombre</label>
                                        <div class="col-sm-6">
                                            <p class="form-control-static" id="nombre_text"</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group form-group-sm">
                                        <label class="col-sm-6 control-label">Tel&eacute;fono fijo o celular</label>
                                        <div class="col-sm-6">
                                            <p class="form-control-static" id="telefono_text"></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group form-group-sm">
                                        <label class="col-sm-6 control-label">Email</label>
                                        <div class="col-sm-6">
                                            <p class="form-control-static" id="email_text"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group form-group-sm">
                                        <label class="col-sm-6 control-label">Cargo del Solicitante</label>
                                        <div class="col-sm-6">
                                          <p class="form-control-static">OTROS</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group form-group-sm">
                                        <label class="col-sm-6 control-label">Nombre del Edificio</label>
                                        <div class="col-sm-6">
                                            <p class="form-control-static" id="edificio_text"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group form-group-sm">
                                        <label class="col-sm-6 control-label">Calle</label>
                                        <div class="col-sm-6">
                                            <p class="form-control-static" id="calle_text"></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group form-group-sm">
                                        <label class="col-sm-6 control-label">N&uacute;mero</label>
                                        <div class="col-sm-6">
                                            <p class="form-control-static" id="numero_text"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group form-group-sm">
                                        <label class="col-sm-6 control-label">Comuna</label>
                                        <div class="col-sm-6">
                                            <p class="form-control-static" id="comuna_nombre_text"></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group form-group-sm">
                                        <label class="col-sm-6 control-label">Ascensor identificador cliente</label>
                                        <div class="col-sm-6">
                                            <p class="form-control-static" id="codigocli_text"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group form-group-sm">
                                        <label class="col-sm-6 control-label">Descripci&oacute;n de la falla</label>
                                        <div class="col-sm-6">
                                            <p class="form-control-static" id="descripcion_text"></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group form-group-sm">
                                        <label class="col-sm-6 control-label">Fecha de generaci&oacute;n del ticket</label>
                                        <div class="col-sm-6">
                                            <p class="form-control-static" id="fecha_text"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group form-group-sm">
                                        <label class="col-sm-6 control-label">Hora de generaci&oacute;n del ticket</label>
                                        <div class="col-sm-6">
                                            <p class="form-control-static" id="hora_text"></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group form-group-sm">
                                        <label class="col-sm-6 control-label">&nbsp;</label>
                                        <div class="col-sm-6">
                                            <p class="form-control-static">&nbsp;</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group form-group-sm">                           
                                        <label class="col-sm-2 control-label">Técnico</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" data-live-search="true" id="tecnico" name="tecnico" required="required"></select>
                                            <input type="hidden" id="ticket" name="ticket">   
                                        </div>                                                              
                                    </div> 
                                </div>
                             </div>
                            
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary" id="agregacentrocosto">Agregar</button>
                        </div>                        
                    </form>
                    </div>
                </div>
            </div>
        </div>

        <hidden id="agregatecnico-modal" name="agregatecnico-modal" data-toggle="modal" data-target="#agregatecnico"></hidden>
            <?php
        } else {
            require 'nopermiso.php';
        }
        require 'footer.php';
        ?>
    <script src="../public/build/js/jspdf.min.js"></script>
    <script src="../public/build/js/jspdf.plugin.autotable.js"></script>
    <script src="../public/build/js/jsPDFcenter.js"></script>


    <script type="text/javascript" src="scripts/tickets.js"></script>
    <?php
}
ob_end_flush();
?>