<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>App Fabrimetal</title>

        <link rel="icon" href="../public/favicon.ico" type="image/x-icon" />


        <!-- Bootstrap -->
        <link href="../public/build/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="../public/build/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="../public/build/css/nprogress.css" rel="stylesheet">
        <!-- bootstrap-daterangepicker -->
        <link href="../public/build/css/daterangepicker.css" rel="stylesheet">
        <!-- Custom Theme Style -->
        <link href="../public/build/css/custom.min.css" rel="stylesheet">

        <!-- PNotify -->
        <link href="../public/build/css/pnotify.css" rel="stylesheet">
        <link href="../public/build/css/pnotify.buttons.css" rel="stylesheet">
        <link href="../public/build/css/pnotify.nonblock.css" rel="stylesheet">

        <!-- Datatables -->
        <link href="../public/build/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="../public/build/css/buttons.bootstrap.min.css" rel="stylesheet">
        <link href="../public/build/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
        <link href="../public/build/css/responsive.bootstrap.min.css" rel="stylesheet">
        <link href="../public/build/css/scroller.bootstrap.min.css" rel="stylesheet">
    </head>
    <body class="">
        <div class="container body" style="width: 100%; height: 100%; text-align: center;">
            <div class="main_container">
                <div class="right_col" role="main">
                    <div class="">

                        <div class="clearfix"></div>

                        <div class="row">
                            <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h3>PROYECTOS EN INSTALACIONES</h3> 
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="row tile_count" id="tiles">

                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <hr>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="x_panel">
                                                <div class="x_content">
                                                    <div id="echart" style="min-height:400px;"></div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="x_panel">
                                                <div class="x_content">
                                                    <div id="grafico" style="height:400px;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>  
                                </div>
                            </div>

                            <!--<div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h3>VISITAS A OBRAS</h3> 
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="x_panel">
                                                <div class="x_content">
                                                    <table id="visitas" class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>SUPERVISOR</th>
                                                                <th>PROY. ASIGNADOS</th>
                                                                <th>VISITAS</th>
                                                                <th>PROM. POR OBRA</th>
                                                                <th>PROY. TERMINADOS</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody></tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="x_panel">
                                                <div class="x_title">
                                                    <h4>Visitas a obra por supervisor</h4>
                                                </div>
                                                <div class="x_content">
                                                    <canvas id="supvisitas"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- jQuery -->
        <script src="../public/build/js/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="../public/build/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="../public/build/js/fastclick.js"></script>
        <!-- NProgress -->
        <script src="../public/build/js/nprogress.js"></script>
        <!-- Chart.js -->
        <script src="../public/build/js/Chart.min.js"></script>
        <!-- jQuery Sparklines -->
        <script src="../public/build/js/jquery.sparkline.min.js"></script>
        <!-- Flot -->
        <script src="../public/build/js/jquery.flot.js"></script>
        <script src="../public/build/js/jquery.flot.pie.js"></script>
        <script src="../public/build/js/jquery.flot.time.js"></script>
        <script src="../public/build/js/jquery.flot.stack.js"></script>
        <script src="../public/build/js/jquery.flot.resize.js"></script>
        <!-- Flot plugins -->
        <script src="../public/build/js/jquery.flot.orderBars.js"></script>
        <script src="../public/build/js/jquery.flot.spline.min.js"></script>
        <script src="../public/build/js/curvedLines.js"></script>
        <!-- DateJS -->
        <script src="../public/build/js/date.js"></script>
        <!-- bootstrap-daterangepicker -->
        <script src="../public/build/js/moment.min.js"></script>
        <script src="../public/build/js/daterangepicker.js"></script>
        
        <script src="../public/build/js/echarts.min.js"></script>
        
        <!-- PNotify -->
        <script src="../public/build/js/pnotify.js"></script>
        <script src="../public/build/js/pnotify.buttons.js"></script>
        <script src="../public/build/js/pnotify.nonblock.js"></script>

        <!-- Datatables -->
        <script src="../public/build/js/jquery.dataTables.min.js"></script>
        <script src="../public/build/js/dataTables.bootstrap.min.js"></script>
        <script src="../public/build/js/dataTables.buttons.min.js"></script>
        <script src="../public/build/js/buttons.bootstrap.min.js"></script>
        <script src="../public/build/js/buttons.flash.min.js"></script>
        <script src="../public/build/js/buttons.html5.min.js"></script>
        <script src="../public/build/js/buttons.print.min.js"></script>
        <script src="../public/build/js/dataTables.fixedHeader.min.js"></script>
        <script src="../public/build/js/dataTables.keyTable.min.js"></script>
        <script src="../public/build/js/dataTables.responsive.min.js"></script>
        <script src="../public/build/js/responsive.bootstrap.js"></script>
        <script src="../public/build/js/dataTables.scroller.min.js"></script>
        <!-- Custom Theme Scripts -->
        <script src="../public/build/js/custom.js"></script>
        <script src="../production/scripts/dashinstalaciones.js"></script>
    </body>
</html>
