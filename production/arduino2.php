<!DOCTYPE html>

    <html lang="es">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <!-- Meta, title, CSS, favicons, etc. -->
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">

            <title>Atencion</title>

            <link rel="icon" href="../public/favicon.ico" type="image/x-icon" />

            <!-- Bootstrap -->
            <link href="../public/build/css/bootstrap.min.css" rel="stylesheet">
            <!-- Font Awesome -->
            <link href="../public/build/css/font-awesome.min.css" rel="stylesheet">
            <!-- NProgress -->
            <link href="../public/build/css/nprogress.css" rel="stylesheet">
            <!-- iCheck -->
            <link href="../public/build/css/green.css" rel="stylesheet">

            <!-- Datatables -->
            <link href="../public/build/css/dataTables.bootstrap.min.css" rel="stylesheet">
            <link href="../public/build/css/buttons.bootstrap.min.css" rel="stylesheet">
            <link href="../public/build/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
            <link href="../public/build/css/responsive.bootstrap.min.css" rel="stylesheet">
            <link href="../public/build/css/scroller.bootstrap.min.css" rel="stylesheet">

            <!-- bootstrap-progressbar -->
            <link href="../public/build/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">

            <!-- Custom Theme Style -->
            <link href="../public/build/css/custom.css" rel="stylesheet">

        </head>

        <body class="login">
            <div class="container body">
                <div class="main_container">
                    <!-- Contenido -->    
                            <div class="">
                                <div class="clearfix"></div>
                                <!-- Datos actulidad en fallas -->
                                <div class="row tile_count">
                                    <div class="col-md-6 col-sm-6 col-xs-6 tile_stats_count">
                                        <span class="count_top"><i class="fa fa-check"></i> NORMAL </span>
                                        <div class="count"><a href="#" id="num_normal"></a></div>
                                        <span class="count_bottom"> <i id="color_normal" class=""> <i id="icon_normal" class=""></i></i><a id="pornormal"></a> % Total</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 tile_stats_count">
                                        <span class="count_top"><i class="fa fa-exclamation-triangle"></i> FALLA</span>
                                        <div class="count"><a href="#" id="num_falla"</a></div>
                                        <span class="count_bottom"> <i id="color_falla" class="green"> <i id="icon_falla" class="fa fa-sort-asc"></i></i><a id="porfalla"></a> % Total</span>
                                    </div>
                                </div>

                                <!-- Contenido Listar alertas especificas -->
                                <div id="Listado" class="row">         
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="x_panel">
                                            <div class="x_title">
                                                <h2>MONITOREO DIRKKK</h2>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div id="listadoarduinos" class="x_content">

                                                <table id="tblarduinos" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>CODIGO</th>
                                                            <th>FUNCION</th>
                                                            <th>DIRK</th>
                                                            <th>UBICAICON</th>
                                                            <th>EDIFICIO</th>
                                                            <th>ACTUALIZACION</th>
                                                            <th>ESTADO</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>



                            </div>

                    </div>
                </div>
            </div>

            <!-- jQuery -->
            <script src="../public/build/js/jquery.min.js"></script>
            <!-- Bootstrap -->
            <script src="../public//build/js/bootstrap.min.js"></script>
            <!-- FastClick -->
            <script src="../public/build/js/fastclick.js"></script>
            <!-- NProgress -->
            <script src="../public/build/js/nprogress.js"></script>

            <!-- Datatables -->
            <script src="../public/build/js/jquery.dataTables.min.js"></script>
            <script src="../public/build/js/dataTables.bootstrap.min.js"></script>
            <script src="../public/build/js/dataTables.buttons.min.js"></script>
            <script src="../public/build/js/buttons.bootstrap.min.js"></script>
            <script src="../public/build/js/buttons.flash.min.js"></script>
            <script src="../public/build/js/buttons.html5.min.js"></script>
            <script src="../public/build/js/buttons.print.min.js"></script>
            <script src="../public/build/js/dataTables.fixedHeader.min.js"></script>
            <script src="../public/build/js/dataTables.keyTable.min.js"></script>
            <script src="../public/build/js/dataTables.responsive.min.js"></script>
            <script src="../public/build/js/responsive.bootstrap.js"></script>
            <script src="../public/build/js/dataTables.scroller.min.js"></script>
            <script src="../public/build/js/jszip.min.js"></script>
            <script src="../public/build/js/pdfmake.min.js"></script>
            <script src="../public/build/js/vfs_fonts.js"></script>

            <!-- Bootbox Alert -->
            <script src="../public/build/js/bootbox.min.js"></script>


            <!-- Custom Theme Scripts -->
            <script src="../public/build/js/custom.js"></script>

            <script type="text/javascript" src="scripts/arduino.js"></script>

        </body>
    </html>