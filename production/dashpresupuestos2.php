<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {

    require 'header.php';
?>
    <div class="right_col" role="main">
        <div class="row">
            <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h3>PRESUPUESTOS</h3>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row top_tiles" style="text-align: center;">
                            <div class="animated flipInY col-lg-3 col-md-6 col-sm-12 col-xs-12">
                                <div class="tile-stats">
                                    <div class="count" id="total_presupuestos"></div>
                                    <h3>Activos</h3>
                                </div>
                            </div>
                            <div class="animated flipInY col-lg-3 col-md-6 col-sm-12 col-xs-12">
                                <div class="tile-stats">
                                    <div class="count" id="en_proceso"></div>
                                    <h3>En Proceso</h3>
                                </div>
                            </div>
                            <div class="animated flipInY col-lg-3 col-md-6 col-sm-12 col-xs-12">
                                <div class="tile-stats">
                                    <div class="count" id="enviados"></div>
                                    <h3>Enviados</h3>
                                </div>
                            </div>
                            <div class="animated flipInY col-lg-3 col-md-6 col-sm-12 col-xs-12">
                                <div class="tile-stats">
                                    <div class="count" id="aceptados"></div>
                                    <h3>Aceptados</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="x_panel">
                    <div class="x_title">
                        <h3>REPARACIONES</h3>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row top_tiles" style="text-align: center;">
                            <div class="animated flipInY col-lg-3 col-md-6 col-sm-12 col-xs-12">
                                <div class="tile-stats">
                                    <div class="count" id="total_pendientes"></div>
                                    <h3>Por Planificar</h3>
                                </div>
                            </div>
                            <div class="animated flipInY col-lg-3 col-md-6 col-sm-12 col-xs-12">
                                <div class="tile-stats">
                                    <div class="count" id="planificadas"></div>
                                    <h3>Planificadas</h3>
                                </div>
                            </div>
                            <div class="animated flipInY col-lg-3 col-md-6 col-sm-12 col-xs-12">
                                <div class="tile-stats">
                                    <div class="count" id="finalizadas"></div>
                                    <h3>Finalizadas</h3>
                                </div>
                            </div>
                            <div class="animated flipInY col-lg-3 col-md-6 col-sm-12 col-xs-12">
                                <div class="tile-stats">
                                    <div class="count" id="facturacion"></div>
                                    <h3>Facturación</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="x_panel">
                    <div class="x_title">
                        <h3 id="titulo_grafico_lineal">PRESUPUESTOS ACEPTADOS DEL AÑO</h3>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="col-md-12 col-sm-12 col-xs-12" style="overflow:hidden;">

                            <div class="row tile_count ">
                                <div class="col-md-12 col-sm-12 col-xs-12" style="overflow:hidden;">
                                    <div class="col-md-12" style="overflow:hidden;">
                                        <canvas height="80%" id="lineChart1"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="x_panel">
                    <div class="x_title">
                        <h3 id="titulo_grafico_reparacion">REPARACIONES DEL AÑO</h3>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="col-md-12 col-sm-12 col-xs-12" style="overflow:hidden;">

                            <div class="row tile_count ">
                                <div class="col-md-12 col-sm-12 col-xs-12" style="overflow:hidden;">
                                    <div class="col-md-12" style="overflow:hidden;">
                                        <canvas height="80%" id="lineChart2"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
    <?php
    require 'footer.php';
    ?>
    <script type="text/javascript" src="scripts/dashpresupuestos2.js"></script>

<?php
}
ob_end_flush();
?>