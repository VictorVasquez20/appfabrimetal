<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {

    require 'header.php';

    if ($_SESSION['administrador'] == 1 || $_SESSION['VentaFacturacion'] == 1) {
        ?>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>FACTURACION - COMERCIAL</h2>
                                <div class="clearfix"></div>
                            </div>
                            <div id="listadoventas" class="x_content">

                                                                                                                <!--<table id="tblproyectos" class="table table-striped border border-gray-dark projects dt-responsive" cellspacing="0" width="100%">-->
                                <table id="tblventas" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>PROYECTO</th>
                                            <th>PEDIDO</th>
                                            <th>MANDANTE</th>
                                            <th>ESTADO</th>
                                            <th>INCIO</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>

                            <div id="mostrarventa" class="x_content">
                                <div class="col-md-4 col-sm-4 col-xs-12">

                                    <section class="panel">

                                        <div class="panel-body">
                                            <h3 class="grey"><i class="fa fa-building"></i> <span id="moproyecto">AMELIE</span></h3>

                                            <div class="project_detail">

                                                <p class="title">N° MEMO</p>
                                                <p id="mocodigo"></p>
                                                <p class="title">MANDANTE</p>
                                                <p id="momandante"></p>
                                                <p class="title">RUT</p>
                                                <p id="morut"></p>
                                                <p class="title">PROYECTO</p>
                                                <p id="moproyecto"></p>
                                                <p class="title">DIRECCION</p>
                                                <p id="modireccion"></p>
                                                <p class="title">COMUNA - REGION</p>
                                                <p id="mocomuna"></p>
                                                <p class="title">N° PEDIDO</p>
                                                <p id="moimportacion"></p>
                                                <p class="title">VENDEDOR</p>
                                                <p id="movendedor"></p>
                                            </div>

                                            <br />
                                            <h5>Informacion:</h5>
                                            <ul class="list-unstyled project_files" id="documentos">

                                            </ul>
                                            <br />

                                            <div class="text-center mtop20" id="botones">

                                            </div>
                                        </div>

                                    </section>

                                </div>
                                <div class="col-md-8 col-sm-8 col-xs-12">

                                    <div class="row">
                                        <div class="col-xs-12 invoice-header">
                                            <h4>
                                                <i class="fa fa-dollar"></i> <b>SUMINISTRO IMPORTADO</b>
                                            </h4>
                                        </div>
                                        <ul class="stats-overview">
                                            <li>
                                                <span class="name"><b> MONTO TOTAL - </b></span>
                                                <span id="momonedasi" name="momonedasi"></span><span class="value text-success" id="momontosi"></span>
                                            </li>
                                            <li>
                                                <span class="name"><b> SOLICITADO - </b></span>
                                                <span id="momonedasi" name="momonedasi"></span><span class="value text-success" id="momontopfactsi"></span>
                                            </li>
                                            <li class="hidden-phone">
                                                <span class="name"><b> FACTURADO - </b></span>
                                                <span id="momonedasi" name="momonedasi"></span><span class="value text-success" id="momontofactsi"></span>
                                            </li>
                                        </ul>

                                    </div>
                                    <br />
                                    <div class="row">
                                        <div class="col-xs-12 invoice-header">
                                            <h4>
                                                <i class="fa fa-dollar"></i> <b>SUMINISTRO NACIONAL</b>
                                            </h4>
                                        </div>
                                        <ul class="stats-overview">
                                            <li>
                                                <span class="name"><b> MONTO TOTAL - </b></span>
                                                <span id="momonedasn" name="momonedasn"></span><span class="value text-success" id="momontosn"></span>
                                            </li>
                                            <li>
                                                <span class="name"><b> SOLICITADO - </b></span>
                                                <span id="momonedasn" name="momonedasn"></span><span class="value text-success" id="momontopfactsn"></span>
                                            </li>
                                            <li class="hidden-phone">
                                                <span class="name"><b> FACTURADO - </b></span>
                                                <span id="momonedasn" name="momonedasn"></span><span class="value text-success" id="momontofactsn"></span>
                                            </li>
                                        </ul>

                                    </div>

                                    <br />

                                    <div>

                                        <h4><b> FACTURAS </b></h4>

                                        <table id="tblfacturas" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>NUMERO</th>
                                                    <th>FECHA</th>
                                                    <th>SUMINISTRO</th>
                                                    <th>MONEDA</th>
                                                    <th>MONTO</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                        <!-- end of user messages -->
                                    </div>
                                </div>
                            </div>
                            <div id="facturar" class="x_content">
                                <section class="content invoice">
                                    <form role="form" id="formsol" name="formsol">
                                        <!-- title row -->
                                        <div class="row">
                                            <div class="col-xs-12 invoice-header">
                                                <h1>
                                                    <i class="fa fa-building"></i><b><span id="faccodigo"></span> / <span id="faproyecto"></span></b>
                                                    <small class="pull-right">Fecha: <span id="fafecha"></span></small>
                                                </h1>
                                            </div>
                                            <!-- /.col -->
                                        </div>
                                        <br />
                                        <!-- info row -->
                                        <div class="row invoice-info">
                                            <div class="col-sm-6 invoice-col">
                                                <h3><b>MANDANTE</b></h3>
                                                <address>
                                                    <strong><span id="famandante"></span></strong>
                                                    <br><span id="farut"></span>
                                                    <br><span id="fadireccion"></span>
                                                </address>
                                            </div>
                                            <!-- /.col -->
                                            <div class="col-sm-6 invoice-col">
                                                <h3><b>PROYECTO</b></h3>
                                                <address>
                                                    <strong><span id="faproyecto2"></span></strong>
                                                    <br><span id="fadireccionpro"></span>
                                                </address>
                                            </div>
                                        </div>
                                        <!-- /.row -->
                                        <br />

                                        <!-- Table row -->
                                        <div class="row">
                                            <div class="col-xs-12 table">
                                                <h3><b>SUMINISTRO IMPORTADO</b></h3>
                                                <input type="hidden" id="arraysi" name="arraysi" class="form-control">
                                                <table class="table table-striped">
                                                    <thead>
                                                        <tr id="cabeceraimp">
        <!--                                                        <th style="width: 40%">Ascensor</th>
                                                            <td align="center"> Inicio contrato <br /> 20% </td>
                                                            <td align="center"> Entrega BL <br /> 80% </td>-->
                                                        </tr>
                                                    </thead>
                                                    <tbody id="cuerpoimp">

                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- /.col -->
                                        </div>
                                        <!-- /.row -->

                                        <br />
                                        <!-- Table row -->
                                        <div class="row">
                                            <div class="col-xs-12 table">
                                                <h3><b>SUMINISTRO NACIONAL</b></h3>
                                                <input type="hidden" id="arraysn" name="arraysn" class="form-control">
                                                <table class="table table-striped">
                                                    <thead>
                                                        <tr id="cabeceranacional">
                                                        </tr>
                                                    </thead>
                                                    <tbody id="cuerposn">

                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- /.col -->
                                        </div>
                                        <!-- /.row -->
                                        <br />

                                        <div class="row">

                                            <div class="col-xs-12">
                                                <h3><b>RESUMEN</b></h3>
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <th style="width:70%">SUMINISTRO IMPORTADO</th>
                                                                <td style="width:10%"><span id="famonedasi"></span></td>
                                                                <td style="width:20%"><span id="famontosi"></span></td>
                                                            </tr>
                                                            <tr>
                                                                <th>SUMINISTRO NACIONAL</th>
                                                                <td><span id="famonedasn"></span></td>
                                                                <td><span id="famontosn">0</span></td>

                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <!-- /.col -->
                                        </div>
                                        <!-- /.row -->
                                        <br />
                                        <!-- this row will not appear when printing -->
                                        <div class="row no-print">
                                            <div class="col-xs-12" id="botonesfact" name="botonesfact">
                                                <button class="btn btn-success"><i class="fa fa-check"></i> SOLICITAR FACTURACION</button>
                                                <button class="btn btn-primary" style="margin-right: 5px;" onclick="mostarform(0)"><i class="fa fa-close"></i> CANCELAR</button>
                                                <button class="btn btn-default pull-right" onclick="pdfsolfactu()"><i class="fa fa-print"></i> Generar PDF</button>

                                            </div>
                                        </div>
                                    </form>
                                </section>
                            </div>
                            <!-- Modal -->
                            <div class="modal fade" id="modalFormFactura" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">
                                                <span aria-hidden="true">&times;</span>
                                                <span class="sr-only">Close</span>
                                            </button>
                                            <h4 class="modal-title" id="myModalLabel">CARGA DE FACTURA</h4>
                                        </div>

                                        <form role="form" id="formfactura" name="formfactura">
                                            <!-- Modal Body -->
                                            <div class="modal-body">
                                                <p class="statusMsg"></p>
                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <label for="mtsuministro">SUMINISTRO <span class="required">*</span></label>
                                                    <select class="form-control selectpicker" data-live-search="true" id="mtsuministro" name="mtsuministro" required="Campo requerido"></select>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <input type="hidden" id="midventa" name="midventa" class="form-control">
                                                    <label>NUMERO DE FACTURA <span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="mnfactura" id="mnfactura" required="Campo requerido">
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <label>FECHA <span class="required">*</span></label>
                                                    <input type="date" class="form-control" name="mfecha" id="mfecha" required="Campo requerido">
                                                </div>
                                                <div class="col-md-8 col-sm-12 col-xs-12 form-group">
                                                    <label>MONTO <span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="mmonto" id="mmonto" data-inputmask="'alias': 'decimal', 'autoGroup': false, 'digits': 2, 'digitsOptional': false, 'placeholder': '0'" required="Campo requerido">
                                                </div>
                                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                                    <label for="mtmoneda">MONEDA <span class="required">*</span></label>
                                                    <select class="form-control selectpicker" data-live-search="true" id="mtmoneda" name="mtmoneda" required="required"></select>
                                                </div>                            
                                            </div>
                                            <div class="clearfix"></div>

                                            <!-- Modal Footer -->
                                            <div class="modal-footer">
                                                <button type="reset" onclick="limpiarfactura()" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                <button type="submit" class="btn btn-primary submitBtn" id="btnGuardarFact">Agregar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <?php
    } else {
        require 'nopermiso.php';
    }
    require 'footer.php';
    ?>
    <script src="../public/build/js/jspdf.min.js"></script>
    <script src="../public/build/js/jspdf.plugin.autotable.js"></script>
    <script src="../public/build/js/jsPDFcenter.js"></script>
    <script type="text/javascript" src="scripts/ventafact.js"></script>
    <?php
}
ob_end_flush();