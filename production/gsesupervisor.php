<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {

    require 'header.php';

    if ($_SESSION['administrador'] == 1 || $_SESSION['RGuias'] == 1) {
        ?>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Guias de Servicio - Supervisor</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-tooltip="tooltip" title="Operaciones" role="button" aria-expanded="false"><i class="fa fa-cog"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a id="op_listar" onclick="mostarform(false)"><i class="fa fa-list-alt"></i> Listar</a></li>
                                        </ul>
                                    </li>                     
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div id="filtroguias" class="x_content">
                                <div class="form-group">
                                    <div class="col-md-2">
                                        <label>SERVICIO</label>
                                        <select id="selservicio" name="selservicio" class="selectpicker form-control"></select>
                                    </div>
                                    <div class="col-md-2">
                                        <label>MES</label>
                                        <select id="mes" name="mes" class="selectpicker form-control">
                                            <option value="0">TODOS</option>
                                            <option value="1">ENERO</option>
                                            <option value="2">FEBRERO</option>
                                            <option value="3">MARZO</option>
                                            <option value="4">ABRIL</option>
                                            <option value="5">MAYO</option>
                                            <option value="6">JUNIO</option>
                                            <option value="7">JULIO</option>
                                            <option value="8">AGOSTO</option>
                                            <option value="9">SEPTIEMBRE</option>
                                            <option value="10">OCTUBRE</option>
                                            <option value="11">NOMVIEMBRE</option>
                                            <option value="12">DICIEMBRE</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <label>AÑO</label>
                                        <select id="ano" name="ano" class="selectpicker form-control"></select>
                                    </div>
                                    <div class="col-md-2">
                                        <label>TECNICO</label>
                                        <select id="tecnico" name="tecnico" class="selectpicker form-control"></select>
                                    </div>
                                    <div class="col-md-2">
                                        <label>FIRMA</label>
                                        <select id="firma" name="firma" class="selectpicker form-control">
                                            <option value="0">TODOS</option>
                                            <option value="1">FIRMADA</option>
                                            <option value="2">POR FIRMAR</option>
                                            <option value="3">NO REQUIERE FIRMA</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <br>
                                        <button type="button" class="btn btn-info" onclick="ListarGSESupervisor();">BUSCAR</button>
                                    </div>
                                </div>
                            </div>
                            <div id="listadoguias" class="x_content">

                                <table id="tblguias" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>SERVICIO</th>
                                            <th>TIPO</th>
                                            <th>EQUIPO</th>
                                            <th>EDIFICIO</th>
                                            <th>TECNICO</th>
                                            <th>ESTADO</th>
                                            <th>F. INI.</th>
                                            <th>H. INI.</th>
                                            <th>F. FIN</th>
                                            <th>H. FIN</th>
                                            <th>HRS SERVICIO</th>
                                            <th>HRS NORMAL</th>
                                            <th>HRS EXTRA</th>
                                            <th>C. COSTO</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
        <!-- /page content -->
        <?php
    } else {
        require 'nopermiso.php';
    }
    require 'footer.php';
    ?>
    <script src="../public/build/js/libs/png_support/png.js"></script>
    <script src="../public/build/js/libs/png_support/zlib.js"></script>
    <script src="../public/build/js/jspdf.debug.js"></script>
    <script src="../public/build/js/jspdf.plugin.autotable.js"></script>
    <script src="../public/build/js/jsPDFcenter.js"></script>
    <script src="../public/build/js/SimpleTableCellEditor.js"></script>
    <!--<script type="text/javascript" src="scripts/gsesup.js"></script>-->
    <script id="myScript"></script>
    <script>
      var url = 'scripts/gsesup.js';
      var extra = '?t=';
      var randomNum = String((Math.floor(Math.random() * 20000000000)));
      document.getElementById('myScript').src = url + extra + randomNum;
    </script>
    <script>
        $(document).ready(function () {

            editor = new SimpleTableCellEditor("tblguias");
            editor.SetEditableClass("editMe");

            $('#tblguias').on("cell:edited", function (event) {
                console.log(`'${event.oldValue}' changed to '${event.newValue}'`);
            });

        });

    </script>

    <?php
}
ob_end_flush();
?>
