<?php

ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {

    require 'header.php';

    if ($_SESSION['administrador'] == 1) {
        ?>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>EQUIPOS DE IZAJE</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-tooltip="tooltip" title="Operaciones" role="button" aria-expanded="false"><i class="fa fa-cog"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a id="op_agregar" onclick="mostrarform(true)"><i class="fa fa-list-alt"></i> Agregar</a></li>
                                            <li><a id="op_listar" onclick="mostrarform(false)"><i class="fa fa-list-alt"></i> Listar</a></li>
                                        </ul>
                                    </li>                     
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div id="listadoequiposizaje" class="x_content">

                                <!--<table id="tblproyectos" class="table table-striped border border-gray-dark projects dt-responsive" cellspacing="0" width="100%">-->
                                <table id="tblequiposizaje" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>N°</th>
                                            <th>SERIE</th>
                                            <th>MARCA</th>
                                            <th>MODELO</th>
                                            <th>CAPACIDAD</th>
                                            <th>ESTADO</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>

                            <div id="formularioequiposizaje" style="display: none;" class="x_content">
                                <br/>
                                <div class="col-md-12 center-margin">
                                    <form class="form-horizontal form-label-left" id="formulario" name="formulario">
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <label>NUMERO</label>
                                                <input type="text" name="numero" id="numero" class="form-control">
                                                <input type="hidden" name="idequipo" id="idequipo" class="form-control">
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <label>SERIE</label>
                                                <input type="text" name="serie" id="serie" class="form-control">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <label>MARCA</label>
                                                <select id="idmarca" name="idmarca" class="selectpicker form-control"></select>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <label>MODELO</label>
                                                <select id="idmodelo" name="idmodelo" class="selectpicker form-control"></select>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <label>CAPACIDAD</label>
                                                <input type="text" id="capacidad" name="capacidad" class="form-control">
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <label>VELOCIDAD</label>
                                                <input type="text" id="velocidad" name="velocidad" class="form-control">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <label>ESTADO</label>
                                                <select id="estado" name="estado" class="selectpicker form-control">
                                                    <option disabled="">SELECCIONES ESTADO</option>
                                                    <option value="0">EN REPARACION</option>
                                                    <option value="1">DISPONIBLE</option>
                                                    <option value="2">EN USO</option>
                                                    <option value="3">EN REPARACION</option>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="ln_solid"></div> 
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <button class="btn btn-success" type="submit" id="btnGuardar">Guardar</button>
                                                <button class="btn btn-primary" style="float: right;" type="button" id="btnCancelar" onclick="cancelarform()">Cancelar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <?php
    } else {
        require 'nopermiso.php';
    }
    require 'footer.php';
    ?>
        <script type="text/javascript" src="scripts/equipoizaje.js"></script>
    <?php
}
ob_end_flush();
