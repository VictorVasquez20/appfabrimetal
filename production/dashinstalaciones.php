<?php include_once 'header.php'; ?>
<div class="right_col" role="main">
    <div class="">

        <div class="clearfix"></div>

        <div class="row">
            <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h3>EQUIPOS EN INSTALACIÓN</h3> 
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="row tile_count" id="tiles">

                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_content">
                                    <div id="echart" style="min-height:400px;"></div>
                                </div>
                            </div>
                        </div>


                        <div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_content">
                                    <div id="grafico" style="height:400px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>

            <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h3>VISITAS A OBRAS</h3> 
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="animated flipInY col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_content">
                                    <table id="visitas" class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>SUPERVISOR</th>
                                                <th>ASIGNADOS</th>
                                                <th>VISITAS</th>
                                                <th>ENTREGADOS</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-8 col-md-8 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h4>Visitas a obra por supervisor</h4>
                                </div>
                                <div class="x_content">
                                    <canvas id="supvisitas"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require 'footer.php'; ?>
<script src="../production/scripts/dashinstalaciones.js"></script>


