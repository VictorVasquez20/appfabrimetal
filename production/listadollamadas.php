<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {
    
    require 'header.php'; ?>

        <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>LISTADO DE LLAMADAS DE EMERGENCIA</h2>
                                <div class="clearfix"></div>
                            </div>
                            <div  class="x_content">
                                <form class="form-inline" id="form_filtros">
                                    <div class="form-group">
                                    <div class="col-md-5">
                                       <label for="">Año</label>
                                       <select class="form-control selectpicker"  id="anio" name="anio" required="required" ></select>  
                                   </div>
                                   <div class="col-md-5">
                                      <label for="">Mes</label>
                                      <select class="form-control selectpicker"  id="mes" name="mes" required="required" ></select>  
                                    </div>
                                        <div class="col-md-2">  <br>
                                        <button type="submit" class="btn btn-primary">Buscar</button>
                                    </div>
                                    </div>
                                </form>                           
                            </div>                            
                        </div> 
                        <div id="div_listallamadas" class="row" style="display: none" >
                            <div class="col-md-12 col-sm-12 col-xs-12" style="overflow:hidden;">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h4 id="titulo_llamadasemergenciames"></h4>
                                        <div class="clearfix"></div>
                                    </div>
                                           <div class="x_content">
                                                <table id="tbllistadollamadas" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                                <thead>
                                                  <tr>
                                                    <th>Fecha</th> 
                                                    <th>Hora</th>
                                                    <th>Destino</th>
                                                    <th>Origen</th>
                                                    <th>Estado</th>
                                                    <th>Duraci&oacute;n (Segundos)</th>
                                                    <th>Conversaci&oacute;n (Segundos)</th>
                                                  </tr>
                                                </thead>
                                                <tbody></tbody>
                                              </table>
                                      </div> 
                                </div>
                            </div>
                        </div>                                                                 
                </div>
            </div>
        </div>
    </div>
        
 <?php require 'footer.php'; ?>
        
    <script type="text/javascript" src="scripts/listadollamadas.js"></script>
    <?php
}
ob_end_flush();
?>


