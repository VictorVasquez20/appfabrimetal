<?php
ob_start();
session_start();

if(!isset($_SESSION["nombre"])){
  header("Location:login.php");
}else{

require 'header.php';


 ?>
        <!-- Contenido -->
        <div class="right_col" role="main">
        
          <!-- Contenido Listar alertas especificas -->
          <div id="Listado" class="row">         
			       <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Movimientos y Recorridos</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div id="listadomovimientos" class="x_content">

                    <table id="tblmovimientos" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                      		  <th>Opciones</th>
                          	<th>Codigo</th>
                          	<th>Edificio</th>
                          	<th>Ubicacion</th>
                          	<th>Movimientos</th>
                          	<th>Recorrido</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
                  
          </div>
          <!-- /Fin Listar alertas especificas -->
          
        </div>
        <!-- /Fin Contenido -->

<?php 
require 'footer.php';
?>
<script type="text/javascript" src="scripts/movimiento.js"></script>

<?php
}
ob_end_flush();
?>
