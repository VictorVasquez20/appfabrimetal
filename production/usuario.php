<?php 
ob_start();
session_start();

if(!isset($_SESSION["nombre"])){
  header("Location:login.php");
}else{

require 'header.php';

if( $_SESSION['administrador']==1)
{

 ?>
        
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Usuarios</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a id="op_agregar" onclick="mostarform(true)">Agregar</a>
                          </li>
                          <li><a id="op_listar" onclick="mostarform(false)">Listar</a>
                          </li>
                        </ul>
                      </li>                     
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div id="listadousuarios" class="x_content">

                    <table id="tblusuarios" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Opciones</th>
                          <th>Nombre y Apellido</th>
                          <th>Usuario</th>
                          <th>Email</th>
                          <th>Role</th>
                          <th>Condicion</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>

                  <div id="formulariousuarios" class="x_content">
                    <br />
                    <form id="formulario" name="formulario" class="form-horizontal form-label-left input_mask">

                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-6 form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left" id="username" name="username" placeholder="Usuario">
                        <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left" id="password" name="password" placeholder="Contrasena">
                        <span class="fa fa-key form-control-feedback left" aria-hidden="true"></span>
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <input type="hidden" class="form-control has-feedback-left" id="iduser" name="iduser">
                        <input type="text" class="form-control has-feedback-left" id="nombre" name="nombre" placeholder="Nombres">
                        <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left" id="apellido" name="apellido" placeholder="Apellidos">
                        <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                      </div>


                      <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                            <select class="form-control selectpicker" data-live-search="true" id="tipo_documento" name="tipo_documento" required="required">
                            <option value="" selected disabled>Tipo de Documento</option>
                                <option value="RUT">RUT</option>
                                <option value="P">Pasaporte</option>
                                <option value="O">Otro</option>
                            </select>
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left" id="num_documento" name="num_documento" placeholder="Numero de Documento" data-inputmask="'mask' : '99.999.999-9'">
                        <span class="fa fa-id-card form-control-feedback left" aria-hidden="true"></span>
                      </div>

                      <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left" id="direccion" name="direccion" placeholder="Direccion">
                        <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <input type="text" id="telefono" name="telefono" class="form-control has-feedback-left" placeholder="Telefono" data-inputmask="'mask' : '+57(9)9999-9999'">
                        <span class="fa fa-mobile form-control-feedback left" aria-hidden="true"></span>
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <input type="email" class="form-control has-feedback-left" id="email" name="email" placeholder="Email">
                        <span class="fa fa-envelope-o form-control-feedback left" aria-hidden="true"></span>
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                        <div class='input-group date' id='myDatepicker2'>
                            <input type='text' id="fecha_nac" name="fecha_nac" class="form-control" placeholder="Fecha Nacimiento" />
                            <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                      </div>


                      

                      <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <select class="form-control selectpicker" data-live-search="true" id="idrole" name="idrole" required="required">
                            <option value="" selected disabled>Role del Usuario</option>
                            </select>
                      </div>

                      
                      

                      <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                        <input id="imagen" name="imagen" type="file" class="custom-file-input">
                        <input id="imagenactual" name="imagenactual" type="hidden" class="custom-file-input">
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                        <img src="" width="150px" height="120px" id="imagenmuestra" name="imagenmuestra">
                      </div>

                      <div class="ln_solid"></div>

                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">
                          <button class="btn btn-primary" type="button" id="btnCancelar" onclick="cancelarform()">Cancelar</button>
                          <button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiar()">Limpiar</button>
                          <button class="btn btn-success" type="submit" id="btnGuardar">Agregar</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php 
}else{
  require 'nopermiso.php';
}
require 'footer.php';
?>
<script>
    $('#myDatepicker2').datetimepicker({
        format: 'DD/MM/YYYY'
    });
</script>
<script type="text/javascript" src="scripts/usuario.js"></script>
<?php 
}
ob_end_flush();
?>