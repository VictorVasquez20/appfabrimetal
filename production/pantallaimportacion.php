<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {
    
?>

<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>App Fabrimetal</title>

        <link rel="icon" href="../public/favicon.ico" type="image/x-icon" />


        <!-- Bootstrap -->
        <link href="../public/build/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="../public/build/css/font-awesome.min.css" rel="stylesheet">
        
        <!-- Custom Theme Style -->
        <!-- <link href="../public/build/css/custom.min.css" rel="stylesheet"> -->

        <!-- PNotify -->
        <link href="../public/build/css/pnotify.css" rel="stylesheet">
        <link href="../public/build/css/pnotify.buttons.css" rel="stylesheet">
        <link href="../public/build/css/pnotify.nonblock.css" rel="stylesheet">

        <!-- Datatables -->
        <link href="../public/build/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="../public/build/css/buttons.bootstrap.min.css" rel="stylesheet">
        <link href="../public/build/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
        <link href="../public/build/css/responsive.bootstrap.min.css" rel="stylesheet">
        <link href="../public/build/css/scroller.bootstrap.min.css" rel="stylesheet">

        <style>
        /*@import url('https://fonts.googleapis.com/css2?family=Montserrat&display=swap');*/
        </style>

        <style>
        /*@import url('https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400&display=swap');*/
        </style>

        <style>
        /*@import url('https://fonts.googleapis.com/css2?family=Roboto&display=swap');*/
        </style>

        <style>
        /*@import url('https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap');*/
        </style>

        <style>
        /*@import url('https://fonts.googleapis.com/css2?family=Roboto:wght@100&display=swap');*/
        </style>

        <style>
        /*@import url('https://fonts.googleapis.com/css2?family=Playfair+Display&display=swap');*/
        </style>

        <style>
        @import url('https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap');
        </style>

        <style>
        /*@import url('https://fonts.googleapis.com/css2?family=Lato:wght@100&family=Open+Sans:wght@300&family=Playfair+Display&display=swap');*/
        </style>

        <style>
        /*@import url('https://fonts.googleapis.com/css2?family=Ubuntu:wght@300&display=swap');*/
        </style>

        <style>
            * {
                font-size: 13px;
                /*font-family: 'Ubuntu', sans-serif, Verdana, Arial;*/
                /*font-family: 'Lato', sans-serif, Verdana, Arial;*/
                font-family: 'Open Sans', sans-serif, Verdana, Arial;
                /*font-family: 'Playfair Display', 'Roboto', Verdana, Arial;*/
                /*font-family: Gotham, 'Roboto', Verdana, Arial;*/
                /*font-family: 'Roboto', Verdana, Arial;*/
                /*font-family: Montserrat, Verdana, Arial;*/
                font-weight: bold;
            }
            body {
                font-size: 2em;
                /*margin:  0;
                padding:  0;*/
            }
            .contenedor {
                display: flex;
                flex-direction: column;
                align-items: center;
                /*Justify-content: space-between;*/
            }

            .titulo {
                font-size:  2em;
            }

            form {
                margin-block-end: 0em;
            }

            .rowRed {
                background-color: darkorange;
                color: #FFFFFF;
            }

            .rowNormal {
                background-color: #FFFFFF;
                color: #000000;
            }

            /*estilos para que el comentario en el listado se ajuste a la celda*/
            /*se coloca este codigo js en la configuracion del datatable*/
            /*columnDefs: [
                {
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap width-98'>" + data + "</div>";
                    },
                    targets: 1 //el indice segun el orden del campo a ajustar
                }
             ]*/
            .text-wrap{
                white-space:normal;
            }
            .width-98{
                width:98%;
            }

            /*estilo para generar scroll en el modal*/
            .modal-body {
                max-height: calc(100vh - 150px);
                overflow-y: auto;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-6 titulo">CONTROL DE IMPORTACION DE REPUESTOS</div>
                <div class="col-md-5">
                    <form class="form-inline" id="form_filtros">
                        <div class="form-group">
                            <label for="">Año</label>
                            <select class="form-control"  id="anio" name="anio" required="required"></select> 
                          </div>
                          <div class="form-group">
                            <label for="">Mes</label>
                          <select class="form-control"  id="mes" name="mes" required="required"></select>
                          </div>
                          <button type="submit" class="btn btn-primary">Buscar</button>
                    </form>
                </div>
            </div>
        </div>
        <hr>
        <div class="container body" style="width: 100%; height: 100%; text-align: center;">
            <div class="main_container">
    <div class="right_col" role="main">
        <div class="">
            <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div id="listaimportacion" class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h4 id="titulo_listaimportacion"></h4>
                                        <div class="clearfix"></div>
                                    </div>
                                           <div class="x_content">
                                                <table id="tbllistaimportacion" class="table table-bordered dt-responsive" cellspacing="0" width="100%">
                                                <thead>
                                                  <tr>
                                                    <th></th>
                                                    <th>ST</th>
                                                    <th>ST Fecha</th>
                                                    <th>REF</th>
                                                    <th>Fec Ped</th>
                                                    <th>KM</th>
                                                    <th>Desc.</th>
                                                    <th>Cant.</th>
                                                    <th>Pres.</th>
                                                    <th>Sol.</th>
                                                    <th>Obs.</th>
                                                    <th>ETD</th>
                                                    <th>Desp</th>
                                                    <th>Est Lleg</th>
                                                    <th>Lleg</th>
                                                  </tr>
                                                </thead>
                                                <tbody></tbody>
                                              </table>
                                      </div> 
                                </div>
                            </div>
                        </div>                                                                 
                </div>
            </div>
        </div>
    </div>
        
</div>
        </div>

        <div class="modal fade" id="modalComent" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">COMENTARIOS DE LA IMPORTACION (<span id="codkm2"></span>)</h4>
                    </div>

                    <form role="form" id="formcomentario" name="formcomentario">
                        <input type="hidden" id="idcom2" name="idcom" value="">
                        <!-- Modal Body -->
                        <div class="modal-body">
                            <?php if($_SESSION['administrador'] == 1 || $_SESSION['Comex'] == 1 || $_SESSION['importacion'] == 1) { ?>
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label>Comentario <span class="required">*</span>(máx. 250 caractéres)</label>
                                    <textarea type="text" class="form-control" name="comentario" id="comentario" required="Campo requerido" rows="4" maxlength="250"></textarea>
                                </div>
                                <div id="contactoedi" name="contactoedi">
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group" style="text-align: right;">
                                    <button type="reset" onclick="//limpiaredificio()" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                    <button type="submit" class="btn btn-primary submitBtn" id="btnGuardarComent">Agregar comentario</button>
                                </div>
                            <?php } ?>
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                <div class="x_panel" id="listacomentarios">
                                    <table id="tblcomentarios" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th> Fecha</th>
                                                <th> Usuario</th>
                                                <th> Comentario</th>
                                                <!-- <th> Observación</th> -->
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
        <!-- jQuery -->
        <script src="../public/build/js/jquery.min.js"></script>
        <script src="../public/build/js/jquery.blink2.js"></script>
        <!-- Bootstrap -->
        <script src="../public/build/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="../public/build/js/fastclick.js"></script>
        <!-- NProgress -->
        <script src="../public/build/js/nprogress.js"></script>
        <!-- Chart.js -->
        <script src="../public/build/js/Chart.min.js"></script>
        <!-- jQuery Sparklines -->
        <script src="../public/build/js/jquery.sparkline.min.js"></script>
        <!-- Flot -->
        <script src="../public/build/js/jquery.flot.js"></script>
        <script src="../public/build/js/jquery.flot.pie.js"></script>
        <script src="../public/build/js/jquery.flot.time.js"></script>
        <script src="../public/build/js/jquery.flot.stack.js"></script>
        <!-- <script src="../public/build/js/jquery.flot.resize.js"></script> -->
        <!-- Flot plugins -->
        <script src="../public/build/js/jquery.flot.orderBars.js"></script>
        <script src="../public/build/js/jquery.flot.spline.min.js"></script>
        <script src="../public/build/js/curvedLines.js"></script>
        <!-- DateJS -->
        <script src="../public/build/js/date.js"></script>
        <!-- bootstrap-daterangepicker -->
        <script src="../public/build/js/moment.min.js"></script>
        <script src="../public/build/js/daterangepicker.js"></script>

        <!-- PNotify -->
        <script src="../public/build/js/pnotify.js"></script>
        <script src="../public/build/js/pnotify.buttons.js"></script>
        <script src="../public/build/js/pnotify.nonblock.js"></script>

        <!-- Datatables -->
        <script src="../public/build/js/jquery.dataTables.min.js"></script>
        <script src="../public/build/js/dataTables.bootstrap.min.js"></script>
        <script src="../public/build/js/dataTables.buttons.min.js"></script>
        <script src="../public/build/js/buttons.bootstrap.min.js"></script>
        <script src="../public/build/js/buttons.flash.min.js"></script>
        <script src="../public/build/js/buttons.html5.min.js"></script>
        <script src="../public/build/js/buttons.print.min.js"></script>
        <script src="../public/build/js/dataTables.fixedHeader.min.js"></script>
        <script src="../public/build/js/dataTables.keyTable.min.js"></script>
        <script src="../public/build/js/dataTables.responsive.min.js"></script>
        <script src="../public/build/js/responsive.bootstrap.js"></script>
        <script src="../public/build/js/dataTables.scroller.min.js"></script>
        <script src="../public/build/js/jszip.min.js"></script>
        <script src="../public/build/js/pdfmake.min.js"></script>
        <script src="../public/build/js/vfs_fonts.js"></script>


        <!-- Custom Theme Scripts -->
        <script src="../public/build/js/custom.js"></script>
        <script src="../public/build/js/utiles.js"></script>
        <script type="text/javascript" src="scripts/pantallaimportacion.js"></script>
    </body>
</html>        
    <?php
}
ob_end_flush();
?>


