<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {

    require 'header.php';

    if ($_SESSION['administrador'] == 1 || $_SESSION['Contratos'] == 1) {
        ?>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2 id="titulo_pagina"></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-tooltip="tooltip" title="Operaciones" role="button" aria-expanded="false"><i class="fa fa-cog"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a id="op_actualizar"><i class="fa fa-refresh"></i> Actualizar</a>
                                            </li>
                                            <li><a id="op_listar" onclick="mostarform(false)"><i class="fa fa-list-alt"></i> Listar</a>
                                            </li>
                                        </ul>
                                    </li>                     
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div id="listadoedificio" class="x_content">

                                <table id="tbledificios" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Opciones</th>
                                            <th>Nombre</th>
                                            <th>Direccion</th>
                                            <th>Region</th>                          
                                            <th>Comuna</th>
                                            <th>Segmento</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>


                            <div id="formularioedificio" class="x_content">

                                <div class="col-md-12 center-margin">
                                    <form class="form-horizontal form-label-left" id="formulario" name="formulario">
                                        <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                            <label>Nombre <span class="required">*</span></label>
                                            <input type="hidden" id="idedificio" name="idedificio" class="form-control">
                                            <input type="text" class="form-control" name="nombre" id="nombre" required="Campo requerido">
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                            <label>Avenida o Calle <span class="required">*</span></label>
                                            <input type="text" class="form-control" name="calle" id="calle" required="Campo requerido">
                                        </div>
                                        <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                                            <label>Numero <span class="required">*</span></label>
                                            <input type="text" class="form-control" name="numero" id="numero" required="Campo requerido">
                                        </div>
                                        <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                                            <label>Oficina</label>
                                            <input type="text" class="form-control" name="oficina" id="oficina">
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                            <label>Segmento <span class="required">*</span></label>
                                            <select class="form-control selectpicker" data-live-search="true" id="idtsegmento" name="idtsegmento" required="Campo requerido">
                                            </select>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                            <label>Residente <span class="required">*</span></label>
                                            <select class="form-control selectpicker" data-live-search="true" id="residente" name="residente" required="Campo requerido">
                                                <option value="1">SI</option>
                                                <option value="0">NO</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                            <label>Coordinacion <span class="required">*</span></label>
                                            <select class="form-control selectpicker" data-live-search="true" id="coordinacion" name="coordinacion" required="Campo requerido">
                                                <option value="1">SI</option>
                                                <option value="0">NO</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                            <label>Region <span class="required">*</span></label>
                                            <select class="form-control selectpicker" data-live-search="true" id="idregiones" name="idregiones" required="Campo requerido">
                                            </select>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                            <label>Provincia <span class="required">*</span></label>
                                            <select class="form-control selectpicker" data-live-search="true" id="idprovincias" name="idprovincias" required="Campo requerido">
                                            </select>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                            <label>Comuna <span class="required">*</span></label>
                                            <select class="form-control selectpicker" data-live-search="true" id="idcomunas" name="idcomunas" required="Campo requerido">
                                            </select>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="ln_solid"></div> 

                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <button class="btn btn-primary" type="button" id="btnCancelar" onclick="cancelarform()">Cancelar</button>
                                                <button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiar()">Limpiar</button>
                                                <button class="btn btn-success" type="submit" id="btnGuardar">Agregar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div id="tabedificio" class="x_content">   
                                <div class="col-md-12 col-sm-12 col-xs-12" style="border:0px solid #e5e5e5;">
                                    <h1 class="prod_title" id="tabnombre" name="tabnombre"></h1>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <ul class="">
                                            <li>
                                                <h5><b>Segmento: </b></h5><p id="tabsegmento" name="tabsegmento"></p>
                                            </li>
                                            <li>
                                                <h5><b>Direccion: </b></h5><p id="tabdireccion" name="tabdireccion"></p>
                                            </li>
                                            <li>
                                                <h5><b>Oficina: </b></h5><p id="taboficina" name="taboficina"></p>
                                            </li>                                            
                                        </ul>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <ul class="">
                                            <li>
                                                <h5><b>Region: </b></h5><p id="tabregion" name="tabregion"></p>
                                            </li>
                                            <li>
                                                <h5><b>Provincia: </b></h5><p id="tabprovincia" name="tabprovincia"></p>
                                            </li>
                                            <li>
                                                <h5><b>Comuna: </b></h5><p id="tabcomuna" name="tabcomuna"></p>
                                            </li>
                                        </ul>
                                    </div>
                                    

                                    <div class="col-md-12">

                                        <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#tab_content1" role="tab" id="home-tab"  data-toggle="tab" aria-expanded="true">Contratos</a>
                                                </li>
                                                <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab"  data-toggle="tab" aria-expanded="false">Ascensores</a>
                                                </li>
                                                <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Contactos</a>
                                                </li>                    
                                            </ul>
                                                                                      
                                            <div id="myTabContent" class="tab-content">
                                                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                                                    <table class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>N° CONTRATO</th>
                                                                <th>FECHA</th>
                                                                <th>TIPO</th>
                                                                <th>OPCIONES</th>                                   
                                                            </tr>
                                                        </thead>
                                                        <tbody id="tabcontratos" name="tabcontratos">                                 
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                                                    <table class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>CODIGO</th>
                                                                <th>TIPO</th>
                                                                <th>MARCA</th>
                                                                <th>MODELO</th>
                                                                <th>VALOR UF</th>
                                                                <th>CONTRATO</th>
                                                                <th>OPCIONES</th>                                                              
                                                            </tr>
                                                        </thead>
                                                        <tbody id="tabascensores" name="tabascensores">
                                                            
                                                        </tbody>
                                                    </table>
                                                </div>
                                                 <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">    
                                                    
                                                  <div class="x_title">
                                                    <h2>Contactos</h2>
                                                    <ul class="nav navbar-right panel_toolbox">
                                                        <li class="dropdown">
                                                            <a href="#" data-toggle="modal" data-target="#modalFormContacto" data-tooltip="tooltip" title="Agregar" role="button" aria-expanded="false" data-idcontacto=""><i class="fa fa-plus-circle"></i></a>                                                           
                                                        </li>                     
                                                    </ul>
                                                    <div class="clearfix"></div>
                                                </div>  
                                                    
                                                    <div id="listadocontactos" class="x_content"> 
                                                        <table  id="tbltabcontactos"   class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                                            <thead>
                                                                <tr>
                                                                    <th>&nbsp;</th>
                                                                    <th>NOMBRE</th>
                                                                    <th>CARGO</th>
                                                                    <th>EMAIL</th>
                                                                    <th>TELF. MOVIL</th>
                                                                    <th>TELF. RESIDENCIAL</th>                                                              
                                                                </tr>
                                                            </thead>
                                                            <tbody></tbody>
                                                        </table>
                                                    </div>  
                                                    
                                               </div>  
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

          <!-- Modal -->
        <div class="modal fade" id="modalFormContacto" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">AGREGAR CONTACTO</h4>
                    </div>

                    <form role="form" id="formcontacto" name="formcontacto">
                       
                        <input type="hidden" name="idcontacto" id="idcontacto" >
                        <input type="hidden" name="idedificio_contacto" id="idedificio_contacto" >
                        <!-- Modal Body -->
                        <div class="modal-body">
                            <p class="statusMsg"></p>

                            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                <label>Nombre <span class="required">*</span></label>
                                <input type="text" class="form-control" name="nombre_contacto" id="nombre_contacto" style="text-transform: uppercase" required="Campo requerido">
                            </div>
                               <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                <label>Cargo</label>
                                <input type="text" class="form-control" name="cargo_contacto" id="cargo_contacto" style="text-transform: uppercase" >
                            </div>
                             <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                <label>Email <span class="required">*</span></label>
                                <input type="email" class="form-control" name="email_contacto" id="email_contacto" required="Campo requerido" style="text-transform: uppercase">
                            </div>
                             <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                 <label>Tel&eacute;fono M&oacute;vil</label>
                                 <input type="text" class="form-control" name="telefonomovil_contacto" id="telefonomovil_contacto" data-inputmask="'mask' : '+56(9)9999-9999'">
                            </div>
                             <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                 <label>Tel&eacute;fono Residencial</label>
                                <input type="text" class="form-control" name="telefonoresi_contacto" id="telefonoresi_contacto" data-inputmask="'mask' : '+56(2)9999-9999'">
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        <!-- Modal Footer -->
                        <div class="modal-footer">
                            <button type="reset"  class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary submitBtn" id="btnGuardarContacto">Agregar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
          
        <?php
    } else {
        require 'nopermiso.php';
    }
    require 'footer.php';
    ?>
    <script type="text/javascript" src="scripts/edificio.js"></script>
    <script type="text/javascript" src="scripts/contacto.js"></script>
    <?php
}
ob_end_flush();
?>