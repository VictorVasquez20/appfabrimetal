<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:loginAdmin.php");
} else {

    require 'header.php';
    ?>

    <div class="right_col" role="main">
        <div class="">
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Visitas</h2>
                            <div class="clearfix"></div>

                            <!--<button id="op-agregar" name="op-agregar" onclick="mostrarform(true)" class="btn btn-primary">Agregar</button>
                            
                            <button id="op-listar" name="op-listar" style="display:none;" onclick="mostrarform(true)" class="btn btn-primary">Listado</button>-->
                        </div>



                        <div id="listadoproy" class="x_content">
                            <table id="tblproy" class="table table-striped projects dt-responsive" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th width="10%">#</th>
                                        <th>Proyecto</th>
                                        <th>Supervisor</th>
                                        <th>Project manajer</th>
                                        <th>Progreso</th>
                                        <th>Estado</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>


                        <div id="formularioproy" style="display:none;" class="x_content">
                            <br />
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <section class="panel">
                                    <div class="x_title">
                                        <h2 class="green">Descripcion proyecto</h2>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="panel-body">
                                        <div class="project_detail">
                                            <p class="title"> Nombre Proyecto</p>
                                            <h3 class="value text-success" id="nombProy"></h3>

                                            <div class="clearfix"></div>

                                            <p class="title"> Estado proyecto  </p>
                                            <p class="value text-info text-bold" id="estadoproy"></p>

                                            <div class="clearfix"></div>

                                            <p class="title"> Fecha inicio </p>
                                            <p class="value text-success" id="created_time"></p>

                                            <div class="clearfix"></div>


                                            <p class="title"> Codigo proyecto </p>
                                            <p class="value text-success" id="codigo"></p>

                                            <p class="title">Centro costo</p>
                                            <p class="green" id="ccnomb"></p>



                                            <p class="title">Project Manager</p>
                                            <p id="pm"></p>

                                            <p class="title">Supervisor</p>
                                            <p id="supervisor"></p>
                                            <!--<p class="title">Project Leader</p>
                                            <p>Tony Chicken</p>-->
                                        </div>
                                    </div>

                                </section>


                                <form class="form-horizontal form-label-left" id="formulario" name="formulario" method="POST">
                                    <input type="hidden" id="idproyecto" name="idproyecto" class="form-control" required="Campo requerido">
                                    <input type="hidden" id="idestado" name="idestado" class="form-control" required="Campo requerido">                                   
                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="observaciones">OBSERVACIONES DE LA VISITA</label>
                                        <textarea type="text" id="observaciones" name="observaciones" class="resizable_textarea form-control"></textarea>
                                    </div>


                                    <div class="clearfix"></div>
                                    <div class="ln_solid"></div> 
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <button class="btn btn-primary" type="button" id="btnCancelar" onclick="cancelarform()">Cancelar</button>
                                            <button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiar()">Limpiar</button>
                                            <button class="btn btn-success" type="submit" id="btnGuardar">Agregar</button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <?php
    require 'footer.php';
    ?>
    <script type="text/javascript" src="scripts/visitasinstalaciones.js"></script>
    <script src="../public/build/js/nprogress.js"></script>
    <script src="../public/build/js/bootstrap-progressbar.min.js"></script>
    <?php
}
ob_end_flush();
