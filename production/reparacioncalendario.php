<?php
	require 'header.php';
?>
<div class="right_col" role="main">
	<div class="">
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
							<h2>Calendario de Reparaciones </h2>
							<a href="reparacion.php" class="navbar-right panel_toolbox btn btn-info"><i class="fa fa-calendar"></i> Volver a Listado</a>
						<div class="clearfix"></div>
					</div>
					<div id='calendario'></div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="extraModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
	<div class="modal-dialog modal-lg" role="document">
		<div id="extraInfo">
			<div id="formulario" class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title" id="myModalLabel"><i class="fa fa-file-o"></i> Detalle de Información</h3>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<div class="form-group">
								<label for="exampleFormControlInput1">Código Ascensor</label>
								<input type="text" class="form-control input-sm" readonly value="" id="codAscensor">
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<div class="form-group">
								<label for="exampleFormControlInput1">Número Presupuesto</label>
								<input type="text" class="form-control input-sm" readonly value="" id="numPresupuesto">
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<div class="form-group">
								<label for="exampleFormControlInput1">Fecha Inicio Reparación</label>
								<input type="text" class="form-control input-sm" readonly value="" id="reqTime">
							</div>
						</div>
					</div>
					<div class="row">
					</div>
					<hr>
					<h4>Datos Edificio</h4>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
							<div class="form-group">
								<label for="exampleFormControlInput1">Nombre</label>
								<input type="text" class="form-control input-sm" readonly value="" id="ediNombre">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="form-group">
								<label for="exampleFormControlInput1">Dirección</label>
								<input type="text" class="form-control input-sm" readonly value="" id="edfDireccion">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<a href="#" class='btn btn-default btn-icon-split' data-dismiss='modal'><span class="icon text-white-50"><i class="fa fa-close"></i></span> <span class="text">Salir</span></a>
				</div>
	
			</div>
		</div>
	</div>
</div>
<?php
require 'footer.php';
?>
<script type="text/javascript" src="scripts/reparacioncalendario.js"></script>