<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {

    require 'header.php';

    if ($_SESSION['administrador'] == 1 || $_SESSION['GGuias'] == 1) {
        ?>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>GUIAS PENDIENTES DE FIRMA</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-tooltip="tooltip" title="Operaciones" role="button" aria-expanded="false"><i class="fa fa-cog"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a id="op_agregar" onclick="actualizar()"><i class="fa fa-refresh"></i>  Actualizar</a>
                                            </li>
                                        </ul>
                                    </li>                     
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div id="listadoguias" class="x_content">

                                <table id="tblguias" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>PDF</th>
                                            <th>TIPO</th>
                                            <th>SERVICIO</th>
                                            <th>EQUIPO</th>
                                            <th>EDIFICIO</th>
                                            <th>TECNICO</th>
                                            <th>SUPERVISOR</th>
                                            <!--<th>MES</th>-->
                                            <th>FECHA</th>
                                            <!--<th>INICIO</th>
                                            <th>FIN</th>-->
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            
                            <!-- Modal -->
                            <div class="modal fade" id="editar" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">
                                                <span aria-hidden="true">&times;</span>
                                                <span class="sr-only">Close</span>
                                            </button>
                                            <h4 class="modal-title" id="myModalLabel">EDITAR SERVICIO</h4>
                                        </div>
                                        <form id="formservicio" name="formservicio" class="form form-horizontal">
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                        <label>ASCENSOR</label>
                                                        <select class="selectpicker form-control" id="idascensor" name="idascensor"  required="campo requerido"></select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                        <label>TECNICO</label>
                                                        <select class="selectpicker form-control" id="idtecnico" name="idtecnico"  required="campo requerido"></select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                                        <label>TIPO SERVICIO</label>
                                                        <select class="selectpicker form-control" id="idtservicio" name="idtservicio"  required="campo requerido"></select>
                                                        <input type="hidden" class="form-control col-md-4" name="idservicio" id="idservicio">
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                                        <label>REQ. FIRMA</label>
                                                        <select class="selectpicker form-control" id="reqfirma" name="reqfirma"  required="campo requerido">
                                                            <option value="0">NO</option>
                                                            <option value="1">SI</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                                        <label>FECHA INI.</label>
                                                        <div class='input-group date' id='myDatepicker'>
                                                            <input type='text' class="form-control" id="created_time" name="created_time"  required="campo requerido"/>
                                                            <span class="input-group-addon">
                                                               <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                                        <label>ESTADO INI.</label>
                                                        <select class="selectpicker form-control" id="estadoini" name="estadoini"  required="campo requerido"></select>
                                                    </div>
                                                </div>	
                                                <div class="form-group">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                        <label>OBSERVACION INI.</label>
                                                        <textarea class="form-control" id="observacionini" name="observacionini"  required="campo requerido"></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                                        <label>FECHA FIN</label>
                                                        <div class='input-group date' id='myDatepicker2'>
                                                            <input type='text' class="form-control" id="closed_time" name="closed_time"  required="campo requerido"/>
                                                            <span class="input-group-addon">
                                                               <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                                        <label>ESTADO FIN</label>
                                                        <select class="selectpicker form-control" id="estadofin" name="estadofin"  required="campo requerido"></select>
                                                    </div>
                                                </div>	
                                                <div class="form-group">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                        <label>OBSERVACION FIN</label>
                                                        <textarea class="form-control" id="observacionfin" name="observacionfin"  required="campo requerido"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="limpiar();">CANCELAR</button>
                                                <button type="submit" class="btn btn-primary" name="editarservicio" id="editarservicio">EDITAR</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <!-- Formulario modificar ascensor -->
                            <div id="mostrarguia" class="x_content">


                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /page content -->
            <hidden id="edit-modal" name="edit-modal" data-toggle="modal" data-target="#editar"></hidden>
            <!-- /page content -->
            <?php
        } else {
            require 'nopermiso.php';
        }
        require 'footer.php';
        ?>
        <script src="../public/build/js/libs/png_support/png.js"></script>
        <script src="../public/build/js/libs/png_support/zlib.js"></script>
        <script src="../public/build/js/jspdf.debug.js"></script>
        <script src="../public/build/js/jspdf.plugin.autotable.js"></script>
        <script src="../public/build/js/jsPDFcenter.js"></script>
        <script type="text/javascript" src="scripts/serpfirma.js"></script>
        <?php
    }
    ob_end_flush();
    ?>