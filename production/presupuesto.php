<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {

    require 'header.php';

    if ($_SESSION['administrador'] == 1 || $_SESSION['GPresupuesto'] == 1 || $_SESSION['APresupuesto'] == 1 ||  $_SESSION['GEServicios'] == 1) {
?>
        <link href="../public/build/css/loading.css" rel="stylesheet">
        <link href="../public/build/lib/lightbox/css/lightbox.css" rel="stylesheet">
        <style>
            .toolt {
                background: #333;
                color: white;
                font-weight: bold;
                padding: 4px 8px;
                font-size: 13px;
                border-radius: 4px;
                display: none;
            }

            .toolt[data-show] {
                display: block;
            }

            
        </style>
        <div id="cargando" class="loading"></div>
        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <!--<i class="fa fa-spin fa-refresh"></i>-->
                <div class="clearfix"></div>
                <div class="row">

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">

                                <div class="col-md-5 col-sm-12 col-xs-12">
                                    <h2 id="tituloPag" name="tituloPag">PRESUPUESTOS</h2>
                                </div>
                                <div id="filtros" class="dt-buttons btn-group">
                                    <p class="btn btn-xs buttons-copy buttons-html5">ESTADOS</p>
                                    <a id="eTodos" name="eTodos" aria-describedby="tooltip" class="btn btn-xs buttons-copy buttons-html5" style="color: white; background-color: blue;" onclick="listarxEstado('1,2,3,4,5,6');">TODOS</a>
                                    <a id="eSol" name="eSol" aria-describedby="tooltip" class="btn btn-xs buttons-copy buttons-html5" style="color:white; background-color: #f0ad4e;" onclick="listarxEstado('1');">SOLICITADO</a>
                                    <a id="eInf" name="eInf" class="btn btn-xs buttons-copy buttons-html5" style="color:white; background-color: #777777;" onclick="listarxEstado('2');">INFORMACION</a>
                                    <a id="eInfor2" name="eInfor2" class="btn btn-xs buttons-copy buttons-html5" style="color:white; background-color: #818080;" onclick="listarxEstado('3');">INFORMADO</a>
                                    <a id="eProc" name="eProc" class="btn btn-xs buttons-copy buttons-html5" style="color:white; background-color: #777777;" onclick="listarxEstado('4');">PROCESADO</a>
                                    <a id="eEnv" name="eEnv" class="btn btn-xs buttons-copy buttons-html5" style="color:white; background-color: #818080;" onclick="listarxEstado('5');">ENVIADO</a>
                                    <a id="eAcep" name="eAcep" class="btn btn-xs buttons-copy buttons-html5" style="color:white; background-color: #5cb85c;" onclick="listarxEstado('6');">ACEPTADO</a>
                                </div>

                                <div id="ttTodos" class="toolt" role="tooltip">0</div>
                                <div id="ttSol" class="toolt" role="tooltip">0</div>
                                <div id="ttInf" class="toolt" role="tooltip">0</div>
                                <div id="ttInfor2" class="toolt" role="tooltip">0</div>
                                <div id="ttProc" class="toolt" role="tooltip">0</div>
                                <div id="ttEnv" class="toolt" role="tooltip">0</div>
                                <div id="ttAcep" class="toolt" role="tooltip">0</div>

                                <div class="clearfix"></div>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-tooltip="tooltip" title="Operaciones" role="button" aria-expanded="false"><i class="fa fa-cog"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a id="op_listar" onclick="Actualizar()"><i class="fa fa-list-alt"></i> Actualizar</a>
                                            </li>
                                            <li><a id="op_historico" onclick="Historico()"><i class="fa fa-history"></i> Histórico</a>
                                            </li>
                                            <li><a id="op_listado" onclick="Lista()"><i class="fa fa-list"></i> Listar</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                    <label for="listasupervisor">Supervisor</label>
                                    <select class="form-control selectpicker" data-live-search="true" id="listasupervisor" name="listasupervisor" required="required"></select>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                    <label for="listaedificio">Edificio</label>
                                    <select class="form-control selectpicker" data-live-search="true" id="listaedificio" name="listaedificio" required="required">
                                        <option value="" disabled> Seleccione Opción</option>
                                    </select>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                    <label for="listaequipo">Equipo</label>
                                    <select class="form-control selectpicker" data-live-search="true" id="listaequipo" name="listaequipo" required="required">
                                        <option value="" disabled> Seleccione Opción</option>
                                    </select>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                    <label for="listaestadoquipo">Estado Equipo</label>
                                    <select class="form-control selectpicker" data-live-search="true" id="listaestadoquipo" name="listaestadoquipo" required="required">
                                        <option value="" disabled> Seleccione Opción</option>
                                    </select>
                                </div>
                            </div>
                            <div id="listadopre" class="x_content">

                                <table id="tblpre" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr class="thead-dark">
                                            <th ></th>
                                            <th >CREACIÓN</th>
                                            <th >N° SOLICITUD</th>
                                            <th >EDIFICIO</th>
                                            <th >EQUIPO</th>
                                            <th >SUPERVISOR</th>
                                            <th >ESTADO EQUIPO</th>
                                            <th >ESTADO</th>
                                            <th >ACTUALIZADO</th>
                                            <th >DÍAS</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>

                            <div id="listadopreHist" class="x_content">
                                <div class="form-group">
                                    <div class="col-md-2">
                                        <label>MES CREACIÓN</label>
                                        <select id="mes" name="mes" class="selectpicker form-control">
                                            <option value="0">TODOS</option>
                                            <option value="1">ENERO</option>
                                            <option value="2">FEBRERO</option>
                                            <option value="3">MARZO</option>
                                            <option value="4">ABRIL</option>
                                            <option value="5">MAYO</option>
                                            <option value="6">JUNIO</option>
                                            <option value="7">JULIO</option>
                                            <option value="8">AGOSTO</option>
                                            <option value="9">SEPTIEMBRE</option>
                                            <option value="10">OCTUBRE</option>
                                            <option value="11">NOVIEMBRE</option>
                                            <option value="12">DICIEMBRE</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <label>AÑO CREACIÓN</label>
                                        <select id="ano" name="ano" class="selectpicker form-control"></select>
                                    </div>
                                    <div class="col-md-2">
                                        <br>
                                        <button type="button" class="btn btn-info" onclick="armaHistorico();">BUSCAR</button>
                                    </div>
                                    <div class="col-md-8"></div>
                                    <div class="col-md-12">
                                        <br>
                                        <table id="tblpreHist" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                            <thead>
                                                <tr class="thead-dark">
                                                    <th ></th>
                                                    <th >CREACIÓN</th>
                                                    <th >N° SOLICITUD</th>
                                                    <th >EDIFICIO</th>
                                                    <th >EQUIPO</th>
                                                    <th >SUPERVISOR</th>
                                                    <th >ESTADO EQUIPO</th>
                                                    <th >ESTADO</th>
                                                    <th >ACTUALIZADO</th>
                                                    <th >Días</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <!-- Formulario cargar presupuesto -->
                            <div id="mostrarpre" class="x_content">
                                <br />
                                <form id="formulariopre" name="formulariopre" class="form-horizontal form-label-left input_mask">

                                    <h4><b>INFORMACION DE LA SOLICITUD</b></h4>
                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="idpresupuesto">NUMERO DE SOLICITUD</label>
                                        <input type="text" class="form-control" readonly="readonly" name="idpresupuesto" id="idpresupuesto">
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="fecha">FECHA Y HORA DE SOLICITUD</label>
                                        <input type="text" class="form-control" disabled="disabled" name="fecha" id="fecha">
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="edificio">EDIFICIO</label>
                                        <input type="text" class="form-control" disabled="disabled" name="edificio" id="edificio">
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="codigo">CODIGO FM</label>
                                        <input type="text" class="form-control" disabled="disabled" name="codigo" id="codigo">
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="equipo">EQUIPO</label>
                                        <input type="text" class="form-control" disabled="disabled" name="equipo" id="equipo">
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="descripcion">OBSERVACION DE LA SOLICITUD</label>
                                        <textarea type="text" id="descripcion" name="descripcion" disabled="disabled" class="resizable_textarea form-control"></textarea>
                                    </div>

                                    <h4><b>INFORMACION DE LA REVISION</b></h4>
                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="descripcionob">OBSERVACION DE LA REVISION</label>
                                        <textarea type="text" id="descripcionob" name="descripcionob" disabled="disabled" class="resizable_textarea form-control"></textarea>
                                    </div>

                                    <h4><b>INFORMACION DEL PRESUPUESTO</b></h4>

                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="npresupuesto">NUMERO DE PRESUPUESTO</label>
                                        <input type="number" class="form-control" name="npresupuesto" id="npresupuesto" required="Campo requerido">
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="descripcionpre">OBSERVACION DEL PRESUPUESTO</label>
                                        <textarea type="text" id="descripcionpre" name="descripcionpre" class="resizable_textarea form-control" style="text-transform: uppercase;" required="Campo requerido"></textarea>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="file">PRESUPUESTO (PDF)</label>
                                        <input id="file" name="file" type="file" accept="application/pdf" class="custom-file-input" required="Campo requerido">
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <button class="btn btn-primary" type="button" id="btnCancelarFormPre" onclick="cancelarformpre()">Cancelar</button>
                                            <button class="btn btn-success" type="submit" id="btnProcesar">Procesar</button>
                                        </div>
                                    </div>
                                </form>

                            </div>

                            <!-- Formulario carga orden de compra -->
                            <div id="mostrarapro" class="x_content">
                                <br />
                                <form id="formularioapro" name="formularioapro" class="form-horizontal form-label-left input_mask">

                                    <h4><b>INFORMACION DE LA SOLICITUD</b></h4>
                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="idpresupuestop">NUMERO DE SOLICITUD</label>
                                        <input type="text" class="form-control" readonly="readonly" name="idpresupuestop" id="idpresupuestop">
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="fechap">FECHA Y HORA DE SOLICITUD</label>
                                        <input type="text" class="form-control" readonly="readonly" name="fechap" id="fechap">
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="edificiop">EDIFICIO</label>
                                        <input type="text" class="form-control" disabled="disabled" name="edificiop" id="edificiop">
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="codigop">CODIGO FM</label>
                                        <input type="text" class="form-control" disabled="disabled" name="codigop" id="codigop">
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="equipop">EQUIPO</label>
                                        <input type="hidden" id="idascensorp" name="idascensorp" class="form-control">
                                        <input type="text" class="form-control" disabled="disabled" name="equipop" id="equipop">
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="descripcionp">OBSERVACION DE LA SOLICITUD</label>
                                        <textarea type="text" id="descripcionp" name="descripcionp" disabled="disabled" class="resizable_textarea form-control"></textarea>
                                    </div>

                                    <h4><b>INFORMACION DE LA REVISION</b></h4>
                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="descripcionobp">OBSERVACION DE LA REVISION</label>
                                        <textarea type="text" id="descripcionobp" name="descripcionobp" disabled="disabled" class="resizable_textarea form-control"></textarea>
                                    </div>

                                    <h4><b>INFORMACION DEL PRESUPUESTO</b></h4>
                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="descripcionprep">OBSERVACION DEL PRESUPUESTO</label>
                                        <textarea type="text" id="descripcionprep" name="descripcionprep" disabled="disabled" class="resizable_textarea form-control"></textarea>
                                    </div>

                                    <h4><b>INFORMACION DE APROBACION</b></h4>

                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="orden">NUMERO DE ORDEN DE COMPRA</label>
                                        <input type="text" class="form-control" name="orden" id="orden" required="Campo requerido">
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="descripcionapro">OBSERVACION DE LA APROBACION</label>
                                        <textarea type="text" id="descripcionapro" name="descripcionapro" class="resizable_textarea form-control" required="Campo requerido"></textarea>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="fileapro">ORDEN DE COMPRA (PDF)</label>
                                        <input id="fileapro" name="fileapro" type="file" accept="application/pdf" class="custom-file-input" required="Campo requerido">
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <button class="btn btn-primary" type="button" id="btnCancelarFormApro" onclick="cancelarformapro()">Cancelar</button>
                                            <button class="btn btn-success" type="submit" id="btnAprobar">Aprobar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>


                            <!-- Formulario solicitud de informacion -->
                            <div id="mostrarsol" class="x_content">
                                <br />
                                <form id="formulariosol" name="formulariosol" class="form-horizontal form-label-left input_mask">

                                    <h3><b>INFORMACION DE LA SOLICITUD</b></h3>
                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="idpresupuesto">NUMERO DE SOLICITUD</label>
                                        <input type="text" class="form-control" readonly="readonly" name="idpresupuestos" id="idpresupuestos">
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="fecha">FECHA Y HORA DE SOLICITUD</label>
                                        <input type="text" class="form-control" disabled="disabled" name="fechas" id="fechas">
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="edificio">EDIFICIO</label>
                                        <input type="text" class="form-control" disabled="disabled" name="edificios" id="edificios">
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="codigo">CODIGO FM</label>
                                        <input type="text" class="form-control" disabled="disabled" name="codigos" id="codigos">
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="equipo">EQUIPO</label>
                                        <input type="text" class="form-control" disabled="disabled" name="equipos" id="equipos">
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="descripcion">OBSERVACION DE LA SOLICITUD</label>
                                        <textarea type="text" id="descripcions" name="descripcions" disabled="disabled" class="resizable_textarea form-control"></textarea>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for="soliinfo">SOLICITUD DE INFORMACION AL SUPERVISOR</label>
                                        <textarea type="text" id="soliinfo" name="soliinfo" class="resizable_textarea form-control" style="text-transform: uppercase;" required="Campo requerido"></textarea>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <button class="btn btn-primary" type="button" id="btnCancelarForm" onclick="cancelarformpre()">Cancelar</button>
                                            <button class="btn btn-success" type="submit" id="btnSolicitar">Solicitar</button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- /page content -->

        <div id="modalMotivo" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="min-width:60%;">
                <form id="Motivoform" class="form-horizontal calender" role="form" enctype="multipart/form-data">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" id="myModalLabel">INGRESE MOTIVO PARA INHABILITAR</h4>
                        </div>
                        <div class="modal-body">
                            <div id="testmodal" style="padding: 5px 10px;">
                                <div class="form-group">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="hidden" id="idpres" name="idpres" class="form-control">
                                        <label>MOTIVO</label>
                                        <select class="form-control selectpicker" data-live-search="true" id="motivoInhab" name="motivoInhab" required="Campo requerido">
                                            <option value="" selected disabled>SELECCIONE MOTIVO</option>
                                            <option value="1">ANULACIÓN</option>
                                            <option value="2">DUPLICADO</option>
                                            <option value="3">GARANTÍA</option>
                                            <option value="4">PRESUPUESTADO</option>
                                        </select>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <label>COMENTARIO</label>                                        
                                        <textarea class="form-control" style="height:35px; width:100%; text-transform: uppercase; resize:none;" id="comentarioMotivo" name="comentarioMotivo" placeholder="INGRESE COMENTARIO" minlength="30" maxlength="200"></textarea>                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default antoclose" data-dismiss="modal">CANCELAR</button>
                            <button type="submit" id="enviar" class="btn btn-primary">GUARDAR</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div id="modalImagenes" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="min-width:60%;">
                <form id="Motivoform" class="form-horizontal calender" role="form" enctype="multipart/form-data">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" id="myModalLabel">IMAGENES ASOCIADAS</h4>
                        </div>
                        <div class="modal-body">
                            <div id="testmodal" style="padding: 5px 10px;">
                                <div class="form-group">
                                <div id="divimagenes" ></div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default antoclose" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>



        <div id="btnMotivo" data-toggle="modal" data-target="#modalMotivo"></div>
    <?php
    } else {
        require 'nopermiso.php';
    }
    require 'footer.php';
    ?>
    <script src="../public/build/js/libs/png_support/png.js"></script>
    <script src="../public/build/js/libs/png_support/zlib.js"></script>
    <script src="../public/build/js/jspdf.debug.js"></script>
    <script src="../public/build/js/jspdf.plugin.autotable.js"></script>
    <script src="../public/build/js/jsPDFcenter.js"></script>        
    <script src="../public/build/lib/lightbox/js/lightbox.js"></script>        
    <script type="text/javascript" src="scripts/presupuesto.js"></script>
<?php
}
ob_end_flush();
?>
