<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {

    require 'header.php';

    if ($_SESSION['administrador'] == 1 || $_SESSION['Comercial'] == 1) {
        ?>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>MEMOS DE VENTA</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-tooltip="tooltip" title="Operaciones" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-cog"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a id="op_actualizar" onclick="mostrarform(true);">Agregar</a></li>
                                            <li><a id="op_listar" onclick="cancelarform();">Volver</a></li>
                                        </ul>
                                    </li>                     
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div id="listadoproyectos" class="x_content">

                                                        <!--<table id="tblproyectos" class="table table-striped border border-gray-dark projects dt-responsive" cellspacing="0" width="100%">-->
                                <table id="tblproyectos" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th style="width:11%;">#</th>
                                            <th style="width:5%;">CODIGO</th>
                                            <th>PROYECTO</th>
                                            <th>DIRECIÓN</th>
                                            <th>CLIENTE</th>    
                                            <th style="width:8%;">RUT</th>
                                            <th>C. COSTO</th>
                                            <th>IMPORTACION</th>
                                            <th>VENDEDOR</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>

                            <div id="formularioproyecto" class="x_panel">
                                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                    <ul id="myTab" class="nav nav-tabs nav-justified" role="tablist">
                                        <li role="presentation" id="step-11" class="active" style="background-color: #E5FFE3; opacity: 30%;">
                                            <a href="#step-1" id="venta" role="tab" data-toggle="tab" aria-expanded="true">
                                                <i class="glyphicon glyphicon-ok"></i> VENTA
                                            </a>
                                        </li>
                                        <li role="presentation" class="" id="step-22" style="background-color: #FFE3E3; opacity: 30%;">
                                            <a href="#" id="garantia" role="tab" data-toggle="tab" aria-expanded="true">
                                                <i class="glyphicon glyphicon-remove"></i> GARANTIAS
                                            </a>
                                        </li>
                                        <li role="presentation" class="" id="step-33" style="background-color: #FFE3E3; opacity: 30%;">
                                            <a href="#" id="proyecto" role="tab" data-toggle="tab" aria-expanded="true">
                                                <i class="glyphicon glyphicon-remove"></i> PROYECTO
                                            </a>
                                        </li>
                                        <li role="presentation" id="step-44" class="" style="background-color: #FFE3E3; opacity: 30%;">
                                            <a href="#" role="tab" id="ascensor" data-toggle="tab" aria-expanded="false">
                                                <i class="glyphicon glyphicon-remove"></i> ASCENSORES
                                            </a>
                                        </li>
                                        <li role="presentation" class="" id="step-55" style="background-color: #FFE3E3; opacity: 30%;">
                                            <a href="#" role="tab" id="info" data-toggle="tab" aria-expanded="false">
                                                <i class="glyphicon glyphicon-remove"></i> INSTALACIÓN
                                            </a>
                                        </li>
                                        <li role="presentation" class="" id="step-66"  style="background-color: #FFE3E3; opacity: 30%;">
                                            <a href="#" role="tab" id="pago" data-toggle="tab" aria-expanded="false">
                                                <i class="glyphicon glyphicon-remove"></i> PAGOS
                                            </a>
                                        </li>
                                        <!--<li role="presentation" class="" id="step-77" style="background-color: #FFE3E3; opacity: 30%;">
                                            <a href="#" role="tab" id="revision" data-toggle="tab" aria-expanded="false">
                                                <i class="glyphicon glyphicon-remove"></i> REVISIÓN
                                            </a>
                                        </li>-->
                                    </ul>
                                    <div id="myTabContent" class="tab-content" style="display: none;">
                                        <div id="step-1" role="tabpanel" class="tab-pane fade active in" >
                                            <div class="x_title">
                                                <h3 style="text-align: center;">INFORMACIÓN DE VENTA</h3>
                                            </div>
                                            <form class="form-horizontal" name="formularioVenta" id="formularioVenta">
                                                <div class="x_content">
                                                    <div class="form-horizontal">
                                                        <div class="col-md-12 center-margin x_panel">
                                                            <div class="form-group">
                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                    <input type="hidden" name="venta_idventa" id="venta_idventa">
                                                                    <label class="control-label">TIPO VENTA <span class="required">*</span></label>
                                                                    <select class="form-control selectpicker" id="venta_tipo" name="venta_tipo" required="required">
                                                                        <option value="">Seleccione una opción</option>
                                                                        <option value="1">NUEVA INSTALACIÓN</option>
                                                                        <option value="2">MODERNIZACIÓN TOTAL</option>
                                                                        <option value="3">MODERNIZACION PARCIAL</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-3 col-sm-3 col-xs-12">
                                                                    <label class="control-label">FECHA </label>
                                                                    <div class="col-md-12 xdisplay_inputx form-group has-feedback">
                                                                        <input type="text" class="form-control has-feedback-left" id="venta_fecha" name="venta_fecha" aria-describedby="inputSuccess2Status2">
                                                                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                                                    </div>
                                                                    <!--<input type="texto" class="date-picker form-control" id="venta_fecha" name="venta_fecha" required="required">
                                                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>-->
                                                                </div>
                                                                <div class="col-md-3 col-sm-3 col-xs-12">
                                                                    <label class="control-label">N° VENTA </label>
                                                                    <input type="text" class="date-picker form-control" id="venta_codigo" name="venta_codigo" required="required" data-inputmask="'mask' : '999/99'">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                    <label class="control-label">CENTRO DE COSTO<span class="required">*</span></label>
                                                                    <select class="selectpicker select2_single form-control" id="venta_cc" name="venta_cc" data-live-search="true" required="required"></select>
                                                                </div>
                                                                <!--<div class="col-md-1 col-sm-6 col-xs-12">
                                                                    <label>&nbsp;</label><br>
                                                                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#agregacc">
                                                                        <i class="fa fa-plus-circle"></i>
                                                                    </button>

                                                                </div>-->
                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                    <label class="control-label">NRO. PEDIDO <span class="required">*</span></label>
                                                                    <select class="selectpicker select2_single form-control" id="venta_pedido" name="venta_pedido" data-live-search="true" required="required"></select>
                                                                </div>
                                                                <!--<div class="col-md-1 col-sm-6 col-xs-12">
                                                                    <label>&nbsp;</label><br>
                                                                    <button type="button" name="addimp" id="addimp" class="btn btn-warning" data-toggle="modal" data-target="#agregaimp"><i class="fa fa-plus-circle"></i></button>

                                                                </div>-->
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-md-3 col-sm-3 col-xs-12">
                                                                    <label class="control-label">MONTO SUMINISTRO IMPORTADO</label>
                                                                    <input type="text" class="form-control" id="venta_montoSI" required="" name="venta_montoSI" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'placeholder': '0'">
                                                                </div>
                                                                <div class="col-md-3 col-sm-3 col-xs-12">
                                                                    <label class="control-label">MONEDA SUMINISTRO IMPORTADO</label>
                                                                    <select class="form-control selectpicker" id="venta_monedaSI" name="venta_monedaSI" data-live-search="true" required=""></select>
                                                                </div>
                                                                <div class="col-md-3 col-sm-3 col-xs-12">
                                                                    <label class="control-label">MONTO SUMINISTRO NACIONAL</label>
                                                                    <input type="text" class="form-control" id="venta_montoSN" required="" name="venta_montoSN" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'placeholder': '0'">
                                                                </div>
                                                                <div class="col-md-3 col-sm-3 col-xs-12">
                                                                    <label class="control-label">MONEDA SUMINISTRO NACIONAL</label>
                                                                    <select class="form-control selectpicker" id="venta_monedaSN" name="venta_monedaSN" data-live-search="true" required=""></select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-md-3 col-sm-3 col-xs-12">
                                                                    <label class="control-label">CONTRATO<span class="required">*</span></label>
                                                                    <input type="checkbox" value="1" name="venta_contrato" id="venta_contrato" class="flat">
                                                                </div>
                                                                <div class="col-md-3 col-sm-3 col-xs-12">
                                                                    <label class="control-label">RETENCIONES <span class="required">*</span></label>
                                                                    <input type="checkbox" value="1"  id="venta_retencion" name="venta_retencion" class="flat">
                                                                </div>
                                                                <div class="col-md-3 col-sm-3 col-xs-12">
                                                                    <label class="control-label">MULTAS<span class="required">*</span></label>
                                                                    <input type="checkbox" value="1" name="venta_multa" id="venta_multa" class="flat">
                                                                </div>
                                                                <div class="col-md-3 col-sm-3 col-xs-12">
                                                                    <label class="control-label">ORDEN DE COMPRA <span class="required">*</span></label>
                                                                    <input type="checkbox" value="1" id="venta_ocompra" name="venta_ocompra" class="flat" >
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-md-6 col-sm-4 col-xs-12">
                                                                    <label class="control-label">FECHA PERMISO DE EDIFICACIÓN</label>
                                                                    <div class="col-md-12 xdisplay_inputx form-group has-feedback">
                                                                        <input type="text" class="form-control has-feedback-left" id="venta_fecedifica" name="venta_fecedifica" aria-describedby="inputSuccess2Status2">
                                                                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                                                    </div>
                                                                </div>
                                                            </div> 

                                                            <div class="col-md-6 well">
                                                                <div class="col-md-12 col-sm-12 col-xs-12" id="">
                                                                    <label class="control-label">GARANTÍA</label>
                                                                    <input type="checkbox" value="1" name="op_garantia" id="op_garantia" class="js-switch form-control">
                                                                </div>
                                                                <br>
                                                                <div class="clearfix"></div>
                                                                <div id="meses_garantia" style="display: none;">
                                                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                                        <label class="control-label">CANTIDAD DE MESES</label>
                                                                        <input type="text" id="mes_garantia" name="mes_garantia" class="form-control">
                                                                        <input type="hidden" value="0" id="venta_garantiaex" name="venta_garantiaex">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 well">
                                                                <div class="col-md-12 col-sm-12 col-xs-12" id="">
                                                                    <label class="control-label">MANTENCIÓN</label>
                                                                    <input type="checkbox" value="1" name="op_mantencion" id="op_mantencion" class="js-switch form-control">
                                                                </div>
                                                                <br>
                                                                <div class="clearfix"></div>
                                                                <div id="meses_mantencion" style="display: none;">
                                                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                                        <label class="control-label">CANTIDAD DE MESES</label>
                                                                        <input type="text" class="form-control" name="mes_mantencion" id="mes_mantencion">
                                                                        <input type="hidden" value="0" id="venta_mantencionpost" name="venta_mantencionpost">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 center-margin x_panel">
                                                            <div class="col-md-12 col-sm-12 col-xs-12" id="">
                                                                <label class="control-label">ADICIONALES<span class="required">*</span></label>
                                                                <input type="checkbox" value="1" name="op_adicionales" id="op_adicionales" class="js-switch form-control">
                                                            </div>
                                                            <br>
                                                            <div class="clearfix"></div>
                                                            <div id="adi" style="display: none;">
                                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                                    <label class="control-label">OPCIONES ADICIONALES</label>
                                                                    <textarea class="form-control" name="venta_adicionales" id="venta_adicionales"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <div class="form-group">
                                                            <div class="x_panel">
                                                                <legend>DATOS DE CLIENTE</legend>
                                                                <div class="form-group">
                                                                    <div class="col-md-11 col-sm-12 col-xs-10 form-group">
                                                                        <select class="form-control selectpicker" data-live-search="true" id="venta_idmandante" name="venta_idmandante" required="required"></select>
                                                                    </div>
                                                                    <div class="col-md-1 col-sm-12 col-xs-12 form-group">
                                                                        <button class="btn btn-warning btn-sm" type="button" data-toggle="modal" data-target="#modalForm"><i class="fa fa-plus-circle"></i></button>
                                                                    </div>
                                                                    <div class="col-md-12 col-sm-12 col-xs-12 right">
                                                                        <h5>Representantes</h5>
                                                                        <table class="table table-bordered" id="informacionrepresentantes">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>NOMBRE</th>
                                                                                    <th>RUT</th>
                                                                                    <th>EMAIL</th>
                                                                                    <th>TELEFONO</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody></tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 right">
                                                        <button class="btn btn-primary" type="button" id="btnCancelar" onclick="cancelarform();">Volver al listado</button>
                                                        <!--<button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiartodo()">Limpiar</button>-->
                                                        <button class="btn btn-success" type="submit" id="btnGuardar">Guardar datos</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div id="step-2" role="tabpanel" class="tab-pane fade" >
                                            <div id="formularioascensor" class="x_panel">
                                                <div class="x_title">
                                                    <h3 style="text-align: center;">GARANTÍAS</h3>
                                                </div>
                                                <form class="form-horizontal" id="formularioGarantia" name="formularioGarantia">
                                                    <div class="col-md-12 center-margin x_content">

                                                        <div class="form-group">
                                                            <fieldset class="well">
                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="col-md-4">
                                                                        <label class="control-label">DESCRIPCION</label>
                                                                        <select class="form-control selectpicker" name="garantia_descripcion" id="garantia_descripcion" required></select>
                                                                        <!--<input type="text" name="garantia_descripcion" id="garantia_descripcion" class="form-control" style='text-transform:uppercase;'>-->
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <label class="control-label">DOCUMENTO</label>
                                                                        <select class="form-control selectpicker" name="garantia_documento" id="garantia_documento" required></select>
                                                                        <!--<input type="text" name="garantia_documento" id="garantia_documento" class="form-control" style='text-transform:uppercase;'>-->
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <label class="control-label">EMISOR</label>
                                                                        <input type="text" name="garantia_banco" id="garantia_banco" class="form-control" style='text-transform:uppercase;'>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <label class="control-label">PAGO</label>
                                                                        <select class="form-control selectpicker" name="garantia_tipo" id="garantia_tipo" required></select>
                                                                        <!--<input type="text" name="garantia_tipo" id="garantia_tipo" class="form-control" style='text-transform:uppercase;'>-->
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <label class="control-label">VALIDEZ</label>
                                                                        <input type="text" name="garantia_validez" id="garantia_validez" class="form-control" required>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                    <br>    
                                                                    <button name="addgarantia" id="addgarantia" style="float: right;" type="submit" class="btn btn-success">Agregar</button>
                                                                </div>
                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                    <br>    
                                                                    <table id="tblboletas" name="tblboletas" class="table">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>DESCRIPCION</th>
                                                                                <th>DOCUMENTO</th>
                                                                                <th>BANCO</th>
                                                                                <th>PAGO</th>
                                                                                <th>VALIDEZ</th>
                                                                                <th>#</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody></tbody>
                                                                    </table>
                                                                </div>
                                                            </fieldset>
                                                        </div>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <div class="col-md-12 col-sm-12 col-xs-12 right">
                                                            <button class="btn btn-primary" type="button" id="btnCancelar" onclick="cancelarform();">volver al listado</button>
                                                            <!--<button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiartodo()">Limpiar</button>
                                                            <button class="btn btn-success" type="submit" id="btnGuardar2">Agregar</button>-->
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div id="step-3" role="tabpanel" class="tab-pane fade">
                                            <div class="x_title">
                                                <h3 style="text-align: center;">DATOS DE PROYECTO</h3>
                                            </div>
                                            <form class="form-horizontal form-label-left" id="formproyecto">
                                                <div class="form-group">
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input type="hidden" id="proyecto_idproyecto" name="proyecto_idproyecto">
                                                        <label class="control-label">NOMBRE <span class="required">*</span></label>
                                                        <input class="form-control" id="proyecto_nombre" name="proyecto_nombre" style='text-transform:uppercase;'>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <label class="control-label">TIPO PROYECTO </label>
                                                        <select id="proyecto_tipo" class="form-control selectpicker" type="text" name="proyecto_tipo">
                                                            <option>SELECCIONE OPCION</option>
                                                            <option value="1">INSTALACIÓN</option>
                                                            <option value="2">MODERNIZACIÓN</option>
                                                        </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <label class="control-label">SEGMENTO<span class="required">*</span></label>
                                                            <select class="selectpicker form-control" data-live-search="true"  id="proyecto_idtsegmento" name="proyecto_idtsegmento" required="required"></select>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <label class="control-label">CLASIFICACIÓN<span class="required">*</span></label>
                                                            <select class="selectpicker form-control" data-live-search="true"  id="proyecto_idtclasificacion" name="proyecto_idtclasificacion" required="required"></select>
                                                        </div>
                                                    </div>
                                                    <!--<div class="col-md-3 col-sm-6 col-xs-12">
                                                        <label class="control-label">#CC (centro de costo)<span class="required">*</span></label>-->
                                                    <input type="hidden" class="form-control" id="proyecto_codigo" name="proyecto_codigo">
                                                    <!--</div>-->
                                                    <div class="form-group">
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <label class="control-label">REGIÓN<span class="required">*</span></label>
                                                            <select class="selectpicker form-control" data-live-search="true"  id="proyecto_region" name="proyecto_region" required="required"></select>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <label class="control-label">COMUNA<span class="required">*</span></label>
                                                            <select class="selectpicker form-control" data-live-search="true"  id="proyecto_comuna" name="proyecto_comuna" required="required"></select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <label class="control-label">CALLE<span class="required">*</span></label>
                                                            <input type="text" class="form-control" id="proyecto_calle" name="proyecto_calle" style='text-transform:uppercase;'>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <label class="control-label">NUMERO<span class="required">*</span></label>
                                                            <input type="text" class="form-control" id="proyecto_numero" name="proyecto_numero">
                                                        </div>
                                                    </div>                                                                 
                                                </div>
                                                <div class="x_title">
                                                    <h3 style="text-align: center;">CONTACTO DE OBRA</h3>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-9 col-sm-6 col-xs-12">
                                                        <label class="control-label">NOMBRE Y APELLIDO<span class="required">*</span></label>
                                                        <input type="text" class="form-control" id="con_nom_obra" name="con_nom_obra" style='text-transform:uppercase;' required="Campo Requerido">
                                                    </div>
                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                        <label class="control-label">RUT<span class="required">*</span></label>
                                                        <input type="text" class="form-control" id="con_rut_obra" name="con_rut_obra" style='text-transform:uppercase;'  required="Campo Requerido" data-inputmask="'mask' : '99.999.999-*'">
                                                    </div>
                                                    <div class="col-md-9 col-sm-6 col-xs-12">
                                                        <label class="control-label">EMAIL</label>
                                                        <input type="text" class="form-control" id="con_email_obra" name="con_email_obra" style='text-transform:uppercase;'>
                                                    </div>
                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                        <label class="control-label">TELEFONO</label>
                                                        <input type="text" class="form-control" id="con_telefono_obra" name="con_telefono_obra" data-inputmask="'mask' : '+56(9)9999-9999'">
                                                    </div>
                                                </div>
                                                <div class="x_title">
                                                    <h3 style="text-align: center;">CONTACTO DE VENTAS</h3>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-9 col-sm-6 col-xs-12">
                                                        <label class="control-label">NOMBRE Y APELLIDO<span class="required">*</span></label>
                                                        <input type="text" class="form-control" id="con_nom_ven" name="con_nom_ven" style='text-transform:uppercase;' required="Campo Requerido">
                                                    </div>
                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                        <label class="control-label">RUT<span class="required">*</span></label>
                                                        <input type="text" class="form-control" id="con_rut_ven" name="con_rut_ven" style='text-transform:uppercase;'  required="Campo Requerido" data-inputmask="'mask' : '99.999.999-*'">
                                                    </div>
                                                    <div class="col-md-9 col-sm-6 col-xs-12">
                                                        <label class="control-label">EMAIL</label>
                                                        <input type="email" class="form-control" id="con_email_ven" name="con_email_ven" style='text-transform:uppercase;'>
                                                    </div>
                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                        <label class="control-label">TELEFONO</label>
                                                        <input type="text" class="form-control" id="con_telefono_ven" name="con_telefono_ven" data-inputmask="'mask' : '+56(9)9999-9999'">
                                                    </div>
                                                </div>      
                                                <div class="modal-footer">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 left">
                                                        <button class="btn btn-primary" type="button" id="btnCancelar" onclick="cancelarform();"> Volver al listado</button>
                                                        <button class="btn btn-success" type="submit" id="btnGuardar3">Guardar datos</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div id="step-4" role="tabpanel" class="tab-pane fade">
                                            <div id="formularioascensor" class="x_panel">
                                                <div class="x_title">
                                                    <h3 style="text-align: center;">ASCENSORES</h3>
                                                </div>
                                                <form class="form-horizontal form-label-left" id="formAscensor">
                                                    <div class="col-md-12 center-margin x_content">

                                                        <div class="col-md-12 col-sm-12 col-xs-12" id="cartera">
                                                            <label class="control-label">EN CARTERA<span class="required">*</span></label>
                                                            <input type="checkbox" value="1" name="venta_cartera" id="venta_cartera" class="js-switch form-control">
                                                        </div>
                                                        <br>
                                                        <div class="clearfix"></div>
                                                        <br>
                                                        <div id="nuevo">
                                                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                                                <label>TIPO EQUIPO <span class="required">*</span></label>
                                                                <input type="hidden" id="ascensor_idascensor" name="ascensor_idascensor" class="form-control">
                                                                <select class="form-control selectpicker" data-live-search="true" id="ascensor_idtascensor" name="ascensor_idtascensor" required="Requerido"></select>
                                                            </div>
                                                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                                                <label>MARCA <span class="required">*</span></label>
                                                                <select class="form-control selectpicker" data-live-search="true" id="ascensor_marca" name="ascensor_marca"  required="Requerido"></select>
                                                            </div>
                                                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                                                <label>MODELO<span class="required">*</span></label>
                                                                <select class="form-control selectpicker" data-live-search="true" id="ascensor_modelo" name="ascensor_modelo" required="Requerido"></select>
                                                            </div>
                                                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                                                <label for="ascensor_codigo">CODIGO FM <small>(si queda vacio toma uno automático)</small></label>
                                                                <input type="text" id="ascensor_codigo" name="ascensor_codigo" class="form-control" maxlength="10" data-inputmask="'mask': 'FM999999'"/>
                                                            </div>

                                                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                                                <label>COMANDO<span class="required">*</span></label>
                                                                <select class="form-control selectpicker" data-live-search="true" id="ascensor_comando" name="ascensor_comando" required="Requerido"></select>
                                                            </div>

                                                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                                                <label>KEN</label>
                                                                <input type="text" class="form-control" name="ascensor_ken" id="ascensor_ken" maxlength="45" data-inputmask="'mask': '99999999'">
                                                            </div>
                                                            <!--<div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                                                <label for="pservicio">CODIGO FM</label>
                                                                <input type="text" id="ascensor_codigo" name="ascensor_codigo" class="form-control"  required="Requerido"/>
                                                            </div>-->
                                                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                                                <label>PARADAS </label>
                                                                <input type="text" class="form-control" name="ascensor_paradas" id="ascensor_paradas" required="Requerido">
                                                            </div>
                                                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                                                <label>ACCESOS</label>
                                                                <input type="text" class="form-control" name="ascensor_accesos" id="ascensor_accesos" required="Requerido">
                                                            </div>
                                                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                                                <label>CAPACIDAD (KG) </label>
                                                                <input type="text" class="form-control" name="ascensor_capkg" id="ascensor_capkg" required="Requerido">
                                                            </div>
                                                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                                                <label>CAPACIDAD PERSONAS </label>
                                                                <input type="text" class="form-control" name="ascensor_capper" id="ascensor_capper" required="Requerido">
                                                            </div>
                                                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                                                <label>VELOCIDAD </label>
                                                                <input type="text" class="form-control" name="ascensor_velocidad" id="ascensor_velocidad" required="Requerido" data-inputmask="'mask': '9,99'">
                                                            </div>
                                                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                                                <label>VALOR SUM. IMPORTADO </label>
                                                                <input type="text" class="form-control" name="ascensor_valorSI" id="ascensor_valorSI" required="Requerido" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'placeholder': '0'">
                                                            </div>
                                                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                                                <label>MONEDA SUM. IMPORTADO</label>
                                                                <input type="text" class="form-control" name="ascensor_monedaSI" id="ascensor_monedaSI" readonly="" >
                                                            </div>
                                                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                                                <label>VALOR SUM. NACIONAL </label>
                                                                <input type="text" class="form-control" name="ascensor_valorSN" id="ascensor_valorSN" required="Requerido" data-inputmask="'alias': 'numeric',  'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'placeholder': '0'">
                                                            </div>
                                                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                                                <label>MONEDA SUM. NACIONAL </label>
                                                                <input type="text" class="form-control" name="ascensor_monedaSN" id="ascensor_monedaSN" readonly="">
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                                <button name="addascensor" id="addascensor" style="float: right;" type="submit" class="btn btn-success">Agregar</button>
                                                            </div>
                                                        </div>
                                                        <div id="cartera2" style="display: none;">
                                                            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                                <label class="control-label">Listado Ascensores</label>
                                                                <select class="selectpicker form-control select2_single" data-live-search="true" id="listaAsc" name="listaAsc"></select>
                                                            </div>
                                                            <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                                                <label>VALOR SUM. IMPORTADO </label>
                                                                <input type="text" class="form-control" name="ascensor_valorSI1" id="ascensor_valorSI1">
                                                            </div>
                                                            <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                                                <label>MONEDA</label>
                                                                <input type="text" class="form-control" name="ascensor_monedaSI1" id="ascensor_monedaSI1" readonly="">
                                                            </div>

                                                            <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                                                <label>VALOR SUM. NACIONAL </label>
                                                                <input type="text" class="form-control" name="ascensor_valorSN1" id="ascensor_valorSN1">
                                                            </div>
                                                            <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                                                <label>MONEDA</label>
                                                                <input type="text" class="form-control" name="ascensor_monedaSN1" id="ascensor_monedaSN1" readonly="">
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                                <button disabled name="addascensor" id="addascensor" style="float: right;" type="button" class="btn btn-success" onclick="agregarAscensorexiste();">Agregar</button>
                                                            </div>
                                                        </div>

                                                        <div class="clearfix"></div>
                                                        <hr>
                                                        <div class="x_content">
                                                            <h3>Listado Ascensores</h3>
                                                            <table id="tblascensores" class="table table-bordered">
                                                                <thead>
                                                                    <tr>
                                                                        <th>NUM</th>
                                                                        <th>TIPO</th>
                                                                        <th>MARCA - MODELO</th>
                                                                        <th>COMANDO</th>
                                                                        <th>CODIGO</th>
                                                                        <th>KEN</th>
                                                                        <th>PARADAS</th>
                                                                        <th>CAPACIDAD (KG)</th>
                                                                        <th>VELOCIDAD</th>
                                                                        <th>SUM. IMPORTADO</th>
                                                                        <th>SUM. NACIONAL</th>
                                                                        <th>#</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody></tbody>
                                                                <tfoot>
                                                                    <tr>
                                                                        <th></th>
                                                                        <th></th>
                                                                        <th></th>
                                                                        <th></th>
                                                                        <th></th>
                                                                        <th></th>
                                                                        <th></th>
                                                                        <th></th>
                                                                        <th>TOTAL</th>
                                                                        <th id="totSI">0</th>
                                                                        <th id="totSN">0</th>
                                                                        <th></th>
                                                                    </tr>
                                                                </tfoot>
                                                            </table>
                                                            <div class="clearfix"></div>
                                                        </div>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <div class="col-md-12 col-sm-12 col-xs-12 right">
                                                            <button class="btn btn-primary" type="button" id="btnCancelar" onclick="cancelarform();">Volver al listado</button>
                                                            <!--<button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiartodo()">Limpiar</button>-->
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div id="step-5" role="tabpanel" class="tab-pane fade">
                                            <div class="x_panel">
                                                <div class="x_title">
                                                    <h3 style="text-align: center;">INFORMACIÓN CONTRACTUAL DE INSTALACION</h3>
                                                </div>
                                                <div class="x_content">
                                                    <div class="form-group">    
                                                        <div id="fechasascensores">

                                                        </div>
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <button name="addinstalacion" id="addinstalacion" style="float: right;" type="button" class="btn btn-round btn-warning btn-lg" onclick="addinfo();"><i class="fa fa-plus-circle"></i></button>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <br>
                                                </div>
                                                <div class="modal-footer">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 right">
                                                        <button class="btn btn-primary" type="button" id="btnCancelar" onclick="cancelarform();">Volver al listado</button>
                                                        <!--<button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiartodo()">Limpiar</button>-->
                                                        <button class="btn btn-success" type="button" id="btnGuardar" onclick="agregarinfo();">Guardar datos</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="step-6" role="tabpanel" class="tab-pane fade">
                                            <div class="x_panel">
                                                <div class="x_title">
                                                    <h3 style="text-align: center;">FORMA DE PAGO CONTRACTUAL</h3>
                                                </div>
                                                <form class="form form-horizontal" id="formpago">
                                                    <div class="x_content">
                                                        <div class="x_panel">
                                                            <legend>SUMINISTRO INTERNACIONAL</legend>
                                                            <div class="form-group">
                                                                <div class="col-md-7 col-sm-12 col-xs-12">
                                                                    <label>DESCRIPCIÓN</label>
                                                                    <select class="form-control selectpicker" name="si_desc" id="si_desc"></select>
                                                                </div>
                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                    <label>PORCENTAJE (%)</label>
                                                                    <input type="text" id="si_porc" name="si_porc" class="form-control"/>
                                                                </div>
                                                                <div class="col-md-2 col-sm-12 col-xs-12">
                                                                    <label for="pservicio">&nbsp;</label>
                                                                    <br>
                                                                    <button name="addsi" id="addsi" style="float: right;" type="button" class="btn btn-success" onclick="agregarSI();">Agregar</button>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <hr>
                                                            <div class="x_content">
                                                                <h4>PARTE IMPORTADA</h4>
                                                                <table id="tblaSI" class="table table-bordered">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>DESCRIPCIÓN</th>
                                                                            <th>PORCENTAJE (%)</th>
                                                                            <th>#</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody></tbody>
                                                                    <tfoot>
                                                                        <tr>
                                                                            <th>TOTAL (%)</th>
                                                                            <th id="sumSI">100</th>
                                                                            <th></th>
                                                                        </tr>
                                                                    </tfoot>
                                                                </table>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                        <div class="x_panel">
                                                            <legend>SUMINISTRO NACIONAL</legend>
                                                            <div class="form-group">
                                                                <div class="col-md-7 col-sm-12 col-xs-12">
                                                                    <label>DESCRIPCIÓN</label>
                                                                    <!--<input type="text" class="form-control" name="sn_desc" id="sn_desc" style='text-transform:uppercase;'>-->
                                                                    <select class="form-control selectpicker" name="sn_desc" id="sn_desc"></select>
                                                                </div>
                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                    <label>PORCENTAJE (%)</label>
                                                                    <input type="text" id="sn_porc" name="sn_porc" class="form-control"/>
                                                                </div>
                                                                <div class="col-md-2 col-sm-12 col-xs-12">
                                                                    <label>&nbsp;</label>
                                                                    <BR>
                                                                    <button name="addsn" id="addsn" style="float: right;" type="button" class="btn btn-success" onclick="agregarSN();">Agregar</button>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <hr>
                                                            <h4>PARTE NACIONAL</h4>
                                                            <table id="tblaSN" class="table table-bordered">
                                                                <thead>
                                                                    <tr>
                                                                        <th>DESCRIPCIÓN</th>
                                                                        <th>PORCENTAJE (%)</th>
                                                                        <th>#</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody></tbody>
                                                                <tfoot>
                                                                    <tr>
                                                                        <th>TOTAL (%)</th>
                                                                        <th id="sumSN">100</th>
                                                                        <th></th>
                                                                    </tr>
                                                                </tfoot>
                                                            </table>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <div class="col-md-12 col-sm-12 col-xs-12 right">
                                                            <button class="btn btn-primary" type="button" id="btnCancelar" onclick="cancelarform();">Volver al listado</button>
                                                            <button class="btn btn-success" type="button" id="fin" disabled="disabled" onclick="volverallistado();">Finalizar Memo</button>
                                                            <!--<button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiartodo()">Limpiar</button>-->
                                                            <!--<button class="btn btn-success" type="submit" id="btnGuardar">Agregar</button>-->
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                        <!--<div id="step-7" role="tabpanel" class="tab-pane fade">
                                            <div class="x_panel">
                                                <div class="x_title">
                                                    <h3 style="text-align: center;">REVISIÓN</h3>
                                                </div>
                                                <div class="x_content" id="print">

                                                    <style>
                                                        .invoice-box {
                                                            max-width: 1050px;
                                                            margin: auto;
                                                            padding: 20px;
                                                            border: 2px solid #eee;
                                                            box-shadow: 0 0 10px rgba(0, 0, 0, .15);
                                                            font-size: 12px;
                                                            line-height: 12px;
                                                            font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
                                                            color: #555;
                                                        }

                                                        .invoice-box table {
                                                            width: 100%;
                                                            line-height: inherit;
                                                            text-align: left;
                                                            border: 1px solid #eee;
                                                        }

                                                        .invoice-box table td {
                                                            padding: 5px;
                                                            vertical-align: top;
                                                            border: 1px;
                                                        }

                                                        .invoice-box table td.logo {
                                                            font-size: 10px;
                                                            line-height: 4px;
                                                        }

                                                        .invoice-box table td.border {
                                                            border: 1px solid black;
                                                            text-align: center;
                                                            vertical-align: middle;
                                                            line-height: 24px;
                                                            font-size: 18px;
                                                        }

                                                        .invoice-box table tr td:nth-child(2) {
                                                            text-align: center;
                                                        }

                                                        .invoice-box table tr.top table td {
                                                            padding-bottom: 20px;
                                                        }

                                                        .invoice-box table tr.top table td.title {
                                                            font-size: 45px;
                                                            line-height: 45px;
                                                            color: #333;
                                                        }

                                                        .invoice-box table tr.information table td {
                                                            padding-bottom: 40px;
                                                            border: 1px solid black;

                                                        }

                                                        .invoice-box table tr.heading td {
                                                            background: #eee;
                                                            border-bottom: 1px solid #ddd;
                                                            font-weight: bold;
                                                        }

                                                        .invoice-box table tr.details td {
                                                            padding-bottom: 20px;
                                                        }

                                                        .invoice-box table tr.item td{
                                                            border-bottom: 1px solid #eee;
                                                            font-size: 13px;
                                                        }

                                                        .invoice-box table tr.item.last td {
                                                            border-bottom: none;
                                                        }

                                                        .invoice-box table tr.total td:nth-child(2) {
                                                            border-top: 2px solid #eee;
                                                            font-weight: bold;
                                                        }

                                                        @media only screen and (max-width: 600px) {
                                                            .invoice-box table tr.top table td {
                                                                width: 100%;
                                                                display: block;
                                                                text-align: center;
                                                            }

                                                            .invoice-box table tr.information table td {
                                                                width: 100%;
                                                                display: block;
                                                                text-align: center;
                                                            }
                                                        }

                                                        /** RTL **/
                                                        .rtl {
                                                            direction: rtl;
                                                            font-family: Tahoma, "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
                                                        }

                                                        .rtl table {
                                                            text-align: right;
                                                        }

                                                        .rtl table tr td:nth-child(2) {
                                                            text-align: left;
                                                        }
                                                    </style>                        


                                                    <div class="invoice-box">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr class="top">
                                                                <td colspan="3">
                                                                    <table>
                                                                        <tr>
                                                                            <td class="title">
                                                                                <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wgARCAA+ASsDAREAAhEBAxEB/8QAHAABAAIDAQEBAAAAAAAAAAAAAAMFBAYHAgEI/8QAGgEBAAMBAQEAAAAAAAAAAAAAAAECAwQGBf/aAAwDAQACEAMQAAAB7l43Kp+bUAADI6J6L7zYAAAAAAAAAAAAACt+fXHwAAAe7rf6lgAAAAAAAAAAAAANL8jlU/OqAABkbT0L3OwAAAAAAAAAAAAAGkeOyqPm1z+2c322wAExtgAAAAAAAAAAAAABW/Prj4JdUv0rAAfTPAAAAAAAAAAAAAANN8nlVfOjO7JzPY6gASm0H53pfDvTaL0wsNqO9N0W5pfO3pb4t5tnjUvtm+VLz79GtTQZjVF75XfTn1LQa59cy04/rnYzHQKX6kADRvG5U/zK2HdOd7fYACU24/MFL7LenR7U4Zlrtl66plrx3fC4pa5W6fplyLPTrGudtlpt8xpedvzdpN8p+rtcs7j66rt49e5eqq1x0qt7SY2oq712KluzIhynzSPsvWspJACUiJQREpDDCkMwnR5iYpj0mUiMKHyWWiaJ+THlMpHDHlAe0ZKZz//EACgQAAAGAAYCAgIDAAAAAAAAAAACAwQFBgEHEhMVFhBAFyYRNhQgJ//aAAgBAQABBQKUm1WLvs647OuOzrjs647OuOzrjs64b2JZZx7q2LPXqjhqjhqjhqjhqjhqjhqjgTFhq92Tg1Xzvq646uuOrrjq646uuOrrjq64b1tZFf3ZScXYu+0OgwsDh085lYcysOZWHMrDmVglLKnV91ZVoU+/HhJZkZTcajcajcajcajcajBRt+fdkoI7511ZUMa8o0d8KccKccKccKccKcJRB01BZZ6xKXD74K8jcjSdun5s1nx75hhWbS7k6REyd3mWH3zAGv0orQkz3tVN7IXeJauro9UcOsxZKLtDrMODbRNevMvMWqGm7pYGv3wRTe2YQsj8gRbCIXv01HHfyUDTWLy9SDOPsFliLG+n3iGYH9JWbXZPOzOxHzzh085hccwuOYXHMLjmFwlKrHVE1WW1pzM+GIIViqM6m2aVVtfrV8MQQr7Xr4iS1LFhiSifgiKhMqU8m4Zdm8onU38+dsvOVEmB7w2y9g2ks8w/1aGLV/4uihim2KNnI65fqeXf6bmsubrpMmITTbsvGdSjrNY20RfvmSBCubKL+UPIPuXBkiGx2ExgkQuO2UbZRtlG2UbZRoL42ibngiRE/Gynq4xmOMZjFBMyYMXA5dhMFSIQw2ibnGtMRxjMIt0m+Bi4HKQhUynSIp4OQqpVGaCw4xmCR7VM2wnuj//EADIRAAEDAwEFBAkFAAAAAAAAAAEAAhIDESEQIjFAQWETUbHwBBQjMDJCUHGRIFJygdH/2gAIAQMBAT8BLrKampqampqanx2FhYWFhYWFhY44tuoFQKgVAqBUCoFQ44usVMoOJP0TCwsfRC26ggy3uKlmhnXR1/lT7Na3vOlEXrdk/vQN9KY2qjXfKgZC6DS7AUvYT53AVVtnAM7kzaynEXZHcU+03NHI6M+Pa3ICSDpKi0udtIOlkKpdrZ9VU2WMI5k+H6S4hTKDiT7is63Zjzz0jmSqu9rDuGjGw9LZbnZC/PSjaVUt3Km6TQ4pzSRsI29V2f3BVsVm/wAf8Ttveqm+j55lVL9q+/fo2zdgKl8X9HwVLcfuUDCm9/RC9sosFRjh0RvUoUndT4aVHRpk81U3+ev55fn7cP04H//EADwRAAAEAgQKBggHAAAAAAAAAAABAgMEEQUSUtEQExYhMUFRYWKhFSMwcaLwICIyQFCBkbEUJDNCU8Hx/9oACAECAQE/AaRpVyEfxaUkf1HT79gud46ffsFzvHT79gud46ffsFzvGUD9gud4ygfsFzvHT79gud4h6ceW8lFQs5ltv9+eODrddVnvkJ0dweETo7g8InR3B4ROjuDwidHcHhE6O4PCJ0dweEJOArFVqT+Xv1IUM7GP41KiIZOP2yGTj9shk4/bIZOP2yGTj9shk4/bIZOP2yDFAPNOpcNZZjI/fqQph+EfxSCKUi23jKKKsp53iCpuIiYhDSklI++/4I65BpV1xpnvkMdR1pHINuwRrImzTPdL4JSFDORj+NSoiGTjv8hCDoNyGfS8ayzdg3NdY9mBMs9YIms17CB5g+cmycRrIGmrgcPM2pP7goqpyBqJGcxV/MG3qlMNKJSJr2hc0nLSJSbcUelIkZJIz14F+wVXTPkFKJJTMKTV0h9RJL1NwUmqcjDclKNGuQa9c3J6vRpGl4iEfxSCKWYZQxexPO8QVNRMREIaWRSPzt7BhMycPYeA1TSSQynqcZtM8ClGuEOeo/P2By1YHZ9SStP+hxNVRpIIWksywmt+KVWs3hjPDn33hHVlIgn9KI86iBSqlLAuausPWHvZ+Zfcg7pLuIVcY8hAOU8wS4bbqD3hEm3Xk+dWBtNdyR6A3/d0+7X9O/AbSFaSGKaskCbQWckifabvS39pv9H/xABBEAABAwICBgMNBwIHAAAAAAABAAIDBBEFEhMhMTIzkRBBoRQiIzRAUXGBk6KywdIGNUJSYWLRFSAkc3SChbHh/9oACAEBAAY/AjE1jHCw2rhR9q4UfauFH2rhR9q4UfauFH2rhR9qjjMcdnOA6/LvDaHP++11tpvdW2m91bab3VtpvdW2m91bab3VtpvdQymnzdVsvlxlbI1osBYrjRrjRrjRrjRrjRrjRrjRqN5lYQ1wPlxiYyMtsN4Lhxcj/KjicyMNceoFbrFusW6xbrFusTGlrLE28utM6EP/AH2ut+m5tQEb4C/qykXW9F2Lei7FvRdi3ouxb0XYhZ0V/V5cZWyNaLAWK4zOSjlMrSGnZZcRq4jVxGriNXEamOzjUb9EuF4RWQUzGQtf4ZrbcyF98Ybzj+lQvxDEKOegB8IIg255NUWE4JUwUxbBpZHTBtu1fe+Hc4/pVZXTOaa+lbKHHL+JouNSjq4cWoWxybBJowfhX3vh3OP6VUV4McWIQVPcxka24P62TXjF8Os4X16P6VJWTYnh80UIzujGTvh5ti+yTocsUWJHw8eW/mHzWJxTUndWFUzw1xib30Q86bXisbK1+5EziE+ay7nqKYUVG+mdLHA5vfW6nXTqqmxSijizluWUMafhX3xhvOP6ViArKylfiDh/hXtaMrfTqVRVzYhSaKBhkdlay9h/tUNbT4hS6GUXbnYwH4VUVeJyRTYjBE5xcwd6T+H5KKpZitAxkrcwa/Rg/CsMpsZraWppax5j8CG6j1bAqDCmub3HLSmRzcuvNc9fq/tMTAwtsNoW7FyP8qKJzY8rj1BbGclsZyWxnJbGclsZyTGkMsTbZ0VVLVmVsQpmvvEbH/pcau9o36VLBRume2R2c6Z11j9TWunZDBKIozEQPl+i41d7Rv0r7Y4H3xjZTulizbSMp/8AFH/UX4mKz8egy5PUuJjPuKpDmObC6uvFmFiW6taY4VFYyV7L3L2kA281kJMXpJ8Qwu48PSPtYfuC+xD6JuWkLjoxa1h3q+1TXNu0uaCCnYgyjGlOsMJuxp84C/44/NH+ruxEVec+LZctvWuJjPuLR4aZtHSgRkTjvv0WL/6WT4Vhn+X81FRx3L6ydkVghmmrc3X4Rv0qPFcOkqXy08zHO0rgQBf0LCMUqA/uc0APeC51ly3av2Q/lYZS4VE52mnbHN3Qz8JPVYqR/dBGXU2O53/yZdnrt6+jW1p9IXDbyVwxoPoW6OS3RyW6OS3RyW6OS3Ry6NJkbn2Zra+k5Ghtzc2G3oc7I3M4WJttC8Ug9mF4pB7MLRmNpj/KRq6C1wDmnaCmeDb3m7q3fQnOaxoc7aQNvRpMjc9rZra14rD7MLxSD2YREUTIwfyNsi1wuDtBQaxoa0bAEM7Q6xuLjZ0Fr2hzT1FDSQRvtqGZoNl4pB7MIObTRNcNhDAtLo26T89tfR//xAAoEAEAAgEDAwQCAwEBAAAAAAABABEhMUHxEFHRYXGR8ECBILHB4aH/2gAIAQEAAT8hAGUu95PecP5Th/KcP5Th/KcP5Th/KfX8ocktTLLXf84ztjz505GORjkY5GORjkY5GN12ZbXtX5zprkN4J9hn2GfYZ9hn2GfYZ9hinJoXmm/ziJJLS8nv0EVLpLc095wb5nBvmcG+Zwb5nBvmHkDVD39/zm9GZF/Z04QQdHxVTnY52OdjnY52GlmcU2v85uqoDtOSSwfpCzOEZwjOEZwjOEYhXDpT36VHfJFZnIznpCwKF5wYBDet5nyEYK6KWsVp3iY6cIJ+8AuIjHaGeZQjzWSGpdOEsdR+jwVm/tCkgG1szlz2WhrQL8Iww8jBbNQuTUxlvvL6u9+v/kwQaycO98Sy7mocJsNRZzn09N5T2XDE9FYzKkIgzbRM4Eb2wxZibVQtonY1XA9Sa04cM2lFf0hKwVpHuaJUbYtw6kUbSADMcW1O7b/ENca2XJ79Kg0lJUvT3nJPM5J5nJPM5J5nJPMrYGqXf36FX3DsANVdK1hs6LuqxQEvjjzUERtaA+elZwTrDaNdhuPwj3VGvfYyL0llVu2PGHQCvdM/dwywQgH7Je1zMvatlZKL09T0ey3lV/YimCaODYjeJdL39GYv0qLXDUitt6fT6LvWcD4zS9LUFZLo3T8Q3XnzIK+7VBgAXczdfNTZ6GFVwf4a3MMHeo276Ey4U79HJ/zGFAHrc7yyR21tsz2HBqZ9EJYHe4M4XAgBuD+Tve94Jxj6AELJarDtfXsN5i3d6KNbA++wvafY/wDJ9j/yHzbQr8JpDghQLEl78H/8JoFGwfc79F7OxbDtfaLlXXf/AIz7H/ky4EQL/ECAagWJMLRB0H6nYb3Nu501yjHY/qPuj6GOxc+x/wCRC2WOR+IuUpKKcffp/9oADAMBAAIAAwAAABCkkliySSSSSSSSSSSSSQ7bbYSSSSSSSSSSSSSSR2223ySSSSSSSSSSSSSRnySQSSSSSSSSSSSSSSQ6gACSSSSSSSSSSSSSSRhAAAQhIicNBfYZJXWSRCgACTj0aNFS6FOadIAGfrSSSSSeQFsQeNuTycT/xAAqEQEAAgAEBAUFAQEAAAAAAAABABEhMVGBEEFhoXGRscHwMEBQ0fEg4f/aAAgBAwEBPxB1UtpLaS2ktpLaS2ktpBKFffOqbZtm2bZtm2bZ01986snWnWnWnWnWnWnWghG/vnoOEYH8ItsZfTBvhX4R1dy+sRDf0BWGJey+xwtq2Nl+EpiZrsYez2qBbUd8SibNfy4d65NeUC8IuZosd6/faGJycZYpTT2Lir8vpKWmJjBopr180PDdlCpoM75fPmEVUxM9TE9Rqu8Qy1D39HhSzye+NPpf9t1RovkX7QgpyU8ogMuLsF12a2hGqhlKHAA74ds76Q8zCfAD/lyjhGB+ggPmPquABjNo8r/cYByC98vPHgTch7Efdmf19uUM8YtGsL29q3uVFzBdyXxWd/nmRvQ92NWaIxh/rq/K3lQDWHTFq8Ou98PDXpMz4Yp8BqwVuXu/5Ljz1j4xT5/w+s6AI+Q9XgBOTLT45ShQaDN05NwKsbyVdC0LlDKJR9Bxq+XHNt4W2PM4mBRArAgpiSitEcW3hnV8ot4vCi7go2QAymZXLhdZQwKMuCDnHGr5cP/EACoRAQABAwIEBgIDAQAAAAAAAAERACExQWEQkbHwUXGBodHxIMEwQFDh/9oACAECAQE/EFpwBluubJ+ckkkggmtSPsQjF10P7zLuexpm9b9W/Vv1b9W/Vv1b9T5NwiL50iLzOP7yAYgQzNiNCuzfiuzfiuzfiuzfiuzfiuzfiuzfiksDWNEfD+82XCXJXJ0HSvpqSLVhglh8V0/xIyLeltm9fYVbH0yn6Rf/ABEARAhHQivrGk9BzAPh/BGZsvg4Q2NLedTqcJ0+Y9KUF8KK3Qn3hzzvTQnUnnSwTQNnDMc+/wB0qvIxQgMkhzYPdoI/BjzhTlEc9oxClE+tjkL45zUESTiNfju1CeyQ7DAxyT/lDEmE9T9cMCzNyW88943EBzQPdpEGoHnejLasPVQX3mkmrlTyZE9L9D3KtWmR7dZ9jefwdqgVxW5slfRUG0rDAzh3fwApnqRwVDBPv9UcVmJ5GepHrwZM431TpDnWPb760zFqMNJZ81X1tUzcKcmkIYdPB6d6JRE28un/ADQBNTU9HxNh7d4tV/p0SvnF/O+PSKLUC0E8zTknPMzWGm7h4FSD1Z/X7q806VoIy6nOpJ2Dt5cHDSvnITy8aisLMgNzN5JwiMpiVISa7g19EfFJgh8irKGT8i0xrxwQY4QQ6HPFuy5rNIOalnxUWIOBaY1oAIOEsRSDZpVu1hnXgg5rL4uApii0xrw//8QAKBABAQACAQMDBAIDAQAAAAAAAREAITEQQVFh0fEgQHGBkaEwscHw/9oACAEBAAE/EB/zTs0eAfXp06dOnT8biMv3YApPLf3xMOELTNXlP8BhhhhhhizSvNH2N2yT74RMwNkOxnxHtz4j258R7c+I9ufEe3PiPbnxHtwroNWBQa9PvgWDUbo8E/roOQSA4JTqs7ePq379+/enmErBAz9vvj4MIeJq8p0eSFEYJn5P4+o88889mFANytSd798WmYkkTt07SAgkdE1fz9RRRRRSkdYsgGf10Lp5ZALX2hAhDJgBHXRkuhRpcM2nvGFmiwgAq7dRAuBYUcLMeIxgDYtAByOqh7Jbc2ecaSYrMEFq/Kt3Kkuu5A1BrVogASnB3lrBpWRBdTU8AjMWtUgGisIgjdFUtUI/MeQAsKrxoA8ElDLUASPuggKbaVLtZolZS4E1ATuOW6GNLEOCKq9EDtkVybLlQbQStNY4MA3VoGsGFMBJmdhJocaPfDlZr5kAXaGFbyR7sXqFtTtXCzCKmAloKCoi+MQu6nEgiIQBm2jqfQeezXKOwf66DFzdNwU6V+PH1SJEiRIPaTKIgZ+3RZbUMWos29ujE+pxlJJHEPFbt4jyyNzxLewBx6MUzHoFLwBeBDlrEleBk1yXY5752RLtG5HX31WCBuk30hoMMh3Q81iguwzVOcUiMiQ3NkRuae7gzYq0GjYIG74WvOaXSBWFDpEuu+NmCVVVX6T1oTTa2pxFDXJybyJkaO88j9YE4u/RWPG0WLgbVQErFCuExYSJu7wGCWmzxPVGNj/G3RueNwzRI0lCNoe954xiTkPhA4CLvp7Zl+PvLpNzQmuMKuobS1k77HLQyKQpHuYilMtP5c/8d/zHBxoZP3MN3Z9GfAvbPgXtnwL2z4F7ZXgdhAI/x0Ggllvmiz0vVtrs115gbdG3fSrCg/BJX0OuqBAVGQFIcAya/GAAAANAYjgRZIiI6R8YqV3ke0nZ2a1MewozT4gWPXoDSMFzd4LsurMehFUyvQgcEoImcKAuOJ8WSIiOkTthWLgz+AaDG2uSTXihpOyb6DR2Fn8K042Jq/hAMPQ6IB12I+4RKHKYa1h4E2bdXp//2Q==" style="width:200px; max-width:300px;">
                                                                            </td>              
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr class="heading" style="height: 30px;">
                                                                <td width="15%;" style=" vertical-align: middle;" id="venta_codigo1"></td>
                                                                <td width="70%;" style="text-align: center; vertical-align: middle;" id="proyecto_nombre1"></td>
                                                                <td width="15%;" style=" vertical-align: middle;" id="venta_fecha1"></td>
                                                            </tr>
                                                        </table>
                                                        <table>
                                                            <tr class="item">
                                                                <td width="30%" style="text-align: left;font-weight: bold;">Nuevas instalaciones</td>
                                                                <td id="venta_tipo1">x</td>
                                                                <td style="text-align: center; font-weight: bold;">Nombre proveedor</td>
                                                                <td style="text-align: right;"># de pedido</td>
                                                                <td style="text-align: center;" id="imp_codigo"></td>
                                                            </tr>
                                                            <tr class="item">
                                                                <td width="30%" style="text-align: left;font-weight: bold;">Modernizacion total</td>
                                                                <td id="venta_tipo2"></td>
                                                                <td style="text-align: center;" id="imp_proveedor"></td>
                                                                <td style="text-align: right;"># nef</td>
                                                                <td style="text-align: center;" id="imp_nef"></td>
                                                            </tr>
                                                            <tr class="item">
                                                                <td width="30%" style="text-align: left;font-weight: bold;">Modernizacion Parcial</td>
                                                                <td id="venta_tipo3">x</td>
                                                                <td style="text-align: center;"></td>
                                                                <td style="text-align: right;"># CC</td>
                                                                <td style="text-align: center;" id="venta_cc1">18075K1</td>
                                                            </tr>
                                                            <tr class="item">
                                                                <td width="30%" style="text-align: left;font-weight: bold;">Equipo en cartera</td>
                                                                <td id="venta_tipo4">x</td>
                                                                <td style="text-align: center;"> </td>
                                                                <td style="text-align: right;"># KEN</td>
                                                                <td style="text-align: center;" id="venta_ken1">pendiente</td>
                                                            </tr>
                                                        </table>
                                                        <table>
                                                            <tr class="heading">
                                                                <td width="30%" style="text-align: left;font-weight: bold;">Nombre del proyecto</td>
                                                                <td colspan="4" style="text-align: left;font-weight: bold;" id="proyecto_nombre2" >Nombre del proyecto</td>
                                                            </tr>
                                                            <tr class="item">
                                                                <td width="30%" style="text-align: left;">Dirección Obra</td>
                                                                <td colspan="4" style="text-align: left;" id="proyecto_direccion1">Matucana 1001, comuna de Santiago</td>
                                                            </tr>
                                                            <tr class="item">
                                                                <td width="30%" style="text-align: left;">Contacto ventas</td>
                                                                <td style="text-align: left;" id="contacto_ventas">PRUEBA</td>
                                                                <td style="text-align: left;" id="contacto_ventas_email">Email:</td>
                                                                <td style="text-align: left;" id="contacto_ventas_fono">Fono:</td>
                                                            </tr>
                                                            <tr class="item">
                                                                <td width="30%" style="text-align: left;">Cotacto Obra</td>
                                                                <td style="text-align: left;" id="contacto_obra">PRUEBA</td>
                                                                <td style="text-align: left;" id="contacto_obra_email">Email:</td>
                                                                <td style="text-align: left;" id="contacto_obra_fono">Fono:</td>
                                                            </tr>
                                                        </table>
                                                        <table>
                                                            <tr class="heading">
                                                                <td width="30%" style="text-align: left;font-weight: bold;">Razon social Cliente</td>
                                                                <td colspan="4" style="text-align: left;" id="mandante_razon"></td>
                                                            </tr>
                                                            <tr class="item">
                                                                <td width="30%" style="text-align: left;">Dirección cliente</td>
                                                                <td colspan="4" style="text-align: left;" id="mandante_direccion"></td>
                                                            </tr>
                                                            <tr class="item">
                                                                <td width="30%" style="text-align: left;">Rut Cliente</td>
                                                                <td colspan="4" style="text-align: left;" id="mandante_rut"></td>
                                                            </tr>
                                                            <tr class="item">
                                                                <td width="30%" style="text-align: left;">Representate legal</td>
                                                                <td style="text-align: left;" id="mandante_representante"></td>
                                                                <td>&nbsp;</td>
                                                                <td style="text-align: left;" id="mandante_respresentante_rut"></td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </table>

                                                        <table id="rev_info">
                                                            <tr class="heading">
                                                                <td colspan="3" style="text-align: left;font-weight: bold;">Informacion Contractual de Instalacion</td>
                                                                <td style="text-align: center;font-weight: bold;">Fechas</td>
                                                            </tr>
                                                        </table>

                                                        <table>
                                                            <tr class="heading">
                                                                <td colspan="3" style="text-align: left;font-weight: bold;">Forma de pago Contractual</td>
                                                            </tr>

                                                            <tr class="item"> 
                                                                <td colspan=3" id="PI"></td>
                                                            </tr>
                                                            <tr class="item">
                                                                <td colspan=3" id="PN"></td>
                                                            </tr>
                                                        </table>

                                                        <table>
                                                            <tr class="heading"><td colspan="7">&nbsp;</td></tr>
                                                            <tr class="item">
                                                                <td width="10%" style="text-align: left;font-weight: bold;">Contrato</td>
                                                                <td width="10%" style="text-align: left;" id="contrato"></td>
                                                                <td width="15%" style="text-align: left;font-weight: bold;" id="descgarantia1">Garantia Anticipo</td>
                                                                <td width="15%" style="text-align: left;" id="garantia1"></td>
                                                                <td width="20%" style="text-align: left;font-weight: bold;" id="descgarantia2">Garantía fiel cumplimiento</td>
                                                                <td width="15%" style="text-align: left;" id="garantia2"></td>
                                                            </tr>
                                                            <tr class="item">
                                                                <td width="10%" style="text-align: left;font-weight: bold;">Retenciones</td>
                                                                <td width="10%" style="text-align: left;" id="retenciones"></td>
                                                                <td width="15%" style="text-align: left;font-weight: bold;">Tipo</td>
                                                                <td width="15%" style="text-align: left;" id="tipo_garantia1"></td>
                                                                <td width="20%" style="text-align: left;font-weight: bold;">Tipo</td>
                                                                <td width="15%" style="text-align: left;" id="tipo_garantia2"></td>
                                                            </tr>
                                                            <tr class="item">
                                                                <td width="10%" style="text-align: left;font-weight: bold;">Multas</td>
                                                                <td width="10%" style="text-align: left;" id="multas"></td>
                                                                <td width="15%" style="text-align: left;font-weight: bold;">validez</td>
                                                                <td width="15%" style="text-align: left;" id="validez_garantia1"></td>
                                                                <td width="20%" style="text-align: left;font-weight: bold;">validez</td>
                                                                <td width="15%" style="text-align: left;" id="validez_garantia2"></td>
                                                            </tr>
                                                            <tr class="item">
                                                                <td width="10%" style="text-align: left;font-weight: bold;">O. Compra</td>
                                                                <td width="10%" style="text-align: left;" id="ordencompra"></td>
                                                                <td width="15%" style="text-align: left;font-weight: bold;">Garantias equipo > 12 mes</td>
                                                                <td width="15%" style="text-align: left;" id="garantias_equipo"></td>
                                                                <td width="20%" style="text-align: left;font-weight: bold;">mantencion sin costo post entrega</td>
                                                                <td width="15%" style="text-align: left;" id="mantencion_equipo"></td>
                                                            </tr>
                                                        </table>

                                                        <table style="text-align: left;">
                                                            <tr class="heading" id="ide" >
                                                                <td style="text-align: left;">IDE</td>
                                                            </tr>
                                                            <tr class="item" id="ken">
                                                                <td style="text-align: left;"># KEN</td>
                                                            </tr>
                                                            <!--<tr class="item" id="cuadro">
                                                                <td width="20%" style="text-align: left;">CUADRO</td>
                                                            </tr>
                                                            <tr class="item" id="unidades">
                                                                <td style="text-align: left;">Unidades</td>
                                                            </tr>
                                                            <tr class="item" id="kg">
                                                                <td style="text-align: left;">Carga (KG)</td>
                                                            </tr>
                                                            <tr class="item" id="vel">
                                                                <td style="text-align: left;">Velocidad (m/s)</td>
                                                            </tr>
                                                            <tr class="item" id="paradas">
                                                                <td style="text-align: left;">Paradas/Accesos</td>
                                                            </tr>
                                                        </table>

                                                        

                                                        <table>   
                                                            <tr class="top">
                                                                <td colspan="3">
                                                                    <table>
                                                                        <tr>
                                                                            <td class="logofooter" align="right">
                                                                                <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wgARCAAoAH8DAREAAhEBAxEB/8QAHAAAAgIDAQEAAAAAAAAAAAAAAAUEBwIDBgEI/8QAGgEAAgMBAQAAAAAAAAAAAAAAAAMCBAYFAf/aAAwDAQACEAMQAAABtDTZ/iuvzWKHLnpYocAuenrOZfuLK6MAAAAAACm9XnKy0PEbVnxWQYIdHZCG1XRULlz5PSc7foxGezEz99Vj4/rubbaVrNd93j173eRLVOMyDqpZ3rnHZD09uHK6OC2EVid0WYSVol6+pv3wZ8473G1TpeC9p2kVyq9p2gEVyrY/B7P0hgdmltV5ipa/ZaGJy8m5qulqaAAAAAAAAAAAAAAAB//EACUQAAICAgEDAwUAAAAAAAAAAAQFAwYBAgATFzUHFRYiMDQ2QP/aAAgBAQABBQK2WwtCx7jsuH35gLP3HZcZX5gGxW35gYx7jsuVi4mOmv2LGi+QWr470UEdf3clsaxHosMrE7TZUi6ESyuQFp6kOKNaHsmIl2Sth9tHE5BMDuaPT3mbeUBvP01c8hS7hJcAd0ZHjWpAGaISLtuFV0GjvChcW8CcDU5lELX1v13omfAo8LKPaHZ4NHjLaDqYeB5xE/Gl3gn0Ji56j+c45/L4984i85ygfsJYcZ0cleGzrIq0kyQminh3RaSz4Rx68FG1Eg/h/8QAOBEAAQIDBAUJBgcAAAAAAAAAAQIDAAQREiExUQUTQWFxEBQiNIGhwdHwFTKCkbHhIzBAQlKy8f/aAAgBAwEBPwGfn3ZV0IQBhHtmYyHf5w7pZ9CqADAd4rnHtmYyHf5w9pZ9t1SABcfW2GdLPuOpQQLz62x7ZmMh3+cSOkXZl7VrA/JnJXnc4EVp0fGOZ2ZfnDhpkKYwJQzK7RNEgJv+EQ9JANa9hdpMOSK39a8jYo3QxK2QzMVxULu37QzJocY17i7I4VjR6G0TYDarQplSJo2Wqk0vH1EKcKFfgmqdmJvsr7TgLr918NvuOKSon+XA4YUUd/yMGYWlZCb7/l0gL+ypGGG2BNrKjeAMzgBVYrjTYMseAjXqTYFcaZ1NcacNt1RiaCJZanWUuKxIryLcQ3PVWadDxh51uflgpSqLT3w262tCpV02ahN/wjyglmRl1oSu0peUCZEu08UnpWzd2iHJpqYSzYuNsXRo55KJazbANdsM36QtWgqo2QpQQCo7IQu0L8fXobo5yitPA7vOC8gf4Y5010b/AHsLjthMy2sFQwuz2whYcFpPJpnrA4eJ5Jn3xwT/AFHJNdYc4n6xK9Yb4j68mies9kONpdTZWLo5sj9nRG671XwFL41AraqfVPIb98OMJcQUYQZRKlBZJqKZbDUbM8oTLhIs1O7dTL71rgaiG2w2myP0X//EADkRAAECAwUCCgkFAQAAAAAAAAECAwAEEQUSITFBUWEQFCI0coGhsdHwExUyUnGCkcHhMDNAQvGy/9oACAECAQE/AbMsxmdZLjhOdMOrdHqCW95XZ4QxYku4mpUcyNNCRsj1BLe8rs8Il7El3WUOFRxAOnhExYku0ytwKOAJ08I9QS3vK7PCLRspmUY9Kgmvnd+jITnEZEuXa8unZHH781xVpNaZmuUKnkyjd0C8oqVQfMYYtFRe4tMouK74atFuWDLDmRSnHz3xMzl9T8rd9lBNer8xMT7jUwJZtu8aVzpFprdckSXUXTXbWJNN56gFcFb/AOpphAbCxR8UVrQAYXkU3A4nZpXCFSrbbbgAxw6sFZ1SPJTBlEKxoRsyx5JOHXQa50wMcUQEgEEnYKVJog7DtO3L4mHpVurigMr2OgpkKb9Mdd0TKEtPLbRkDTgbaW7ZtG015enRiXZcs2bKUoq2vZp5/O6HWXW1pnWU3rpUCPmV4wA/aM2hxbZQlGOMGUM09LhSTduDHqMNSb8qt/0mIuHHz3RasutybCvRqUmmn+GJjCy7lwpodc9uwQ2guLCBrC2FBVE45duUCTdV7NNNRr1xxZdAdu8duOEcTe5Qp7OeI8YVJOIArnjqMKUzNaDOFoU2q6rgsDmyul9hwSn7Z6Sv+jwSXNmuiO6J3mzvRPdwW3zQ/EQ06plV5GcCedwK+URt+o+n3hMyUilB/mR6q/DaDCJpSFX6V/Ap53wmdUhJQlIoa7dRQ6wZxRzSNa760rX6aUppDjhdVeP8L//EADsQAAEDAgMDCAgDCQAAAAAAAAECAxEEEgATIQUxQRAjMkJRYXGRFCIzdIGSstIVMMFAYnOCobGz0fD/2gAIAQEABj8CbYYbZWhTQXzgM7z392PYUvyq+7CUJZpiC02vVKusgKPHvx7Cl+VX3YqmEM0xQ06pAlKp0PjilYWzTBDrqUGEqnU+OPYUvyq+7Apn22EoKSZbSZ/v+Sinzsi2jzLrbuuf94/E6p5VPeYZZy5LnZx0wpxTqaWkZpmC4+vhzKcHaOzqwV1Kkwv1bVIxteup1XrarHU5EakTOnnuxsXamdOdWIbyrd3rHj/Lhe0amv8ARGkrsPMlf69+G00lX6a3kqJXllEHsg4kuFlGa0FLC7ITmJnXhpgmidL9Mo82464txN+W4TrMkaJ7ePHFEsupCFXIiLUuG9vo2rIO/t4KwkFxt5CQkuGwgsi9I9Yz2FRnTozuwohbVOibS68lRSkXvASJEdBPn4YoWytClKQzzSkkuOhQErCp4a8Or34pnnrcxxAWbBA15L33m2EHZ8XOKtHtMNuO1SKfaNGDKHVAZvbHjH/b8VOxqx4UiX2GHEPndOUjf8o/risYZrWq+rrRZzRlKRHj3nG3FNvNJqxtFaktKIlQuTOnnjYxpyltz8RbUtjrJMmT5nf34W36bS01RnEgVCuGnCRjO9KYq1OsXFVN0RpEbz2YcdUCQgTCd57hi9zm1ALvTvi0wrCszNTCljRlaujvOg3ajXCki9Vsza0s7iBppr8MUxvWE1EZSlNLAVO7WMORmZYShSTlLuXdduTEno8P0wHGzKT8ORj3cfUrkb93Y/xJ5Noe8OfUcbP94b+ociP4asBt4XtXBRQdyvHC0sldIhcyliEjUQeHHTyweddRJM2nqmLk/GPHsIwW8xxsG7oR1lBRG7ujww285UPuOJtkm0XWquEwMGH37haG1SJbCZiNP3iNZwGkSQJMq3kkyT5/sX//xAAnEAEBAAICAgEDAwUAAAAAAAABEQAhMUFRYRBxgZEwQNGxweHw8f/aAAgBAQABPyF60FfSfQ1p8bHVA96/Rmr42O8B7goXz1jvAeYCM89/GyW1uNH1f6PVe9lCkp/xn3zGQbWIdvHBd0weoQU6IavvevxUANC+AbPvvRpHjeWxajVUV2+j+M+h3zru239neaeP3oR1X4ZV6f4jQRvvGF4XJA1oindziShnJNB7wi66av6WPYFRg+3GnMZBG1hNtqTonIrhP6QM9EyYXW43oDBF+lB4pu3KpGJE69jFgK8Wfx8AogRo9K96cks6RsK47ik4dSJiYE1AF4ln4LpRmXmsy1Zs0hs7U1BzRnM+v5USKe/Gb0XSCEl2NPL2phUWEU022vPeIKkuWj6ohee8eI2a+g7XgM5e82dMs2DxoojMVBpnQMP7aG+cFNzvFUJHqu61/SoOhVqioFig9b4TIJ5MQUGqSqEl8s2SdyKEYiOxERHYn6M1fYsf6nxmquDDPYDssZ6OqZ0SAex5wgudlI2vRrUZfCNCqmjehmhot6YAoToSUjc8vMYqMOqyEOHkHLYQdTJ92kcw7uJowjVQh8qX7/sv/9oADAMBAAIAAwAAABDqKReSSSSarQuxL/TR0/wbR5xZZLQcbSySSSSSSST/xAAmEQEBAQACAgECBgMAAAAAAAABESEAMUFRYXGhEIGRsfDxIDBA/9oACAEDAQE/EHTBDo+08J65/R8CW2vT4F+pzn9HwkvoFHwpxJfAYPlDj+j4DgCLg3Pqv+nxTXZfI9nvnZds0Po9kHfHRS0r0NsvH6F+dw/IVsE7kT8v3wyMTeMtqHGoNo+Xep+dzndubHWvN/Z544hGdvT0/PrmjMbWvUf3+eHgGrZBJ0SZa3g2zdlNCMReliL03BQBA5oDKAp5VYMNRZgCuNIyrhdAJRA8Q4DGkBljUradKAdaUZRURKnSooCCDyMsWCGlmr19fwg+MVQ+/hgTuKHTZ8sydOSI8lFOPCmLc8PV0ozgksnkBJscxY+VMg8FSBBJU0zuJevn1xOmi4o1rPIrb5u7Tj4V3bxnij9+TvlV6mSdvgvfnnQSFfOGuc65AonyX6MQqQqGFnJpXuERYJYFhBenG6cqSslidlJBuazo1mXNqiBYKoFk3xfG9bxwVAqFNIISozEo76eBUzfCdMcYiJEfw+x/4Cj+S9ufyXp+HT9XEliin0RPuFPJjRTiWJoMkLEfFwxidkCq0ifhDIEMpbKQJg5QKH1OpJokmScYW7GKQmCB8C5VhF4lhoz0IDrzoNMOEkXtV7VVV6NVYAHQBD/i/8QAJhEBAQEAAwABAwMFAQAAAAAAAREhADFBURBhgaHw8SAwQHGRsf/aAAgBAgEBPxAxJFoBAXq+efynBPdMT0E/4N+/P5ThTFYjFQc4pikVigu8fynBevQak3/Q/s+6qizstsfjnRUrkI7JGph32xkUANjTt93ufbGv5QnSlNp+X/motKJEh4tvBSRPDPWekrzpt2DvGSZ+XnKk/wAPy+SefPMx9yMfNPyT7cbQqAhSJoI7IR3mLOYTsKEEYrU0vVCoruo8SwUMQtB6p1Ib2Q4pDwKVX4DjPBUSg4GiFoYKJohQCyQdNY7gFBgBob0VaRcZcDuWTOt7+iZj3ArPWcXc0iFdsveFjezDR4taesR2A31e5jEHn2VRitpKFqFOgHahxwxkgYNDeqMY/aiPBQsM3EgBfECfAMyPGZAmXvfYfpysimHdXXwVTrziKgoN6L6vgevhzpUcOFm+sfnUES8k6UWn7ho1jnedcHZDEo9i9hoMoXzeJsi2gdAisKQupY52Jw4ikJh2OwWIojDtOKBifnsoiYiaJiaZ9P1T+gD/ALT8OftPy+uKTkIPpcU+8pfLSMSwJ1G1x+QtWXxDSBSG67HtPpqERqOxvC8ETu9oGiN20RAJOAf9DU3irZ61NDFFMwNCMejWaEwWg4/mOEOgAAPsABa5qv8Ahf/EACIQAQEAAgIBBQADAAAAAAAAAAERACExQVEQMGFxkUDR8P/aAAgBAQABPxA58zUxBPp0Wrv0HPWvERIQ6GO4FV24OWNs4mSCMCwC9GDG2cTJLCFKJen0HLJqwNEKSb8ez/UF3Hd3t+m9FHyUDF+pWVx9Buj1e6FQWh0AatRiDYPWGitYhpQIUg3qHrNHXhBsYxWL8v8AMXP+ifnrdAi6FddFsghp5zmMMvaNisHQ49OKvjZl5UAUCC7xTzaxNCVrgGAZ9CKnzm1ookdETlgYhqObE9ANQmEnFmlAjHSW3VmHkdf5OAHJX7BB9LmC5Y9AldosqGCfAoi0Kgwk5g+MV4Le5dhABpWMBU5ep+NAqg6Nkt6cgsE92xiRBtQ0sZJRAmFYKAhGhSzdrhBnGUi2iCfejcqqyIAQFO5MQHHPuElwJX6QmP8At9DCnYU7Ch3lCBuzWoCwFUIbQW9V3O7Br+CpGGFbJ0UWMKTgoiuH2nNmGHsQCwBQk1akKqYM9wT0JjtpQqQHgYEICCJ7IaDWrf7Xhg/SooNcMTxqk1IsYYahmoDhhElGBnem+YW0XcOgOA2MRqtJSoTEEq5KKFUBJ0iKwCCTvG7W6VOoeUkdK+fURnQ5XgFUAh/C/9k=" style="width:200px; max-width:150px;"><br>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="button" class="btn btn-info" onclick="printDiv('print')" value="imprimir" />
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bs-example-modal-sm" id="agregacc" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Agregar Centro de costo</h4>
                    </div>
                    <form id="formularioCC" name="formularioCC" class="form form-horizontal">
                        <div class="modal-body">

                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                    <label class="control-label">Codigo</label>
                                    <input type="hidden" id="idcentrocosto" name="idcentrocosto" class="form-control">
                                    <input type="text" class="form-control col-md-4" name="codigo" id="codigo">
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                    <label class="control-label">Codigo externo</label>
                                    <input type="text" class="form-control col-md-4" name="codexterno" id="codexterno">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                                    <label class="control-label">Nombre</label>
                                    <input type="text" class="form-control" name="nombre" id="nombre" style='text-transform:uppercase;'>
                                </div>
                            </div>	
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label class="control-label">Area</label>
                                    <select class="form-control" data-live-search="true" id="tcentrocosto" name="tcentrocosto" required="required"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                                    <label class="control-label">Vigencia</label>
                                    <div class="">
                                        <label>
                                            <input name="condicion" id="condicion" type="checkbox" class="icheckbox_flat-green" value="1"/>
                                        </label>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary" id="agregacentrocosto">Agregar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade bs-example-modal-sm" id="agregaimp" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Agregar importación</h4>
                    </div>
                    <form id="formularioimp" name="formularioimp" class="form form-horizontal">
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                    <label class="control-label"># PEDIDO</label>
                                    <input type="hidden" id="idimportacion" name="idimportacion" class="form-control" required>
                                    <input type="text" class="form-control col-md-4" name="codigo" id="codigo">
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                    <label class="control-label"># NEF</label>
                                    <input type="text" class="form-control col-md-4" name="nef" id="nef" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                                    <label class="control-label">NOMBRE PROVEEDOR</label>
                                    <input type="text" class="form-control" name="proveedor" id="proveedor" style='text-transform:uppercase;' required>
                                </div>
                            </div>	
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary" name="agregaImportacion" id="agregaImportacion">Agregar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalForm" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">AGREGAR CLIENTE</h4>
                    </div>

                    <form role="form" id="formcliente" name="formcliente">
                        <!-- Modal Body -->
                        <div class="modal-body">
                            <p class="statusMsg"></p>
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                <label>RUT <span class="required">*</span></label>
                                <input type="text" class="form-control" name="rut" id="rut" required="Campo requerido" data-inputmask="'mask' : '[*9.][999.999]-[*]'">
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                <label>Razon Social <span class="required">*</span></label>
                                <input type="text" class="form-control" name="razon" id="razon" required="Campo requerido" style='text-transform:uppercase;' readonly="">
                            </div>

                            <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                <label>Avenida o Calle <span class="required">*</span></label>
                                <input type="text" class="form-control" name="callecli" id="callecli" required="Campo requerido" style='text-transform:uppercase;'>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                <label>Numero <span class="required">*</span></label>
                                <input type="text" class="form-control" name="numerocli" id="numerocli" required="Campo requerido">
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                <label>Oficina</label>
                                <input type="text" class="form-control" name="oficinacli" id="oficinacli">
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                <label>Region <span class="required">*</span></label>
                                <select class="form-control selectpicker" data-live-search="true" id="idregionescli" name="idregionescli" required="Campo requerido">
                                </select>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                <label>Comuna <span class="required">*</span></label>
                                <select class="form-control selectpicker" data-live-search="true" id="idcomunascli" name="idcomunascli" required="Campo requerido">
                                </select>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                <label>N° Representantes<span class="required">*</span></label>
                                <select class="form-control selectpicker" data-live-search="true" id="nrep" name="nrep" required="Campo requerido">
                                    <option value="" selected disabled>N° de representantes</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                </select>
                            </div>
                            <div id="replegal" name="replegal"></div>
                        </div>
                        <div class="clearfix"></div>

                        <!-- Modal Footer -->
                        <div class="modal-footer">
                            <button type="reset" onclick="limpiarcliente()" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary submitBtn" id="btnGuardarCli">Agregar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <?php
    } else {
        require 'nopermiso.php';
    }
    require 'footer.php';
    ?>
    <script src="../public/build/js/libs/png_support/png.js"></script>
    <script src="../public/build/js/libs/png_support/zlib.js"></script>
    <script src="../public/build/js/jspdf.debug.js"></script>
    <script src="../public/build/js/jspdf.plugin.autotable.js"></script>
    <script src="../public/build/js/jsPDFcenter.js"></script>
    <script type="text/javascript" src="scripts/preventa.js"></script>
    <?php
}
ob_end_flush();

