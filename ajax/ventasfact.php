<?php

session_start();

require_once "../modelos/FacturaVenta.php";



$venta = new FacturaVenta();

switch ($_GET["op"]) {

    case 'LVF':
        $rspta = $venta->LVF();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => '<button class="btn btn-info btn-xs" onclick="pdf(' . $reg->idventa . ')" data-tooltip="tooltip" title="Memo de Venta (PDF)"><i class="fa fa-file-pdf-o"></i></button><button class="btn btn-success btn-xs" onclick="mostrar(' . $reg->idventa . ')" data-tooltip="tooltip" title="Mostrar Venta"><i class="fa fa-newspaper-o"></i></button><button class="btn btn-success btn-xs" onclick="SolFacturaracion(' . $reg->idventa . ')" data-tooltip="tooltip" title="Solicitar Facturacion"><i class="fa fa-share"></i></button><button class="btn btn-success btn-xs" type="button" data-toggle="modal" data-target="#modalFormFactura" data-idventa="' . $reg->idventa . '" data-tooltip="tooltip" title="Agregar Factura"><i class="fa fa-plus-circle"></i></button>',
                "1" => $reg->proyecto,
                "2" => $reg->codigo,
                "3" => $reg->mandante,
                "4" => $reg->estado,
                "5" => $reg->fecha
            );
        }

        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );
        echo json_encode($results);
        break;

    case 'LFact':
        $idventa = isset($_POST["idventa"]) ? limpiarCadena($_POST["idventa"]) : "";
        $rspta = $venta->LFact($idventa);
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => '<button class="btn btn-info btn-xs" onclick="cancelarfact(' . $reg->idfactura_venta . ')" data-tooltip="tooltip" title="Cancelar Factura"><i class="fa fa-close"></i></button>',
                "1" => $reg->numero,
                "2" => $reg->fecha,
                "3" => $reg->descripcion,
                "4" => $reg->simbolo,
                "5" => $reg->monto
            );
        }

        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );
        echo json_encode($results);
        break;

    case 'MV':
        $idventa = isset($_POST["idventa"]) ? limpiarCadena($_POST["idventa"]) : "";
        $rspta = $venta->MV($idventa);
        echo json_encode($rspta);
        break;


    case 'MVSol':
        $idventa = isset($_POST["idventa"]) ? limpiarCadena($_POST["idventa"]) : "";
        $rspta = $venta->MVSol($idventa);
        echo json_encode($rspta);
        break;

    case 'SolFact':
        $idventa = isset($_POST["idventa"]) ? limpiarCadena($_POST["idventa"]) : "";
        $rspta = $venta->SolFact($idventa);
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => $reg->porcentaje,
                "1" => $reg->idtformapago,
                "2" => $reg->monto,
            );
        }

        $results = array(
            "cantidad" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;

    case 'Factu':
        $idventa = isset($_POST["idventa"]) ? limpiarCadena($_POST["idventa"]) : "";
        $rspta = $venta->Factu($idventa);
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => $reg->monto,
                "1" => $reg->idtformapago,
            );
        }

        $results = array(
            "cantidad" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;

    case 'DFact':
        $idfactura = isset($_POST["idfactura"]) ? limpiarCadena($_POST["idfactura"]) : "";
        $rspta = $venta->DFact($idfactura);
        echo $rspta ? "FACTURA CANCELADA" : "FACTURA NO PUDE SER CANCELADA";
        break;

    case 'pdf':
        $idservicio = isset($_POST["idservicio"]) ? limpiarCadena($_POST["idservicio"]) : "";
        $rspta = $servicio->pdf($idservicio);
        echo json_encode($rspta);
        break;

    case 'SelMon':
        $idtformapago = $_GET["idtformapago"];
        $rspta = $venta->SelecMon($idtformapago);
        echo '<option value="" selected disabled>SELECCIONE MONEDA</option>';
        while ($reg = $rspta->fetch_object()) {
            echo '<option value=' . $reg->idmoneda . '>' . $reg->nombre . ' - ' . $reg->simbolo . '</option>';
        }
        break;

    case 'SelTForma':
        $rspta = $venta->SelecTF();
        echo '<option value="" selected disabled>SELECCIONE SUMINISTRO</option>';
        while ($reg = $rspta->fetch_object()) {
            echo '<option value=' . $reg->idtformapago . '>' . $reg->descripcion . '</option>';
        }
        break;

    case 'GuarFact':
        $mtsuministro = isset($_POST["mtsuministro"]) ? limpiarCadena($_POST["mtsuministro"]) : "";
        $midventa = isset($_POST["midventa"]) ? limpiarCadena($_POST["midventa"]) : "";
        $mnfactura = isset($_POST["mnfactura"]) ? limpiarCadena($_POST["mnfactura"]) : "";
        $mfecha = isset($_POST["mfecha"]) ? limpiarCadena($_POST["mfecha"]) : "";
        $mmonto = isset($_POST["mmonto"]) ? limpiarCadena($_POST["mmonto"]) : "";
        $mtmoneda = isset($_POST["mtmoneda"]) ? limpiarCadena($_POST["mtmoneda"]) : "";

        $rspta = $venta->GFact($midventa, $mtsuministro, $mnfactura, $mmonto, $mtmoneda, $mfecha);
        echo $rspta ? $midventa : 'NO';
        break;

    case 'GuarSolFact':
        $contsi = 0;
        $contsn = 0;
        if (!empty($_POST["arraysi"])) {
            if (strpos($_POST["arraysi"], ',') !== false) {
                $si = explode(',', $_POST["arraysi"]);
                $numsi = count($si);
                for ($o = 0; $o < (int) $numsi; $o++) {
                    $rsp = $venta->GSolFact($si[$o]);
                    if ($rsp)
                        $contsi++;
                }
            }else {

                $numsi = 1;
                $rsp = $venta->GSolFact($_POST["arraysi"]);
                if ($rsp) {
                    $contsi++;
                }
            }
        } else {
            $numsi = 0;
        }


        if (!empty($_POST["arraysn"])) {
            if (strpos($_POST["arraysn"], ',') !== false) {
                $sn = explode(",", $_POST["arraysn"]);
                $numsn = count($sn);
                for ($o = 0; $o < (int) $numsn; $o++) {
                    $rsp = $venta->GSolFact($sn[$o]);
                    if ($rsp)
                        $contsn++;
                }
            }else {
                $numsn = 1;
                $rsp = $venta->GSolFact($_POST["arraysn"]);
                if ($rsp) {
                    $contsn++;
                }
            }
        } else {
            $numsn = 0;
        }

        if ($contsi == $numsi && $contsn == $numsn) {
            echo 'SI';
        } else {
            echo 'NO';
        }

        break;

        case 'LTablaFP':
        $idventa = isset($_POST["idventa"]) ? limpiarCadena($_POST["idventa"]) : "";
        $rspta = $venta->FormPaSI($idventa);
        $fsi = Array();
        while ($reg = $rspta->fetch_object()) {
        $fsi[] = array(
        "0" => $reg->descripcion,
        "1" => $reg->porcentaje
        );
    }
    $rspta = $venta->FormPaSN($idventa);
    $fsn = Array();
    while ($reg = $rspta->fetch_object()) {
        $fsn[] = array(
            "0" => $reg->descripcion,
            "1" => $reg->porcentaje
        );
    }

    $rspta = $venta->LisFormPaSI($idventa);
    $lsi = Array();
    while ($reg = $rspta->fetch_object()) {
        $lsi[] = array(
            "0" => $reg->idformapago,
            "1" => $reg->codigo,
            "2" => $reg->montosi,
            "3" => $reg->porcentaje,
            "4" => $reg->facturado
        );
    }
    $rspta = $venta->LisFormPaSN($idventa);
    $lsn = Array();
    while ($reg = $rspta->fetch_object()) {
        $lsn[] = array(
            "0" => $reg->idformapago,
            "1" => $reg->codigo,
            "2" => $reg->montosn,
            "3" => $reg->porcentaje,
            "4" => $reg->facturado
        );
    }

    $nascen = $venta->Nascen($idventa);

    $results = array(
        "FormPSi" => $fsi,
        "FormPSn" => $fsn,
        "ListaPSi" => $lsi,
        "ListaPSn" => $lsn,
        "nascen" => $nascen
    );

    echo json_encode($results);
    break;


    case 'PDF':
        $idventa = isset($_POST["idventa"]) ? limpiarCadena($_POST["idventa"]) : "";


        //DATOS DEL MEMO
        $datos = $venta->pdfdatos($idventa);

        //NUMERO DE ETAPAS
        $ninfo = $venta->ninfo($idventa);


        //INFORMACIO CONTRACTUAL
        $rspinfo = $venta->pdfinfo($idventa);
        $info = Array();
        while ($reg = $rspinfo->fetch_object()) {
            $info[] = array(
                "0" => $reg->descripcion,
                "1" => $reg->fecha
            );
        }

        $rspsi = $venta->pdfformasi($idventa);
        $fsi = Array();
        while ($reg = $rspsi->fetch_object()) {
            $fsi[] = array(
                "0" => $reg->descripcion,
                "1" => $reg->porcentaje
            );
        }

        $rspsn = $venta->pdfformasn($idventa);
        $fsn = Array();
        while ($reg = $rspsn->fetch_object()) {
            $fsn[] = array(
                "0" => $reg->descripcion,
                "1" => $reg->porcentaje
            );
        }

        $rspboleta = $venta->pdfboleta($idventa);
        $boletas = Array();
        while ($reg = $rspboleta->fetch_object()) {
            $boletas[] = array(
                "0" => $reg->banco,
                "1" => $reg->descripcion,
                "2" => $reg->documento,
                "3" => $reg->tipo,
                "4" => $reg->validez
            );
        }

        $rspequipos = $venta->pdfequipos($idventa);
        $equipos = Array();
        while ($reg = $rspequipos->fetch_object()) {
            $equipos[] = array(
                "0" => $reg->codigo,
                "1" => $reg->ken,
                "2" => $reg->capkg,
                "3" => $reg->capper,
                "4" => $reg->paradas,
                "4" => $reg->tascensor,
                "4" => $reg->marca,
                "4" => $reg->modelo,
            );
        }
        
        $results = array(
            "Datos" => $datos,
            "Etapas" => $ninfo,
            "Info" => $info,
            "FSi" => $fsi,
            "FSn" => $fsn,
            "Boletas" => $boletas,
            "Equipos" => $equipos
        );

        echo json_encode($results);
        break;
}
?>