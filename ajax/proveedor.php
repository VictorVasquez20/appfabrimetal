<?php
session_start();

require_once '../modelos/Proveedor.php';
require_once '../modelos/Presupuesto.php';

$prov = new Proveedor();

$idproveedor = isset($_POST['idproveedor']) ? $_POST['idproveedor'] : "";
$rut = isset($_POST['rut']) ? $_POST['rut'] : ""; 
$razonsocial = isset($_POST['razonsocial']) ? $_POST['razonsocial'] : ""; 
$idcategoria = isset($_POST['idcategoria']) ? $_POST['idcategoria'] : ""; 
$idregion = isset($_POST['idregion']) ? $_POST['idregion'] : ""; 
$idcomuna = isset($_POST['idcomuna']) ? $_POST['idcomuna'] : ""; 
$direccion = isset($_POST['direccion']) ? $_POST['direccion'] : ""; 
$contacto = isset($_POST['contacto']) ? $_POST['contacto'] : ""; 
$fono = isset($_POST['fono']) ? $_POST['fono'] : ""; 
$email = isset($_POST['email']) ? $_POST['email'] : ""; 
$idcondicionpago = isset($_POST['idcondicionpago']) ? $_POST['idcondicionpago'] : ""; 
$plazopago = isset($_POST['plazopago']) ? $_POST['plazopago'] : ""; 
$servicioflete = isset($_POST['servicioflete']) ? $_POST['servicioflete'] : ""; 
$observacion = isset($_POST['observacion']) ? $_POST['observacion'] : ""; 
$condicion = isset($_POST['condicion']) ? $_POST['condicion'] : 0;

switch ($_GET['op']){
    case 'guardaryeditar':
        if(!$idproveedor){
            $iduser = $_SESSION['iduser'];
            $rspta = $prov->Insertar($rut, $razonsocial, $idcategoria, $idregion, $idcomuna, $direccion, $contacto, $fono, $email, $idcondicionpago, $plazopago, $servicioflete, $observacion, $condicion, $iduser);
            echo $rspta;
        }else{
            $rspta = $prov->Editar($idproveedor, $rut, $razonsocial, $idcategoria, $idregion, $idcomuna, $direccion, $contacto, $fono, $email, $idcondicionpago, $plazopago, $servicioflete, $observacion, $condicion);
            echo $rspta;
        }
        break;
    
    case 'mostrar':
        $rspta = $prov->Mostrar($idproveedor);
        echo json_encode($rspta);
        break;
    
    case 'ficha':
        $rspta = $prov->Ficha($idproveedor);
        echo json_encode($rspta);
        break;
    
    case 'listar':
        $idcategoria = isset($_REQUEST['idcategoria']) ? $_REQUEST['idcategoria'] : ""; 
        
        $rspta = $prov->Listar($idcategoria);
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            
            $ev = '<p class="ratings">'
                    . '<a href="#"><span class="fa '. ($reg->evaluacion >= 1 ? 'fa-star' : 'fa-star-o') .'"></span></a>'
                    . '<a href="#"><span class="fa '. ($reg->evaluacion >= 2 ? 'fa-star' : 'fa-star-o') .'"></span></a>'
                    . '<a href="#"><span class="fa '. ($reg->evaluacion >= 3 ? 'fa-star' : 'fa-star-o') .'"></span></a>'
                    . '<a href="#"><span class="fa '. ($reg->evaluacion >= 4 ? 'fa-star' : 'fa-star-o') .'"></span></a>'
                    . '<a href="#"><span class="fa '. ($reg->evaluacion >= 5 ? 'fa-star' : 'fa-star-o') .'"></span></a>'
                    . '</p>';
            
            $data[] = array(
                "0" => '<button class="btn btn-warning btn-xs" onclick="mostrar(' . $reg->idproveedor . ')" data-tooltip="tooltip" title="Editar"><i class="fa fa-pencil"></i></button>'.
                       '<button class="btn btn-info btn-xs" onclick="ficha(' . $reg->idproveedor . ')" data-tooltip="tooltip" title="Ficha"><i class="fa fa-list-alt"></i></button>'.
                       '<button class="btn btn-success btn-xs" onclick="evaluar(' . $reg->idproveedor . ')" data-tooltip="tooltip" title="Evaluar"><i class="fa fa-star"></i></button>',
                "1" => $reg->razonsocial,
                "2" => $reg->rut,
                "3" => $reg->categoriastr,
                "4" => $reg->created_time,
                "5" => $reg->ultimacompra,
                "6" => $ev,
                "7" => ($reg->condicion) ? '<span class="label bg-green">Activo</span>' : '<span class="label bg-red">No activo</span>'
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
        
    case 'selectpresupuesto':
        $presu = new Presupesto();
        $rspta = $presu->listar_pre();
        echo '<option selected disabled>Seleccione un presupuesto</option>';
        while ($reg = $rspta->fetch_object()) {
            if($reg->estado = 6){
                echo '<option value=' . $reg->idpresupuesto . '>' . $reg->npresupuesto . '</option>';
            }
        }
        break;
}