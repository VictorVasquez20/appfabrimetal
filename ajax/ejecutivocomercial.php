<?php
session_start();

require_once '../modelos/EjecutivoComercial.php';

$ejecutivo = new EjecutivoComercial();

$idejecutivo = isset($_POST['idejecutivo']) ? $_POST['idejecutivo'] : "";
$nombre = isset($_POST['nombre']) ? $_POST['nombre'] : "";
$apellido = isset($_POST['apellido']) ? $_POST['apellido'] : "";
$email = isset($_POST['email']) ? $_POST['email'] : "";
$telefono = isset($_POST['telefono']) ? $_POST['telefono'] : "";
$idproveedor = isset($_POST['idproveedor']) ? $_POST['idproveedor'] : ""; 
$condicion = isset($_POST['condicion']) ? $_POST['condicion'] : "";

switch ($_GET['op']){
    case 'guardaryeditar':
        if(!$idejecutivo){
            $respt = $ejecutivo->Insertar($nombre, $apellido, $email, $telefono, $idproveedor, $condicion);
            echo $respt;
        }else{
            $respt = $ejecutivo->Editar($idejecutivo, $nombre, $apellido, $email, $telefono, $idproveedor, $condicion);
            echo $respt;
        }
        break;
        
    case 'listar':
        $idproveedor = isset($_REQUEST['idproveedor']) ? $_REQUEST['idproveedor'] : ""; 
        $rspta = $ejecutivo->Listar($idproveedor);
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => $reg->nombre. " ". $reg->apellido,
                "1" => $reg->telefono,
                "2" => $reg->email,
                "3" => '<span class="glyphicon glyphicon-remove-sign" aria-hidden="true" onclick="delejecutivo(' .$reg->idejecutivo .')"></span>'
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
    
    case 'eliminar':
        if($idejecutivo != 0){
            $rspta = $ejecutivo->Eliminar($idejecutivo);
            echo $rspta;
        }
        break;
}

