<?php 
	require_once "../modelos/DocsAsociados.php";
	$docsAsociados = new DocsAsociados();

	switch ($_GET["op"]) {
		case 'mostrarimagenes':
			$data = $_POST;
			$docsAsociados = $docsAsociados->BuscarImagenes('GSERVICIO_PRESUPUESTO', $data['id'],$data['tabla']);
			$img = '<div class="row">';
			while ($reg = $docsAsociados->fetch_object()){
				$img .= '<div class="col-12 col-lg-4 col-md-4 col-sm-12"><a href="../files/docsasociados/'.$reg->documento.'" data-lightbox="image-1"><img class="img-thumbnail" data-lightbox="image" src="../files/docsasociados/'.$reg->documento.'"/></a></div>';
				
			}
			$img .='</div>';
			echo json_encode(array("imagenes"=>$img));
		break;

		case 'mostrarimagenesAnalisis':
			$data = $_POST;
			$docsAsociados = $docsAsociados->BuscarImagenes('APPFABRIMETAL_ANALISISFALLA', $data['id'],$data['tabla']);
			$img = '<div class="row">';
			while ($reg = $docsAsociados->fetch_object()){
				$img .= '<div class="col-12 col-lg-4 col-md-4 col-sm-12"><a href="../files/docsasociados/'.$reg->documento.'" data-lightbox="image-1"><img class="img-thumbnail" data-lightbox="image" src="../files/docsasociados/'.$reg->documento.'"/></a></div>';
				
			}
			$img .='</div>';
			echo json_encode(array("imagenes"=>$img));
		break;
	}

 ?>