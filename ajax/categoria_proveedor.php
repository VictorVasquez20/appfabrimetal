<?php
session_start();

require_once '../modelos/Categoria_proveedor.php';

$cat = new Categoria_proveedor();

$idcategoria = isset($_POST['idcategoria']) ? $_POST['idcategoria'] : "";
$nombre = isset($_POST['nombre']) ? $_POST['nombre'] : "";
$condicion = isset($_POST['condicion']) ? $_POST['condicion'] : 0;

switch ($_GET['op']) {
    case 'guardaryeditar':

        if (!$idcategoria) {
            $rspta = $cat->Insertar($nombre, $condicion);
            echo $rspta;
        } else {
            $rspta = $cat->Editar($idcategoria, $nombre, $condicion);
            echo $rspta;
        }

        break;

    case 'listar':
        $rspta = $cat->Listar();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => '<button class="btn btn-warning btn-xs" onclick="mostrar(' . $reg->idcategoria . ')"><i class="fa fa-pencil"></i></button>',
                "1" => $reg->nombre,
                "2" => ($reg->condicion) ? '<span class="label bg-green">Activo</span>' : '<span class="label bg-red">No activo</span>'
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;

    case 'mostrar':
        $rspta = $cat->Mostrar($idcategoria);
        echo json_encode($rspta);
        break;

    case 'selectcategoria':
        $rspta = $cat->SelectCategoria();
        echo '<option selected disabled>Seleccione una categoria</option>';
        while ($reg = $rspta->fetch_object()) {
            echo '<option value=' . $reg->idcategoria . '>' . $reg->nombre . '</option>';
        }
        break;
        
    case 'selectcategoriafiltro':
        $rspta = $cat->SelectCategoria();
        echo '<option value="0">TODOS</option>';
        while ($reg = $rspta->fetch_object()) {
            echo '<option value=' . $reg->idcategoria . '>' . $reg->nombre . '</option>';
        }
        break;
}

