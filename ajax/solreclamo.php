<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

session_start();

require_once "../modelos/SolReclamo.php";

$solreclamo = new SolReclamo();

$idsolreclamo = isset($_POST["idsolreclamo"]) ? limpiarCadena($_POST["idsolreclamo"]) : "";            
$nombre = mb_strtoupper(isset($_POST["nombre"]) ? limpiarCadena($_POST["nombre"]) : "");
$rut = mb_strtoupper(isset($_POST["rut"]) ? limpiarCadena($_POST["rut"]) : "");
$telefono = mb_strtoupper(isset($_POST["telefono"]) ? limpiarCadena($_POST["telefono"]) : "");
$email = mb_strtoupper(isset($_POST["email"]) ? limpiarCadena($_POST["email"]) : "");
$cargo = mb_strtoupper(isset($_POST["cargo"]) ? limpiarCadena($_POST["cargo"]) : "");
$edificio = mb_strtoupper(isset($_POST["edificio"]) ? limpiarCadena($_POST["edificio"]) : "");
$mensaje = mb_strtoupper(isset($_POST["mensaje"]) ? limpiarCadena($_POST["mensaje"]) : "");
$motivo = mb_strtoupper(isset($_POST["motivo"]) ? limpiarCadena($_POST["motivo"]) : "");
        
switch ($_GET["op"]) {
    
	case 'guardar':
            
            $rspta=$solreclamo->insertar($nombre, $rut, $telefono, $email, $cargo, $edificio, $mensaje);
             
            if($rspta){
                $estatus=1;
                $mensaje="Solicitud Registrada, en breves momentos nos contactaremos con Usted.";
            }else{
                $estatus=0;
                $mensaje="No se Pudo Registrar la Solicitud.";
            }
         
        echo json_encode(array('estatus'=>$estatus,'mensaje'=>$mensaje));

        break;
    
    case 'listar':
        
        $rspta = $solreclamo->listar();
        
        $data = Array();
        
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" =>'<button class="btn btn-primary btn-xs" onclick="procesado('.$reg->idsolreclamo.')" data-tooltip="tooltip" title="PROCESAR"><i class="fa fa-check"></i></button>
                       <button class="btn btn-danger btn-xs" onclick="descartar('.$reg->idsolreclamo.')" data-tooltip="tooltip" title="DESCARTAR"><i class="fa fa-close"></i></button>',
                "1" => $reg->edificio,
                "2" => $reg->nombre,
                "3" => $reg->cargo,
                "4" => $reg->telefono,
                "5" => $reg->email,
                "6" => $reg->created_time,
            );
        }
        
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        
    break;
    

    case 'procesar':
        
        $iduser = $_SESSION['iduser'];
        
        $rspta = $solreclamo->procesado($idsolreclamo, $iduser);
        
        if($rspta){
             $estatus=1;
             $mensaje="Solicitud Procesada";
        }else{
            $estatus=0;
            $mensaje="Solicitud no pudo ser Procesada";
        }
         
        echo json_encode(array('estatus'=>$estatus,'mensaje'=>$mensaje));

    break;

    case 'descartar':
        
        $iduser = $_SESSION['iduser'];

        $rspta = $solreclamo->rechazado($idsolreclamo, $motivo, $iduser);
        
        if($rspta){
             $estatus=1;
             $mensaje="Solicitud Descartada";
        }else{
            $estatus=0;
            $mensaje="Solicitud no pudo ser descartada";
        }
         
        echo json_encode(array('estatus'=>$estatus,'mensaje'=>$mensaje));
         
    break;   
    
}