<?php
session_start();

require_once '../modelos/Stock.php';
require_once '../modelos/Producto.php';

$stock = new Stock();
$producto =  new Producto();

$idstock = isset($_POST['idstock']) ? $_POST['idstock'] : "";
$idproducto = isset($_POST['idproducto']) ? $_POST['idproducto'] : 0;
$idbodega = isset($_POST['idbodega']) ? $_POST['idbodega']: 0;

$referencia = isset($_POST['referencia']) ? $_POST['referencia'] : "";
$cantidad = isset($_POST['cantidad']) ? $_POST['cantidad'] : 0;
$tipomovimiento = isset($_POST['tipomovimiento']) ? $_POST['tipomovimiento'] : 0;
$centrocosto = isset($_POST['centrocosto']) ? $_POST['centrocosto'] : 0;
$solicitante = isset($_POST['solicitante']) ? $_POST['solicitante'] : 0;
$autoriza = isset($_POST['autoriza']) ? $_POST['autoriza'] : 0;
$created_user = $_SESSION['iduser'];
 
/*
* $tipomovimiento
* 1 = ingreso
* 0 = salida
*/

switch ($_GET["op"]){
    case 'guardaryeditar':
        
        if(!$idstock){
            $rspta1 = $stock->insertar($idproducto, $idbodega, $created_user, $referencia, $cantidad, $tipomovimiento, $centrocosto, $solicitante, $autoriza);
            if($rspta1 == 1){
                $rspta2 = $producto->editarStock($idproducto, $cantidad, $tipomovimiento, $idbodega);
            }
            echo $rspta2; //== 1 ? "Stock guardado" : "Stock no guardado".$rspta1. " - ".$rspta2;
        } else {
            $rspta = $stock->editar($idstock, $idproducto, $idbodega, $referencia, $cantidad, $tipomovimiento, $centrocosto, $solicitante, $autoriza);
            echo $rspta; //? "Stock editado" : "Stock no editado";
        }
        break;
    case "listar":
        $idproducto = $_GET['idp'];
        $bodega = $_GET['bodega'];
        
        $rspta = $stock->listar($idproducto, $bodega);
        $data = Array();
        while ($reg = $rspta->fetch_object()){
            $data[] = array(
                "0"=>date("d/m/Y - H:i:s" , strtotime($reg->created_time)),
                "1"=>$reg->nombUser. ' - ' .$reg->referencia ,
                "2"=>$reg->cantidad,
                "3"=>($reg->tipomovimiento) ?'<span class="label bg-green">Entrada</span>':'<span class="label bg-red">Salida</span>'
                
            );
        }
        $results = array(
                "sEcho"=>1,
                "iTotalRecords"=>count($data),
                "iTotalDisplayRecords"=>count($data), 
                "aaData"=>$data
        );

        echo json_encode($results);
        break;
        
    case "stock":
        $idproducto = $_GET['idproducto'];
        
        $rspta = $stock->stock($idproducto);
        $data = Array();
        while ($reg = $rspta->fetch_object()){
            $data[] = array(
                "0"=>$reg->producto,
                "1"=>$reg->stock,
                "2"=>$reg->cantidad,
                "3"=>$reg->nombre,
                "4"=>$reg->idbodega
            );
        }
        /*$results = array(
                "sEcho"=>1,
                "iTotalRecords"=>count($data),
                "iTotalDisplayRecords"=>count($data), 
                "aaData"=>$data
        );*/

        echo json_encode($data);
        break;
}