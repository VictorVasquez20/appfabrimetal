<?php
require_once '../modelos/Detenidos.php';

$detenidos = new Detenidos();

switch ($_GET["op"]) {

	case 'ContarDatos':

		$enEval = 0;
		$coti = 0;
		$apro = 0;

		$rspta=$detenidos->listarDetenidos();
		$data = Array();
		while ($reg = $rspta->fetch_object()){
			$rspta1=$detenidos->listaDatos($reg->idascensor);
			$data1 = Array();
			while ($reg1 = $rspta1->fetch_object()){

				$presup = $reg1->idpresupuesto;
				$estado_presup = $reg1->estado;
				if ($presup == null){
					$enEval = $enEval + 1;
				}
				else{
					if ($estado_presup == 1 || $estado_presup == 3 || $estado_presup == 4){
						// SOLICITADO, INFORMADO, PROCESADO
						$enEval = $enEval + 1;
					}
					if ($estado_presup == 2){
						// INFORMACIÓN
						$enEval = $enEval + 1;
					}
					if ($estado_presup == 5){
						// ENVIADO
						$coti = $coti + 1;
					}
					if ($estado_presup == 6){
						// ACEPTADO
						$apro = $apro + 1;
					}

				}
			}
		}
    
        $results = array(
                "detenidos"=>$detenidos->detenidos(),
                "evaluacion"=>$enEval, 
                "cotizados"=>$coti, 
                "aprobados"=>$apro
            );

        echo json_encode($results);
	break;

	case 'listar':
		$rspta=$detenidos->listarDetenidos();
		$data = Array();
		while ($reg = $rspta->fetch_object()){
			$rspta1=$detenidos->listaDatos($reg->idascensor);
			$data1 = Array();
			while ($reg1 = $rspta1->fetch_object()){

				$presup = $reg1->idpresupuesto;
				$estado_presup = $reg1->estado;
				if ($presup == null){
					$estado = "<span class='label label-warning'>EN EVALUACIÓN</span>";
					$depto = "INGENIERÍA";
					$responsable = "PATRICIO VÁSQUEZ";
				}
				else{
					if ($estado_presup == 1 || $estado_presup == 3 || $estado_presup == 4){
						// SOLICITADO, INFORMADO, PROCESADO
						$estado = "<span class='label label-warning'>EN EVALUACIÓN</span>";
						$depto = "PRESUPUESTOS";
						$responsable = " ";
					}
					if ($estado_presup == 2){
						// INFORMACIÓN
						$estado = "<span class='label label-warning'>EN EVALUACIÓN</span>";
						$depto = "INGENIERÍA";
						$responsable = "PATRICIO VÁSQUEZ";
					}
					if ($estado_presup == 5){
						// ENVIADO
						$estado = "<span class='label label-success'>COTIZADO</span>";
						$depto = "PRESUPUESTOS";
						$responsable = " ";
					}
					if ($estado_presup == 6){
						// ACEPTADO
						$estado = "<span class='label label-danger'>APROBADO</span>";
						$depto = "OPERACIONES";
						$responsable = "EMMANUEL GONZÁLEZ";
					}

				}

				$data[] = array(					
					"0"=>$reg1->idservicio,
					"1"=>$reg1->edificio,
					"2"=>$reg1->codigo,
					"3"=>$reg1->closed_time,
					"4"=>$reg1->tecnico,
					"5"=>$reg1->dias_detenido,
					"6"=>$reg1->region,
					"7"=>$estado,
					"8"=>$depto,
					"9"=>$responsable,
					"10"=>$reg1->observacionfin
				);
			}
		}
		$results = array(
				"sEcho"=>1,
				"iTotalRecords"=>count($data),
				"iTotalDisplayRecords"=>count($data), 
				"aaData"=>$data
			);

		echo json_encode($results);
	break;

	case 'GraficoDetenidosPorMes':

        $anio_busq = isset($_REQUEST['anio_busq']) ? $_REQUEST['anio_busq'] : "YEAR(NOW())";
        
        $rspta = $detenidos->IngresoGSEDetenidosPorMes($anio_busq);
        $data_ingreso = Array();
        while ($reg = $rspta->fetch_object()) {
            $data_ingreso[] = array(
                "mes" => $reg->mes,
                "cantidad" => $reg->cantidad
            );
        }
        
        
        echo json_encode( array('ingreso'=>$data_ingreso,'salida'=>$data_ingreso) );
        
	break; 
}