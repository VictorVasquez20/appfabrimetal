<?php
session_start();
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');


require_once "../modelos/SolNosotros.php";

$solnosotros = new SolNosotros();

$idsolnosotros = isset($_POST["idsolnosotros"]) ? $_POST["idsolnosotros"] : 0;
$procesado = isset($_POST["procesado"]) ? $_POST["procesado"] : 0;
$condicion = isset($_POST["condicion"]) ? $_POST["condicion"] : 0;


switch ($_GET["op"]) {
    case 'guardar':
        $nombre = isset($_POST["nombre"]) ? limpiarCadena($_POST["nombre"]) : "";
        $telefono = isset($_POST["telefono"]) ? limpiarCadena($_POST["telefono"]) : "";
        $email = isset($_POST["email"]) ? limpiarCadena($_POST["email"]) : "";
        $area = isset($_POST["area"]) ? limpiarCadena($_POST["area"]) : "";
        $ext = explode(".", $_FILES['archivo']['name']);
        if ($_FILES['archivo']['type'] == "application/pdf") {
            $archivo = round(microtime(true)) . "." . end($ext);
            move_uploaded_file($_FILES['archivo']['tmp_name'], "../files/cv/" . $archivo);
        }
        $rspta = $solnosotros->insertar($nombre,$telefono,$email, $area, $archivo);
        echo $rspta ? "NFORMACION ENVIADA" : "NO SE PUDO ENVIAR INFORMACION";
        break;
        
    case 'listar':
        $rspta = $solnosotros->Listar();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => '<a class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="Imprimir CV" href="../files/cv/'.$reg->archivo.'" target="_blank"><i class="fa fa-list-alt"></i></a>'
                     . '<button class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="top" title="Procesado" onclick="editar(' . $reg->idsolnosotros . ', '. $reg->procesado .')"><i class="fa fa-check"></i></button>',
                "1" => $reg->nombre,
                "2" => $reg->telefono,
                "3" => $reg->email,
                "4" => $reg->nombArea,
                "5" => $reg->procesado == 0 ? '<span class="label bg-red">No</span>':'<span class="label bg-green">Si</span>'
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        
        break;
    
    case 'editar':
        if($idsolnosotros){
            $closed_user = $_SESSION["iduser"];
            
            //var_dump($idsolnosotros.", ".$closed_user.", ". $procesado.", ". $condicion);
            $rspta = $solnosotros->Editar($idsolnosotros, $closed_user, $procesado, $condicion);
            echo $rspta ? "Editado con exito" : "No editado";
        }
        break;
    case 'mostrar':
        $rspta = $solnosotros->Mostrar($idsolnosotros);
        echo json_encode($rspta);
        break;
}


