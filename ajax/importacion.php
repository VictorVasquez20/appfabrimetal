<?php
session_start();

require_once '../modelos/Importacion.php';

$imp =  new Importacion();

$idimportacion = isset($_POST['idimportacion']) ? $_POST['idimportacion'] : "";
$codigo = isset($_POST['codigo']) ? $_POST['codigo'] : "";
$nef = isset($_POST['nef']) ? $_POST['nef'] : "";
$proveedor = isset($_POST['proveedor']) ? strtoupper($_POST['proveedor']) : "";
$created_user = isset($_POST['created_user']) ? $_POST['created_user'] : "";
$arrival_time = isset($_POST['arrival_time']) ? $_POST['arrival_time'] : NULL;
$dispatch_time = isset($_POST['dispatch_time']) ? $_POST['dispatch_time'] : NULL;

switch($_GET['op']){
    case 'guardaryeditar':
        
        if(!$idimportacion){
            $created_user = $_SESSION['iduser'];
            $rspta = $imp->Insertar($codigo, $nef, $proveedor, $created_user);
            echo $rspta;
        }else{
            $rspta = $imp->Editar($idimportacion, $codigo, $nef, $proveedor);
            echo $rspta ? "Importacion editada." : "Importacion no editada.";
        }
        break;
      
    case 'listar':
        $rspta = $imp->Listar();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            if(!$reg->dispatch_time){
                $op = '<button class="btn btn-warning btn-xs" onclick="mostrar(' . $reg->idimportacion . ')" data-toggle="tooltip" data-placement="top" data-original-title="Editar"><i class="fa fa-pencil"></i></button>';
            }else{
                $op = '<button class="btn btn-primary btn-xs" onclick="mostrar(' . $reg->idimportacion . ')" data-toggle="tooltip" data-placement="top" data-original-title="Editar"><i class="fa fa-eye"></i></button>';
            }
            if(!$reg->dispatch_time){
                $op.= '<button class="btn btn-info btn-xs" onclick="despacho(' . $reg->idimportacion . ')" data-toggle="tooltip" data-placement="top" data-original-title="Despacho"><i class="fa fa-truck"></i></button>';
            }else{
                if(!$reg->arrival_time){
                    $op.= '<button class="btn btn-success btn-xs" onclick="llegada(' . $reg->idimportacion . ')" data-toggle="tooltip" data-placement="top" data-original-title="Llegada"><i class="fa fa-archive"></i></button>';
                }
            }
            
            
            $data[] = array(
                "0" => $op,
                "1" => $reg->codigo,
                "2" => $reg->nef,
                "3" => $reg->proveedor,
                "4" => $reg->dispatch_time != NULL ? date("d/m/Y h:i:s", strtotime($reg->dispatch_time)).'<br><span class="label bg-green">Despachado</span>' : "", 
                "5" => $reg->arrival_time != NULL ? date("d/m/Y h:i:s", strtotime($reg->arrival_time)).'<br><span class="label bg-green">Llegada</span>': ""
             );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );
        echo json_encode($results);
        break;
        
    case 'selectimportacion':
        $rspta = $imp->selectimportacion();
        echo '<option value="" selected disabled>Seleccione importación</option>';
        while($reg = $rspta->fetch_object()){
            echo '<option value='.$reg->idimportacion.'>'.$reg->codigo.' / ' .$reg->proveedor .'</option>';
        }
        break;
        
    case 'mostrar':
        $rspta = $imp->Mostrar($idimportacion);
        echo json_encode($rspta);
        break;
    
    case 'editartime':
        if($idimportacion){
            $rspta = $imp->Editartime($idimportacion, $arrival_time, $dispatch_time);
            echo $rspta;
        }
        break;
}