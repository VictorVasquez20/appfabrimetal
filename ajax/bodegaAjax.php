<?php
session_start();

require_once '../modelos/Bodega.php';

$bodega =  new Bodega();

$idbodega = isset($_POST["idbodega"]) ? $_POST["idbodega"] : "";
$nombre = strtoupper(isset($_POST["nombre"]) ? $_POST["nombre"] : "");
$descripcion = strtoupper(isset($_POST["descripcion"]) ? $_POST["descripcion"] : "");
$vigencia = isset($_POST["vigencia"]) ? $_POST["vigencia"] : 0;

switch($_GET["op"]){
    case "guardaryeditar":
        if(empty($idbodega)){
            $rspta = $bodega->Insertar($nombre, $descripcion, $vigencia);
            echo $rspta ? "Bodega Guardada" : "Bodega No Guardada";
        }else{
            $rspta = $bodega->Editar($idbodega, $nombre, $descripcion, $vigencia);
            echo $rspta ? "Bodega Editada" : "Bodega No Editada";
        }
        break;
    case "listar":
        
        $rspta = $bodega->Listar();
        $data = Array();
        while ($reg = $rspta->fetch_object()){
            $data[] = array(
                "0"=>'<button class="btn btn-warning btn-xs" onclick="mostrar('.$reg->idbodega.')"><i class="fa fa-pencil"></i></button>',
                "1"=>$reg->nombre,
                "2"=>$reg->descripcion,
                "3"=>($reg->vigencia)?'<span class="label bg-green">Activo</span>':'<span class="label bg-red">No activo</span>'
            );
        }
        $results = array(
                "sEcho"=>1,
                "iTotalRecords"=>count($data),
                "iTotalDisplayRecords"=>count($data), 
                "aaData"=>$data
        );

        echo json_encode($results);
        
        break;
    
    case "mostrar":
        $rspta=$bodega->mostrar($idbodega);
        echo json_encode($rspta);
        break;
    
    case "selectbodega":
        $rspta = $bodega->SelectBodega();
        echo '<option value="" selected disabled>Seleccione bodega</option>';
        while($reg = $rspta->fetch_object()){
            if($reg->idbodega == $idcategoria){
                echo '<option value='.$reg->idbodega.' selected="selected">'.$reg->nombre.'</option>';
            }else{
                echo '<option value='.$reg->idbodega.'>'.$reg->nombre.'</option>';
            }
        }
        break;
}
