<?php
session_start();
require_once '../modelos/InfoVenta.php';

$iv = new InfoVenta();

$idinfoventa = isset($_POST["idinforventa"]) ? $_POST["idinforventa"] : 0;
$idascensor = isset($_POST["idascensor"]) ? $_POST["idascensor"] : 0;
$descripcion = isset($_POST["descripcion"]) ? strtoupper($_POST["descripcion"]) : ""; 
$fecha = isset($_POST["fecha"]) ? $_POST["fecha"] : "";

switch($_GET["op"]){
    case 'guardaryeditar':
        if(!$idinfoventa){
            $rspta = $iv->Insertar($idascensor, $descripcion, $fecha);
            echo $rspta;
        }else{
            $rspta = $iv->Editar($idinfoventa, $idascensor, $descripcion, $fecha);
            echo $rspta ? "1" : "0"; 
        }
        break;
        
    case 'listar':
        
        $idventa = $_POST['idventa'];
        $rspta = $iv->Listar($idventa);
        $data = Array();
        $countinfo = 0;
        
        while ($reg = $rspta->fetch_object()) {
           $hidden = "" ;       
            
            $data[] = array(
                "0" => $reg->idinfoventa,
                "1" => $reg->idascensor,
                "2" => $reg->descripcion,
                "3" => date_format(date_create($reg->fecha), "Y-m-d" ),
                "4" => "<i class='fa fa-trash' onclick='delinfo($reg->idinfoventa);'></i>",
                "5" => $reg->codigo
            );
                    
        }
        
        echo json_encode($data);
        break;
        
    case 'listarrevisar':
        
        $idventa = $_POST['idventa'];
        $rspta = $iv->ListarRevisar($idventa);
        $data = Array();
        $countinfo = 0;
        
        while ($reg = $rspta->fetch_object()) {
           $hidden = "" ;       
            
            $data[] = array(
                "0" => $reg->idinfoventa,
                "1" => $reg->idascensor,
                "2" => $reg->descripcion,
                "3" => date_format(date_create($reg->fecha), "Y-m-d" )
            );
                    
        }
        
        echo json_encode($data);
        break;
      
    case 'eliminar':
        
        $ids = $_POST["ids"];
        
        $rspta = $iv->Eliminar($ids);
        echo $rspta ? "Eliminado" : "NO eliminado";
        break;
    
    case 'eliminarxascensor':
        $rspta = $iv->Eliminarxascensor($idascensor);
        echo $rspta ? "Eliminado" : "NO eliminado";
        break;
}
