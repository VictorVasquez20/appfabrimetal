<?php 

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');


require_once "../modelos/SolAtencion.php";

$solatencion = new SolAtencion();


switch ($_GET["op"]) {
	case 'guardar':
            $nombre = isset($_POST["nombre"]) ? limpiarCadena($_POST["nombre"]) : "";
            $rut = isset($_POST["rut"]) ? limpiarCadena($_POST["rut"]) : "";
            $telefono = isset($_POST["telefono"]) ? limpiarCadena($_POST["telefono"]) : "";
            $email = isset($_POST["email"]) ? limpiarCadena($_POST["email"]) : "";
            $cargo = isset($_POST["cargo"]) ? limpiarCadena($_POST["cargo"]) : "";
            $edificio = isset($_POST["edificio"]) ? limpiarCadena($_POST["edificio"]) : "";
            $idascensor = isset($_POST["idascensor"]) ? limpiarCadena($_POST["idascensor"]) : "";
            $mensaje = isset($_POST["mensaje"]) ? limpiarCadena($_POST["mensaje"]) : "";
            $rspta=$solatencion->insertar($nombre, $rut, $telefono, $email, $cargo, $edificio, $idascensor, $mensaje);
            echo $rspta ? "SOLICITUD REGISTRADA, EN BREVES MOMENTOS NOS CONTACTAREMOS CON USTED" : "NO SE PUDO REGISTRAR LA SOLICITUD";
	break;
    
    case 'guardarm':
            $nombre = isset($_POST["nombrem"]) ? limpiarCadena($_POST["nombrem"]) : "";
            $rut = isset($_POST["rutm"]) ? limpiarCadena($_POST["rutm"]) : "";
            $telefono = isset($_POST["telefonom"]) ? limpiarCadena($_POST["telefonom"]) : "";
            $email = isset($_POST["emailm"]) ? limpiarCadena($_POST["emailm"]) : "";
            $cargo = isset($_POST["cargom"]) ? limpiarCadena($_POST["cargom"]) : "";
            $edificio = isset($_POST["edificiom"]) ? limpiarCadena($_POST["edificiom"]) : "";
            $idascensor = isset($_POST["idascensorm"]) ? limpiarCadena($_POST["idascensorm"]) : "";
            $mensaje = isset($_POST["mensajem"]) ? limpiarCadena($_POST["mensajem"]) : "";
            $rspta=$solatencion->insertar($nombre, $rut, $telefono, $email, $cargo, $edificio, $idascensor, $mensaje);
            echo $rspta ? "SOLICITUD REGISTRADA, EN BREVES MOMENTOS NOS CONTACTAREMOS CON USTED" : "NO SE PUDO REGISTRAR LA SOLICITUD";
	break;
    
    case 'listar':
        $rspta = $solatencion->listar();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" =>'<button class="btn btn-success btn-xs" onclick="procesado('.$reg->idsolatencion.')" data-tooltip="tooltip" title="PROCESAR"><i class="fa fa-check"></i></button><button class="btn btn-warning btn-xs" onclick="descartar('.$reg->idsolatencion.')" data-tooltip="tooltip" title="DESCARTAR"><i class="fa fa-close"></i></button>',
                "1" => $reg->edificio,
                "2" => $reg->idascensor,
                "3" => $reg->nombre,
                "4" => $reg->cargo,
                "5" => $reg->telefono,
                "6" => $reg->email,
                "7" => $reg->created_time,
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
    break;
    
    case 'pantalla':
        $rspta = $solatencion->listarpantalla();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            
            if(is_null($reg->resultado)){
                $estado='<button type="button" class="btn btn-danger btn-xs">SOLICITADA</button>';
            }elseif($reg->resultado == 0){
                $estado='<button type="button" class="btn btn-warning btn-xs">DESCARTADA</button>';
            }elseif($reg->resultado == 1){
                $estado='<button type="button" class="btn btn-success btn-xs">PROCESADA</button>';
            }
            
            $data[] = array(
                "0" => $estado,
                "1" => $reg->hora,
                "2" => $reg->edificio,
                "3" => $reg->idascensor,
                "4" => $reg->nombre,
                "5" => $reg->cargo,
                "6" => $reg->telefono,
                "7" => $reg->email,
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
    break;
    
    case 'procesar':
        $iduser = $_SESSION['iduser'];
        $idsolatencion = isset($_POST["idsolatencion"]) ? limpiarCadena($_POST["idsolatencion"]) : "";
        $rspta = $solatencion->procesado($idsolatencion, $iduser);
        echo $rspta ? "SOLICITUD PROCESADA" : "SOLICITUD NO PUDO SER PROCESADA";
    break;

    case 'descartar':
        $iduser = $_SESSION['iduser'];
        $idsolatencion = isset($_POST["idsolatencion"]) ? limpiarCadena($_POST["idsolatencion"]) : "";
        $motivo = isset($_POST["motivo"]) ? limpiarCadena($_POST["motivo"]) : "";
        $rspta = $solatencion->rechazado($idsolatencion, $motivo, $iduser);
        echo $rspta ? "SOLICITUD DESCARTADA" : "SOLICITUD NO PUDO SER DESCARTADA";
    break;

}


