<?php
session_start();
require_once '../modelos/Tecnico.php';

$tecnico = new Tecnico();

switch ($_GET['op']){
    case 'selecttecnico':
        $rspta = $tecnico->selecttecnico();
        echo '<option value="" selected disabled>Seleccione Tecnico</option>';
        while($reg = $rspta->fetch_object()){
            echo '<option value='.$reg->idtecnico.'>'.$reg->nombre.' '.$reg->apellidos.' / '.$reg->rut.'</option>';
        }
        break;
        
    case 'selecttecnicoSupervisor':
        $idsupervisor = isset($_POST["idsupervisor"]) ? $_POST["idsupervisor"] : 0;
        $rspta = $tecnico->selecttecnicosupervisor($idsupervisor);
        echo '<option value="0" selected>TODOS</option>';
        while($reg = $rspta->fetch_object()){
            echo '<option value='.$reg->idtecnico.'>'.$reg->nombre.' '.$reg->apellidos.' / '.$reg->rut.'</option>';
        }
        break;
        
    case 'selecttecnicoSupervisorRut':
        $rutsupervisor = isset($_POST["rutsupervisor"]) ? $_POST["rutsupervisor"] : $_SESSION['rut'];
        $rspta = $tecnico->selecttecnicosupervisorRut($rutsupervisor);
        echo '<option value="0" selected>TODOS</option>';
        while($reg = $rspta->fetch_object()){
            echo '<option value='.$reg->idtecnico.'>'.$reg->nombre.' '.$reg->apellidos.' / '.$reg->rut.'</option>';
        }
        break;
}