<?php

session_start();
require_once "../modelos/Contrato.php";
require_once "../modelos/Cliente.php";
require_once "../modelos/Edificio.php";
require_once "../modelos/Ascensor.php";
require_once "../modelos/ContactoCliente.php";
require_once '../modelos/Contacto.php';
require_once '../modelos/CentroCosto.php';

$contrato = new Contrato();
$cliente = new Cliente();
$edificio = new Edificio();
$ascensor = new Ascensor();
$contactocli = new ContactoCliente();
$contactoedif = new Contacto();
$centros = new CentroCosto();


//Datos desde el formulario - Seccion de contrato
$idcontrato = isset($_POST["idcontrato"]) ? limpiarCadena($_POST["idcontrato"]) : "";
$tcontrato = isset($_POST["tcontrato"]) ? limpiarCadena($_POST["tcontrato"]) : "";
$ncontrato = isset($_POST["ncontrato"]) ? limpiarCadena($_POST["ncontrato"]) : "";
$nexterno = isset($_POST["nexterno"]) ? limpiarCadena($_POST["nexterno"]) : "";
$nreferencia = isset($_POST["nreferencia"]) ? limpiarCadena($_POST["nreferencia"]) : "";
$fecha = isset($_POST["fecha"]) ? limpiarCadena($_POST["fecha"]) : "";
$fecha_ini = isset($_POST["fecha_ini"]) ? limpiarCadena($_POST["fecha_ini"]) : "";
$fecha_fin = isset($_POST["fecha_fin"]) ? limpiarCadena($_POST["fecha_fin"]) : "";
$idperiocidad = isset($_POST["idperiocidad"]) ? limpiarCadena($_POST["idperiocidad"]) : "";
$ubicacion = isset($_POST["ubicacion"]) ? limpiarCadena($_POST["ubicacion"]) : "";
$observaciones = isset($_POST["observaciones"]) ? limpiarCadena($_POST["observaciones"]) : "";
$rautomatica = isset($_POST["rautomatica"]) ? limpiarCadena($_POST["rautomatica"]) : "";



switch ($_GET["op"]) {

    case 'guardaryeditar':
        $resp = "";
        $iduser = $_SESSION['iduser'];
        if (empty($idcontrato)) {
            $idcon = $contrato->insertar($tcontrato, $ncontrato, $nexterno, $nreferencia, $ubicacion, $fecha, $fecha_ini, $fecha_fin, $idperiocidad, $observaciones);
            if ($idcon > 0) {
                for ($i = 0; $i < (int) $nclientes; $i++) {
                    $id = $cliente->VerCliente($_POST['rut' . $i . '']);
                    if (!empty($id)) {
                        $idcli = intval($id["idcliente"]);
                    } else {
                        $idcli = $cliente->insertar($iduser, $_POST['tcliente' . $i . ''], $_POST['rut' . $i . ''], $_POST['razon_social' . $i . ''], $_POST['calle' . $i . ''], $_POST['numero' . $i . ''], $_POST['oficina' . $i . ''], $_POST['idregiones' . $i . ''], $_POST['idprovincias' . $i . ''], $_POST['idcomunas' . $i . '']);
                    }
                    if ($idcli > 0) {
                        $idconcli = $contrato->contrato_cliente($idcli, $idcon);
                        if ($idconcli > 0) {
                            for ($o = 0; $o < (int) $_POST['ncon_cli' . $i . '']; $o++) {
                                $idcontacli = $contactocli->insertar($_POST['tipocon' . $i . '' . $o . ''], $_POST['nombre_concli' . $i . '' . $o . ''], $_POST['numero_concli' . $i . '' . $o . ''], $_POST['email_concli' . $i . '' . $o . '']);
                                if ($idcontacli > 0) {
                                    $respconcli = $contactocli->contacto_cc($idcontacli, $idconcli);
                                    if ($respconcli) {
                                        echo "Contrato - Cliente (OK) / ";
                                    } else {
                                        echo "Error asociaciones contacto-cliente-contrato";
                                    }
                                } else {
                                    echo "Error contacto cliente";
                                }
                            }
                        } else {
                            echo "Error asociaciones contrato-clientes";
                        }
                    } else {
                        echo "Error Cliente";
                    }
                }

                for ($i = 0; $i < (int) $nedificios; $i++) {
                    $id = $edificio->VerEdificio($_POST['nombre' . $i . ''], $_POST['calle_ed' . $i . ''], $_POST['numero_ed' . $i . '']);
                    if (!empty($id)) {
                        $idedi = intval($id["idedificio"]);
                    } else {
                        $idedi = $edificio->insertar($_POST['nombre' . $i . ''], $_POST['calle_ed' . $i . ''], $_POST['numero_ed' . $i . ''], $_POST['idtsegmento' . $i . ''], $_POST['corcorreo' . $i . ''], $_POST['residente' . $i . ''], $_POST['idregiones_ed' . $i . ''], $_POST['idprovincias_ed' . $i . ''], $_POST['idcomunas_ed' . $i . '']);
                    }
                    if ($idedi > 0) {
                        $idconedi = $contrato->edificio_contrato($idedi, $idcon);
                        if ($idconedi > 0) {
                            for ($w = 0; $w < (int) $_POST['ncon_edi' . $i . '']; $w++) {
                                $idcontaedif = $contactoedif->insertar($_POST['nombre_conedi' . $i . '' . $w . ''], $_POST['numero_conedi' . $i . '' . $w . ''], $_POST['email_conedi' . $i . '' . $w . '']);
                                if ($idcontaedif > 0) {
                                    $respconedi = $contactoedif->contacto_ec($idcontaedif, $idconedi);
                                    if ($respconedi) {
                                        echo "Contrato - Edificio (OK) / ";
                                    } else {
                                        echo "Error asociaciones contacto-cliente-contrato";
                                    }
                                } else {
                                    echo "Error contacto edificio";
                                }
                            }

                            for ($o = 0; $o < (int) $_POST['nascensores' . $i . '']; $o++) {
                                //echo $iduser.' / '.$idconedi.' / '.$_POST['idtascensor'.$i.''.$o.''].' / '.$_POST['marca'.$i.''.$o.''].' / '.$_POST['modelo'.$i.''.$o.''].' / '.$_POST['valoruf'.$i.''.$o.''].' / '.$_POST['valorclp'.$i.''.$o.''].' / '.$_POST['paradas'.$i.''.$o.''].' / '.$_POST['capper'.$i.''.$o.''].' / '.$_POST['capkg'.$i.''.$o.''].' / '.$_POST['velocidad'.$i.''.$o.''].' / '.$_POST['pservicio'.$i.''.$o.''].' / '.$_POST['gtecnica'.$i.''.$o.''].' / '.$_POST['ken'.$i.''.$o.''].' / '.$_POST['dcs'.$i.''.$o.''].' / '.$_POST['elink'.$i.''.$o.''].'<br/>';
                                //$ascensor->insertar($iduser, $idconedi, $_POST['idtascensor'.$i.''.$o.''],$_POST['marca'.$i.''.$o.''],$_POST['modelo'.$i.''.$o.''],$_POST['valoruf'.$i.''.$o.''],$_POST['valorclp'.$i.''.$o.'']);
                                $respas = $ascensor->insertar($iduser, $idconedi, $_POST['idtascensor' . $i . '' . $o . ''], $_POST['marca' . $i . '' . $o . ''], $_POST['modelo' . $i . '' . $o . ''], $_POST['valoruf' . $i . '' . $o . ''], $_POST['valorclp' . $i . '' . $o . ''], $_POST['paradas' . $i . '' . $o . ''], $_POST['capper' . $i . '' . $o . ''], $_POST['capkg' . $i . '' . $o . ''], $_POST['velocidad' . $i . '' . $o . ''], $_POST['pservicio' . $i . '' . $o . ''], $_POST['gtecnica' . $i . '' . $o . ''], $_POST['ken' . $i . '' . $o . ''], $_POST['dcs' . $i . '' . $o . ''], $_POST['elink' . $i . '' . $o . '']);
                                if ($respas) {
                                    echo "Ascensores (OK)";
                                } else {
                                    echo "Error ascensores";
                                }
                            }
                        } else {
                            echo " Error asociaciones contrato-edificios";
                        }
                    } else {
                        echo "Error datos edificios";
                    }
                }
                echo "Contrato registrado";
            } else {
                echo " Error datos de contrato";
            }
        }
        break;

    case 'guardar':
        $iduser = $_SESSION['iduser'];
        $idregiones = isset($_POST["conidregiones"]) ? limpiarCadena($_POST["conidregiones"]) : "";
        $nascensores = isset($_POST["nascensores"]) ? limpiarCadena($_POST["nascensores"]) : "";
        $idcliente = isset($_POST["idcliente"]) ? limpiarCadena($_POST["idcliente"]) : "";
        $idedificio = isset($_POST["idedificio"]) ? limpiarCadena($_POST["idedificio"]) : "";
        $nexterno = !empty($nexterno) ? $nexterno : "S/N";
        $nreferencia = !empty($nreferencia) ? $nreferencia : "S/N";
        $ubicacion = !empty($ubicacion) ? $ubicacion : "S/N";
        $fecha_fin = !empty($fecha_fin) ? $fecha_fin : "0000-00-00";
        
        $idcontrato = $contrato->InsertarContrato($idcliente, $idperiocidad, $tcontrato, $idregiones, $ncontrato, $nexterno, $nreferencia, $ubicacion, $fecha, $fecha_ini, $fecha_fin, $rautomatica, $observaciones);

        if (!empty($idcontrato)) {
            /*if ($nascensores != '') {
            $nasc = 0;
            for ($i = 0; $i < intval($nascensores); $i++) {
                $ubicacionasc = isset($_POST['ubicacionasc'.$i.'']) ? limpiarCadena($_POST['ubicacionasc'.$i.'']) : "";
                $cocliente = isset($_POST["idedificio"]) ? limpiarCadena($_POST["idedificio"]) : "";
                $ken = isset($_POST['ken'.$i.'']) ? limpiarCadena($_POST['ken'.$i.'']) : "";
                $pservicio = isset($_POST['pservicio'.$i.'']) ? limpiarCadena($_POST['pservicio'.$i.'']) : "";
                $gtecnica =  isset($_POST['gtecnica'.$i.'']) ? limpiarCadena($_POST['gtecnica'.$i.'']) : "";
                $ubicacionasc = !empty($ubicacionasc) ? $ubicacionasc : "S/N";
                $cocliente = !empty($cocliente) ? $cocliente : "S/N";
                $ken = !empty($ken) ? $ken : "S/N";
                $pservicio = !empty($pservicio) ? $pservicio : "0000-00-00";
                $gtecnica =  !empty($gtecnica) ? $gtecnica : "0000-00-00";
                $respas = $ascensor->InsertarAscensor($idedificio, $idcontrato, $_POST['idtascensor'.$i.''], $_POST['marca'.$i.''], $_POST['modelo'.$i.''], $ubicacionasc, $_POST['codigo'.$i.''], $cocliente, $ken, $_POST['paradas'.$i.''], $_POST['capper'.$i.''], $_POST['capkg'.$i.''], $_POST['velocidad'.$i.''], $_POST['dcs'.$i.''], $_POST['elink'.$i.''], $pservicio, $gtecnica, $iduser);
                $respas? $nasc++:""; 
            }
            var_dump($nasc);
            var_dump(intval($nascensores));
            if($nasc == intval($nascensores)){
                    echo "CONTRATO Y EQUIPOS REGISTRADOS";
                }else{
                    echo "CONTRATO REGISTRADO, PERO OCURRIO UN ERROR AL REGISTRAR EQUIPOS";
                }        
                
                }else{
                    echo "CONTRATO REGISTRADO, PERO NO SE REGISTRO NINGUN EQUIPO";
                }*/
            echo $idcontrato;
        } else {
            echo "CONTRATO NO PUDO SER REGISTRADO";
        }
        break;

    case 'editar':
        $idcontrato = isset($_POST["fidcontrato"]) ? limpiarCadena($_POST["fidcontrato"]) : "";
        $tcontrato = isset($_POST["ftcontrato"]) ? limpiarCadena($_POST["ftcontrato"]) : "";
        $ncontrato = isset($_POST["fncontrato"]) ? limpiarCadena($_POST["fncontrato"]) : "";
        $nexterno = isset($_POST["fnexterno"]) ? limpiarCadena($_POST["fnexterno"]) : "";
        $nreferencia = isset($_POST["fnreferencia"]) ? limpiarCadena($_POST["fnreferencia"]) : "";
        $fecha = isset($_POST["ffecha"]) ? limpiarCadena($_POST["ffecha"]) : "";
        $fecha_ini = isset($_POST["ffecha_ini"]) ? limpiarCadena($_POST["ffecha_ini"]) : NULL;
        $fecha_fin = isset($_POST["ffecha_fin"]) ? limpiarCadena($_POST["ffecha_fin"]) : NULL;
        $idperiocidad = isset($_POST["fidperiocidad"]) ? limpiarCadena($_POST["fidperiocidad"]) : "";
        $ubicacion = isset($_POST["fubicacion"]) ? limpiarCadena($_POST["fubicacion"]) : "";
        $observaciones = isset($_POST["fobservaciones"]) ? limpiarCadena($_POST["fobservaciones"]) : "";
        $calle = isset($_POST["fcalle"]) ? limpiarCadena($_POST["fcalle"]) : "";
        $numero = isset($_POST["fnumero"]) ? limpiarCadena($_POST["fnumero"]) : "";
        $oficina = isset($_POST["foficina"]) ? limpiarCadena($_POST["foficina"]) : "";
        $idregiones = isset($_POST["fidregiones"]) ? limpiarCadena($_POST["fidregiones"]) : "";
        $idprovincias = isset($_POST["fidprovincias"]) ? limpiarCadena($_POST["fidprovincias"]) : "";
        $idcomunas = isset($_POST["fidcomunas"]) ? limpiarCadena($_POST["fidcomunas"]) : "";

        if (!empty($idcontrato)) {
            $rspta = $contrato->editar($idcontrato, $tcontrato, $ncontrato, $nexterno, $nreferencia, $ubicacion, $fecha, $fecha_ini, $fecha_fin, $idregiones, $idprovincias, $idcomunas, $calle, $numero, $oficina, $observaciones, $idperiocidad);
            echo $rspta ? "Contrato editado" : "Contrato no pudo ser editado";
        }
        break;


    case 'listarcontrato':
        $rspta = $contrato->listar();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                /*"0" => '<button class="btn btn-info btn-xs" onclick="mostar(' . $reg->idcontrato . ')"><i class="fa fa-list-alt"></i></button><button class="btn btn-info btn-xs" onclick="editar(' . $reg->idcontrato . ')"><i class="fa fa-pencil"></i></button><button class="btn btn-info btn-xs" type="button" onclick="editarcontrato(' . $reg->idcontrato . ')"><i class="fa fa-plus-circle"></i></button>',*/
                "0" => '<button class="btn btn-info btn-xs" onclick="mostar(' . $reg->idcontrato . ')"><i class="fa fa-list-alt"></i></button><button class="btn btn-info btn-xs" type="button" onclick="editarcontrato(' . $reg->idcontrato . ')"><i class="fa fa-pencil"></i></button>',
                "1" => $reg->ncontrato,
                "2" => $reg->fecha,
                "3" => $reg->tipo,
                "4" => $reg->razon_social . ' - RUT ' . $reg->rut,
                "5" => $reg->ubicacion,
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
        
    case 'selectcontrato':
        $rspta = $contrato->listar();
        echo '<option value="" selected disabled>Seleccione contrato</option>';
        while($reg = $rspta->fetch_object()){
            echo '<option value='.$reg->idcontrato.'>'.$reg->ncontrato.' -  '.$reg->razon_social. ' / '.$reg->rut.'</option>';
        }
        break;

    case 'listarsoid':
        $rspta = $contrato->listarsolid();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => '<button class="btn btn-info btn-xs" onclick="addformasc(' . $reg->idcontrato . ',' . $reg->nascensores . ')" data-tooltip="tooltip" title="Asignar IDs" ><i class="fa fa-hashtag"></i> Asignar IDs</button>',
                "1" => $reg->ncontrato,
                "2" => $reg->fecha,
                "3" => $reg->region_nombre . ' - ' . $reg->region_ordinal,
                "4" => $reg->nedificios,
                "5" => $reg->nascensores
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;

    case 'edificio_contrato':
        $rspta = $edificio->edificios_contrato($idcontrato);
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => '<button class="btn btn-info btn-xs" data-tooltip="tooltip" title="Modificar"><i class="fa fa-pencil"></i></button>',
                "1" => $reg->nombre,
                "2" => $reg->calle . ' ' . $reg->numero,
                "3" => $reg->segmento,
                "4" => $reg->region,
                "5" => $reg->comuna
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );
        echo json_encode($results);
        break;

    case 'ascensor_contrato':
        $rspta = $ascensor->ascensores_contrato($idcontrato);
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => '<button class="btn btn-info btn-xs" data-tooltip="tooltip" title="Modificar"><i class="fa fa-pencil"></i></button>',
                "1" => $reg->codigo,
                "2" => $reg->codigocli,
                "3" => $reg->ken,
                "4" => $reg->marca,
                "5" => $reg->modelo,
                "6" => $reg->edificio,
                "7" => $reg->region
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );
        echo json_encode($results);
        break;

    case 'cc_contrato':
        $rspta = $contrato->contrato_cc($idcontrato);
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => '<button class="btn btn-info btn-xs" data-tooltip="tooltip" title="Modificar"><i class="fa fa-pencil"></i></button>',
                "1" => $reg->codigo,
                "2" => $reg->nombre
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );
        echo json_encode($results);
        break;

    case 'mostrar_contrato':
        $dcontrato = $contrato->mostrar($idcontrato);
        $dcliente = $cliente->cliente_contrato($idcontrato);

        $results = array(
            "dcontrato" => $dcontrato,
            "dcliente" => $dcliente
        );

        echo json_encode($results);
        break;

    case 'formeditar':
        $rspta = $contrato->formeditar($idcontrato);
        echo json_encode($rspta);
        break;
    
    case 'VerificarNContrato':
        $rspta = $contrato->VerificarNContrato($ncontrato);
        echo $rspta;
        break;

    case 'editarcontrato':
        $idcontrato = $_POST['idcontrato'];
        $rspta = $contrato->vercontrato($idcontrato);
        echo json_encode($rspta);
    break;
    case 'guardareditar':
        $idcontrato = isset($_POST['idcontrato']) ? limpiarCadena($_POST['idcontrato']) : "";
        $idcliente = isset($_POST['idcliente']) ? limpiarCadena($_POST['idcliente']) : "";
        $idregiones = isset($_POST['idregiones']) ? limpiarCadena($_POST['idregiones']) : "";
        $tcontrato = isset($_POST['tcontrato']) ? limpiarCadena($_POST['tcontrato']) : "";
        $ncontrato = isset($_POST['ncontrato']) ? limpiarCadena($_POST['ncontrato']) : "";
        $nexterno = isset($_POST['nexterno']) ? limpiarCadena($_POST['nexterno']) : "";
        $ubicacion = isset($_POST['ubicacion']) ? limpiarCadena($_POST['ubicacion']) : "";
        $fecha = isset($_POST['fecha']) ? limpiarCadena($_POST['fecha']) : "";
        $fecha_ini = isset($_POST['fecha_ini']) ? limpiarCadena($_POST['fecha_ini']) : "";
        $fecha_fin = isset($_POST['fecha_fin']) ? limpiarCadena($_POST['fecha_fin']) : "";
        $idperiocidad = isset($_POST['idperiocidad']) ? limpiarCadena($_POST['idperiocidad']) : "";
        $rautomatica = isset($_POST['rautomatica']) ? limpiarCadena($_POST['rautomatica']) : "";
        $observaciones = isset($_POST['observaciones']) ? limpiarCadena($_POST['observaciones']) : "";

        if (!empty($idcontrato)) {
            $rspta = $contrato->guardareditar($idcontrato, $idcliente, $idregiones, $tcontrato, $ncontrato, $nexterno, $ubicacion, $fecha, $fecha_ini, $fecha_fin, $idperiocidad, $rautomatica, $observaciones);
            echo $rspta ? "Contrato editado" : "Contrato no pudo ser editado";
            //echo $rspta;
        }
    break;
}
?>