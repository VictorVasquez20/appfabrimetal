<?php

session_start();
require_once '../modelos/VisitaInstalaciones.php';
require_once '../modelos/Proyecto.php';
require_once '../modelos/Ascensor.php';
require_once '../modelos/EtapaProyecto.php';
require_once '../modelos/DocsAsociados.php';
require_once '../modelos/Encuesta.php';

$visita = new VisitasInstalaciones();
$proyecto = new Proyecto();
$ascensor = new Ascensor();
$docsasociados = new DocsAsociados();
$encuesta = new Encuesta();


//Datos desde el formulario - Editar ascensor
$idproyecto = isset($_POST["idproyecto"]) ? limpiarCadena($_POST["idproyecto"]) : "";
$idestado = isset($_POST["idestado"]) ? limpiarCadena($_POST["idestado"]) : "";
$idventa = isset($_POST["idventa"]) ? limpiarCadena($_POST["idventa"]) : "";
$idascensor1 = isset($_POST["idascensor1"]) ? limpiarCadena($_POST["idascensor1"]) : "";
$tipoproyecto = isset($_REQUEST['tipoproyecto']) ? $_REQUEST['tipoproyecto'] : "";

switch ($_GET["op"]) {
    case 'visita':
        $iduser = $_SESSION['iduser'];
        $scensores = count($_POST['idascensor']);

        for ($o = 0; $o < (int) $scensores; $o++) {

            $idascensor = $_POST['idascensor'][$o];
            $observaciones = limpiarCadena($_POST['observaciones' . $idascensor]);
            $esetapa = $_POST['esetapa' . $idascensor];
            $estadoins = $_POST['estadoins' . $idascensor];

            $rspta = $visita->insertar($idproyecto, $idestado, $observaciones, $iduser, $idascensor);

            if ($esetapa == 1) {
                $ascensor->estado($idascensor, (intval($estadoins) + 1));
            }
        }



        echo $rspta ? "REGISTRADA" : "ERROR AL REGISTRAR";
        break;


    case 'visitayetapa':

        //id usuario por session
        $iduser = $_SESSION['iduser'];  
        $ascensores = isset($_POST['idascensor']) ? count($_POST['idascensor']) : 0;
        //inicializo objeto etapa
        //recorro ascensores
        for($o = 0; $o < $ascensores; $o++) {
            $_POST['idascensor'][$o].'<br>';
            $idascensor = $_POST['idascensor'][$o];
            $observaciones = limpiarCadena($_POST['observaciones']);

            $esetapa = isset($_POST['esetapa']) ? $_POST['esetapa'] : "";
        //inserto la visita por ascensor
            if ($esetapa == 1) {
                $estadoins = $_POST['estadoins']+1;
                $ascensor->estado($idascensor, $estadoins);
            }
            $rspta = $visita->insertarRetorna($idproyecto, $idestado, $observaciones, $iduser, $idascensor);

        // recorro los archivos para insertar y copiar en la carpeta
        if(isset($_FILES['file']) && !empty($_FILES['file'])){
            $contador = count($_FILES['file']['name']);
            for ($i=0; $i < $contador; $i++) {
                $array = explode('.', $_FILES['file']['name'][$i]);
                $extension = end($array);
                $newImgName = 'visita-' . $rspta . '-' . $i . '-' . date('YmdHis') . '.' . $extension;
                $temporal = $_FILES['file']['tmp_name'][$i];
                $destino = '../files/docsasociados/'.$newImgName;
                copy($temporal,$destino);
                $tabla = 'visita';
                $datoid = $rspta;
                $documento = $newImgName;
                $docsasociados->insertarVisita('APPFABRIMETAL_VISITA_EQUIPO_FOTO', $tabla, $datoid, $documento);
            }
        }
        }
        echo $rspta ? "REGISTRADA" : "ERROR AL REGISTRAR";
        break;


    case 'listar':
        $rspta = $proyecto->listarEquipos($_SESSION['rut'], $tipoproyecto);

        $data = Array();

        while ($reg = $rspta->fetch_object()) {

            $barra = '<td class="project_progress"><div class="progress progress_sm" style="margin-bottom:5px;"><div class="progress-bar bg-green" role="progressbar" data-transitiongoal="' . $reg->carga . '" aria-valuenow="' . $reg->carga . '" style="width: ' . $reg->carga . '%;"></div></div><small>' . $reg->carga . '% Completo</small></td>';
            $barraestado = '<button type="button" class="btn btn-xs" style="background-color:' . $reg->color . '; color: #fff;">' . $reg->estadonomb . '</button>';

            $op = "";

            if ($_SESSION['SupProyectos'] == 1) {
                $op = '<div class="btn-group">'
                        . ' <button data-toggle="dropdown" class="btn btn-success dropdown-toggle btn-xs" type="button"><i class="fa fa-cog"></i></button>'
                        . ' <ul role="menu" class="dropdown-menu">'
                        . '     <li><a onclick="mostrar(' . $reg->idascensor . ')">Visita</a></li>'
                        . '     <li class="divider"></li>'
                        . '     <li><a onclick="revisar(' . $reg->idproyecto . ')">Visualizar estado</a></li>'
                        . '</ul>'
                        . '</div>';
            } elseif ($_SESSION['PMProyectos'] == 1 && $reg->estado > 1) {
                $op = '<div class="btn-group">'
                        . ' <button data-toggle="dropdown" class="btn btn-success dropdown-toggle btn-xs" type="button"><i class="fa fa-cog"></i></button>'
                        . ' <ul role="menu" class="dropdown-menu">'
                        . '     <li><a onclick="visitaotro(' . $reg->idproyecto . ')">Visita</a></li>'
                        . '     <li class="divider"></li>'
                        . '     <li><a onclick="revisar(' . $reg->idproyecto . ')">Visualizar estado</a></li>';
                $op .= '</ul>'
                        . '</div>';
            } elseif ($_SESSION['PMProyectos'] == 1 && $reg->estado == 1) {
                $op = '<div class="btn-group">'
                        . ' <button data-toggle="dropdown" class="btn btn-success dropdown-toggle btn-xs" type="button"><i class="fa fa-cog"></i></button>'
                        . ' <ul role="menu" class="dropdown-menu">'
                        . '     <li><a onclick="revisar(' . $reg->idproyecto . ')">Visualizar estado</a></li>';
                $op .= '</ul>'
                        . '</div>';
            } elseif ($_SESSION['GEProyectos'] == 1 && $reg->estado > 1) {
                $op = '<div class="btn-group">'
                        . ' <button data-toggle="dropdown" class="btn btn-success dropdown-toggle btn-xs" type="button"><i class="fa fa-cog"></i></button>'
                        . ' <ul role="menu" class="dropdown-menu">'
                        . '     <li><a onclick="visitaotro(' . $reg->idproyecto . ')">Visita</a></li>'
                        . '     <li class="divider"></li>'
                        . '     <li><a onclick="revisar(' . $reg->idproyecto . ')">Visualizar estado</a></li>';
                $op .= '</ul>'
                        . '</div>';
            } elseif ($_SESSION['GEProyectos'] == 1 && $reg->estado == 1) {
                $op = '<div class="btn-group">'
                        . ' <button data-toggle="dropdown" class="btn btn-success dropdown-toggle btn-xs" type="button"><i class="fa fa-cog"></i></button>'
                        . ' <ul role="menu" class="dropdown-menu">'
                        . '     <li><a onclick="revisar(' . $reg->idproyecto . ')">Visualizar estado</a></li>';
                $op .= '</ul>'
                        . '</div>';
            } elseif ($_SESSION['administrador'] == 1 && $reg->estado > 1) {
                $op = '<div class="btn-group">'
                        . ' <button data-toggle="dropdown" class="btn btn-success dropdown-toggle btn-xs" type="button"><i class="fa fa-cog"></i></button>'
                        . ' <ul role="menu" class="dropdown-menu">'
                        . '     <li><a onclick="mostrar(' . $reg->idascensor . ')">Visita</a></li>'
                        . '     <li class="divider"></li>'
                        . '     <li><a onclick="visitaotro(' . $reg->idproyecto . ')">Visita PM</a></li>'
                        . '     <li class="divider"></li>'
                        . '     <li><a onclick="visitaotro(' . $reg->idproyecto . ')">Visita Gerente</a></li>'
                        . '     <li class="divider"></li>'
                        . '     <li><a onclick="revisar(' . $reg->idproyecto . ')">Visualizar estado</a></li>'
                        . '     <li class="divider"></li>'
                        . '     <li><a onclick="listavisitas(' . $reg->idascensor . ')">Visitas Equipo</a></li>';
                $op .= '</ul>'
                        . '</div>';
            }
            if (($_SESSION['AsigProyectos'] == 1 || $_SESSION['administrador'] == 1) && $reg->estado > 1) {
                // retorna los datos de "INFORME DE VISITA A OBRA"
                $visita = $encuesta->ultimaVisita(1,$reg->idascensor);

                // retona los datos de "SISTEMA DE GESTION DE SEGURIDAD Y SALUD OCUPACIONAL (F-PREV-23)"
                $sistema = $encuesta->ultimaVisita(2,$reg->idascensor);
                if(!empty($visita)){
                    $visitaid = $visita['infv_id'];
                }else{
                    $visitaid = "''";
                }

                if(!empty($sistema)){
                    $sistemaid = $sistema['infv_id'];
                }else{
                    $sistemaid = "''";
                }

                $op .= '<div class="btn-group ml-1"><button class="btn btn-info btn-xs" onclick="MostrarDocumentos(' . $reg->idventa . ',\''.$reg->idproyecto.'\','.$visitaid.','.$sistemaid.')" data-tooltip="tooltip" title="DOCUMENTOS"><i class="fa fa-folder-open"></i></button></div>';
            }
            $data[] = array(
                "0" => $op,
                "1" => $reg->nombre,
                "2" => $reg->codigo,
                "3" => $reg->pm,
                "4" => $reg->supervisor,
                "5" => $barra,
                "6" => $barraestado,
                "7" => $reg->updated_time,
                "8" => $reg->dias);
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;


    /** MODERNIZACIONES * */
    case 'listarModernizacion':
        $rspta = $proyecto->listar($_SESSION['rut'], $tipoproyecto);

        $data = Array();

        while ($reg = $rspta->fetch_object()) {

            $barra = '<td class="project_progress"><div class="progress progress_sm" style="margin-bottom:5px;"><div class="progress-bar bg-green" role="progressbar" data-transitiongoal="' . $reg->carga . '" aria-valuenow="' . $reg->carga . '" style="width: ' . $reg->carga . '%;"></div></div><small>' . $reg->carga . '% Completo</small></td>';
            $barraestado = '<button type="button" class="btn btn-xs" style="background-color:' . $reg->color . '; color: #fff;">' . $reg->estadonomb . '</button>';


            if ($_SESSION['administrador'] == 1 || $_SESSION['Modernizaciones'] == 1) {
                /* $op = '<div class="btn-group">'
                  . ' <button data-toggle="dropdown" class="btn btn-success dropdown-toggle btn-xs" type="button"><i class="fa fa-cog"></i></button>'
                  . ' <ul role="menu" class="dropdown-menu">'
                  . '     <li><a onclick="finalizar(' . $reg->idproyecto . ')">Finalizar</a></li>'
                  . '     <li class="divider"></li>'
                  . '     <li><a onclick="revisar(' . $reg->idproyecto . ')">Visualizar estado</a></li>'
                  . '</ul>'
                  . '</div>'; */
                $op = '<button class="btn btn-info btn-xs" onclick="revisar(' . $reg->idproyecto . ')"><i class="fa fa-eye"></i></button>';
            }

            $data[] = array(
                "0" => $op,
                "1" => $reg->nombre,
                "2" => $reg->codigo,
                "3" => $reg->pm,
                "4" => $reg->supervisor,
                "5" => $barra,
                "6" => $barraestado,
                "7" => $reg->updated_time,
                "8" => $reg->dias);
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;


    case 'finalizarmodernizacion':
        $iduser = $_SESSION['iduser'];
        $estadoins = isset($_POST['idestado2']) ? $_POST['idestado2'] : 0;
        $idproyecto = isset($_POST['idproyecto2']) ? $_POST['idproyecto2'] : 0;
        $scensores = isset($_POST['idascensor']) ? count($_POST['idascensor']) : 0;
        $update_time = date("Y-m-d H:i:s");

        for ($o = 0; $o < (int) $scensores; $o++) {
            $idascensor = $_POST['idascensor'][$o];
            $ascensor->estado($idascensor, (intval($estadoins) + 1));
        }
        $rspta = $proyecto->etapa($idproyecto, (intval($estadoins) + 1), $update_time, $iduser);
        echo $rspta ? "REGISTRADA" : "ERROR AL REGISTRAR" . $rspta;
        break;


    /** MODERNIZACIONES * */
    case 'listarNoPmSup':
        $rspta = $proyecto->listarnoPMSUP($_SESSION['rut'], $tipoproyecto);

        $data = Array();

        while ($reg = $rspta->fetch_object()) {


            if ($_SESSION['AsigProyectos'] == 1) {
                $op = '<button type="button" class="btn btn-success btn-xs" onclick="mostrar(' . $reg->idproyecto . ')"><i class="fa fa-pencil"></i></button><button type="button" class="btn btn-info btn-xs" onclick="pdf(' . $reg->idventa . ')" data-tooltip="tooltip" title="Memo de Venta (PDF)"><i class="fa fa-file-pdf-o"></i></button>';
            } elseif ($_SESSION['administrador'] == 1) {
                $op = '<button type="button" class="btn btn-success btn-xs" onclick="mostrar(' . $reg->idproyecto . ')"><i class="fa fa-pencil"></i></button><button type="button" class="btn btn-info btn-xs" onclick="pdf(' . $reg->idventa . ')" data-tooltip="tooltip" title="Memo de Venta (PDF)"><i class="fa fa-file-pdf-o"></i></button>';
            }

            $data[] = array(
                "0" => $op,
                "1" => $reg->nombre,
                "2" => $reg->codigo,
                "3" => $reg->pm,
                "4" => $reg->supervisor,
                "5" => 'SIN ASIGNAR');
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;


    case 'listarNoPmSupAsignados':
        $rspta = $proyecto->listarnoPMSUPasignados($_SESSION['rut'], $tipoproyecto);

        $data = Array();

        while ($reg = $rspta->fetch_object()) {


            if ($_SESSION['AsigProyectos'] == 1) {
                $op = '<button type="button" class="btn btn-success btn-xs" onclick="mostrar(' . $reg->idproyecto . ')"><i class="fa fa-pencil"></i></button><button type="button" class="btn btn-info btn-xs" onclick="pdf(' . $reg->idventa . ')" data-tooltip="tooltip" title="Memo de Venta (PDF)"><i class="fa fa-file-pdf-o"></i></button>';
            } elseif ($_SESSION['administrador'] == 1) {
                $op = '<button type="button" class="btn btn-success btn-xs" onclick="mostrar(' . $reg->idproyecto . ')"><i class="fa fa-pencil"></i></button><button type="button" class="btn btn-info btn-xs" onclick="pdf(' . $reg->idventa . ')" data-tooltip="tooltip" title="Memo de Venta (PDF)"><i class="fa fa-file-pdf-o"></i></button>';
            }

            $data[] = array(
                "0" => $op,
                "1" => $reg->nombre,
                "2" => $reg->codigo,
                "3" => $reg->pm,
                "4" => $reg->supervisor,
                "5" => 'ASIGNADOS');
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;


    case 'mostrarNoPmSup':
        $rspta = $proyecto->mostrarNoPmSup($idproyecto);
        echo json_encode($rspta);
        break;


    case 'editarNoPmSup':
        if ($idproyecto) {
            $supervisor = isset($_POST['supervisor']) ? $_POST['supervisor'] : 0;
            $pm = isset($_POST['pm']) ? $_POST['pm'] : 0;
            if ($supervisor <> 0 && $pm <> 0) {
                $updated_user = $_SESSION['iduser'];
                $rspta = $proyecto->Editar($idproyecto, $pm, $supervisor, $updated_user);
                echo $rspta ? "Proyecto Editado" : "Proyecto no editado";
            } else {
                echo "Falta Project manager o Supervisor";
            }
        }
        break;


    case 'mostrar':
        $rspta = $proyecto->mostrarEquipos($idascensor1);
        echo json_encode($rspta);
        break;


    case 'numerovisita':
        $rspta = $visita->numerovisita($idproyecto);
        echo json_encode($rspta);
        break;


    case 'PDF':
        $idproyecto = isset($_REQUEST["idproyecto"]) ? limpiarCadena($_REQUEST["idproyecto"]) : "";

        //Se ocupara sistema de plantillas TemplatePower
        require_once("../public/build/lib/TemplatePower/class.TemplatePower.php7.inc.php");
        $t = new TemplatePower("../production/informes/instalaciones/pla_reporte_proyecto.html");
        $t->prepare();

        // $path_reportes = '../files/visitas/reportes/';

        //DATOS DEL PROYECTO
        $datos = $visita->pdfdatos($idproyecto);
        $datos['importacion'] = $datos['codigoimp'] . ' - ' . $datos['proveedor'];

        //NUMERO DE VISITAS
        $nvisitas = $visita->cantvisitas($idproyecto);

        //NUMERO DE EQUIPOS
        $nequipos = $visita->cantequipos($idproyecto);

        //informacion del proyecto
        $t->assign('codigoimp', $datos['codigoimp'] . '');
        $t->assign('proyecto', $datos['proyecto'] . '');
        $t->assign('direcpro', $datos['direcpro'] . '');
        $t->assign('cc', $datos['cc'] . '');
        $t->assign('importacion', $datos['importacion'] . '');
        $t->assign('nequipos', $nequipos . '');
        $t->assign('nvisitas', $nvisitas . '');
        $t->assign('estado', $datos['estado'] . '');
        $t->assign('fechaini', $datos['fechaini'] . '');
        $t->assign('fechaact', $datos['fechaact'] . '');
        $t->assign('fechaclo', $datos['fechaclo'] . '');

        //informacion de venta
        $t->assign('fecha', $datos['fecha'] . '');
        $t->assign('descripcion', $datos['descripcion'] . '');
        $t->assign('codigo', $datos['codigo'] . '');
        $t->assign('vendedor', $datos['vendedor'] . '');
        $t->assign('man_post', $datos['man_post'] . '');
        $t->assign('garan_ex', $datos['garan_ex'] . '');

        //ETAPAS DEL PROYECTO
        $rspetapas = $visita->infoetapas($idproyecto);
        $rows = $rspetapas->fetch_all(MYSQLI_ASSOC);
        foreach($rows as $i => $item)
        {
            $t->newBlock('etapas');
            $t->assign('fecha', $item['fecha'] . '');
            $t->assign('duracion', $item['duracion'] . '');
            $t->assign('nombre', $item['nombre'] . '');
        }

        //DATOS DE LA VISITA
        $rspvi = $visita->infovi($idproyecto);
        $rows = $rspvi->fetch_all(MYSQLI_ASSOC);
        foreach($rows as $i => $item)
        {
            $t->newBlock('datosvisita');
            $t->assign('numvisita', ($i + 1) . '');
            $t->assign('fecha', $item['fecha'] . '');
            $t->assign('estado', $item['estado'] . '');

            if (count($rows) != ($i + 1))
                $t->assign('pagebreak', '<pagebreak />');

            //VISITAS A ASENSORES
            $rspinvi = $visita->infovife($item['fehora'], $idproyecto);
            $rows2 = $rspinvi->fetch_all(MYSQLI_ASSOC);
            foreach($rows2 as $j => $item2)
            {
                $t->newBlock('visitainstalacion');
                $t->assign('codigo', $item2['codigo'] . '');
                $t->assign('colaborador', $item2['colaborador'] . '');
                $t->assign('nombre', $item2['nombre'] . '');
                $t->assign('observacion', $item2['observacion'] . '');
                // if ($item2['file'] && file_exists($path_reportes . $item2['file']))
                //     $t->assign('filevisita', '<img width="300" alt="" src="' . $path_reportes . $item2['file'] . '">' . '');
            }
        }

        $style02 = '../production/informes/css/reportes_fm.css';

        require_once("../public/build/lib/mPdfSC/vendor/autoload.php");

        $mpdf = new \Mpdf\Mpdf([
            'mode' => 'c',
            'margin_left' => 20,
            'margin_right' => 20,
            'margin_top' => 32,
            'margin_bottom' => 20,
            'margin_header' => 16,
            'margin_footer' => 13
        ]);

        $mpdf->SetHTMLHeader('<img src="../production/informes/img/fm-logo-negro.png" height="40" alt="" style="float: left; display: inline-block;"> <div style="text-align: right">Página {PAGENO} / {nb}<div>', 'O', true);

        // $mpdf->SetDisplayMode('fullwidth');
        $mpdf->SetDisplayMode(100);
        // $mpdf->SetDisplayMode('fullpage');

        $mpdf->SetDisplayPreferences('/HideToolbar/CenterWindow/FitWindow');
        // $mpdf->SetDisplayPreferences('/HideMenubar/HideToolbar/CenterWindow/FitWindow');

        $mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list

        // Load a stylesheet
        $stylesheet02 = file_get_contents($style02);

        $html = $t->getOutputContent();

        $mpdf->WriteHTML($stylesheet02, 1); // The parameter 1 tells that this is css/style only and no body/html/text
        $mpdf->WriteHTML($html,2);

        $PROYECTO_REPORTE_PDF = 'REPORTE_DEL_PROYECTO_' . $datos['codigoimp'] . '.pdf';

        $mpdf->Output($PROYECTO_REPORTE_PDF, \Mpdf\Output\Destination::INLINE);

        break;


    case 'dashinstalacion':
        $idproyecto = isset($_POST["idproyecto"]) ? limpiarCadena($_POST["idproyecto"]) : "";
        //DATOS DEL PROYECTO
        $datos = $visita->dashinstalacion($idproyecto);
        
         while ($reg = $datos->fetch_object()) {
             $data[] = array(
                "dia" => $reg->dia,
                "mes" => $reg->mes,
                "user"=> $reg->user,
                "obs" => $reg->observacion,
                "asc" => $reg->codigo,
                "imagen"=> $reg->imagen,
                "fecha" => $reg->fecha);
             
         }
        echo json_encode($data);
        break;

    case 'listavisitas':
        $idascensor = isset($_POST["idascensor"]) ? limpiarCadena($_POST["idascensor"]) : "";

        $idproyecto = isset($_REQUEST["idproyecto"]) ? limpiarCadena($_REQUEST["idproyecto"]) : "";
        // $idproyecto = isset($_POST["idproyecto"]) ? limpiarCadena($_POST["idproyecto"]) : "";

        //Se ocupara sistema de plantillas TemplatePower
        require_once("../public/build/lib/TemplatePower/class.TemplatePower.php7.inc.php");
        $t = new TemplatePower("../production/pla_lista_visitas_equipo.html");
        $t->prepare();

        $idencuesta = isset($_REQUEST["idencuesta"]) ? limpiarCadena($_REQUEST["idencuesta"]) : "";
        $idencuesta = 1;

        
        $t->assign('idascensor', $idascensor . '');

        //DATOS DEL PROYECTO
        // $datos = $visita->pdfdatos($idproyecto);
        /*echo '<pre>';
        print_r($datos);
        echo '</pre>';
        exit();*/

        $rspproyecto = $proyecto->mostrar($idproyecto);
        $t->assign('obra', $rspproyecto['nombre'] . '');
        $t->assign('supervisor', $rspproyecto['supervisor'] . '');
        $t->assign('direccion', $rspproyecto['calle'] . ' ' . $rspproyecto['numero'] . '');

        $rspequipo = $encuesta->infoEquipo($idascensor);
        $rows = $rspequipo->fetch_all(MYSQLI_ASSOC);
        foreach($rows as $i => $item)
        {
            $t->assign('obra', $item['nombre'] . '');
            $t->assign('ascensor', $item['codigo'] . '');
        }

        $rspvisitas = $encuesta->visitasEquipo($idascensor);
        $rows = $rspvisitas->fetch_all(MYSQLI_ASSOC);
        foreach($rows as $i => $item)
        {
            $t->newBlock('visitas');
            $t->assign('idvisita', $item['infv_id'] . '');
            $t->assign('visita', '\'' . $item['infv_id'] . ',' . $item['enc_id'] . '\'');
            $t->assign('tipoencuesta', $item['enc_nombre'] . '');
            $t->assign('fechavis', $item['infv_fecha'] . '');
        }

        

        /*//BLOQUES DE LA ENCUESTA
        $rspbloques = $encuesta->bloques($idencuesta);

        $rows = $rspbloques->fetch_all(MYSQLI_ASSOC);
        foreach($rows as $i => $item)
        {
            $t->newBlock('bloques');
            $t->assign('nombloque', $item['blq_nombre'] . '');

            $idbloque = $item['blq_id'] . '';

            //PREGUNTAS DEL BLOQUE
            $rsppreguntas = $encuesta->preguntas($idbloque);
            $rows2 = $rsppreguntas->fetch_all(MYSQLI_ASSOC);

            foreach($rows2 as $j => $item2)
            {
                $t->newBlock('preguntas');
                $t->assign('pregunta', $item2['preg_nombre'] . '');
                $idpregunta = $item2['preg_id'];
                $t->assign('idpreg', $idpregunta . '');

                $t->assign('comentario', '<textarea class="form-control" rows="2" maxlength="80" name="compreg[' . $item2['preg_id'] . ']"></textarea>');
            }
        }*/

        //print the result
        $html = $t->getOutputContent();
        // $t->printToScreen();
        // echo json_encode($data);
        echo $html;
        break;

    case 'newvisitaequipo':
        $idascensor = isset($_REQUEST["idascensor"]) ? limpiarCadena($_REQUEST["idascensor"]) : "";
        $idencuesta = isset($_REQUEST["tipoencuesta"]) ? limpiarCadena($_REQUEST["tipoencuesta"]) : "";

        // $idproyecto = isset($_REQUEST["idproyecto"]) ? limpiarCadena($_REQUEST["idproyecto"]) : "";
        // $idproyecto = isset($_POST["idproyecto"]) ? limpiarCadena($_POST["idproyecto"]) : "";

        //Se ocupara sistema de plantillas TemplatePower
        require_once("../public/build/lib/TemplatePower/class.TemplatePower.php7.inc.php");
        $t = new TemplatePower("../production/pla_encuesta_visita_obra.html");
        $t->prepare();

        /*$idencuesta = isset($_REQUEST["idencuesta"]) ? limpiarCadena($_REQUEST["idencuesta"]) : "";
        $idencuesta = 1;*/

        
        $t->assign('idascensor', $idascensor . '');
        $t->assign('idencuesta', $idencuesta . '');

        $t->assign('fechanow', date('Y-m-d H:i:s') . '');

        if ($idencuesta == 1)
            $t->assign('tituloencuesta', 'INFORME DE VISITA A OBRA');
        else
            $t->assign('tituloencuesta', 'INFORME GESTION DE SEGURIDAD Y SALUD OCUPACIONAL');


        //DATOS DEL PROYECTO
        // $datos = $visita->pdfdatos($idproyecto);
        /*echo '<pre>';
        print_r($datos);
        echo '</pre>';
        exit();*/

        /*$rspproyecto = $proyecto->mostrar($idproyecto);
        $t->assign('obra', $rspproyecto['nombre'] . '');
        $t->assign('supervisor', $rspproyecto['supervisor'] . '');
        $t->assign('direccion', $rspproyecto['calle'] . ' ' . $rspproyecto['numero'] . '');*/

        $rspequipo = $encuesta->infoEquipo($idascensor);
        $rows = $rspequipo->fetch_all(MYSQLI_ASSOC);
        $modelo = '';
        $tipoascensor = '';
        foreach($rows as $i => $item)
        {
            $t->assign('obra', $item['nombre'] . '');
            $t->assign('ascensor', $item['codigo'] . '');
            $modelo = $item['modelo'] . '';
            $tipoascensor = $item['tipoascensor'] . '';
            $t->assign('modelo', $modelo . '');
            $t->assign('tipoascensor', $tipoascensor . '');
            $t->assign('supervisor', $item['supervisor'] . '');
            $t->assign('empresa', trim($item['razon_social']) . '');
            $t->assign('direccion', trim($item['calle'] . ' ' . $item['numero']) . '');
            $t->assign('idcliente', trim($item['idcliente']) . '');
        }

        //BLOQUES DE LA ENCUESTA
        $rspbloques = $encuesta->bloques($idencuesta);

        $rows = $rspbloques->fetch_all(MYSQLI_ASSOC);
        foreach($rows as $i => $item)
        {
            //verificacion de que modelo del equipo contenga el tipo de equipo del bloque
            if (strtolower($tipoascensor) == 'escalera')
                $modelo = $tipoascensor;
            else
            {
                // if(strpos(strtolower($item['blq_tipoequipo']), strtolower($modelo) ) !== 0)
                if(substr(strtolower($item['blq_tipoequipo']), 0, strlen($modelo)) !== strtolower($modelo))
                    $modelo = 'OTROS';
            }
            
            // if(strpos(strtolower($item['blq_tipoequipo']), strtolower($modelo) ) === 0 || $item['blq_tipoequipo'] == 'TODOS')
            if(substr(strtolower($item['blq_tipoequipo']), 0, strlen($modelo)) === strtolower($modelo) || $item['blq_tipoequipo'] == 'TODOS')
            {
                $t->newBlock('bloques');

                $t->assign('nombloque', $item['blq_nombre'] . '');

                $idbloque = $item['blq_id'] . '';

                //PREGUNTAS DEL BLOQUE
                $rsppreguntas = $encuesta->preguntas($idbloque);
                $rows2 = $rsppreguntas->fetch_all(MYSQLI_ASSOC);

                foreach($rows2 as $j => $item2)
                {
                    $t->newBlock('preguntas');
                    $t->assign('pregunta', $item2['preg_nombre'] . '');
                    $idpregunta = $item2['preg_id'];
                    $t->assign('idpreg', $idpregunta . '');

                    $t->assign('comentario', '<textarea class="form-control" rows="2" maxlength="80" name="compreg[' . $item2['preg_id'] . ']"></textarea>');
                }
            }
        }

        //print the result
        $html = $t->getOutputContent();
        // $t->printToScreen();
        // echo json_encode($data);
        echo $html;
        break;

    case 'guardarvisitaequipo':
        $idencuesta = isset($_POST["idencuesta"]) ? limpiarCadena($_POST["idencuesta"]) : "";
        $idascensor = isset($_POST["idascensor"]) ? limpiarCadena($_POST["idascensor"]) : "";
        $idcliente = isset($_POST["idcliente"]) ? limpiarCadena($_POST["idcliente"]) : "";
        
        $nomcli = isset($_POST["nomcli"]) ? limpiarCadena($_POST["nomcli"]) : "";
        $rutcli = isset($_POST["rutcli"]) ? limpiarCadena($_POST["rutcli"]) : "";
        $celularcli = isset($_POST["celularcli"]) ? limpiarCadena($_POST["celularcli"]) : "";
        $emailcli = isset($_POST["emailcli"]) ? limpiarCadena($_POST["emailcli"]) : "";

        $observaciones = isset($_POST["observaciones"]) ? limpiarCadena($_POST["observaciones"]) : "";
        $empresa = isset($_POST["empresa"]) ? limpiarCadena($_POST["empresa"]) : "";
        $direccion = isset($_POST["direccion"]) ? limpiarCadena($_POST["direccion"]) : "";

        //firma del cliente
        $firma = isset($_POST["firma"]) ? limpiarCadena($_POST["firma"]) : '';
        $encoded_image = explode(",", $firma) [1];
        $decoded_image = base64_decode($encoded_image);
        $imgfirma = round(microtime(true)) . ".png";
        $patchfir = "../files/visitaequipo/firmas/" . $imgfirma;
        file_put_contents($patchfir, $decoded_image);

        if (!$idcliente)
            $idcliente = 0;

        switch($idencuesta)
        {
            case 1: //INFORME DE VISITA A OBRA
            case 2: //F-PREV-23
                $params = [
                    'encuesta' => $idencuesta . '',
                    'equipo' => $idascensor . '',
                    'periodo' => date('Ym') . '',
                    // 'fecha' => date('Y-m-d H:i:s'), //es automatico pero se puede setear
                    'observaciones' => $observaciones . '',
                    'empleado' => $_SESSION['iduser'] . '',
                    'firmaempleado' => 'firmarmpleado.png',
                    'cliente' => $idcliente . '',
                    'firmacliente' => $imgfirma . '',
                    'empresa' => $empresa . '',
                    'direccion' => $direccion . '',
                    'nomcli' => $nomcli . '',
                    'rutcli' => $rutcli . '',
                    'celularcli' => $celularcli . '',
                    'emailcli' => $emailcli . ''
                ];

                //SE INSERTA LA VISITA Y SE OBTIENE EL ID, PARA LUEGO INSERTAR LAS RESPUESTAS DE LA VISITA
                $rspvisita = $encuesta->nuevaVisita($params);
                // $rspvisita = 9;

                echo '<br>NUEVO ID DE VISITA A EQUIPO: ' . $rspvisita . '<br>';

                /*$params = [
                    'tiporesp' => [
                    ]
                    'respPreg' => $_POST['preg'],
                    'respComPreg' => $_POST['compreg'],
                    'firmaempleado' => $firmaempleado . '',
                    'cliente' => $idcliente . '',
                    'firmacliente' => $firmacliente . '',
                    'comentario' => $comentario . ''
                ];*/

                $respuestas = array();
                $resp01 = array("tipo" => "pregunta", "data" => $_POST['preg']);
                $resp02 = array("tipo" => "comentario", "data" => $_POST['compreg']);
                $respuestas[] = $resp01;
                $respuestas[] = $resp02;

                $rsppregvisita = $encuesta->nuevaRespuestaVisita($rspvisita, $respuestas);
            break;
            /*default;
                echo 'Por favor haga una nueva selección...';
            break;*/
        }


        echo 'VISITA REGISTRADA CORECTAMENTE';

        break;

    case 'vervisitaequipo':
        $idascensor = isset($_POST["idascensor"]) ? limpiarCadena($_POST["idascensor"]) : "";

        $idproyecto = isset($_POST["idproyecto"]) ? limpiarCadena($_POST["idproyecto"]) : "";

        //Se ocupara sistema de plantillas TemplatePower
        require_once("../public/build/lib/TemplatePower/class.TemplatePower.php7.inc.php");
        $t = new TemplatePower("../production/pla_encuesta_visita_obra.html");
        $t->prepare();

        $idencuesta = isset($_REQUEST["idencuesta"]) ? limpiarCadena($_REQUEST["idencuesta"]) : "";
        $idencuesta = 1;

        $idvisita = isset($_REQUEST["idvisita"]) ? limpiarCadena($_REQUEST["idvisita"]) : "";

        $idproyecto = isset($_REQUEST["idproyecto"]) ? limpiarCadena($_REQUEST["idproyecto"]) : "";
        
        $t->assign('idascensor', $idascensor . '');

        //DATOS DEL PROYECTO
        $datos = $visita->pdfdatos($idproyecto);
        /*echo '<pre>';
        print_r($datos);
        echo '</pre>';
        exit();*/

        $rspproyecto = $proyecto->mostrar($idproyecto);
        $t->assign('obra', $rspproyecto['nombre'] . '');
        $t->assign('supervisor', $rspproyecto['supervisor'] . '');
        $t->assign('direccion', $rspproyecto['calle'] . ' ' . $rspproyecto['numero'] . '');

        $rspequipo = $encuesta->infoEquipo($idascensor);
        $rows = $rspequipo->fetch_all(MYSQLI_ASSOC);
        foreach($rows as $i => $item)
        {
            $t->assign('obra', $item['nombre'] . '');
            $t->assign('ascensor', $item['codigo'] . '');
        }

        //BLOQUES DE LA ENCUESTA
        $rspbloques = $encuesta->bloques($idencuesta);

        //RESPUESTAS DE LA VISITA
        $rspvisita = $encuesta->respPreguntas($idvisita);
        $respuestas = $rspvisita->fetch_all(MYSQLI_ASSOC);
        foreach ( $respuestas as $key => $respuesta )
        {
            $resp[$respuesta['resp_tipo']][$respuesta['resp_idtipo']] = $respuesta['resp_data'] . '';
        }

        /*echo '<pre>';
        print_r($resp);
        echo '</pre>';*/

        $rows = $rspbloques->fetch_all(MYSQLI_ASSOC);
        foreach($rows as $i => $item)
        {
            $t->newBlock('bloques');
            $t->assign('nombloque', $item['blq_nombre'] . '');

            $idbloque = $item['blq_id'] . '';

            //PREGUNTAS DEL BLOQUE
            $rsppreguntas = $encuesta->preguntas($idbloque);
            $rows2 = $rsppreguntas->fetch_all(MYSQLI_ASSOC);

            foreach($rows2 as $j => $item2)
            {
                $t->newBlock('preguntas');
                $t->assign('pregunta', $item2['preg_nombre'] . '');
                $idpregunta = $item2['preg_id'];
                $t->assign('idpreg', $idpregunta . '');

                if (isset($resp['pregunta'][$idpregunta]))
                {
                    $t->assign('opsel', $resp['pregunta'][$idpregunta] . '');
                    /*if ($resp['pregunta'][$idpregunta] == 'SI')
                        $t->assign('respsi', 'SI');
                    else
                        $t->assign('respno', 'NO');*/
                }

                if ($item2['preg_comentario'])
                {
                    if (isset($resp['comentario'][$idpregunta]))
                    {
                        $t->assign('comentario', '<textarea class="form-control" rows="2" maxlength="80" name="compreg[' . $item2['preg_id'] . ']">' . $resp['comentario'][$idpregunta] . '</textarea>');
                        // $t->assign('comentario', '<input type="text" style="width:98%" name="compreg[' . $item2['preg_id'] . ']" value="' . $resp['comentario'][$idpregunta] .'" maxlength="80">');
                    }
                    else
                    {
                        $t->assign('comentario', '<textarea class="form-control" rows="2" maxlength="80" name="compreg[' . $item2['preg_id'] . ']"></textarea>');
                        // $t->assign('comentario', '<input type="text" style="width:98%" name="compreg[' . $item2['preg_id'] . ']" maxlength="80">');
                    }
                }
            }
        }

        /*$params = [
            'encuesta' => $idencuesta . '',
            'equipo' => $idequipo . '',
            'fecha' => $fechavis . '',
            'observaciones' => $observaciones . '',
            'empleado' => $idempleado . '',
            'firmaempleado' => $firmaempleado . '',
            'cliente' => $idcliente . '',
            'firmacliente' => $firmacliente . '',
            'comentario' => $comentario . ''
        ];*/

        /*$params = [
            'encuesta' => '1',
            'equipo' => '33',
            // 'fecha' => date('Y-m-d H:i:s'), //es automatico pero se puede setear
            'observaciones' => 'UNA OBSERVACION DE LA VISITA',
            'empleado' => '1',
            'firmaempleado' => 'firmarmpleado.png',
            'cliente' => '2',
            'firmacliente' => 'firmacliente.png',
            'comentario' => 'comentario de la visita'
        ];

        //SE INSERTA LA VISITA Y SE OBTIENE EL ID
        $rspvisita = $encuesta->nuevaVisita($params);

        echo '<br>NUEVO ID DE VISITA: ' . $rspvisita;*/

        //print the result
        $html = $t->getOutputContent();
        // $t->printToScreen();
        // echo json_encode($data);
        echo $html;
        break;

    case 'PDFVISITA':
        $visita = isset($_REQUEST["visita"]) ? limpiarCadena($_REQUEST["visita"]) : "";
        // echo $visita;
        $visita = explode(",", $visita);
        /*echo $visita[0] . '|' . $visita[1];
        exit();*/

        $idvisita = $visita[0];
        $idencuesta = $visita[1];

        //Se ocupara sistema de plantillas TemplatePower
        require_once("../public/build/lib/TemplatePower/class.TemplatePower.php7.inc.php");

        switch ($idencuesta) {
            case 1:
                $t = new TemplatePower("../production/pla_print_visita_obra.html");
                $headerPdf = '
                <table>
                    <tr>
                        <td class="col-30 sinborde"><img src="../public/build/images/fm-logo-negro.png" height="40" alt=""></td>
                        <td class="textochico verticalcentertext sinborde"><center>Nº VISITA: {idvisita}</center></td>
                        <td class="col-25 right sinborde"><img src="../public/build/images/kone-logo.png" height="40" alt="" style="border:1px solid grey"></td>
                    </tr>
                </table>
                ';
                // $headerPdf = '<img src="../public/build/images/fm-logo-negro.png" height="40" alt="" style="float: left; display: inline-block;"> <div style="text-align: right"><img src="../public/build/images/kone-logo.png" height="40" alt="" style="float: left; display: inline-block; border:1px solid grey"><div>';
                $footerPdf = '';
                $opcionesMPDF = [
                    'mode' => 'c',
                    'margin_left' => 15,
                    'margin_right' => 15,
                    'margin_top' => 30,
                    'margin_bottom' => 20,
                    'margin_header' => 16,
                    'margin_footer' => 13
                ];
                break;
            case 2:
                $t = new TemplatePower("../production/pla_print_f-prev-23.html");
                $headerPdf = '';
                $footerPdf = '
                <table>
                    <tr>
                        <td class="col-25 sinborde"><img src="../public/build/images/fm-logo-negro.png" height="25" alt=""></td>
                        <td class="encabezado verticalcentertext sinborde"><center>DEPARTAMENTO DE PREVENCIÓN DE RIESGOS</center></td>
                        <td class="col-25 right sinborde"><img src="../public/build/images/kone-logo.png" height="25" alt="" style="border:1px solid grey"></td>
                    </tr>
                </table>
                ';
                // $footerPdf = '<img src="../public/build/images/fm-logo-negro.png" height="25" alt="" style="float: left; display: inline-block;"> <span style="text-align: center; display: inline-block; vertical-align: middle">DEPARTAMENTO DE PREVENCIÓN DE RIESGOS</span> <div style="text-align: right"><img src="../public/build/images/kone-logo.png" height="25" alt="" style="float: left; display: inline-block; border:1px solid grey"><div>';
                $opcionesMPDF = [
                    'mode' => 'c',
                    'margin_left' => 15,
                    'margin_right' => 15,
                    'margin_top' => 15,
                    'margin_bottom' => 20,
                    'margin_header' => 16,
                    'margin_footer' => 13
                ];
                break;
            
            // default:
                // code...
                // break;
        }

        $t->prepare();

        // $path_reportes = '../files/visitas/reportes/';

        $rspequipo = $encuesta->infoVisita($idvisita);
        $rows = $rspequipo->fetch_all(MYSQLI_ASSOC);
        $modelo = '';
        $tipoascensor = '';
        $idascensor = '';
        $fechavisita = '';
        foreach($rows as $i => $item)
        {
            $t->assign('obra', $item['nombre'] . '');
            $t->assign('ascensor', $item['codigo'] . '');
            $modelo = $item['modelo'] . '';
            $tipoascensor = $item['tipoascensor'] . '';
            $idascensor = $item['infv_ascensor'] . '';
            $fechavisita = $item['infv_fecha'] . '';
            $t->assign('modelo', $modelo . '');
            $t->assign('tipoascensor', $tipoascensor . '');
            $t->assign('observaciones', $item['infv_observaciones'] . '');
            $t->assign('supervisor', $item['supervisor'] . '');
            $t->assign('nomuser', $item['nomusuario'] . '');
            $t->assign('rutuser', $item['num_documento'] . '');
            $t->assign('nomcli', mb_strtoupper($item['infv_nomcli']) . '');
            $t->assign('rutcli', mb_strtoupper($item['infv_rutcli']) . '');
            $t->assign('celularcli', mb_strtoupper($item['infv_celularcli']) . '');
            $t->assign('emailcli', mb_strtoupper($item['infv_emailcli']) . '');
            $t->assign('fechavisita', $fechavisita . '');
            $t->assign('empresa', (!$item['razon_social'] ? $item['infv_empresa'] . '' : $item['razon_social'] . '') . '');
            $t->assign('direccion', (!$item['calle'] ? $item['infv_direccion'] . '' : $item['calle'] . ' ' . $item['numero']) . '');
            $path_firmacliente = '../files/visitaequipo/firmas/';
            $firmacliente = $path_firmacliente . $item['infv_firmacliente'];
            if (file_exists($firmacliente)) {
                if (is_file($firmacliente))
                    $t->assign('firmacliente', '<img src="' . $firmacliente . '" alt="firmacliente" height="70">' . '');
            }

            $path_firmausuario = '../files/usuarios/firmas/';
            $firmausuario = $path_firmausuario . $item['filefir'];
            if (file_exists($firmausuario)) {
                if (is_file($firmausuario))
                    $t->assign('firmausuario', '<img src="' . $firmausuario . '" alt="firmausuario" height="70">' . '');
            }
        }

        $result = $encuesta->numeroDeVisita($idencuesta, $idascensor, $idvisita);
        /*echo '<pre>';
        print_r($nVisita);
        echo '</pre>';
        exit();*/
        $t->assign('nvisita', $result['nvisita'] . '');
        $t->assign('idvisita', sprintf("%05d", $idvisita) . '');
        // $t->assign('idvisita', date('Y', strtotime($fechavisita)) . sprintf("%05d", $idvisita) . '');

        $headerPdf = str_replace('{nvisita}', $result['nvisita'] . '', $headerPdf);
        $headerPdf = str_replace('{idvisita}', sprintf("%05d", $idvisita) . '', $headerPdf);

        //BLOQUES DE LA ENCUESTA
        $rspbloques = $encuesta->bloques($idencuesta);

        //RESPUESTAS DE LA VISITA
        $rspvisita = $encuesta->respPreguntas($idvisita);
        $respuestas = $rspvisita->fetch_all(MYSQLI_ASSOC);
        foreach ( $respuestas as $key => $respuesta )
        {
            $resp[$respuesta['resp_tipo']][$respuesta['resp_idtipo']] = $respuesta['resp_data'] . '';
        }

        $rows = $rspbloques->fetch_all(MYSQLI_ASSOC);
        $jk = 0;
        foreach($rows as $i => $item)
        {
            //verificacion de que modelo del equipo contenga el tipo de equipo del bloque
            if (strtolower($tipoascensor) == 'escalera')
                $modelo = $tipoascensor;
            else
            {
                // if(strpos(strtolower($item['blq_tipoequipo']), strtolower($modelo) ) !== 0)
                if(substr(strtolower($item['blq_tipoequipo']), 0, strlen($modelo)) !== strtolower($modelo))
                    $modelo = 'OTROS';
            }
            
            // if(strpos(strtolower($item['blq_tipoequipo']), strtolower($modelo) ) === 0 || $item['blq_tipoequipo'] == 'TODOS')
            if(substr(strtolower($item['blq_tipoequipo']), 0, strlen($modelo)) === strtolower($modelo) || $item['blq_tipoequipo'] == 'TODOS')
            {
                $t->newBlock('bloques');
                $t->assign('nombloque', $item['blq_nombre'] . '');

                $idbloque = $item['blq_id'] . '';

                //PREGUNTAS DEL BLOQUE
                $rsppreguntas = $encuesta->preguntas($idbloque);
                $rows2 = $rsppreguntas->fetch_all(MYSQLI_ASSOC);
                foreach($rows2 as $j => $item2)
                {
                    $t->newBlock('preguntas');
                    $jk++;
                    $t->assign('num', $jk);
                    $idpregunta = $item2['preg_id'];
                    $t->assign('pregunta', $item2['preg_nombre'] . '');

                    if (isset($resp['pregunta'][$idpregunta]))
                    {
                        if ($resp['pregunta'][$idpregunta] == 'SI')
                            $t->assign('si', '<img src="../public/build/images/circle_2.png" height="8" style="display: block; margin-left: auto; margin-right: auto">');
                        else
                            $t->assign('no', '<img src="../public/build/images/circle_2.png" height="8" style="display: block; margin-left: auto; margin-right: auto">');
                    }

                    if ($item2['preg_comentario'])
                    {
                        if (isset($resp['comentario'][$idpregunta]))
                            $t->assign('comentario', $resp['comentario'][$idpregunta] . '');
                    }
                }
            }
        }

        //print the result
        // $t->printToScreen();
        // exit();

        $style02 = '../public/build/css/form_visita_obra.css';

        require_once("../public/build/lib/mPdfSC/vendor/autoload.php");

        $mpdf = new \Mpdf\Mpdf($opcionesMPDF);

        // $mpdf->keep_table_proportions = true;

        $mpdf->SetHTMLHeader($headerPdf, 'O', true);
        $mpdf->SetHTMLFooter($footerPdf, 'O', true);

        // $mpdf->SetHTMLHeader('<img src="../public/build/images/fm-logo-negro.png" height="40" alt="" style="float: left; display: inline-block;"> <div style="text-align: right">Página {PAGENO} / {nb}<div>', 'O', true);
        // $mpdf->SetHTMLHeader('<img src="../production/informes/img/fm-logo-negro.png" height="40" alt="" style="float: left; display: inline-block;"> <div style="text-align: right">Página {PAGENO} / {nb}<div>', 'O', true);

        // $mpdf->SetDisplayMode('fullwidth');
        $mpdf->SetDisplayMode(100);
        // $mpdf->SetDisplayMode('fullpage');

        $mpdf->SetDisplayPreferences('/HideToolbar/CenterWindow/FitWindow');
        // $mpdf->SetDisplayPreferences('/HideMenubar/HideToolbar/CenterWindow/FitWindow');

        $mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list

        // Load a stylesheet
        $stylesheet02 = file_get_contents($style02);

        $html = $t->getOutputContent();

        $mpdf->WriteHTML($stylesheet02, 1); // The parameter 1 tells that this is css/style only and no body/html/text
        $mpdf->WriteHTML($html,2);

        // $PROYECTO_REPORTE_PDF = 'REPORTE_DEL_PROYECTO_' . $datos['codigoimp'] . '.pdf';
        $PROYECTO_REPORTE_PDF = 'VISITA_' . $idvisita . '_EQUIPO_' . $idascensor . '.pdf';

        $mpdf->Output($PROYECTO_REPORTE_PDF, \Mpdf\Output\Destination::INLINE);

        break;
}

