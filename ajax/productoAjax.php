<?php
session_start();

require_once '../modelos/Producto.php';


$producto =  new Producto();


$idproducto = isset($_POST["idproducto"]) ? $_POST["idproducto"] : "";
$nombre = strtoupper(isset($_POST["nombre"]) ? $_POST["nombre"] : "");
$descripcion = strtoupper(isset($_POST["descripcion"]) ? $_POST["descripcion"] : "");
$codigo = strtoupper(isset($_POST["codigo"]) ? $_POST["codigo"] : "");
$codigobarras = strtoupper(isset($_POST["codigobarras"]) ? $_POST["codigobarras"] : "");
$categoria= isset($_POST["categoria"]) ? $_POST["categoria"] : "";
$created_user= $_SESSION["iduser"];
$vigencia= isset($_POST["vigencia"]) ? $_POST["vigencia"] : 0;
$precio= isset($_POST["precio"]) ? $_POST["precio"] : 0;

switch($_GET["op"]){
    case "guardaryeditar":
        if(empty($idproducto)){
            $rspta = $producto->Insertar($nombre, $descripcion, $codigo, $codigobarras, $categoria, $created_user, $vigencia, $precio);
            echo $rspta;
        }else{
            $rspta = $producto->Editar($idproducto, $nombre, $descripcion, $codigo, $codigobarras, $categoria, $vigencia, $precio);
            echo $rspta;
        }
        break;
        
    case "listar":
        
        $rspta = $producto->Listar();
        
        $data = Array();
        while ($reg = $rspta->fetch_object()){
            
            $data[] = array(
                "0"=>'<button class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="Movimiento de Producto" onclick="modalbodega('.$reg->idproducto.')"><i class="fa fa-plus-circle"></i></button>'.
                     '<button class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Stock producto por bodega" onclick="stock('.$reg->idproducto.')"><i class="fa fa-dropbox"></i></button>',
                "1"=>$reg->codigo,
                "2"=>$reg->nombre,
                "3"=>$reg->nombCategoria,
                "4"=>$reg->stock,
                "5"=>($reg->vigencia)?'<span class="label bg-green">Activo</span>':'<span class="label bg-red">No activo</span>',
                "6"=>'<button class="btn btn-warning btn-xs" onclick="mostrar('.$reg->idproducto.')"><i class="fa fa-pencil"></i></button>'
            );
        }
        $results = array(
                "sEcho"=>1,
                "iTotalRecords"=>count($data),
                "iTotalDisplayRecords"=>count($data), 
                "aaData"=>$data
        );

        echo json_encode($results);
        
        break;
        
    case "listaVigentes":
        
        $rspta = $producto->ListarVigentes();
        
        $data = Array();
        while ($reg = $rspta->fetch_object()){
            $data[] = array(
                "idproducto"=>$reg->idproducto,
                "nombre"=>$reg->nombre,
                "codigo"=>$reg->codigo,
                "precio"=>$reg->precio,
                "categoria"=>$reg->categoria,
                "nombCategoria"=>$reg->nombCategoria,
                
            );
        }       

        echo json_encode($data);
        
        break;
    
    case "listaSearch":
        
        $texto = isset($_REQUEST['texto']) ? $_REQUEST['texto'] : "";
        
        $data = array();
        
        if($texto != "" && $bodega != 0){
           /*$rspta = $producto->ListarSearch($texto, $bodega);

            $data = Array();
            while ($reg = $rspta->fetch_object()){
                $data[] = array(
                    "idproducto"=>$reg->idproducto,
                    "nombre"=>$reg->nombre,
                    "codigo"=>$reg->codigo,
                    "precio"=>$reg->precio,
                    "categoria"=>$reg->categoria,
                    "nombCategoria"=>$reg->nombCategoria,
                    "bodega"=>$reg->bodega,
                    "nombBodega"=>$reg->nombBodega,

                );
            }*/
            $rspta = $producto->ListarSearch($texto);
            while($reg = $rspta->fetch_object()){
                echo '<option value='.$reg->idproducto.' ondblclick="addProducto(this);">'.$reg->nombre.'</option>';
            }
        }
        break;
    
    case "mostrar":
        $rspta= $producto->mostrar($idproducto);
        echo json_encode($rspta);
        break;
    
    case "ver":
        $bodega = isset($_POST['bodega']) ? $_POST['bodega'] : 0;
        
        $rspta= $producto->ver($idproducto, $bodega);
        echo json_encode($rspta);
        break;
    
    case 'existe':
        $rspta = $producto->existe($codigo);
        echo json_encode($rspta);
        break;
    
    case "selectproductos":
        $rspta = $producto->ListarVigentes();
        echo '<option value="" selected disabled>Seleccione producto</option>';
        while($reg = $rspta->fetch_object()){
            if($reg->idproducto == $idproducto){
                echo '<option value='.$reg->idproducto.' selected="selected">'.$reg->nombre.'</option>';
            }else{
                echo '<option value='.$reg->idproducto.'>'.$reg->nombre.'</option>';
            }
        }
        break;
        
    case "selectproductoscategoria":
        $rspta = $producto->selectproducto($categoria);
        echo '<option value="" selected disabled>Seleccione producto</option>';
        while($reg = $rspta->fetch_object()){
            if($reg->idproducto == $idproducto){
                echo '<option value='.$reg->idproducto.' selected="selected">'.$reg->nombre.'</option>';
            }else{
                echo '<option value='.$reg->idproducto.'>'.$reg->nombre.'</option>';
            }
        }
        break;
    
}


