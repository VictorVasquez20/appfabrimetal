<?php

session_start();
require_once '../modelos/Documento_servicio.php';

$DS = new Documento_servicio();

$iddocumento = isset($_POST['iddocumento']) ? $_POST['iddocumento'] : "";
$nombre = isset($_POST['nombre']) ? $_POST['nombre'] : "";
$archivo = isset($_POST['archivo']) ? $_POST['archivo'] : "";
$idservicio = isset($_POST['idservicio1']) ? $_POST['idservicio1'] : "";


switch ($_GET["op"]) {
    case 'guardaryeditar':
        if (!$iddocumento) {
            $counter = 0;
            foreach ($_FILES['docto']['tmp_name'] as $key => $tmp_name) {

                if ($_FILES['docto']['name'][$key]) {
                    $ext = explode(".", $_FILES['docto']['name'][$key]);
                    if ($_FILES['docto']['type'][$key] == "application/pdf" || $ext[1] == 'pdf' || $ext[1] == 'PDF') {
                        //var_dump($ext[1]);
                        if ($_FILES["docto"]["size"][$key] <= 10000000) {
                            $link = "../files/servicio"; //round(microtime(true)) . "." . end($ext);
                            $archivo = $counter . round(microtime(true)) . "." . end($ext);
                            if (!file_exists($link)) {
                                mkdir($link, 0777, true);
                            }

                            if (move_uploaded_file($_FILES['docto']['tmp_name'][$key], $link . '/' . $archivo)) {
                                $nombre = $_POST["nombfile"][$key];
                                $rspta = $DS->Insertar($nombre, $archivo, $idservicio);
                                if ($rspta != 1) {
                                    $rspta = "Error al insertar archivo. " . $rspta;
                                    echo $rspta;
                                    break;
                                }
                            } else {
                                $rspta = "Error al cargar el archivo al servidor";
                                    echo $rspta;
                                    break;
                            }
                        } else {
                            $rspta = "el tamaño del archivo no puede ser mayor a 3mb.";
                            echo $rspta;
                            break;
                        }
                    } else {
                        $rspta = "El archivo debe ser pdf";
                        echo $rspta;
                        break;
                    }
                }
                $counter += 1;
            }

            echo $rspta;
            break;
        } else {
            $rspta = $DS->Editar($iddocumento, $nombre, $archivo, $idservicio);
            echo $rspta;
            break;
        }
        break;

    case 'selectArchivos':

        $rspta = $DS->Listar($idservicio);
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "iddocumento" => $reg->iddocumento,
                "nombre" => $reg->nombre,
                "archivo" => $reg->archivo,
                "idservicio" => $reg->idservicio
            );
        }
        echo json_encode($data);
        break;
}