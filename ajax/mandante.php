<?php
session_start();

require_once '../modelos/Mandante.php';
require_once '../modelos/ContactoMandante.php';

$mandante = new Mandante();
$contactomandante = new ContactoMandante();

$idmandante = isset($_POST['idmandante']) ? $_POST['idmandante'] : 0;
$razon_social = isset($_POST['razon_social']) ? strtoupper($_POST['razon_social']) : 0;
$rut = isset($_POST['rut']) ? $_POST['rut'] : 0;
$calle = isset($_POST['calle']) ? strtoupper($_POST['calle']) : 0;
$numero = isset($_POST['numero']) ? $_POST['numero'] : 0;
$oficina = isset($_POST['oficina']) ? $_POST['oficina'] : 0;
$idregion = isset($_POST['idregion']) ? $_POST['idregion'] : 0;
$idcomuna = isset($_POST['idcomun']) ? $_POST['idcomun'] : 0;
$created_user = isset($_POST['created_user']) ? $_POST['created_user'] : 0;
$condicion = isset($_POST['condicion']) ? $_POST['condicion'] : 0;


switch($_GET['op']){
    case 'guardaryeditar':
        $created_user = $_SESSION['iduser'];
        if(!$idmandante){
            $rspta = $mandante->Insertar($razon_social, $rut, $calle, $numero, $oficina, $idregion, $idcomuna, $created_user, $condicion);
            echo $rspta;
        }else{
            $rspta = $mandante->Editar($idmandante, $razon_social, $rut, $calle, $numero, $oficina, $idregion, $idcomuna, $condicion);
            echo $rspta ? "Mandante editado" : "Mandante No editado";
        }
        break;
    case 'guardar':
        $created_user = $_SESSION['iduser'];
        $razon_social = isset($_POST["razon"]) ? strtoupper(limpiarCadena($_POST["razon"])) : "";
        $rut = isset($_POST["rut"]) ? limpiarCadena($_POST["rut"]) : "";
        $calle = isset($_POST["callecli"]) ? strtoupper(limpiarCadena($_POST["callecli"])) : "";
        $numero = isset($_POST["numerocli"]) ? limpiarCadena($_POST["numerocli"]) : "";
        $oficina = isset($_POST["oficinacli"]) ? limpiarCadena($_POST["oficinacli"]) : "";
        $idregion = isset($_POST["idregionescli"]) ? limpiarCadena($_POST["idregionescli"]) : "";
        $idcomuna = isset($_POST["idcomunascli"]) ? limpiarCadena($_POST["idcomunascli"]) : "";
        $nrep = isset($_POST['nrep']) ? $_POST['nrep'] : 0;
        $condicion = 1;
        
        $rspta = $mandante->Insertar($razon_social, $rut, $calle, $numero, $oficina, $idregion, $idcomuna, $created_user, $condicion);
        
        if (isset($rspta)) {
            if (intval($nrep) > 0) {
                for ($o = 0; $o < intval($nrep); $o++) {
                    $nreg = 1;
                    $respcon = $contactomandante->Insertar($rspta, strtoupper($_POST['nombre_rep'.$o.'']), $_POST['rut_rep'.$o.''], strtoupper($_POST['email_rep'.$o.''] == "" ? 'N/S': $_POST['email_rep'.$o.'']), $_POST['fono_rep'.$o.''] == "" ? 'N/S': $_POST['fono_rep'.$o.'']);
                    $respcon? $nreg++:"";                   
                }
                if($nreg == intval($nrep)){
                    echo $rspta;
                }else{
                    echo "0";
                }
            } else {
                echo $rspta;
            }
        } else {
            echo "0";
        }
        break;
    
    case 'selectmandante':
        $rspta = $mandante->Listar();
        echo '<option value="" selected disabled>Seleccione Mandante</option>';
        while($reg = $rspta->fetch_object()){
            echo '<option value='.$reg->idmandante.'>'.$reg->razon_social.' / '.$reg->rut.'</option>';
        }
        break;
        
    case 'mostrar':
        $rspta = $mandante->Mostrar($idmandante);
        echo json_encode($rspta);
        break;
    
    case 'listarContacto':
        $rspta = $contactomandante->Listar($idmandante);
        $data = Array();
        
        while ($reg = $rspta->fetch_object()) {
            
            $data[] = array(
                "idmandante" => $reg->idmandante,
                "nombre" => $reg->nombre,
                "rut" => $reg->rut,
                "email" => $reg->email,
                "telefono" => $reg->telefono);
            
        }
        echo json_encode($data);
        break;
}
