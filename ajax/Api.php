<?php

//función validando todos los parametros disponibles
//pasaremos los parámetros requeridos a esta función

function ParametrosDisponiblesPost($params) {
    //Suponemos que todos los parametros vienene disponibles
    $disponible = true;
    $sinparams = "";

    foreach ($params as $param) {
        if (!isset($_POST[$param]) || strlen($_POST[$param]) <= 0) {
            $disponible = false;
            $sinparams = $sinparams . ", " . $param;
        }
    }

    //si faltan parametros
    if (!$disponible) {
        $response = array();
        $response['error'] = true;
        $response['message'] = 'Parametro' . substr($sinparams, 1, strlen($sinparams)) . ' vacio';

        //error de visualización
        echo json_encode($response);

        //detener la ejecición adicional
        die();
    }
}

function ParametrosDisponiblesGet($params) {
    //Suponemos que todos los parametros vienene disponibles
    $disponible = true;
    $sinparams = "";

    foreach ($params as $param) {
        if (!isset($_GET[$param]) || strlen($_GET[$param]) <= 0) {
            $disponible = false;
            $sinparams = $sinparams . ", " . $param;
        }
    }

    //si faltan parametros
    if (!$disponible) {
        $response = array();
        $response['error'] = true;
        $response['message'] = 'Parametro' . substr($sinparams, 1, strlen($sinparams)) . ' vacio';

        //error de visualización
        echo json_encode($response);

        //detener la ejecición adicional
        die();
    }
}

//una matriza para mostrar las respuestas de nuestra api
$response = array();


if (isset($_GET['apicall'])) {

    //Aqui iran todos los llamados de nuestra api
    switch ($_GET['apicall']) {

        case 'LeerServicios':
            require_once "../modelos/Servicio.php";
            $servicio = new Servicio();
            $rspta = $servicio->LGSESup('13.291.599-7');
            if (isset($rspta) && !empty($rspta)) {
                $data = Array();
                while ($reg = $rspta->fetch_object()) {
                    array_push($data, $reg);
                }
                $response['error'] = false;
                $response['message'] = 'solicitud completada correctamente';
                $response['contenido'] = $data;
            } else {
                $response['error'] = true;
                $response['message'] = 'No es posible realizar esta peticion';
            }
            break;

        case 'LeerServiciosRut':
            require_once "../modelos/Servicio.php";
            ParametrosDisponiblesGet(array('rut'));
            if (isset($_GET['rut']) && !empty($_GET['rut'])) {
                $servicio = new Servicio();
                $rspta = $servicio->LGSESup($_GET['rut']);
                $data = Array();

                while ($reg = $rspta->fetch_object()) {

                    array_push($data, $reg);
                }

                $response['error'] = false;
                $response['message'] = 'solicitud completada correctamente';
                $response['contenido'] = $data;
            } else {
                $response['error'] = true;
                $response['message'] = 'No es posible realizar esta peticion';
            }

            break;


        case 'IniciarSesionGuias':
            require_once "../modelos/Usuario.php";
            ParametrosDisponiblesPost(array('email', 'password'));
            $usuario = new Usuario();
            if (isset($_POST['email']) && !empty($_POST['email']) && isset($_POST['password']) && !empty($_POST['password'])) {
                $username = $_POST['email'];
                $password = $_POST['password'];
                $password_hash = hash("SHA256", $password);
                $rspta = $usuario->verificar($username, $password_hash);
                $fecth = $rspta->fetch_object();
                if (empty($fecth)) {
                    $response['error'] = true;
                    $response['message'] = 'Error al iniciar sesion';
                } else {
                    $response['error'] = false;
                    $response['message'] = 'solicitud completada correctamente';
                    $response['contenido'] = $fecth;
                }
            } else {
                $response['error'] = true;
                $response['message'] = 'No es posible realizar esta peticion';
            }

            break;


        case 'LeerCentrosCosto':
            require_once "../modelos/CentroCosto.php";

            //Objeto del modelo
            $centrocosto = new CentroCosto();
            $rspta = $centrocosto->selectcentrocostoEX();
            if (isset($rspta) && !empty($rspta)) {
                $data = Array();
                while ($reg = $rspta->fetch_object()) {
                    array_push($data, $reg);
                }
                $response['error'] = false;
                $response['message'] = 'solicitud completada correctamente';
                $response['contenido'] = $data;
            } else {
                $response['error'] = true;
                $response['message'] = 'No es posible realizar esta peticion';
            }
            break;

        case 'LeerEdificiosSupervisor':
            //Requerimos el modelo
            require_once "../modelos/Edificio.php";

            //Objeto del modelo
            $edificio = new Edificio();
            $rspta = $edificio->ListarEdificiosSup();
            if (isset($rspta) && !empty($rspta)) {
                $data = Array();
                while ($reg = $rspta->fetch_object()) {
                    array_push($data, $reg);
                }
                $response['error'] = false;
                $response['message'] = 'solicitud completada correctamente';
                $response['contenido'] = $data;
            } else {
                $response['error'] = true;
                $response['message'] = 'No es posible realizar esta peticion';
            }
            break;

        case 'LeerServiciosEdificio':
            //Requerimos el modelo
            require_once "../modelos/Servicio.php";


            //VERIFICAMOS PARAMETROS
            ParametrosDisponiblesGet(array('idedificio'));

            //VERIFICAMOS NUEVAMENTE PARAMETROS PARA RESPUESTA AL CLIENTE
            if (isset($_GET['idedificio']) && !empty($_GET['idedificio'])) {

                //Objeto del modelo
                $servicios = new Servicio();
                $rspta = $servicios->ListarGuiasEdificio($_GET['idedificio']);

                //EVALUAMOS RESPUESTA DE LA BD
                if (isset($rspta) && !empty($rspta)) {
                    $data = Array();
                    while ($reg = $rspta->fetch_object()) {
                        array_push($data, $reg);
                    }
                    $response['error'] = false;
                    $response['message'] = 'solicitud completada correctamente';
                    $response['contenido'] = $data;
                } else {
                    $response['error'] = true;
                    $response['message'] = 'No es posible realizar esta peticion';
                }
            } else {
                $response['error'] = true;
                $response['message'] = 'No es posible realizar esta peticion, error de parametros';
            }
            break;
    }
} else {
    //Valida que venga una llamada correcta a la APi
    $response['error'] = true;
    $response['message'] = 'Llamda incorrecta a la API';
}

echo json_encode($response);
?>