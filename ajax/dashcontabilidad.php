<?php

require_once '../modelos/DashContabilidad.php';

$dcont = new DashContabilidad();

switch ($_GET["op"]){
    case 'indicadores':
        $rspta = $dcont->getResumenGSE();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => $reg->nombre,
                "1" => $reg->idtservicio,
                "2" => $reg->gsemes,
                "3" => $reg->gsefact
            );
        }
        echo json_encode($data);
        break;
        
        
    case 'grafico':
        $rspta = $dcont->getGraficoGSE();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => $reg->mes,
                "1" => $reg->mesname,
                "2" => $reg->guiames,
                "3" => $reg->guiamesfact,
                "4" => $reg->idtservicio
            );
        }
        echo json_encode($data);
        break;
        
        
    case 'graficoemergencia':
        $rspta = $dcont->graficoGSEemergencias();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => $reg->mes,
                "1" => $reg->mesname,
                "2" => $reg->guiames,
                "3" => $reg->guiamesfact,
                "4" => $reg->idtservicio,
                "5" => $reg->estadofin
            );
        }
        echo json_encode($data);
        break;
        
    /** INDICADORES CON FITRO DE FECHA **/
    case 'indicadoresfiltro':
        $fecha = isset($_GET['fecha']) ? $_GET['fecha'] : date('Y-m-d');
        
        $rspta = $dcont->getResumenGSEfiltro($fecha);
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => $reg->nombre,
                "1" => $reg->idtservicio,
                "2" => $reg->gsemes,
                "3" => $reg->gsefact
            );
        }
        echo json_encode($data);
        break;
    case 'gseemergenciafiltro':
        $fecha = isset($_GET['fecha']) ? $_GET['fecha'] : date('Y-m-d');
        $rspta = $dcont->GSEemergencias($fecha);
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => $reg->mes,
                "1" => $reg->mesname,
                "2" => $reg->guiames,
                "3" => $reg->guiamesfact,
                "4" => $reg->idtservicio,
                "5" => $reg->estadofin
            );
        }
        echo json_encode($data);
        break;
}
