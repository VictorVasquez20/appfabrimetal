<?php

require_once '../modelos/Marca_izaje.php';

$marca = new Marca_izaje();

$idmarca = isset($_POST['idmarca']) ? $_POST['idmarca'] : "";
$nombre = isset($_POST['nombre']) ? $_POST['nombre'] : "";

switch ($_GET['op']) {
    case 'guardaryeditar':
        if (!$idmarca) {
            $rspta = $marca->Insrtar($nombre);
        } else {
            $rspta = $marca->Editar($idmarca, $nombre);
        }
        echo $rspta;
        break;

    case 'listar':
        $rspta = $marca->Listar();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => '<button class="btn btn-info btn-xs" onclick="mostrar(' . $reg->idmarca . ')"><i class="fa fa-pencil"></i></button>',
                "1" => $reg->nombre
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;

    case 'mostrar':
        $rspta = $marca->Mostrar($idmarca);
        echo json_encode($rspta);
        break;

    case 'selectmarca':
        $rspta = $marca->Listar();
        echo '<option value="" selected disabled>SELECCIONE MARCA</option>';
        while ($reg = $rspta->fetch_object()) {
            echo '<option value=' . $reg->idmarca . '>' . $reg->nombre . '</option>';
        }
        break;
}
