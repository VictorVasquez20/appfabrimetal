<?php
session_start();

require_once '../modelos/ContactoInfo.php';

$contacto = new ContactoInfo();

$idcontacto = isset($_POST['idcontacto']) ? $_POST['idcontacto'] : 0;
$nombre = isset($_POST['nombre']) ? $_POST['nombre'] : "";
$correo = isset($_POST['correo']) ? $_POST['correo'] : "";
$idareainfo = isset($_POST['idareainfo']) ? $_POST['idareainfo'] : 0;


switch ($_GET["op"]) {
    
    case 'guardaryeditar':
        if(!$idcontacto){
            $rspta = $contacto->insertar($nombre, $correo, $idareainfo);
            echo $rspta ? "Contacto guardado" : "Contacto No guardado";
        }else{
            $rspta = $contacto->editar($idcontacto, $nombre, $correo, $idareainfo);
            echo $rspta ? "Contacto Editado" : "Contacto No Editado";
        }
        
        break;
        
    case 'mostrar' :
        $rspta = $contacto->mostrar($idcontacto);
        echo json_encode($rspta);
        break;
    
    case 'listar':
        $rspta = $contacto->listar();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => '<button class="btn btn-info btn-xs" onclick="editar(' . $reg->idcontacto . ')"><i class="fa fa-pencil"></i></button>',
                "1" => $reg->nombre,
                "2" => $reg->correo,
                "3" => $reg->nombArea
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );
        echo json_encode($results);
        break;
}