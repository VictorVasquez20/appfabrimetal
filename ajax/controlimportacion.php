<?php
session_start();

require_once '../modelos/ControlImportacion.php';

$conimp = new ControlImportacion();


switch($_GET['op']){
    case 'listaranios':
    $rspta = $conimp->listarAnios();
    echo '<option value="" selected disabled>Seleccione Opción</option>';
    while($reg = $rspta->fetch_object()){
        echo '<option value='.$reg->anio.'>'.$reg->anio.'</option>';
    }
    break;
    case 'guardar':
        //echo '<pre>';print_r($_POST);echo '</pre>';die;
        $ctli_iduser = $_SESSION['iduser'];
        $ctli_numref = isset($_POST["ctli_numref"]) ? limpiarCadena($_POST["ctli_numref"]) : "";
        $ctli_fechapedido = isset($_POST["ctli_fechapedido"]) ? $_POST["ctli_fechapedido"] : "";
        $ctli_codkm = isset($_POST["ctli_codkm"]) ? limpiarCadena($_POST["ctli_codkm"]) : "";
        $ctli_descripcion = isset($_POST["ctli_descripcion"]) ? limpiarCadena($_POST["ctli_descripcion"]) : "";
        $ctli_codst = isset($_POST["ctli_codst"]) ? limpiarCadena($_POST["ctli_codst"]) : "";
        $ctli_st_fecha = isset($_POST["ctli_codst_fecha"]) ? limpiarCadena($_POST["ctli_codst_fecha"]) : NULL;
        $ctli_cantidad = isset($_POST["ctli_cantidad"]) ? $_POST["ctli_cantidad"] : "";
        $ctli_numpres = isset($_POST["ctli_numpres"]) ? limpiarCadena($_POST["ctli_numpres"]) : "";
        $ctli_solicitante = isset($_POST["ctli_solicitante"]) ? limpiarCadena($_POST["ctli_solicitante"]) : "";
        $ctli_observacion = isset($_POST["ctli_observacion"]) ? limpiarCadena($_POST["ctli_observacion"]) : "";

        $id = $conimp->InsertarImportacion($ctli_iduser,$ctli_numref,$ctli_fechapedido,$ctli_codkm,$ctli_descripcion,$ctli_codst,$ctli_cantidad,$ctli_numpres,$ctli_solicitante,$ctli_observacion,$ctli_st_fecha);

        if(!empty($id)){
            echo $id;
        }else{
            echo "NO PUDO SER REGISTRADO";
        }
    break;

    case 'mostraritem':
        $rspta = $conimp->Mostrar($_GET['codigo']);
        $rspta['ctli_codst_fecha'] = ((!empty($rspta['ctli_codst_fecha'])) ? date('Y-m-d',strtotime($rspta['ctli_codst_fecha'])) : '' );
        echo json_encode($rspta);
    break;

    case 'guardareditar':
        //echo '<pre>';print_r($_POST);echo '</pre>';die;
        $ctli_id = isset($_POST["id"]) ? $_POST["id"] : "";
        $ctli_iduser = $_SESSION['iduser'];
        $ctli_numref = isset($_POST["ctli_numref"]) ? limpiarCadena($_POST["ctli_numref"]) : "";
        $ctli_fechapedido = isset($_POST["ctli_fechapedido"]) ? $_POST["ctli_fechapedido"] : "";
        $ctli_codkm = isset($_POST["ctli_codkm"]) ? limpiarCadena($_POST["ctli_codkm"]) : "";
        $ctli_descripcion = isset($_POST["ctli_descripcion"]) ? limpiarCadena($_POST["ctli_descripcion"]) : "";
        $ctli_codst = isset($_POST["ctli_codst"]) ? limpiarCadena($_POST["ctli_codst"]) : "";
        $ctli_st_fecha = isset($_POST["ctli_codst_fecha"]) ? limpiarCadena($_POST["ctli_codst_fecha"]) : NULL;
        $ctli_cantidad = isset($_POST["ctli_cantidad"]) ? $_POST["ctli_cantidad"] : "";
        $ctli_numpres = isset($_POST["ctli_numpres"]) ? limpiarCadena($_POST["ctli_numpres"]) : "";
        $ctli_solicitante = isset($_POST["ctli_solicitante"]) ? limpiarCadena($_POST["ctli_solicitante"]) : "";
        $ctli_observacion = isset($_POST["ctli_observacion"]) ? limpiarCadena($_POST["ctli_observacion"]) : "";

        if(!empty($ctli_id)){
            $id = $conimp->EditarImportacion($ctli_id, $ctli_iduser,$ctli_numref,$ctli_fechapedido,$ctli_codkm,$ctli_descripcion,$ctli_codst,$ctli_cantidad,$ctli_numpres,$ctli_solicitante,$ctli_observacion,$ctli_st_fecha);
            echo json_encode(array('status'=>'success'));
        }else{
            echo json_encode(array('status'=>'fail','msg'=>'NO PUDO SER EDITADO'));
        }
    break;

    case 'listar':

        $mes = isset($_GET["mes"]) ? limpiarCadena($_GET["mes"]) : "";
        $anio = isset($_GET["anio"]) ? limpiarCadena($_GET["anio"]) : "";
        $rspta = $conimp->ListarAgrupado($mes,$anio);
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            
            $rspta99 = $conimp->ListarporCodigo($reg->ctli_numref);
            $data99 = Array();
            $boton = "listo";
            while ($reg99 = $rspta99->fetch_object()) {
                if ($reg99->ctli_status == 'pendiente'){
                    $boton = "pendiente";
                }                
            }

            if ($boton == "pendiente"){
                $rspta88 = $conimp->ListarporCodigo($reg->ctli_numref);
                $data88 = Array();
                while ($reg88 = $rspta88->fetch_object()) {
                    if ($reg88->ctli_status != 'pendiente'){
                        $boton = "parcial";
                    }                
                }
            }
            

            if($boton == "pendiente"){
                $op = '<button class="btn btn-warning btn-xs" onclick="mostrar(\'' . $reg->ctli_numref . '\',\'' . date('Y-m-d',strtotime($reg->ctli_fechapedido)) . '\')" data-toggle="tooltip" data-placement="top" data-original-title="Editar" title="Editar (Pendiente)"><i class="fa fa-pencil"></i></button>';
            }
            if($boton == "parcial"){
                $op = '<button class="btn btn-primary btn-xs" onclick="mostrar(\'' . $reg->ctli_numref . '\',\'' . date('Y-m-d',strtotime($reg->ctli_fechapedido)) . '\')" data-toggle="tooltip" data-placement="top" data-original-title="Editar" title="Editar (Recibido parcial)"><i class="fa fa-pencil"></i></button>';
            }
            if($boton == "listo"){
                $op = '<button class="btn btn-success btn-xs" onclick="mostrar(\'' . $reg->ctli_numref . '\',\'' . date('Y-m-d',strtotime($reg->ctli_fechapedido)) . '\')" data-toggle="tooltip" data-placement="top" data-original-title="Editar" title="Editar (Recibido)"><i class="fa fa-pencil"></i></button>';
            }
            /*else{
                $op = '<button class="btn btn-success btn-xs" onclick="mostrar(\'' . $reg->ctli_numref . '\',\'' . date('Y-m-d',strtotime($reg->ctli_fechapedido)) . '\')" data-toggle="tooltip" data-placement="top" data-original-title="Editar"><i class="fa fa-pencil"></i></button>';
            }  */         
            
            $data[] = array(
                "0" => $op,
                "1" => $reg->ctli_numref,
                "2" => $reg->ctli_fechapedido!= NULL ? date("m/d/Y", strtotime($reg->ctli_fechapedido)) : "",
                "3" => $reg->grupo
             );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );
        echo json_encode($results);
    break;

    case 'listarcsv':
        $mes = isset($_GET["mes"]) ? limpiarCadena($_GET["mes"]) : "";
        $anio = isset($_GET["anio"]) ? limpiarCadena($_GET["anio"]) : "";
        $rspta = $conimp->Listar($mes,$anio);
        $data = Array();
        $num = 0;
        $comencount = 0;
        while($p = $rspta->fetch_array()) {
            $prod[$num]['ctli_codst'] = $p['ctli_codst'];
            $prod[$num]['ctli_codst_fecha'] = $p['ctli_codst_fecha'] != NULL ? date("d/m/Y", strtotime($p['ctli_codst_fecha'] )) : "";
            $prod[$num]['ctli_numref'] = $p['ctli_numref'];
            $prod[$num]['ctli_codkm'] = $p['ctli_codkm'];
            $prod[$num]['ctli_descripcion'] = $p['ctli_descripcion'];
            $prod[$num]['ctli_cantidad'] = $p['ctli_cantidad'];
            $prod[$num]['ctli_numpres'] = $p['ctli_numpres'];
            $prod[$num]['ctli_solicitante'] = $p['ctli_solicitante'];
            $prod[$num]['ctli_observacion'] = $p['ctli_observacion'];
            $prod[$num]['ctli_fechapedido'] = date("d/m/Y", strtotime($p['ctli_fechapedido']));
            $prod[$num]['ctli_fechaestdesp'] = $p['ctli_fechaestdesp'] != NULL ? date("d/m/Y", strtotime($p['ctli_fechaestdesp'] )) : "";
            $prod[$num]['ctli_fechadesp'] = $p['ctli_fechadesp'] != NULL ? date("d/m/Y", strtotime($p['ctli_fechadesp'] )) : "";
            $prod[$num]['ctli_fechaestllegada'] = $p['ctli_fechaestllegada'] != NULL ? date("d/m/Y", strtotime($p['ctli_fechaestllegada'] )) : "";
            $prod[$num]['ctli_fechallegada'] = $p['ctli_fechallegada'] != NULL ? date("d/m/Y", strtotime($p['ctli_fechallegada'] )) : "";
            $comentarios = $conimp->BuscarComentarios($p['ctli_id']);
            while($c = $comentarios->fetch_array()) {
                $prod[$num]['comnetario_'.$comencount] = $c['usuario'] . ': ' . $c['cicm_comentario'];
                $comencount++;
            }
            $num++;
        }
        
        $delimiter = ';';
        $output = fopen("php://output",'w') or die("Can't open php://output");
        header('Content-Encoding: UTF-8');
        //header("Content-Type:application/csv; charset=UTF-8"); 
        header("Content-Disposition:attachment;filename=pressurecsv.csv"); 
        fputcsv($output, array('ST','ST Fecha','REF.:','KM','Descripcion','CANTIDAD','Numero de presupuesto','Solicitante','Observacion','Fecha de pedido','Fecha estimada de despacho','Fecha despacho','Fecha estimada de llegada','Fecha de llegada'),$delimiter);
        foreach($prod as $product) {
            fputcsv($output, $product, $delimiter);
        }
        fclose($output) or die("Can't close php://output");
    break;

    case 'listarporcodigo':
        $rspta = $conimp->ListarporCodigo($_GET['codigo']);
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $botones = '<button class="btn btn-success btn-xs" type="button" onclick="mostrarcomentarios(' . $reg->ctli_id . ', \'' . $reg->ctli_codkm . '\')"><i class="fa fa-commenting"></i></button>';

            // if($_SESSION['administrador'] == 1) {
                $botones .= '<button class="btn btn-primary btn-xs" type="button" onclick="editaritem(' . $reg->ctli_id . ')"><i class="fa fa-pencil"></i></button>';
            // }

            $botones .= '<button class="btn btn-info btn-xs" type="button" onclick="mostrarfechas(' . $reg->ctli_id . ', \'' . $reg->ctli_codkm . '\')"><i class="fa fa-calendar"></i></button>';

            // if($_SESSION['administrador'] == 1) {
                if ($reg->ctli_status == 'pendiente')
                    $botones .= '<button class="btn btn-danger btn-xs" type="button" onclick="cambiarstatus(' . $reg->ctli_id . ')" title="Status: ' . $reg->ctli_status . '" style="background-color: #FF0000; border-color: #FF0000"><i class="fa fa-times"></i></button>';
                else
                    $botones .= '<button class="btn btn-info btn-xs" type="button" title="Status: ' . $reg->ctli_status . '" style="background-color: #99BE0C; border-color: #99BE0C"><i class="fa fa-check"></i></button>';
            // }
            
            $data[] = array(
                "0" => $botones,
                "1" => $reg->ctli_codst,
                "2" => (!empty($reg->ctli_codst_fecha) ? date('d-m-Y',strtotime($reg->ctli_codst_fecha)) : ''),
                "3" => $reg->ctli_codkm,
                "4" => $reg->ctli_descripcion,
                "5" => $reg->ctli_cantidad,
                "6" => $reg->ctli_numpres,
                "7" => $reg->ctli_solicitante,
                "8" => $reg->ctli_observacion,
                "9" => (($reg->ctli_status == 'pendiente') ? '<span class="label label-warning">'.$reg->ctli_status.'</span>' : '<span class="label label-success">'.$reg->ctli_status.'</span>'),
             );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );
        echo json_encode($results);
    break;

    case 'listarcomentarios':
        $rspta = $conimp->ListarComentarios($_GET['codigo']);
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => $reg->cicm_created_time,
                "1" => $reg->usuario,
                "2" => $reg->cicm_comentario,
             );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );
        echo json_encode($results);
    break;

    case 'listarpantalla':
        $estado = isset($_POST["estado"]) ? limpiarCadena($_POST["estado"]) : "%";
        $anio = !empty($_POST["anio"]) ? limpiarCadena($_POST["anio"]) : "";
        $mes = !empty($_POST["mes"]) ? limpiarCadena($_POST["mes"]) : "";
        $rspta = $conimp->ListarPantalla($anio, $mes, $estado);
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $botones = '<button class="btn btn-success btn-xs" type="button" onclick="mostrarcomentarios(' . $reg->ctli_id . ', \'' . $reg->ctli_codkm . '\')"><i class="fa fa-commenting"></i></button>';

            $data[] = array(
                "0" => $botones,
                "1" => $reg->ctli_codst,
                "2" => (!empty($reg->ctli_codst_fecha) ? date('d-m-Y',strtotime($reg->ctli_codst_fecha)) : ''),
                "3" => $reg->ctli_numref,
                "4" => (($reg->ctli_fechapedido) ? date("d-m-Y", strtotime($reg->ctli_fechapedido)) : ''),
                "5" => $reg->ctli_codkm,
                "6" => $reg->ctli_descripcion,
                "7" => $reg->ctli_cantidad,
                "8" => $reg->ctli_numpres,
                "9" => $reg->ctli_solicitante,
                "10" => $reg->ctli_observacion,
                "11" => (($reg->ctli_fechaestdesp) ? date("d-m-Y", strtotime($reg->ctli_fechaestdesp)) : ''),
                "12" => (($reg->ctli_fechadesp) ? date("d-m-Y", strtotime($reg->ctli_fechadesp)) : ''),
                "13" => (($reg->ctli_fechaestllegada) ? date("d-m-Y", strtotime($reg->ctli_fechaestllegada)) : ''),
                "14" => (($reg->ctli_fechallegada) ? date("d-m-Y", strtotime($reg->ctli_fechallegada)) : ''),
                "15" => $reg->ctli_status,
             );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );
        echo json_encode($results);
    break;

    case 'guardarcomentario':
        $ctli_id = isset($_POST["idcom"]) ? limpiarCadena($_POST["idcom"]) : "";
        $ctcm_comentario = isset($_POST["comentario"]) ? limpiarCadena($_POST["comentario"]) : "";

        $id = $conimp->InsertarComentario($ctli_id,$ctcm_comentario);

        if(!empty($id)){
            echo $id;
        }else{
            echo "NO PUDO SER REGISTRADO";
        }
    break;

    case 'cambiarstatus':
        $ctli_id = isset($_POST["idcom"]) ? limpiarCadena($_POST["idcom"]) : "";

        $id = $conimp->CambiarStatus($ctli_id);

        if(!empty($id)){
            echo $id;
        }else{
            echo "NO PUDO SER REGISTRADO";
        }
    break;

    case 'mostrarfechas':
        $ctli_id = isset($_GET["codigo"]) ? limpiarCadena($_GET["codigo"]) : "";
        $rspta = $conimp->Mostrar($ctli_id);
        $rspta['ctli_fechaestdesp'] = ((!empty($rspta['ctli_fechaestdesp'])) ? date('Y-m-d',strtotime($rspta['ctli_fechaestdesp'])) : '' );
        $rspta['ctli_fechadesp'] = ((!empty($rspta['ctli_fechadesp'])) ? date('Y-m-d',strtotime($rspta['ctli_fechadesp'])) : '' );
        $rspta['ctli_fechaestllegada'] = ((!empty($rspta['ctli_fechaestllegada'])) ? date('Y-m-d',strtotime($rspta['ctli_fechaestllegada'])) : '' );
        $rspta['ctli_fechallegada'] = ((!empty($rspta['ctli_fechallegada'])) ? date('Y-m-d',strtotime($rspta['ctli_fechallegada'])) : '' );
        echo json_encode($rspta);

    break;

    case 'guardarfechas':
        //echo '<pre>';print_r($_POST);echo '</pre>';die;
        $ctli_id = isset($_POST["idcom"]) ? limpiarCadena($_POST["idcom"]) : "";
        $ctli_fechaestdesp = isset($_POST["ctli_fechaestdesp"]) ? limpiarCadena($_POST["ctli_fechaestdesp"]) : NULL;
        $ctli_fechadesp = isset($_POST["ctli_fechadesp"]) ? limpiarCadena($_POST["ctli_fechadesp"]) : NULL;
        $ctli_fechaestllegada = isset($_POST["ctli_fechaestllegada"]) ? limpiarCadena($_POST["ctli_fechaestllegada"]) : NULL;
        $ctli_fechallegada = isset($_POST["ctli_fechallegada"]) ? limpiarCadena($_POST["ctli_fechallegada"]) : NULL;
        if(!empty($ctli_id)){
            $id = $conimp->InsertarFechas($ctli_id,$ctli_fechaestdesp,$ctli_fechadesp,$ctli_fechaestllegada,$ctli_fechallegada);
            echo json_encode(array('status'=>'success'));
        }else{
            echo json_encode(array('status'=>'fail','msg'=>'NO PUDO SER EDITADO'));
        }
    break;

    case 'selectaniopantalla':
        
                $rspta = $conimp->AnioPantalla();
                echo '<option value="" selected disabled>--seleccione--</option>';
                while($reg = $rspta->fetch_object()){
                        echo '<option value='.$reg->anio.'>'.$reg->anio.'</option>';
                }

    break;
    case 'selectmespantalla':
        $anio = isset($_GET["anio"]) ? limpiarCadena($_GET["anio"]) : "";
        $rspta = $conimp->MesPantalla($anio);
        echo '<option value="" disabled>--seleccione--</option>';
        echo '<option value="" selected >TODOS</option>';
        while($reg = $rspta->fetch_object()){
                echo '<option value='.$reg->mes.'>'.$reg->nombre_mes.'</option>';
        }
    break;
}