<?php

session_start();

require_once '../modelos/Categoria_Producto.php';

$catProd =  new Categoria_Producto();

$idcategoria = isset($_POST["idcategoria"]) ? $_POST["idcategoria"] : "";
$nombre = strtoupper(isset($_POST["nombre"]) ? $_POST["nombre"] : "");
$descripcion = strtoupper(isset($_POST["descripcion"]) ? $_POST["descripcion"] : "");
$vigencia = isset($_POST["vigencia"]) ? $_POST["vigencia"] : 0;

switch($_GET["op"]){
    case "guardaryeditar":
        if(empty($idcategoria)){
            $rspta = $catProd->Insertar($nombre, $descripcion, $vigencia);
            echo $rspta ? "Categoria Guardada" : "Categoria No Guardada";
        }else{
            $rspta = $catProd->Editar($idcategoria, $nombre, $descripcion, $vigencia);
            echo $rspta ? "Categoria Editada" : "Categoria No Editada";
        }
        break;
    case "listar":
        
        $rspta = $catProd->Listar();
        $data = Array();
        while ($reg = $rspta->fetch_object()){
            $data[] = array(
                "0"=>'<button class="btn btn-warning btn-xs" onclick="mostrar('.$reg->idcategoria.')"><i class="fa fa-pencil"></i></button>',
                "1"=>$reg->nombre,
                "2"=>$reg->descripcion,
                "3"=>($reg->vigencia)?'<span class="label bg-green">Activo</span>':'<span class="label bg-red">No activo</span>'
            );
        }
        $results = array(
                "sEcho"=>1,
                "iTotalRecords"=>count($data),
                "iTotalDisplayRecords"=>count($data), 
                "aaData"=>$data
        );

        echo json_encode($results);
        
        break;
    
    case "mostrar":
        $rspta= $catProd->mostrar($idcategoria);
        echo json_encode($rspta);
        break;
    
    case "selectcategoria":
        $rspta = $catProd->SelectCategoria();
        echo '<option value="" selected disabled>Seleccione categoria</option>';
        while($reg = $rspta->fetch_object()){
            if($reg->idcategoria == $idcategoria){
                echo '<option value='.$reg->idcategoria.' selected="selected">'.$reg->nombre.'</option>';
            }else{
                echo '<option value='.$reg->idcategoria.'>'.$reg->nombre.'</option>';
            }
        }
        break;
}

