<?php
session_start();

require_once '../modelos/FormaPago.php';

$fp = new FormaPago();

$idformapago = isset($_POST['idformapago']) ? $_POST['idformapago'] : 0;
$idtformapago = isset($_POST['idtformapago']) ? $_POST['idtformapago'] : 0; 
$idascensor = isset($_POST['idascensor']) ? $_POST['idascensor'] : 0; 
$idfactura = isset($_POST['idfactura']) ? $_POST['idfactura'] : 0; 
$descripcion = isset($_POST['descripcion']) ? strtoupper($_POST['descripcion']) : 0; 
$secuencia = isset($_POST['secuencia']) ? $_POST['secuencia'] : 0; 
$porcentaje = isset($_POST['porcentaje']) ? $_POST['porcentaje'] : 0; 
$facturado = isset($_POST['facturado']) ? $_POST['facturado'] : 0;

switch ($_GET['op']){
    case 'guardaryeditar':
        if(!$idformapago){
            $rspta = $fp->Insert($idtformapago, $idascensor, $descripcion, $porcentaje, $secuencia);
            echo $rspta;
        }else{
            $rspta = $fp->editar($idformapago, $idtformapago, $idascensor, $idfactura, $descripcion, $secuencia, $porcentaje, $facturado);
            echo $rspta;
        }
        break;
    
    case 'listarventaSN':
        $idventa = $_POST['idventa'];
        $rspta = $fp->listar($idventa, 2);
        $data = Array();
        $countSN = 0;
        
        while ($reg = $rspta->fetch_object()) {
           $hidden = "<input type='hidden' name='sn[]' id='sn_" . $countSN . "__idformapago' value='" .$reg->idformapago . "' />" .
                "<input type='hidden' name='sn[].idtformapago' id='sn_" . $countSN .  "__idtformapago' value='" .$reg->idtformapago . "' />" .
                "<input type='hidden' name='sn[].idascensor' id='sn_" . $countSN .  "__idascensor' value='" .$reg->idascensor . "' />" .
                "<input type='hidden' name='sn[].descripcion' id='sn_" . $countSN .  "__descripcion' value='" .$reg->descripcion . "' />" .
                "<input type='hidden' name='sn[].secuencia' id='sn_" . $countSN .  "__secuencia' value='" .$reg->secuencia . "' />" .
                "<input type='hidden' name='sn[].secuencia2' id='sn_" . $countSN .  "__secuencia2' value='" . ($countSN + 1) . "' />" .
                "<input type='hidden' name='sn[].porcetaje' id='sn_" . $countSN .  "__porcetaje' value='" .$reg->porcentaje . "' />" ;       
            
            $data[] = array(
                "0" => $hidden . ($reg->porcentaje * 100),
                "1" => $reg->descripcion,
                "2" => "<i class='fa fa-trash' onclick='delSN($idventa, 2, $reg->secuencia);'></i>",
                "3" => ($reg->porcentaje * 100));
                    
            $countSN += 1;
        }
        
        echo json_encode($data);
        break;
        
    case 'listarventaSI':
        $idventa = $_POST['idventa'];
        $rspta = $fp->listar($idventa, 1);
        $data = Array();
        $countSI = 0;
        while ($reg = $rspta->fetch_object()) {
           $hidden = "<input type='hidden' name='si[]' id='si_" . $countSI . "__idformapago' value='" .$reg->idformapago . "' />" .
                "<input type='hidden' name='si[].idtformapago' id='si_" . $countSI .  "__idtformapago' value='" .$reg->idtformapago . "' />" .
                "<input type='hidden' name='si[].idascensor' id='si_" . $countSI .  "__idascensor' value='" .$reg->idascensor . "' />" .
                "<input type='hidden' name='si[].descripcion' id='si_" . $countSI .  "__descripcion' value='" .$reg->descripcion . "' />" .
                "<input type='hidden' name='si[].secuencia' id='si_" . $countSI .  "__secuencia' value='" .$reg->secuencia . "' />" .
                "<input type='hidden' name='si[].secuencia2' id='si_" . $countSI .  "__secuencia2' value='" . ($countSI + 1) . "' />" .
                "<input type='hidden' name='si[].porcetaje' id='si_" . $countSI .  "__porcetaje' value='" .$reg->porcentaje . "' />" ;       
            
            $data[] = array(
                "0" => $hidden . ($reg->porcentaje * 100),
                "1" => $reg->descripcion,
                "2" => "<i class='fa fa-trash' onclick='delSI($idventa, 1, $reg->secuencia);'></i>",
                "3" => ($reg->porcentaje * 100));
            
            $countSI += 1;
        }
        
        echo json_encode($data);
        break;
        
    case 'listarelimina':
        $idventa = $_POST['idventa'];
        $rspta = $fp->listaElimina($idventa, $idtformapago, $secuencia);
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "id" => $reg->idformapago
            );
        }
        echo json_encode($data);
        break;
        
    case 'eliminar':
        $rspta = $fp->eliminar($idformapago);
        echo $rspta;
        break;
    case 'eliminarxascensor':
        $rspta = $fp->eliminarxascensor($idascensor);
        echo $rspta ? "Eliminado" : "NO eliminado";
        break;
    
    case 'editarSecuencia':
        $rspta = $fp->editarSecuencia($idformapago, $secuencia);
        echo $rspta;
        break;
}