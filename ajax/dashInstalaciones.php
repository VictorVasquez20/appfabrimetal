<?php

require_once '../modelos/DashInstalaciones.php';

$dins = new DashInstalaciones();

$idpm=isset($_POST["idpm"])?limpiarCadena($_POST["idpm"]):"";


switch($_GET['op']){
    
    case 'instalacionesxestado':
        
        $rspta = $dins->instalacionesXestado();
        
        $data = Array();
        
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "idestado" => $reg->idestadopro,
                "estado" => $reg->nombre,
                "cantidad" => $reg->num,
                "proyectos" => $reg->equipos
            );
        }
        
        echo json_encode($data);
        
        break; 
    
    case 'visitasxobra':
        
        $rspta = $dins->visitasXobra();
        
        $data = Array();
        
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "idsup" => $reg->idsupervisor,
                "sup" => $reg->sup,
                "proyectos" => $reg->proy,
                "visitas" => $reg->visitas,
                "procentaje" => round($reg->visitas / $reg->proy , 2),
                "terminados" => $reg->terminados
            );
        }
        
        echo json_encode($data);
        
        break;
        
    case 'estadosProyectosPm':
    
    $data = Array();   
        
    $rspta =$dins->estapaIniciadoProyectosPm($idpm);
    
    while ($reg = $rspta->fetch_object()) {
        $data[] = array(
            "idestadopro" => $reg->idestadopro,
            "nombre_estado" => $reg->nombre_estado,
            "cantidad" => $reg->cantidad,   
            "porcentaje" => $reg->porcentaje,  
        );        
    }
    
    $rspta =$dins->estadosProyectosPm($idpm);
    
    while ($reg = $rspta->fetch_object()) {
        $data[] = array(
            "idestadopro" => $reg->idestadopro,
            "nombre_estado" => $reg->nombre_estado,
            "cantidad" => $reg->cantidad,   
            "porcentaje" => $reg->porcentaje,  
        );        
    }
    
  
    
        echo json_encode($data);
        
    break;    
    
       case 'visitasProyectoPm':
        
        $rspta = $dins->visitasProyectoPm($idpm);
        
        $data = Array();
        
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "idsupervisorins" => $reg->idsupervisorins,
                "idpm" => $reg->idpm,
                "nombre_supervisor" => $reg->nombre_supervisor,
                "total_proyectos" => $reg->total_proyectos,
                "cantidad_ascensores"=>$reg->cantidad_ascensores,
                "cantidad_paradas"=>$reg->cantidad_paradas,
                "visitas_proyectos"=>$reg->visitas_proyectos,
                "cantidad_meses"=>$reg->cantidad_meses,
                "visitas_ascensores"=>$reg->visitas_ascensores
            );
        }
        
        echo json_encode($data);
        
        break;
}