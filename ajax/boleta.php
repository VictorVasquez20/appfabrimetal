<?php
session_start();
require_once '../modelos/Boleta.php';

$bol = new Boleta();

$idboleta = isset($_POST['idboleta']) ? $_POST['idboleta'] : 0;
$idventa = isset($_REQUEST['idventa']) ? $_REQUEST['idventa'] : 0;
$descripcion = isset($_POST['descripcion']) ? strtoupper($_POST['descripcion']) : "";
$documento = isset($_POST['documento']) ? strtoupper($_POST['documento']) : "";
$tipo = isset($_POST['tipo']) ? $_POST['tipo'] : "";
$validez = isset($_POST['validez']) ? date_format(date_create($_POST['validez']),'Y-m-d H:i:s') : "";
$ejecutada = isset($_POST['ejecutada']) ? $_POST['ejecutada'] : 0;
$banco = isset($_POST['banco']) ? strtoupper($_POST['banco']) : "";

switch ($_GET['op']){
    case 'guardaryeditar':
        //var_dump($idboleta );
        if($idboleta == 0){
            $rspta = $bol->Insertar($idventa, $descripcion, $documento, $tipo, $validez, $ejecutada, $banco);
            echo $rspta ? "1" : $rspta;
        }else{
            $rspta = $bol->Editar($idboleta, $idventa, $descripcion, $documento, $tipo, $validez, $ejecutada, $banco);
            echo $rspta ? "1" : $rspta;
        }
        break;
    
    case 'mostrar':
        $rspta = $bol->mostrar($idventa);
        echo json_encode($rspta);
        break;
    
    case 'listarventa':
        
        $rspta = $bol->mostrar($idventa);
        $data = Array();
        $countboleta = 0;
        while ($reg = $rspta->fetch_object()) {
            
            $hiden = "<input type='hidden' name='garantia[]' id='garantia_". $countboleta . "__idboleta' value='" .$reg->idboleta . "' />" .
                "<input type='hidden' name='garantia[].idventa' id='garantia_" . $countboleta . "__idventa' value='" .$reg->idventa ."' />" .
                "<input type='hidden' name='garantia[].descripcion' id='garantia_" . $countboleta . "__descripcion' value='" .$reg->descripcion . "' />" .
                "<input type='hidden' name='garantia[].documento' id='garantia_" . $countboleta . "__documento' value='" .$reg->documento . "' />" .
                "<input type='hidden' name='garantia[].tipo' id='garantia_" . $countboleta . "__tipo' value='" .$reg->tipo . "' />" .
                "<input type='hidden' name='garantia[].validez' id='garantia_" . $countboleta . "__validez' value='".$reg->validez . "' />" .
                "<input type='hidden' name='garantia[].ejecutada' id='garantia_" . $countboleta . "__ejecutada' value='".$reg->ejecutada . "' />";
            
            $data[] = array(
                "0" => $hiden. $reg->descripcion,
                "1" => $reg->documento,
                "2" => $reg->banco,
                "3" => $reg->tipo,
                "4" => date_format(date_create($reg->validez), 'd-m-Y'),
                "5" => "<i class='fa fa-trash' onclick='delGarantia($reg->idboleta, $idventa);'></i>",
                "6" => $reg->descripcion);
            
            $countboleta += 1;
        }
        
        /*$results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );*/
        echo json_encode($data);
        break;
        
    case 'eliminar':
        $rspta = $bol->Eliminar($idboleta);
        echo $rspta;
        break;
}
