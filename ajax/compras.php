<?php
require_once '../modelos/Compras.php';

$comp =  new Compras();

$idcompra = isset($_POST['idcompra']) ? $_POST['idcompra'] : "";
$idproveedor = isset($_POST['idproveedor']) ? $_POST['idproveedor'] : "";
$ordencompra = isset($_POST['ordencompra']) ? $_POST['ordencompra'] : "";
$presupuesto = isset($_POST['presupuesto']) ? $_POST['presupuesto'] : "";
$factura = isset($_POST['factura']) ? $_POST['factura'] : "";
$flete = isset($_POST['flete']) ? $_POST['flete'] : "";
$buy_time = isset($_POST['buy_time']) ? $_POST['buy_time'] : "";
$reception_time = isset($_POST['reception_time']) ? $_POST['reception_time'] : "";
$evaluacion = isset($_POST['evaluacion']) ? $_POST['evaluacion'] : "";
$condicion = isset($_POST['condicion']) ? $_POST['condicion'] : "";

switch($_GET['op']){
    case 'guardaryeditar':
        if(!$idcompra){
            $rspta = $comp->Insertar($idproveedor, $ordencompra, $presupuesto, $factura, $flete, $buy_time, $reception_time, $evaluacion, $condicion);
            echo $rspta;
        }else{
            $rspta = $comp->Editar($idcompra, $idproveedor, $ordencompra, $presupuesto, $factura, $flete, $buy_time, $reception_time, $evaluacion, $condicion);
            echo $rspta;
        }
        break;
    
    case 'listar':
        $idproveedor = isset($_REQUEST['idproveedor']) ? $_REQUEST['idproveedor'] : "";
        $rspta = $comp->Listar($idproveedor);
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            
            $ev = '<p class="ratings">'
                    . '<a href="#"><span class="fa '. ($reg->evaluacion >= 1 ? 'fa-star' : 'fa-star-o') .'"></span></a>'
                    . '<a href="#"><span class="fa '. ($reg->evaluacion >= 2 ? 'fa-star' : 'fa-star-o') .'"></span></a>'
                    . '<a href="#"><span class="fa '. ($reg->evaluacion >= 3 ? 'fa-star' : 'fa-star-o') .'"></span></a>'
                    . '<a href="#"><span class="fa '. ($reg->evaluacion >= 4 ? 'fa-star' : 'fa-star-o') .'"></span></a>'
                    . '<a href="#"><span class="fa '. ($reg->evaluacion >= 5 ? 'fa-star' : 'fa-star-o') .'"></span></a>'
                    . '</p>';
            
            $data[] = array(
                "0" => $reg->factura,
                "1" => $reg->presupuesto,
                "2" => $reg->ordencompra,
                "3" => $reg->buy_time,
                "4" => $ev,
                "5" => '<span class="glyphicon glyphicon-remove-sign" aria-hidden="true" onclick="delcompra(' .$reg->idcompra .')"></span>'
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
        
    case 'mostrar':
        $rspta = $comp->Mostrar($idcompra);
        echo json_encode($rspta);
        break;
    
    case 'eliminar':
        $rspta = $comp->Eliminar($idcompra);
        echo $rspta;
        break;
}