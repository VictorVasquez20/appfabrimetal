<?php

session_start();

require_once "../modelos/Ascensor.php";
require_once "../modelos/Servicio.php";
require_once '../public/build/lib/fabrimetal/pdf.php';

$ascensor = new Ascensor();
$servicio = new Servicio();

switch ($_GET["op"]) {

    case 'EE':
        $rspta = $ascensor->Estados();

        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => $reg->estado,
                "1" => $reg->equipos
            );
        }

        $results = array(
            "estados" => count($data),
            "aaData" => $data
        );
        echo json_encode($results);
        break;

    case 'DG':

        $penfirma = $servicio->penfirma();
        $proceso = $servicio->proceso();
        $completadas = $servicio->terminados();

        $results = array(
            "penfirma" => $penfirma,
            "proceso" => $proceso,
            "completadas" => $completadas,
        );

        echo json_encode($results);
        break;

    case 'GM':
        $rspta = $servicio->mesguias();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => $reg->mes,
                "1" => $reg->servicios
            );
        }
        $results = array(
            "servicios" => count($data),
            "aaData" => $data
        );
        echo json_encode($results);
        break;

    case 'GMM':
        $rspta = $servicio->mesmantencion();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => $reg->mes,
                "1" => $reg->servicios
            );
        }
        $results = array(
            "mantenciones" => count($data),
            "aaData" => $data
        );
        echo json_encode($results);
        break;


    case 'listarguias':
        
        $tservicio = isset($_REQUEST['tservicio']) ? $_REQUEST['tservicio'] : 0; 
        $ano = isset($_REQUEST['ano']) ? $_REQUEST['ano'] : 0; 
        $mes = isset($_REQUEST['mes']) ? $_REQUEST['mes'] : 0; 
        $sup = isset($_REQUEST['supervisor']) ? $_REQUEST['supervisor'] : 0;
        $tec = isset($_REQUEST['tecnico']) ? $_REQUEST['tecnico'] : 0;
        
        //$rspta = $servicio->listar_guias();
        $rspta = $servicio->listar_guias_filtro($tservicio, $ano, $mes, $sup, $tec);
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $op = "";
            
            if($_SESSION['administrador'] == 1){ //|| $_SESSION['editgse'] == 1){
                $op = '<button class="btn btn-info btn-xs" onclick="mostrar(' . $reg->idservicio . ')" data-toggle="tooltip" data-placement="top" title data-original-title="Editar"><i class="fa fa-list-alt"></i></button>'
                     . '<button class="btn btn-info btn-xs" onclick="pdf(' . $reg->idservicio . ')" data-toggle="tooltip" data-placement="top" title data-original-title="PDF"><i class="fa fa-file-pdf-o"></i></button>'
                     . '<button class="btn btn-success btn-xs" onclick="openmodal(' . $reg->idservicio . ')" data-toggle="tooltip" data-placement="top" title data-original-title="Procesar"><i class="fa fa-mail-forward"></i></button>';
            }else{
                $op = '<button class="btn btn-info btn-xs" onclick="pdf(' . $reg->idservicio . ')"><i class="fa fa-file-pdf-o"></i></button>'
                        . '<button class="btn btn-success btn-xs" onclick="openmodal(' . $reg->idservicio . ')" data-toggle="tooltip" data-placement="top" title data-original-title="Procesar"><i class="fa fa-mail-forward"></i></button>';
            }

            /*if($reg->idtservicio == 3){
                $op.='<button class="btn btn-success btn-xs" onclick="MostrarPreview(\'informeservicio\',\''. $reg->idservicio . ',' . $reg->idascensor . ',3\')" data-tooltip="tooltip" title="Descargar Checklist"><i class="fa fa-check-circle"></i></button>';
            }*/

            if($reg->idencuesta == 3){
                $op.='<button class="btn btn-success btn-xs" onclick="MostrarPreview(\'informeservicio\',\''. $reg->idservicio . ',' . $reg->idascensor . ',3\')" data-tooltip="tooltip" title="Descargar Checklist"><i class="fa fa-check-circle"></i></button>';
            }
            if($reg->idencuesta == 4){
                $op.='<button class="btn btn-success btn-xs" onclick="MostrarPreview(\'informeservicioescalera\',\''. $reg->idservicio . ',' . $reg->idascensor . ',4\')" data-tooltip="tooltip" title="Descargar Checklist"><i class="fa fa-check-circle"></i></button>';
            }
            
            $infodevs = '"'.trim($reg->infodev).'"';
            $data[] = array(
                "0" => $op . ($reg->infodev ? "<button class='btn btn-danger btn-xs' onclick='modalComentario($infodevs);'><i class='fa fa-comment'></i></button>" : ""),
                "1" => $reg->idservicio,
                "2" => $reg->nrodocumento,
                "3" => $reg->tipo,
                "4" => $reg->codigo,
                "5" => $reg->nombre,
                "6" => $reg->estadofin,
                "7" => $reg->fecha,
                "8" => $reg->reqfirma == 1 ? "SI" : "NO",
                "9" => $reg->firma == "" ? "NO" : "SI"
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
    
    case 'listarguias2':
        
        $tservicio = isset($_REQUEST['tservicio']) ? $_REQUEST['tservicio'] : 0; 
        $ano = isset($_REQUEST['ano']) ? $_REQUEST['ano'] : 0; 
        $mes = isset($_REQUEST['mes']) ? $_REQUEST['mes'] : 0; 
        $sup = isset($_REQUEST['supervisor']) ? $_REQUEST['supervisor'] : 0;
        $tec = isset($_REQUEST['tecnico']) ? $_REQUEST['tecnico'] : 0;
        $edificio = isset($_REQUEST['edificio']) ? $_REQUEST['edificio'] : 0;
        $equipo = isset($_REQUEST['equipo']) ? $_REQUEST['equipo'] : 0;
        
        //$rspta = $servicio->listar_guias();
        $rspta = $servicio->listar_guias_filtro2($tservicio, $ano, $mes, $sup, $tec,$edificio,$equipo);
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $hservicio = "";
            $hnor = "";
            $hex = "";
            
            //HORAS TOTALES
            $m = ($reg->tservicio % 60) <= 9 ? "0".($reg->tservicio % 60): ($reg->tservicio % 60);
            $h = (($reg->tservicio - ($reg->tservicio % 60)) / 60) <= 9 ? "0".(($reg->tservicio - ($reg->tservicio % 60)) / 60) :  (($reg->tservicio - ($reg->tservicio % 60)) / 60);
            $hservicio = $h.":".$m;
            
            //CALCULO HORAS NORMALES
            $mn = ($reg->horasnormal % 60) <= 9 ? "0".($reg->horasnormal % 60): ($reg->horasnormal % 60);
            $hn = (($reg->horasnormal - ($reg->horasnormal % 60)) / 60) <= 9 ? "0".(($reg->horasnormal - ($reg->horasnormal % 60)) / 60) :  (($reg->horasnormal - ($reg->horasnormal % 60)) / 60);
            $hnor = $hn.":".$mn;
            
            //CALCULO HORAS EXTRAS
            $me = ($reg->horasextras % 60) <= 9 ? "0".($reg->horasextras % 60): ($reg->horasextras % 60);
            $he = (($reg->horasextras - ($reg->horasextras % 60)) / 60) <= 9 ? "0".(($reg->horasextras - ($reg->horasextras % 60)) / 60) :  (($reg->horasextras - ($reg->horasextras % 60)) / 60);
            $hex = $he.":".$me;
            
            //SI ES FIN DE SEMANA EL TIEMPO TOTAL DE LA DURACION DEL SERVICIO QUEDA COMO HORA EXTRA
            if($reg->dia == "Sab" || $reg->dia == "Dom"){
                $hex = $hservicio;
            }
            $op = '<button class="btn btn-info btn-xs" onclick="pdf(' . $reg->idservicio . ')" data-tooltip="tooltip" title="Descargar GSE"><i class="fa fa-file-pdf-o"></i></button>';
            if($reg->idencuesta == 3){
                $op.='<button class="btn btn-success btn-xs" onclick="MostrarPreview(\'informeservicio\',\''. $reg->idservicio . ',' . $reg->idascensor . ',3\')" data-tooltip="tooltip" title="Descargar Checklist"><i class="fa fa-check-circle"></i></button>';
            }
            if($reg->idencuesta == 4){
                $op.='<button class="btn btn-success btn-xs" onclick="MostrarPreview(\'informeservicioescalera\',\''. $reg->idservicio . ',' . $reg->idascensor . ',4\')" data-tooltip="tooltip" title="Descargar Checklist"><i class="fa fa-check-circle"></i></button>';
            }
            
            if(!empty($reg->latini) && !empty($reg->lonini) && !empty($reg->latfin) && !empty($reg->lonfin)){
                $op.= '<button class="btn btn-success btn-xs" onclick="MostrarMapa(\''.$reg->latini.'\',\''.$reg->lonini.'\',\''.$reg->latfin.'\',\''.$reg->lonfin.'\')" data-toggle="tooltip" data-placement="top" title data-original-title="Procesar"><i class="fa fa-street-view" aria-hidden="true"></i></button>';
            }
                        
        
            $infodevs = '"'.trim($reg->infodev).'"';
            $data[] = array(
                "0" => $op,
                "1" => $reg->idservicio,
                "2" => $reg->tipo,
                "3" => $reg->codigo,
                "4" => $reg->nombre,
                "5" => $reg->nombre_supervisor,
                "6" => $reg->nombre_tecnico,
                "7" => $reg->ccosto,
                "8" => $reg->fecini,
                "9" => $reg->inicio,
                "10" => $reg->fecfin,
                "11" => $reg->fin,
                "12"=> $hservicio,
                "13"=> $hnor,
                "14"=> $hex,
                "15"=> $reg->estado
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;

    case 'listarguiasdescarga':
        $tservicio = isset($_REQUEST['tservicio']) ? $_REQUEST['tservicio'] : 0; 
        $sup = isset($_REQUEST['supervisor']) ? $_REQUEST['supervisor'] : 0;
        $edificio = isset($_REQUEST['edificio']) ? $_REQUEST['edificio'] : 0;
        $equipo = isset($_REQUEST['equipo']) ? $_REQUEST['equipo'] : 0;
        $desde = isset($_REQUEST['desde']) ? $_REQUEST['desde'] : 0;
        $hasta = isset($_REQUEST['hasta']) ? $_REQUEST['hasta'] : 0;
        
        //$rspta = $servicio->listar_guias();
        $rspta = $servicio->listar_guias_descarga($tservicio,$sup,$edificio,$equipo,$desde,$hasta);
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $hservicio = "";
            $hnor = "";
            $hex = "";
            
            //HORAS TOTALES
            $m = ($reg->tservicio % 60) <= 9 ? "0".($reg->tservicio % 60): ($reg->tservicio % 60);
            $h = (($reg->tservicio - ($reg->tservicio % 60)) / 60) <= 9 ? "0".(($reg->tservicio - ($reg->tservicio % 60)) / 60) :  (($reg->tservicio - ($reg->tservicio % 60)) / 60);
            $hservicio = $h.":".$m;
            
            //CALCULO HORAS NORMALES
            $mn = ($reg->horasnormal % 60) <= 9 ? "0".($reg->horasnormal % 60): ($reg->horasnormal % 60);
            $hn = (($reg->horasnormal - ($reg->horasnormal % 60)) / 60) <= 9 ? "0".(($reg->horasnormal - ($reg->horasnormal % 60)) / 60) :  (($reg->horasnormal - ($reg->horasnormal % 60)) / 60);
            $hnor = $hn.":".$mn;
            
            //CALCULO HORAS EXTRAS
            $me = ($reg->horasextras % 60) <= 9 ? "0".($reg->horasextras % 60): ($reg->horasextras % 60);
            $he = (($reg->horasextras - ($reg->horasextras % 60)) / 60) <= 9 ? "0".(($reg->horasextras - ($reg->horasextras % 60)) / 60) :  (($reg->horasextras - ($reg->horasextras % 60)) / 60);
            $hex = $he.":".$me;
            
            //SI ES FIN DE SEMANA EL TIEMPO TOTAL DE LA DURACION DEL SERVICIO QUEDA COMO HORA EXTRA
            if($reg->dia == "Sab" || $reg->dia == "Dom"){
                $hex = $hservicio;
            }
            $op = '<button class="btn btn-info btn-xs" onclick="pdf(' . $reg->idservicio . ')" data-tooltip="tooltip" title="Descargar GSE"><i class="fa fa-file-pdf-o"></i></button>';
            if($reg->idencuesta == 3){
                $op.='<button class="btn btn-success btn-xs" onclick="MostrarPreview(\'informeservicio\',\''. $reg->idservicio . ',' . $reg->idascensor . ',3\')" data-tooltip="tooltip" title="Descargar Checklist"><i class="fa fa-check-circle"></i></button>';
            }
            if($reg->idencuesta == 4){
                $op.='<button class="btn btn-success btn-xs" onclick="MostrarPreview(\'informeservicioescalera\',\''. $reg->idservicio . ',' . $reg->idascensor . ',4\')" data-tooltip="tooltip" title="Descargar Checklist"><i class="fa fa-check-circle"></i></button>';
            }
            
            if(!empty($reg->latini) && !empty($reg->lonini) && !empty($reg->latfin) && !empty($reg->lonfin)){
                $op.= '<button class="btn btn-success btn-xs" onclick="MostrarMapa(\''.$reg->latini.'\',\''.$reg->lonini.'\',\''.$reg->latfin.'\',\''.$reg->lonfin.'\')" data-toggle="tooltip" data-placement="top" title data-original-title="Procesar"><i class="fa fa-street-view" aria-hidden="true"></i></button>';
            }
                        
        
            $infodevs = '"'.trim($reg->infodev).'"';
            $data[] = array(
                "0" => $reg->idservicio,
                "1" => $reg->idservicio,
                "2" => $reg->tipo,
                "3" => $reg->codigo,
                "4" => $reg->nombre,
                "5" => $reg->nombre_supervisor,
                "6" => $reg->nombre_tecnico,
                "7" => $reg->ccosto,
                "8" => $reg->fecini,
                "9" => $reg->inicio,
                "10" => $reg->fecfin,
                "11" => $reg->fin,
                "12"=> $hservicio,
                "13"=> $hnor,
                "14"=> $hex,
                "15"=> $reg->estado,
                "16"=> $reg->nombre."/".$reg->codigo."/".$reg->ano."/".$reg->mes
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
        
        
    case 'listarguiasmes':
        $rspta = $servicio->listar_guiasmes();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => '<button class="btn btn-info btn-xs" onclick="mostrar(' . $reg->idservicio . ')"><i class="fa fa-list-alt"></i></button><button class="btn btn-info btn-xs" onclick="pdf(' . $reg->idservicio . ')"><i class="fa fa-file-pdf-o"></i></button>',
                "1" => $reg->idservicio,
                "2" => $reg->tipo,
                "3" => $reg->codigo,
                "4" => $reg->nombre,
                "5" => $reg->fecha,
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
        
    case 'listaremergencia':
        $rspta = $servicio->listar_emergencia();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            if(is_null($reg->fin)){
                            $fin='<span class="label bg-red">PROCESO</span>';
                            $op='';
            }else{
                            $fin=$reg->fin;
                            $op='<button class="btn btn-info btn-xs" onclick="pdf(' . $reg->idservicio . ')"><i class="fa fa-file-pdf-o"></i></button>';
            }
            
            
            $data[] = array(
                "0" => $op,
                "1" => $reg->idservicio,
                "2" => $reg->nrodoc,
                "3" => $reg->codigo,
                "4" => $reg->nombre,
                "5" => $reg->nomtec.' '.$reg->apetec,
                "6" => $reg->mes,
                "7" => $reg->fecha,
                "8" => $reg->inicio,
                "9" => $fin,
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
    break;
    
    case 'listarpfirma':
        $rspta = $servicio->listar_pfirma();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $op = "";
            
            if($_SESSION['administrador'] == 1 || $_SESSION['editgse'] == 1){
                $op = '<button class="btn btn-info btn-xs" onclick="mostrar(' . $reg->idservicio . ')"><i class="fa fa-list-alt"></i></button>'
                     . '<button class="btn btn-info btn-xs" onclick="pdf(' . $reg->idservicio . ')"><i class="fa fa-file-pdf-o"></i></button>';
            }else{
                $op = '<button class="btn btn-info btn-xs" onclick="pdf(' . $reg->idservicio . ')"><i class="fa fa-file-pdf-o"></i></button>';
            }
            
            $data[] = array(
                "0" => $op,
                "1" => $reg->idservicio,
                "2" => $reg->tipo,
                "3" => $reg->codigo,
                "4" => $reg->nombre,
                "5" => $reg->nomtec.' '.$reg->apetec,
                "6" => $reg->nomsup.' '.$reg->apesup,
                //"7" => $reg->mes,
                "7" => $reg->fecha,
                //"9" => $reg->inicio,
                //"10" => $reg->fin,
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
    break;
    
    case 'listarpfirmasup':
        $rut = $_SESSION['rut'];
        $rspta = $servicio->listar_psup($rut);
        $data = Array();
        while ($reg = $rspta->fetch_object()) {

            $data[] = array(
                "0" => $reg->fecha,
                "1" => $reg->idservicio,
                "2" => $reg->tipo,
                "3" => $reg->codigo,
                "4" => $reg->nombre,
                "5" => $reg->tecnico
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
    break;
    
    
    case 'LGSESup':
        $rut = $_SESSION['rut'];
        $rspta = $servicio->LGSESup($rut);
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            //! código antiguo - comentado
            /*
            $data[] = array(
                "0" => $reg->fecha,
                "1" => $reg->idservicio,
                "2" => $reg->tipo,
                "3" => $reg->codigo,
                "4" => $reg->nombre,
                "5" => $reg->tecnico,
                "6" => $reg->estado
            );
            */

            //! inicio código nuevo

            $hservicio = "";
            $hnor = "";
            $hex = "";
            
            //HORAS TOTALES
            $m = ($reg->tservicio % 60) <= 9 ? "0".($reg->tservicio % 60): ($reg->tservicio % 60);
            $h = (($reg->tservicio - ($reg->tservicio % 60)) / 60) <= 9 ? "0".(($reg->tservicio - ($reg->tservicio % 60)) / 60) :  (($reg->tservicio - ($reg->tservicio % 60)) / 60);
            $hservicio = $h.":".$m;
            
            //CALCULO HORAS NORMALES
            $mn = ($reg->horasnormal % 60) <= 9 ? "0".($reg->horasnormal % 60): ($reg->horasnormal % 60);
            $hn = (($reg->horasnormal - ($reg->horasnormal % 60)) / 60) <= 9 ? "0".(($reg->horasnormal - ($reg->horasnormal % 60)) / 60) :  (($reg->horasnormal - ($reg->horasnormal % 60)) / 60);
            $hnor = $hn.":".$mn;
            
            //CALCULO HORAS EXTRAS
            $me = ($reg->horasextras % 60) <= 9 ? "0".($reg->horasextras % 60): ($reg->horasextras % 60);
            $he = (($reg->horasextras - ($reg->horasextras % 60)) / 60) <= 9 ? "0".(($reg->horasextras - ($reg->horasextras % 60)) / 60) :  (($reg->horasextras - ($reg->horasextras % 60)) / 60);
            $hex = $he.":".$me;
            
            //SI ES FIN DE SEMANA EL TIEMPO TOTAL DE LA DURACION DEL SERVICIO QUEDA COMO HORA EXTRA
            if($reg->dia == "Sab" || $reg->dia == "Dom"){
                $hex = $hservicio;
            }

            $op = '<button class="btn btn-info btn-xs" onclick="pdf(' . $reg->idservicio . ')" data-tooltip="tooltip" title="Descargar GSE"><i class="fa fa-file-pdf-o"></i></button>';

            $data[] = array(
                "0" => $op,
                "1" => $reg->idservicio,
                "2" => $reg->tipo,
                "3" => $reg->codigo,
                "4" => $reg->nombre,
                "5" => $reg->tecnico,
                "6" => $reg->estado,
                "7" => $reg->fecini,
                "8" => $reg->inicio,
                "9" => $reg->fecfin,
                "10" => $reg->fin,
                "11"=> $hservicio,
                "12"=> $hnor,
                "13"=> $hex,
                "14"=> $reg->ccosto
            );

            //! fin código nuevo
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
    break;
    
    case 'listarGSESupervisor':
        $rut = $_SESSION['rut'];
        $tservicio = isset($_REQUEST['tservicio']) ? $_REQUEST['tservicio'] : 0; 
        $ano = isset($_REQUEST['ano']) ? $_REQUEST['ano'] : 0; 
        $mes = isset($_REQUEST['mes']) ? $_REQUEST['mes'] : 0; 
        $tec = isset($_REQUEST['tecnico']) ? $_REQUEST['tecnico'] : 0;
        $firma = isset($_REQUEST['firma']) ? $_REQUEST['firma'] : 0;
        
        //$rspta = $servicio->listar_guias();
        $rspta = $servicio->LGSESupervisor($rut, $tservicio, $ano, $mes, $tec, $firma);
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $hservicio = "";
            $hnor = "";
            $hex = "";
            
            //HORAS TOTALES
            $m = ($reg->tservicio % 60) <= 9 ? "0".($reg->tservicio % 60): ($reg->tservicio % 60);
            $h = (($reg->tservicio - ($reg->tservicio % 60)) / 60) <= 9 ? "0".(($reg->tservicio - ($reg->tservicio % 60)) / 60) :  (($reg->tservicio - ($reg->tservicio % 60)) / 60);
            $hservicio = $h.":".$m;
            
            //CALCULO HORAS NORMALES
            $mn = ($reg->horasnormal % 60) <= 9 ? "0".($reg->horasnormal % 60): ($reg->horasnormal % 60);
            $hn = (($reg->horasnormal - ($reg->horasnormal % 60)) / 60) <= 9 ? "0".(($reg->horasnormal - ($reg->horasnormal % 60)) / 60) :  (($reg->horasnormal - ($reg->horasnormal % 60)) / 60);
            $hnor = $hn.":".$mn;
            
            //CALCULO HORAS EXTRAS
            $me = ($reg->horasextras % 60) <= 9 ? "0".($reg->horasextras % 60): ($reg->horasextras % 60);
            $he = (($reg->horasextras - ($reg->horasextras % 60)) / 60) <= 9 ? "0".(($reg->horasextras - ($reg->horasextras % 60)) / 60) :  (($reg->horasextras - ($reg->horasextras % 60)) / 60);
            $hex = $he.":".$me;
            
            //SI ES FIN DE SEMANA EL TIEMPO TOTAL DE LA DURACION DEL SERVICIO QUEDA COMO HORA EXTRA
            if($reg->dia == "Sab" || $reg->dia == "Dom"){
                $hex = $hservicio;
            }
            
            $op = '<button class="btn btn-info btn-xs" onclick="pdf(' . $reg->idservicio . ')" data-tooltip="tooltip" title="Descargar GSE"><i class="fa fa-file-pdf-o"></i></button>';
            
            $data[] = array(
                "0" => $op,
                "1" => $reg->idservicio,
                "2" => $reg->tipo,
                "3" => $reg->codigo,
                "4" => $reg->nombre,
                "5" => $reg->tecnico,
                "6" => $reg->estado,
                "7" => $reg->fecini,
                "8" => $reg->inicio,
                "9" => $reg->fecfin,
                "10" => $reg->fin,
                "11" => $hservicio,
                "12"=> $hnor,
                "13"=> $hex,
                "14"=> $reg->ccosto
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
    
    case 'dashemergencia':
        $rspta = $servicio->listar_dashemergencia();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            if(is_null($reg->fin)){
                            $fin='<span class="label bg-red">PROCESO</span>';
                            $estadofin='<span class="label bg-red">PROCESO</span>';
            }else{
                            $fin=$reg->fin;
                            $estadofin=$reg->esfin;;
            }
            
            
            $data[] = array(
                "0" => $reg->idservicio,
                "1" => $reg->codigo,
                "2" => $reg->nombre,
                "3" => $reg->nomtec.' '.$reg->apetec,
                "4" => $reg->fecha,
                "5" => $reg->inicio,
                "6" => $reg->esini,
                "7" => $fin,
                "8" => $estadofin
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
    break;

    case 'pdf':
        $idservicio = isset($_POST["idservicio"]) ? limpiarCadena($_POST["idservicio"]) : "";
        $rspta = $servicio->pdf($idservicio);
        echo json_encode($rspta);
        break;
    
    case 'pdfemergencia':
        $idservicio = isset($_POST["idservicio"]) ? limpiarCadena($_POST["idservicio"]) : "";
        $rspta = $servicio->pdfemergencia($idservicio);
        echo json_encode($rspta);
    break;

    case 'graficomantencion':
        $rspta = $servicio->Grafico();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => $reg->guias,
                "1" => $reg->mes,
                "2" => $reg->tipo
            );
        }
        echo json_encode($data);
        break;
       
    case 'listarxestado':
        $estado = isset($_REQUEST['estado']) ? $_REQUEST['estado'] : 0;
        $rspta = $ascensor->listarAscEstados($estado);
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => $reg->codigo,
                "1" => $reg->stredificio,
                "2" => $reg->ubicacion,
                "3" => $reg->strmarca,
                "4" => $reg->strmodelo,
                "5" => $reg->codigocli
            );
        }
       
        echo json_encode($data);
        break;
    
    case "selectano":
        $rspta = $servicio->selectano();
        echo '<option value="0" selected>TODOS</option>';
        while($reg = $rspta->fetch_object()){
             echo '<option value='.$reg->ano.'>'.$reg->ano.'</option>';
        }
        break;
    
    case "mostrar":
        $idservicio = isset($_POST['idservicio']) ? $_POST['idservicio'] : 0;
        $rspta = $servicio->Mostrar($idservicio);
        echo json_encode($rspta);
        break;
    
    case "editar":
        $idservicio = isset($_POST["idservicio"]) ? $_POST["idservicio"] : 0;
        $idascensor = isset($_POST["idascensor"]) ? $_POST["idascensor"] : 0;
        $idtservicio = isset($_POST["idtservicio"]) ? $_POST["idtservicio"] : 0;
        $idtecnico = isset($_POST["idtecnico"]) ? $_POST["idtecnico"] : 0;
        $estadofin = isset($_POST["estadofin"]) ? $_POST["estadofin"] : 0;
        $reqfirma = isset($_POST["reqfirma"]) ? $_POST["reqfirma"] : 0;
        $observacionfin = isset($_POST["observacionfin"]) ? $_POST["observacionfin"] : 0;
        $created_time = isset($_POST["created_time"]) ? $_POST["created_time"] : 0;
        $closed_time = isset($_POST["closed_time"]) ? $_POST["closed_time"] : 0;
        $estadoini = isset($_POST["estadoini"]) ? $_POST["estadoini"] : 0;
        $observacionini = isset($_POST["observacionini"]) ? $_POST["observacionini"] : 0;
                 
        if($idservicio != 0){
            $rspta = $servicio->Editar($idservicio, $idtservicio, $idtecnico, $idascensor, $estadofin, $observacionfin, $reqfirma, $created_time, $closed_time, $estadoini, $observacionini);
            echo $rspta;
        }else{
            echo 0;
        }
        break;
        
    case 'setFacturacion':
        
        $idservicio = isset($_POST["idservicio"]) ? $_POST["idservicio"] : 0;
        $facturacion = isset($_POST["facturacion"]) ? $_POST["facturacion"] : 0;
        $idcentrocos = isset($_POST["idcentrocos"]) ? $_POST["idcentrocos"] : 0;
        $infopro = isset($_POST["infopro"]) ? strtoupper($_POST["infopro"]) : 0;
        
        if($idservicio){
            $rspta = $servicio->setFacturacion($idservicio, $facturacion, $idcentrocos, $infopro);
            echo $rspta;
        }else{
            echo 0;
        }
        
        break;

    case "selectequipo":
        if(isset($_POST['idedificio'])){
            $rspta = $servicio->selectequipoxedificio($_POST['idedificio']);
        }else{
            $rspta = $servicio->selectequipo();
        }
        echo '<option value="0" selected>TODOS</option>';
        while($reg = $rspta->fetch_object()){
             echo '<option value='.$reg->id.'>'.$reg->codigo.'</option>';
        }
        break;

        case 'PDFINFSERVICIO':
            $visita = isset($_REQUEST["visita"]) ? limpiarCadena($_REQUEST["visita"]) : "";
            $visita = explode(",", $visita);
    
            $idservicio = $visita[0];
            $idascensor = $visita[1];
            $idencuesta = $visita[2];
    
            $params['idservicio'] = $idservicio . '';
            $params['idascensor'] = $idascensor . '';
            $params['idencuesta'] = $idencuesta . '';
    
            newPdf('informemantencion', '', 'browser', $params);
    
            break;
}