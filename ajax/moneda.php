<?php
session_start();

require_once '../modelos/Moneda.php';

$moneda = new Moneda();

$idmoneda = isset($_POST['idmoneda']) ? $_POST['idmoneda'] : 0;
$idtformapago = isset($_POST['idtformapago']) ? $_POST['idtformapago'] : 0;
$nombre = isset($_POST['nombre']) ? $_POST['nombre'] : 0;
$simbolo = isset($_POST['simbolo']) ? $_POST['simbolo'] : 0;
$condicion = isset($_POST['condicion']) ? $_POST['condicion'] : 0;

switch ($_GET['op']){
    case 'guardaryeditar':
        if(!$idmoneda){
            $rspta = $moneda->Insertar($idtformapago, $nombre, $simbolo, $condicion);
            echo $rspta ? "Moneda guardada" :  "Moneda no guardada";
        }else{
            $rspta = $moneda->Editar($idmoneda, $idtformapago, $nombre, $simbolo, $condicion);
            echo $rspta ? "Moneda editada" : "Moneda no editada";
        }
        break;
    
    case 'selectmoneda':
        $rspta = $moneda->selectMoneda($idtformapago);
        echo '<option value="0" selected disabled>Seleccione moneda</option>';
        while($reg = $rspta->fetch_object()){
                echo '<option value='.$reg->idmoneda.'>'.$reg->nombre. ' ('. $reg->simbolo .')</option>';
        }
        break;
}
