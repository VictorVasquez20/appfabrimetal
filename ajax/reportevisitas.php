<?php
session_start();

require_once '../modelos/VisitaInstalaciones.php';
require_once '../modelos/Proyecto.php';

$visita = new VisitasInstalaciones();
$proyecto = new Proyecto();

$tipoproyecto = isset($_REQUEST['tipoproyecto']) ? $_REQUEST['tipoproyecto'] : "";
$idproyecto = isset($_REQUEST['idproyecto']) ? $_REQUEST['idproyecto'] : "";
$created_user = isset($_REQUEST['created_user']) ? $_REQUEST['created_user'] : "";

 switch ($_GET["op"]) {
              
         case 'listaproyectos':
        
        $rut='00000000-0'; // para que liste todos los proyectos.
             
        $rspta = $proyecto->listar($rut, $tipoproyecto);

        $data = Array();

        while ($reg = $rspta->fetch_object()) {

            $data[] = array(
                "0" => $reg->nombre,
                "1" => $reg->codigo,
                "2" => $reg->pm,
                "3" => $reg->supervisor,
                "4" => $reg->carga." % Completo",
                "5" => $reg->estadonomb,
                "6" => $reg->dias_comienzo,
                "7" => $reg->updated_time,
                "8" => $reg->dias
                    );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        
     break; 
     
     case 'visitasproyectos':
         
        $rspta = $visita->visitaproyectos();

        $data = Array();

        while ($reg = $rspta->fetch_object()) {
            
         $detallevisitas =' <button data-toggle="modal" data-target="#modalVisitas" class="btn btn-success btn-xs" data-idproyecto="'.$reg->idproyecto.'" data-created_user="'.$reg->created_user.'" title=""><i class="fa fa-check-square-o"></i></button>';
         
            $data[] = array(
                "0" => $reg->nombre_proyecto,
                "1" => $reg->codigo,
                "2" => $reg->nombre_estado,
                "3" => $reg->cargo,
                "4" => $reg->nombre_tecnico,
                "5" => $reg->cantidad_visitas,
                "6" => $detallevisitas,
         
                    );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        
     break;    
     
     case 'visitasetapa':
        
        $rspta = $visita->visitasetapa($idproyecto,$created_user);

        $data = Array();

        while ($reg = $rspta->fetch_object()) {
            
            $barraestado = '<button type="button" class="btn btn-xs" style="background-color:' . $reg->color . '; color: #fff;">' . $reg->nombre_estado . '</button>';

            $data[] = array(
                "0" => $barraestado,
                "1" => $reg->cantidad_visitas,
                    );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        
     break;   
}

