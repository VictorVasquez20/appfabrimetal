<?php

session_start();

require_once "../modelos/AproVentas.php";

$venta = new AproVentas();

// DTR.INI ETAPAS FM Y FECHAS
function getof($idventa)
{
	global $venta;

	$query = $venta->getStep($idventa);

	while ($row = $query->fetch_object()) {
		$Step[] = $row->descripcion;
	}
	$Step = implode(',', $Step);

	$query = $venta->getStepsEquip($idventa);

	while ($row = $query->fetch_object()) {
		$step_array2[$row->codigo][] = $row->fecha;
		$step_array[$row->descripcion][$row->fecha][] = $row->codigo;
	}

	$temp = 1; /* edificio */

	foreach ($step_array as $step => $date_array) {

		if (sizeof($date_array) > $temp) {

			foreach ($date_array as $fecha => $fm_array) {
				
				$code_fm[] = implode(',', $fm_array);

				foreach ($fm_array as $key => $fm) {

					$dates[implode(',', $step_array2[$fm])] = 1;

				}

			}

			$temp = sizeof($date_array);
		}

	}

	if ($temp <= 1) {
		return false;
	}

	foreach ($dates as $key => $value) {
		$date[] = $key;
	}

	for ($i=0; $i < $temp; $i++) {
		
		$of[$i] = array($Step => array(
				'date' => $date[$i],
				'fm' => $code_fm[$i]
			)
		);

	}

	return $of;

}
// DTR.FIN

switch ($_GET["op"]) {

	case 'LV':
		$rspta = $venta->LV();
		$data = Array();
		while ($reg = $rspta->fetch_object()) {
			$data[] = array(
				"0" => '<button class="btn btn-success btn-xs" onclick="enviararevision('.$reg->idventa.',3)" data-tooltip="tooltip" title="Aprobar Memo de Venta"><i class="fa fa-check"></i></button><button class="btn btn-danger btn-xs" onclick="enviararevision('.$reg->idventa.',2)" data-tooltip="tooltip" title="Revision Memo de Venta"><i class="fa fa-close"></i></button><button class="btn btn-info btn-xs" onclick="pdf(' . $reg->idventa . ')" data-tooltip="tooltip" title="Memo de venta (PDF)"><i class="fa fa-file-pdf-o"></i></button>',
				"1" => $reg->proyecto,
				"2" => $reg->codigo,
				"3" => $reg->mandante,
				"4" => $reg->fecha,
				"5" => $reg->vendedor
			);
		}

		$results = array(
			"sEcho" => 1,
			"iTotalRecords" => count($data),
			"iTotalDisplayRecords" => count($data),
			"aaData" => $data
		);
		echo json_encode($results);
		break;


	case 'PDF':
		$idventa = isset($_POST["idventa"]) ? limpiarCadena($_POST["idventa"]) : "";

		//DATOS DEL MEMO
		$datos = $venta->pdfdatos($idventa);
		
		//DATOS DEL COMERCIALES VENTA
		$vencomer = $venta->pdfMontoPro($idventa);

		//NUMERO DE ETAPAS
		$ninfo = $venta->ninfo($idventa);

		//INFORMACIO CONTRACTUAL
		$rspinfo = $venta->pdfinfo($idventa);
		$info = Array();
		while ($reg = $rspinfo->fetch_object()) {
			$info[] = array(
				"0" => $reg->descripcion,
				"1" => $reg->fecha
			);
		}

		$rspsi = $venta->pdfformasi($idventa);
		$fsi = Array();
		while ($reg = $rspsi->fetch_object()) {
			$fsi[] = array(
				"0" => $reg->descripcion,
				"1" => $reg->porcentaje
			);
		}

		$rspsn = $venta->pdfformasn($idventa);
		$fsn = Array();
		while ($reg = $rspsn->fetch_object()) {
			$fsn[] = array(
				"0" => $reg->descripcion,
				"1" => $reg->porcentaje
			);
		}

		$rspboleta = $venta->pdfboleta($idventa);
		$boletas = Array();
		while ($reg = $rspboleta->fetch_object()) {
			$boletas[] = array(
				"0" => $reg->banco,
				"1" => $reg->descripcion,
				"2" => $reg->documento,
				"3" => $reg->tipo,
				"4" => $reg->validez
			);
		}

		$rspequipos = $venta->pdfequipos($idventa);
		$equipos = Array();
		while ($reg = $rspequipos->fetch_object()) {
			$equipos[] = array(
				"0" => $reg->tascensor,
				"1" => $reg->modelo,
				"2" => $reg->comando,
				"3" => $reg->codigo,
				"4" => $reg->ken,
				"5" => $reg->paradas.' / '.$reg->accesos,
				"6" => $reg->capper.' PERSONAS / '.$reg->capkg.' KG',

			);
		}
		
		$montoequipos = $venta->pdfMontoEquipos($idventa);
		$montos = Array();
		while ($reg = $montoequipos->fetch_object()) {
			$montos[] = array(
				"0" => $reg->nombre.' '.$reg->modelo,
				"1" => $reg->codigo,
				"2" => $reg->ken,
				"3" => $reg->montosi,
				"4" => $reg->simsi,
				"5" => $reg->montosn,
				"6" => $reg->simsn
			);
		}
		
		$results = array(
			"Datos" => $datos,
			"Etapas" => $ninfo,
			"Info" => $info,
			"FSi" => $fsi,
			"FSn" => $fsn,
			"Of" => (getof($idventa) ? getof($idventa): 'null'), // DTR.INI
			"Boletas" => $boletas,
			"Equipos" => $equipos,
			"MontoVen" => $vencomer,
			"MontoAsc" => $montos
		);

		echo utf8_encode(json_encode($results));
		break;
}
