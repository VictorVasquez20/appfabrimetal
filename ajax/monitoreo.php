<?php

session_start();
require_once "../modelos/Monitoreo.php";


$monitoreo = new Monitoreo();

$op = $_GET["op"];

switch ($op) {

    case 'listarAlerta':
        $rspta = $monitoreo->listar();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => $reg->idascensor,
                "1" => $reg->codigo,
                "2" => $reg->codigocli,
                "3" => $reg->ubicacion,
                "4" => $reg->monitoreo,
                "5" => $reg->lat,
                "6" => $reg->lon,
                "7" => $reg->nombre,
                "8" => $reg->calle . ' ' . $reg->numero,
                "9" => 'Region Metropolitana - ' . $reg->comuna,
                "10" => $reg->segmento,
                "11" => $reg->contacto,
                "12" => $reg->email,
                "13" => $reg->telefono
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;

    case 'datos':
        $codigo = $_GET["C"];
        $rspta = $monitoreo->datos($codigo);
        echo json_encode($rspta);
        break;
}
?>