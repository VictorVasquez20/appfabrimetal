<?php

require_once '../modelos/DashCor.php';

$dcor = new DashCor();

switch ($_GET["op"]){
    case 'indicadores':
        $rspta = $dcor->getResumenGSE();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => $reg->nombre,
                "1" => $reg->idtservicio,
                "2" => $reg->gsemes,
                "3" => $reg->gsefact
            );
        }
        echo json_encode($data);
        break;
        
        
    case 'grafico':
        $rspta = $dcor->getGraficoGSE();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => $reg->mes,
                "1" => $reg->mesname,
                "2" => $reg->guiames,
                "3" => $reg->guiamesfact,
                "4" => $reg->idtservicio
            );
        }
        echo json_encode($data);
        break;
        
        
    case 'graficoemergencia':
        $rspta = $dcor->graficoGSEemergencias();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => $reg->mes,
                "1" => $reg->mesname,
                "2" => $reg->guiames,
                "3" => $reg->guiamesfact,
                "4" => $reg->idtservicio,
                "5" => $reg->estadofin
            );
        }
        echo json_encode($data);
        break;
        
    /** INDICADORES CON FITRO DE FECHA **/
    case 'equiposcartera':
        $fecha = isset($_GET['fecha']) ? $_GET['fecha'] : date('Y-m-d');
        
        $rspta = $dcor->getEquiposCartera();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => $reg->cartera
            );
        }
        echo json_encode($data);
        break;

    case 'gseEm':
        $fecha = isset($_GET['fecha']) ? $_GET['fecha'] : date('Y-m-d');
        
        $rspta = $dcor->getGseEm();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => $reg->cant_em
            );
        }
        echo json_encode($data);
        break;

    case 'equiposcarteraRM':
        $fecha = isset($_GET['fecha']) ? $_GET['fecha'] : date('Y-m-d');
        
        $rspta = $dcor->getEquiposCarteraRM();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => $reg->cartera
            );
        }
        echo json_encode($data);
        break;

    case 'gseEmRM':
        $fecha = isset($_GET['fecha']) ? $_GET['fecha'] : date('Y-m-d');
        
        $rspta = $dcor->getGseEmRM();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => $reg->cant_em
            );
        }
        echo json_encode($data);
        break;

    case 'equiposcarteraREG':
        $fecha = isset($_GET['fecha']) ? $_GET['fecha'] : date('Y-m-d');
        
        $rspta = $dcor->getEquiposCarteraREG();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => $reg->cartera
            );
        }
        echo json_encode($data);
        break;

    case 'gseEmREG':
        $fecha = isset($_GET['fecha']) ? $_GET['fecha'] : date('Y-m-d');
        
        $rspta = $dcor->getGseEmREG();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => $reg->cant_em
            );
        }
        echo json_encode($data);
        break;

    case 'selectSupervisor':
        $idregion = isset($_POST["idregion"]) ? $_POST["idregion"] : 0;
        $rspta = $dcor->selectSupervisor($idregion);
        echo '<option value="0" selected>TODOS</option>';
        while($reg = $rspta->fetch_object()){
            echo '<option value='.$reg->idsupervisor.'>'.$reg->supervisor.'</option>';
        }
        break;

    case 'selectTecnicos':
        $idregion = isset($_POST["idregion"]) ? $_POST["idregion"] : 0;
        $idsuperv = isset($_POST["idsuperv"]) ? $_POST["idsuperv"] : 0;
        $rspta = $dcor->selectTecnico($idregion,$idsuperv);
        echo '<option value="0" selected>TODOS</option>';
        while($reg = $rspta->fetch_object()){
            echo '<option value='.$reg->idtecnico.'>'.$reg->tecnico.'</option>';
        }
        break;

    case 'equiposcarteraFiltro':
        $idregion = isset($_POST["idregion"]) ? $_POST["idregion"] : 0;
        $idsuperv = isset($_POST["idsuperv"]) ? $_POST["idsuperv"] : 0;
        $idtec = isset($_POST["idtecnico"]) ? $_POST["idtecnico"] : 0;
        
        $rspta = $dcor->getEquiposCarteraFiltro($idregion,$idsuperv, $idtec);
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => $reg->cartera
            );
        }
        echo json_encode($data);
        break;

    case 'gseEmFiltro':
        $idregion = isset($_POST["idregion"]) ? $_POST["idregion"] : 0;
        $idsuperv = isset($_POST["idsuperv"]) ? $_POST["idsuperv"] : 0;
        $idtec = isset($_POST["idtecnico"]) ? $_POST["idtecnico"] : 0;
        $dias = isset($_POST["dias"]) ? $_POST["dias"] : 0;
        
        $rspta = $dcor->getGseEmFiltro($idregion,$idsuperv, $idtec,$dias);
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => $reg->cant_em
            );
        }
        echo json_encode($data);
        break;

    case 'listarguiasdetalle':
    
        $region = isset($_REQUEST['reg']) ? $_REQUEST['reg'] : 0; 
        $supervisor = isset($_REQUEST['sup']) ? $_REQUEST['sup'] : 0; 
        $tecnico = isset($_REQUEST['tec']) ? $_REQUEST['tec'] : 0; 
        
        $rspta = $dcor->listarguiasdetalle($region, $supervisor, $tecnico);
        $data = Array();
        while ($reg = $rspta->fetch_object()) {

            $op = '<button class="btn btn-info btn-xs" onclick="pdf(' . $reg->idservicio . ')" data-tooltip="tooltip" title="Descargar GSE"><i class="fa fa-file-pdf-o"></i></button>';
            
            $data[] = array(
                "0" => $op,
                "1" => $reg->idservicio,
                "2" => $reg->nombre,
                "3" => $reg->codigo
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;

    


    
}
