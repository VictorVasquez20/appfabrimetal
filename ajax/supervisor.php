<?php

require_once '../modelos/Supervisor.php';

$sup =  new Supervisor();

switch ($_GET['op']){
    case "selectsupervisor":
        $rspta = $sup->selectSupervisor();
        echo '<option value="0" selected>TODOS</option>';
        while ($reg = $rspta->fetch_object()) {
            echo '<option value=' . $reg->idsupervisor . '>' . $reg->nombre . ' ' . $reg->apellido. '</option>';
        }
        break;
}