<?php 

session_start();

require_once "../modelos/DashAsterisk.php";

$asterisk = new DashAsterisk();

    
$anio = isset($_REQUEST["anio"]) ? $_REQUEST["anio"] : "";
$mes = isset($_REQUEST["mes"]) ? $_REQUEST["mes"] : "";
$dst = "'600','9958169782','9987801300','9983568606','9989022831'";

switch ($_GET["op"]) {
        
    case 'cantidadllamadasporhora':

        $resultado = $asterisk->cantidadllamadasporhora($anio, $mes,$dst);
    
        echo json_encode($resultado);
    
    break;

    case 'cantidadllamadasporhorario':
        
		$respuesta = $asterisk->cantidadllamadasporhorario($anio, $mes, $dst);
                
                $total_findesemana=intval($respuesta['finsemana_respondido'])+intval($respuesta['finsemana_norespondido']);
		$total_nocturno=intval($respuesta['nocturna_respondido'])+intval($respuesta['nocturna_norespondido']);
                $total_normal=intval($respuesta['normal_respondido'])+intval($respuesta['normal_norespondido']);
                $total_mes=intval($total_findesemana)+intval($total_nocturno)+intval($total_normal);
                
                $total_contestadas=intval($respuesta['finsemana_respondido'])+intval($respuesta['nocturna_respondido'])+intval($respuesta['normal_respondido']);
                $total_nocontestadas=intval($respuesta['finsemana_norespondido'])+intval($respuesta['nocturna_norespondido'])+intval($respuesta['normal_norespondido']);
                        
                $porcentajefinsemana_respondido= round(($respuesta['finsemana_respondido']/$total_contestadas)*100,2);
                $porcentajenocturna_respondido=round(($respuesta['nocturna_respondido']/$total_contestadas)*100,2);
                $porcentajenormal_respondido=round(($respuesta['normal_respondido']/$total_contestadas)*100,2);
                              
                $porcentajefinsemana_norespondido=round(($respuesta['finsemana_norespondido']/$total_nocontestadas)*100,2);
                $porcentajenocturna_norespondido=round(($respuesta['nocturna_norespondido']/$total_nocontestadas)*100,2);
                $porcentajenormal_norespondido=round(($respuesta['normal_norespondido']/$total_nocontestadas)*100,2);
                
                $porcentajefinsemana=round(($total_findesemana/$total_mes)*100,2);
                $porcentajenocturno=round(($total_nocturno/$total_mes)*100,2);
                $porcentajenormal=round(($total_normal/$total_mes)*100,2);
                
                $data = Array();
                // 1 era fila
                $data[] = array(
                                "0"=>"CONTESTADAS",
                                "1"=>$respuesta['finsemana_respondido']." (".$porcentajefinsemana_respondido."%)",
                                "2"=>$respuesta['nocturna_respondido']." (".$porcentajenocturna_respondido."%)",
                                "3"=>$respuesta['normal_respondido'] ." (".$porcentajenormal_respondido."%)",
                                "4"=>intval($respuesta['finsemana_respondido'])+intval($respuesta['nocturna_respondido'])+intval($respuesta['normal_respondido']),
                        );
                
		 // 2 era fila
                 $data[] = array(
                                "0"=>"NO CONTESTADAS",
                                "1"=>$respuesta['finsemana_norespondido']." (".$porcentajefinsemana_norespondido."%)",
                                "2"=>$respuesta['nocturna_norespondido']." (".$porcentajenocturna_norespondido."%)",
                                "3"=>$respuesta['normal_norespondido'] ." (".$porcentajenormal_norespondido."%)",
                                "4"=>intval($respuesta['finsemana_norespondido'])+intval($respuesta['nocturna_norespondido'])+intval($respuesta['normal_norespondido']),
                        );
                
                 // 3 era fila
                $data[] = array(
                                "0"=>"TOTAL",
                                "1"=>$total_findesemana." (".$porcentajefinsemana."%)",
                                "2"=>$total_nocturno." (".$porcentajenocturno."%)",
                                "3"=>$total_normal." (".$porcentajenormal."%)",
                                "4"=>$total_mes
                        );
                
		$results = array(
				"sEcho"=>1,
				"iTotalRecords"=>count($data),
				"iTotalDisplayRecords"=>count($data), 
				"aaData"=>$data
			);

		echo json_encode($results);
                
		break;
    
    case 'tiempopromediollamadas':
        
		$respuesta = $asterisk->tiempollamadas($anio, $mes, $dst);
        
		$data = Array();

		 // 1 era fila
                 $data[] = array(
                                "0"=>"TIEMPO CONVERSADO",
                                 "1"=>($respuesta['promedio_conversado']),

                        );
                
                 // 2 da fila
                $data[] = array(
                                "0"=>"TIEMPO EN ESPERA",
                                "1"=>($respuesta['promedio_espera']),

                        );
                
                //3era fila.
                $data[] = array(
                          "0"=>"TIEMPO TOTAL",
                          "1"=>($respuesta['promedio_total']),
                  );
                      
		$results = array(
				"sEcho"=>1,
				"iTotalRecords"=>count($data),
				"iTotalDisplayRecords"=>count($data), 
				"aaData"=>$data
			);

		echo json_encode($results);
                
		break;
            
    case 'selectaniollamadasemergencia':
        
                $rspta = $asterisk->aniollamadasemergencia($dst);             
                echo '<option value="" selected disabled>Seleccione Año</option>';
                while($reg = $rspta->fetch_object()){
                        echo '<option value='.$reg->anio.'>'.$reg->anio.'</option>';
                }
                
    break;
    
        case 'selectmesllamadasemergencia':
        
                $rspta = $asterisk->mesllamadasemergencia($anio, $dst);             
                echo '<option value="0" selected >TODOS</option>';
                while($reg = $rspta->fetch_object()){
                        echo '<option value='.$reg->mes.'>'.$reg->nombre_mes.'</option>';
                }
                
    break;
}

 ?>