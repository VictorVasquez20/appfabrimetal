<?php
session_start();

require_once "../modelos/Llamadas.php";
require_once "../modelos/DashAsterisk.php";

require_once "../modelos/Tickets.php";

$llamadas = new Llamadas();

$asterisk = new DashAsterisk();

$tickets = new Tickets();

//datos de la llamada
$uniqueid = isset($_POST["uniqueid"]) ? limpiarCadena($_POST["uniqueid"]) : "";
$src = isset($_POST["src"]) ? limpiarCadena($_POST["src"]) : "";
$dst = isset($_POST["dst"]) ? limpiarCadena($_POST["dst"]) : "";
$calldate = isset($_POST["calldate"]) ? limpiarCadena($_POST["calldate"]) : "";
$estado = isset($_POST["estado"]) ? limpiarCadena($_POST["estado"]) : "";
$observacion=mb_strtoupper(isset($_POST["observacion"])?limpiarCadena($_POST["observacion"]):"");

//datos del formulario de ticket de emergencia
$ascensor = isset($_POST["ascensor"]) ? limpiarCadena($_POST["ascensor"]) : "";
$descripcion = mb_strtoupper(isset($_POST["descripcion"]) ? limpiarCadena($_POST["descripcion"]) : "");
$nombre = mb_strtoupper(isset($_POST["nombre"]) ? limpiarCadena($_POST["nombre"]) : "");
$correo = mb_strtoupper(isset($_POST["correo"]) ? limpiarCadena($_POST["correo"]) : "");
$telefono = isset($_POST["telefono"]) ? limpiarCadena($_POST["telefono"]) : "";

$dst = "'600','9958169782','9987801300','9983568606','9989022831'";


switch ($_GET["op"]) {
      
    case 'ListarLlamadas':
     
    //consulta en la bd de fabrimetal.    
    $resp=$llamadas->ListarLlamadasGestionadas();
        
    $cant=0;
    $arreglo= Array();
    while ($registro = $resp->fetch_object()){
        $cant++;
        $arreglo[$registro->uniqueid]=$registro->estado;  
    }
    
    //consultar en la bd de la central telefonica
    $rspta=$asterisk->listarllamadasemergenciahoy($dst);
       
    $data = Array();

    while ($reg = $rspta->fetch_object()){
        
        $estado="";  
        
        if(isset($arreglo[$reg->uniqueid])){
            if($arreglo[$reg->uniqueid]==1){
              $estado="LLAMADA GESTIONADA";   
            }elseif ($arreglo[$reg->uniqueid]==2) {
               $estado="LLAMADA RECHAZADA";       
            } 
        }else{
           $estado="SIN GESTIONAR";  
        }
    
            $data[] = array(
                            "0"=>(!isset($arreglo[$reg->uniqueid]))?
                            '<button title="Gestionar" class="btn btn-success btn-xs" data-toggle="modal" data-target="#ModalGestion" data-uniqueid="'.$reg->uniqueid.'" data-src="'.$reg->origen.'" data-dst="'.$reg->destino.'" data-calldate="'.$reg->calldate.'" data-estado="1" ><i class="fa fa-cog"></i></button>'.
                            '<button title="Rechazar" class="btn btn-danger btn-xs"   data-toggle="modal" data-target="#ModalGestion" data-uniqueid="'.$reg->uniqueid.'" data-src="'.$reg->origen.'" data-dst="'.$reg->destino.'" data-calldate="'.$reg->calldate.'" data-estado="2" ><i class="fa fa-close"></i></button>' :
                            '',
                            "1"=> $reg->origen,
                            "2"=> $reg->destino,
                            "3"=> $reg->fecha,
                            "4"=> $reg->hora,
                            "5"=> $reg->duration,
                            "6"=> $reg->conversacion,
                            "7"=> ($reg->duration - $reg->conversacion),
                            "8"=> $reg->amaflags,
                            "9"=> $reg->tipo_llamada,
                            "10"=> $estado
                    );       
    }
                  
    $results = array(
                    "sEcho"=>1,
                    "iTotalRecords"=>count($data),
                    "iTotalDisplayRecords"=>count($data), 
                    "aaData"=>$data
            );

    echo json_encode($results);

    break;
      
    case 'guardarGestion':
    
    $created_user=$_SESSION['iduser'];
     
    if($estado==1){     // crea el ticket de emergencia.
        
        $observacion = $descripcion; // guardo la misma descripcion en llamadas y en tickets.  
        
        // Crear el ticket de emergencia.    
        $idticket = $tickets->registrar($created_user, $ascensor, $nombre, $telefono, $correo, $descripcion, $uniqueid);

    }
        
    $rspta=$llamadas->guardarGestionLlamada($uniqueid,$src,$dst,$calldate,$estado,$observacion,$created_user);
 
    echo $rspta ? "Gestion de llamada Registrada " : "Gestion de llamada no pudo ser registrada ";    
    
    break;
    
}