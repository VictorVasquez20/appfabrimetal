<?php

require_once '../modelos/Modelo_izaje.php';

$modelo = new Modelo_izaje();

$idmodelo = isset($_POST['idmodelo']) ? $_POST['idmodelo'] : "";
$nombre = isset($_POST['nombre']) ? $_POST['nombre'] : "";
$idmarca = isset($_POST['idmarca']) ? $_POST['idmarca'] : "";

switch ($_GET['op']) {
    case 'guardaryeditar':
        if (!$idmodelo) {
            $rspta = $modelo->Insertar($nombre, $idmarca);
        } else {
            $rspta = $modelo->Editar($idmodelo, $nombre, $idmarca);
        }
        echo $rspta;
        break;

    case 'listar':
        $rspta = $modelo->Listar();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => '<button class="btn btn-info btn-xs" onclick="mostrar(' . $reg->idmodelo . ')"><i class="fa fa-pencil"></i></button>',
                "1" => $reg->nombre,
                "2" => $reg->marca
            );
        };
        
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;

    case 'mostrar':
        $rspta = $modelo->Mostrar($idmodelo);
        echo json_encode($rspta);
        break;

    case 'selectmodelo':
        $rspta = $modelo->selectmodelo($idmarca);
        echo '<option value="" selected disabled>SELECCIONE MODELO</option>';
        while ($reg = $rspta->fetch_object()) {
            echo '<option value=' . $reg->idmodelo . '>' . $reg->nombre . '</option>';
        }
        break;
}

 ?>
