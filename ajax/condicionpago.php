<?php
session_start();

require_once '../modelos/CondicionPago.php';

$cat = new CondicionPago();

$idcondicionpago = isset($_POST['idcondicionpago']) ? $_POST['idcondicionpago'] : "";
$nombre = isset($_POST['nombre']) ? $_POST['nombre'] : "";
$condicion = isset($_POST['condicion']) ? $_POST['condicion'] : 0;

switch ($_GET['op']) {
    case 'guardaryeditar':

        if (!$idcondicionpago) {
            $rspta = $cat->Insertar($nombre, $condicion);
            echo $rspta;
        } else {
            $rspta = $cat->Editar($idcondicionpago, $nombre, $condicion);
            echo $rspta;
        }

        break;

    case 'listar':
        $rspta = $cat->Listar();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => '<button class="btn btn-warning btn-xs" onclick="mostrar(' . $reg->idcondicionpago . ')"><i class="fa fa-pencil"></i></button>',
                "1" => $reg->nombre,
                "2" => ($reg->condicion) ? '<span class="label bg-green">Activo</span>' : '<span class="label bg-red">No activo</span>'
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;

    case 'mostrar':
        $rspta = $cat->Mostrar($idcondicionpago);
        echo json_encode($rspta);
        break;

    case 'selectcondicion':
        $rspta = $cat->SelectCategoria();
        echo '<option selected disabled>Seleccione una condición de pago</option>';
        while ($reg = $rspta->fetch_object()) {
            echo '<option value=' . $reg->idcondicionpago . '>' . $reg->nombre . '</option>';
        }
        break;
}
