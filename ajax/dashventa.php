<?php

include_once '../modelos/DashVenta.php';

$vta = new DashVenta();

$anio_busq = isset($_POST["anio_busq"]) ? limpiarCadena($_POST["anio_busq"]) : "0";
$mes_busq = isset($_POST["mes_busq"]) ? limpiarCadena($_POST["mes_busq"]) : "0";

switch($_GET['op']){
    
    case 'listar':
        
        $rspta = $vta->Listado($anio_busq,$mes_busq);
        $data = Array();
        
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "idventa"        => $reg->idventa,
                "razon_social"   => $reg->razon_social,
                "rut"            => $reg->rut,
                "calle"          => $reg->calle,
                "numero"         => $reg->numero,
                "codVenta"       => $reg->codVenta,
                "pronomb"        => $reg->pronomb,
                "procalle"       => $reg->procalle,
                "pronumero"      => $reg->pronumero,
                "montosi"        => $reg->montosi,
                "mntoFactSI"     => $reg->mntoFactSI,
                "monedaSI"       => $reg->monedaSI,
                "montosn"        => $reg->montosn,
                "mntoFactSN"     => $reg->mntoFactSN,
                "monedaSN"       => $reg->monedaSN
            );
        }
        
        echo json_encode($data);
        
        break;
        
        case "selectanioventa":
        
        $rspta = $vta->Anioventa();
        
        echo '<option value="0" selected="selected" >TODOS</option>';    
        
        while($reg = $rspta->fetch_object()){
             echo '<option value='.$reg->anio.'>'.$reg->anio.'</option>';
        }
        
        break;
        
        case "selectmesventa":
        
        $rspta = $vta->Mesventa($anio_busq);
        
        echo '<option value="0" selected="selected" >TODOS</option>';            
        
        while($reg = $rspta->fetch_object()){
             echo '<option value='.$reg->mes.'>'.$reg->nombre_mes.'</option>';
        }
        
        
        break;
}