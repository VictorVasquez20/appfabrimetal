<?php
session_start();

require_once '../modelos/FacturaServicio.php';
require_once '../modelos/Documento_servicio.php';

$FS = new FacturaServicio();
$documento = new Documento_servicio();

$idfactura = isset($_POST['idfactura']) ? $_POST['idfactura'] : 0;
$nrofactura = isset($_POST['nrofactura']) ? $_POST['nrofactura'] : 0;
$fecha = isset($_POST['fecha']) ? $_POST['fecha'] : "";
$estado = isset($_POST['estado']) ? $_POST['estado'] : 0;
$monto = isset($_POST['monto']) ? $_POST['monto'] : 0;
$created_user = isset($_POST['created_user']) ? $_POST['created_user'] : "";
$closed_user = isset($_POST['closed_user']) ? $_POST['closed_user'] : "";
$condicion = isset($_POST['condicion']) ? $_POST['condicion'] : 0;

switch ($_GET["op"]){
    case "guardaryeditar":
        $created_user = $_SESSION["iduser"];
        $closed_user = $_SESSION["iduser"];
        if(!$idfactura){
            $rspta = $FS->Insrtar($nrofactura, $fecha, $monto, $created_user);
            echo $rspta;
        }else{
            if($estado != 0){
                $rspta = $FS->Editar($idfactura, $nrofactura, $estado, $closed_user, $condicion);
                echo $rspta;
            }
        }
        break;
        
    case "listar":
        $ano = isset($_REQUEST['ano']) ? $_REQUEST['ano'] : 0; 
        $mes = isset($_REQUEST['mes']) ? $_REQUEST['mes'] : 0; 
        $estado = isset($_REQUEST['estado']) ? $_REQUEST['estado'] : 0; 
        
        $rspta = $FS->Listar($ano, $mes, $estado);
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            
            if($reg->estado == 0){
                $st = "<button class='btn btn-primary btn-xs'>Creada</button>";
            }elseif($reg->estado == 1){
                $st = "<button class='btn btn-success btn-xs'>Pagada</button>";
            }elseif($reg->estado == 2){
                $st = "<button class='btn btn-danger btn-xs'>Anulada</button>";
            }
            $data[] = array(
                "0" => '<button class="btn btn-info btn-xs" onclick="mostrar(' . $reg->idfactura . ')" data-toggle="tooltip" data-placement="top" title="Revisar"><i class="fa fa-eye"></i></button>',
                "1" => $reg->nrofactura,
                "2" => date_format( date_create($reg->fecha), 'd-m-Y'),
                "3" => $reg->monto,
                "4" => $reg->nombre,
                "5" => $st
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
        
    case "mostrar":
        $rspta = $FS->Mostrar($idfactura);
        echo json_encode($rspta);
        break;
    
    case "listarGSExFactura":
        $rspta = $FS->ListarGSExFactura($idfactura);
        $data = Array();
        $i = 0;
        while ($reg = $rspta->fetch_object()) {
            $hiden ='<input type="hidden" name="id[]" id="id_' . $i . '__idservicio" value="'. $reg->idservicio .'">';
            
            $data[] = array(
                "0" => $hiden.$reg->idservicio,
                "1" => $reg->tipostr,
                "2" => $reg->codigo,
                "3" => $reg->nombre,
                "4" => $reg->strmes,
                "5" => $reg->fecha,
                "6" => $reg->rut,
                "7" => substr($reg->razon_social, 0, 30)." ..."
            );
            $i++;
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
    
    case "listarGSE":
        
        $tservicio = isset($_REQUEST['tservicio']) ? $_REQUEST['tservicio'] : 0; 
        $ano = isset($_REQUEST['ano']) ? $_REQUEST['ano'] : 0; 
        $mes = isset($_REQUEST['mes']) ? $_REQUEST['mes'] : 0; 
        $cc = isset($_REQUEST['cc']) ? $_REQUEST['cc'] : 0;      
        
        $rspta = $FS->ListarGSE($tservicio, $ano, $mes, $cc);
        
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $arch  = "";

            $doc = $documento->Listar($reg->idservicio);        
            while ($do = $doc->fetch_object()) {
                $arch.= "<a  target='_blank' data-toggle='tooltip' data-placement='top' title='".utf8_encode($do->nombre)."' href='../files/servicio/". $do->archivo ."'><i class='fa fa-file-pdf-o'></i></a> ";
            }
             
            $infopros = '"'.$reg->infopro.'"';
            $data[] = array(
                "0" => '<button class="btn btn-danger btn-xs" onclick="rechazar(' . $reg->idservicio . ')" data-toggle="tooltip" data-placement="top" title="devolver a revisión"><i class="fa fa-mail-reply"></i></button>'
                     . '<button class="btn btn-info btn-xs" onclick="pdf(' . $reg->idservicio . ')" data-toggle="tooltip" data-placement="top" title="PDF"><i class="fa fa-file-pdf-o"></i></button>'
                     . ($reg->infopro ? "<button class='btn btn-success btn-xs' onclick='modalComentario($infopros);'><i class='fa fa-comment'></i></button>" : ""),
                "1" => '<input type="checkbox" name="table_records" onclick="checar();" class="flat" value="' . $reg->idservicio . '">',
                "2" => $reg->idservicio,
                "3" => $reg->tipostr,
                "4" => $reg->codigo,
                "5" => utf8_encode($reg->nombre),
                "6" => utf8_encode($reg->strmes),
                "7" => $reg->fecha,
                "8" => $reg->rut,
                "9" => utf8_encode(substr($reg->razon_social, 0, 30)." ..."),
                "10" => $arch,
                "11" => utf8_encode($reg->ccnombre)
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );
                   
        echo json_encode($results);
        break;
     
    case "UpdateGSE":
        $stFacturacion = isset($_POST["stFacturacion"]) ? $_POST["stFacturacion"] : 0;
        $idservicio = isset($_POST["idservicio"]) ? $_POST["idservicio"] : 0;
        $infodev = isset($_POST["infodev"]) ? strtoupper($_POST["infodev"]) : 0;
        
        $rspta = $FS->UpdateGSE($idservicio, $stFacturacion, $idfactura, $infodev);
        
        if($rspta){
            $link = "../files/servicio/";
            $doc = $documento->Listar($idservicio);
            
            while ($do = $doc->fetch_object()) {
                unlink($link . $do->archivo);
            }
            
            $docDel = $documento->EliminarTodos($idservicio);
        }
        
        echo $rspta;
        break;
    
     case "selectanoFS":
        $rspta = $FS->selectano();
        echo '<option value="0" selected>TODOS</option>';
        while($reg = $rspta->fetch_object()){
             echo '<option value='.$reg->ano.'>'.$reg->ano.'</option>';
        }
        break;
    
    case "selectano":
        $rspta = $FS->SelectanoGSE();
        echo '<option value="0" selected>TODOS</option>';
        while($reg = $rspta->fetch_object()){
             echo '<option value='.$reg->ano.'>'.$reg->ano.'</option>';
        }
        break;
     
    case 'selecttservicio':
        $rspta = $FS->selecttservicioGSE();
        echo '<option value="0" selected>TODOS</option>';
        while($reg = $rspta->fetch_object()){
                echo '<option value='.$reg->idtservicio.'>'.$reg->nombre.'</option>';
        }
        break;
}