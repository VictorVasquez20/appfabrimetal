<?php
require_once '../modelos/Equipos_Izaje.php';

$equipo = new Equipos_Izaje();

$idequipo = isset($_POST['idequipo']) ? $_POST['idequipo'] : "";
$numero = isset($_POST['numero']) ? $_POST['numero'] : "";
$serie = isset($_POST['serie']) ? $_POST['serie'] : "";
$idmarca = isset($_POST['idmarca']) ? $_POST['idmarca'] : "";
$idmodelo = isset($_POST['idmodelo']) ? $_POST['idmodelo'] : "";
$capacidad = isset($_POST['capacidad']) ? $_POST['capacidad'] : "";
$velocidad = isset($_POST['velocidad']) ? $_POST['velocidad'] : "";
$estado = isset($_POST['estado']) ? $_POST['estado'] : "";

switch ($_GET['op']){
    case 'guardaryeditar':
        if(!$idequipo){
            $rspta = $equipo->Insetrar($numero, $serie, $idmarca, $idmodelo, $capacidad, $velocidad);
        }else{
            $rspta = $equipo->Editar($idequipo, $numero, $serie, $idmarca, $idmodelo, $capacidad, $velocidad, $estado);
        }
        echo $rspta;
        break;
    
    case 'listar':
        $rspta = $equipo->Listar();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $st = "";
            
            switch ($reg->estado){
                case '0':
                    $st = '<span class="label label-warning">EN REPARACION</span>';
                    break;
                case '1':
                    $st = '<span class="label label-success">DISPONIBLE</span>';
                    break;
                case '2':
                    $st = '<span class="label label-info">EN USO</span>';
                    break;
                case '3':
                    $st = '<span class="label label-danger">EN REPARACION</span>';
                    break;
            }
            
            $data[] = array(
                "0" => '<button class="btn btn-info btn-xs" onclick="mostrar(' . $reg->idequipo . ')"><i class="fa fa-pencil"></i></button>',
                "1" => $reg->numero,
                "2" => $reg->serie,
                "3" => $reg->marca,
                "4" => $reg->modelo,
                "5" => $reg->capacidad,
                "6" => $st
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
    
    case 'mostrar':
        $rspta = $equipo->Mostrar($idequipo);
        echo json_encode($rspta);
        break;
}

