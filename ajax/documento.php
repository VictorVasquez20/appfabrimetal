<?php

require_once '../modelos/Documento.php';

$doc = new Documento();

$iddocumento = isset($_POST['iddocumento']) ? $_POST['iddocumento'] : "";
$descripcion = isset($_POST['descripcion']) ? $_POST['descripcion'] : "";
$idproveedor = isset($_POST['idproveedor']) ? $_POST['idproveedor'] : "";
$condicion = isset($_POST['condicion']) ? $_POST['condicion'] : "";
$nombre = isset($_POST['nombre']) ? $_POST['nombre'] : "";


switch ($_GET['op']) {
    case 'guardaryeditar':
        if (!$iddocumento) {
            $ext = explode(".", $_FILES['docto']['name']);
            if ($_FILES['docto']['type'] == "application/pdf" || $ext[1] == 'pdf' || $ext[1] == 'PDF') {
                //var_dump($ext[1]);
                
                if ($_FILES["docto"]["size"] <= 3000000) {
                    $link = "../files/proveedor/". $idproveedor; //round(microtime(true)) . "." . end($ext);
                    $archivo =  $_FILES['docto']['name'];

                    if(!file_exists($link)){
                        mkdir($link, 0777, true );
                    }
                    move_uploaded_file($_FILES['docto']['tmp_name'], $link.'/'.$archivo);
                } else {
                    $rspta = "el tamaño del archivo no puede ser mayor a 3mb.";
                    echo $rspta;
                    break;
                }
            } else {
                $rspta = "El archivo debe ser pdf";
                echo $rspta;
                break;
            }
                        
            $rspta = $doc->Insertar($archivo, $descripcion, $idproveedor, $condicion);
            echo $rspta;
            break;
        } else {
            $rspta = $doc->Editar($iddocumento ,$nombre ,$descripcion ,$idproveedor ,$condicion);
            echo $rspta;
            break;
        }
        

    case 'listar':
        $rspta = $doc->Listar($idproveedor);
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => $reg->iddocumento,
                "1" => $reg->descripcion,
                "2" => $reg->idproveedor,
                "3" => $reg->nombre
            );
        }
        /*$results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );*/

        echo json_encode($data);
        break;
        
    case 'eliminar':
        if($iddocumento != 0){
            $nombre = isset($_POST["nombre"]) ? $_POST["nombre"] : "";
            $rspta = $doc->Eliminar($iddocumento);
            if($rspta != 0){
                $link = "../files/proveedor/". $idproveedor.'/'.$nombre;
                if(file_exists($link)){
                    unlink($link);
                }
            }
            echo $rspta;
        }
        break;
}