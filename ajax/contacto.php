<?php
session_start();

require_once '../modelos/Contacto.php';

$contactoedif = new Contacto();

$idcontacto = isset($_POST["idcontacto"]) ? limpiarCadena($_POST["idcontacto"]) : "";
$idedificio_contacto = isset($_POST["idedificio_contacto"]) ? limpiarCadena($_POST["idedificio_contacto"]) : "";
$nombre_contacto=mb_strtoupper(isset($_POST["nombre_contacto"])?limpiarCadena($_POST["nombre_contacto"]):"");
$cargo_contacto =mb_strtoupper(isset($_POST["cargo_contacto"]) ? limpiarCadena($_POST["cargo_contacto"]) : "");
$email_contacto =mb_strtoupper(isset($_POST["email_contacto"]) ? limpiarCadena($_POST["email_contacto"]) : "");
$telefonomovil_contacto = isset($_POST["telefonomovil_contacto"]) ? limpiarCadena($_POST["telefonomovil_contacto"]) : "";
$telefonoresi_contacto = isset($_POST["telefonoresi_contacto"]) ? limpiarCadena($_POST["telefonoresi_contacto"]) : "";

switch ($_GET["op"]) {
    
    case 'guardaryeditar':
            $estatus=1;
            $mensaje="";
		if(empty($idcontacto)){
			$rspta=$contactoedif->insertar_contacto($nombre_contacto,$cargo_contacto,$email_contacto,$telefonomovil_contacto,$telefonoresi_contacto,$idedificio_contacto);
                        if($rspta){
                           $estatus=1;
                           $mensaje="Contacto Registrado"; 
                        }else{
                           $estatus=0;
                           $mensaje="Contacto No pudo ser Registrado";  
                        }                        
		}
		else{
			$rspta=$contactoedif->editar_contacto($idcontacto,$nombre_contacto,$cargo_contacto,$email_contacto,$telefonomovil_contacto,$telefonoresi_contacto);
			if($rspta){
                           $estatus=1;
                           $mensaje="Contacto Editado"; 
                        }else{
                           $estatus=0;
                           $mensaje="Contacto No pudo ser Editado";  
                        } 
		}
                
                echo json_encode(array("estatus"=>$estatus,"mensaje"=>$mensaje));
                
    break;
    
    case 'listar_contactos':
        
        $rspta = $contactoedif->listar_contactos($idedificio_contacto,$_SESSION['administrador']);
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0"=>($reg->condicion)?
					'<button class="btn btn-info btn-xs" data-tooltip="tooltip" title="Modificar" data-toggle="modal" data-target="#modalFormContacto" data-idcontacto="'.$reg->idcontacto.'"><i class="fa fa-pencil"></i></button>'.
					'<button class="btn btn-danger btn-xs" onclick="desactivar_contacto('.$reg->idcontacto.')"><i class="fa fa-close"></i></button>':
					'<button class="btn btn-info btn-xs" data-tooltip="tooltip" title="Modificar" data-toggle="modal" data-target="#modalFormContacto" data-idcontacto="'.$reg->idcontacto.'"><i class="fa fa-pencil"></i></button>'.
					'<button class="btn btn-primary btn-xs" onclick="activar_contacto('.$reg->idcontacto.')"><i class="fa fa-check"></i></button>',                
                "1" => $reg->nombre,
                "2" => $reg->cargo,
                "3" => $reg->email,
                "4" => $reg->telefonomovil,
                "5" => $reg->telefonoresi,
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );
        echo json_encode($results);
        
            
     break;  
     
    case 'desactivar_contacto':
        
	$rspta=$contactoedif->desactivar_contacto($idcontacto);
        
	echo $rspta ? "Contacto Eliminado" : "El Contacto no se puede eliminar.";
        
    break;

    case 'activar_contacto':
        
	$rspta=$contactoedif->activar_contacto($idcontacto);
        
	echo $rspta ? "Contacto Activado" : "El Contacto no se puede activar.";
        
    break;    

    case 'mostrarContacto':
        
        $rspta = $contactoedif->mostrarContacto($idcontacto);
        
        echo json_encode($rspta);
        
    break;

}
?>

