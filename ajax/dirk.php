<?php

require_once "../modelos/Dirk.php";

$dirk = new Dirk();

$iddirk = isset($_POST["iddirk"]) ? limpiarCadena($_POST["iddirk"]) : "";
$idedificio = isset($_POST["idedificio"]) ? limpiarCadena($_POST["idedificio"]) : "";
$codigo=mb_strtoupper(isset($_POST["codigo"])?limpiarCadena($_POST["codigo"]):"");
$ubicacion=mb_strtoupper(isset($_POST["ubicacion"])?limpiarCadena($_POST["ubicacion"]):"");
        
switch ($_GET["op"]) {

  case 'listar':
      
    $rspta=$dirk->listar();
    
    $data = Array();

    while ($reg = $rspta->fetch_object()){
        
    $data[] = array(
        "0"=>'<button class="btn btn-warning btn-xs" onclick="mostrar('.$reg->iddirk.')"><i class="fa fa-pencil"></i></button>',
        "1"=>$reg->codigo,
        "2"=>$reg->ubicacion,
        "3"=>$reg->nombre_edificio
        );
    
    }
    
    $results = array(
        "sEcho"=>1,
        "iTotalRecords"=>count($data),
        "iTotalDisplayRecords"=>count($data), 
        "aaData"=>$data
    );

    echo json_encode($results);
    
    break;
    
    case 'mostrar':
	
        $rspta=$dirk->mostrar($iddirk);
	
        echo json_encode($rspta);
	
    break;
            
    case 'selectDirk':
    
    echo '<option value="" selected disabled>Seleccione M&oacute;dulo</option>';
    
    $rspta=$dirk->listar();  
    
    while($reg = $rspta->fetch_object()){
            echo '<option value='.$reg->iddirk.'>'.$reg->codigo.' - '.$reg->ubicacion.'</option>';
    }
    
    break;
    
    case 'guardar':
          
        if(empty($iddirk)){
                
        $rspta=$dirk->insertar($idedificio,$codigo,$ubicacion);
                  
        if($rspta){
             $estatus=1;
             $mensaje="Modulo registrado";
        }else{
            $estatus=0;
            $mensaje="Modulo no se registro";
        }
         
        echo json_encode(array('estatus'=>$estatus,'mensaje'=>$mensaje));
                 
        }

    break; 
        
    case 'actualizarDirk':

         $rspta=$dirk->actualizarDirk($iddirk,$idedificio);

         if($rspta){
             $estatus=1;
             $mensaje="Modulo Actualizado";
         }else{
             $estatus=0;
             $mensaje="Modulo no se pudo Actualizar";
         }
         
         echo json_encode(array('estatus'=>$estatus,'mensaje'=>$mensaje));
 
    break;                    
}

