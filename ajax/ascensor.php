<?php 
session_start();
require_once "../modelos/Ascensor.php";

$ascensor = new Ascensor();

//Datos desde el formulario - Editar ascensor
$idventa = isset($_POST["idventa"]) ? limpiarCadena($_POST["idventa"]) : "";
$idascensor = isset($_POST["idascensor"]) ? limpiarCadena($_POST["idascensor"]) : "";
$idtascensor = isset($_POST["idtascensor"]) ? limpiarCadena($_POST["idtascensor"]) : "";
$marca = isset($_POST["marca"]) ? limpiarCadena($_POST["marca"]) : "";
$modelo = isset($_POST["modelo"]) ? limpiarCadena($_POST["modelo"]) : "";
$ken = isset($_POST["ken"]) ? limpiarCadena($_POST["ken"]) : "";
$pservicio = isset($_POST["pservicio"]) ? limpiarCadena($_POST["pservicio"]) : "";
$gtecnica = isset($_POST["gtecnica"]) ? limpiarCadena($_POST["gtecnica"]) : "";
$valoruf = isset($_POST["valoruf"]) ? limpiarCadena($_POST["valoruf"]) : "";
$valorclp = isset($_POST["valorclp"]) ? limpiarCadena($_POST["valorclp"]) : "";
$comando = isset($_POST["comando"]) ? limpiarCadena($_POST["comando"]) : "";
$paradas = isset($_POST["paradas"]) ? limpiarCadena($_POST["paradas"]) : "";
$accesos = isset($_POST["accesos"]) ? limpiarCadena($_POST["accesos"]) : "";
$capkg = isset($_POST["capkg"]) ? limpiarCadena($_POST["capkg"]) : "";
$capper = isset($_POST["capper"]) ? limpiarCadena($_POST["capper"]) : "";
$velocidad = isset($_POST["velocidad"]) ? limpiarCadena($_POST["velocidad"]) : "";
$dcs = isset($_POST["dcs"]) ? limpiarCadena($_POST["dcs"]) : "";
$elink = isset($_POST["elink"]) ? limpiarCadena($_POST["elink"]) : "";
$codigo = isset($_POST["codigo"]) ? limpiarCadena($_POST["codigo"]) : "";
$idedificio = isset($_POST["idedificio"]) ? limpiarCadena($_POST["idedificio"]) : NULL;
$idcontrato = isset($_POST["idcontrato"]) ? limpiarCadena($_POST["idcontrato"]) : NULL;
$ubicacion = isset($_POST["ubicacion"]) ? limpiarCadena($_POST["ubicacion"]) : "";
$codigocli = isset($_POST["codigocli"]) ? limpiarCadena($_POST["codigocli"]) : "";
$lat = isset($_POST["lat"]) ? limpiarCadena($_POST["lat"]) : "";
$lon = isset($_POST["lon"]) ? limpiarCadena($_POST["lon"]) : "";
$iddirk = isset($_POST["iddirk"]) ? limpiarCadena($_POST["iddirk"]) : "";

$montosi = isset($_POST['montosi']) ? $_POST['montosi'] : 0;
$montosn = isset($_POST['montosn']) ? $_POST['montosn'] : 0;
        
switch ($_GET["op"]) {
	
        case 'solid_edifid':
            $idcontrato = $_GET["id"]; 
            $rspta=$ascensor->solid_ascid($idcontrato);
            $ascensores = Array();
		while ($reg = $rspta->fetch_object()){
			$ascensores[] = array(
					"0"=>$reg->idascensor,
					"1"=>$reg->nombre.", ".$reg->calle." ".$reg->numero,
					"2"=>$reg->region_ordinal,
					"3"=>$reg->marca,
					"4"=>$reg->modelo
				);
		}

		$results = array(
				"ascensores"=>$ascensores,
				"nascensores"=>count($ascensores)
			);
                
		echo json_encode($results);
		break;


	case 'listarsoid':
	    $rspta=$contrato->listarsolid();
		$data = Array();
		while ($reg = $rspta->fetch_object()){
			$data[] = array(
					"0"=>'<button class="btn btn-info btn-xs" onclick="mostar('.$reg->idcontrato.')" data-tooltip="tooltip" title="Asignar IDs" ><i class="fa fa-hashtag"></i></button>',
					"1"=>$reg->ncontrato,
					"2"=>$reg->fecha,
					"3"=>$reg->region_nombre.' - '.$reg->region_ordinal,
					"4"=>$reg->nedificios,
					"5"=>$reg->nascensores
				);
		}
		$results = array(
				"sEcho"=>1,
				"iTotalRecords"=>count($data),
				"iTotalDisplayRecords"=>count($data), 
				"aaData"=>$data
			);

		echo json_encode($results);
		break;
        
        case 'InsertarIds':
            $idascensor = $_GET["id"]; 
            $iduser=$_SESSION['iduser'];
            $idascensores = $_POST['idascensor'];
            $codigos = $_POST['codigo'];
            foreach( $idascensores as $index => $idascensor ) {
                $ascensor->InsertarIds($idascensor, $codigos[$index], $iduser);
            }
            echo "Identificadores registrados";

        break;
        
        case 'listarascensor':
            $rspta = $ascensor->listar();
            $data = Array();
            while ($reg = $rspta->fetch_object()) {
                if ($reg->condicion){
                    $op =  '<button class="btn btn-info btn-xs" onclick="mostrar(' . $reg->idascensor . ')"><i class="fa fa-list-alt"></i></button>'
                         . '<button class="btn btn-info btn-xs" onclick="editar(' . $reg->idascensor . ')"><i class="fa fa-pencil"></i></button>'
                         . ' <button class="btn btn-danger btn-xs" onclick="cambiarcondicion('.$reg->idascensor.', 0)"><i class="fa fa-close"></i></button>';
                }else{
                    $op =  '<button class="btn btn-info btn-xs" onclick="mostrar(' . $reg->idascensor . ')"><i class="fa fa-list-alt"></i></button>'
                         . '<button class="btn btn-info btn-xs" onclick="editar(' . $reg->idascensor . ')"><i class="fa fa-pencil"></i></button>'
                         . ' <button class="btn btn-primary btn-xs" onclick="cambiarcondicion('.$reg->idascensor.', 1)"><i class="fa fa-check"></i></button>';
                }
                
                $data[] = array(
                    "0" => $op,
                    "1" => $reg->codigo,
                    "2" => $reg->edificio,
                    "3" => $reg->contrato,
                    "4" => $reg->condicion == 1 ? '<h5><span class="label label-success">SI</span></h5>': '<span class="label label-danger">NO</span>',
                );
            }
            $results = array(
                "sEcho" => 1,
                "iTotalRecords" => count($data),
                "iTotalDisplayRecords" => count($data),
                "aaData" => $data
            );

            echo json_encode($results);
            break;
        
        
        case 'editar':
            $iduser = $_SESSION['iduser'];
            if (!empty($idascensor)) {
                $rspta = $ascensor->editar($idascensor, $idtascensor, $marca, $modelo, $ken, $pservicio, $gtecnica, $valoruf, $valorclp, $paradas, $capkg, $capper, $velocidad, $dcs, $elink, $iduser);
                echo $rspta ? "Ascensor editado" : "Ascensor no pudo ser editado";
            }
            break;
        
        case 'mostrar':
            $dascensor = $ascensor->mostrar($idascensor);

            if ($dascensor['dcs'] = 1) {
                $dascensor['dcs'] = "SI";
            } else {
                $dascensor['dcs'] = "NO";
            }

            if ($dascensor['elink'] = 1) {
                $dascensor['elink'] = "SI";
            } else {
                $dascensor['elink'] = "NO";
            }

            if (is_null($dascensor['codigo'])) {
                $dascensor['codigo'] = "S/C";
            }

            if (is_null($dascensor['ken'])) {
                $dascensor['ken'] = "S/C";
            }

            if (is_null($dascensor['valorclp'])) {
                $dascensor['valorclp'] = "-";
            }

            if (is_null($dascensor['paradas'])) {
                $dascensor['paradas'] = "-";
            }

            if (is_null($dascensor['capper'])) {
                $dascensor['capper'] = "-";
            }

            if (is_null($dascensor['capkg'])) {
                $dascensor['capkg'] = "-";
            }

            if (is_null($dascensor['velocidad'])) {
                $dascensor['velocidad'] = "-";
            }

            if (is_null($dascensor['gtecnica'])) {
                $dascensor['gtecnica'] = "S/F";
            }

            if (is_null($dascensor['pservicio'])) {
                $dascensor['pservicio'] = "S/F";
            }

            echo json_encode($dascensor);
            break;

        case 'formeditar':
            $rspta = $ascensor->formeditar($idascensor);
            echo json_encode($rspta);
            break;

        case 'dguia':
            $rspta = $ascensor->guia($codigo);
            echo json_encode($rspta);
            break;
        
        case 'selectasc':
                        $idedificio = isset($_POST["idedificio"]) ? limpiarCadena($_POST["idedificio"]) : "";
			$rspta = $ascensor->SelectAsc($idedificio);
                        echo '<option value="" selected disabled>Seleccione Ascensor</option>';
			while($reg = $rspta->fetch_object()){
				echo '<option value='.$reg->idascensor.'>'.$reg->tascensor.' '.$reg->marca.' / '.$reg->codigo.' / '.$reg->ubicacion.' '.$reg->codigocli.' / '.$reg->ken.'</option>';
			}
            break;
            
        case 'selectascensores':
                        
                    $rspta = $ascensor->SelectAscensores();
                    echo '<option value="" selected disabled>Seleccione Ascensor</option>';
                    while($reg = $rspta->fetch_object()){
                        echo '<option value='.$reg->idascensor.'>'.$reg->tascensor.' '.$reg->marca.' / '.$reg->codigo.' / '.$reg->ubicacion.' '.$reg->codigocli.' / '.$reg->ken.'</option>';
                    }
            break;
        
        case 'VerificarCodigo':
            $rspta = $ascensor->VerificarCodigo($codigo);
            echo $rspta;
            break;
        
        case 'VerificarKEN':
            $rspta = $ascensor->VerificarKEN($ken);
            echo $rspta;
            break;
        
        case 'listarventa':
            
            $rspta = $ascensor->listarpro($idventa);
            $data = Array();
            $counAscensor = 0;
            while ($reg = $rspta->fetch_object()) {
                
                $hidden = "<input type='hidden' name='ascensor[]' id='ascensor_". $counAscensor ."__idascensor' value='" .$reg->idascensor. "' />" .
                    "<input type='hidden' name='ascensor[].codigo' id='ascensor_". $counAscensor ."__codigo' value='" . $reg->codigo . "' />" .
                    "<input type='hidden' name='ascensor[].idascensor' id='ascensor_". $counAscensor ."__idtascensor' value='" .$reg->idtascensor. "' />" .
                    "<input type='hidden' name='ascensor[].marca' id='ascensor_". $counAscensor ."__marca' value='". $reg->marca ."' />" .
                    "<input type='hidden' name='ascensor[].modelo' id='ascensor_". $counAscensor ."__modelo' value='". $reg->modelo."' />" .
                    "<input type='hidden' name='ascensor[].ken' id='ascensor_". $counAscensor ."__ken' value='" .$reg->ken . "' />" .
                    "<input type='hidden' name='ascensor[].pservicio' id='ascensor_". $counAscensor ."__pservicio' value='" .$reg->pservicio . "' />" .
                    "<input type='hidden' name='ascensor[].gtecnica' id='ascensor_". $counAscensor ."__gtecnica' value='" .$reg->gtecnica . "' />" .
                    "<input type='hidden' name='ascensor[].valoruf' id='ascensor_". $counAscensor ."__valoruf' value='" .$reg->valoruf . "' />" .
                    "<input type='hidden' name='ascensor[].valorcl' id='ascensor_". $counAscensor ."__valorclp' value='" .$reg->valorclp . "' />" .
                    "<input type='hidden' name='ascensor[].paradas' id='ascensor_". $counAscensor ."__paradas' value='" .$reg->paradas . "' />" .
                    "<input type='hidden' name='ascensor[].capkg' id='ascensor_". $counAscensor ."__capkg' value='" .$reg->capkg . "' />" .
                    "<input type='hidden' name='ascensor[].capper' id='ascensor_". $counAscensor ."__capper' value='" .$reg->capper . "' />" .
                    "<input type='hidden' name='ascensor[].velocidad' id='ascensor_". $counAscensor ."__velocidad' value='" .$reg->velocidad . "' />" .
                    "<input type='hidden' name='ascensor[].dcs' id='ascensor_". $counAscensor ."__dcs' value='" .$reg->dcs . "' />" .
                    "<input type='hidden' name='ascensor[].montosi' id='ascensor_". $counAscensor ."__montosi' value='" . (is_null($reg->montosi) ? 0 : $reg->montosi) . "' />" .
                    "<input type='hidden' name='ascensor[].montosn' id='ascensor_". $counAscensor ."__montosn' value='" . (is_null($reg->montosn) ? 0 : $reg->montosn) . "' />" .
                    "<input type='hidden' name='ascensor[].elink' id='ascensor_". $counAscensor ."__elink' value='" .$reg->elink . "' /> ";
                
                $data[] = array(
                    "0" => $counAscensor + 1,
                    "1" => $hidden. $reg->tiponomb,
                    "2" => $reg->strmarca. " - ". $reg->strmodelo ,
                    "3" => $reg->comando,
                    "4" => $reg->codigo,
                    "5" => $reg->ken,
                    "6" => $reg->paradas,
                    "7" => $reg->capkg,
                    "8" => $reg->velocidad,
                    "9" => is_null($reg->montosi) ? 0 : $reg->montosi,
                    "10" => is_null($reg->montosn) ? 0 : $reg->montosn,
                    "11" => "<i class='fa fa-trash' onclick='delasc(".$reg->idascensor.");'></i>"
                );
                
                $counAscensor+= 1;
            }
            /*$results = array(
                "sEcho" => 1,
                "iTotalRecords" => count($data),
                "iTotalDisplayRecords" => count($data),
                "aaData" => $data
            );*/

            echo json_encode($data);
            
            break;
            
        case 'listarxedificio':
            $idedificio = isset($_POST["idedificio"]) ? limpiarCadena($_POST["idedificio"]) : "";
            $idcontrato = isset($_POST["idcontrato"]) ? limpiarCadena($_POST["idcontrato"]) : "";
            
            $rspta = $ascensor->listarxedificio($idedificio, $idcontrato);
            $data = Array();
            $counAscensor = 0;
            while ($reg = $rspta->fetch_object()) {
                
                $hidden = "<input type='hidden' name='ascensor[]' id='ascensor_". $counAscensor ."__idascensor' value='" .$reg->idascensor. "' />" .
                    "<input type='hidden' name='ascensor[].codigo' id='ascensor_". $counAscensor ."__codigo' value='" . $reg->codigo . "' />" .
                    "<input type='hidden' name='ascensor[].idascensor' id='ascensor_". $counAscensor ."__idtascensor' value='" .$reg->idtascensor. "' />" .
                    "<input type='hidden' name='ascensor[].marca' id='ascensor_". $counAscensor ."__marca' value='". $reg->marca ."' />" .
                    "<input type='hidden' name='ascensor[].modelo' id='ascensor_". $counAscensor ."__modelo' value='". $reg->modelo."' />" .
                    "<input type='hidden' name='ascensor[].ken' id='ascensor_". $counAscensor ."__ken' value='" .$reg->ken . "' />" .
                    "<input type='hidden' name='ascensor[].pservicio' id='ascensor_". $counAscensor ."__pservicio' value='" .$reg->pservicio . "' />" .
                    "<input type='hidden' name='ascensor[].gtecnica' id='ascensor_". $counAscensor ."__gtecnica' value='" .$reg->gtecnica . "' />" .
                    "<input type='hidden' name='ascensor[].valoruf' id='ascensor_". $counAscensor ."__valoruf' value='" .$reg->valoruf . "' />" .
                    "<input type='hidden' name='ascensor[].valorcl' id='ascensor_". $counAscensor ."__valorclp' value='" .$reg->valorclp . "' />" .
                    "<input type='hidden' name='ascensor[].paradas' id='ascensor_". $counAscensor ."__paradas' value='" .$reg->paradas . "' />" .
                    "<input type='hidden' name='ascensor[].capkg' id='ascensor_". $counAscensor ."__capkg' value='" .$reg->capkg . "' />" .
                    "<input type='hidden' name='ascensor[].capper' id='ascensor_". $counAscensor ."__capper' value='" .$reg->capper . "' />" .
                    "<input type='hidden' name='ascensor[].velocidad' id='ascensor_". $counAscensor ."__velocidad' value='" .$reg->velocidad . "' />" .
                    "<input type='hidden' name='ascensor[].dcs' id='ascensor_". $counAscensor ."__dcs' value='" .$reg->dcs . "' />" .
                    "<input type='hidden' name='ascensor[].elink' id='ascensor_". $counAscensor ."__elink' value='" .$reg->elink . "' /> ";
                $data[] = array(
                    "0" => $counAscensor + 1,
                    "1" => $hidden. $reg->tiponomb,
                    "2" => $reg->strmarca,
                    "3" => $reg->codigo,
                    "4" => $reg->paradas,
                    "5" => $reg->capkg,
                    "6" => $reg->velocidad,
                    "7" => "<i class='fa fa-trash' onclick='delasc(".$reg->idascensor.");'></i>"
                );
                
                $counAscensor+= 1;
            }
            /*$results = array(
                "sEcho" => 1,
                "iTotalRecords" => count($data),
                "iTotalDisplayRecords" => count($data),
                "aaData" => $data
            );*/

            echo json_encode($data);
            
            break;
            
        case 'guardaryeditar':
            $created_user = $_SESSION['iduser'];
            $updated_user = $_SESSION['iduser'];
            $estadoins = 1;
            $ubicacion = empty($ubicacion) ? "N/S" : $ubicacion;
            $codigocli = empty($codigocli) ? "N/S" : $codigocli;
            $ken = empty($ken) ? "N/S" : $ken;
            $pservicio = empty($pservicio) ? "0000-00-00" : $pservicio;
            $gtecnica = empty($gtecnica) ? "0000-00-00" : $gtecnica;
            $comando = empty($comando) ? "N/S" : $comando;
            $accesos = empty($accesos) ? $paradas : $accesos;
            
            if(!$idascensor){
                $rspta = $ascensor->InsertarAscensorNUEVO($idedificio, $idcontrato, $idventa, $idtascensor, $marca, $modelo, $ubicacion, $codigo, 
                                                    $codigocli, $ken, $paradas, $capper, $capkg, $velocidad, $dcs, $elink, $pservicio, 
                                                    $gtecnica, $created_user, $estadoins, $montosi, $montosn, $comando, $accesos);
                echo $rspta;
            }else{
                $rspta = $ascensor->editarNUEVO($idascensor, $idventa, $montosi, $montosn, $updated_user);
                echo $rspta;
            }
            break;
            
        case 'eliminar':
             $rspta = $ascensor->eliminar($idascensor);
             echo $rspta;
             break;

        case 'getedificio':
            $idcontrato = isset($_POST['idcontrato']) ? $_POST['idcontrato']: 0;
            $rspta = $ascensor->getEdificio($idcontrato);
            echo json_encode($rspta);
            break;
        
        case 'cambiarcondicion':
            $condicion = isset($_POST['condicion']) ? $_POST['condicion'] : "";
            if($condicion == ""){
                echo 0;
            }else{
                $rspta = $ascensor->cambiarcondicion($idascensor, $condicion);
                echo $rspta;
            }
            
            break;
            
             
        case 'actualizar_ascensor':

            $rspta=$ascensor->actualizar_ascensor($idascensor,$iddirk,$lat,$lon);

            if($rspta){
                 $estatus=1;
                 $mensaje="Ascensor Asignado";
            }else{
                $estatus=0;
                $mensaje="Ascensor no se pudo Asignar";
            }
         
            echo json_encode(array('estatus'=>$estatus,'mensaje'=>$mensaje));
 
        break;
    
        case 'listadoAscensores':
      
            $rspta=$ascensor->listadoAscensores($idedificio,$iddirk);

            $data = Array();

            while ($reg = $rspta->fetch_object()){

            $data[] = array(
                "0"=>'<button class="btn btn-warning btn-xs" onclick="eliminarAscensorDirk('.$reg->idascensor.')"><i class="fa fa-trash"></i></button>
                      <button class="btn btn-success btn-xs"  ><i class="fa fa-pencil"></i></button>',
                "1"=>$reg->codigo,
                "2"=>$reg->codigocli,
                "3"=>$reg->lat,
                "4"=>$reg->lon
                );

            }

            $results = array(
                "sEcho"=>1,
                "iTotalRecords"=>count($data),
                "iTotalDisplayRecords"=>count($data), 
                "aaData"=>$data
            );

            echo json_encode($results);
    
    break;
    
      case 'inhabilitarAscensoresDirk':
    
            $rspta=$ascensor->inhabilitarAscensoresDirk($iddirk);
          
            if(intval($rspta) >= 0){
               $estatus=1;
               $mensaje="Se inhabilito el dirk de : ".$rspta." Ascensor(es) ";
               
            }elseif (intval($rspta) == -1) {
                
              $estatus=0;
              $mensaje="Hubo un error al inhabilitar el dirk de los ascensores.";
              
        }
            echo json_encode(array('estatus'=>$estatus,'mensaje'=>$mensaje));
    

        break;
        
        case 'validarAscensoresSinDirk':
            
            $verifica_dirk=1;
            
            //ascensores con dirk
            $rspta=$ascensor->SelectAscensoresSinDirk($idedificio);
            
            $data = Array();
            $ascensores_dirk=0;
            
            while ($reg = $rspta->fetch_object()){
               $ascensores_dirk++; 
            }
            
            if($ascensores_dirk>0){
                $estatus=1;
                $mensaje="";
            }elseif ($ascensores_dirk==0) {
                $estatus=0;
                $mensaje="Ya Todos los Ascensores de Este Edificio Tienen Dirk Asignado.";
            }
            
            echo json_encode(array('estatus'=>$estatus,'mensaje'=>$mensaje));

        break;
        
        case 'inhabilitarAscensorDirk':
    
        $rspta=$ascensor->inhabilitarAscensorDirk($idascensor);
            
         if($rspta){
            $estatus=1;
            $mensaje="Ascensor Inhabilitado para este Dirk";
        }else{
           $estatus=0;
           $mensaje="Ascensor no se pudo Inhabilitar";
        } 
        
            echo json_encode(array('estatus'=>$estatus,'mensaje'=>$mensaje));
    

        break;
        
        case 'selectAscensoresSinDirk':
                        
            $rspta=$ascensor->SelectAscensoresSinDirk($idedificio);

           echo '<option value="" selected disabled>Seleccione Ascensor</option>';

           while($reg = $rspta->fetch_object()){
               echo '<option value='.$reg->idascensor.'>'.$reg->tascensor.' '.$reg->marca.' / '.$reg->codigo.' / '.$reg->ubicacion.' '.$reg->codigocli.' / '.$reg->ken.'</option>';
           }
            
        break;
}
 ?>