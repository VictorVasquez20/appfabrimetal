<?php

session_start();
require_once '../modelos/VisitaInstalaciones.php';
require_once '../modelos/Proyecto.php';
require_once '../modelos/Ascensor.php';
require_once '../modelos/EtapaProyecto.php';
require_once '../modelos/DocsAsociados.php';

$visita = new VisitasInstalaciones();
$proyecto = new Proyecto();
$ascensor = new Ascensor();
$docsasociados = new DocsAsociados();


//Datos desde el formulario - Editar ascensor
$idproyecto = isset($_POST["idproyecto"]) ? limpiarCadena($_POST["idproyecto"]) : "";
$idestado = isset($_POST["idestado"]) ? limpiarCadena($_POST["idestado"]) : "";
$idventa = isset($_POST["idventa"]) ? limpiarCadena($_POST["idventa"]) : "";

$tipoproyecto = isset($_REQUEST['tipoproyecto']) ? $_REQUEST['tipoproyecto'] : "";

switch ($_GET["op"]) {
    case 'visita':
        $iduser = $_SESSION['iduser'];
        $scensores = count($_POST['idascensor']);

        for ($o = 0; $o < (int) $scensores; $o++) {

            $idascensor = $_POST['idascensor'][$o];
            $observaciones = limpiarCadena($_POST['observaciones' . $idascensor]);
            $esetapa = $_POST['esetapa' . $idascensor];
            $estadoins = $_POST['estadoins' . $idascensor];

            $rspta = $visita->insertar($idproyecto, $idestado, $observaciones, $iduser, $idascensor);

            if ($esetapa == 1) {
                $ascensor->estado($idascensor, (intval($estadoins) + 1));
            }
        }



        echo $rspta ? "REGISTRADA" : "ERROR AL REGISTRAR";
        break;


    case 'visitayetapa':
        if ($_SESSION['SupProyectos'] == 1) {

            //id usuario por session
            $iduser = $_SESSION['iduser'];
            //cuento los ascensores que fueron seleccionados para subir de etapa

            $scensores = isset($_POST['idascensor']) ? count($_POST['idascensor']) : 0;
            //inicializo objeto etapa
            $etapa = new EtapaProyecto();

            //recorro ascensores
            for ($o = 0; $o < (int) $scensores; $o++) {

                $idascensor = $_POST['idascensor'][$o];
                $observaciones = limpiarCadena($_POST['observaciones' . $idascensor]);
                $esetapa = isset($_POST['esetapa' . $idascensor]) ? $_POST['esetapa' . $idascensor] : 0;
                $estadoins = $_POST['estadoins' . $idascensor];

                //inserto la visita por ascensor
                $rspta = $visita->insertar($idproyecto, $idestado, $observaciones, $iduser, $idascensor);

                //si etapa de proyecto es 1 cambio estado de ascensor a 2
                if ($esetapa == 1) {
                    if ($estadoins < 12) {
                        $ascensor->estado($idascensor, (intval($estadoins) + 1));
                    }
                }
            }

            //Si estado de protyecto es 1 modifico todos los ascensores a 2
            if ($idestado == 1) {
                $ascensor->estadotodos($idventa, 2);
            }

            //obtengo el ascensor que va en la menor etapa de todos
            $respuesta = $ascensor->menoretapa($idventa);
            while ($reg = $respuesta->fetch_object()) {
                $menoretapa = $reg->estado;
            }

            //si la menor etapa es distinta al estado del proyecto, modifico el estadp proyecto
            if ($menoretapa != $idestado) {

                //cambio estado de proyecto con fecha de actualizacion y usuario
                $update_time = date("Y-m-d H:i:s");
                if ((int) $menoretapa == 12) {
                    $closed_time = date("Y-m-d H:i:s");
                    $closed_user = $_SESSION['iduser'];
                    $rspta = $proyecto->etapafinal($idproyecto, $menoretapa, $update_time, $iduser, $closed_time, $closed_user);
                } else {
                    $rspta = $proyecto->etapa($idproyecto, $menoretapa, $update_time, $iduser);
                }

                //si estado es distinto de 1la duracion de dias es 0, si no, 
                //calculo los dias transcurridos entre la ultima acualizacion y la fecha actual
                if ($idestado != 1) {

                    //Obtengo mayor fecha de actualizacion
                    $respuesta2 = $etapa->ultimafecha($idproyecto);
                    while ($reg2 = $respuesta2->fetch_object()) {
                        $created_time = date_create($reg2->fecha);
                        $update_time = date_create($update_time);
                    }

                    $fini = date_format($created_time, "Y-m-d");
                    $ffin = date_format($update_time, "Y-m-d");

                    $diff = (strtotime($fini) - strtotime($ffin)) / 86400;
                    $diff = abs($diff);
                    $diff = floor($diff);

                    $duracion = $diff;
                } else {
                    $duracion = 0;
                }

                //Inserto la etapa proyecto, con el id de proyecto, estado de proyecto y cantidad de dias en actualizar
                $rspra = $etapa->insertar($idproyecto, $idestado, $duracion);
            }
            echo $rspta ? "REGISTRADA" : "ERROR AL REGISTRAR";
        } elseif ($_SESSION['GEProyectos'] == 1 || $_SESSION['PMProyectos'] == 1 || $_SESSION['administrador'] == 1) {

            //id usuario por session
            $iduser = $_SESSION['iduser'];
            //cuento los ascensores que fueron seleccionados para subir de etapa
            $scensores = count($_POST['idascensor']);

            //recorro ascensores
            for ($o = 0; $o < (int) $scensores; $o++) {

                $idascensor = $_POST['idascensor'][$o];
                $observaciones = limpiarCadena($_POST['observaciones' . $idascensor]);
                $estadoins = $_POST['estadoins' . $idascensor];

                //inserto la visita por ascensor
                $rspta = $visita->insertar($idproyecto, $idestado, $observaciones, $iduser, $idascensor);
            }
            echo $rspta ? "REGISTRADA" : "ERROR AL REGISTRAR";
        }
        break;


    case 'listar':
        $rspta = $proyecto->listar($_SESSION['rut'], $tipoproyecto);

        $data = Array();

        while ($reg = $rspta->fetch_object()) {

            $barra = '<td class="project_progress"><div class="progress progress_sm" style="margin-bottom:5px;"><div class="progress-bar bg-green" role="progressbar" data-transitiongoal="' . $reg->carga . '" aria-valuenow="' . $reg->carga . '" style="width: ' . $reg->carga . '%;"></div></div><small>' . $reg->carga . '% Completo</small></td>';
            $barraestado = '<button type="button" class="btn btn-xs" style="background-color:' . $reg->color . '; color: #fff;">' . $reg->estadonomb . '</button>';

            $op = "";

            if ($_SESSION['SupProyectos'] == 1) {
                $op = '<div class="btn-group">'
                        . ' <button data-toggle="dropdown" class="btn btn-success dropdown-toggle btn-xs" type="button"><i class="fa fa-cog"></i></button>'
                        . ' <ul role="menu" class="dropdown-menu">'
                        . '     <li><a onclick="mostrar(' . $reg->idproyecto . ')">Visita</a></li>'
                        . '     <li class="divider"></li>'
                        . '     <li><a onclick="revisar(' . $reg->idproyecto . ')">Visualizar estado</a></li>'
                        . '</ul>'
                        . '</div>';
            } elseif ($_SESSION['PMProyectos'] == 1 && $reg->estado > 1) {
                $op = '<div class="btn-group">'
                        . ' <button data-toggle="dropdown" class="btn btn-success dropdown-toggle btn-xs" type="button"><i class="fa fa-cog"></i></button>'
                        . ' <ul role="menu" class="dropdown-menu">'
                        . '     <li><a onclick="visitaotro(' . $reg->idproyecto . ')">Visita</a></li>'
                        . '     <li class="divider"></li>'
                        . '     <li><a onclick="revisar(' . $reg->idproyecto . ')">Visualizar estado</a></li>';
                $op .= '</ul>'
                        . '</div>';
            } elseif ($_SESSION['PMProyectos'] == 1 && $reg->estado == 1) {
                $op = '<div class="btn-group">'
                        . ' <button data-toggle="dropdown" class="btn btn-success dropdown-toggle btn-xs" type="button"><i class="fa fa-cog"></i></button>'
                        . ' <ul role="menu" class="dropdown-menu">'
                        . '     <li><a onclick="revisar(' . $reg->idproyecto . ')">Visualizar estado</a></li>';
                $op .= '</ul>'
                        . '</div>';
            } elseif ($_SESSION['GEProyectos'] == 1 && $reg->estado > 1) {
                $op = '<div class="btn-group">'
                        . ' <button data-toggle="dropdown" class="btn btn-success dropdown-toggle btn-xs" type="button"><i class="fa fa-cog"></i></button>'
                        . ' <ul role="menu" class="dropdown-menu">'
                        . '     <li><a onclick="visitaotro(' . $reg->idproyecto . ')">Visita</a></li>'
                        . '     <li class="divider"></li>'
                        . '     <li><a onclick="revisar(' . $reg->idproyecto . ')">Visualizar estado</a></li>';
                $op .= '</ul>'
                        . '</div>';
            } elseif ($_SESSION['GEProyectos'] == 1 && $reg->estado == 1) {
                $op = '<div class="btn-group">'
                        . ' <button data-toggle="dropdown" class="btn btn-success dropdown-toggle btn-xs" type="button"><i class="fa fa-cog"></i></button>'
                        . ' <ul role="menu" class="dropdown-menu">'
                        . '     <li><a onclick="revisar(' . $reg->idproyecto . ')">Visualizar estado</a></li>';
                $op .= '</ul>'
                        . '</div>';
            } elseif ($_SESSION['administrador'] == 1 && $reg->estado > 1) {
                $op = '<div class="btn-group">'
                        . ' <button data-toggle="dropdown" class="btn btn-success dropdown-toggle btn-xs" type="button"><i class="fa fa-cog"></i></button>'
                        . ' <ul role="menu" class="dropdown-menu">'
                        . '     <li><a onclick="mostrar(' . $reg->idproyecto . ')">Visita</a></li>'
                        . '     <li class="divider"></li>'
                        . '     <li><a onclick="visitaotro(' . $reg->idproyecto . ')">Visita PM</a></li>'
                        . '     <li class="divider"></li>'
                        . '     <li><a onclick="visitaotro(' . $reg->idproyecto . ')">Visita Gerente</a></li>'
                        . '     <li class="divider"></li>'
                        . '     <li><a onclick="revisar(' . $reg->idproyecto . ')">Visualizar estado</a></li>';
                $op .= '</ul>'
                        . '</div>';
            }
            if (($_SESSION['AsigProyectos'] == 1 || $_SESSION['administrador'] == 1) && $reg->estado > 1) {
                $op .= '<div class="btn-group ml-1"><button class="btn btn-info btn-xs" onclick="MostrarDocumentos(' . $reg->idventa . ',\''.$reg->idproyecto.'\')" data-tooltip="tooltip" title="DOCUMENTOS"><i class="fa fa-folder-open"></i></button></div>';
            }
            $data[] = array(
                "0" => $op,
                "1" => $reg->nombre,
                "2" => $reg->codigo,
                "3" => $reg->pm,
                "4" => $reg->supervisor,
                "5" => $barra,
                "6" => $barraestado,
                "7" => $reg->updated_time,
                "8" => $reg->dias);
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;


    /** MODERNIZACIONES * */
    case 'listarModernizacion':
        $rspta = $proyecto->listar($_SESSION['rut'], $tipoproyecto);

        $data = Array();

        while ($reg = $rspta->fetch_object()) {

            $barra = '<td class="project_progress"><div class="progress progress_sm" style="margin-bottom:5px;"><div class="progress-bar bg-green" role="progressbar" data-transitiongoal="' . $reg->carga . '" aria-valuenow="' . $reg->carga . '" style="width: ' . $reg->carga . '%;"></div></div><small>' . $reg->carga . '% Completo</small></td>';
            $barraestado = '<button type="button" class="btn btn-xs" style="background-color:' . $reg->color . '; color: #fff;">' . $reg->estadonomb . '</button>';


            if ($_SESSION['administrador'] == 1 || $_SESSION['Modernizaciones'] == 1) {
                /* $op = '<div class="btn-group">'
                  . ' <button data-toggle="dropdown" class="btn btn-success dropdown-toggle btn-xs" type="button"><i class="fa fa-cog"></i></button>'
                  . ' <ul role="menu" class="dropdown-menu">'
                  . '     <li><a onclick="finalizar(' . $reg->idproyecto . ')">Finalizar</a></li>'
                  . '     <li class="divider"></li>'
                  . '     <li><a onclick="revisar(' . $reg->idproyecto . ')">Visualizar estado</a></li>'
                  . '</ul>'
                  . '</div>'; */
                $op = '<button class="btn btn-info btn-xs" onclick="revisar(' . $reg->idproyecto . ')"><i class="fa fa-eye"></i></button>';
            }

            $data[] = array(
                "0" => $op,
                "1" => $reg->nombre,
                "2" => $reg->codigo,
                "3" => $reg->pm,
                "4" => $reg->supervisor,
                "5" => $barra,
                "6" => $barraestado,
                "7" => $reg->updated_time,
                "8" => $reg->dias);
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;


    case 'finalizarmodernizacion':
        $iduser = $_SESSION['iduser'];
        $estadoins = isset($_POST['idestado2']) ? $_POST['idestado2'] : 0;
        $idproyecto = isset($_POST['idproyecto2']) ? $_POST['idproyecto2'] : 0;
        $scensores = isset($_POST['idascensor']) ? count($_POST['idascensor']) : 0;
        $update_time = date("Y-m-d H:i:s");

        for ($o = 0; $o < (int) $scensores; $o++) {
            $idascensor = $_POST['idascensor'][$o];
            $ascensor->estado($idascensor, (intval($estadoins) + 1));
        }
        $rspta = $proyecto->etapa($idproyecto, (intval($estadoins) + 1), $update_time, $iduser);
        echo $rspta ? "REGISTRADA" : "ERROR AL REGISTRAR" . $rspta;
        break;


    /** MODERNIZACIONES * */
    case 'listarNoPmSup':
        $rspta = $proyecto->listarnoPMSUP($_SESSION['rut'], $tipoproyecto);

        $data = Array();

        while ($reg = $rspta->fetch_object()) {


            if ($_SESSION['AsigProyectos'] == 1) {
                $op = '<button type="button" class="btn btn-success btn-xs" onclick="mostrar(' . $reg->idproyecto . ')"><i class="fa fa-pencil"></i></button><button type="button" class="btn btn-info btn-xs" onclick="pdf(' . $reg->idventa . ')" data-tooltip="tooltip" title="Memo de Venta (PDF)"><i class="fa fa-file-pdf-o"></i></button>';
            } elseif ($_SESSION['administrador'] == 1) {
                $op = '<button type="button" class="btn btn-success btn-xs" onclick="mostrar(' . $reg->idproyecto . ')"><i class="fa fa-pencil"></i></button><button type="button" class="btn btn-info btn-xs" onclick="pdf(' . $reg->idventa . ')" data-tooltip="tooltip" title="Memo de Venta (PDF)"><i class="fa fa-file-pdf-o"></i></button>';
            }

            $data[] = array(
                "0" => $op,
                "1" => $reg->nombre,
                "2" => $reg->codigo,
                "3" => $reg->pm,
                "4" => $reg->supervisor,
                "5" => 'SIN ASIGNAR');
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;


    case 'listarNoPmSupAsignados':
        $rspta = $proyecto->listarnoPMSUPasignados($_SESSION['rut'], $tipoproyecto);

        $data = Array();

        while ($reg = $rspta->fetch_object()) {


            if ($_SESSION['AsigProyectos'] == 1) {
                $op = '<button type="button" class="btn btn-success btn-xs" onclick="mostrar(' . $reg->idproyecto . ')"><i class="fa fa-pencil"></i></button><button type="button" class="btn btn-info btn-xs" onclick="pdf(' . $reg->idventa . ')" data-tooltip="tooltip" title="Memo de Venta (PDF)"><i class="fa fa-file-pdf-o"></i></button>';
            } elseif ($_SESSION['administrador'] == 1) {
                $op = '<button type="button" class="btn btn-success btn-xs" onclick="mostrar(' . $reg->idproyecto . ')"><i class="fa fa-pencil"></i></button><button type="button" class="btn btn-info btn-xs" onclick="pdf(' . $reg->idventa . ')" data-tooltip="tooltip" title="Memo de Venta (PDF)"><i class="fa fa-file-pdf-o"></i></button>';
            }

            $data[] = array(
                "0" => $op,
                "1" => $reg->nombre,
                "2" => $reg->codigo,
                "3" => $reg->pm,
                "4" => $reg->supervisor,
                "5" => 'ASIGNADOS');
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;


    case 'mostrarNoPmSup':
        $rspta = $proyecto->mostrarNoPmSup($idproyecto);
        echo json_encode($rspta);
        break;


    case 'editarNoPmSup':
        if ($idproyecto) {
            $supervisor = isset($_POST['supervisor']) ? $_POST['supervisor'] : 0;
            $pm = isset($_POST['pm']) ? $_POST['pm'] : 0;
            if ($supervisor <> 0 && $pm <> 0) {
                $updated_user = $_SESSION['iduser'];
                $rspta = $proyecto->Editar($idproyecto, $pm, $supervisor, $updated_user);
                echo $rspta ? "Proyecto Editado" : "Proyecto no editado";
            } else {
                echo "Falta Project manager o Supervisor";
            }
        }
        break;


    case 'mostrar':
        $rspta = $proyecto->mostrar($idproyecto);
        echo json_encode($rspta);
        break;


    case 'numerovisita':
        $rspta = $visita->numerovisita($idproyecto);
        echo json_encode($rspta);
        break;


    case 'PDF':
        $idproyecto = isset($_REQUEST["idproyecto"]) ? limpiarCadena($_REQUEST["idproyecto"]) : "";

        //Se ocupara sistema de plantillas TemplatePower
        require_once("../public/build/lib/TemplatePower/class.TemplatePower.php7.inc.php");
        $t = new TemplatePower("../production/informes/instalaciones/pla_reporte_proyecto_2.html");
        $t->prepare();

        // $path_reportes = '../files/visitas/reportes/';

        //DATOS DEL PROYECTO
        $datos = $visita->pdfdatos($idproyecto);
        $datos['importacion'] = $datos['codigoimp'] . ' - ' . $datos['proveedor'];

        //NUMERO DE VISITAS
        $nvisitas = $visita->cantvisitas($idproyecto);

        //NUMERO DE EQUIPOS
        $nequipos = $visita->cantequipos($idproyecto);

        //informacion del proyecto
        $t->assign('codigoimp', $datos['codigoimp'] . '');
        $t->assign('proyecto', $datos['proyecto'] . '');
        $t->assign('direcpro', $datos['direcpro'] . '');
        $t->assign('cc', $datos['cc'] . '');
        $t->assign('importacion', $datos['importacion'] . '');
        $t->assign('nequipos', $nequipos . '');
        $t->assign('nvisitas', $nvisitas . '');
        $t->assign('estado', $datos['estado'] . '');
        $t->assign('fechaini', $datos['fechaini'] . '');
        $t->assign('fechaact', $datos['fechaact'] . '');
        $t->assign('fechaclo', $datos['fechaclo'] . '');

        //informacion de venta
        $t->assign('fecha', $datos['fecha'] . '');
        $t->assign('descripcion', $datos['descripcion'] . '');
        $t->assign('codigo', $datos['codigo'] . '');
        $t->assign('vendedor', $datos['vendedor'] . '');
        $t->assign('man_post', $datos['man_post'] . '');
        $t->assign('garan_ex', $datos['garan_ex'] . '');

        //ETAPAS DEL PROYECTO
        $rspetapas = $visita->infoetapas($idproyecto);
        $rows = $rspetapas->fetch_all(MYSQLI_ASSOC);
        foreach($rows as $i => $item)
        {
            $t->newBlock('etapas');
            $t->assign('fecha', $item['fecha'] . '');
            $t->assign('duracion', $item['duracion'] . '');
            $t->assign('nombre', $item['nombre'] . '');
        }

        //DATOS DE LA VISITA
        $rspvi = $visita->infovi($idproyecto);
        $rows = $rspvi->fetch_all(MYSQLI_ASSOC);
        foreach($rows as $i => $item)
        {
            $t->newBlock('datosvisita');
            $t->assign('numvisita', ($i + 1) . '');
            $t->assign('fecha', $item['fecha'] . '');
            $t->assign('estado', $item['estado'] . '');

            if (count($rows) != ($i + 1))
                $t->assign('pagebreak', '<pagebreak />');

            //VISITAS A ASENSORES
            $rspinvi = $visita->infovife($item['fehora'], $idproyecto);
            $rows2 = $rspinvi->fetch_all(MYSQLI_ASSOC);
            foreach($rows2 as $j => $item2)
            {
                $t->newBlock('visitainstalacion');
                $t->assign('codigo', $item2['codigo'] . '');
                $t->assign('colaborador', $item2['colaborador'] . '');
                $t->assign('nombre', $item2['nombre'] . '');
                $t->assign('observacion', $item2['observacion'] . '');

                //LISTO LAS IMAGENES DE LA VISITA
                $path_foto_visitas = '../files/docsasociados/';
                $filesvisita = '';
                $rspfotovis = $docsasociados->BuscarImagenes('APPFABRIMETAL_VISITA_EQUIPO_FOTO', $item2['idvisita'], 'visita');
                $filesvisita = '';
                $rows3 = $rspfotovis->fetch_all(MYSQLI_ASSOC);
                foreach($rows3 as $k => $item3)
                {
                    $fotovisita = $path_foto_visitas . $item3['documento'];
                    if (file_exists($fotovisita)) {
                        if (is_file($fotovisita)) {
                            // $t->newBlock('filesvisita');
                            // $t->assign('filevisita', $fotovisita . '');
                            $filesvisita .= '<img alt="" src="' . $fotovisita . '" style="margin: 5px; max-width: 350px; border: 1px solid grey; vertical-align: top;">';
                            // $filesvisita .= '<img alt="" src="' . $fotovisita . '" style="margin: 5px; max-width: 100mm; border: 1px solid grey;">';
                            // $mpdf->Image($fotovisita, 0, 0, 210, 297, 'jpg', '', true, false);
                            /*$filesvisita .= '<div style="position: fixed; left:0; right: 0; top: 0; bottom: 0;">
                                    <img src="' . $fotovisita . '" 
                                         style="height: 120mm; margin: 0;" />
                                </div>';*/
                            // $t->assign('filevisita', '<img width="300" alt="" src="' . $path_reportes . $item2['file'] . '">' . '');
                        }
                    }
                }
                $t->assign('datosvisita.filesvisita', $filesvisita . '');
            }
        }

        $style02 = '../production/informes/css/reportes_fm.css';

        require_once("../public/build/lib/mPdfSC/vendor/autoload.php");

        $mpdf = new \Mpdf\Mpdf([
            'mode' => 'c',
            'margin_left' => 20,
            'margin_right' => 20,
            'margin_top' => 32,
            'margin_bottom' => 20,
            'margin_header' => 16,
            'margin_footer' => 13
        ]);

        $mpdf->SetHTMLHeader('<img src="../production/informes/img/fm-logo-negro.png" height="40" alt="" style="float: left; display: inline-block;"> <div style="text-align: right">Página {PAGENO} / {nb}<div>', 'O', true);

        // $mpdf->SetDisplayMode('fullwidth');
        $mpdf->SetDisplayMode(100);
        // $mpdf->SetDisplayMode('fullpage');

        $mpdf->SetDisplayPreferences('/HideToolbar/CenterWindow/FitWindow');
        // $mpdf->SetDisplayPreferences('/HideMenubar/HideToolbar/CenterWindow/FitWindow');

        $mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list

        // $mpdf->shrink_tables_to_fit = 1;

        // $mpdf->ignore_table_percents = true;

        // $mpdf->ignore_table_width = true;
        
        // $mpdf->showImageErrors = true

        $mpdf->tableMinSizePriority = true;

        /*$mpdf->simpleTables = true;
        $mpdf->packTableData = true;
        $mpdf->keep_table_proportions = TRUE;
        $mpdf->shrink_tables_to_fit=1;*/

        // Load a stylesheet
        $stylesheet02 = file_get_contents($style02);

        $html = $t->getOutputContent();

        $mpdf->WriteHTML($stylesheet02, 1); // The parameter 1 tells that this is css/style only and no body/html/text
        $mpdf->WriteHTML($html,2);

        $PROYECTO_REPORTE_PDF = 'REPORTE_DEL_PROYECTO_' . $datos['codigoimp'] . '.pdf';

        $mpdf->Output($PROYECTO_REPORTE_PDF, \Mpdf\Output\Destination::INLINE);

        break;


    case 'dashinstalacion':
        $idproyecto = isset($_POST["idproyecto"]) ? limpiarCadena($_POST["idproyecto"]) : "";
        //DATOS DEL PROYECTO
        $datos = $visita->dashinstalacion($idproyecto);
        
         while ($reg = $datos->fetch_object()) {
             $data[] = array(
                "dia" => $reg->dia,
                "mes" => $reg->mes,
                "user"=> $reg->user,
                "obs" => $reg->observacion,
                "asc" => $reg->codigo,
                "imagen"=> $reg->imagen,
                "fecha" => $reg->fecha);
             
         }
        echo json_encode($data);
        break;
}

