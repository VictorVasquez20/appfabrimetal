<?php 

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');


require_once "../modelos/Ascensor.php";
require_once "../modelos/Servicio.php";

$ascensor = new Ascensor();
$servicio = new Servicio();


switch ($_GET["op"]) {
    
    case 'listarpfirmasup':
        $rut = $_GET["rut"];
        $rspta = $servicio->listar_psup($rut);
        $data = Array();
        while ($reg = $rspta->fetch_object()) {

            $data[] = array(
                "0" => $reg->fecha,
                "1" => $reg->idservicio,
                "2" => $reg->tipo,
                "3" => $reg->codigo,
                "4" => $reg->nombre,
                "5" => $reg->nomtec.' '.$reg->apetec
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
    break;
	
}
