<?php

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

//Modelos
require_once "../modelos/CentroCosto.php";
require_once "../modelos/Usuario.php";





//Funcion para validar pasametros por POST
function ParametrosDisponiblesPost($params) {
    //Suponemos que todos los parametros vienene disponibles
    $disponible = true;
    $sinparams = "";

    foreach ($params as $param) {
        if (!isset($_POST[$param]) || strlen($_POST[$param]) <= 0) {
            $disponible = false;
            $sinparams = $sinparams . ", " . $param;
        }
    }

    //si faltan parametros
    if (!$disponible) {
        $response = array();
        $response['error'] = true;
        $response['message'] = 'Parametro' . substr($sinparams, 1, strlen($sinparams)) . ' vacio';

        //error de visualización
        echo json_encode($response);

        //detener la ejecición adicional
        die();
    }
}

//Funcion para validar pasametros por GET
function ParametrosDisponiblesGet($params) {
    //Suponemos que todos los parametros vienene disponibles
    $disponible = true;
    $sinparams = "";

    foreach ($params as $param) {
        if (!isset($_GET[$param]) || strlen($_GET[$param]) <= 0) {
            $disponible = false;
            $sinparams = $sinparams . ", " . $param;
        }
    }

    //si faltan parametros
    if (!$disponible) {
        $response = array();
        $response['error'] = true;
        $response['message'] = 'Parametro' . substr($sinparams, 1, strlen($sinparams)) . ' vacio';

        //error de visualización
        echo json_encode($response);

        //detener la ejecición adicional
        die();
    }
}

//una matriza para mostrar las respuestas de nuestra api
$response = array();


if (isset($_GET['apicall'])) {

    //Aqui iran todos los llamados de nuestra api
    switch ($_GET['apicall']) {

        case 'SelectCentrosCosto':
            //Objeto del modelo
            $centrocosto = new CentroCosto();
            $rspta = $centrocosto->selectcentrocostoEX();
            if (isset($rspta) && !empty($rspta)) {
                $contenido = '<option value="" selected disabled>SELECCIONE CENTRO DE COSTO</option>';
                while ($reg = $rspta->fetch_object()) {
                    $contenido .= '<option value=' . $reg->codigo . '>' . $reg->nombre . ' / ' . $reg->codigo . ' - ' . $reg->descripcion . '</option>';
                }

                $response['error'] = false;
                $response['message'] = 'solicitud completada correctamente';
                $response['contenido'] = $contenido;
            } else {
                $response['error'] = true;
                $response['message'] = 'No es posible realizar esta peticion';
            }

            break;

        case 'verificarUsuario':
            require_once "../modelos/Role.php";
            require_once "../modelos/Usuario.php";

            ParametrosDisponiblesPost(array('username_form', 'password_form'));

            $usuario = new Usuario();


            if (isset($_POST['username_form']) && !empty($_POST['username_form']) && isset($_POST['password_form']) && !empty($_POST['password_form'])) {
                $username_form = $_POST['username_form'];

                $password_form = $_POST['password_form'];

                $password_hash = hash("SHA256", $password_form);

                $rspta = $usuario->verificar($username_form, $password_hash);

                $fecth = $rspta->fetch_object();

                if (isset($fecth)) {
                    $_SESSION['iduser'] = $fecth->iduser;
                    $_SESSION['nombre'] = $fecth->nombre;
                    $_SESSION['apellido'] = $fecth->apellido;
                    $_SESSION['imagen'] = $fecth->imagen;
                    $_SESSION['username'] = $fecth->username;
                    $_SESSION['idrole'] = $fecth->idrole;
                    $_SESSION['rut'] = $fecth->num_documento;

                    $role = new Role();
                    $permisos = $role->listarmarcados($fecth->idrole);

                    $valores = array();

                    while ($per = $permisos->fetch_object()) {
                        array_push($valores, $per->idpermiso);
                    }


                    in_array(1, $valores) ? $_SESSION['administrador'] = 1 : $_SESSION['administrador'] = 0;
                    in_array(2, $valores) ? $_SESSION['mantencion'] = 1 : $_SESSION['mantencion'] = 0;
                    in_array(3, $valores) ? $_SESSION['Icontratos'] = 1 : $_SESSION['Icontratos'] = 0;
                    in_array(4, $valores) ? $_SESSION['Mcontratos'] = 1 : $_SESSION['Mcontratos'] = 0;
                    in_array(5, $valores) ? $_SESSION['Vcontratos'] = 1 : $_SESSION['Vcontratos'] = 0;
                    in_array(6, $valores) ? $_SESSION['Lcontratos'] = 1 : $_SESSION['Lcontratos'] = 0;
                    in_array(7, $valores) ? $_SESSION['Contratos'] = 1 : $_SESSION['Contratos'] = 0;
                    in_array(8, $valores) ? $_SESSION['Guia'] = 1 : $_SESSION['Guia'] = 0;
                    in_array(9, $valores) ? $_SESSION['Servicio'] = 1 : $_SESSION['Servicio'] = 0;
                    in_array(10, $valores) ? $_SESSION['GGuias'] = 1 : $_SESSION['GGuias'] = 0;
                    in_array(11, $valores) ? $_SESSION['DGuias'] = 1 : $_SESSION['DGuias'] = 0;
                    in_array(12, $valores) ? $_SESSION['Dllamadas'] = 1 : $_SESSION['Dllamadas'] = 0;
                    in_array(12, $valores) ? $_SESSION['Tickets'] = 1 : $_SESSION['Tickets'] = 0;
                    in_array(14, $valores) ? $_SESSION['GGuiasE'] = 1 : $_SESSION['GGuiasE'] = 0;
                    in_array(15, $valores) ? $_SESSION['APresupuesto'] = 1 : $_SESSION['APresupuesto'] = 0;
                    in_array(16, $valores) ? $_SESSION['GPresupuesto'] = 1 : $_SESSION['GPresupuesto'] = 0;
                    in_array(17, $valores) ? $_SESSION['Contabilidad'] = 1 : $_SESSION['Contabilidad'] = 0;
                    in_array(18, $valores) ? $_SESSION['RPresupuesto'] = 1 : $_SESSION['RPresupuesto'] = 0;
                    in_array(19, $valores) ? $_SESSION['RGuias'] = 1 : $_SESSION['RGuias'] = 0;
                    in_array(20, $valores) ? $_SESSION['GEServicios'] = 1 : $_SESSION['GEServicios'] = 0;
                    in_array(21, $valores) ? $_SESSION['Pantallas'] = 1 : $_SESSION['Pantallas'] = 0;
                    in_array(22, $valores) ? $_SESSION['AReparacion'] = 1 : $_SESSION['AReparacion'] = 0;
                    in_array(23, $valores) ? $_SESSION['GReparacion'] = 1 : $_SESSION['GReparacion'] = 0;
                    in_array(24, $valores) ? $_SESSION['JReparacion'] = 1 : $_SESSION['JReparacion'] = 0;
                    in_array(25, $valores) ? $_SESSION['SPantallas'] = 1 : $_SESSION['SPantallas'] = 0;
                    in_array(26, $valores) ? $_SESSION['Instalaciones'] = 1 : $_SESSION['Instalaciones'] = 0;
                    in_array(27, $valores) ? $_SESSION['SupProyectos'] = 1 : $_SESSION['SupProyectos'] = 0;
                    in_array(28, $valores) ? $_SESSION['PMProyectos'] = 1 : $_SESSION['PMProyectos'] = 0;
                    in_array(29, $valores) ? $_SESSION['GEProyectos'] = 1 : $_SESSION['GEProyectos'] = 0;
                    in_array(30, $valores) ? $_SESSION['AsigProyectos'] = 1 : $_SESSION['AsigProyectos'] = 0;
                    in_array(31, $valores) ? $_SESSION['Comercial'] = 1 : $_SESSION['Comercial'] = 0;
                    in_array(32, $valores) ? $_SESSION['Preventa'] = 1 : $_SESSION['Preventa'] = 0;
                    in_array(33, $valores) ? $_SESSION['GPreventa'] = 1 : $_SESSION['GPreventa'] = 0;
                    in_array(34, $valores) ? $_SESSION['CPreventa'] = 1 : $_SESSION['CPreventa'] = 0;
                    in_array(35, $valores) ? $_SESSION['Aprobacion'] = 1 : $_SESSION['Aprobacion'] = 0;
                    in_array(36, $valores) ? $_SESSION['GAprobacion'] = 1 : $_SESSION['GAprobacion'] = 0;
                    in_array(37, $valores) ? $_SESSION['VentaFacturacion'] = 1 : $_SESSION['VentaFacturacion'] = 0;
                    in_array(38, $valores) ? $_SESSION['CFacturacion'] = 1 : $_SESSION['CFacturacion'] = 0;
                    in_array(39, $valores) ? $_SESSION['AFacturacion'] = 1 : $_SESSION['AFacturacion'] = 0;
                    in_array(40, $valores) ? $_SESSION['GFacturacion'] = 1 : $_SESSION['GFacturacion'] = 0;
                    in_array(41, $valores) ? $_SESSION['Modernizaciones'] = 1 : $_SESSION['Modernizaciones'] = 0;
                    in_array(42, $valores) ? $_SESSION['Adquisiciones'] = 1 : $_SESSION['Adquisiciones'] = 0;
                    in_array(43, $valores) ? $_SESSION['Comex'] = 1 : $_SESSION['Comex'] = 0;
                    in_array(44, $valores) ? $_SESSION['RRHH'] = 1 : $_SESSION['RRHH'] = 0;
                    in_array(45, $valores) ? $_SESSION['Gerencia'] = 1 : $_SESSION['Gerencia'] = 0;
                    
                }

                $response['error'] = false;
                $response['message'] = 'solicitud completada correctamente';
                $response['contenido'] = $fecth;
            } else {
                $response['error'] = true;
                $response['message'] = 'No es posible realizar esta peticion';
            }

            break;
    }
} else {
    //Valida que venga una llamada correcta a la APi
    $response['error'] = true;
    $response['message'] = 'Llamda incorrecta a la API';
}

echo json_encode($response);
?>