<?php
session_start();

require_once '../modelos/ContratoInstalacion.php';
require_once "../modelos/Edificio.php";
require_once '../modelos/Contacto.php';

$CI = new ContratoInstalacion();
$edificio = new Edificio();
$contactoedif = new Contacto();

$idproyecto = isset($_REQUEST['idproyecto']) ? $_REQUEST['idproyecto'] : 0;

//Datos desde el formulario - Seccion de contrato
$idcliente = isset($_POST["idcliente"]) ? $_POST["idcliente"] : "";
$idcontrato = isset($_POST["idcontrato"]) ? limpiarCadena($_POST["idcontrato"]) : "";
$tcontrato = isset($_POST["tcontrato"]) ? limpiarCadena($_POST["tcontrato"]) : "";
$ncontrato = isset($_POST["ncontrato"]) ? limpiarCadena($_POST["ncontrato"]) : "";
$nexterno = isset($_POST["nexterno"]) ? limpiarCadena($_POST["nexterno"]) : "";
$nreferencia = isset($_POST["nreferencia"]) ? limpiarCadena($_POST["nreferencia"]) : "";
$fecha = isset($_POST["fecha"]) ? limpiarCadena($_POST["fecha"]) : "";
$fecha_ini = isset($_POST["fecha_ini"]) ? limpiarCadena($_POST["fecha_ini"]) : "";
$fecha_fin = isset($_POST["fecha_fin"]) ? limpiarCadena($_POST["fecha_fin"]) : "";
$idperiocidad = isset($_POST["idperiocidad"]) ? limpiarCadena($_POST["idperiocidad"]) : "";
$ubicacion = isset($_POST["ubicacion"]) ? limpiarCadena($_POST["ubicacion"]) : "";
$observaciones = isset($_POST["observaciones"]) ? limpiarCadena($_POST["observaciones"]) : "";
$rautomatica = isset($_POST["rautomatica"]) ? limpiarCadena($_POST["rautomatica"]) : "";
$idregiones = isset($_POST["conidregiones"]) ? $_POST["conidregiones"] : 0;
$idprovincias = isset($_POST["conidprovincias"]) ? $_POST["conidprovincias"] : NULL;
$idcomunas = isset($_POST["concomuna"]) ? $_POST["concomuna"] : 0;

switch ($_GET['op']) {
    case 'guardaryeditar':
        $val = 0;
        if (!$idcontrato) {
            
            /**
             * INSERTA CONTRATO
             */
            $ubicacion = empty($ubicacion) ? "N/S" : $ubicacion; 
            $fecha_ini = empty($fecha_ini) ? "0000-00-00" : $fecha_ini;
            $fecha_fin = empty($fecha_fin) ? "0000-00-00" : $fecha_fin;
            
            $rspta = $CI->Insertar($idcliente, $idperiocidad, $tcontrato, $idregiones, $idcomunas, $ncontrato, $nexterno, $nreferencia, $ubicacion, $fecha, $fecha_ini, $fecha_fin, $rautomatica, $observaciones);
            
            $val = $rspta != 0 ? 1 : 0;
            /**
             * INSERTA EDIFICIO
             */
            $iduser = $_SESSION['iduser'];
            $nombre = isset($_POST["nombreedi"]) ? limpiarCadena($_POST["nombreedi"]) : "";
            $calle = isset($_POST["calleedi"]) ? limpiarCadena($_POST["calleedi"]) : "";
            $numero = isset($_POST["numeroedi"]) ? limpiarCadena($_POST["numeroedi"]) : "";
            $idtsegmento = isset($_POST["idtsegmento"]) ? limpiarCadena($_POST["idtsegmento"]) : "";
            $coordinacion = isset($_POST["corcorreo"]) ? limpiarCadena($_POST["corcorreo"]) : "";
            $residente = isset($_POST["residente"]) ? limpiarCadena($_POST["residente"]) : "";
            $idregiones = isset($_POST["idregionesedi"]) ? limpiarCadena($_POST["idregionesedi"]) : "";
            $idcomunas = isset($_POST["idcomunasedi"]) ? limpiarCadena($_POST["idcomunasedi"]) : "";
            $ncon_edi = isset($_POST["ncon_edi"]) ? limpiarCadena($_POST["ncon_edi"]) : "";
            $idedificio = $edificio->InsertarEdificio_ID($nombre, $calle, $numero, $idtsegmento, $coordinacion, $residente, $idregiones, $idcomunas);

            if (isset($idedificio) && $rspta != 0) {
                if (intval($ncon_edi) > 0) {
                    for ($o = 0; $o < intval($ncon_edi); $o++) {
                        $nreg = 1;
                        $respcon = $contactoedif->ContactoEdificio($_POST['nombre_conedi' . $o . ''], $_POST['email_conedi' . $o . ''], $_POST['numero_conedi' . $o . ''], $idedificio);
                        $respcon ? $nreg++ : "";
                    }
                    if ($nreg == intval($ncon_edi)) {
                        echo "Contrato guardado, Edificio guardado, contacto guardado <br>";
                        $val += 1;
                    } else {
                        echo "Contrato guardado, Edificio guardado, contacto guardado <br>";
                        $val += 1;
                    }
                } else {
                    echo "Contrato guardado, Edificio guardado <br>";
                }
            } else {
                echo "NOK";
            }
            
            /**
             * INSERTA ASCENSORES
             */            
            if (isset($idedificio) && $rspta != 0) {
                for ($o = 0; $o < (int) $_POST['nascensores']; $o++) {
                    $updated_user = $_SESSION['iduser'];
                    
                    //echo $iduser.' / '.$idconedi.' / '.$_POST['idtascensor'.$i.''.$o.''].' / '.$_POST['marca'.$i.''.$o.''].' / '.$_POST['modelo'.$i.''.$o.''].' / '.$_POST['valoruf'.$i.''.$o.''].' / '.$_POST['valorclp'.$i.''.$o.''].' / '.$_POST['paradas'.$i.''.$o.''].' / '.$_POST['capper'.$i.''.$o.''].' / '.$_POST['capkg'.$i.''.$o.''].' / '.$_POST['velocidad'.$i.''.$o.''].' / '.$_POST['pservicio'.$i.''.$o.''].' / '.$_POST['gtecnica'.$i.''.$o.''].' / '.$_POST['ken'.$i.''.$o.''].' / '.$_POST['dcs'.$i.''.$o.''].' / '.$_POST['elink'.$i.''.$o.''].'<br/>';
                    //$ascensor->insertar($iduser, $idconedi, $_POST['idtascensor'.$i.''.$o.''],$_POST['marca'.$i.''.$o.''],$_POST['modelo'.$i.''.$o.''],$_POST['valoruf'.$i.''.$o.''],$_POST['valorclp'.$i.''.$o.'']);
                    
                    $pfservicio = empty( $pfservicio= $_POST['ascensor_' . $o . '__pservicio']) ? "0000-00-00" :  $pfservicio= $_POST['ascensor_' . $o . '__pservicio'];
                    $gftecnica = empty($pfservicio= $_POST['ascensor_' . $o . '__pservicio']) ? "0000-00-00" : $pfservicio= $_POST['ascensor_' . $o . '__pservicio'];
                    $respas = $CI->editarAscensorContrato($iduser, $_POST['ascensor_' . $o . '__idascensor'], $idedificio, $rspta,  $_POST['ascensor_' . $o . '__dcs'], $_POST['ascensor_' . $o . '__elink'], $pfservicio, $gftecnica, 1, $updated_user);
                    if ($respas) {
                        echo "- Ascensor ". $_POST['ascensor_' . $o . '__codigo'] ." guardado <br>";
                        $val += 1;
                    } else {
                        echo "Error ascensor ". $_POST['ascensor_' . $o . '__codigo'] ." <br>";
                    }
                }
            }
        }
        
        if($val != 0){
            echo "EXITO!";
        }else{
            echo "ERROR!";
        }
        
        break;

    case 'guardar':
        
        $idcontrato = isset($_POST["idcontrato"]) ? $_POST["idcontrato"] : "";
        $idedificio = isset($_POST["idedificio"]) ? $_POST["idedificio"] : "";
        
        $ubicacion = empty($ubicacion) ? "N/S" : $ubicacion; 
        $fecha_ini = empty($fecha_ini) ? "0000-00-00" : $fecha_ini;
        $fecha_fin = empty($fecha_fin) ? "0000-00-00" : $fecha_fin;
        
        if (isset($idedificio) && isset($idcontrato)) {
            for ($o = 0; $o < (int) $_POST['nascensores']; $o++) {
                $updated_user = $_SESSION['iduser'];
                $iduser = $_SESSION['iduser'];
                
                $pfservicio = empty( $pfservicio= $_POST['ascensor_' . $o . '__pservicio']) ? "0000-00-00" :  $pfservicio= $_POST['ascensor_' . $o . '__pservicio'];
                $gftecnica = empty($pfservicio= $_POST['ascensor_' . $o . '__pservicio']) ? "0000-00-00" : $pfservicio= $_POST['ascensor_' . $o . '__pservicio'];
                
                $respas = $CI->editarAscensorContrato($iduser, $_POST['ascensor_' . $o . '__idascensor'], $idedificio, $idcontrato,  $_POST['ascensor_' . $o . '__ubicacion'], $_POST['ascensor_' . $o . '__codigocli'], $pfservicio, $gftecnica, 1, $updated_user);
                if ($respas) {
                    echo "- Ascensor ". $_POST['ascensor_' . $o . '__codigo'] ." guardado <br>";
                } else {
                    echo "Error ascensor ". $_POST['ascensor_' . $o . '__codigo'] ." <br>";
                }
            }
        }
        
        break;
        
    case 'listar':
        $rspta = $CI->Listar();

        $data = Array();

        while ($reg = $rspta->fetch_object()) {

            if ($reg->estado == 13) {
                $op = '<button class="btn btn-info btn-xs" onclick="mostrar(' . $reg->idproyecto . ')" data-tooltip="tooltip" title="Crear Contrato"><i class="fa fa-list-alt"></i></button>';
            } elseif ($reg->estado == 12) {
                $op = '<button class="btn btn-info btn-xs" onclick="mostrar(' . $reg->idproyecto . ')" data-tooltip="tooltip" title="Crear Contrato"><i class="fa fa-list-alt"></i></button>' .
                      '<button class="btn btn-warning btn-xs" onclick="ficha(' . $reg->idproyecto . ')" data-tooltip="tooltip" title="Ficha proyecto" data-toggle="modal" data-target="#revisar"><i class="fa fa-eye"></i></button>';
            } elseif ($reg->estado == 11) {
                $op = '<button class="btn btn-warning btn-xs" onclick="ficha(' . $reg->idproyecto . ')" data-tooltip="tooltip" title="Ficha proyecto" data-toggle="modal" data-target="#revisar"><i class="fa fa-eye"></i></button>';
            } else{
                $op = '<button class="btn btn-info btn-xs" onclick="mostrar(' . $reg->idproyecto . ')" data-tooltip="tooltip" title="Crear Contrato"><i class="fa fa-list-alt"></i></button>' .
                      '<button class="btn btn-warning btn-xs" onclick="ficha(' . $reg->idproyecto . ')" data-tooltip="tooltip" title="Ficha proyecto" data-toggle="modal" data-target="#revisar"><i class="fa fa-eye"></i></button>';
            }

            $data[] = array(
                "0" => $op,
                "1" => $reg->nombre,
                "2" => $reg->codigoimp,
                "3" => $reg->razon_social,
                "4" => $reg->calle . " #" . $reg->numero . ", " . $reg->region_nombre,
                "5" => $reg->ascensores,
                "6" => $reg->ascensoresDefinitiva,
                "7" => $reg->ascensoresScontrato
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;

    case 'mostrar':
        $rspta = $CI->Mostrar($idproyecto);
        echo json_encode($rspta);
        break;
    
    case 'revisar':
        $rspta = $CI->revisar($idproyecto);
        echo json_encode($rspta);
        break;
    
    case 'listascensor':
        $rspta = $CI->listaAscensor($idproyecto);
        $data = Array();
        $counAscensor = 0;
        while ($reg = $rspta->fetch_object()) {

            $hidden = "<input type='hidden' name='ascensor_" . $counAscensor . "__idascensor' id='ascensor_" . $counAscensor . "__idascensor' value='" . $reg->idascensor . "' />" .
                    "<input type='hidden' name='ascensor_" . $counAscensor . "__codigo' id='ascensor_" . $counAscensor . "__codigo' value='" . $reg->codigo . "' />" .
                    "<input type='hidden' name='ascensor[].idascensor' id='ascensor_" . $counAscensor . "__idtascensor' value='" . $reg->idtascensor . "' />" .
                    "<input type='hidden' name='ascensor[].marca' id='ascensor_" . $counAscensor . "__marca' value='" . $reg->marca . "' />" .
                    "<input type='hidden' name='ascensor[].modelo' id='ascensor_" . $counAscensor . "__modelo' value='" . $reg->modelo . "' />" .
                    "<input type='hidden' name='ascensor[].ken' id='ascensor_" . $counAscensor . "__ken' value='" . $reg->ken . "' />" .
                    "<input type='hidden' name='ascensor[].valoruf' id='ascensor_" . $counAscensor . "__valoruf' value='" . $reg->valoruf . "' />" .
                    "<input type='hidden' name='ascensor[].valorcl' id='ascensor_" . $counAscensor . "__valorclp' value='" . $reg->valorclp . "' />" .
                    "<input type='hidden' name='ascensor[].paradas' id='ascensor_" . $counAscensor . "__paradas' value='" . $reg->paradas . "' />" .
                    "<input type='hidden' name='ascensor[].capkg' id='ascensor_" . $counAscensor . "__capkg' value='" . $reg->capkg . "' />" .
                    "<input type='hidden' name='ascensor[].capper' id='ascensor_" . $counAscensor . "__capper' value='" . $reg->capper . "' />" .
                    "<input type='hidden' name='ascensor[].velocidad' id='ascensor_" . $counAscensor . "__velocidad' value='" . $reg->velocidad . "' />".
                    "<input type='hidden' name='ascensor[].dcs' id='ascensor_" . $counAscensor . "__dcs' value='". $reg->dcs . "' />".
                    "<input type='hidden' name='ascensor[].elink' id='ascensor_" . $counAscensor . "__elink' value='". $reg->elink . "' />";

            $data[] = array(
                "0" => $counAscensor + 1,
                "1" => $hidden . $reg->codigo,
                "2" => "<input type='date' class='form-control' name='ascensor_" . $counAscensor . "__pservicio' id='ascensor_" . $counAscensor . "__pservicio' value='" . $reg->pservicio . "' required='Campo requerido' />",
                "3" => "<input type='date' class='form-control' name='ascensor_" . $counAscensor . "__gtecnica' id='ascensor_" . $counAscensor . "__gtecnica' value='" . $reg->gtecnica . "' required='Campo requerido'/>",
                "4" => "<input type='text' class='form-control' name='ascensor_" . $counAscensor . "__ubicacion' id='ascensor_" . $counAscensor . "__ubicacion' value='" . $reg->ubicacion . "' required='Campo requerido'/>",
                "5" => "<input type='text' class='form-control' name='ascensor_" . $counAscensor . "__codigocli' id='ascensor_" . $counAscensor . "__codigocli' value='" . $reg->codigocli . "' required='Campo requerido'/>"
            );

            $counAscensor += 1;
        }

        echo json_encode($data);
        break;
        
        
    case 'validarncontrato':
        $rspta = $CI->validarContrato($ncontrato);
        echo $rspta;
        break;
}