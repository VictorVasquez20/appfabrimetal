<?php

session_start();
require_once "../modelos/Proyecto.php";
require_once "../modelos/AnalisisFalla.php";
require_once "../modelos/DocsAsociados.php";
require_once "../modelos/Usuario.php";
//cargo archivo con funcion SendEmail para envio de correo con PhpMailer
require_once("../public/build/lib/fabrimetal/mail.php");

$proyecto = new Proyecto();
$analisisFalla = new AnalisisFalla();
$docsAsociados = new DocsAsociados();
$usuario = new Usuario();

$obra=isset($_POST["obra"])?limpiarCadena($_POST["obra"]):"";
$tipoFalla=isset($_POST["tipofalla"])?limpiarCadena($_POST["tipofalla"]):"";
$descripcionFalla=isset($_POST["descripcionFalla"])?limpiarCadena($_POST["descripcionFalla"]):"";
$imputable=isset($_POST["imputable"])?limpiarCadena($_POST["imputable"]):"";
$requerimiento=isset($_POST["requerimiento"])?limpiarCadena($_POST["requerimiento"]):"";
$descRequerimiento=isset($_POST["descRequerimiento"])?limpiarCadena($_POST["descRequerimiento"]):"";
$responsable=isset($_POST["responsable"])?limpiarCadena($_POST["responsable"]):"";
$dTime = isset($_POST["dTime"]) ? limpiarCadena($_POST["dTime"]) : "";
$imgs[] = isset($_POST["file01"]) ? limpiarCadena($_POST["file01"]) : "";
$imgs[] = isset($_POST["file02"]) ? limpiarCadena($_POST["file02"]) : "";
$imgs[] = isset($_POST["file03"]) ? limpiarCadena($_POST["file03"]) : "";

switch ($_GET["op"]) {

    case 'cargaObra':
            $rspta = $proyecto->listar($_SESSION['rut'], 1);
            // $rspta = $proyecto->listar2();
            echo '<option value="" selected disabled>Seleccione obra</option>';
            while($reg = $rspta->fetch_object()){
                echo '<option value='.$reg->idproyecto.'>'.$reg->nombre. ' / '. $reg->codigo.'</option>';
            }
    break;

    case 'responsables':
        $rspta = $analisisFalla->responsables();
        echo '<option value="" selected disabled>Seleccione responsable</option>';
        while($reg = $rspta->fetch_object()){
            echo '<option value='.$reg->iduser.'>'.$reg->nombre. ' '. $reg->apellido.'</option>';
        }
    break;

    case 'guardarAnalisis':
        $rspta=$analisisFalla->insertar($obra, $tipoFalla, $descripcionFalla, $imputable, $requerimiento, $descRequerimiento,$responsable,$imgs,$dTime);
        echo $rspta;
    break;

    case 'listaranalisis':
        
        $rspta = $analisisFalla->listaranalisis();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $op = '<button class="btn btn-info btn-xs" onclick="detalle(\'' . $reg->descripcionfalla . '\',\'' . $reg->descrequerimiento . '\')" data-toggle="tooltip" data-placement="top" title data-original-title="Ver Detalle"><i class="fa fa-list"></i></button>
            <button type="button" class="btn btn-primary btn-xs" onclick="MostrarImagenes('.$reg->idanalisis_falla.')"><i class="fa fa-file-image-o"></i></button>
            <button class="btn btn-success btn-xs" onclick="aprobar(' . $reg->idanalisis_falla . ')" data-toggle="tooltip" data-placement="top" title data-original-title="Aprobar"><i class="fa fa-check"></i></button>
            <button class="btn btn-danger btn-xs" onclick="rechazar(' . $reg->idanalisis_falla . ')" data-toggle="tooltip" data-placement="top" title data-original-title="Rechazar"><i class="fa fa-times"></i></button>';

            if ($reg->requerimiento == "SOLCOMPONENTE"){
                $reque = "SOLICITUD DE COMPONENTE";
            }
            elseif ($reg->requerimiento == "REPCOMPONENTE"){
                $reque = "REPARACIÓN DE COMPONENTE";
            }
            elseif ($reg->requerimiento == "SOLPERSONAL"){
                $reque = "SOLICITUD DE PERSONAL";
            }
            elseif ($reg->requerimiento == "EXTERNO"){
                $reque = "CONTRATACIÓN DE EXTERNO";
            }
            elseif ($reg->requerimiento == "REINSTALACION"){
                $reque = "RE-INSTALACIÓN";
            }
            elseif ($reg->requerimiento == "OTRO"){
                $reque = "OTRO";
            }

            $data[] = array(
                "0" => $op,
                "1" => $reg->obra,
                "2" => $reg->tipofalla,
                "3" => $reg->imputable,
                "4" => $reque,
                "5" => $reg->responsable,
                "6" => $reg->fecha
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
    break;

    case 'guardarSup':
        $idanalisis = isset($_POST["idanalisisfalla"]) ? limpiarCadena($_POST["idanalisisfalla"]) : "";
        $destinatarios = isset($_POST["destinatarios"]) ? limpiarCadena($_POST["destinatarios"]) : "";
        $obs_ji = isset($_POST["obs_ji"]) ? limpiarCadena($_POST["obs_ji"]) : "";
        
        $rspta = $analisisFalla->guardarSup($idanalisis, $obs_ji);
        if ($rspta) {
            $resp = $analisisFalla->listaranalisis2($idanalisis);
            $idusuario = $resp["idresponsable"];
            $emailusuario = $resp["email"];
            $body = '
            <html>
            <head>
                <meta charset="utf-8">
                <title>Solicitud de Análisis de Falla N° ' . $resp["idanalisis_falla"] . ' </title>                
            </head>

            <body>
                <div class="invoice-box">
                    <table cellpadding="0" cellspacing="0">
                        <tr class="top">
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td class="title">
                                            <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wgARCAA+ASsDAREAAhEBAxEB/8QAHAABAAIDAQEBAAAAAAAAAAAAAAMFBAYHAgEI/8QAGgEBAAMBAQEAAAAAAAAAAAAAAAECAwQGBf/aAAwDAQACEAMQAAAB7l43Kp+bUAADI6J6L7zYAAAAAAAAAAAAACt+fXHwAAAe7rf6lgAAAAAAAAAAAAANL8jlU/OqAABkbT0L3OwAAAAAAAAAAAAAGkeOyqPm1z+2c322wAExtgAAAAAAAAAAAAABW/Prj4JdUv0rAAfTPAAAAAAAAAAAAAANN8nlVfOjO7JzPY6gASm0H53pfDvTaL0wsNqO9N0W5pfO3pb4t5tnjUvtm+VLz79GtTQZjVF75XfTn1LQa59cy04/rnYzHQKX6kADRvG5U/zK2HdOd7fYACU24/MFL7LenR7U4Zlrtl66plrx3fC4pa5W6fplyLPTrGudtlpt8xpedvzdpN8p+rtcs7j66rt49e5eqq1x0qt7SY2oq712KluzIhynzSPsvWspJACUiJQREpDDCkMwnR5iYpj0mUiMKHyWWiaJ+THlMpHDHlAe0ZKZz//EACgQAAAGAAYCAgIDAAAAAAAAAAACAwQFBgEHEhMVFhBAFyYRNhQgJ//aAAgBAQABBQKUm1WLvs647OuOzrjs647OuOzrjs64b2JZZx7q2LPXqjhqjhqjhqjhqjhqjhqjgTFhq92Tg1Xzvq646uuOrrjq646uuOrrjq64b1tZFf3ZScXYu+0OgwsDh085lYcysOZWHMrDmVglLKnV91ZVoU+/HhJZkZTcajcajcajcajcajBRt+fdkoI7511ZUMa8o0d8KccKccKccKccKcJRB01BZZ6xKXD74K8jcjSdun5s1nx75hhWbS7k6REyd3mWH3zAGv0orQkz3tVN7IXeJauro9UcOsxZKLtDrMODbRNevMvMWqGm7pYGv3wRTe2YQsj8gRbCIXv01HHfyUDTWLy9SDOPsFliLG+n3iGYH9JWbXZPOzOxHzzh085hccwuOYXHMLjmFwlKrHVE1WW1pzM+GIIViqM6m2aVVtfrV8MQQr7Xr4iS1LFhiSifgiKhMqU8m4Zdm8onU38+dsvOVEmB7w2y9g2ks8w/1aGLV/4uihim2KNnI65fqeXf6bmsubrpMmITTbsvGdSjrNY20RfvmSBCubKL+UPIPuXBkiGx2ExgkQuO2UbZRtlG2UbZRoL42ibngiRE/Gynq4xmOMZjFBMyYMXA5dhMFSIQw2ibnGtMRxjMIt0m+Bi4HKQhUynSIp4OQqpVGaCw4xmCR7VM2wnuj//EADIRAAEDAwEFBAkFAAAAAAAAAAEAAhIDESEQIjFAQWETUbHwBBQjMDJCUHGRIFJygdH/2gAIAQMBAT8BLrKampqampqanx2FhYWFhYWFhY44tuoFQKgVAqBUCoFQ44usVMoOJP0TCwsfRC26ggy3uKlmhnXR1/lT7Na3vOlEXrdk/vQN9KY2qjXfKgZC6DS7AUvYT53AVVtnAM7kzaynEXZHcU+03NHI6M+Pa3ICSDpKi0udtIOlkKpdrZ9VU2WMI5k+H6S4hTKDiT7is63Zjzz0jmSqu9rDuGjGw9LZbnZC/PSjaVUt3Km6TQ4pzSRsI29V2f3BVsVm/wAf8Ttveqm+j55lVL9q+/fo2zdgKl8X9HwVLcfuUDCm9/RC9sosFRjh0RvUoUndT4aVHRpk81U3+ev55fn7cP04H//EADwRAAAEAgQKBggHAAAAAAAAAAABAgMEEQUSUtEQExYhMUFRYWKhFSMwcaLwICIyQFCBkbEUJDNCU8Hx/9oACAECAQE/AaRpVyEfxaUkf1HT79gud46ffsFzvHT79gud46ffsFzvGUD9gud4ygfsFzvHT79gud4h6ceW8lFQs5ltv9+eODrddVnvkJ0dweETo7g8InR3B4ROjuDwidHcHhE6O4PCJ0dweEJOArFVqT+Xv1IUM7GP41KiIZOP2yGTj9shk4/bIZOP2yGTj9shk4/bIZOP2yDFAPNOpcNZZjI/fqQph+EfxSCKUi23jKKKsp53iCpuIiYhDSklI++/4I65BpV1xpnvkMdR1pHINuwRrImzTPdL4JSFDORj+NSoiGTjv8hCDoNyGfS8ayzdg3NdY9mBMs9YIms17CB5g+cmycRrIGmrgcPM2pP7goqpyBqJGcxV/MG3qlMNKJSJr2hc0nLSJSbcUelIkZJIz14F+wVXTPkFKJJTMKTV0h9RJL1NwUmqcjDclKNGuQa9c3J6vRpGl4iEfxSCKWYZQxexPO8QVNRMREIaWRSPzt7BhMycPYeA1TSSQynqcZtM8ClGuEOeo/P2By1YHZ9SStP+hxNVRpIIWksywmt+KVWs3hjPDn33hHVlIgn9KI86iBSqlLAuausPWHvZ+Zfcg7pLuIVcY8hAOU8wS4bbqD3hEm3Xk+dWBtNdyR6A3/d0+7X9O/AbSFaSGKaskCbQWckifabvS39pv9H/xABBEAABAwICBgMNBwIHAAAAAAABAAIDBBEFEhMhMTIzkRBBoRQiIzRAUXGBk6KywdIGNUJSYWLRFSAkc3SChbHh/9oACAEBAAY/AjE1jHCw2rhR9q4UfauFH2rhR9q4UfauFH2rhR9qjjMcdnOA6/LvDaHP++11tpvdW2m91bab3VtpvdW2m91bab3VtpvdQymnzdVsvlxlbI1osBYrjRrjRrjRrjRrjRrjRrjRqN5lYQ1wPlxiYyMtsN4Lhxcj/KjicyMNceoFbrFusW6xbrFusTGlrLE28utM6EP/AH2ut+m5tQEb4C/qykXW9F2Lei7FvRdi3ouxb0XYhZ0V/V5cZWyNaLAWK4zOSjlMrSGnZZcRq4jVxGriNXEamOzjUb9EuF4RWQUzGQtf4ZrbcyF98Ybzj+lQvxDEKOegB8IIg255NUWE4JUwUxbBpZHTBtu1fe+Hc4/pVZXTOaa+lbKHHL+JouNSjq4cWoWxybBJowfhX3vh3OP6VUV4McWIQVPcxka24P62TXjF8Os4X16P6VJWTYnh80UIzujGTvh5ti+yTocsUWJHw8eW/mHzWJxTUndWFUzw1xib30Q86bXisbK1+5EziE+ay7nqKYUVG+mdLHA5vfW6nXTqqmxSijizluWUMafhX3xhvOP6ViArKylfiDh/hXtaMrfTqVRVzYhSaKBhkdlay9h/tUNbT4hS6GUXbnYwH4VUVeJyRTYjBE5xcwd6T+H5KKpZitAxkrcwa/Rg/CsMpsZraWppax5j8CG6j1bAqDCmub3HLSmRzcuvNc9fq/tMTAwtsNoW7FyP8qKJzY8rj1BbGclsZyWxnJbGclsZyTGkMsTbZ0VVLVmVsQpmvvEbH/pcau9o36VLBRume2R2c6Z11j9TWunZDBKIozEQPl+i41d7Rv0r7Y4H3xjZTulizbSMp/8AFH/UX4mKz8egy5PUuJjPuKpDmObC6uvFmFiW6taY4VFYyV7L3L2kA281kJMXpJ8Qwu48PSPtYfuC+xD6JuWkLjoxa1h3q+1TXNu0uaCCnYgyjGlOsMJuxp84C/44/NH+ruxEVec+LZctvWuJjPuLR4aZtHSgRkTjvv0WL/6WT4Vhn+X81FRx3L6ydkVghmmrc3X4Rv0qPFcOkqXy08zHO0rgQBf0LCMUqA/uc0APeC51ly3av2Q/lYZS4VE52mnbHN3Qz8JPVYqR/dBGXU2O53/yZdnrt6+jW1p9IXDbyVwxoPoW6OS3RyW6OS3RyW6OS3Ry6NJkbn2Zra+k5Ghtzc2G3oc7I3M4WJttC8Ug9mF4pB7MLRmNpj/KRq6C1wDmnaCmeDb3m7q3fQnOaxoc7aQNvRpMjc9rZra14rD7MLxSD2YREUTIwfyNsi1wuDtBQaxoa0bAEM7Q6xuLjZ0Fr2hzT1FDSQRvtqGZoNl4pB7MIObTRNcNhDAtLo26T89tfR//xAAoEAEAAgEDAwQCAwEBAAAAAAABABEhMUHxEFHRYXGR8ECBILHB4aH/2gAIAQEAAT8hAGUu95PecP5Th/KcP5Th/KcP5Th/KfX8ocktTLLXf84ztjz505GORjkY5GORjkY5GN12ZbXtX5zprkN4J9hn2GfYZ9hn2GfYZ9hinJoXmm/ziJJLS8nv0EVLpLc095wb5nBvmcG+Zwb5nBvmHkDVD39/zm9GZF/Z04QQdHxVTnY52OdjnY52GlmcU2v85uqoDtOSSwfpCzOEZwjOEZwjOEYhXDpT36VHfJFZnIznpCwKF5wYBDet5nyEYK6KWsVp3iY6cIJ+8AuIjHaGeZQjzWSGpdOEsdR+jwVm/tCkgG1szlz2WhrQL8Iww8jBbNQuTUxlvvL6u9+v/kwQaycO98Sy7mocJsNRZzn09N5T2XDE9FYzKkIgzbRM4Eb2wxZibVQtonY1XA9Sa04cM2lFf0hKwVpHuaJUbYtw6kUbSADMcW1O7b/ENca2XJ79Kg0lJUvT3nJPM5J5nJPM5J5nJPMrYGqXf36FX3DsANVdK1hs6LuqxQEvjjzUERtaA+elZwTrDaNdhuPwj3VGvfYyL0llVu2PGHQCvdM/dwywQgH7Je1zMvatlZKL09T0ey3lV/YimCaODYjeJdL39GYv0qLXDUitt6fT6LvWcD4zS9LUFZLo3T8Q3XnzIK+7VBgAXczdfNTZ6GFVwf4a3MMHeo276Ey4U79HJ/zGFAHrc7yyR21tsz2HBqZ9EJYHe4M4XAgBuD+Tve94Jxj6AELJarDtfXsN5i3d6KNbA++wvafY/wDJ9j/yHzbQr8JpDghQLEl78H/8JoFGwfc79F7OxbDtfaLlXXf/AIz7H/ky4EQL/ECAagWJMLRB0H6nYb3Nu501yjHY/qPuj6GOxc+x/wCRC2WOR+IuUpKKcffp/9oADAMBAAIAAwAAABCkkliySSSSSSSSSSSSSQ7bbYSSSSSSSSSSSSSSR2223ySSSSSSSSSSSSSRnySQSSSSSSSSSSSSSSQ6gACSSSSSSSSSSSSSSRhAAAQhIicNBfYZJXWSRCgACTj0aNFS6FOadIAGfrSSSSSeQFsQeNuTycT/xAAqEQEAAgAEBAUFAQEAAAAAAAABABEhMVGBEEFhoXGRscHwMEBQ0fEg4f/aAAgBAwEBPxB1UtpLaS2ktpLaS2ktpBKFffOqbZtm2bZtm2bZ01986snWnWnWnWnWnWnWghG/vnoOEYH8ItsZfTBvhX4R1dy+sRDf0BWGJey+xwtq2Nl+EpiZrsYez2qBbUd8SibNfy4d65NeUC8IuZosd6/faGJycZYpTT2Lir8vpKWmJjBopr180PDdlCpoM75fPmEVUxM9TE9Rqu8Qy1D39HhSzye+NPpf9t1RovkX7QgpyU8ogMuLsF12a2hGqhlKHAA74ds76Q8zCfAD/lyjhGB+ggPmPquABjNo8r/cYByC98vPHgTch7Efdmf19uUM8YtGsL29q3uVFzBdyXxWd/nmRvQ92NWaIxh/rq/K3lQDWHTFq8Ou98PDXpMz4Yp8BqwVuXu/5Ljz1j4xT5/w+s6AI+Q9XgBOTLT45ShQaDN05NwKsbyVdC0LlDKJR9Bxq+XHNt4W2PM4mBRArAgpiSitEcW3hnV8ot4vCi7go2QAymZXLhdZQwKMuCDnHGr5cP/EACoRAQABAwIEBgIDAQAAAAAAAAERACExQWEQkbHwUXGBodHxIMEwQFDh/9oACAECAQE/EFpwBluubJ+ckkkggmtSPsQjF10P7zLuexpm9b9W/Vv1b9W/Vv1b9T5NwiL50iLzOP7yAYgQzNiNCuzfiuzfiuzfiuzfiuzfiuzfiuzfiksDWNEfD+82XCXJXJ0HSvpqSLVhglh8V0/xIyLeltm9fYVbH0yn6Rf/ABEARAhHQivrGk9BzAPh/BGZsvg4Q2NLedTqcJ0+Y9KUF8KK3Qn3hzzvTQnUnnSwTQNnDMc+/wB0qvIxQgMkhzYPdoI/BjzhTlEc9oxClE+tjkL45zUESTiNfju1CeyQ7DAxyT/lDEmE9T9cMCzNyW88943EBzQPdpEGoHnejLasPVQX3mkmrlTyZE9L9D3KtWmR7dZ9jefwdqgVxW5slfRUG0rDAzh3fwApnqRwVDBPv9UcVmJ5GepHrwZM431TpDnWPb760zFqMNJZ81X1tUzcKcmkIYdPB6d6JRE28un/ADQBNTU9HxNh7d4tV/p0SvnF/O+PSKLUC0E8zTknPMzWGm7h4FSD1Z/X7q806VoIy6nOpJ2Dt5cHDSvnITy8aisLMgNzN5JwiMpiVISa7g19EfFJgh8irKGT8i0xrxwQY4QQ6HPFuy5rNIOalnxUWIOBaY1oAIOEsRSDZpVu1hnXgg5rL4uApii0xrw//8QAKBABAQACAQMDBAIDAQAAAAAAAREAITEQQVFh0fEgQHGBkaEwscHw/9oACAEBAAE/EB/zTs0eAfXp06dOnT8biMv3YApPLf3xMOELTNXlP8BhhhhhhizSvNH2N2yT74RMwNkOxnxHtz4j258R7c+I9ufEe3PiPbnxHtwroNWBQa9PvgWDUbo8E/roOQSA4JTqs7ePq379+/enmErBAz9vvj4MIeJq8p0eSFEYJn5P4+o88889mFANytSd798WmYkkTt07SAgkdE1fz9RRRRRSkdYsgGf10Lp5ZALX2hAhDJgBHXRkuhRpcM2nvGFmiwgAq7dRAuBYUcLMeIxgDYtAByOqh7Jbc2ecaSYrMEFq/Kt3Kkuu5A1BrVogASnB3lrBpWRBdTU8AjMWtUgGisIgjdFUtUI/MeQAsKrxoA8ElDLUASPuggKbaVLtZolZS4E1ATuOW6GNLEOCKq9EDtkVybLlQbQStNY4MA3VoGsGFMBJmdhJocaPfDlZr5kAXaGFbyR7sXqFtTtXCzCKmAloKCoi+MQu6nEgiIQBm2jqfQeezXKOwf66DFzdNwU6V+PH1SJEiRIPaTKIgZ+3RZbUMWos29ujE+pxlJJHEPFbt4jyyNzxLewBx6MUzHoFLwBeBDlrEleBk1yXY5752RLtG5HX31WCBuk30hoMMh3Q81iguwzVOcUiMiQ3NkRuae7gzYq0GjYIG74WvOaXSBWFDpEuu+NmCVVVX6T1oTTa2pxFDXJybyJkaO88j9YE4u/RWPG0WLgbVQErFCuExYSJu7wGCWmzxPVGNj/G3RueNwzRI0lCNoe954xiTkPhA4CLvp7Zl+PvLpNzQmuMKuobS1k77HLQyKQpHuYilMtP5c/8d/zHBxoZP3MN3Z9GfAvbPgXtnwL2z4F7ZXgdhAI/x0Ggllvmiz0vVtrs115gbdG3fSrCg/BJX0OuqBAVGQFIcAya/GAAAANAYjgRZIiI6R8YqV3ke0nZ2a1MewozT4gWPXoDSMFzd4LsurMehFUyvQgcEoImcKAuOJ8WSIiOkTthWLgz+AaDG2uSTXihpOyb6DR2Fn8K042Jq/hAMPQ6IB12I+4RKHKYa1h4E2bdXp//2Q==" style="width:100%; max-width:300px;"><br><br>
                                            <b>SOLICITUD DE ANÁLISIS DE FALLA N° ' . $resp["idanalisis_falla"] . '</b><br>
                                        </td>              
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <br>
                        <br>
                        <tr class="heading">
                            <td>
                                INFORMACION DE LA SOLICITUD
                            </td>            
                        </tr>
                        <br/>
                        <br/>                        

                        <tr class="item">
                            <td>
                                <b>FECHA Y HORA: </b>' . $resp["fecha"] . '
                            </td>
                        </tr>

                        <tr class="item">
                            <td>
                                <b>EDIFICIO: </b> ' . $resp["obra"] . '
                            </td>
                        </tr>
                      
                        <tr class="item">
                            <td>
                                <b>TIPO DE FALLA: </b> ' . $resp["tipofalla"] . '
                            </td>
                        </tr>

                        <tr class="item">
                            <td>
                                <b>DESCRIPCIÓN DE FALLA: </b> ' . $resp["descripcionfalla"] . '
                            </td>
                        </tr>

                        <tr class="item">
                            <td>
                                <b>REQUERIMIENTO: </b> ' . $resp["requerimiento"] . '
                            </td>
                        </tr>

                        <tr class="item">
                            <td>
                                <b>DESCRIPCIÓN DE REQUERIMIENTO: </b> ' . $resp["descrequerimiento"] . '
                            </td>
                        </tr>

                        <tr class="item">
                            <td>
                                <b>SOLICITANTE: </b> ' . $resp["responsable"] . '
                            </td>
                        </tr>

                        <tr class="item">
                            <td>
                                <b>OBSERVACIONES JEFA DE INSTALACIONES: </b> ' . $resp["obs_ji"] . '
                            </td>
                        </tr>
                    </table>
                </div>
            </body>
            </html>
            ';

            //cargo la plantilla que genera el pdf
            require_once("../public/build/lib/TemplatePower/class.TemplatePower.php7.inc.php");
            $t = new TemplatePower("../production/informes/analisisfalla/pla_reporte_analisis_falla.html");
            $t->prepare();
            
            $t->assign('idanalisis_falla', $resp["idanalisis_falla"] . '');
            $t->assign('fecha', $resp["fecha"] . '');
            $t->assign('obra', $resp["obra"] . '');
            $t->assign('tipofalla', $resp["tipofalla"] . '');
            $t->assign('descripcionfalla', $resp["descripcionfalla"] . '');
            $t->assign('requerimiento', $resp["requerimiento"] . '');
            $t->assign('descrequerimiento', $resp["descrequerimiento"] . '');
            $t->assign('responsable', $resp["responsable"] . '');
            $t->assign('obs_ji', $resp["obs_ji"] . '');

            //LISTO LAS IMAGENES DE LA VISITA
            $path_foto_visitas = '../files/docsasociados/';
            $filesvisita = '';
            $rspfotovis = $docsAsociados->BuscarImagenes('APPFABRIMETAL_ANALISISFALLA', $resp["idanalisis_falla"], 'analisisfalla');
            /* echo '<pre>';
            print_r($rspfotovis);
            echo '</pre>';
            exit(); */
            $filesvisita = '';
            // $rows3 = $rspfotovis->fetch_all(MYSQLI_ASSOC);

            while($item3 = $rspfotovis->fetch_object()){
                $fotovisita = $path_foto_visitas . $item3->documento;
                if (file_exists($fotovisita)) {
                    if (is_file($fotovisita)) {
                        $filesvisita .= '<img alt="" src="' . $fotovisita . '" style="margin: 5px; max-width: 400px; border: 1px solid grey; vertical-align: top;">';
                    }
                }
            }


            /* foreach($rows3 as $k => $item3)
            {
                $fotovisita = $path_foto_visitas . $item3['documento'];
                if (file_exists($fotovisita)) {
                    if (is_file($fotovisita)) {
                        $filesvisita .= '<img alt="" src="' . $fotovisita . '" style="margin: 5px; max-width: 400px; border: 1px solid grey; vertical-align: top;">';
                    }
                }
            } */
            $t->assign('filesvisita', $filesvisita . '');

            $style02 = '../production/informes/css/reportes_fm.css';

            require_once("../public/build/lib/mPdfSC/vendor/autoload.php");

            $mpdf = new \Mpdf\Mpdf([
                'mode' => 'c',
                'margin_left' => 20,
                'margin_right' => 20,
                'margin_top' => 32,
                'margin_bottom' => 20,
                'margin_header' => 16,
                'margin_footer' => 13
            ]);

            $mpdf->SetHTMLHeader('<img src="../production/informes/img/fm-logo-negro.png" height="40" alt="" style="float: left; display: inline-block;"> <div style="text-align: right">Página {PAGENO} / {nb}<div>', 'O', true);

            // $mpdf->SetDisplayMode('fullwidth');
            $mpdf->SetDisplayMode(100);
            // $mpdf->SetDisplayMode('fullpage');

            $mpdf->SetDisplayPreferences('/HideToolbar/CenterWindow/FitWindow');
            // $mpdf->SetDisplayPreferences('/HideMenubar/HideToolbar/CenterWindow/FitWindow');

            $mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list

            // $mpdf->shrink_tables_to_fit = 1;

            // $mpdf->ignore_table_percents = true;

            // $mpdf->ignore_table_width = true;
            
            // $mpdf->showImageErrors = true

            $mpdf->tableMinSizePriority = true;

            /*$mpdf->simpleTables = true;
            $mpdf->packTableData = true;
            $mpdf->keep_table_proportions = TRUE;
            $mpdf->shrink_tables_to_fit=1;*/

            // Load a stylesheet
            $stylesheet02 = file_get_contents($style02);

            $html1 = $t->getOutputContent();

            $mpdf->WriteHTML($stylesheet02, 1); // The parameter 1 tells that this is css/style only and no body/html/text
            $mpdf->WriteHTML($html1,2);

            $PROYECTO_REPORTE_PDF = 'REPORTE_DEL_ANALISIS_' . $resp["idanalisis_falla"] . '.pdf';

            ob_start();
            $mpdf->Output($PROYECTO_REPORTE_PDF, \Mpdf\Output\Destination::INLINE);

            // holds the buffer into a variable
            $html = ob_get_contents(); 
            ob_get_clean();

            //envio de email
            $myObj = new stdClass();
            $myObj->subject  = "App Fabrimetal - Sol. Análisis de Falla N° " . $resp["idanalisis_falla"];
            $myObj->message  = $body;
            $myObj->emails[] = (object) array(
                                                'email' => $emailusuario,
                                                'name' => ''
                                             );
            $myObj->emails[] = (object) array(
                                                'email' => $destinatarios,
                                                'name' => ''
                                             );
            $myObj->attachments[] = (object) array(
                                                        'string' => $html,
                                                        'filename' => $PROYECTO_REPORTE_PDF,
                                                        'encoding' => 'base64',
                                                        'type' => 'application/pdf'
                                                  );

            $result = SendEmail($myObj);

            echo $result->message;
        }

        echo $rspta ? "Solicitud aprobada" : "No se pudo aprobar la solicitud";
    break;

    case 'rechazar':
        $idanalisis = isset($_GET["idanalisisfalla"]) ? limpiarCadena($_GET["idanalisisfalla"]) : "";
        
        $rspta = $analisisFalla->rechazar($idanalisis);
        if ($rspta) {
            $resp = $analisisFalla->listaranalisis2($idanalisis);
            $idusuario = $resp["idresponsable"];
            $emailusuario = $resp["email"];
            /* $infousu = $usuario->mostrar($idusuario);
            $emailusuario = $infousu["email"]; */
            $body = '
            <html>
            <head>
                <meta charset="utf-8">
                <title>Solicitud de Análisis de Falla N° ' . $resp["idanalisis_falla"] . ' </title>                
            </head>

            <body>
                <div class="invoice-box">
                    <table cellpadding="0" cellspacing="0">
                        <tr class="top">
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td class="title">
                                            <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wgARCAA+ASsDAREAAhEBAxEB/8QAHAABAAIDAQEBAAAAAAAAAAAAAAMFBAYHAgEI/8QAGgEBAAMBAQEAAAAAAAAAAAAAAAECAwQGBf/aAAwDAQACEAMQAAAB7l43Kp+bUAADI6J6L7zYAAAAAAAAAAAAACt+fXHwAAAe7rf6lgAAAAAAAAAAAAANL8jlU/OqAABkbT0L3OwAAAAAAAAAAAAAGkeOyqPm1z+2c322wAExtgAAAAAAAAAAAAABW/Prj4JdUv0rAAfTPAAAAAAAAAAAAAANN8nlVfOjO7JzPY6gASm0H53pfDvTaL0wsNqO9N0W5pfO3pb4t5tnjUvtm+VLz79GtTQZjVF75XfTn1LQa59cy04/rnYzHQKX6kADRvG5U/zK2HdOd7fYACU24/MFL7LenR7U4Zlrtl66plrx3fC4pa5W6fplyLPTrGudtlpt8xpedvzdpN8p+rtcs7j66rt49e5eqq1x0qt7SY2oq712KluzIhynzSPsvWspJACUiJQREpDDCkMwnR5iYpj0mUiMKHyWWiaJ+THlMpHDHlAe0ZKZz//EACgQAAAGAAYCAgIDAAAAAAAAAAACAwQFBgEHEhMVFhBAFyYRNhQgJ//aAAgBAQABBQKUm1WLvs647OuOzrjs647OuOzrjs64b2JZZx7q2LPXqjhqjhqjhqjhqjhqjhqjgTFhq92Tg1Xzvq646uuOrrjq646uuOrrjq64b1tZFf3ZScXYu+0OgwsDh085lYcysOZWHMrDmVglLKnV91ZVoU+/HhJZkZTcajcajcajcajcajBRt+fdkoI7511ZUMa8o0d8KccKccKccKccKcJRB01BZZ6xKXD74K8jcjSdun5s1nx75hhWbS7k6REyd3mWH3zAGv0orQkz3tVN7IXeJauro9UcOsxZKLtDrMODbRNevMvMWqGm7pYGv3wRTe2YQsj8gRbCIXv01HHfyUDTWLy9SDOPsFliLG+n3iGYH9JWbXZPOzOxHzzh085hccwuOYXHMLjmFwlKrHVE1WW1pzM+GIIViqM6m2aVVtfrV8MQQr7Xr4iS1LFhiSifgiKhMqU8m4Zdm8onU38+dsvOVEmB7w2y9g2ks8w/1aGLV/4uihim2KNnI65fqeXf6bmsubrpMmITTbsvGdSjrNY20RfvmSBCubKL+UPIPuXBkiGx2ExgkQuO2UbZRtlG2UbZRoL42ibngiRE/Gynq4xmOMZjFBMyYMXA5dhMFSIQw2ibnGtMRxjMIt0m+Bi4HKQhUynSIp4OQqpVGaCw4xmCR7VM2wnuj//EADIRAAEDAwEFBAkFAAAAAAAAAAEAAhIDESEQIjFAQWETUbHwBBQjMDJCUHGRIFJygdH/2gAIAQMBAT8BLrKampqampqanx2FhYWFhYWFhY44tuoFQKgVAqBUCoFQ44usVMoOJP0TCwsfRC26ggy3uKlmhnXR1/lT7Na3vOlEXrdk/vQN9KY2qjXfKgZC6DS7AUvYT53AVVtnAM7kzaynEXZHcU+03NHI6M+Pa3ICSDpKi0udtIOlkKpdrZ9VU2WMI5k+H6S4hTKDiT7is63Zjzz0jmSqu9rDuGjGw9LZbnZC/PSjaVUt3Km6TQ4pzSRsI29V2f3BVsVm/wAf8Ttveqm+j55lVL9q+/fo2zdgKl8X9HwVLcfuUDCm9/RC9sosFRjh0RvUoUndT4aVHRpk81U3+ev55fn7cP04H//EADwRAAAEAgQKBggHAAAAAAAAAAABAgMEEQUSUtEQExYhMUFRYWKhFSMwcaLwICIyQFCBkbEUJDNCU8Hx/9oACAECAQE/AaRpVyEfxaUkf1HT79gud46ffsFzvHT79gud46ffsFzvGUD9gud4ygfsFzvHT79gud4h6ceW8lFQs5ltv9+eODrddVnvkJ0dweETo7g8InR3B4ROjuDwidHcHhE6O4PCJ0dweEJOArFVqT+Xv1IUM7GP41KiIZOP2yGTj9shk4/bIZOP2yGTj9shk4/bIZOP2yDFAPNOpcNZZjI/fqQph+EfxSCKUi23jKKKsp53iCpuIiYhDSklI++/4I65BpV1xpnvkMdR1pHINuwRrImzTPdL4JSFDORj+NSoiGTjv8hCDoNyGfS8ayzdg3NdY9mBMs9YIms17CB5g+cmycRrIGmrgcPM2pP7goqpyBqJGcxV/MG3qlMNKJSJr2hc0nLSJSbcUelIkZJIz14F+wVXTPkFKJJTMKTV0h9RJL1NwUmqcjDclKNGuQa9c3J6vRpGl4iEfxSCKWYZQxexPO8QVNRMREIaWRSPzt7BhMycPYeA1TSSQynqcZtM8ClGuEOeo/P2By1YHZ9SStP+hxNVRpIIWksywmt+KVWs3hjPDn33hHVlIgn9KI86iBSqlLAuausPWHvZ+Zfcg7pLuIVcY8hAOU8wS4bbqD3hEm3Xk+dWBtNdyR6A3/d0+7X9O/AbSFaSGKaskCbQWckifabvS39pv9H/xABBEAABAwICBgMNBwIHAAAAAAABAAIDBBEFEhMhMTIzkRBBoRQiIzRAUXGBk6KywdIGNUJSYWLRFSAkc3SChbHh/9oACAEBAAY/AjE1jHCw2rhR9q4UfauFH2rhR9q4UfauFH2rhR9qjjMcdnOA6/LvDaHP++11tpvdW2m91bab3VtpvdW2m91bab3VtpvdQymnzdVsvlxlbI1osBYrjRrjRrjRrjRrjRrjRrjRqN5lYQ1wPlxiYyMtsN4Lhxcj/KjicyMNceoFbrFusW6xbrFusTGlrLE28utM6EP/AH2ut+m5tQEb4C/qykXW9F2Lei7FvRdi3ouxb0XYhZ0V/V5cZWyNaLAWK4zOSjlMrSGnZZcRq4jVxGriNXEamOzjUb9EuF4RWQUzGQtf4ZrbcyF98Ybzj+lQvxDEKOegB8IIg255NUWE4JUwUxbBpZHTBtu1fe+Hc4/pVZXTOaa+lbKHHL+JouNSjq4cWoWxybBJowfhX3vh3OP6VUV4McWIQVPcxka24P62TXjF8Os4X16P6VJWTYnh80UIzujGTvh5ti+yTocsUWJHw8eW/mHzWJxTUndWFUzw1xib30Q86bXisbK1+5EziE+ay7nqKYUVG+mdLHA5vfW6nXTqqmxSijizluWUMafhX3xhvOP6ViArKylfiDh/hXtaMrfTqVRVzYhSaKBhkdlay9h/tUNbT4hS6GUXbnYwH4VUVeJyRTYjBE5xcwd6T+H5KKpZitAxkrcwa/Rg/CsMpsZraWppax5j8CG6j1bAqDCmub3HLSmRzcuvNc9fq/tMTAwtsNoW7FyP8qKJzY8rj1BbGclsZyWxnJbGclsZyTGkMsTbZ0VVLVmVsQpmvvEbH/pcau9o36VLBRume2R2c6Z11j9TWunZDBKIozEQPl+i41d7Rv0r7Y4H3xjZTulizbSMp/8AFH/UX4mKz8egy5PUuJjPuKpDmObC6uvFmFiW6taY4VFYyV7L3L2kA281kJMXpJ8Qwu48PSPtYfuC+xD6JuWkLjoxa1h3q+1TXNu0uaCCnYgyjGlOsMJuxp84C/44/NH+ruxEVec+LZctvWuJjPuLR4aZtHSgRkTjvv0WL/6WT4Vhn+X81FRx3L6ydkVghmmrc3X4Rv0qPFcOkqXy08zHO0rgQBf0LCMUqA/uc0APeC51ly3av2Q/lYZS4VE52mnbHN3Qz8JPVYqR/dBGXU2O53/yZdnrt6+jW1p9IXDbyVwxoPoW6OS3RyW6OS3RyW6OS3Ry6NJkbn2Zra+k5Ghtzc2G3oc7I3M4WJttC8Ug9mF4pB7MLRmNpj/KRq6C1wDmnaCmeDb3m7q3fQnOaxoc7aQNvRpMjc9rZra14rD7MLxSD2YREUTIwfyNsi1wuDtBQaxoa0bAEM7Q6xuLjZ0Fr2hzT1FDSQRvtqGZoNl4pB7MIObTRNcNhDAtLo26T89tfR//xAAoEAEAAgEDAwQCAwEBAAAAAAABABEhMUHxEFHRYXGR8ECBILHB4aH/2gAIAQEAAT8hAGUu95PecP5Th/KcP5Th/KcP5Th/KfX8ocktTLLXf84ztjz505GORjkY5GORjkY5GN12ZbXtX5zprkN4J9hn2GfYZ9hn2GfYZ9hinJoXmm/ziJJLS8nv0EVLpLc095wb5nBvmcG+Zwb5nBvmHkDVD39/zm9GZF/Z04QQdHxVTnY52OdjnY52GlmcU2v85uqoDtOSSwfpCzOEZwjOEZwjOEYhXDpT36VHfJFZnIznpCwKF5wYBDet5nyEYK6KWsVp3iY6cIJ+8AuIjHaGeZQjzWSGpdOEsdR+jwVm/tCkgG1szlz2WhrQL8Iww8jBbNQuTUxlvvL6u9+v/kwQaycO98Sy7mocJsNRZzn09N5T2XDE9FYzKkIgzbRM4Eb2wxZibVQtonY1XA9Sa04cM2lFf0hKwVpHuaJUbYtw6kUbSADMcW1O7b/ENca2XJ79Kg0lJUvT3nJPM5J5nJPM5J5nJPMrYGqXf36FX3DsANVdK1hs6LuqxQEvjjzUERtaA+elZwTrDaNdhuPwj3VGvfYyL0llVu2PGHQCvdM/dwywQgH7Je1zMvatlZKL09T0ey3lV/YimCaODYjeJdL39GYv0qLXDUitt6fT6LvWcD4zS9LUFZLo3T8Q3XnzIK+7VBgAXczdfNTZ6GFVwf4a3MMHeo276Ey4U79HJ/zGFAHrc7yyR21tsz2HBqZ9EJYHe4M4XAgBuD+Tve94Jxj6AELJarDtfXsN5i3d6KNbA++wvafY/wDJ9j/yHzbQr8JpDghQLEl78H/8JoFGwfc79F7OxbDtfaLlXXf/AIz7H/ky4EQL/ECAagWJMLRB0H6nYb3Nu501yjHY/qPuj6GOxc+x/wCRC2WOR+IuUpKKcffp/9oADAMBAAIAAwAAABCkkliySSSSSSSSSSSSSQ7bbYSSSSSSSSSSSSSSR2223ySSSSSSSSSSSSSRnySQSSSSSSSSSSSSSSQ6gACSSSSSSSSSSSSSSRhAAAQhIicNBfYZJXWSRCgACTj0aNFS6FOadIAGfrSSSSSeQFsQeNuTycT/xAAqEQEAAgAEBAUFAQEAAAAAAAABABEhMVGBEEFhoXGRscHwMEBQ0fEg4f/aAAgBAwEBPxB1UtpLaS2ktpLaS2ktpBKFffOqbZtm2bZtm2bZ01986snWnWnWnWnWnWnWghG/vnoOEYH8ItsZfTBvhX4R1dy+sRDf0BWGJey+xwtq2Nl+EpiZrsYez2qBbUd8SibNfy4d65NeUC8IuZosd6/faGJycZYpTT2Lir8vpKWmJjBopr180PDdlCpoM75fPmEVUxM9TE9Rqu8Qy1D39HhSzye+NPpf9t1RovkX7QgpyU8ogMuLsF12a2hGqhlKHAA74ds76Q8zCfAD/lyjhGB+ggPmPquABjNo8r/cYByC98vPHgTch7Efdmf19uUM8YtGsL29q3uVFzBdyXxWd/nmRvQ92NWaIxh/rq/K3lQDWHTFq8Ou98PDXpMz4Yp8BqwVuXu/5Ljz1j4xT5/w+s6AI+Q9XgBOTLT45ShQaDN05NwKsbyVdC0LlDKJR9Bxq+XHNt4W2PM4mBRArAgpiSitEcW3hnV8ot4vCi7go2QAymZXLhdZQwKMuCDnHGr5cP/EACoRAQABAwIEBgIDAQAAAAAAAAERACExQWEQkbHwUXGBodHxIMEwQFDh/9oACAECAQE/EFpwBluubJ+ckkkggmtSPsQjF10P7zLuexpm9b9W/Vv1b9W/Vv1b9T5NwiL50iLzOP7yAYgQzNiNCuzfiuzfiuzfiuzfiuzfiuzfiuzfiksDWNEfD+82XCXJXJ0HSvpqSLVhglh8V0/xIyLeltm9fYVbH0yn6Rf/ABEARAhHQivrGk9BzAPh/BGZsvg4Q2NLedTqcJ0+Y9KUF8KK3Qn3hzzvTQnUnnSwTQNnDMc+/wB0qvIxQgMkhzYPdoI/BjzhTlEc9oxClE+tjkL45zUESTiNfju1CeyQ7DAxyT/lDEmE9T9cMCzNyW88943EBzQPdpEGoHnejLasPVQX3mkmrlTyZE9L9D3KtWmR7dZ9jefwdqgVxW5slfRUG0rDAzh3fwApnqRwVDBPv9UcVmJ5GepHrwZM431TpDnWPb760zFqMNJZ81X1tUzcKcmkIYdPB6d6JRE28un/ADQBNTU9HxNh7d4tV/p0SvnF/O+PSKLUC0E8zTknPMzWGm7h4FSD1Z/X7q806VoIy6nOpJ2Dt5cHDSvnITy8aisLMgNzN5JwiMpiVISa7g19EfFJgh8irKGT8i0xrxwQY4QQ6HPFuy5rNIOalnxUWIOBaY1oAIOEsRSDZpVu1hnXgg5rL4uApii0xrw//8QAKBABAQACAQMDBAIDAQAAAAAAAREAITEQQVFh0fEgQHGBkaEwscHw/9oACAEBAAE/EB/zTs0eAfXp06dOnT8biMv3YApPLf3xMOELTNXlP8BhhhhhhizSvNH2N2yT74RMwNkOxnxHtz4j258R7c+I9ufEe3PiPbnxHtwroNWBQa9PvgWDUbo8E/roOQSA4JTqs7ePq379+/enmErBAz9vvj4MIeJq8p0eSFEYJn5P4+o88889mFANytSd798WmYkkTt07SAgkdE1fz9RRRRRSkdYsgGf10Lp5ZALX2hAhDJgBHXRkuhRpcM2nvGFmiwgAq7dRAuBYUcLMeIxgDYtAByOqh7Jbc2ecaSYrMEFq/Kt3Kkuu5A1BrVogASnB3lrBpWRBdTU8AjMWtUgGisIgjdFUtUI/MeQAsKrxoA8ElDLUASPuggKbaVLtZolZS4E1ATuOW6GNLEOCKq9EDtkVybLlQbQStNY4MA3VoGsGFMBJmdhJocaPfDlZr5kAXaGFbyR7sXqFtTtXCzCKmAloKCoi+MQu6nEgiIQBm2jqfQeezXKOwf66DFzdNwU6V+PH1SJEiRIPaTKIgZ+3RZbUMWos29ujE+pxlJJHEPFbt4jyyNzxLewBx6MUzHoFLwBeBDlrEleBk1yXY5752RLtG5HX31WCBuk30hoMMh3Q81iguwzVOcUiMiQ3NkRuae7gzYq0GjYIG74WvOaXSBWFDpEuu+NmCVVVX6T1oTTa2pxFDXJybyJkaO88j9YE4u/RWPG0WLgbVQErFCuExYSJu7wGCWmzxPVGNj/G3RueNwzRI0lCNoe954xiTkPhA4CLvp7Zl+PvLpNzQmuMKuobS1k77HLQyKQpHuYilMtP5c/8d/zHBxoZP3MN3Z9GfAvbPgXtnwL2z4F7ZXgdhAI/x0Ggllvmiz0vVtrs115gbdG3fSrCg/BJX0OuqBAVGQFIcAya/GAAAANAYjgRZIiI6R8YqV3ke0nZ2a1MewozT4gWPXoDSMFzd4LsurMehFUyvQgcEoImcKAuOJ8WSIiOkTthWLgz+AaDG2uSTXihpOyb6DR2Fn8K042Jq/hAMPQ6IB12I+4RKHKYa1h4E2bdXp//2Q==" style="width:100%; max-width:300px;"><br><br>
                                            <b>SOLICITUD DE ANÁLISIS DE FALLA N° ' . $resp["idanalisis_falla"] . '</b><br>
                                        </td>              
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class="heading">
                            <td style="color: red;">
                                <b>ESTIMADO(A): INFORMAMOS QUE LA SIGUIENTE SOLICITUD HA SIDO RECHAZADA.</b>
                            </td>            
                        </tr>
                        <tr class="heading">
                            <td>
                                INFORMACION DE LA SOLICITUD
                            </td>            
                        </tr>
                        <tr class="item">
                            <td>
                                <b>FECHA Y HORA: </b>' . $resp["fecha"] . '
                            </td>
                        </tr>

                        <tr class="item">
                            <td>
                                <b>EDIFICIO: </b> ' . $resp["obra"] . '
                            </td>
                        </tr>
                      
                        <tr class="item">
                            <td>
                                <b>TIPO DE FALLA: </b> ' . $resp["tipofalla"] . '
                            </td>
                        </tr>

                        <tr class="item">
                            <td>
                                <b>DESCRIPCIÓN DE FALLA: </b> ' . $resp["descripcionfalla"] . '
                            </td>
                        </tr>

                        <tr class="item">
                            <td>
                                <b>REQUERIMIENTO: </b> ' . $resp["requerimiento"] . '
                            </td>
                        </tr>

                        <tr class="item">
                            <td>
                                <b>DESCRIPCIÓN DE REQUERIMIENTO: </b> ' . $resp["descrequerimiento"] . '
                            </td>
                        </tr>

                        <tr class="item">
                            <td>
                                <b>SOLICITANTE: </b> ' . $resp["responsable"] . '
                            </td>
                        </tr>

                        <tr class="item">
                            <td>
                                <b>OBSERVACIONES JEFA DE INSTALACIONES: </b> ' . $resp["obs_ji"] . '
                            </td>
                        </tr>
                    </table>
                </div>
            </body>
            </html>
            ';

            //envio de email
            $myObj = new stdClass();
            $myObj->subject  = "App Fabrimetal - Sol. Análisis de Falla N° " . $resp["idanalisis_falla"];
            $myObj->message  = $body;
            $myObj->emails[] = (object) array(
                                                'email' => $emailusuario,
                                                'name' => ''
                                             );

            $result = SendEmail($myObj);

            echo $result->message;
        }

        echo $rspta ? "Solicitud ha sido rechazada" : "No se pudo rechazar la solicitud";
    break;


}
?>