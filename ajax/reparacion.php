<?php

session_start();


require_once "../modelos/Tecnico.php";
require_once "../modelos/Reparacion.php";
require_once '../public/build/lib/PHPMailer/class.phpmailer.php';
require_once '../public/build/lib/PHPMailer/class.smtp.php';

$tecnico = new Tecnico();
$reparacion = new Reparacion();

switch ($_GET["op"]) {

    case 'listarpre':
        $rspta = $presupuesto->listar_dashpre();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {

            if ($reg->estado == '1') {
                $estado = '<span class="label label-warning">SOLICITADO</span>';
            } elseif ($reg->estado == '2') {
                $estado = '<span class="label label-default">INFORMACION</span>';
            } elseif ($reg->estado == '3') {
                $estado = '<span class="label label-primary">INFORMADO</span>';
            } elseif ($reg->estado == '4') {
                $estado = '<span class="label label-info">PROCESADO</span>';
            } elseif ($reg->estado == '5') {
                $estado = '<span class="label label-success">ENVIADO</span>';
            } elseif ($reg->estado == '6') {
                $estado = '<span class="label label-success">ACEPTADO</span>';
            }
            
            if ($reg->antiguedad >= 0 && $reg->antiguedad <= 3) {
                $demora = '<p class="text-success"><b>'.$reg->antiguedad.'</b></p>';
            } elseif ($reg->antiguedad >= 4 && $reg->antiguedad <= 6) {
                $demora = '<p class="text-warning"><b>'.$reg->antiguedad.'</b></p>';
            } else {
                $demora = '<p class="text-danger"><b>'.$reg->antiguedad.'</b></p>';
            }


            $data[] = array(
                "0" => $reg->idpresupuesto,
                "1" => $reg->fecha,
                "2" => $demora,
                "3" => $reg->idservicio,
                "4" => $reg->tservicio,
                "5" => $reg->tascensor,
                "6" => $reg->estadofin,
                "7" => $reg->edificio,
                "8" => $reg->codigocli . ' ' . $reg->ubicacion,
                "9" => $reg->codigo,
                "10" => $reg->nomsup . ' ' . $reg->apesup,
                "11" => $estado,
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
    break;
    
    case 'listardet':
        $rspta = $presupuesto->listar_dashdet();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {

            if ($reg->estado == '1') {
                $estado = '<span class="label label-warning">SOLICITADO</span>';
            } elseif ($reg->estado == '2') {
                $estado = '<span class="label label-default">INFORMACION</span>';
            } elseif ($reg->estado == '3') {
                $estado = '<span class="label label-primary">INFORMADO</span>';
            } elseif ($reg->estado == '4') {
                $estado = '<span class="label label-info">PROCESADO</span>';
            } elseif ($reg->estado == '5') {
                $estado = '<span class="label label-success">ENVIADO</span>';
            } elseif ($reg->estado == '6') {
                $estado = '<span class="label label-success">ACEPTADO</span>';
            }


            $data[] = array(
                "0" => $reg->idpresupuesto,
                "1" => $reg->fecha,
                "2" => '<p class="text-danger"><b>'.$reg->antiguedad.'</p></b>',
                "3" => $reg->idservicio,
                "4" => $reg->tservicio,
                "5" => $reg->tascensor,
                "6" => $reg->edificio,
                "7" => $reg->codigocli . ' ' . $reg->ubicacion,
                "8" => $reg->codigo,
                "9" => $reg->nomsup . ' ' . $reg->apesup,
                "10" => $estado,
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
    break;

    case 'listarpresupuestosup':
        $rut = $_SESSION['rut'];
        $rspta = $presupuesto->listar_sup($rut);
        $data = Array();
        while ($reg = $rspta->fetch_object()) {

            if ($reg->estado == '2') {
                $data[] = array(
                    "0" => '<button class="btn btn-info btn-xs" onclick="solpdf(' . $reg->idpresupuesto . ')" data-tooltip="tooltip" title="PDF Solicitud Presupuesto"><i class="fa fa-file-pdf-o"></i></button><button class="btn btn-success btn-xs" onclick="informar(' . $reg->idpresupuesto . ')" data-tooltip="tooltip" title="Informar Solicitud"><i class="fa fa-share"></i></button>',
                    "1" => $reg->idpresupuesto,
                    "2" => $reg->idservicio,
                    "3" => $reg->edificio,
                    "4" => $reg->codigocli . ' ' .$reg->ubicacion,
                    "5" => $reg->codigo,
                    "6" => $reg->estadofin,
                    "7" => $reg->created,
                    "8" => '<span class="label label-danger">INFORMACION</span>',
                );
            } else{
                if ($reg->estado == '1') {
                    $estado = '<span class="label label-warning">SOLICITADO</span>';
                    $opciones = '<button class="btn btn-info btn-xs" onclick="solpdf(' . $reg->idpresupuesto . ')" data-tooltip="tooltip" title="PDF Solicitud Presupuesto"><i class="fa fa-file-pdf-o"></i></button>';
                    $time = $reg->created;
                } elseif ($reg->estado == '3') {
                    $estado = '<span class="label label-default">INFORMADO</span>';
                    $opciones ='<button class="btn btn-info btn-xs" onclick="revpdf(' . $reg->idpresupuesto . ')" data-tooltip="tooltip" title="PDF Solicitud Presupuesto"><i class="fa fa-file-pdf-o"></i></button>';
                    $time = $reg->revised;
                } elseif ($reg->estado == '4') {
                    $estado = '<span class="label label-default">PROCESADO</span>';
                    $time = $reg->process;
                    if($reg->informado == '1'){
                        $opciones ='<button class="btn btn-info btn-xs" onclick="revpdf(' . $reg->idpresupuesto . ')" data-tooltip="tooltip" title="PDF Solicitud Presupuesto"><i class="fa fa-file-pdf-o"></i></button>';
                    }
                    else{
                        $opciones ='<button class="btn btn-info btn-xs" onclick="solpdf(' . $reg->idpresupuesto . ')" data-tooltip="tooltip" title="PDF Solicitud Presupuesto"><i class="fa fa-file-pdf-o"></i></button>';
                    }
                } elseif ($reg->estado == '5') {
                    $estado = '<span class="label label-default">ENVIDADO</span>';                    
                    $time = $reg->sent;
                    if($reg->informado == '1'){
                        $opciones ='<button class="btn btn-info btn-xs" onclick="revpdf(' . $reg->idpresupuesto . ')" data-tooltip="tooltip" title="PDF Solicitud Presupuesto"><i class="fa fa-file-pdf-o"></i></button>';
                    }
                    else{
                        $opciones ='<button class="btn btn-info btn-xs" onclick="solpdf(' . $reg->idpresupuesto . ')" data-tooltip="tooltip" title="PDF Solicitud Presupuesto"><i class="fa fa-file-pdf-o"></i></button>';
                    }
                } elseif ($reg->estado == '6') {
                    $estado = '<span class="label label-success">ACEPTADO</span>';        
                    $time = $reg->aproved;
                    if($reg->informado == '1'){
                        $opciones ='<button class="btn btn-info btn-xs" onclick="revpdf(' . $reg->idpresupuesto . ')" data-tooltip="tooltip" title="PDF Solicitud Presupuesto"><i class="fa fa-file-pdf-o"></i></button>';
                    }
                    else{
                        $opciones ='<button class="btn btn-info btn-xs" onclick="solpdf(' . $reg->idpresupuesto . ')" data-tooltip="tooltip" title="PDF Solicitud Presupuesto"><i class="fa fa-file-pdf-o"></i></button>';
                    }
                }
                
                $data[] = array(
                    "0" => $opciones,
                    "1" => $reg->idpresupuesto,
                    "2" => $reg->idservicio,
                    "3" => $reg->edificio,
                    "4" => $reg->codigocli . ' ' .$reg->ubicacion,
                    "5" => $reg->codigo,
                    "6" => $reg->estadofin,
                    "7" => $time,
                    "8" => $estado,
                );
            }
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
        
        
    case 'listarreparacion':
        $rspta = $reparacion->listar_rep();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {

                if ($reg->estado == '1') {
                    $estado = '<span class="label label-default">NUEVO</span>';
                    $opciones = '<button class="btn btn-info btn-xs" onclick="MostrarDocumentos(' . $reg->idpresupuesto . ',\''.$reg->file.'\',\''.$reg->fileapro.'\')" data-tooltip="tooltip" title="DOCUMENTOS"><i class="fa fa-folder-open"></i></button>
                                <button class="btn btn-danger btn-xs" onclick="pendiente(' . $reg->idreparacion . ')" data-tooltip="tooltip" title="DEJAR REPARACION PENDIENTE"><i class="fa fa-hourglass-end"></i></button>
                                <button class="btn btn-primary btn-xs" onclick="planificable(' . $reg->idreparacion . ')" data-tooltip="tooltip" title="ENVIAR A PLANIFICACIÓN"><i class="fa fa-share"></i></button>
                                <button class="btn btn-dark btn-xs" onclick="historial(' . $reg->idreparacion . ')" data-tooltip="tooltip" title="VER HISTORIAL"><i class="fa fa-history"></i></button>';
                    $time = $reg->update_time;
                    $supervisor = 'SIN ASIGNAR';
                } elseif ($reg->estado == '2') {
                    $estado = '<span class="label label-danger">PENDIENTE / ' . $reg->motivopend . '</span>';
                    $opciones ='<button class="btn btn-info btn-xs" onclick="MostrarDocumentos(' . $reg->idpresupuesto . ',\''.$reg->file.'\',\''.$reg->fileapro.'\')" data-tooltip="tooltip" title="DOCUMENTOS"><i class="fa fa-folder-open"></i></button>
                    <button class="btn btn-primary btn-xs" onclick="planificable(' . $reg->idreparacion . ')" data-tooltip="tooltip" title="ENVIAR A PLANIFICACIÓN"><i class="fa fa-share"></i></button>
                    <button class="btn btn-dark btn-xs" onclick="historial(' . $reg->idreparacion . ')" data-tooltip="tooltip" title="VER HISTORIAL"><i class="fa fa-history"></i></button>';
                    $time = $reg->update_time;
                    $supervisor = 'SIN ASIGNAR';
                } elseif ($reg->estado == '3') {
                    $estado = '<span class="label label-warning">POR PLANIFICAR</span>';
                    $opciones ='<button class="btn btn-info btn-xs" onclick="MostrarDocumentos(' . $reg->idpresupuesto . ',\''.$reg->file.'\',\''.$reg->fileapro.'\')" data-tooltip="tooltip" title="DOCUMENTOS"><i class="fa fa-folder-open"></i></button>
                                <button class="btn btn-success btn-xs" onclick="planificar(' . $reg->idreparacion . ')" data-tooltip="tooltip" title="PLANIFICAR REPARACIÓN"><i class="fa fa-user"></i></button>
                                <button class="btn btn-dark btn-xs" onclick="historial(' . $reg->idreparacion . ')" data-tooltip="tooltip" title="VER HISTORIAL"><i class="fa fa-history"></i></button>
                    ';
                    $time = $reg->update_time;
                    $supervisor = $reg->nombre.' '.$reg->apellido;
                } elseif ($reg->estado == '4') {
                    $estado = '<span class="label label-primary">PLANIFICADO</span>';
                    $opciones ='<button class="btn btn-info btn-xs" onclick="MostrarDocumentos(' . $reg->idpresupuesto . ',\''.$reg->file.'\',\''.$reg->fileapro.'\')" data-tooltip="tooltip" title="DOCUMENTOS"><i class="fa fa-folder-open"></i></button>
                                <button class="btn btn-success btn-xs" onclick="ejecutado(' . $reg->idreparacion . ')" data-tooltip="tooltip" title="REPARACIÓN EJECUTADA"><i class="fa fa-check-circle"></i></button>
                                <button class="btn btn-dark btn-xs" onclick="historial(' . $reg->idreparacion . ')" data-tooltip="tooltip" title="VER HISTORIAL"><i class="fa fa-history"></i></button>
                    ';
                    $time = $reg->update_time;
                    $supervisor = $reg->nombre.' '.$reg->apellido;
                } elseif ($reg->estado == '5') {
                    $estado = '<span class="label label-success">POR FACTURAR</span>';
                    $opciones = '<button class="btn btn-info btn-xs" onclick="MostrarDocumentos(' . $reg->idpresupuesto . ',\''.$reg->file.'\',\''.$reg->fileapro.'\')" data-tooltip="tooltip" title="DOCUMENTOS"><i class="fa fa-folder-open"></i></button>
                                <button class="btn btn-info btn-xs" onclick="facturacion(' . $reg->idreparacion . ')" data-tooltip="tooltip" title="ENVIADO A FACTURACIÓN"><i class="fa fa-file"></i></button>
                                <button class="btn btn-dark btn-xs" onclick="historial(' . $reg->idreparacion . ')" data-tooltip="tooltip" title="VER HISTORIAL"><i class="fa fa-history"></i></button>';
                    $time = $reg->update_time; 
                    $supervisor = $reg->nombre.' '.$reg->apellido;
                } elseif ($reg->estado == '6') {
                    $estado = '<span class="label label-info">ENVIADO A FACTURACIÓN</span>';
                    $opciones = '<button class="btn btn-info btn-xs" onclick="MostrarDocumentos(' . $reg->idpresupuesto . ',\''.$reg->file.'\',\''.$reg->fileapro.'\')" data-tooltip="tooltip" title="DOCUMENTOS"><i class="fa fa-folder-open"></i></button>
                                <button class="btn btn-dark btn-xs" onclick="historial(' . $reg->idreparacion . ')" data-tooltip="tooltip" title="VER HISTORIAL"><i class="fa fa-history"></i></button>';
                    $time = $reg->update_time; 
                    $supervisor = $reg->nombre.' '.$reg->apellido;
                } elseif ($reg->estado == '7') {
                    $estado = '<span class="label label-default">RE-PLANIFICAR</span>';
                    $opciones = '<button class="btn btn-info btn-xs" onclick="MostrarDocumentos(' . $reg->idpresupuesto . ',\''.$reg->file.'\',\''.$reg->fileapro.'\')" data-tooltip="tooltip" title="DOCUMENTOS"><i class="fa fa-folder-open"></i></button>
                                <button class="btn btn-danger btn-xs" onclick="pendiente(' . $reg->idreparacion . ')" data-tooltip="tooltip" title="DEJAR REPARACION PENDIENTE"><i class="fa fa-hourglass-end"></i></button>
                                <button class="btn btn-primary btn-xs" onclick="planificable(' . $reg->idreparacion . ')" data-tooltip="tooltip" title="ENVIAR A PLANIFICACIÓN"><i class="fa fa-share"></i></button>
                                <button class="btn btn-dark btn-xs" onclick="historial(' . $reg->idreparacion . ')" data-tooltip="tooltip" title="VER HISTORIAL"><i class="fa fa-history"></i></button>';
                    $time = $reg->update_time; 
                    $supervisor = $reg->nombre.' '.$reg->apellido;
                } 
                if ($reg->tipo_reparacion == 1){
                    $tipoRep = 'MENOR';
                } elseif ($reg->tipo_reparacion == 2){
                    $tipoRep = 'MAYOR';
                } elseif ($reg->tipo_reparacion == 3){
                    $tipoRep = 'MIXTA';
                } elseif ($reg->tipo_reparacion == null){
                    $tipoRep = '';
                    $opciones = $opciones . '<button class="btn btn-dark btn-xs" onclick="tipo(' . $reg->idreparacion . ')" data-tooltip="tooltip" title="DEFINIR TIPO REPARACIÓN"><i class="fa fa-users"></i></button>';
                }
                $data[] = array(
                    "0" => $opciones,
                    "1" => $reg->idreparacion,
                    "2" => $reg->idpresupuesto,
                    "3" => $reg->edificio,
                    "4" => $reg->codigo,
                    "5" => $reg->estado_asc,
                    "6" => $tipoRep,
                    "7" => $time,
                    "8" => $estado,
                );
            
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
    break;

    case 'listarhist':
        $idreparacion = isset($_POST["idrep"]) ? limpiarCadena($_POST["idrep"]) : "";
        $rspta = $reparacion->listarHist($idreparacion);
        $data = Array();
        while ($reg = $rspta->fetch_object()) {

                if ($reg->estado_act == '1') {
                    $estado = '<span class="label label-default">NUEVO</span>';
                } elseif ($reg->estado_act == '2') {
                    $estado = '<span class="label label-danger">PENDIENTE</span>';
                } elseif ($reg->estado_act == '3') {
                    $estado = '<span class="label label-warning">POR PLANIFICAR</span>';
                } elseif ($reg->estado_act == '4') {
                        $estado = '<span class="label label-primary">PLANIFICADO</span>';
                } elseif ($reg->estado_act == '5') {
                    $estado = '<span class="label label-success">POR FACTURAR</span>';
                } elseif ($reg->estado_act == '6') {
                    $estado = '<span class="label label-info">ENVIADO A FACTURACIÓN</span>';
                } elseif ($reg->estado_act == '7') {
                    $estado = '<span class="label label-default">RE-PLANIFICAR</span>';
                } 
                $data[] = array(
                    "0" => $reg->nombre,
                    "1" => $reg->fh_insert,
                    "2" => $estado,
                    "3" => $reg->comentario,
                );
            
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
    break;

    case 'guardaTipoRep':
        $idreparacion = isset($_POST["idreparacionTipo"]) ? limpiarCadena($_POST["idreparacionTipo"]) : "";
        $idTipoRep = isset($_POST["idTipoRep"]) ? limpiarCadena($_POST["idTipoRep"]) : "";
        $rspta = $reparacion->guardaTipoRep($idreparacion, $idTipoRep);        
        echo $rspta ? "Tipo reparación guardada" : "Tipo reparación no pudo ser guardada";
        
    break;

    case 'selectMotivoPendiente':
        $rspta = $reparacion->listarMotivoPendiente();
        echo '<option value="" selected disabled>SELECCIONE</option>';
        while($reg = $rspta->fetch_object()){
            echo '<option value='.$reg->idmotivopend.'>'.$reg->descripcion.'</option>';
        }
        break;
    
    case 'selectResponsables':
        $rspta = $reparacion->listarResponsables();
        echo '<option value="" selected disabled>SELECCIONE</option>';
        while($reg = $rspta->fetch_object()){
            echo '<option value='.$reg->iduser.'>'.$reg->supervisor.'</option>';
        }
        break;
        

    case 'dejarPendiente':
        $idreparacion = isset($_POST["idreparacion"]) ? limpiarCadena($_POST["idreparacion"]) : "";
        $motivoPendiente = isset($_POST["idMotivoPendiente"]) ? limpiarCadena($_POST["idMotivoPendiente"]) : "";
        $comentarioPendiente = isset($_POST["comentarioPendiente"]) ? limpiarCadena($_POST["comentarioPendiente"]) : "";
        $rspta = $reparacion->dejarPendiente($idreparacion, $motivoPendiente, $comentarioPendiente);
        if ($rspta){
            $iduser = $_SESSION['iduser'];
            $rspta2 = $reparacion->dejarPendienteHist($iduser, $idreparacion, $comentarioPendiente);
            echo $rspta2 ? "Reparación quedó pendiente" : "Reparación no pudo ser procesada";
        }
        else{
            echo "Reparación no pudo ser procesada";
        }
        
        break;

    case 'porPlanificar':
        $idreparacion = isset($_POST["idreparacionPlan"]) ? limpiarCadena($_POST["idreparacionPlan"]) : "";
        $comentario = isset($_POST["comentarioPlan"]) ? limpiarCadena($_POST["comentarioPlan"]) : "";
        $rspta = $reparacion->pasarPorPlanificar($idreparacion);
        if ($rspta){
            $iduser = $_SESSION['iduser'];
            $rspta2 = $reparacion->pasarPorPlanificarHist($iduser, $idreparacion, $comentario);
            echo $rspta2 ? "Reparación paso a Por planificar" : "Reparación no pudo ser procesada";
        }
        else{
            echo "Reparación no pudo ser procesada.";
        }
        
        break;

    case 'programar':
        $idreparacion = isset($_POST["idreparacionProgra"]) ? limpiarCadena($_POST["idreparacionProgra"]) : "";
        $responsable = isset($_POST["idResponsable"]) ? limpiarCadena($_POST["idResponsable"]) : "";
        $prioridad = isset($_POST["idPrioridad"]) ? limpiarCadena($_POST["idPrioridad"]) : "";
        $inicio = isset($_POST["inicioProgra"]) ? limpiarCadena($_POST["inicioProgra"]) : "";
        $comentario = isset($_POST["comentarioProgra"]) ? limpiarCadena($_POST["comentarioProgra"]) : "";
        $rspta = $reparacion->programarRepa($idreparacion, $responsable, $prioridad, $inicio, $comentario);
        
        if ($rspta){
            $iduser = $_SESSION['iduser'];
            $rspta2 = $reparacion->programarRepaHist($iduser, $idreparacion, $comentario);
            echo $rspta2 ? "Reparación programada" : "Reparación no pudo ser programada";
        }
        else{
            echo "Reparación no pudo ser programada";
        }
        break;    

    case 'ejecutar':
        $idreparacion = isset($_POST["idreparacionEjec"]) ? limpiarCadena($_POST["idreparacionEjec"]) : "";
        $completa = isset($_POST["idcompleto"]) ? limpiarCadena($_POST["idcompleto"]) : "";
        $comentario = isset($_POST["comentarioEjec"]) ? limpiarCadena($_POST["comentarioEjec"]) : "";

        if ($completa == 1){
            $rspta = $reparacion->ejecutar($idreparacion, $comentario);
            
            if ($rspta){
                $iduser = $_SESSION['iduser'];
                $rspta2 = $reparacion->ejecutarHist($iduser, $idreparacion, $comentario);
                echo $rspta2 ? "Reparación ejecutada" : "Reparación no pudo ser ejecutada";
            }
            else{
                echo "Reparación no pudo ser ejecutada";
            }
        }
        else{
            $rspta = $reparacion->volverAPlanificar($idreparacion);
            if ($rspta){
                $iduser = $_SESSION['iduser'];
                $rspta2 = $reparacion->volverAPlanificarHist($iduser, $idreparacion, $comentario);
                echo $rspta2 ? "Reparación por planificar" : "Reparación no pudo ser actualizada";
            }
            else{
                echo "Reparación no pudo ser actualizada";
            }
            
        }
        break; 
        
    case 'facturar':
        $idreparacion = isset($_POST["idreparacionFact"]) ? limpiarCadena($_POST["idreparacionFact"]) : "";
        $estado = isset($_POST["idFact"]) ? limpiarCadena($_POST["idFact"]) : "";
        $comentario = isset($_POST["comentarioFact"]) ? limpiarCadena($_POST["comentarioFact"]) : "";
        $rspta = $reparacion->facturar($idreparacion, $estado);
        if ($rspta){
            $iduser = $_SESSION['iduser'];
            $rspta2 = $reparacion->facturarHist($iduser, $idreparacion, $estado, $comentario);
            echo $rspta2 ? "Operación realizada con éxito" : "No se pudo realizar la operación";
        }
        else{
            echo "No se pudo realizar la operación";
        }
        
        break;
}

function resizeImage($resourceType,$image_width,$image_height) {
    $resizeWidth = 400;
    $resizeHeight = 400;
    $imageLayer = imagecreatetruecolor($resizeWidth,$resizeHeight);
    imagecopyresampled($imageLayer,$resourceType,0,0,0,0,$resizeWidth,$resizeHeight, $image_width,$image_height);
    return $imageLayer;
}

?>