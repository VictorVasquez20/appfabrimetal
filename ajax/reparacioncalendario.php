<?php

if(strlen(session_id()) < 1){
	session_start();
}

require_once "../modelos/Reparacion.php";

$reparacion = new Reparacion();

switch ($_GET['op']) {
	case 'eventos':
		$rspta=$reparacion->listar_eventos($_GET['start'],$_GET['end']);
		$data = Array();
		while ($reg = $rspta->fetch_object()){
			$data[]=array(
				'title'=>$reg->codigo.' - '.$reg->nomEdificio,
				'description'=>$reg->direccion,
				'start'=>$reg->inicio_reparacion,
				'end'=>$reg->endtime,
				'color'=>'#008000',
				'id'=>$reg->idreparacion,
				'url' => '../ajax/reparacioncalendario.php?op=detalle&id='.$reg->idreparacion,
			);
		}

		echo json_encode($data);
	break;

	case 'detalle':
		$rspta=$reparacion->detalle_rep($_GET['id']);
		echo json_encode($rspta);
	break;
}