<?php 
session_start();

require_once "../modelos/Usuario.php";
require_once "../modelos/Role.php";

$user = new Usuario();

$iduser=isset($_POST["iduser"])?limpiarCadena($_POST["iduser"]):"";
$idrole=isset($_POST["idrole"])?limpiarCadena($_POST["idrole"]):"";
$username=isset($_POST["username"])?limpiarCadena($_POST["username"]):"";
$password=isset($_POST["password"])?limpiarCadena($_POST["password"]):"";
$nombre=isset($_POST["nombre"])?limpiarCadena($_POST["nombre"]):"";
$apellido=isset($_POST["apellido"])?limpiarCadena($_POST["apellido"]):"";
$tipo_documento=isset($_POST["tipo_documento"])?limpiarCadena($_POST["tipo_documento"]):"";
$num_documento=isset($_POST["num_documento"])?limpiarCadena($_POST["num_documento"]):"";
$fecha_nac=isset($_POST["fecha_nac"])?limpiarCadena($_POST["fecha_nac"]):"";
$direccion=isset($_POST["direccion"])?limpiarCadena($_POST["direccion"]):"";
$telefono=isset($_POST["telefono"])?limpiarCadena($_POST["telefono"]):"";
$email=isset($_POST["email"])?limpiarCadena($_POST["email"]):"";


switch ($_GET["op"]) {
	case 'guardaryeditar':
		if(!file_exists($_FILES['imagen']['tmp_name']) || !is_uploaded_file($_FILES['imagen']['tmp_name'])){
			$imagen=$_POST["imagenactual"];	
		}else{
			$ext = explode(".",$_FILES['imagen']['name']);
			if($_FILES['imagen']['type'] == "image/jpg" || $_FILES['imagen']['type'] == "image/jpeg" || $_FILES['imagen']['type'] == "image/png" ){
				$imagen = round(microtime(true)).".".end($ext);
				move_uploaded_file($_FILES['imagen']['tmp_name'], "../files/usuarios/".$imagen);
			}
		}

		$clavehash = hash("SHA256", $password);

		if(empty($iduser)){
			$rspta=$user->insertar($idrole,$username,$clavehash,$nombre,$apellido,$tipo_documento,$num_documento,$fecha_nac,$direccion,$telefono,$email,$imagen);
			echo $rspta ? "Usuario registrado" : $username."Usuario no pudo ser registrado";
		}
		else{
			$rspta=$user->editar($iduser,$idrole,$username,$clavehash,$nombre,$apellido,$tipo_documento,$num_documento,$fecha_nac,$direccion,$telefono,$email,$imagen);
			echo $rspta ? "Usuario editado" : "Usuario no pudo ser editado";
		}
		break;

	case 'desactivar':
		$rspta=$user->desactivar($iduser);
			echo $rspta ? "Usuario inhabilitado" : "Usuario no se pudo inhabilitar";
		break;

	case 'activar':
		$rspta=$user->activar($iduser);
			echo $rspta ? "Usuario habilitado" : "Usuario no se pudo habilitar";
		break;

	case 'mostar':
		$rspta=$user->mostrar($iduser);
			echo json_encode($rspta);
		break;
			
	case 'listar':
		$rspta=$user->listar();
		$data = Array();
		while ($reg = $rspta->fetch_object()){
			$data[] = array(
					"0"=>($reg->condicion)?
					'<button class="btn btn-warning btn-xs" onclick="mostar('.$reg->iduser.')"><i class="fa fa-pencil"></i></button>'.
					' <button class="btn btn-danger btn-xs" onclick="desactivar('.$reg->iduser.')"><i class="fa fa-close"></i></button>':
					'<button class="btn btn-warning btn-xs" onclick="mostar('.$reg->iduser.')"><i class="fa fa-pencil"></i></button>'.
					' <button class="btn btn-primary btn-xs" onclick="activar('.$reg->iduser.')"><i class="fa fa-check"></i></button>',
					"1"=>$reg->nombre.''.$reg->apellido,
					"2"=>$reg->username,
					"3"=>$reg->email,
					"4"=>$reg->role,
					"5"=>($reg->condicion)?'<span class="label bg-green">Habilitado</span>':'<span class="label bg-red">Inhabilitado</span>'
				);
		}
		$results = array(
				"sEcho"=>1,
				"iTotalRecords"=>count($data),
				"iTotalDisplayRecords"=>count($data), 
				"aaData"=>$data
			);

		echo json_encode($results);
		break;

		case 'selectRole':
			require_once "../modelos/Role.php";
			$role = new Role();
			$rspta = $role->select();
			while($reg = $rspta->fetch_object()){
				echo '<option value='.$reg->idrole.'>'.$reg->nombre.'</option>';
			}
			break;

		case 'getFirma':
			require_once "../modelos/Usuario.php";
			$usuario = new Usuario();
			$rspta = $usuario->getFirma($_SESSION['iduser']);
			echo json_encode($rspta);
			break;

		case 'setFirma':
			$firma = isset($_POST["firma"]) ? limpiarCadena($_POST["firma"]) : '';
			if ($firma)
			{
				require_once "../modelos/Usuario.php";
				$usuario = new Usuario();

				$encoded_image = explode(",", $firma)[1];
				$decoded_image = base64_decode($encoded_image);
				$imgfirma = round(microtime(true)) . ".png";
				$patchfir = "../files/usuarios/firmas/" . $imgfirma;
				file_put_contents($patchfir, $decoded_image);
				$rspta=$usuario->setFirma($_SESSION['iduser'], $firma, $imgfirma);
				echo $rspta;
			}
			else
				echo 0;
			break;

		case 'verificar':
			require_once "../modelos/Usuario.php";
			$usuario= new Usuario();

			$username_form = $_POST['username_form'];
			$password_form = $_POST['password_form'];

			$password_hash = hash("SHA256", $password_form);
			
			$rspta=$usuario->verificar($username_form, $password_hash);
           
			$fecth = $rspta->fetch_object();

			if(isset($fecth)){
				$_SESSION['iduser']=$fecth->iduser;
				$_SESSION['nombre']=$fecth->nombre;
				$_SESSION['apellido']=$fecth->apellido;
				$_SESSION['imagen']=$fecth->imagen;
				$_SESSION['username']=$fecth->username;
				$_SESSION['idrole']=$fecth->idrole;
                                $_SESSION['rut']=$fecth->num_documento;
                                $_SESSION['role']=$fecth->role;
                                $_SESSION['email']=$fecth->email;

				$role= new Role();
				$permisos = $role->listarmarcados($fecth->idrole);

				$valores=array();

				while ($per = $permisos->fetch_object()){
					array_push($valores, $per->idpermiso);
				}

			
				in_array(1, $valores)? $_SESSION['administrador']=1:$_SESSION['administrador']=0;
				in_array(2, $valores)? $_SESSION['mantencion']=1:$_SESSION['mantencion']=0;
                                in_array(3, $valores)? $_SESSION['Icontratos']=1:$_SESSION['Icontratos']=0;
                                in_array(4, $valores)? $_SESSION['Mcontratos']=1:$_SESSION['Mcontratos']=0;
                                in_array(5, $valores)? $_SESSION['Vcontratos']=1:$_SESSION['Vcontratos']=0;
                                in_array(6, $valores)? $_SESSION['Lcontratos']=1:$_SESSION['Lcontratos']=0;
                                in_array(7, $valores)? $_SESSION['Contratos']=1:$_SESSION['Contratos']=0;
                                in_array(8, $valores)? $_SESSION['Guia']=1:$_SESSION['Guia']=0;
                                in_array(9, $valores)? $_SESSION['Servicio']=1:$_SESSION['Servicio']=0;
                                in_array(10, $valores)? $_SESSION['GGuias']=1:$_SESSION['GGuias']=0;
                                in_array(11, $valores)? $_SESSION['DGuias']=1:$_SESSION['DGuias']=0;
                                in_array(12, $valores)? $_SESSION['Dllamadas']=1:$_SESSION['Dllamadas']=0;
                                in_array(12, $valores)? $_SESSION['Tickets']=1:$_SESSION['Tickets']=0;
                                in_array(14, $valores)? $_SESSION['GGuiasE']=1:$_SESSION['GGuiasE']=0;
                                in_array(15, $valores)? $_SESSION['APresupuesto']=1:$_SESSION['APresupuesto']=0;
                                in_array(16, $valores)? $_SESSION['GPresupuesto']=1:$_SESSION['GPresupuesto']=0;
                                in_array(17, $valores)? $_SESSION['Contabilidad']=1:$_SESSION['Contabilidad']=0;
                                in_array(18, $valores)? $_SESSION['RPresupuesto']=1:$_SESSION['RPresupuesto']=0;
                                in_array(19, $valores)? $_SESSION['RGuias']=1:$_SESSION['RGuias']=0;
                                in_array(20, $valores)? $_SESSION['GEServicios']=1:$_SESSION['GEServicios']=0;
                                in_array(21, $valores)? $_SESSION['Pantallas']=1:$_SESSION['Pantallas']=0;
                                in_array(22, $valores)? $_SESSION['AReparacion']=1:$_SESSION['AReparacion']=0;
                                in_array(23, $valores)? $_SESSION['GReparacion']=1:$_SESSION['GReparacion']=0;
                                in_array(24, $valores)? $_SESSION['JReparacion']=1:$_SESSION['JReparacion']=0;
                                in_array(25, $valores)? $_SESSION['SPantallas']=1:$_SESSION['SPantallas']=0;
                                in_array(26, $valores)? $_SESSION['Instalaciones']=1:$_SESSION['Instalaciones']=0;
                                in_array(27, $valores)? $_SESSION['SupProyectos']=1:$_SESSION['SupProyectos']=0;
                                in_array(28, $valores)? $_SESSION['PMProyectos']=1:$_SESSION['PMProyectos']=0;
                                in_array(29, $valores)? $_SESSION['GEProyectos']=1:$_SESSION['GEProyectos']=0;
                                in_array(30, $valores)? $_SESSION['AsigProyectos']=1:$_SESSION['AsigProyectos']=0;
                                in_array(31, $valores)? $_SESSION['Comercial']=1:$_SESSION['Comercial']=0;
                                in_array(32, $valores)? $_SESSION['Preventa']=1:$_SESSION['Preventa']=0;
                                in_array(33, $valores) ? $_SESSION['GPreventa']=1:$_SESSION['GPreventa']=0;
                                in_array(34, $valores) ? $_SESSION['CPreventa'] =1 : $_SESSION['CPreventa'] = 0;
                                in_array(35, $valores) ? $_SESSION['Aprobacion'] = 1 : $_SESSION['Aprobacion'] =0;
                                in_array(36, $valores) ? $_SESSION['GAprobacion'] = 1: $_SESSION['GAprobacion'] = 0;
                                in_array(37, $valores) ? $_SESSION['VentaFacturacion'] = 1: $_SESSION['VentaFacturacion'] = 0;
                                in_array(38, $valores) ? $_SESSION['CFacturacion'] = 1: $_SESSION['CFacturacion'] = 0;
                                in_array(39, $valores) ? $_SESSION['AFacturacion'] = 1: $_SESSION['AFacturacion'] = 0;
                                in_array(40, $valores) ? $_SESSION['GFacturacion'] = 1: $_SESSION['GFacturacion'] = 0;
                                in_array(41, $valores) ? $_SESSION['Modernizaciones'] = 1: $_SESSION['Modernizaciones'] = 0;
                                in_array(42, $valores) ? $_SESSION['Adquisiciones'] = 1: $_SESSION['Adquisiciones'] = 0;
                                in_array(43, $valores) ? $_SESSION['Comex'] = 1: $_SESSION['Comex'] = 0; 
                                
                                //RRHH
                                in_array(44, $valores) ? $_SESSION['RRHH'] = 1: $_SESSION['RRHH'] = 0;
                                
                                //CALIDAD
                                in_array(50, $valores) ? $_SESSION['Calidad'] = 1: $_SESSION['Calidad'] = 0;
                                
                                //GERENCIA
                                //MODULO GERENCIA
                                in_array(45, $valores) ? $_SESSION['Gerencia'] = 1: $_SESSION['Gerencia'] = 0;
                                    //LISTADO GENERECIA
                                    in_array(47, $valores) ? $_SESSION['LGerencia'] = 1: $_SESSION['LGerencia'] = 0;
                                    //DASHBOARD GERENCIA
                                    in_array(48, $valores) ? $_SESSION['DGerencia'] = 1: $_SESSION['DGerencia'] = 0;
                                    //PANTALLAS GERENCIA
                                    in_array(49, $valores) ? $_SESSION['TVGerencia'] = 1: $_SESSION['TVGerencia'] = 0;
                                
                                
                                //ATENCION AL CLIENTE / SUGERENCIA - RECLAMOS
                                in_array(46, $valores) ? $_SESSION['GReclamos'] = 1: $_SESSION['GReclamos'] = 0;
                                
                                

			}
			
			$usuario_= new Usuario();

			$reparacionesMod = $usuario_->verificaRep($_SESSION['rut']);
			$_SESSION['reparaciones'] = $reparacionesMod;

			echo json_encode($fecth); 			
			break;
                        
                        case 'loginandro':
			require_once "../modelos/Usuario.php";
			$usuario= new Usuario();

			$username = $_GET['username'];
			$password = $_GET['password'];

			$password_hash = hash("SHA256", $password);
			
			$rspta=$usuario->verificar($username, $password_hash);
           
			$fecth = $rspta->fetch_object();

			echo json_encode($fecth); 			
			break;

			case 'salir':
			session_unset();
			session_destroy();
			header("Location: ../index.php");
			break;
                    
                        case 'salirguia':
			session_unset();
			session_destroy();
			header("Location: ../production/guia/index.php");
			break;
}

 ?>