<?php
require_once '../modelos/Correlativo_FM.php';

$CFM = new Correlativo_FM();

$region = isset($_POST['region']) ? $_POST['region'] : 0;
$correlativo = isset($_POST['correlativo']) ? $_POST['correlativo'] : 0;

switch ($_GET['op']){
    case 'getcorreativo':
        if($region != 0){
            $rspta = $CFM->GetCorrelativo($region);
            echo json_encode($rspta);
        }
        break;
    
    case  'setcorrelativo':
        $rspta = $CFM->SetCorrelativo($region, $correlativo);
        echo $rspta;
        break;
}