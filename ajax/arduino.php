<?php 
session_start();
require_once "../modelos/Arduino.php";


$arduino = new Arduino();

$idarduino = isset($_POST["idarduino"]) ? limpiarCadena($_POST["idarduino"]) : "";
$iddirk = isset($_POST["iddirk"]) ? limpiarCadena($_POST["iddirk"]) : "";
$codigo=mb_strtoupper(isset($_POST["codigo"])?limpiarCadena($_POST["codigo"]):"");
$ip = isset($_POST["ip"]) ? limpiarCadena($_POST["ip"]) : "";
$mac = isset($_POST["mac"]) ? limpiarCadena($_POST["mac"]) : "";
$funcion = isset($_POST["funcion"]) ? limpiarCadena($_POST["funcion"]) : "";
$idedificio = isset($_POST["idedificio"]) ? limpiarCadena($_POST["idedificio"]) : "";
      
$op=$_GET["op"];
switch ($op) {
        		
    case 'A':
        if(isset($_GET["C"])){
            $codigo = $_GET["C"];
            $ultimo=$arduino->verificarultimo($codigo);
            $idarduino=$arduino->id_arduino($codigo);
            $id=intval($idarduino["idarduino"]);
            if($ultimo["estado"] != 1){              
                $rspta=$arduino->activar($id);
                echo "OK";
            }else{
                $rsptaa = $arduino->updated($id);
                if($rsptaa){
                    echo "OK";
                }
            }
        }
    break;

    case 'D':
        if(isset($_GET["C"])){
            $codigo = $_GET["C"];
            $ultimo=$arduino->verificarultimo($codigo);
            if($ultimo["estado"] != 0){
                $idarduino=$arduino->id_arduino($codigo);
                $id=intval($idarduino["idarduino"]);
                $rspta=$arduino->desactivar($id);
                echo "OK";
            }else{
                echo "OK";
            }
        }
    break;
        
    
    case 'desactivados':
        $rspta=$arduino->desactivados();
        $data = Array();
        while ($reg = $rspta->fetch_object()){
            $data[] = array(
                    "0"=>$reg->codigo,
                    "1"=>$reg->funcion,
                    "2"=>$reg->dirk,
                    "3"=>$reg->ubicacion,
                    "4"=>$reg->edificio,
                    "5"=>$reg->updated_time,
                    "6"=>($reg->estado)?'<span class="label bg-green">NORMAL</span>':'<span class="label bg-red">FALLA</span>'
                        );
        }
        $results = array(
                "sEcho"=>1,
                "iTotalRecords"=>count($data), 
                "aaData"=>$data
            );

        echo json_encode($results);
        break;

        case 'ContarEstado':
        $rspta=$arduino->contar();
        $data = Array();
        while ($reg = $rspta->fetch_object()){
            $data[] = array(
                    "0"=>$reg->estado
                        );
        }
        $results = array(
                "Totalestados"=>count($data), 
                "aaData"=>$data
            );

        echo json_encode($results);
        break;

      
    case 'verificar':
        
        $rspta=$arduino->verificar();
        
        echo $rspta ? "Arduinos verificados" : "No se logro verificar";
        
    break;
            
    case 'listarPorDirkEdificio':
      
        $rspta=$arduino->listarPorDirkEdificio($iddirk,$idedificio);

        $data = Array();

        while ($reg = $rspta->fetch_object()){

        $data[] = array(
            "0"=>'<button class="btn btn-warning btn-xs" onclick="eliminarArduino('.$reg->idarduino.')"><i class="fa fa-trash"></i></button>
                  <button class="btn btn-success btn-xs"  ><i class="fa fa-pencil"></i></button>',
            "1"=>$reg->codigo,
            "2"=>$reg->ip,
            "3"=>$reg->mac,
            "4"=>$reg->funcion
            );

        }

            $results = array(
                "sEcho"=>1,
                "iTotalRecords"=>count($data),
                "iTotalDisplayRecords"=>count($data), 
                "aaData"=>$data
            );

            echo json_encode($results);
    
    break;
    
      case 'guardar':
        
        $estado = 0;
          
        if(empty($idarduino)){
                
            $rspta=$arduino->insertar($iddirk,$codigo,$ip,$mac,$estado,$funcion,$idedificio);

            if($rspta){
                 $estatus=1;
                 $mensaje="Arduino registrado";
            }else{
                $estatus=0;
                $mensaje="Arduino no se registro";
            }

            echo json_encode(array('estatus'=>$estatus,'mensaje'=>$mensaje));
                
        }

        break; 
        
           case 'actualizarEdificioArduino':
    
            $rspta=$arduino->actualizarEdificioArduino($idedificio, $iddirk);
            if(intval($rspta) >= 0){
               $estatus=1;
               $mensaje="Se reasignaron: ".$rspta." Arduino(s) al edificio";
            }elseif (intval($rspta) == -1) {
              $estatus=0;
              $mensaje="Hubo un error al reasignar los Arduinos al edificio";
        }
            echo json_encode(array('estatus'=>$estatus,'mensaje'=>$mensaje));
    

        break; 
        
          case 'eliminarArduino':
    
            $rspta=$arduino->eliminarArduino($idarduino);
              
             if($rspta){
                 $estatus=1;
                 $mensaje="Arduino eliminado";
            }else{
                $estatus=0;
                $mensaje="Arduino no se pudo eliminar";
            }
            
            echo json_encode(array('estatus'=>$estatus,'mensaje'=>$mensaje));
    

        break;
        
}

 ?>