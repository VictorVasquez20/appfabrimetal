<?php

require_once '../modelos/DetSolAdquisicion.php';

$detsol = new DetSolAdquisicion();

$iddetsol = isset($_POST['iddetsol']) ? $_POST['iddetsol'] : 0; 
$idsolicitud = isset($_POST['idsolicitud']) ? $_POST['idsolicitud'] : 0;
$idproducto = isset($_POST['idproducto']) ? $_POST['idproducto'] : 0;
$cantidad = isset($_POST['cantidad']) ? $_POST['cantidad'] : 0;
$valor = isset($_POST['valor']) ? $_POST['valor'] : 0;
$observacion = isset($_POST['observacion']) ? limpiarCadena($_POST['observacion']) : "";


switch ($_GET['op']){
    case 'guardaryeditar':
        
        if(!$iddetsol && $idsolicitud != 0){
            $rspta = $detsol->insertar($idsolicitud, $idproducto, $cantidad, $valor, $observacion);
            echo $rspta;
        }else{
            $rspta = $detsol->editar($iddetsol, $idsolicitud, $idproducto, $cantidad, $valor, $observacion);
            echo $rspta;
        }
        break;
        
    case 'listar':
        $rspta = $detsol->listar($idsolicitud);
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $op = "";
            
            $op = '<button class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="eliminar" onclick="del(' . $reg->iddetsol.')" type="button"><i class="fa fa-trash"></i></button>';

            
            $data[] = array(
                "0" => '<b>'.$reg->nombre.'</b>',
                "1" => '<b>'.$reg->cantidad.'</b>',
                "2" => '<b>'.$reg->valor.'</b>',
                "3" => round($reg->cantidad * $reg->valor, 1),
                "4" => '<b>'.$reg->observacion.'</b>',
                "5" => $op
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
       
    case 'eliminar':
        
        $iddetsol = $_GET['iddet'];
        $rspta = 0;
        if($iddetsol){
            $rspta = $detsol->eliminar($iddetsol);
        }
        echo $rspta;
        break;
}