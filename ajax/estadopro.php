<?php
session_start();
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once '../modelos/EstadoPro.php';
$estadopro = new EstadoPro();

$idestadopro = isset($_POST['idestadopro']) ? $_POST['idestadopro'] : 0;
$estado = isset($_POST['estado']) ? $_POST['estado'] : 0;
$nombre = isset($_POST['nombre']) ? $_POST['nombre'] : 0;
$color = isset($_POST['color']) ? $_POST['color'] : 0;
$carga = isset($_POST['carga']) ? $_POST['carga'] : 0;
$condicio = isset($_POST['condicio']) ? $_POST['condicio'] : 0;

switch($_GET["op"]){
    case 'guardaryeditar':
        if(!$idestadopro){
            $rspta = $estadopro->Insertar($estado, $nombre, $color, $carga, $condicio);
            echo $rspta ? "Estado Guardado" : "estado no guardado";
        }else{
            $rspta = $estadopro->Editar($idestadopro, $estado, $nombre, $color, $carga, $condicio);
            echo $rspta ? "Estado Editado" : "estado no Editado";
        }
        break;
    case 'listar':
        $rspta = $estadopro->Listar();

        $data = Array();

        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => '<button class="btn btn-info btn-xs" onclick="revisar(' . $reg->idestadopro . ')"><i class="fa fa-eye"></i></button>',
                "1" => $reg->estado,
                "2" => $reg->nombre,
                "3" => $reg->condicio ?'<span class="label bg-green">Activo</span>':'<span class="label bg-red">No activo</span>');
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
    case 'listarCodigoSI':
        
        $codigoInicial = isset($_REQUEST['codigoinicial']) ? $_REQUEST['codigoinicial'] : 0;
        $rspta = $estadopro->ListarCodigos($codigoInicial);
        $data = Array();
        $cuenta = 0;
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => '<h5 style="color:'.$reg->color.'">'.$reg->nombre.'</h5>',
                "1" => "<input type='text' class='form-control' name='SI[].cobro' id='SI_". $cuenta ."__cobro' value='0'> "
                     . "<input type='hidden' name='SI[]' id='SI_". $cuenta ."__idestadopro' value='" . $reg->idestadopro . "'>" ,
                "2" => "<input type='checkbox' id='SI_". $cuenta ."__hito' name='SI[].cuenta' value='1'>");
            $cuenta += 1;
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
        
    case 'listarCodigoSN':
        
        $codigoInicial = isset($_REQUEST['codigoinicial']) ? $_REQUEST['codigoinicial'] : 0;
        $rspta = $estadopro->ListarCodigos($codigoInicial);
        $data = Array();
        $cuenta = 0;
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => '<h5 style="color:'.$reg->color.'">'.$reg->nombre.'</h5>',
                "1" => "<input type='text' class='form-control' name='SN[].cobro' id='SN_". $cuenta ."__cobro' value='0'> "
                     . "<input type='hidden' name='SN[]' id='SN_". $cuenta ."__idestadopro' value='" . $reg->idestadopro . "'>" ,
                "2" => "<input type='checkbox' id='SN_". $cuenta ."__hito' name='SN[].hito' value='1'>");
            $cuenta += 1;
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
}
