<?php
require_once '../config/conexion.php';

/**
 * Description of Modelo_izaje
 *
 * @author azuni
 */
class Modelo_izaje {
    //put your code here
    public $idmodelo;
    public $nombre;
    public $idmarca;
    

    function __construct() {
    }
    
    function Insertar($nombre, $idmarca){
        $sql = "INSERT INTO `modelo_izaje`( `nombre`, `idmarca`) VALUES ('$nombre','$idmarca')";
        return ejecutarConsulta_retornarID($sql);
    }
    
    function Editar($idmodelo, $nombre, $idmarca ){
        $sql = "UPDATE `modelo_izaje` "
                . "SET `nombre`= '$nombre',"
                . "`idmarca`= '$idmarca' "
                . "WHERE `idmodelo`= '$idmodelo'";
        return ejecutarConsulta($sql);
    }
    
    function Listar(){
        $sql = "SELECT mo.*, ma.nombre as 'marca' FROM `modelo_izaje` mo INNER JOIN marca_izaje ma ON ma.idmarca = mo.idmarca";
        return ejecutarConsulta($sql);
    }
    
    function selectmodelo($idmarca){
        $sql = "SELECT mo.*, ma.nombre as 'marca' FROM `modelo_izaje` mo INNER JOIN marca_izaje ma ON ma.idmarca = mo.idmarca WHERE mo.idmarca = $idmarca";
        return ejecutarConsulta($sql);
    }
    
    function Mostrar($idmodelo){
        $sql = "SELECT * FROM `modelo_izaje` WHERE `idmodelo`= $idmodelo ";
        return ejecutarConsultaSimpleFila($sql);
    }
}
