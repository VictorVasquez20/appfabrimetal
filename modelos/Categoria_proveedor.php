<?php
require_once '../config/conexion.php';

/**
 * Description of categoria_proveedor
 *
 * @author azuni
 */
class Categoria_proveedor {
    
    function __construct() {
        
    }
    
    function Insertar($nombre, $condicion){
        $sql = "INSERT INTO `categoria_proveedor`(`nombre`, `condicion`) VALUES ('$nombre', $condicion)";
        return ejecutarConsulta_retornarID($sql);
    }
    
    function Editar($idcategoria, $nombre, $condicion){
        $sql = "UPDATE `categoria_proveedor` "
                . "SET `nombre`= '$nombre',"
                . "`condicion`= $condicion "
                . "WHERE `idcategoria`= $idcategoria ";
        return ejecutarConsulta($sql);
    }
    
    function Listar(){
        $sql = "SELECT * FROM `categoria_proveedor`";
        return ejecutarConsulta($sql);
    }
    
    function Mostrar($idcategoria){
        $sql = "SELECT * FROM `categoria_proveedor` WHERE `idcategoria`= $idcategoria ";
        return ejecutarConsultaSimpleFila($sql);
    }
    
    function SelectCategoria(){
        $sql = "SELECT * FROM `categoria_proveedor` WHERE condicion = 1";
        return ejecutarConsulta($sql);
    }
}
