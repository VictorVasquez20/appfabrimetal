<?php

require_once '../config/conexion.php';

/**
 * Description of Compras
 *
 * @author azuni
 */
class Compras {

    //put your code here
    function __construct() {
        
    }

    function Insertar($idproveedor, $ordencompra, $presupuesto, $factura, $flete, $buy_time, $reception_time, $evaluacion, $condicion) {
        $sql = "INSERT INTO `compras`(`idproveedor`, `ordencompra`, `presupuesto`, `factura`, `flete`, `buy_time`, `reception_time`, `evaluacion`, `condicion`) "
                . "VALUES ($idproveedor, '$ordencompra',$presupuesto,'$factura', $flete, '$buy_time', '$reception_time', $evaluacion ,$condicion)";
        return ejecutarConsulta($sql);
    }

    function Editar($idcompra, $idproveedor, $ordencompra, $presupuesto, $factura, $flete, $buy_time, $reception_time, $evaluacion, $condicion) {
        $sql = "UPDATE `compras` "
                . "SET `idproveedor`= $idproveedor,"
                . "`ordencompra`= '$ordencompra',"
                . "`presupuesto`= $presupuesto,"
                . "`factura`= '$factura',"
                . "`flete`= $flete,"
                . "`buy_time`= '$buy_time',"
                . "`reception_time`= '$reception_time',"
                . "`evaluacion`= $evaluacion,"
                . "`condicion`= $condicion "
                . "WHERE `idcompra`= $idcompra";
        return ejecutarConsulta($sql);
    }

    function Listar($idproveedor) {
        $sql = "SELECT * FROM compras WHERE idproveedor = $idproveedor";
        return ejecutarConsulta($sql);
    }

    function Mostrar($idcompra) {
        $sql = "SELECT * FROM compras WHERE idcompra = $idcompra";
        return ejecutarConsultaSimpleFila($sql);
    }
    
    function Eliminar($idcompra){
        $sql = "DELETE FROM `compras` WHERE idcompra = $idcompra";
        return ejecutarConsulta($sql);
    }

}
