<?php 

require "../config/asteriskdb.php";

	Class DashAsterisk{
		//Constructor para instancias
		public function __construct(){

		}
                
        public function llamadas(){
			$sql="SELECT COUNT(clid) AS nllamadas FROM cdr WHERE DATE(calldate)=DATE(now()) AND dst='600'";
			return ejecutarConsultaSimpleFila2($sql);
		}
                
        public function contestadas(){
			$sql="SELECT COUNT(clid) AS ncontestadas FROM cdr WHERE DATE(calldate)=DATE(now()) AND disposition='ANSWERED' AND dst='600'";
			return ejecutarConsultaSimpleFila2($sql);
		}
                
        public function nocontestadas(){
			$sql="SELECT COUNT(clid) AS nocontestadas FROM cdr WHERE DATE(calldate)=DATE(now()) AND disposition='NO ANSWER' AND dst='600'";
			return ejecutarConsultaSimpleFila2($sql);
		}
                
        public function tcontestar(){
                        $sql="SELECT SUM(duration) AS total, SUM(billsec) as conversacion FROM cdr WHERE DATE(calldate)=DATE(now()) AND dst='600'";
			return ejecutarConsultaSimpleFila2($sql);
		}
                
        public function tconversacion(){
                        $sql="SELECT SUM(billsec) AS conversacion FROM cdr WHERE DATE(calldate)=DATE(now()) AND disposition='ANSWERED' AND dst='600'";
			return ejecutarConsultaSimpleFila2($sql);
		}
                
        public function mesllamados(){
			$sql="SELECT MONTH(calldate) AS mes, COUNT(clid) AS llamadas FROM cdr WHERE YEAR(calldate)=YEAR(now()) AND dst='600' GROUP BY MONTH(calldate)";
			return ejecutarConsulta2($sql);
		}
                
        public function llamadasmes(){
			$sql="SELECT COUNT(clid) AS nllamadas FROM cdr WHERE MONTH(calldate)=MONTH(now()) AND YEAR(calldate)=YEAR(now()) AND dst='600'";
			return ejecutarConsultaSimpleFila2($sql);
		}
                
        public function contestadasmes(){
			$sql="SELECT COUNT(clid) AS ncontestadas FROM cdr WHERE MONTH(calldate)=MONTH(now()) AND YEAR(calldate)=YEAR(now()) AND disposition='ANSWERED' AND dst='600'";
			return ejecutarConsultaSimpleFila2($sql);
		}
                
        public function nocontestadasmes(){
			$sql="SELECT COUNT(clid) AS nocontestadas FROM cdr WHERE MONTH(calldate)=MONTH(now()) AND YEAR(calldate)=YEAR(now()) AND disposition='NO ANSWER' AND dst='600'";
			return ejecutarConsultaSimpleFila2($sql);
		}
                
        public function tcontestarmes(){
                        $sql="SELECT SUM(duration) AS total, SUM(billsec) as conversacion FROM cdr WHERE MONTH(calldate)=MONTH(now()) AND YEAR(calldate)=YEAR(now()) AND dst='600'";
			return ejecutarConsultaSimpleFila2($sql);
		}
                
        public function tconversacionmes(){
                        $sql="SELECT SUM(billsec) AS conversacion FROM cdr WHERE MONTH(calldate)=MONTH(now()) AND YEAR(calldate)=YEAR(now()) AND disposition='ANSWERED' AND dst='600'";
			return ejecutarConsultaSimpleFila2($sql);
		}
                
        public function listarllamadasemergenciahoy($dst){                      
                        $sql="SELECT uniqueid,src as origen,dst as destino,DATE_FORMAT(calldate,'%d/%m/%Y') as fecha,
                            DATE_FORMAT(calldate,'%H:%i') as hora,calldate,billsec as conversacion,
                            IF(billsec=0 OR disposition='NO ANSWER','NO CONTESTADA',IF(disposition='ANSWERED','CONTESTADA','')) as tipo_llamada,amaflags,duration 
                            FROM cdr  
                            WHERE  dst IN($dst)
                            AND DATE(calldate) = DATE(now())
                            ORDER BY calldate DESC";
            return ejecutarConsulta2($sql);        
        }             
        
        public function cantidadllamadasporhorario($anio,$mes,$dst) {
            
            $sql="SELECT 
                    SUM(
                        IF( ((DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '00:00:00' AND '06:59:59') OR
                             (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '19:00:00' AND '23:59:59')) AND
                            (WEEKDAY(calldate) NOT IN(5,6)) AND disposition='NO ANSWER'    
                           ,
                           1,0 )
                       ) 
                    as nocturna_norespondido,
                    SUM(
                        IF( ((DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '00:00:00' AND '06:59:59') OR
                             (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '19:00:00' AND '23:59:59')) AND
                            (WEEKDAY(calldate) NOT IN(5,6)) AND disposition='ANSWERED'    
                           ,
                           1,0 )
                       ) 
                    as nocturna_respondido,

                    SUM(
                        IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '07:00:00' AND '18:59:59') AND
                            (WEEKDAY(calldate) NOT IN(5,6)) AND disposition='NO ANSWER'     
                        ,1,0 )
                       ) 
                    as normal_norespondido,
                    SUM(
                        IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '07:00:00' AND '18:59:59') AND
                            (WEEKDAY(calldate) NOT IN(5,6)) AND disposition='ANSWERED'     
                        ,1,0 )
                       ) 
                    as normal_respondido,
                    SUM( IF( (WEEKDAY(calldate)=5 OR WEEKDAY(calldate)=6) AND disposition='NO ANSWER' , 1,0 ) ) as finsemana_norespondido,
                    SUM( IF( (WEEKDAY(calldate)=5 OR WEEKDAY(calldate)=6) AND disposition='ANSWERED' , 1,0 ) ) as finsemana_respondido

                    FROM cdr  
                    WHERE YEAR(calldate) = $anio
                    AND dst IN($dst)";
            
            if($mes>0){
                $sql.=" AND MONTH(calldate) = ".$mes;
            }        
            
            
            return ejecutarConsultaSimpleFila2($sql);
            
        }
        
        public function cantidadllamadasporhora($anio,$mes,$dst) {
            $sql="SELECT  
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '00:00:00' AND '00:59:59') AND disposition='NO ANSWER',1,0 ) ) as nocontestada_0,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '01:00:00' AND '01:59:59') AND disposition='NO ANSWER',1,0 ) ) as nocontestada_1,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '02:00:00' AND '02:59:59') AND disposition='NO ANSWER',1,0 ) ) as nocontestada_2,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '03:00:00' AND '03:59:59') AND disposition='NO ANSWER',1,0 ) ) as nocontestada_3,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '04:00:00' AND '04:59:59') AND disposition='NO ANSWER',1,0 ) ) as nocontestada_4,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '05:00:00' AND '05:59:59') AND disposition='NO ANSWER',1,0 ) ) as nocontestada_5,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '06:00:00' AND '06:59:59') AND disposition='NO ANSWER',1,0 ) ) as nocontestada_6,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '07:00:00' AND '07:59:59') AND disposition='NO ANSWER',1,0 ) ) as nocontestada_7,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '08:00:00' AND '08:59:59') AND disposition='NO ANSWER',1,0 ) ) as nocontestada_8,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '09:00:00' AND '09:59:59') AND disposition='NO ANSWER',1,0 ) ) as nocontestada_9,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '10:00:00' AND '10:59:59') AND disposition='NO ANSWER',1,0 ) ) as nocontestada_10,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '11:00:00' AND '11:59:59') AND disposition='NO ANSWER',1,0 ) ) as nocontestada_11,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '12:00:00' AND '12:59:59') AND disposition='NO ANSWER',1,0 ) ) as nocontestada_12,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '13:00:00' AND '13:59:59') AND disposition='NO ANSWER',1,0 ) ) as nocontestada_13,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '14:00:00' AND '14:59:59') AND disposition='NO ANSWER',1,0 ) ) as nocontestada_14,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '15:00:00' AND '15:59:59') AND disposition='NO ANSWER',1,0 ) ) as nocontestada_15,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '16:00:00' AND '16:59:59') AND disposition='NO ANSWER',1,0 ) ) as nocontestada_16,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '17:00:00' AND '17:59:59') AND disposition='NO ANSWER',1,0 ) ) as nocontestada_17,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '18:00:00' AND '18:59:59') AND disposition='NO ANSWER',1,0 ) ) as nocontestada_18,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '19:00:00' AND '19:59:59') AND disposition='NO ANSWER',1,0 ) ) as nocontestada_19,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '20:00:00' AND '20:59:59') AND disposition='NO ANSWER',1,0 ) ) as nocontestada_20,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '21:00:00' AND '21:59:59') AND disposition='NO ANSWER',1,0 ) ) as nocontestada_21,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '22:00:00' AND '22:59:59') AND disposition='NO ANSWER',1,0 ) ) as nocontestada_22,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '23:00:00' AND '23:59:59') AND disposition='NO ANSWER',1,0 ) ) as nocontestada_23,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '00:00:00' AND '00:59:59') AND disposition='ANSWERED',1,0 ) ) as contestada_0,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '01:00:00' AND '01:59:59') AND disposition='ANSWERED',1,0 ) ) as contestada_1,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '02:00:00' AND '02:59:59') AND disposition='ANSWERED',1,0 ) ) as contestada_2,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '03:00:00' AND '03:59:59') AND disposition='ANSWERED',1,0 ) ) as contestada_3,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '04:00:00' AND '04:59:59') AND disposition='ANSWERED',1,0 ) ) as contestada_4,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '05:00:00' AND '05:59:59') AND disposition='ANSWERED',1,0 ) ) as contestada_5,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '06:00:00' AND '06:59:59') AND disposition='ANSWERED',1,0 ) ) as contestada_6,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '07:00:00' AND '07:59:59') AND disposition='ANSWERED',1,0 ) ) as contestada_7,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '08:00:00' AND '08:59:59') AND disposition='ANSWERED',1,0 ) ) as contestada_8,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '09:00:00' AND '09:59:59') AND disposition='ANSWERED',1,0 ) ) as contestada_9,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '10:00:00' AND '10:59:59') AND disposition='ANSWERED',1,0 ) ) as contestada_10,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '11:00:00' AND '11:59:59') AND disposition='ANSWERED',1,0 ) ) as contestada_11,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '12:00:00' AND '12:59:59') AND disposition='ANSWERED',1,0 ) ) as contestada_12,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '13:00:00' AND '13:59:59') AND disposition='ANSWERED',1,0 ) ) as contestada_13,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '14:00:00' AND '14:59:59') AND disposition='ANSWERED',1,0 ) ) as contestada_14,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '15:00:00' AND '15:59:59') AND disposition='ANSWERED',1,0 ) ) as contestada_15,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '16:00:00' AND '16:59:59') AND disposition='ANSWERED',1,0 ) ) as contestada_16,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '17:00:00' AND '17:59:59') AND disposition='ANSWERED',1,0 ) ) as contestada_17,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '18:00:00' AND '18:59:59') AND disposition='ANSWERED',1,0 ) ) as contestada_18,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '19:00:00' AND '19:59:59') AND disposition='ANSWERED',1,0 ) ) as contestada_19,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '20:00:00' AND '20:59:59') AND disposition='ANSWERED',1,0 ) ) as contestada_20,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '21:00:00' AND '21:59:59') AND disposition='ANSWERED',1,0 ) ) as contestada_21,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '22:00:00' AND '22:59:59') AND disposition='ANSWERED',1,0 ) ) as contestada_22,
                    SUM(IF( (DATE_FORMAT(calldate, '%H:%i:%s') BETWEEN '23:00:00' AND '23:59:59') AND disposition='ANSWERED',1,0 ) ) as contestada_23
                    FROM cdr  
                    WHERE YEAR(calldate) = $anio
                    AND dst IN($dst)";
            
                    if($mes>0){
                        $sql.=" AND MONTH(calldate) = ".$mes;
                    }
                    
                  
            return ejecutarConsultaSimpleFila2($sql);
        }
        
        public function tiempollamadas($anio,$mes,$dst) {
            
            $sql="SELECT 
                    SEC_TO_TIME(ROUND( SUM(duration)/count(uniqueid))) as promedio_total,
                    SEC_TO_TIME(ROUND( SUM(billsec)/count(uniqueid))) as promedio_conversado,
                    SEC_TO_TIME(ROUND( SUM(duration-billsec)/count(uniqueid))) as promedio_espera
                    FROM cdr  
                    WHERE  YEAR(calldate) = $anio
                    AND dst IN($dst)
                    AND disposition='ANSWERED'";
            
            if($mes>0){
               $sql.=" AND MONTH(calldate) = ".$mes;
            }
                    
              return ejecutarConsultaSimpleFila2($sql);
        }
        
            public function aniollamadasemergencia($dst) {
            
            $sql="SELECT YEAR(calldate) as anio FROM `cdr` 
                  WHERE  dst IN($dst)
                  GROUP BY YEAR(calldate)
                  ORDER BY YEAR(calldate)";
            
              return ejecutarConsulta2($sql);
        }
        
            public function mesllamadasemergencia($anio,$dst) {
            
            $sql="SELECT YEAR(calldate) as anio,MONTH(calldate)  as mes ,
                  CASE
                      WHEN MONTH(calldate) = 1 THEN 'ENERO'
                      WHEN MONTH(calldate) = 2 THEN 'FEBRERO'
                      WHEN MONTH(calldate) = 3 THEN 'MARZO'
                      WHEN MONTH(calldate) = 4 THEN 'ABRIL'
                      WHEN MONTH(calldate) = 5 THEN 'MAYO'
                      WHEN MONTH(calldate) = 6 THEN 'JUNIO'
                      WHEN MONTH(calldate) = 7 THEN 'JULIO'
                      WHEN MONTH(calldate) = 8 THEN 'AGOSTO'
                      WHEN MONTH(calldate) = 9 THEN 'SEPTIEMBRE'
                      WHEN MONTH(calldate) = 10 THEN 'OCTUBRE'
                      WHEN MONTH(calldate) = 11 THEN 'NOVIEMBRE'
                      WHEN MONTH(calldate) = 12 THEN 'DICIEMBRE'
                  END AS nombre_mes            
                  FROM `cdr` 
                  WHERE YEAR(calldate)=$anio 
                  AND dst IN($dst)
                  GROUP BY YEAR(calldate),MONTH(calldate)
                  ORDER BY YEAR(calldate),MONTH(calldate)";
            
              return ejecutarConsulta2($sql);
        }
        
        public function listarllamadasemergenciames($anio,$mes,$dst){   
            
                        $sql="SELECT uniqueid,src as origen,dst as destino,DATE_FORMAT(calldate,'%d/%m/%Y') as fecha,
                            DATE_FORMAT(calldate,'%H:%i') as hora,calldate,billsec as conversacion,
                            IF(billsec=0 OR disposition='NO ANSWER','NO CONTESTADA',IF(disposition='ANSWERED','CONTESTADA','')) as estado,amaflags,duration 
                            FROM cdr  
                            WHERE YEAR(calldate) = $anio
                            AND MONTH(calldate) = $mes    
                            AND dst IN ($dst)    
                            ORDER BY calldate DESC";
                        
            return ejecutarConsulta2($sql);        
        }  
        
    }
?>