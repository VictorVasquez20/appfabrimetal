<?php

require "../config/conexion.php";

Class Monitoreo {

    //Constructor para instancias
    public function __construct() {
        
    }

    public function insertar($codigo, $alerta, $idascensor) {
        $sql = "INSERT INTO monitoreo (idascensor, codigo, alerta, estado) VALUES ('$idascensor','$codigo','$alerta',0)";
        return ejecutarConsulta($sql);
    }

    public function estado($codigo) {
        $sql = "UPDATE monitoreo SET estado=1 WHERE codigo='$codigo'";
        return ejecutarConsulta($sql);
    }

    public function error($codigo, $error) {
        $sql = "INSERT INTO error (idascensor,codigo, error) VALUES (1,'$codigo', '$error' )";
        return ejecutarConsulta($sql);
    }

    public function listar() {
        $sql = "SELECT a.idascensor, e.idedificio, a.lon, a.lat, a.monitoreo, a.codigo, a.codigocli, a.ubicacion, e.nombre, e.calle, e.numero, x.comuna_nombre AS comuna, z.nombre AS segmento, c.nombre AS contacto, c.email, c.telefonomovil AS telefono FROM ascensor a INNER JOIN edificio e ON a.idedificio = e.idedificio LEFT JOIN contacto c ON e.idedificio = c.idedificio INNER JOIN comunas x ON e.idcomunas = x.comuna_id INNER JOIN tsegmento z ON e.idtsegmento = z.idtsegmento WHERE e.idregiones=7 AND a.monitoreo IS NOT NULL GROUP BY a.idascensor";
        return ejecutarConsulta($sql);
    }
    
    public function listargse() {
        $sql = "SELECT a.idascensor, e.idedificio, a.lon, a.lat, a.estado, a.codigo, a.codigocli, a.ubicacion, e.nombre, e.calle, e.numero, x.comuna_nombre AS comuna, z.nombre AS segmento, c.nombre AS contacto, c.email, c.telefonomovil AS telefono FROM ascensor a INNER JOIN edificio e ON a.idedificio = e.idedificio LEFT JOIN contacto c ON e.idedificio = c.idedificio INNER JOIN comunas x ON e.idcomunas = x.comuna_id INNER JOIN tsegmento z ON e.idtsegmento = z.idtsegmento WHERE e.idregiones=7 AND a.monitoreo IS NOT NULL AND a.estado <> 1 GROUP BY a.idascensor";
        return ejecutarConsulta($sql);
    }

    public function verificarultimo($codigo) {
        $sql = "SELECT alerta FROM monitoreo WHERE codigo='$codigo' ORDER BY created_time DESC LIMIT 1";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function id_ascensor($codigo) {
        $sql = "SELECT idascensor FROM ascensor WHERE codigo='$codigo'";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function indatos($idascensor, $recorrido, $movimientos) {
        $sql = "INSERT INTO datos (idascensor, recorrido, movimientos) VALUES ('$idascensor','$recorrido','$movimientos')";
        return ejecutarConsulta($sql);
    }

    public function datos($codigo) {
        $sql = "SELECT a.idascensor, a.codigo, e.nombre, e.calle, e.numero, r.nombre as region, c.nombre as ciudad, o.nombre as comuna, l.rut, l.razon_social, t.nombre as segmento FROM ascensor a INNER JOIN edificio e ON a.id_edificio=e.idedificio INNER JOIN region r ON r.idregion=e.id_region INNER JOIN ciudad c ON c.idciudad=e.id_ciudad INNER JOIN comuna o ON o.idcomuna=e.id_comuna INNER JOIN cliente l ON l.idcliente=e.idcliente INNER JOIN tipo_segmento t ON t.idtipo_segmento=e.id_tipo_segmento WHERE a.codigo = '$codigo'";
        //$sql="SELECT m.idmonitoreo, m.codigo, m.alerta, a.lat, a.lon FROM monitoreo m INNER JOIN ascensor a ON m.idascensor=a.idascensor GROUP BY m.idascensor ORDER BY m.created_time DESC";
        return ejecutarConsultaSimpleFila($sql);
        ;
    }

}

?>