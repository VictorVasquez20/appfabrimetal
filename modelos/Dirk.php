<?php

require "../config/conexion.php";

class Dirk {
    
    //Constructor para instancias
    public function __construct(){

    }
    
    public function insertar($idedificio,$codigo,$ubicacion){
        
        $sql="INSERT INTO `dirk`(`idedificio`, `codigo`, `ubicacion`) VALUES ($idedificio,'$codigo','$ubicacion')";
        
        return ejecutarConsulta($sql);
    }
    
     public function actualizarDirk($iddirk,$idedificio){
        
        $sql="UPDATE dirk SET idedificio=$idedificio WHERE iddirk=$iddirk";
        
        return ejecutarConsulta($sql);
    }
    
    public function listar(){
        
        $sql="SELECT a.`iddirk`, IF(a.`idedificio` IS NULL,0,a.idedificio) as idedificio, a.`codigo`, a.`ubicacion`,b.nombre as nombre_edificio
              FROM `dirk` as a 
              LEFT JOIN edificio as b on a.idedificio = b.idedificio
              ORDER BY a.codigo, a.ubicacion";
        
        return ejecutarConsulta($sql);
    }
    
    public function mostrar($iddirk){
            $sql="SELECT iddirk,IF(idedificio IS NULL,0,idedificio) as idedificio,codigo,ubicacion FROM dirk WHERE iddirk=$iddirk";
            return ejecutarConsultaSimpleFila($sql);
    }
    
}
