<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once '../config/conexion.php';
/**
 * Description of Stock
 *
 * @author aaron
 */
class Stock {
    //put your code here
    
    function __construct() {
        
    }
    
    function insertar($idproducto, $idbodega, $created_user, $referencia, $cantidad, $tipomovimiento, $centrocosto, $solicitante, $autoriza){
        $sql = "INSERT INTO `stock`(`idproducto`, `idbodega` , `created_user`, `referencia`, `cantidad`, `tipomovimiento`, `centrocosto`, `solicitante`, `autoriza`) "
             . "VALUES ($idproducto, $idbodega ,$created_user, '$referencia', $cantidad , $tipomovimiento, $centrocosto, '$solicitante','$autoriza')";
        //var_dump($sql);
        return ejecutarConsulta($sql);
    }
    
    function editar($idstock, $idproducto, $idbodega, $referencia, $cantidad, $tipomovimiento, $centrocosto, $solicitante, $autoriza){
        $sql = "UPDATE `stock` SET "
                . "`idproducto`= $idproducto,"
                . "`idbodega` = $idbodega, "
                . "`referencia`= '$referencia',"
                . "`cantidad`= $cantidad,"
                . "`tipomovimiento`= $tipomovimiento,"
                . "`centrocosto`= $centrocosto,"
                . "`solicitante`= '$solicitante',"
                . "`autoriza`= '$autoriza' "
                . " WHERE `idstock`= $idstock";
        return ejecutarConsulta($sql);
    }
    
    function listar($idproducto, $bodega){
        $sql = "Select s.*, concat(u.nombre, ' ', u.apellido) as 'nombUser' "
                ." from stock s "
                ." inner join user u on u.iduser = s.created_user"
                ." Where s.idproducto = $idproducto  and s.idbodega = $bodega";
        
        return ejecutarConsulta($sql);
    }
    
    function stock($idproducto){
        /*$sql = "SELECT SUM(IF(s.tipomovimiento = 1, s.cantidad, (s.cantidad * -1))) as cantidad, s.idbodega, b.nombre, p.stock, p.nombre as 'producto' "
                . "FROM stock s "
                . "INNER JOIN producto p on p.idproducto = s.idproducto "
                . "INNER JOIN bodega b on b.idbodega = s.idbodega "
                . "Where s.idproducto = $idproducto "
                . "group by s.idbodega";*/
        
        $sql = "SELECT  IFNULL(s.stock,0) as 'cantidad', b.idbodega, b.nombre, SUM(IFNULL(s.stock,0)) as 'stock', p.nombre as 'producto'
                FROM bodega b
                LEFT JOIN stockprodbodega s on b.idbodega = s.idbodega and s.idproducto = $idproducto
                LEFT JOIN producto p on p.idproducto = $idproducto
                GROUP by b.idbodega";
        
        return ejecutarConsulta($sql);
    }
    
}
