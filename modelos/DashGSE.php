<?php
require_once '../config/conexion.php';
/**
 * Description of DashGSE
 *
 * @author azuni
 */
class DashGSE {
    //put your code here
    function __construct() {}
    
    function NumEquiposCartera(){
        $sql = "SELECT COUNT(*) as 'cartera' FROM `ascensor` Where idcontrato is not NULL and condicion = 1";
        return ejecutarConsultaSimpleFila($sql);
    }
    
    function datosDashGSE($idtservicio){
        $sql = "Select (SELECT count(*) as guiames FROM `servicio` Where MONTH(created_time) = MONTH(curdate()) and YEAR(created_time) = YEAR(curdate()) AND idtservicio = $idtservicio and estadofin is not null and closed_time is not null) as 'guiames',"
                . "(SELECT count(*) as guiadia FROM `servicio` Where DAY(created_time) = DAY(curdate()) and MONTH(created_time) = MONTH(curdate()) and YEAR(created_time) = YEAR(curdate()) AND idtservicio = $idtservicio and estadofin is not null and closed_time is not null) as 'guiadia',"
                . "(SELECT count(*) as guiaenproceso FROM `servicio` Where estadofin is null and idtservicio = $idtservicio AND MONTH(created_time) = MONTH(curdate()) and YEAR(created_time) = YEAR(curdate())) as 'guiaenproceso',"
                . "(SELECT count(*) as guiacompletada FROM `servicio` Where estadofin is not null and idtservicio = $idtservicio AND MONTH(created_time) = MONTH(curdate()) and YEAR(created_time) = YEAR(curdate()) and closed_time is not null) as 'guiacompletada',"
                . "(SELECT count(*) as guiacerrada FROM `servicio` Where reqfirma = 1 and firma is null and idtservicio = $idtservicio AND MONTH(created_time) = MONTH(curdate()) and YEAR(created_time) = YEAR(curdate()) and closed_time is not null) as 'guiacerrada', "
                . "(SELECT count(*) as guiacerradaSF FROM `servicio` Where reqfirma = 0 and idtservicio = $idtservicio AND MONTH(created_time) = MONTH(curdate()) and YEAR(created_time) = YEAR(curdate()) and closed_time is not null) as 'guiacerradaSF', "
                . "(SELECT COUNT(*) as 'cartera' FROM `ascensor` Where idcontrato is not NULL and condicion = 1) as 'cartera'";
        return ejecutarConsultaSimpleFila($sql);
    }
    
    
    function datosDashGSEMasdeuno($idtservicios){
        $sql = "Select distinct ts.nombre, ts.idtservicio, "
            . "(SELECT count(*) as guiames FROM `servicio` Where MONTH(created_time) = MONTH(curdate()) and YEAR(created_time) = YEAR(curdate()) AND idtservicio = s.idtservicio) as 'guiames',"
            . "(SELECT count(*) as guiadia FROM `servicio` Where DAY(created_time) = DAY(curdate()) and MONTH(created_time) = MONTH(curdate()) and YEAR(created_time) = YEAR(curdate()) AND idtservicio = s.idtservicio) as 'guiadia', "
            . "(SELECT count(*) as guiaenproceso FROM `servicio` Where estadofin is null and idtservicio = s.idtservicio AND MONTH(created_time) = MONTH(curdate()) and YEAR(created_time) = YEAR(curdate())) as 'guiaenproceso', "
            . "(SELECT count(*) as guiacompletada FROM `servicio` Where estadofin is not null and idtservicio = s.idtservicio AND MONTH(created_time) = MONTH(curdate()) and YEAR(created_time) = YEAR(curdate()) and closed_time is not null) as 'guiacompletada', "
            . "(SELECT count(*) as guiacerrada FROM `servicio` Where reqfirma = 1 and firma is null and idtservicio = s.idtservicio AND MONTH(created_time) = MONTH(curdate()) and YEAR(created_time) = YEAR(curdate()) and closed_time is not null) as 'guiacerrada',  "
            . "(SELECT count(*) as guiacerrada FROM `servicio` Where reqfirma = 1 and firma is not null and idtservicio = s.idtservicio AND MONTH(created_time) = MONTH(curdate()) and YEAR(created_time) = YEAR(curdate()) and closed_time is not null) as 'guiacerradaCF',"    
            . "(SELECT count(*) as guiacerradaSF FROM `servicio` Where reqfirma = 0 and idtservicio = s.idtservicio AND MONTH(created_time) = MONTH(curdate()) and YEAR(created_time) = YEAR(curdate()) and closed_time is not null) as 'guiacerradaSF',"
            . "(SELECT COUNT(*) as 'cartera' FROM `ascensor` Where idcontrato is not NULL and condicion = 1) as 'cartera' "
            . "FROM servicio s "
            . "inner join tservicio ts on ts.idtservicio = s.idtservicio "
            . "Where s.idtservicio in ($idtservicios) "
            . "group by ts.idtservicio";
            //var_dump($sql);
        return ejecutarConsulta($sql);
    }
    
    
    function datosDashemergencia($idtservicio, $estados){
        $sql = "Select distinct te.nombre, ts.idtservicio, "
            ." (SELECT count(*) as guiames FROM `servicio` Where MONTH(created_time) = MONTH(curdate()) and YEAR(created_time) = YEAR(curdate()) AND idtservicio = s.idtservicio and estadofin = s.estadofin) as 'guiames', "
            ." (SELECT count(*) as guiadia FROM `servicio` Where DAY(created_time) = DAY(curdate()) and MONTH(created_time) = MONTH(curdate()) and YEAR(created_time) = YEAR(curdate()) AND idtservicio = s.idtservicio and estadofin = s.estadofin) as 'guiadia',  "
            ." (SELECT count(*) as guiaenproceso FROM `servicio` Where estadofin is null and idtservicio = s.idtservicio AND MONTH(created_time) = MONTH(curdate()) and YEAR(created_time) = YEAR(curdate()) and estadofin = s.estadofin) as 'guiaenproceso', "
            ." (SELECT count(*) as guiacompletada FROM `servicio` Where estadofin is not null and idtservicio = s.idtservicio AND MONTH(created_time) = MONTH(curdate()) and YEAR(created_time) = YEAR(curdate()) and estadofin = s.estadofin and closed_time is not null) as 'guiacompletada', "
            ." (SELECT count(*) as guiacerrada FROM `servicio` Where reqfirma = 1 and firma is null and idtservicio = s.idtservicio AND MONTH(created_time) = MONTH(curdate()) and YEAR(created_time) = YEAR(curdate()) and estadofin = s.estadofin and closed_time is not null) as 'guiacerrada',  "
            ." (SELECT count(*) as guiacerradaSF FROM `servicio` Where reqfirma = 0 and idtservicio = s.idtservicio AND MONTH(created_time) = MONTH(curdate()) and YEAR(created_time) = YEAR(curdate()) and estadofin = s.estadofin and closed_time is not null) as 'guiacerradaSF',"
            ." (SELECT COUNT(*) as 'cartera' FROM `ascensor` Where idcontrato is not NULL and condicion = 1) as 'cartera' "
            ." FROM servicio s "
            ." inner join tservicio ts on ts.idtservicio = s.idtservicio "
            ." inner join testado te on te.id = s.estadofin "
            ." Where s.idtservicio in ($idtservicio)  and s.estadofin in ($estados) "
            ." group by te.id";
        return ejecutarConsulta($sql);
    }
    
    //ultimos 12 meses
    function graficoUltimos12Meses($idtservicio){
        $sql = " SELECT MONTH(s.created_time) mes , MONTHNAME(s.created_time) mesname, YEAR(s.created_time) ano, count(*) as guiacompletada "
            . " FROM `servicio` s "
            . " INNER JOIN tservicio ts ON ts.idtservicio = s.idtservicio "
            . " Where s.estadofin is not null and s.idtservicio in ($idtservicio) AND s.created_time >= DATE_ADD(curdate(), INTERVAL -12 MONTH) "
            . " group by MONTH(created_time),  YEAR(s.created_time) "
            . " order by YEAR(s.created_time),MONTH(s.created_time)";
        return ejecutarConsulta($sql);
    }
    
    
    
}
