<?php

require "../config/conexion.php";

Class FacturaVenta {

    //Constructor para instancias
    public function __construct() {
        
    }

    public function LVF() {
        $sql = "SELECT ve.idventa, pro.nombre AS proyecto, imp.codigo, CONCAT(man.razon_social, ' - ', man.rut) AS mandante, esta.nombre AS estado, DATE(ve.created_time) AS fecha  FROM venta ve INNER JOIN proyecto pro ON ve.idventa = pro.idventa INNER JOIN importacion imp ON ve.idimportacion = imp.idimportacion INNER JOIN estadopro esta ON pro.estado = esta.estado INNER JOIN mandante man ON ve.idmandante = man.idmandante WHERE ve.condicion = 1 AND ve.estado = 3 ORDER BY pro.nombre ASC";
        return ejecutarConsulta($sql);
    }

    public function MV($idventa) {
        $sql = "SELECT ven.idventa, ven.codigo, imp.codigo AS pedido, man.razon_social, man.rut, pro.nombre, CONCAT(pro.calle,' ', pro.numero) AS direccion, ven.montosi, ven.montosn, mon.simbolo AS monedasn , mone.simbolo AS monedasi , CONCAT(comu.comuna_nombre, ', ',regi.region_nombre, ' (', regi.region_ordinal,')') AS comuna FROM venta ven INNER JOIN importacion imp ON ven.idimportacion = imp.idimportacion INNER JOIN mandante man ON ven.idmandante = man.idmandante INNER JOIN proyecto pro ON ven.idventa = pro.idventa INNER JOIN moneda mon ON ven.idmonedasn = mon.idmoneda INNER JOIN moneda mone ON ven.idmonedasi = mone.idmoneda INNER JOIN regiones regi ON pro.idregion = regi.region_id INNER JOIN comunas comu ON pro.idcomuna = comu.comuna_id WHERE ven.idventa = '$idventa'";
        return ejecutarConsultaSimpleFila($sql);
    }
    
    public function MVSol($idventa) {
        $sql = "SELECT ven.idventa, ven.codigo, imp.codigo AS pedido, man.razon_social, man.rut, CONCAT(man.calle,' ', man.numero) AS direccion_man, pro.nombre, CONCAT(pro.calle,' ', pro.numero) AS direccion, ven.montosi, ven.montosn, mon.simbolo AS monedasn , mone.simbolo AS monedasi , CONCAT(comu.comuna_nombre, ', ',regi.region_nombre, ' (', regi.region_ordinal,')') AS comuna, DATE_FORMAT(now(), '%d-%m-%Y') AS revised FROM venta ven INNER JOIN importacion imp ON ven.idimportacion = imp.idimportacion INNER JOIN mandante man ON ven.idmandante = man.idmandante INNER JOIN proyecto pro ON ven.idventa = pro.idventa INNER JOIN moneda mon ON ven.idmonedasn = mon.idmoneda INNER JOIN moneda mone ON ven.idmonedasi = mone.idmoneda INNER JOIN regiones regi ON pro.idregion = regi.region_id INNER JOIN comunas comu ON pro.idcomuna = comu.comuna_id WHERE ven.idventa = '$idventa'";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function SolFact($idventa) {
        $sql = "SELECT form.porcentaje, form.idtformapago, IF(form.idtformapago = 1, ascen.montosi, ascen.montosn) AS monto FROM formapago form INNER JOIN ascensor ascen ON form.idascensor = ascen.idascensor WHERE form.idascensor IN (SELECT idascensor FROM ascensor WHERE idventa = '$idventa') AND form.facturado = 1";
        return ejecutarConsulta($sql);
    }

    public function Factu($idventa) {
        $sql = "SELECT SUM(IF(montocon IS NULL, monto, montocon)) AS monto, idtformapago FROM factura_venta WHERE condicion = 1 and idventa = '$idventa' GROUP BY idtformapago";
        return ejecutarConsulta($sql);
    }

    public function LFact($idventa) {
        $sql = "SELECT fven.idfactura_venta, fven.numero, DATE(fven.fecha_fact) AS fecha, tforma.descripcion, mon.simbolo, fven.monto FROM factura_venta fven INNER JOIN tformapago tforma ON fven.idtformapago = tforma.idtformapago INNER JOIN moneda mon ON fven.idmoneda = mon.idmoneda WHERE fven.idventa='$idventa' AND fven.condicion = 1 ORDER BY fven.fecha_fact DESC";
        return ejecutarConsulta($sql);
    }

    public function DFact($idfactura) {
        $sql = "UPDATE factura_venta SET condicion='0' WHERE idfactura_venta ='$idfactura'";
        return ejecutarConsulta($sql);
    }

    public function GFact($idventa, $idtformapago, $numero, $monto, $idmoneda, $fechafact) {
        $sql = "INSERT INTO factura_venta (idventa, idtformapago, numero, monto, idmoneda, fecha_fact, created_user) VALUES ('$idventa', '$idtformapago', '$numero', '$monto', '$idmoneda', '$fechafact', '1')";
        return ejecutarConsulta($sql);
    }
    
    public function GSolFact($idformapago) {
        $sql = "UPDATE formapago SET facturado = 1 WHERE idformapago = '$idformapago'";
        return ejecutarConsulta($sql);
    }
    
    public function SelecMon($idtformapago) {
        $sql = "SELECT * FROM moneda WHERE idtformapago = '$idtformapago' AND condicion = 1";
        return ejecutarConsulta($sql);
    }
    
    public function SelecTF() {
        $sql = "SELECT * FROM tformapago";
        return ejecutarConsulta($sql);
    }
    
    public function FormPaSI($idventa) {
        $sql = "SELECT form.idtformapago, form.descripcion, form.porcentaje  FROM formapago form INNER JOIN ascensor ascen ON form.idascensor = ascen.idascensor  WHERE ascen.idventa = '$idventa' AND idtformapago = 1 GROUP BY descripcion ORDER BY form.secuencia ASC";
        return ejecutarConsulta($sql);
    }
    
    public function FormPaSN($idventa) {
        $sql = "SELECT form.idtformapago, form.descripcion, form.porcentaje  FROM formapago form INNER JOIN ascensor ascen ON form.idascensor = ascen.idascensor  WHERE ascen.idventa = '$idventa' AND idtformapago = 2 GROUP BY descripcion ORDER BY form.secuencia ASC";
        return ejecutarConsulta($sql);
    }
    
    public function LisFormPaSI($idventa) {
        $sql = "SELECT form.idformapago, form.facturado, ascen.codigo, ascen.montosi, form.idtformapago, form.porcentaje FROM formapago form INNER JOIN ascensor ascen ON form.idascensor = ascen.idascensor WHERE ascen.idventa = '$idventa' AND idtformapago = 1 ORDER BY ascen.idascensor ASC";
        return ejecutarConsulta($sql);
    }
    
    public function LisFormPaSN($idventa) {
        $sql = "SELECT form.idformapago, form.facturado, ascen.codigo, ascen.montosn, form.idtformapago, form.porcentaje FROM formapago form INNER JOIN ascensor ascen ON form.idascensor = ascen.idascensor WHERE ascen.idventa = '$idventa' AND idtformapago = 2 ORDER BY ascen.idascensor ASC";
        return ejecutarConsulta($sql);
    }
    
    public function Nascen($idventa) {
        $sql = "SELECT COUNT(idascensor) AS nascen FROM `ascensor` WHERE idventa='$idventa'";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function ninfo($idventa) {
        $sql = "SELECT descripcion FROM infoventa info INNER JOIN ascensor ascen ON info.idascensor = ascen.idascensor WHERE ascen.idventa = '$idventa' GROUP BY info.descripcion";
        return NumeroFilas($sql);
    }
    
    public function pdfinfo($idventa) {
        $sql = "SELECT DISTINCT info.descripcion,  DATE_FORMAT(info.fecha , '%d-%m-%Y') AS fecha FROM infoventa info INNER JOIN ascensor ascen ON info.idascensor = ascen.idascensor WHERE ascen.idventa = '$idventa' ORDER BY info.fecha ASC";
        return ejecutarConsulta($sql);
    }
    
    public function pdfformasi($idventa) {
        $sql = "SELECT form.descripcion ,form.porcentaje FROM formapago form INNER JOIN ascensor ascen ON form.idascensor = ascen.idascensor WHERE ascen.idventa = '$idventa' AND idtformapago = 1 GROUP BY form.descripcion ORDER BY form.secuencia ASC";
        return ejecutarConsulta($sql);
    }
    
    public function pdfformasn($idventa) {
        $sql = "SELECT form.descripcion ,form.porcentaje FROM formapago form INNER JOIN ascensor ascen ON form.idascensor = ascen.idascensor WHERE ascen.idventa = '$idventa' AND idtformapago = 2 GROUP BY form.descripcion ORDER BY form.secuencia ASC";
        return ejecutarConsulta($sql);
    }
      
    public function pdfboleta($idventa) {
        $sql = "SELECT banco, descripcion, documento, tipo, DATE_FORMAT(validez , '%d-%m-%Y') AS validez FROM boletas WHERE idventa = '$idventa'";
        return ejecutarConsulta($sql);
    }
    
    public function pdfequipos($idventa) {
        $sql = "SELECT ascen.codigo, ascen.ken, ascen.capkg, ascen.capper, ascen.paradas, ascen.velocidad, tascen.nombre AS tascensor, mar.nombre AS marca, mo.nombre AS modelo FROM ascensor ascen INNER JOIN tascensor tascen ON ascen.idtascensor = tascen.idtascensor INNER JOIN marca mar ON ascen.marca = mar.idmarca LEFT JOIN modelo mo ON ascen.modelo = mo.idmodelo WHERE ascen.idventa='$idventa'";
        return ejecutarConsulta($sql);
    }

}

?>