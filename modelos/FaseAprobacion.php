<?php
include_once '../config/conexion.php';
/**
 * Description of FaseAprobacion
 *
 * @author azuni
 */
class FaseAprobacion {
    //put your code here
    function __construct() {
        
    }
    
    function insertar($idtcentrocosto, $iduser, $orden, $completasolicitud){
        $sql = "INSERT INTO `faseaprobacion`(`idtcentrocosto`, `iduser`, `orden`, `completasolicitud`) "
                . "VALUES ($idtcentrocosto, $iduser,$orden,$completasolicitud)";
        return ejecutarConsulta($sql);
    }
    
    function editar($idfase, $idtcentrocosto, $iduser, $orden, $completasolicitud){
        $sql = "UPDATE `faseaprobacion` SET "
                . "`idtcentrocosto`=$idtcentrocosto,"
                . "`iduser`=$iduser,"
                . "`orden`=$orden,"
                . "`completasolicitud`=$completasolicitud"
                . " WHERE `idfase`=$idfase ";
        return ejecutarConsulta($sql);
    }
    
    function listar($idtcentrocosto){
        $sql = "SELECT fa.*, tc.nombre, concat(u.nombre, ' ', u.apellido) as 'usuario'
                FROM `faseaprobacion` fa
                INNER JOIN tcentrocosto tc on tc.idtcentrocosto = fa.idtcentrocosto
                INNER JOIN user u ON u.iduser = fa.iduser
                WHERE fa.idtcentrocosto = $idtcentrocosto 
                ORDER BY fa.orden ASC";
        return ejecutarConsulta($sql);
    }
    
    
    function listarTcentrocosto(){
        $sql = "SELECT tc.`idtcentrocosto`, tc.`nombre`, tc.`condicion`, (SELECT COUNT(*) FROM faseaprobacion f WHERE f.idtcentrocosto = tc.idtcentrocosto) as 'fases'
                FROM `tcentrocosto` tc 
                WHERE tc.condicion = 1";
        return ejecutarConsulta($sql);
    }
    
    function eliminar($idfase){
        $sql = "DELETE FROM `faseaprobacion` WHERE `idfase`= $idfase";
        return ejecutarConsulta($sql);
    }
    
    
    public function EnviarMail($emailfrom, $fromName, $username, $pass, $asunto, $mensaje, $aprueba, $nombaprueba){
            $Mailer = new PHPMailer();

            $Mailer->isSMTP();
            $Mailer->CharSet = 'UTF-8';

            $Mailer->Port = 587;
            $Mailer->SMTPAuth = true;
            $Mailer->SMTPSecure = "tls";
            $Mailer->SMTPDebug = 0;
            $Mailer->Debugoutput = 'html';
            $Mailer->Host = "smtp.gmail.com";

            $Mailer->From = $emailfrom; //"emergencia@fabrimetal.cl";
            $Mailer->FromName = $fromName; //"Sistema de solicitud de presupuestos";
            $Mailer->Subject = $asunto; //"Solicitud Informacion - Sol. Presupuesto N° " . $resp["idpresupuesto"];
            $Mailer->msgHTML($mensaje);
            $Mailer->SMTPAuth = true;
            $Mailer->Username = $username; //"emergencia@fabrimetal.cl";
            $Mailer->Password = $pass; //"1322cncn";

            $Mailer->addAddress($aprueba, $nombaprueba);
            
            
            //DIRECCION DE PRUEBA
            //$Mailer->addAddress("omaldonado@fabrimetal.cl",'');
            
            if (!$Mailer->send()) {
                echo "Error al enviar correo: " . $Mailer->ErrorInfo . "<br>";
            } else {
                echo "Correo enviado exitosamente<br>";
            }  
        }
    
}
