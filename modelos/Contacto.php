<?php 

require "../config/conexion.php";

	Class Contacto{
		//Constructor para instancias
		public function __construct(){

		}

		public function insertar($nombre,$email,$telefonomovil){
			$sql="INSERT INTO contacto(nombre, email, telefonomovil) VALUES ('$nombre','$email','$telefonomovil')";
			return ejecutarConsulta_retornarID($sql);
		}

		public function contacto_ec($idcontacto, $idedificio_contrato){
			$sql="INSERT INTO contacto_ec(idedificio_contrato, idcontacto, condicion) VALUES ('$idedificio_contrato','$idcontacto',1)";
			return ejecutarConsulta($sql);
		}
                
                public function ContactoEdificio($nombre,$email,$telefonomovil, $idedificio){
			$sql="INSERT INTO contacto(nombre, email, telefonomovil, idedificio ) VALUES ('$nombre','$email','$telefonomovil','$idedificio')";
			return ejecutarConsulta($sql);
		}
                
                public function insertar_contacto($nombre,$cargo,$email,$telefonomovil,$telefonoresi,$idedificio) {
                    $sql="INSERT INTO `contacto`(`nombre`, `cargo`, `email`, `telefonomovil`, `telefonoresi`, `idedificio`) VALUES ('$nombre','$cargo','$email','$telefonomovil','$telefonoresi','$idedificio')";
                    return ejecutarConsulta($sql);                    
                }
                
                public function editar_contacto($idcontacto,$nombre,$cargo,$email,$telefonomovil,$telefonoresi) {
                    $sql="UPDATE `contacto` SET `nombre`='$nombre',`cargo`='$cargo',`email`='$email',`telefonomovil`='$telefonomovil',`telefonoresi`='$telefonoresi' WHERE idcontacto=".$idcontacto;
                    return ejecutarConsulta($sql);                    
                }
                
                public function activar_contacto($idcontacto) {
                    $sql="UPDATE contacto SET `condicion`=1 WHERE idcontacto=".$idcontacto;
                    return ejecutarConsulta($sql);                     
                }
                
                public function desactivar_contacto($idcontacto) {
                    $sql="UPDATE contacto SET `condicion`=0 WHERE idcontacto=".$idcontacto;
                    return ejecutarConsulta($sql);                     
                }
                
                public function listar_contactos($idedificio,$administrador) {
                    $cond=($administrador==0)?" AND condicion=1":"";
                    $sql="SELECT `idcontacto`, `nombre`, `cargo`, `email`, `telefonomovil`, `telefonoresi`, `idedificio`,condicion FROM `contacto` where idedificio=".$idedificio."".$cond;
                    return ejecutarConsulta($sql);
                }
                
                public function mostrarContacto($idcontacto) {
                    $sql="SELECT `idcontacto`, `nombre`, `cargo`, `email`, `telefonomovil`, `telefonoresi`, `idedificio` FROM `contacto` WHERE idcontacto=".$idcontacto;
                    return ejecutarConsultaSimpleFila($sql);
                }
                
	}
?>