<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once '../config/conexion.php';
/**
 * Description of EstadoPro
 *
 * @author azuni
 */
class EstadoPro {
    //put your code here
    
    public function __construct() {
        
    }
    
    public function Insertar($estado, $nombre, $color, $carga, $condicio){
        $sql = "INSERT INTO `estadopro`(`estado`, `nombre`, `color`, `carga`, `condicio`) "
                . "VALUES ($estado,'$nombre','$color', $carga, $condicio])";
        return ejecutarConsulta($sql);
    }
    
    public function Editar($idestadopro, $estado, $nombre, $color, $carga, $condicio){
        $sql = "UPDATE `estadopro` "
                . "SET `estado`= $estado,"
                . "`nombre`= '$nombre',"
                . "`color`= '$color',"
                . "`carga`= $carga,"
                . "`condicio`= $condicio "
                . "WHERE `idestadopro`= $idestadopro";
        return ejecutarConsulta($sql);
    }
    
    public function Mostrar($idestadopro){
        $sql = "SELECT * FROM estadopro WHERE idestadopro = $idestadopro";
        return ejecutarConsultaSimpleFila($sql);
    }
    
    public function Listar(){
        $sql = "SELECT * FROM estadopro";
        return ejecutarConsulta($sql);
    }
    
    /**
     * Entrega el listado de los estados, para esto debemos entregarle el codigo inicial
     * 0 = Instalaciones
     * 200 = Modernizacion
     * 300 = Suministro internacional
     * @param int $codigoInicial
     * @return type
     */
    public function ListarCodigos($codigoInicial){
        $codigofinal = $codigoInicial + 99;
        $sql = "SELECT `idestadopro`, `estado`, `nombre`, `color`, `carga`, `condicio` "
                . "FROM `estadopro` "
                . "WHERE (estado >= $codigoInicial and estado <= $codigofinal) and condicio = 1";
        return ejecutarConsulta($sql);
    }
}
