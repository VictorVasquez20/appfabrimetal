<?php
require_once '../config/conexion.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of solcotizacion
 *
 * @author azuni
 */
class solcotizacion {
    //put your code here
    
    function __construct() {
        
    }
    
    function insertar($idsolicitud, $idmoneda, $idcondpago, $idproveedor, $archivo){
        $sql = "INSERT INTO `solcotizacion`(`idsolicitud`, `idmoneda`, `idcondpago`, `idproveedor`, `archivo`) "
                . "VALUES ($idsolicitud, $idmoneda,$idcondpago,$idproveedor, '$archivo')";
        //var_dump($sql);
        return ejecutarConsulta($sql);
    }
    
    function editar($idcotizacion, $idsolicitud, $idmoneda, $idcondpago, $idproveedor, $archivo){
        $sql = "UPDATE `solcotizacion` SET "
                . "`idsolicitud`=$idsolicitud ,"
                . "`idmoneda`= $idmoneda,"
                . "`idcondpago`= $idcondpago,"
                . "`idproveedor`=$idproveedor,"
                . "`archivo`='$archivo' "
                . " WHERE `idcotizacion`= $idcotizacion ";
        //var_dump($sql);
        return ejecutarConsulta($sql);
    }
    
    function mostrar($idsolicitud){
        $sql = "SELECT s.idcotizacion, s.idsolicitud, s.idmoneda, m.nombre as 'moneda', s.idcondpago, c.nombre as 'condpago', s.idproveedor, p.razonsocial, p.rut, s.archivo, IFNULL(sa.archivo, '') as 'selected'
                FROM `solcotizacion` s
                INNER JOIN proveedor p on p.idproveedor = s.idproveedor
                INNER JOIN moneda m on m.idmoneda = s.idmoneda
                INNER JOIN condicionpago c on c.idcondicionpago = s.idcondpago
                LEFT JOIN soladquisicion sa on sa.idsolicitud = s.idsolicitud and sa.archivo = s.archivo
                WHERE s.`idsolicitud`= $idsolicitud ";
        return ejecutarConsulta($sql);
    }
    
    function getCotizacion($idcotizacion){
        $sql = "SELECT `idcotizacion`, `idsolicitud`, `idmoneda`, `idcondpago`, `idproveedor`, `archivo` FROM `solcotizacion` WHERE idcotizacion = $idcotizacion";
        return ejecutarConsultaSimpleFila($sql);
    }
    
    
}
