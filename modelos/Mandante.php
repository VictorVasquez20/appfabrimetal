<?php
require_once '../config/conexion.php';

/**
 * Description of Mandante
 *
 * @author azuni
 */
class Mandante {
    //put your code here
    function __construct() {
        
    }
    
    function Insertar($razon_social, $rut, $calle, $numero, $oficina, $idregion, $idcomuna, $created_user, $condicion){
        $sql = "INSERT INTO `mandante`(`razon_social`, `rut`, `calle`, `numero`, `oficina`, `idregion`, `idcomuna`, `created_user`, `condicion`) "
             . "VALUES ('$razon_social','$rut','$calle', '$numero','$oficina',$idregion,$idcomuna, $created_user,$condicion)";
        return ejecutarConsulta_retornarID($sql);
    }
    
    function Editar($idmandante, $razon_social, $rut, $calle, $numero, $oficina, $idregion, $idcomuna, $condicion){
        $sql = "UPDATE `mandante` "
                . "SET `razon_social`= '$razon_social',"
                . "`rut`= '$rut',"
                . "`calle`= '$calle',"
                . "`numero`= '$numero',"
                . "`oficina`= '$oficina',"
                . "`idregion`= $idregion,"
                . "`idcomuna`= $idcomuna,"
                . "`condicion`= $condicion "
                . "WHERE  `idmandante`= $idmandante ";
        return ejecutarConsulta($sql);
    }
    
    function Listar(){
        $sql = "SELECT * FROM mandante order by razon_social";
        return ejecutarConsulta($sql);
    }
    
    function Mostrar($idmandante){
        $sql = "SELECT * FROM mandante WHERE idmandante = $idmandante";
        return ejecutarConsultaSimpleFila($sql);
    }
}
