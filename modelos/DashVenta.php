<?php
require_once '../config/conexion.php';
/**
 * Description of DashVenta
 *
 * @author azuni
 */
class DashVenta {
    //put your code here
    
    function __construct(){
        
    }
    
    function Listado($anio_busq,$mes_busq){
        
        $sql = "SELECT v.idventa, m.razon_social, m.rut, m.calle, m.numero, i.codigo as 'codVenta', p.nombre as 'pronomb', p.calle as 'procalle' , p.numero as 'pronumero', "
            . " IFNULL(v.montosi, 0) as 'montosi', v.idmonedasi, IFNULL(v.montosn,0) as 'montosn', v.idmonedasn, "
            . " IFNULL((SELECT sum(fv.monto) as SI FROM `factura_venta` fv Where fv.idventa = v.idventa and fv.idtformapago = 1), 0) as 'mntoFactSI', "
            . " IFNULL((SELECT sum(fv.monto) as SN FROM `factura_venta` fv Where fv.idventa = v.idventa and fv.idtformapago = 2), 0)as 'mntoFactSN', "
            . " (SELECT simbolo FROM `moneda` mon Where mon.idmoneda = v.idmonedasi) as 'monedaSI', "
            . " (SELECT simbolo FROM `moneda` mon Where mon.idmoneda = v.idmonedasn) as 'monedaSN' "
            . " FROM `venta` v left join mandante m on m.idmandante = v.idmandante "
            . " left join importacion i on i.idimportacion = v.idimportacion "
            . " left join proyecto p on p.idventa = v.idventa "
            . " Where v.condicion = 1";
        
        if($anio_busq>0){
           $sql .=" AND YEAR(v.fecha)=$anio_busq";  
        }
      
        if($mes_busq >0){                  
            $sql .=" AND MONTH(v.fecha)=$mes_busq";
        }
         
        return ejecutarConsulta($sql);
    }
    
    function Anioventa() {
        $sql="SELECT YEAR(fecha) as anio FROM `venta` WHERE fecha !='' GROUP BY YEAR(fecha) ORDER BY YEAR(fecha) DESC";
        return ejecutarConsulta($sql);
    }
    
    function Mesventa($anio_busq) {
        
        $sql="SELECT MONTH(fecha) as mes,
                CASE
                    WHEN MONTH(fecha)=1 THEN 'ENERO'
                    WHEN MONTH(fecha)=2 THEN 'FEBRERO'
                    WHEN MONTH(fecha)=3 THEN 'MARZO'
                    WHEN MONTH(fecha)=4 THEN 'ABRIL'
                    WHEN MONTH(fecha)=5 THEN 'MAYO'
                    WHEN MONTH(fecha)=6 THEN 'JUNIO'
                    WHEN MONTH(fecha)=7 THEN 'JULIO'
                    WHEN MONTH(fecha)=8 THEN 'AGOSTO'
                    WHEN MONTH(fecha)=9 THEN 'SEPTIEMBRE'
                    WHEN MONTH(fecha)=10 THEN 'OCTUBRE'
                    WHEN MONTH(fecha)=11 THEN 'NOVIEMBRE'
                    WHEN MONTH(fecha)=12 THEN 'DICIEMBRE'
                    ELSE 'SIN NOMBRE'
                END as nombre_mes
                FROM `venta` 
                WHERE fecha != ''
                AND YEAR(fecha) = $anio_busq
                GROUP BY MONTH(fecha)
                ORDER BY MONTH(fecha)";
        
        return ejecutarConsulta($sql);
    }
}
