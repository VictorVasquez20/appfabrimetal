<?php
require_once '../config/conexion.php';
/**
 * Description of DashContabilidad
 *
 * @author azuni
 */
class DashContabilidad {
    //put your code here
    
    function __construct() {
        
    }
    
    function getResumenGSE(){
        $sql = "Select distinct ts.nombre, ts.idtservicio, "
                ." (SELECT count(*) as guiames FROM `servicio` Where MONTH(created_time) = MONTH(curdate()) and YEAR(created_time) = YEAR(curdate()) AND idtservicio = s.idtservicio and closed_time is not null and estadofin is not null) as 'gsemes',"
                ." (SELECT count(*) as guiames FROM `servicio` Where MONTH(created_time) = MONTH(curdate()) and YEAR(created_time) = YEAR(curdate()) AND idtservicio = s.idtservicio and idfactura is not null) as 'gsefact' "
                ." FROM servicio s "
                ." inner join tservicio ts on ts.idtservicio = s.idtservicio "
                ." group by ts.idtservicio ";
        return ejecutarConsulta($sql);
    }
    
    
    function getResumenGSEfiltro($fecha){
        $sql = "Select distinct ts.nombre, ts.idtservicio, "
                ." (SELECT count(*) as guiames FROM `servicio` Where MONTH(created_time) = MONTH('$fecha') and YEAR(created_time) = YEAR('$fecha') AND idtservicio = s.idtservicio and closed_time is not null and estadofin is not null) as 'gsemes',"
                ." (SELECT count(*) as guiames FROM `servicio` Where MONTH(created_time) = MONTH('$fecha') and YEAR(created_time) = YEAR('$fecha') AND idtservicio = s.idtservicio and idfactura is not null) as 'gsefact' "
                ." FROM servicio s "
                ." inner join tservicio ts on ts.idtservicio = s.idtservicio "
                ." group by ts.idtservicio ";
        return ejecutarConsulta($sql);
    }
    
    function getGraficoGSE(){
        /*$sql = "Select mes, mesname, sum(guiamesfact) as 'guiamesfact', SUM(guiames) as 'guiames' "
                ." FROM((SELECT MONTH(created_time) as mes, MONTHNAME(created_time) mesname, 0 as 'guiamesfact',COUNT(*) as 'guiames' FROM `servicio` Where YEAR(created_time) = YEAR(curdate()) group by MONTH(created_time)) union "
                ." (SELECT MONTH(created_time) as mes, MONTHNAME(created_time) mesname, COUNT(*) as 'guiamesfact', 0 FROM `servicio` Where YEAR(created_time) = YEAR(curdate()) and idfactura is not null group by MONTH(created_time))) as tabla "
                ." group by mes order by mes";*/
        $sql = "Select mes, mesname, sum(guiamesfact) as 'guiamesfact', SUM(guiames) as 'guiames' , idtservicio FROM(
                (SELECT MONTH(created_time) as mes, MONTHNAME(created_time) mesname, 0 as 'guiamesfact',COUNT(*) as 'guiames', idtservicio FROM `servicio` Where YEAR(created_time) = YEAR(curdate()) group by MONTH(created_time) , idtservicio) 
                union 
                (SELECT MONTH(created_time) as mes, MONTHNAME(created_time) mesname, COUNT(*) as 'guiamesfact', 0 , idtservicio FROM `servicio` Where YEAR(created_time) = YEAR(curdate()) and idfactura is not null group by MONTH(created_time), idtservicio)) as tabla 
                group by mes , idtservicio order by mes";
        return ejecutarConsulta($sql);
    }
    
    function graficoGSEemergencias(){
        $sql = "SELECT MONTH(created_time) as mes, MONTHNAME(created_time) mesname, 0 as 'guiamesfact',COUNT(*) as 'guiames', idtservicio , estadofin
                FROM `servicio` 
                Where YEAR(created_time) = YEAR(curdate()) and idtservicio = 2 and estadofin is not null
                group by MONTH(created_time) , idtservicio, estadofin";
        return ejecutarConsulta($sql);
    }
    
    function GSEemergencias($fecha){
        $sql = "SELECT MONTH(created_time) as mes, MONTHNAME(created_time) mesname, 0 as 'guiamesfact',COUNT(*) as 'guiames', idtservicio , estadofin
                FROM `servicio` 
                Where YEAR(created_time) = YEAR('$fecha') and MONTH(created_time) = MONTH('$fecha') and idtservicio = 2 and estadofin is not null
                group by MONTH(created_time) , idtservicio, estadofin";
        return ejecutarConsulta($sql);
    }
}
