<?php
require_once '../config/conexion.php';
/**
 * Description of CondicionPago
 *
 * @author azuni
 */
class CondicionPago {
    //put your code here
    
    function __construct() {
    }
    
    function Insertar($nombre, $condicion){
        $sql = "INSERT INTO `condicionpago`(`nombre`, `condicion`) VALUES ('$nombre', $condicion)";
        return ejecutarConsulta_retornarID($sql);
    }
    
    function Editar($idcondicionpago, $nombre, $condicion){
        $sql = "UPDATE `condicionpago` "
                . "SET `nombre`= '$nombre',"
                . "`condicion`= $condicion "
                . "WHERE `idcondicionpago`= $idcondicionpago ";
        return ejecutarConsulta($sql);
    }
    
    function Listar(){
        $sql = "SELECT * FROM `condicionpago`";
        return ejecutarConsulta($sql);
    }
    
    function Mostrar($idcondicionpago){
        $sql = "SELECT * FROM `condicionpago` WHERE `idcondicionpago`= $idcondicionpago ";
        return ejecutarConsultaSimpleFila($sql);
    }
    
    function SelectCategoria(){
        $sql = "SELECT * FROM `condicionpago` WHERE condicion = 1";
        return ejecutarConsulta($sql);
    }
}
