<?php
require_once '../config/conexion.php';
/**
 * Description of InfoVenta
 *
 * @author azuni
 */
class InfoVenta {
    //put your code here
    function __construct() {
        
    }
    
    function Insertar( $idascensor, $descripcion, $fecha){
        $sql = "INSERT INTO `infoventa`(`idascensor`, `descripcion`, `fecha`) "
                . "VALUES ($idascensor,'$descripcion', '$fecha')";
        return ejecutarConsulta($sql);
    }
    
    function Editar($idinfoventa, $idascensor, $descripcion, $fecha){
        $sql = "UPDATE `infoventa` "
                . "SET `idascensor`= $idascensor,"
                . "`descripcion`= '$descripcion',"
                . "`fecha`= '$fecha' "
                . "WHERE `idinfoventa`= $idinfoventa";
        return ejecutarConsulta($sql);
    }
    
    function Listar($idventa){
        $sql = "SELECT iv.*, a.codigo FROM `infoventa` iv inner join ascensor a on a.idascensor = iv.idascensor WHERE iv.`idascensor` in (select a.idascensor from venta v inner join ascensor a on a.idventa = v.idventa Where v.idventa = $idventa) ORDER BY fecha, idinfoventa, descripcion";
        return ejecutarConsulta($sql);
    }
    
    function ListarRevisar($idventa){
        $sql = "SELECT * FROM `infoventa` Where idascensor in (SELECT idascensor from ascensor Where idventa = $idventa) group by descripcion, fecha order by idinfoventa";
        return ejecutarConsulta($sql);
    }
    
    function Eliminar($idinfoventa){
        $sql = "DELETE FROM `infoventa` WHERE `idinfoventa` in ($idinfoventa)";
        return ejecutarConsulta($sql);
    }
    
    function Eliminarxascensor($idascensor){
        $sql = "DELETE FROM `infoventa` WHERE idascensor = $idascensor";
        return ejecutarConsulta($sql);
    }
}
