<?php

require_once '../config/conexion.php';

/**
 * Description of Documento_servicio
 *
 * @author azuni
 */
class Documento_servicio {
    //put your code here
    
    function __construct() {
        
    }

    function Insertar($nombre, $archivo, $idservicio){
        $sql = "INSERT INTO `documentos_servicio`(`nombre`, `archivo`, `idservicio`) VALUES ('$nombre','$archivo', $idservicio)";
        return ejecutarConsulta($sql);
    }
    
    function Editar($iddocumento, $nombre, $archivo, $idservicio){
        $sql = "UPDATE `documentos_servicio` "
                . "SET `nombre`= '$nombre' ,"
                . "`archivo`= '$archivo',"
                . "`idservicio`= $idservicio "
                . "WHERE `iddocumento`= $iddocumento ";
        return ejecutarConsulta($sql);
    }
    
    function Listar($idservicio){
        $sql = "SELECT `iddocumento`, `nombre`, `archivo`, `idservicio` FROM `documentos_servicio` WHERE idservicio = $idservicio";
        
        return ejecutarConsulta($sql);
    }
    
    function EliminarTodos($idservicio){
        $sql = "DELETE FROM `documentos_servicio` WHERE idservicio = $idservicio";
        return ejecutarConsulta($sql);
    }
    
    function Eliminar($iddocumento){
        $sql = "DELETE FROM `documentos_servicio` WHERE  `iddocumento`= $iddocumento";
        return ejecutarConsulta($sql);
    }
    
}
