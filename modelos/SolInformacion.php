<?php 

require "../config/conexion.php";
require_once '../modelos/ContactoInfo.php';
require_once '../public/build/lib/PHPMailer/class.phpmailer.php';
require_once '../public/build/lib/PHPMailer/class.smtp.php';


Class SolInformacion{
        //Constructor para instancias
        public function __construct(){

        }

        public function insertar($nombre,$rut,$email,$telefono, $area, $mensaje){
            $sql="INSERT INTO solinformacion (nombre, rut, email, telefono, area, mensaje) VALUES ('$nombre', '$rut', '$email', '$telefono', '$area', '$mensaje')";
            return ejecutarConsulta($sql);
        }

        public function listar(){
                //$sql="SELECT * FROM solinformacion";
            $sql = "SELECT s.*, a.nombre as 'nombArea' FROM `solinformacion` s left join areainfo as a on a.idarea = s.area";
            return ejecutarConsulta($sql);
        }
        
        public function mostrar($idsolinformacion){
            $sql = "SELECT  s.*, a.nombre as 'nombArea' "
                    ." FROM `solinformacion` s"
                    ." left join areainfo as a on a.idarea = s.area "
                    ." WHERE idsolinformacion = $idsolinformacion";
            return ejecutarConsultaSimpleFila($sql);
        }
        
        public function editar($idsolinformacion, $procesado, $condicion){
            $p = $procesado == 0 ? 1 : 1 ;
            $c = $condicion == 0 ? 1 : 1;
            $sql = "UPDATE `solinformacion` "
                    . "SET "
                    //. "`closed_time`=[value-9]"
                    //. ",`closed_user`=[value-10],"
                    . "`procesado`= $p,"
                    . "`condicion`= $c "   
                    . " WHERE `idsolinformacion`= $idsolinformacion";           
            return ejecutarConsulta($sql);
        }


        public function EnviarMail($emailfrom, $fromName, $username, $pass, $asunto, $mensaje, $area){
            $Mailer = new PHPMailer();

            $Mailer->isSMTP();
            $Mailer->CharSet = 'UTF-8';

            $Mailer->Port = 587;
            $Mailer->SMTPAuth = true;
            $Mailer->SMTPSecure = "tls";
            $Mailer->SMTPDebug = 0;
            $Mailer->Debugoutput = 'html';
            $Mailer->Host = "smtp.gmail.com";

            $Mailer->From = $emailfrom; //"emergencia@fabrimetal.cl";
            $Mailer->FromName = $fromName; //"Sistema de solicitud de presupuestos";
            $Mailer->Subject = $asunto; //"Solicitud Informacion - Sol. Presupuesto N° " . $resp["idpresupuesto"];
            $Mailer->msgHTML($mensaje);
            $Mailer->SMTPAuth = true;
            $Mailer->Username = $username; //"emergencia@fabrimetal.cl";
            $Mailer->Password = $pass; //"1322cncn";

            $contacto = new ContactoInfo();
            $respemail = $contacto->emails($area);
            
            while($reg = $respemail->fetch_object()){
                //var_dump($reg);
                $Mailer->addAddress($reg->correo, $reg->nombre);
            }
            
            //DIRECCION DE PRUEBA
            //$Mailer->addAddress("omaldonado@fabrimetal.cl",'');
            
            if (!$Mailer->send()) {
                echo "Error al enviar correo: " . $Mailer->ErrorInfo . "<br>";
            } else {
                echo "Correo enviado exitosamente<br>";
            }  
        }
        
}