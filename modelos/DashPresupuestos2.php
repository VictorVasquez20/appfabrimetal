<?php

require "../config/conexion.php";

Class Dash {

    //Constructor para instancias
    public function __construct() {
        
    }

    public function presupuestos() {
        $sql = "SELECT COUNT(idpresupuesto) AS total FROM presupuesto WHERE condicion = 1";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function proceso() {
        $sql = "SELECT COUNT(idpresupuesto) AS proceso FROM presupuesto WHERE condicion=1 and estado in (1,2,3,4)";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function enviados() {
        $sql = "SELECT COUNT(idpresupuesto) AS enviados FROM presupuesto WHERE condicion=1 and estado = 5";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function aceptados() {
        $sql = "SELECT COUNT(idpresupuesto) AS aceptados FROM presupuesto WHERE condicion = 1 and estado = 6";
        return ejecutarConsultaSimpleFila($sql);
    }

     public function solicitados() {
        $sql = "SELECT COUNT(idpresupuesto) AS solicitados FROM presupuesto WHERE condicion = 1 and estado = 1";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function informacion() {
        $sql = "SELECT COUNT(idpresupuesto) AS informacion FROM presupuesto WHERE condicion = 1 and estado = 2";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function informados() {
        $sql = "SELECT COUNT(idpresupuesto) AS informados FROM presupuesto WHERE condicion = 1 and estado = 3";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function procesados() {
        $sql = "SELECT COUNT(idpresupuesto) AS total FROM presupuesto WHERE condicion = 1 and estado = 4";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function reparaciones() {
        $sql = "SELECT COUNT(idreparacion) AS total FROM reparacion WHERE estado in (1,2,3,7)";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function planificadas() {
        $sql = "SELECT COUNT(idreparacion) AS planificadas FROM reparacion WHERE estado = 4";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function finalizadas() {
        $sql = "SELECT COUNT(idreparacion) AS finalizadas FROM reparacion WHERE estado = 5";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function facturacion() {
        $sql = "SELECT COUNT(idreparacion) AS facturacion FROM reparacion WHERE estado = 6";
        return ejecutarConsultaSimpleFila($sql);
    }
    
     public function ascensoresRecuperadosKone() {
        $sql = "SELECT COUNT(idascensor) AS cantidad FROM ascensor WHERE condicion = 1 and idcontrato is not null and marca = 1 and idventa is null";
        return ejecutarConsultaSimpleFila($sql);
    }
    
     public function ascensoresNuevosKone() {
        $sql = "SELECT COUNT(idascensor) AS cantidad FROM ascensor WHERE condicion = 1 and idcontrato is not null and marca = 1 and idventa is not null";
        return ejecutarConsultaSimpleFila($sql);
    }
    
    public function contarcontratos() {
        $sql = "SELECT MONTH(fecha) AS mes, COUNT(idcontrato) AS contratos FROM contrato WHERE YEAR(fecha)=YEAR(NOW()) GROUP BY MONTH(fecha)";
        return ejecutarConsulta($sql);
    }

    public function contarcontratosant() {
        $sql = "SELECT MONTH(fecha) AS mes, COUNT(idcontrato) AS contratos FROM contrato WHERE YEAR(fecha)=YEAR(NOW())-1 GROUP BY MONTH(fecha)";
        return ejecutarConsulta($sql);
    }

    public function graficoAscfecha($mes_busqueda, $anio_busqueda) {
        
        if ($mes_busqueda == 0) { $mes_busqueda = 12;}

        $sql = "Select IFNULL((SELECT count(a.idascensor) as 'ascensoresOUT' FROM `ascensor` a INNER JOIN contrato c ON c.idcontrato = a.idcontrato WHERE a.created_time <= '$anio_busqueda-$mes_busqueda-28' and c.idregiones = reg.region_id group by c.idregiones),0) 'out',"
                ."IFNULL((SELECT count(a.idascensor) as 'ascensoresINT' FROM `ascensor` a INNER JOIN contrato c ON c.idcontrato = a.idcontrato WHERE c.fecha <= '$anio_busqueda-$mes_busqueda-28' and c.idregiones = reg.region_id group by c.idregiones),0) 'in' ,"
                ."reg.region_numero "
                ."FROM regiones reg "
                . "order by reg.region_numero";
 
        return ejecutarConsulta($sql);
    }

    public function SelectAno() {
        $sql = "SELECT DISTINCT YEAR(fecha) 'ano' FROM `contrato` ORDER BY YEAR(fecha) DESC";
        return ejecutarConsulta($sql);
    }
    
    public function IngresoPresupuestosPorMes($anio_busq){
        
        $sql = "SELECT YEAR(created_time) as anio, MONTH(created_time) as mes ,count(idpresupuesto) as cantidad
                FROM presupuesto
                WHERE YEAR(created_time) = $anio_busq
                AND condicion = 1
                GROUP BY YEAR(created_time),MONTH(created_time)
                ORDER BY YEAR(created_time),MONTH(created_time)";
        return ejecutarConsulta($sql);
    }
    
     public function SalidaPresupuestosPorMes($anio_busq){
         
        $sql = "SELECT YEAR(aproved_time) as anio, MONTH(aproved_time) as mes ,count(idpresupuesto) as cantidad
                FROM presupuesto
                WHERE YEAR(aproved_time) = $anio_busq
                AND estado = 6
                GROUP BY YEAR(aproved_time),MONTH(aproved_time)
                ORDER BY YEAR(aproved_time),MONTH(aproved_time)";
        return ejecutarConsulta($sql);
    }

    public function IngresoReparacionesPorMes($anio_busq){
        
        $sql = "SELECT YEAR(created_time) as anio, MONTH(created_time) as mes ,count(idreparacion) as cantidad
                FROM reparacion
                WHERE YEAR(created_time) = $anio_busq
                GROUP BY YEAR(created_time),MONTH(created_time)
                ORDER BY YEAR(created_time),MONTH(created_time)";
        return ejecutarConsulta($sql);
    }
    
     public function SalidaReparacionesPorMes($anio_busq){
         
        $sql = "SELECT YEAR(update_time) as anio, MONTH(update_time) as mes ,count(idreparacion) as cantidad
                FROM reparacion
                WHERE YEAR(update_time) = $anio_busq
                AND estado in (5,6)
                GROUP BY YEAR(update_time),MONTH(update_time)
                ORDER BY YEAR(update_time),MONTH(update_time)";
        return ejecutarConsulta($sql);
    }
    
}

?>