<?php

require "../config/conexion.php";

Class Servicio {

    //Constructor para instancias
    public function __construct() {
        
    }

    public function pdf($idservicio) {
        $sql = "SELECT s.idservicio, now() AS actual, u.nombre AS esini, s.observacionini, i.nombre AS esfin, s.observacionfin, s.nombre AS nomvali, s.apellidos AS apevali, s.rut AS rutvali, s.firma, s.created_time AS ini, s.closed_time AS fin, TIMESTAMPDIFF(MINUTE, s.created_time, s.closed_time) AS duracion, s.reqfirma, a.codigo, a.codigocli, a.ubicacion, q.nombre AS tascen, m.nombre AS marca, n.nombre AS modelo, t.nombre AS tser, e.nombre AS edi, e.calle, e.numero, r.region_nombre AS region, c.comuna_nombre AS comuna, w.nombre AS segmen, p.nombre AS nomtec, p.apellidos AS apetec, p.rut AS ruttec, o.nombre AS cartec, s.idtservicio AS idtserv, s.nrodocumento AS nrodoc, a.paradas, a.capper, a.capkg FROM servicio s INNER JOIN ascensor a ON s.idascensor = a.idascensor INNER JOIN edificio e ON a.idedificio = e.idedificio INNER JOIN tascensor q ON a.idtascensor = q.idtascensor INNER JOIN marca m ON a.marca=m.idmarca INNER JOIN modelo n ON a.modelo = n.idmodelo INNER JOIN tsegmento w ON e.idtsegmento = w.idtsegmento INNER JOIN regiones r ON e.idregiones = r.region_id INNER JOIN comunas c ON e.idcomunas = c.comuna_id INNER JOIN tservicio t ON s.idtservicio = t.idtservicio INNER JOIN testado u ON s.estadoini = u.id INNER JOIN testado i ON s.estadofin = i.id INNER JOIN tecnico p ON s.idtecnico = p.idtecnico INNER JOIN cargotec o ON p.idcargotec = o.idcargotec WHERE s.idservicio = '$idservicio'";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function listar_guias() {
        $sql = "(SELECT s.idservicio, y.nombre AS tipo, a.codigo, e.nombre, ELT(DATE_FORMAT(s.created_time,'%m'),'ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE') as mes, DATE(s.created_time) AS fecha, TIME(s.created_time) AS inicio, TIME(s.closed_time) AS fin, s.reqfirma, s.created_time FROM servicio s INNER JOIN ascensor a ON s.idascensor = a.idascensor INNER JOIN edificio e ON a.idedificio = e.idedificio  INNER JOIN tservicio y ON s.idtservicio = y.idtservicio WHERE s.closed_time IS NOT NULL AND firma IS NOT NULL ORDER BY s.created_time DESC) UNION (SELECT s.idservicio, y.nombre AS tipo, a.codigo, e.nombre, ELT(DATE_FORMAT(s.created_time,'%m'),'ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE') as mes, DATE(s.created_time) AS fecha, TIME(s.created_time) AS inicio, TIME(s.closed_time) AS fin, s.reqfirma, s.created_time FROM servicio s INNER JOIN ascensor a ON s.idascensor = a.idascensor INNER JOIN edificio e ON a.idedificio = e.idedificio  INNER JOIN tservicio y ON s.idtservicio = y.idtservicio WHERE s.closed_time IS NOT NULL AND s.reqfirma = 0)  
ORDER BY `created_time` DESC";
        return ejecutarConsulta($sql);
    }

    public function listar_guiasmes() {
        $sql = "(SELECT s.idservicio, y.nombre AS tipo, a.codigo, e.nombre, DATE(s.created_time) AS fecha, s.reqfirma, s.created_time FROM servicio s INNER JOIN ascensor a ON s.idascensor = a.idascensor INNER JOIN edificio e ON a.idedificio = e.idedificio INNER JOIN tservicio y ON s.idtservicio = y.idtservicio WHERE MONTH(s.created_time) = MONTH(CURRENT_TIMESTAMP) AND s.closed_time IS NOT NULL AND firma IS NOT NULL ORDER BY s.created_time DESC) UNION (SELECT s.idservicio, y.nombre AS tipo, a.codigo, e.nombre, DATE(s.created_time) AS fecha, s.reqfirma, s.created_time FROM servicio s INNER JOIN ascensor a ON s.idascensor = a.idascensor INNER JOIN edificio e ON a.idedificio = e.idedificio INNER JOIN tservicio y ON s.idtservicio = y.idtservicio WHERE MONTH(s.created_time) = MONTH(CURRENT_TIMESTAMP) AND s.closed_time IS NOT NULL AND s.reqfirma = 0)  
ORDER BY `created_time` DESC";
        return ejecutarConsulta($sql);
    }

    public function listar_emergencia() {
        $sql = "SELECT s.idservicio, y.nombre AS tipo, a.codigo, e.nombre, ELT(DATE_FORMAT(s.created_time,'%m'),'ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE') as mes, DATE(s.created_time) AS fecha, TIME(s.created_time) AS inicio, TIME(s.closed_time) AS fin, r.nombre AS nomtec, r.apellidos AS apetec, s.nrodocumento AS nrodoc FROM servicio s INNER JOIN ascensor a ON s.idascensor = a.idascensor INNER JOIN edificio e ON a.idedificio = e.idedificio INNER JOIN tservicio y ON s.idtservicio = y.idtservicio INNER JOIN tecnico r ON s.idtecnico=r.idtecnico WHERE s.idtservicio=2 ORDER BY s.created_time DESC";
        return ejecutarConsulta($sql);
    }

    public function listar_dashemergencia() {
        $sql = "SELECT s.idservicio, a.codigo, e.nombre, DATE(s.created_time) AS fecha, TIME(s.created_time) AS inicio, o.nombre AS esini, TIME(s.closed_time) AS fin, p.nombre AS esfin, r.nombre AS nomtec, r.apellidos AS apetec FROM servicio s INNER JOIN ascensor a ON s.idascensor = a.idascensor INNER JOIN edificio e ON a.idedificio = e.idedificio INNER JOIN tservicio y ON s.idtservicio = y.idtservicio INNER JOIN tecnico r ON s.idtecnico=r.idtecnico LEFT JOIN testado o ON s.estadoini=o.id LEFT JOIN testado p ON s.estadofin=p.id WHERE s.idtservicio=2 AND DATE(s.created_time)=DATE(NOW()) ORDER BY s.created_time DESC";
        return ejecutarConsulta($sql);
    }

    public function listar_pfirma() {
        //$sql = "SELECT s.idservicio, y.nombre AS tipo, a.codigo, e.nombre, ELT(DATE_FORMAT(s.created_time,'%m'),'ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE') as mes, DATE(s.created_time) AS fecha, TIME(s.created_time) AS inicio, TIME(s.closed_time) AS fin, r.nombre AS nomtec, r.apellidos AS apetec, p.nombre AS nomsup, p.apellido AS apesup FROM servicio s INNER JOIN ascensor a ON s.idascensor = a.idascensor INNER JOIN edificio e ON a.idedificio = e.idedificio INNER JOIN tservicio y ON s.idtservicio = y.idtservicio INNER JOIN tecnico r ON s.idtecnico=r.idtecnico LEFT JOIN supervisor p ON r.idsupervisor = p.idsupervisor WHERE s.firma IS null AND s.reqfirma = 1 AND s.closed_time IS NOT null ORDER BY s.created_time DESC";
        $sql = "SELECT s.idservicio, y.nombre AS tipo, a.codigo, e.nombre, "
            . "ELT(DATE_FORMAT(s.created_time,'%m'),'ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE') as mes, "
            . "DATE(s.created_time) AS fecha, TIME(s.created_time) AS inicio, TIME(s.closed_time) AS fin, r.nombre AS nomtec, r.apellidos AS apetec, p.nombre AS nomsup, p.apellido AS apesup "
            . "FROM servicio s "
            . "INNER JOIN ascensor a ON s.idascensor = a.idascensor "
            . "INNER JOIN edificio e ON a.idedificio = e.idedificio "
            . "INNER JOIN tservicio y ON s.idtservicio = y.idtservicio "
            . "INNER JOIN tecnico r ON s.idtecnico=r.idtecnico "
            . "LEFT JOIN supervisor p ON r.idsupervisor = p.idsupervisor "
            . "WHERE s.firma IS null AND s.reqfirma = 1 AND s.closed_time IS NOT null and MONTH(s.created_time) >= MONTH(curdate()) - 1 and YEAR(s.created_time) = YEAR(curdate()) "
            . "ORDER BY s.created_time DESC";
        return ejecutarConsulta($sql);
    }

    public function LGSESup($rut) {
        $sql = "SELECT s.idservicio, y.nombre AS tipo, a.codigo, e.nombre, DATE_FORMAT(s.created_time, '%d-%m-%Y') as fecha, TIME(s.created_time) AS inicio,CONCAT(r.nombre,' ', r.apellidos) AS tecnico, IF(s.reqfirma=0,'NO REQUIERE',IF(s.firma IS NOT NULL,'FIRMADA','POR FIRMAR')) AS estado FROM servicio s INNER JOIN ascensor a ON s.idascensor = a.idascensor INNER JOIN edificio e ON a.idedificio = e.idedificio INNER JOIN tservicio y ON s.idtservicio = y.idtservicio INNER JOIN tecnico r ON s.idtecnico=r.idtecnico LEFT JOIN supervisor p ON r.idsupervisor = p.idsupervisor WHERE s.idtecnico IN (SELECT t.idtecnico FROM tecnico t INNER JOIN supervisor sup ON t.idsupervisor=sup.idsupervisor WHERE sup.rut='$rut') AND MONTH(s.created_time)=MONTH(now()) AND s.closed_time IS NOT null ORDER BY s.created_time DESC";
        return ejecutarConsulta($sql);
    }
    
    public function ListarGuiasEdificio($idedificio) {
        $sql = "SELECT s.idservicio, y.nombre AS tipo, a.codigo, e.nombre, DATE_FORMAT(s.created_time, '%d-%m-%Y') as fecha, CONCAT(r.nombre,' ', r.apellidos) AS tecnico, IF(s.reqfirma=0,'NO REQUIERE',IF(s.firma IS NOT NULL,'FIRMADA','POR FIRMAR')) AS estado FROM servicio s INNER JOIN ascensor a ON s.idascensor = a.idascensor INNER JOIN edificio e ON a.idedificio = e.idedificio INNER JOIN tservicio y ON s.idtservicio = y.idtservicio INNER JOIN tecnico r ON s.idtecnico=r.idtecnico LEFT JOIN supervisor p ON r.idsupervisor = p.idsupervisor WHERE e.idedificio = '$idedificio' AND MONTH(s.created_time) = MONTH(NOW()) AND s.closed_time IS NOT null ORDER BY s.created_time DESC";
        return ejecutarConsulta($sql);
    }
    
    public function LGSESupervisor($rut, $tservicio, $ano, $mes, $tecnico, $firma) {
        $sql = "SELECT s.idservicio, y.nombre AS tipo, a.codigo, e.nombre, CONCAT(r.nombre,' ', r.apellidos) AS tecnico, IF(s.reqfirma=0,'NO REQUIERE',IF(s.firma IS NOT NULL,'FIRMADA','POR FIRMAR')) AS estado, DATE(s.created_time) as 'fecini', TIME(s.created_time) AS inicio,  DATE(s.closed_time) as 'fecfin', TIME(s.closed_time) AS fin "
             . ",TIMESTAMPDIFF(MINUTE, s.created_time, s.closed_time) as 'tservicio',IF(TIME(s.created_time) <= TIME('18:00:00')    ,(IF(TIME(s.closed_time) >= TIME('18:00:00'), TIMESTAMPDIFF(MINUTE, TIME(s.created_time), TIME('18:00:00')), TIMESTAMPDIFF(MINUTE, s.created_time, s.closed_time))) ,0) as 'horasnormal',IF(TIME(s.created_time) >= TIME('18:00:00'), TIMESTAMPDIFF(MINUTE, s.created_time, s.closed_time), (IF(TIME(s.closed_time) >= TIME('18:00:00'), TIMESTAMPDIFF(MINUTE, TIME('18:00:00'), TIME(s.closed_time)), 0))) as 'horasextras',(ELT(WEEKDAY(s.created_time) + 1, 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom')) AS 'dia', IFNULL(IFNULL(cca.codigo, ccs.codigo), 'SIN CENTRO COSTO') as 'ccosto' "
             . "   FROM servicio s"
             . "   INNER JOIN ascensor a ON s.idascensor = a.idascensor "
             . "   INNER JOIN edificio e ON a.idedificio = e.idedificio "
             . "   INNER JOIN tservicio y ON s.idtservicio = y.idtservicio"
             . "   INNER JOIN tecnico r ON s.idtecnico=r.idtecnico "
             . "   LEFT JOIN supervisor p ON r.idsupervisor = p.idsupervisor "
             . "   LEFT JOIN centrocosto cca ON cca.idcentrocosto = a.idcentrocosto "
             . "   LEFT JOIN centrocosto ccs ON ccs.idcentrocosto = s.idcentrocos " 
             . "   WHERE s.idtecnico IN (SELECT t.idtecnico FROM tecnico t "
             . "                         INNER JOIN supervisor sup ON t.idsupervisor=sup.idsupervisor "
             . "                         WHERE sup.rut='$rut') "
             . "   AND s.closed_time IS NOT null ";
                
        if ($tservicio <> 0)    {$sql .= " AND s.idtservicio = $tservicio " ;}
        if ($ano <> 0)          {$sql .= " AND YEAR(s.created_time) = $ano "; }
        if ($mes <> 0)          {$sql .= " AND MONTH(s.created_time) = $mes ";}
        if ($tecnico <> 0)      {$sql .= " AND r.idtecnico = $tecnico ";}
        if ($firma == 1)        {$sql .= " AND s.firma IS NOT NULL ";} elseif ($firma == 2){ $sql .= " AND s.firma IS NULL AND s.reqfirma=1";} elseif ($firma == 3){ $sql .= " AND s.reqfirma=0 ";}
        
        $sql .= " ORDER BY s.created_time DESC";
        return ejecutarConsulta($sql);
    }

    public function listar_psup($rut) {
        $sql = "SELECT s.idservicio, y.nombre AS tipo, a.codigo, e.nombre, DATE_FORMAT(s.created_time, '%d-%m-%Y') as fecha, CONCAT(r.nombre,' ', r.apellidos) AS tecnico FROM servicio s INNER JOIN ascensor a ON s.idascensor = a.idascensor INNER JOIN edificio e ON a.idedificio = e.idedificio INNER JOIN tservicio y ON s.idtservicio = y.idtservicio INNER JOIN tecnico r ON s.idtecnico=r.idtecnico LEFT JOIN supervisor p ON r.idsupervisor = p.idsupervisor WHERE s.idtecnico IN (SELECT t.idtecnico FROM tecnico t INNER JOIN supervisor sup ON t.idsupervisor=sup.idsupervisor WHERE sup.rut='$rut') AND MONTH(s.created_time)=MONTH(now()) AND s.firma IS null AND s.reqfirma = 1 AND s.closed_time IS NOT null ORDER BY s.created_time DESC";
        return ejecutarConsulta($sql);
    }

    public function terminados() {
        $sql = "SELECT COUNT(idservicio) AS guias FROM servicio WHERE estadofin IS NOT NULL AND DATE(created_time) = DATE(now())";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function proceso() {
        $sql = "SELECT COUNT(idservicio) AS guias FROM servicio WHERE estadoini IS NOT NULL AND estadofin IS NULL AND DATE(created_time) = DATE(now())";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function penfirma() {
        $sql = "SELECT COUNT(idservicio) AS guias FROM servicio WHERE estadofin IS NOT NULL AND firma IS NULL AND reqfirma=1 AND MONTH(created_time) = MONTH(curdate()) AND YEAR(created_time) = YEAR(curdate())";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function mesguias() {
        $sql = "SELECT MONTH(created_time) AS mes, COUNT(idservicio) AS servicios FROM servicio WHERE YEAR(created_time)=YEAR(now()) GROUP BY MONTH(created_time)";
        return ejecutarConsulta($sql);
    }

    public function mesmantencion() {
        $sql = "SELECT MONTH(created_time) AS mes, COUNT(idservicio) AS servicios FROM servicio WHERE YEAR(created_time)=YEAR(now()) AND idtservicio = 3 GROUP BY MONTH(created_time)";
        return ejecutarConsulta($sql);
    }

    public function Grafico() {
        $sql = "SELECT * from "
                . "(SELECT count(*) as guias, MONTH(created_time) as mes, 'CM' as tipo  FROM `servicio` Where estadofin is not null and idtservicio = 3 AND YEAR(created_time) = YEAR(curdate()) and closed_time is not null GROUP BY MONTH(created_time) "
                . "UNION "
                . "SELECT count(*) as guiaenproceso, MONTH(created_time) as mes, 'PR' FROM `servicio` Where estadofin is null and idtservicio = 3 AND YEAR(created_time) = YEAR(curdate()) GROUP by MONTH(created_time) "
                . "UNION "
                . "SELECT count(*) as guiacerrada, MONTH(created_time) as mes, 'CSF' FROM `servicio` Where reqfirma = 1 and firma is null and idtservicio = 3 AND YEAR(created_time) = YEAR(curdate()) GROUP by MONTH(created_time)) as tabla "
                . "order by mes, tipo";
        return ejecutarConsulta($sql);
    }

    function selectano() {
        $sql = "SELECT DISTINCT YEAR(created_time) AS 'ano' FROM `servicio`";
        return ejecutarConsulta($sql);
    }

    public function listar_guias_filtro($tservicio, $ano, $mes, $supervisor, $tecnico) {
        $sw = true;
        $sql = "SELECT (select enc_id from informevisita where infv_id = tabla.nrodocumento and infv_servicio = tabla.idservicio) AS idencuesta, tabla.* FROM (SELECT a.idascensor, s.idservicio, s.idtservicio, s.nrodocumento, y.nombre AS tipo, a.codigo, e.nombre, ELT(DATE_FORMAT(s.created_time,'%m'),'ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE') as mes, "
               . " MONTH(s.created_time) as intmes, YEAR(s.created_time) as ano, DATE(s.created_time) AS fecha, DATE(s.created_time) as 'fecini', TIME(s.created_time) AS inicio,  DATE(s.closed_time) as 'fecfin', TIME(s.closed_time) AS fin, s.reqfirma, "
               . " s.created_time, t.idtecnico, sup.idsupervisor, s.facturacion, s.infodev, ts.nombre as 'estadofin', s.firma, IFNULL(IFNULL(cca.codigo, ccs.codigo), 'SIN CENTRO COSTO') as 'ccosto' "
               . " FROM servicio s  "
               . " INNER JOIN ascensor a ON s.idascensor = a.idascensor "
               . " INNER JOIN edificio e ON a.idedificio = e.idedificio "
               . " INNER JOIN tservicio y ON s.idtservicio = y.idtservicio "
               . " INNER JOIN tecnico t ON t.idtecnico = s.idtecnico "
               . " INNER JOIN supervisor sup ON sup.idsupervisor = t.idsupervisor "
               . " INNER JOIN testado ts ON ts.id = s.estadofin "
               . " LEFT JOIN centrocosto cca ON cca.idcentrocosto = a.idcentrocosto "
               . " LEFT JOIN centrocosto ccs ON ccs.idcentrocosto = s.idcentrocos "
               . " WHERE s.closed_time IS NOT NULL AND firma IS NOT NULL "
               . " UNION "
               . " (SELECT a.idascensor, s.idservicio, s.idtservicio, s.nrodocumento, y.nombre AS tipo, a.codigo, e.nombre,  "
               . " ELT(DATE_FORMAT(s.created_time,'%m'),'ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE') as mes, "
               . " MONTH(s.created_time) as intmes, YEAR(s.created_time) as ano, DATE(s.created_time) AS fecha, DATE(s.created_time) as 'fecini', TIME(s.created_time) AS inicio,  DATE(s.closed_time) as 'fecfin', TIME(s.closed_time) AS fin, s.reqfirma, "
               . " s.created_time, t.idtecnico, sup.idsupervisor, s.facturacion, s.infodev, ts.nombre as 'estadofin', s.firma , IFNULL(IFNULL(cca.codigo, ccs.codigo), 'SIN CENTRO COSTO') as 'ccosto' "
               . " FROM servicio s "
               . " INNER JOIN ascensor a ON s.idascensor = a.idascensor "
               . " INNER JOIN edificio e ON a.idedificio = e.idedificio "
               . " INNER JOIN tservicio y ON s.idtservicio = y.idtservicio "
               . " INNER JOIN tecnico t ON t.idtecnico = s.idtecnico "
               . " INNER JOIN testado ts ON ts.id = s.estadofin "
               . " INNER JOIN supervisor sup ON sup.idsupervisor = t.idsupervisor "
               . " LEFT JOIN centrocosto cca ON cca.idcentrocosto = a.idcentrocosto "
               . " LEFT JOIN centrocosto ccs ON ccs.idcentrocosto = s.idcentrocos "
               . " WHERE s.closed_time IS NOT NULL AND s.reqfirma = 0)  ORDER BY `created_time` DESC) as tabla "
               . " WHERE facturacion = 0 ";
        
        if ($tservicio <> 0)    {$sql .= " AND idtservicio = $tservicio ";$sw = true;}
        if ($ano <> 0)          {if($sw){$sql .= " and ano = $ano "; }else{$sql .= " WHERE ano = $ano "; $sw = true;}}
        if ($mes <> 0)          {if($sw){$sql .= " and intmes = $mes ";}else{$sql .= " WHERE intmes = $mes ";$sw = true;}}
        if ($supervisor <> 0)   {if($sw){$sql .= " and idsupervisor = $supervisor ";}else{$sql .= " WHERE idsupervisor = $supervisor ";$sw = true;}}
        if ($tecnico <> 0)      {if($sw){$sql .= " and idtecnico = $tecnico ";}else{$sql .= " WHERE idtecnico = $tecnico ";$sw = true;}
        }
        
        $sql .= " ORDER BY `created_time` DESC";
        
        
        return ejecutarConsulta($sql);
    }
    
    public function listar_guias_filtro2($tservicio, $ano, $mes, $supervisor, $tecnico,$edificio,$equipo) {
        $sw = false;
                
        /*$sql = "SELECT * FROM (SELECT s.idservicio, s.idtservicio, s.nrodocumento, y.nombre AS tipo, a.codigo, e.nombre, ELT(DATE_FORMAT(s.created_time,'%m'),'ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE') as mes, "
               . " MONTH(s.created_time) as intmes, YEAR(s.created_time) as ano, DATE(s.created_time) AS fecha, DATE(s.created_time) as 'fecini', TIME(s.created_time) AS inicio,  DATE(s.closed_time) as 'fecfin', TIME(s.closed_time) AS fin, s.reqfirma, "
               . " s.created_time, t.idtecnico, sup.idsupervisor, s.facturacion, s.infodev, ts.nombre as 'estadofin', s.firma, IFNULL(IFNULL(cca.codigo, ccs.codigo), 'SIN CENTRO COSTO') as 'ccosto' "
               . ",TIMESTAMPDIFF(MINUTE, s.created_time, s.closed_time) as 'tservicio',IF(TIME(s.created_time) <= TIME('18:00:00') ,(IF(TIME(s.closed_time) >= TIME('18:00:00'), TIMESTAMPDIFF(MINUTE, TIME(s.created_time), TIME('18:00:00')), TIMESTAMPDIFF(MINUTE, s.created_time, s.closed_time))) ,0) as 'horasnormal',IF(TIME(s.created_time) >= TIME('18:00:00'), TIMESTAMPDIFF(MINUTE, s.created_time, s.closed_time), (IF(TIME(s.closed_time) >= TIME('18:00:00'), TIMESTAMPDIFF(MINUTE, TIME('18:00:00'), TIME(s.closed_time)), 0))) as 'horasextras',(ELT(WEEKDAY(s.created_time) + 1, 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom')) AS 'dia' "
               . " FROM servicio s  "
               . " INNER JOIN ascensor a ON s.idascensor = a.idascensor "
               . " INNER JOIN edificio e ON a.idedificio = e.idedificio "
               . " INNER JOIN tservicio y ON s.idtservicio = y.idtservicio "
               . " INNER JOIN tecnico t ON t.idtecnico = s.idtecnico "
               . " INNER JOIN supervisor sup ON sup.idsupervisor = t.idsupervisor "
               . " INNER JOIN testado ts ON ts.id = s.estadofin "
               . " LEFT JOIN centrocosto cca ON cca.idcentrocosto = a.idcentrocosto "
               . " LEFT JOIN centrocosto ccs ON ccs.idcentrocosto = s.idcentrocos "
               . " WHERE s.closed_time IS NOT NULL AND firma IS NOT NULL "
               . " UNION "
               . " (SELECT s.idservicio, s.idtservicio, s.nrodocumento, y.nombre AS tipo, a.codigo, e.nombre,  "
               . " ELT(DATE_FORMAT(s.created_time,'%m'),'ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE') as mes, "
               . " MONTH(s.created_time) as intmes, YEAR(s.created_time) as ano, DATE(s.created_time) AS fecha, DATE(s.created_time) as 'fecini', TIME(s.created_time) AS inicio,  DATE(s.closed_time) as 'fecfin', TIME(s.closed_time) AS fin, s.reqfirma, "
               . " s.created_time, t.idtecnico, sup.idsupervisor, s.facturacion, s.infodev, ts.nombre as 'estadofin', s.firma , IFNULL(IFNULL(cca.codigo, ccs.codigo), 'SIN CENTRO COSTO') as 'ccosto' "
               . ",TIMESTAMPDIFF(MINUTE, s.created_time, s.closed_time) as 'tservicio',IF(TIME(s.created_time) <= TIME('18:00:00'),(IF(TIME(s.closed_time) >= TIME('18:00:00'), TIMESTAMPDIFF(MINUTE, TIME(s.created_time), TIME('18:00:00')), TIMESTAMPDIFF(MINUTE, s.created_time, s.closed_time))) ,0) as 'horasnormal',IF(TIME(s.created_time) >= TIME('18:00:00'), TIMESTAMPDIFF(MINUTE, s.created_time, s.closed_time), (IF(TIME(s.closed_time) >= TIME('18:00:00'), TIMESTAMPDIFF(MINUTE, TIME('18:00:00'), TIME(s.closed_time)), 0))) as 'horasextras',(ELT(WEEKDAY(s.created_time) + 1, 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom')) AS 'dia' "
               . " FROM servicio s "
               . " INNER JOIN ascensor a ON s.idascensor = a.idascensor "
               . " INNER JOIN edificio e ON a.idedificio = e.idedificio "
               . " INNER JOIN tservicio y ON s.idtservicio = y.idtservicio "
               . " INNER JOIN tecnico t ON t.idtecnico = s.idtecnico "
               . " INNER JOIN testado ts ON ts.id = s.estadofin "
               . " INNER JOIN supervisor sup ON sup.idsupervisor = t.idsupervisor "
               . " LEFT JOIN centrocosto cca ON cca.idcentrocosto = a.idcentrocosto "
               . " LEFT JOIN centrocosto ccs ON ccs.idcentrocosto = s.idcentrocos "
               . " WHERE s.closed_time IS NOT NULL AND s.reqfirma = 0)  ORDER BY `created_time` DESC) as tabla ";*/
        $sql = "SELECT * FROM (SELECT (select enc_id from informevisita where infv_id = s.nrodocumento and infv_servicio = s.idservicio) AS idencuesta, s.idservicio, s.idtservicio, s.nrodocumento, y.nombre AS tipo, a.codigo, e.idedificio, e.nombre, ELT(DATE_FORMAT(s.created_time,'%m'),'ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE') as mes, MONTH(s.created_time) as intmes, YEAR(s.created_time) as ano, DATE(s.created_time) AS fecha, DATE(s.created_time) as 'fecini', TIME(s.created_time) AS inicio, DATE(s.closed_time) as 'fecfin', TIME(s.closed_time) AS fin, s.reqfirma, s.created_time, t.idtecnico,CONCAT(t.nombre,', ',t.apellidos) as nombre_tecnico, sup.idsupervisor,CONCAT(sup.nombre,', ',sup.apellido) as nombre_supervisor, s.facturacion, s.infodev, ts.nombre as 'estadofin', s.firma, IFNULL(IFNULL(cca.codigo, ccs.codigo), 'SIN CENTRO COSTO') as 'ccosto' ,TIMESTAMPDIFF(MINUTE, s.created_time, s.closed_time) as 'tservicio',IF(TIME(s.created_time) <= TIME('18:00:00') ,(IF(TIME(s.closed_time) >= TIME('18:00:00'), TIMESTAMPDIFF(MINUTE, TIME(s.created_time), TIME('18:00:00')), TIMESTAMPDIFF(MINUTE, s.created_time, s.closed_time))) ,0) as 'horasnormal',IF(TIME(s.created_time) >= TIME('18:00:00'), TIMESTAMPDIFF(MINUTE, s.created_time, s.closed_time), (IF(TIME(s.closed_time) >= TIME('18:00:00'), TIMESTAMPDIFF(MINUTE, TIME('18:00:00'), TIME(s.closed_time)), 0))) as 'horasextras',(ELT(WEEKDAY(s.created_time) + 1, 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom')) AS 'dia', IF(s.closed_time IS NOT NULL, 'Cerrada', 'En Proceso') as 'estado', s.closed_time, s.idascensor, s.latini, s.lonini, s.latfin, s.lonfin "
                . "FROM servicio s INNER JOIN ascensor a ON s.idascensor = a.idascensor INNER JOIN edificio e ON a.idedificio = e.idedificio INNER JOIN tservicio y ON s.idtservicio = y.idtservicio INNER JOIN tecnico t ON t.idtecnico = s.idtecnico INNER JOIN supervisor sup ON sup.idsupervisor = t.idsupervisor INNER JOIN testado ts ON ts.id = s.estadofin LEFT JOIN centrocosto cca ON cca.idcentrocosto = a.idcentrocosto LEFT JOIN centrocosto ccs ON ccs.idcentrocosto = s.idcentrocos) as tabla ";        
        if ($tservicio <> 0)    {$sql .= " WHERE idtservicio = $tservicio ";$sw = true;}
        if ($ano <> 0)          {if($sw){$sql .= " and ano = $ano "; }else{$sql .= " WHERE ano = $ano "; $sw = true;}}
        if ($mes <> 0)          {if($sw){$sql .= " and intmes = $mes ";}else{$sql .= " WHERE intmes = $mes ";$sw = true;}}
        if ($supervisor <> 0)   {if($sw){$sql .= " and idsupervisor = $supervisor ";}else{$sql .= " WHERE idsupervisor = $supervisor ";$sw = true;}}
        if ($edificio <> 0)      {if($sw){$sql .= " and idedificio = $edificio ";}else{$sql .= " WHERE idedificio = $edificio ";$sw = true;}}
        if ($equipo <> 0)      {if($sw){$sql .= " and idascensor = $equipo ";}else{$sql .= " WHERE idascensor = $equipo ";$sw = true;}}
        if ($tecnico <> 0)      {if($sw){$sql .= " and idtecnico = $tecnico ";}else{$sql .= " WHERE idtecnico = $tecnico ";$sw = true;}
        }
        
        $sql .= " ORDER BY `created_time` DESC";
        
        //echo $sql;die;
        return ejecutarConsulta($sql);
    }

    public function listar_guias_descarga($tservicio,$supervisor,$edificio,$equipo,$desde,$hasta) {
        $sw = false;
        $sql = "SELECT * FROM (SELECT (select enc_id from informevisita where infv_id = s.nrodocumento and infv_servicio = s.idservicio) AS idencuesta, s.idservicio, s.idtservicio, s.nrodocumento, y.nombre AS tipo, a.codigo, e.idedificio, e.nombre, ELT(DATE_FORMAT(s.created_time,'%m'),'ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE') as mes, MONTH(s.created_time) as intmes, YEAR(s.created_time) as ano, DATE(s.created_time) AS fecha, DATE(s.created_time) as 'fecini', TIME(s.created_time) AS inicio, DATE(s.closed_time) as 'fecfin', TIME(s.closed_time) AS fin, s.reqfirma, s.created_time, t.idtecnico,CONCAT(t.nombre,', ',t.apellidos) as nombre_tecnico, sup.idsupervisor,CONCAT(sup.nombre,', ',sup.apellido) as nombre_supervisor, s.facturacion, s.infodev, ts.nombre as 'estadofin', s.firma, IFNULL(IFNULL(cca.codigo, ccs.codigo), 'SIN CENTRO COSTO') as 'ccosto' ,TIMESTAMPDIFF(MINUTE, s.created_time, s.closed_time) as 'tservicio',IF(TIME(s.created_time) <= TIME('18:00:00') ,(IF(TIME(s.closed_time) >= TIME('18:00:00'), TIMESTAMPDIFF(MINUTE, TIME(s.created_time), TIME('18:00:00')), TIMESTAMPDIFF(MINUTE, s.created_time, s.closed_time))) ,0) as 'horasnormal',IF(TIME(s.created_time) >= TIME('18:00:00'), TIMESTAMPDIFF(MINUTE, s.created_time, s.closed_time), (IF(TIME(s.closed_time) >= TIME('18:00:00'), TIMESTAMPDIFF(MINUTE, TIME('18:00:00'), TIME(s.closed_time)), 0))) as 'horasextras',(ELT(WEEKDAY(s.created_time) + 1, 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom')) AS 'dia', IF(s.closed_time IS NOT NULL, 'Cerrada', 'En Proceso') as 'estado', s.closed_time, s.idascensor, s.latini, s.lonini, s.latfin, s.lonfin "
                . "FROM servicio s INNER JOIN ascensor a ON s.idascensor = a.idascensor INNER JOIN edificio e ON a.idedificio = e.idedificio INNER JOIN tservicio y ON s.idtservicio = y.idtservicio INNER JOIN tecnico t ON t.idtecnico = s.idtecnico INNER JOIN supervisor sup ON sup.idsupervisor = t.idsupervisor INNER JOIN testado ts ON ts.id = s.estadofin LEFT JOIN centrocosto cca ON cca.idcentrocosto = a.idcentrocosto LEFT JOIN centrocosto ccs ON ccs.idcentrocosto = s.idcentrocos) as tabla ";
        if ($tservicio <> 0)    {$sql .= " WHERE idtservicio = $tservicio ";$sw = true;}        
        if ($supervisor <> 0)   {if($sw){$sql .= " and idsupervisor = $supervisor ";}else{$sql .= " WHERE idsupervisor = $supervisor ";$sw = true;}}
        if ($edificio <> 0)      {if($sw){$sql .= " and idedificio = $edificio ";}else{$sql .= " WHERE idedificio = $edificio ";$sw = true;}}
        if ($equipo <> 0)      {if($sw){$sql .= " and idascensor = $equipo ";}else{$sql .= " WHERE idascensor = $equipo ";$sw = true;}}
        if ($desde <> 0 && $hasta <> 0) {if($sw){$sql .= " and (created_time BETWEEN '".$desde."' AND '".$hasta."')";}else{$sql .= " WHERE (created_time BETWEEN '".$desde."' AND '".$hasta."') ";$sw = true;}}
        
        $sql .= " ORDER BY `created_time` DESC";
        
        //echo $sql;die;
        return ejecutarConsulta($sql);
    }

    public function Mostrar($idservicio){
        $sql = "SELECT `idservicio`, `idtservicio`, `iduser`, `idtecnico`, `idascensor`, `estadoini`, `observacionini`, `estadofin`, `observacionfin`, `nrodocumento`, `nombre`, `apellidos`, `rut`, `firma`, `reqfirma`, `estado`, "
            . "DATE_FORMAT(`created_time`, '%d-%m-%Y %H:%i:%s') as created_time, DATE_FORMAT(`closed_time`,'%d-%m-%Y %H:%i:%s') as closed_time, `latini`, `lonini`, `latfin`, `lonfin`, `file`, `filefir`, infopro, infodev, idcentrocos FROM `servicio` WHERE idservicio = $idservicio ";
        return ejecutarConsultaSimpleFila($sql);
    }
    
    public function Editar($idservicio, $idtservicio, $idtecnico, $idascensor, $estadofin, $observacionfin, $reqfirma, $created_time, $closed_time, $estadoini,$observacionini ){
        $sql = "UPDATE `servicio` "
                . "SET `idtservicio`= $idtservicio,"
                . "`idtecnico`= $idtecnico,"
                . "`idascensor`= $idascensor,"
                . "`estadofin`= $estadofin,"
                . "`observacionfin`= '$observacionfin', "
                . "`reqfirma` = $reqfirma,"
                . "`created_time` = '".date('Y-m-d H:i:s', strtotime($created_time))."',"
                . "`closed_time` =  '".date('Y-m-d H:i:s', strtotime($closed_time))."',"
                . "`estadoini`= $estadoini,"
                . "`observacionini` = '$observacionini'"
                . "WHERE `idservicio`= $idservicio ";
        return ejecutarConsulta($sql);
    }
    
    public function setFacturacion($idservicio, $facturacion, $idcentrocos, $infopro){
        $sql = "UPDATE `servicio` "
                . "SET facturacion = $facturacion , "
                . " idcentrocos = $idcentrocos, "
                . " infopro = '$infopro' "
                . " WHERE `idservicio`= $idservicio ";
        return ejecutarConsulta($sql);
    }

    function selectequipo() {
        $sql = "SELECT s.idascensor AS 'id', a.codigo AS 'codigo' FROM `servicio` s INNER JOIN ascensor a ON s.idascensor = a.idascensor GROUP BY s.idascensor";
        return ejecutarConsulta($sql);
    }

    function selectequipoxedificio($idedificio) {
        $sql = "SELECT s.idascensor AS 'id', a.codigo AS 'codigo' FROM `servicio` s INNER JOIN ascensor a ON s.idascensor = a.idascensor WHERE a.idedificio = $idedificio GROUP BY s.idascensor";
        return ejecutarConsulta($sql);
    }
}