<?php
require_once '../config/conexion.php';

/**
 * Description of FacturaServicio
 *
 * @author azuni
 */
class FacturaServicio {
    //put your code here
    function __construct() {
        
    }
    
    function Insrtar($nrofactura, $fecha, $monto, $created_user){
        $sql = "INSERT INTO `factura_servicio`(`nrofactura`, `fecha`, `monto`, `created_user`) "
                . "VALUES ($nrofactura, '$fecha',  $monto, $created_user)";
        return ejecutarConsulta_retornarID($sql);
    }
    
    function Editar($idfactura, $nrofactura, $estado, $closed_user, $condicion){
        $sql = "UPDATE `factura_servicio` "
                . "SET estado = $estado, "
                . " nrofactura = '$nrofactura', "
                . " closed_time = CURRENT_TIMESTAMP, "
                . " closed_user = $closed_user, "
                . "`condicion`= $condicion "
                . " WHERE `idfactura`= $idfactura";
        return ejecutarConsulta($sql);
    }
    
    function Mostrar($idfactura){
        $sql = "SELECT * FROM `factura_servicio` WHERE `idfactura`= $idfactura";
        return ejecutarConsultaSimpleFila($sql);
    }
    
    function Listar($ano, $mes, $estado){
        $sw = false;
        $sql = "SELECT f.* , concat(u.nombre, ' ', u.apellido) as 'nombre' "
             . "FROM `factura_servicio` f "
             . "INNER JOIN user u ON u.iduser = f.created_user ";
        
        if($ano <> 0){
            $sql.= " WHERE YEAR(f.fecha) = $ano ";
            $sw = true;
        }
        if($mes <> 0){
            if($sw){
                $sql.= " AND MONTH(f.fecha) = $mes ";
            }else{
                $sql.= " WHERE MONTH(f.fecha) = $mes ";
                $sw = true;
            }
        }
        if($estado <> 'N'){
            if($sw){
                $sql.= " AND f.estado = $estado ";
            }else{
                $sql.= " WHERE f.estado = $estado ";
            }
        }
        return ejecutarConsulta($sql);
    }
    
    /**
     * Obtiene los años de todas las facturas generadas para luego mostrarlos en los filtros
     * @return type
     */
    function selectano(){
        $sql = "SELECT distinct YEAR(`fecha`) as ano FROM `factura_servicio`";
        return ejecutarConsulta($sql);
    }
    
    /**
     * Lista las Guias que fueron pagadfas con una factura en especifico
     * @param int $idfactura
     * @return type
     */
    function ListarGSExFactura($idfactura){
        $sql = "SELECT * FROM ((SELECT s.facturacion, s.idfactura, s.idservicio, s.idtservicio, y.nombre AS tipostr, a.codigo, e.nombre, cl.razon_social, cl.rut, ELT(DATE_FORMAT(s.created_time,'%m'),'ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE') as strmes, MONTH(s.created_time) as mes, YEAR(s.created_time) as ano, DATE(s.created_time) AS fecha, TIME(s.created_time) AS inicio, TIME(s.closed_time) AS fin, s.reqfirma, s.created_time FROM servicio s INNER JOIN ascensor a ON s.idascensor = a.idascensor INNER JOIN contrato c ON a.idcontrato = c.idcontrato INNER JOIN cliente cl ON c.idcliente = cl.idcliente INNER JOIN edificio e ON a.idedificio = e.idedificio INNER JOIN tservicio y ON s.idtservicio = y.idtservicio WHERE s.closed_time IS NOT NULL AND firma IS NOT NULL ORDER BY s.created_time DESC) "
            . " UNION "
            . " (SELECT s.facturacion, s.idfactura, s.idservicio, s.idtservicio, y.nombre AS tipostr, a.codigo, e.nombre, cl.razon_social, cl.rut, ELT(DATE_FORMAT(s.created_time,'%m'),'ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE') as strmes, MONTH(s.created_time) as mes, YEAR(s.created_time) as ano, DATE(s.created_time) AS fecha, TIME(s.created_time) AS inicio, TIME(s.closed_time) AS fin, s.reqfirma, s.created_time FROM servicio s INNER JOIN ascensor a ON s.idascensor = a.idascensor INNER JOIN contrato c ON a.idcontrato = c.idcontrato INNER JOIN cliente cl ON c.idcliente = cl.idcliente INNER JOIN edificio e ON a.idedificio = e.idedificio  INNER JOIN tservicio y ON s.idtservicio = y.idtservicio WHERE s.closed_time IS NOT NULL AND s.reqfirma = 0)) as tabla "
            . " WHERE facturacion = 2 and idfactura = $idfactura";
        return ejecutarConsulta($sql);
    }
    
    /**
     * Lista Guias de servicio con filtros de mes, año y tipo de servicio
     * @param int $tservicio
     * @param int $ano
     * @param int $mes
     * @return type
     */
    function ListarGSE($tservicio, $ano, $mes, $cc){
        $sql = "SELECT * FROM ((SELECT s.facturacion, s.idfactura, s.idservicio, s.idtservicio, y.nombre AS tipostr, a.codigo, e.nombre, cl.razon_social, cl.rut, "
                . "ELT(DATE_FORMAT(s.created_time,'%m'),'ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE') as strmes, "
                . "MONTH(s.created_time) as mes, YEAR(s.created_time) as ano, DATE(s.created_time) AS fecha, TIME(s.created_time) AS inicio, TIME(s.closed_time) AS fin, s.reqfirma, s.created_time, "
                . "s.idcentrocos, cc.codigo as 'ccnombre', s.infopro "
                . "FROM servicio s "
                . "INNER JOIN ascensor a ON s.idascensor = a.idascensor "
                . "INNER JOIN contrato c ON a.idcontrato = c.idcontrato "
                . "INNER JOIN cliente cl ON c.idcliente = cl.idcliente "
                . "INNER JOIN edificio e ON a.idedificio = e.idedificio "
                . "INNER JOIN tservicio y ON s.idtservicio = y.idtservicio "
                . "LEFT JOIN centrocosto cc ON s.idcentrocos = cc.idcentrocosto "
                . "WHERE s.closed_time IS NOT NULL AND firma IS NOT NULL ORDER BY s.created_time DESC) "
            . " UNION "
            . " (SELECT s.facturacion, s.idfactura, s.idservicio, s.idtservicio, y.nombre AS tipostr, a.codigo, e.nombre, cl.razon_social, cl.rut, "
                . "ELT(DATE_FORMAT(s.created_time,'%m'),'ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE') as strmes, "
                . "MONTH(s.created_time) as mes, YEAR(s.created_time) as ano, DATE(s.created_time) AS fecha, TIME(s.created_time) AS inicio, TIME(s.closed_time) AS fin, s.reqfirma, s.created_time, " 
                . "s.idcentrocos, cc.codigo as 'ccnombre', s.infopro "
                . "FROM servicio s "
                . "INNER JOIN ascensor a ON s.idascensor = a.idascensor "
                . "INNER JOIN contrato c ON a.idcontrato = c.idcontrato "
                . "INNER JOIN cliente cl ON c.idcliente = cl.idcliente "
                . "INNER JOIN edificio e ON a.idedificio = e.idedificio  "
                . "INNER JOIN tservicio y ON s.idtservicio = y.idtservicio "
                . "LEFT JOIN centrocosto cc ON s.idcentrocos = cc.idcentrocosto "
                . "WHERE s.closed_time IS NOT NULL AND s.reqfirma = 0)) as tabla "
            . "  WHERE facturacion = 1";
        
        if($tservicio <> 0){
            $sql.= " AND idtservicio = $tservicio ";
        }        
        if($ano <> 0){
            $sql.= " and ano = $ano ";
        }
        if($mes <> 0){
            $sql.= " and mes = $mes ";
        }
        if($cc <> 0){
            $sql.= " and idcentrocos = $cc ";
        }
        
        $sql.= " ORDER BY `created_time` DESC";
        //var_dump($sql);
        return ejecutarConsulta($sql);
    }
    
    
    /**
     * Actualiza los campos facturacion 
     * @param int $idservicio
     * @param int $stFacturacion
     * @param int $idfactura
     * @param String $infodev
     * @return type
     */
    function UpdateGSE($idservicio, $stFacturacion, $idfactura, $infodev){
        $sql = "UPDATE `servicio` "
             . "SET `facturacion`= '$stFacturacion', "
             . " idfactura = $idfactura ,"
             . " infodev = '$infodev' "
             . "WHERE `idservicio`= $idservicio";
        //var_dump($sql);
        return ejecutarConsulta($sql);
    }
    
    /**
     * Obtiene los años de todas las guias para luego mostrarlos en el filtro
     * @return type
     */
    function SelectanoGSE(){
        $sql = "SELECT DISTINCT YEAR(created_time) as ano FROM `servicio`";
        return ejecutarConsulta($sql);
    }
    
    /**
     * Obtiene un listado con todos los tipos de servicios que existen
     * @return type
     */
    public function selecttservicioGSE(){
        $sql="SELECT * FROM tservicio"; 
        return ejecutarConsulta($sql);
    }
}
