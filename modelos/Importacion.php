<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once '../config/conexion.php';
/**
 * Description of Importacion
 *
 * @author azuni
 */
class Importacion {
    //put your code here
    
    function __construct() {
    }
    
    function Insertar($codigo, $nef, $proveedor, $created_user){
        $sql = "INSERT INTO `importacion`(`codigo`, `nef`, `proveedor`,`created_user`)"
             . "VALUES ('$codigo', '$nef' , '$proveedor', $created_user)";
        return ejecutarConsulta_retornarID($sql);
    }
    
    function Editar($idimportacion, $codigo, $nef, $proveedor){
        $sql = "UPDATE `importacion` "
                . "SET `codigo`= '$codigo',"
                . "`nef`= '$nef',"
                . "`proveedor`= '$proveedor' "
                . "WHERE `idimportacion`= $idimportacion";
        return ejecutarConsulta($sql);
    }
    
    function Editartime($idimportacion, $arrival_time, $dispatch_time){
        $sql = "UPDATE `importacion` SET ";
        if($arrival_time != 0){
            $sql.= "`arrival_time` = CURRENT_TIMESTAMP ";
        }
        if($dispatch_time != 0){
            $sql.= " `dispatch_time` = CURRENT_TIMESTAMP ";
        }
        $sql.= " WHERE `idimportacion`=  $idimportacion";
        //var_dump($sql);
        return ejecutarConsulta($sql);
    }
    
    function Listar(){
        $sql = "SELECT * FROM `importacion`";
        return ejecutarConsulta($sql);
    }
    
    function Mostrar($idimportacion){
        $sql = "SELECT * FROM `importacion` WHERE idimportacion = $idimportacion";
        return ejecutarConsultaSimpleFila($sql);
    }
    
    function selectimportacion(){
        $sql = "SELECT * FROM `importacion`";
        return ejecutarConsulta($sql);
    }
}
