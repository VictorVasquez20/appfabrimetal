<?php

require_once '../config/conexion.php';

/**
 * Description of Proveedor
 *
 * @author azuni
 */
class Proveedor {

    //put your code here
    function __construct() {
        
    }

    function Insertar($rut, $razonsocial, $idcategoria, $idregion, $idcomuna, $direccion, $contacto, $fono, $email, $idcondicionpago, $plazopago, $servicioflete, $observacion, $condicion, $created_user) {
        $sql = "INSERT INTO `proveedor` (`rut`, `razonsocial`, `idcategoria`, `idregion`, `idcomuna`, `direccion`, `contacto`, `fono`, `email`, `idcondicionpago`, `plazopago`, `servicioflete`, `observacion`, `condicion`, `created_user`) "
                . "VALUES ('$rut','$razonsocial',$idcategoria,$idregion, $idcomuna, '$direccion', '$contacto', '$fono', '$email',$idcondicionpago, '$plazopago' , $servicioflete,'$observacion',$condicion, $created_user)";
        return ejecutarConsulta_retornarID($sql);
    }

    function Editar($idproveedor, $rut, $razonsocial, $idcategoria, $idregion, $idcomuna, $direccion, $contacto, $fono, $email, $idcondicionpago, $plazopago, $servicioflete, $observacion, $condicion) {
        $sql = "UPDATE `proveedor` "
                . "SET `rut`= '$rut' ,"
                . "`razonsocial`= '$razonsocial',"
                . "`idcategoria`= $idcategoria,"
                . "`idregion`= $idregion,"
                . "`idcomuna`= $idcomuna,"
                . "`direccion`= '$direccion',"
                . "`contacto`= '$contacto',"
                . "`fono`= '$fono',"
                . "`email`= '$email',"
                . "`idcondicionpago`= $idcondicionpago,"
                . "`plazopago`= '$plazopago',"
                . "`servicioflete`= $servicioflete,"
                . "`observacion`= '$observacion',"
                . "`condicion`= $condicion "
                . "WHERE `idproveedor`= $idproveedor";
        var_dump($sql);
        return ejecutarConsulta($sql);
    }

    function Mostrar($idproveedor) {
        $sql = "SELECT * FROM `proveedor` WHERE idproveedor = $idproveedor";
        return ejecutarConsultaSimpleFila($sql);
    }

    function Ficha($idproveedor) {
        $sql = "SELECT p.*, cp.nombre as 'categoriastr', r.region_nombre as 'regionstr', c.comuna_nombre as 'comunastr', cnp.nombre as 'condpagostr' "
                . "FROM proveedor p  "
                . "INNER JOIN categoria_proveedor cp ON cp.idcategoria = p.idcategoria "
                . "INNER JOIN regiones r on r.region_id = p.idregion "
                . "INNER JOIN comunas c on c.comuna_id = p.idcomuna "
                . "INNER JOIN condicionpago cnp ON cnp.idcondicionpago = p.idcondicionpago "
                . "WHERE p.idproveedor = $idproveedor";
        return ejecutarConsultaSimpleFila($sql);
    }

    function Listar($idcategoria) {
        $sql = "SELECT p.*, cp.nombre as 'categoriastr', "
            . "IFNULL((select ROUND(avg(c.evaluacion)) from compras c Where c.idproveedor = p.idproveedor), '0') as 'evaluacion', "
            . "IFNULL((select c.factura from compras c Where c.idproveedor = p.idproveedor order by c.idcompra DESC LIMIT 1), '0') as 'ultimacompra' "
            . "FROM proveedor p "
            . "INNER JOIN categoria_proveedor cp ON cp.idcategoria = p.idcategoria ";

        if ($idcategoria != 0) {
            $sql .= "WHERE p.idcategoria = $idcategoria";
        }
        
        return ejecutarConsulta($sql);
    }

}
