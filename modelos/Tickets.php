<?php

require "../config/conexion.php";

Class Tickets {

    //Constructor para instancias
    public function __construct() {
        
    }

    public function listar() {
        //$sql="SELECT t.idticket, t.estado, t.idtecnico, y.nombre, y.apellidos, a.codigo, a.ubicacion, a.codigocli, i.nombre AS tascensor, o.nombre AS marca, e.nombre AS edificio, e.calle, e.numero, t.created_time, t.assignment_time, t.attention_time, t.attention_time FROM ticket t INNER JOIN user u ON t.iduser=u.iduser LEFT JOIN tecnico y ON t.idtecnico=y.idtecnico INNER JOIN ascensor a ON t.idascensor=a.idascensor INNER JOIN tascensor i ON a.idtascensor=i.idtascensor INNER JOIN edificio e ON a.idedificio=e.idedificio INNER JOIN marca o ON a.marca = o.idmarca WHERE t.estado != 4";
        $sql = "SELECT t.idticket, t.estado, t.idtecnico, CONCAT(IFNULL(y.nombre, 'SIN'), ' ', IFNULL(y.apellidos, 'ASIGNAR')) as 'tecnico', a.codigo, a.ubicacion, a.codigocli, i.nombre AS tascensor, o.nombre AS marca, e.nombre AS edificio, e.calle, e.numero, t.created_time, t.assignment_time, t.attention_time, t.attention_time"
                . ",(SELECT s.estadofin FROM servicio s Where s.nrodocumento = t.idticket) as 'estadofin',IF(t.idllamada IS NULL,'NO TIENE',t.idllamada) as idllamada"
                . " FROM ticket t "
                . " INNER JOIN user u ON t.iduser=u.iduser "
                . " LEFT JOIN tecnico y ON t.idtecnico=y.idtecnico "
                . " INNER JOIN ascensor a ON t.idascensor=a.idascensor "
                . " INNER JOIN tascensor i ON a.idtascensor=i.idtascensor "
                . " INNER JOIN edificio e ON a.idedificio=e.idedificio "
                . " INNER JOIN marca o ON a.marca = o.idmarca "
                . " Where t.estado != 6";
        return ejecutarConsulta($sql);
    }

    public function registrar($iduser, $idascensor, $nombre, $telefono, $email, $descripcion,$idllamada='null') {
        $sql = "INSERT INTO ticket (iduser, idascensor, nombre, telefono, email, descripcion,idllamada) VALUES ('$iduser', '$idascensor', '$nombre','$telefono','$email','$descripcion',$idllamada)";
        return ejecutarConsulta_retornarID($sql);
    }
    
    public function mostrar($idticket){
        $sql = "SELECT t.idticket, t.nombre, t.telefono, t.email, t.descripcion, t.estado, t.idtecnico, a.codigo, a.ubicacion, a.codigocli, i.nombre AS tascensor, o.nombre AS marca, e.nombre AS edificio, e.calle, e.numero,c.comuna_nombre, t.created_time, t.assignment_time, t.attention_time, t.attention_time,(SELECT s.estadofin FROM servicio s Where s.nrodocumento = t.idticket) as 'estadofin'"
                . " ,DATE(t.created_time) as 'fecha', TIME(t.created_time) as 'hora' "
                . " FROM ticket t "
                . " INNER JOIN user u ON t.iduser=u.iduser "
                . " LEFT JOIN tecnico y ON t.idtecnico=y.idtecnico "
                . " INNER JOIN ascensor a ON t.idascensor=a.idascensor "
                . " INNER JOIN tascensor i ON a.idtascensor=i.idtascensor "
                . " INNER JOIN edificio e ON a.idedificio=e.idedificio "
                . " INNER JOIN comunas c ON c.comuna_id = e.idcomunas "
                . " INNER JOIN marca o ON a.marca = o.idmarca "
                . " Where t.idticket = $idticket";
        return ejecutarConsultaSimpleFila($sql);
    }
    
    public function asignatecnico($idticket, $idtecnico, $estado){
        $sql = "UPDATE `ticket` SET `idtecnico`= $idtecnico , `estado`= $estado,`assignment_time`= current_timestamp WHERE idticket = $idticket";
        return ejecutarConsulta($sql);
    }
    
    public function EnviarMailemergencia($emailfrom, $fromName, $username, $pass, $asunto, $mensaje, $correos){
            $Mailer = new PHPMailer();
            $Mailer->isSMTP();
            $Mailer->CharSet = 'UTF-8';
            $Mailer->Port = 587;
            $Mailer->SMTPAuth = true;
            $Mailer->SMTPSecure = "tls";
            $Mailer->SMTPDebug = 0;
            $Mailer->Debugoutput = 'html';
            $Mailer->Host = "smtp.gmail.com";
            $Mailer->From = $emailfrom; //"emergencia@fabrimetal.cl";
            $Mailer->FromName = $fromName; //"Sistema de solicitud de presupuestos";
            $Mailer->Subject = $asunto; //"Solicitud Informacion - Sol. Presupuesto N° " . $resp["idpresupuesto"];
            $Mailer->msgHTML($mensaje);
            $Mailer->SMTPAuth = true;
            $Mailer->Username = $username; //"emergencia@fabrimetal.cl";
            $Mailer->Password = $pass; //"1322cncn";
            
            for($i = 0; $i < count($correos); $i ++){
                $Mailer->addAddress($correos[$i]);
            }
            if (!$Mailer->send()) {
                echo "Error al enviar correo: " . $Mailer->ErrorInfo . "<br>";
            } else {
                echo "Correo enviado exitosamente<br>";
            } 
        }
}
