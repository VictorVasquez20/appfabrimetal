<?php

require "../config/conexion.php";

Class Tecnico {

    //Constructor para instancias
    public function __construct() {
        
    }

    public function Idtecnico($rut) {
        $sql = "SELECT idtecnico FROM tecnico WHERE rut='$rut'";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function Email($idservicio) {
        $sql = "SELECT t.email_interno AS email FROM servicio s INNER JOIN tecnico t ON s.idtecnico=t.idtecnico WHERE s.idservicio='$idservicio'";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function EmailSup($idservicio) {
        $sql = "SELECT u.email_interno AS email FROM servicio s INNER JOIN tecnico t ON s.idtecnico=t.idtecnico INNER JOIN supervisor u ON t.idsupervisor=u.idsupervisor  WHERE s.idservicio='$idservicio'";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function selecttecnico() {
        $sql = "Select * from tecnico";
        return ejecutarConsulta($sql);
    }
    
    public function selecttecnicosupervisor($idsupervisor) {
        $sql = "Select * from tecnico Where idsupervisor = $idsupervisor";
        return ejecutarConsulta($sql);
    }
    
    public function selecttecnicosupervisorRut($rutsupervisor) {
        $sql = "SELECT t.* FROM tecnico t INNER JOIN supervisor sup ON t.idsupervisor=sup.idsupervisor WHERE sup.rut= '$rutsupervisor'";
        return ejecutarConsulta($sql);
    }
    
    public function getEmail_tecnico_supervisor($idtecnico){
        $sql = "SELECT t.email_interno as 'etecnico', s.email_interno as 'esupervisor' FROM `tecnico` t INNER JOIN supervisor s ON s.idsupervisor = t.idsupervisor Where t.idtecnico = $idtecnico";
        return ejecutarConsultaSimpleFila($sql);
    }

}

?>