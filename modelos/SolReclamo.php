<?php 

require "../config/conexion.php";

	Class SolReclamo{
            
		//Constructor para instancias
		public function __construct(){

		}

		public function insertar($nombre, $rut, $telefono, $email, $cargo, $edificio, $mensaje){
			$sql="INSERT INTO solreclamo (nombre, rut, telefono, email, cargo, edificio, mensaje) VALUES ('$nombre', '$rut', '$telefono', '$email', '$cargo', '$edificio', '$mensaje')";
			return ejecutarConsulta($sql);
		}

		public function listar(){
			$sql="SELECT * FROM solreclamo WHERE condicion=1 ORDER BY created_time DESC";
			return ejecutarConsulta($sql);
		}
                
                public function procesado($idsolreclamo, $iduser){
                        $sql="UPDATE solreclamo SET condicion=0, resultado=1, closed_time=CURRENT_TIMESTAMP, closed_user=$iduser WHERE idsolreclamo=$idsolreclamo";
			return ejecutarConsulta($sql);
		}
                
                public function rechazado($idsolreclamo, $motivo, $iduser){
                        $sql="UPDATE solreclamo SET condicion=0, resultado=0, motivo='$motivo', closed_time=CURRENT_TIMESTAMP, closed_user=$iduser WHERE idsolreclamo='$idsolreclamo'";
			return ejecutarConsulta($sql);
		}
                               
	}
?>