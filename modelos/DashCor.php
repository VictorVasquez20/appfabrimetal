<?php
require_once '../config/conexion.php';
/**
 * Description of DashCor
 *
 * @author azuni
 */
class DashCor {
    //put your code here
    
    function __construct() {
        
    }

    function getEquiposCartera(){
        $sql = "SELECT count(*) cartera "
                ." FROM ascensor " 
                ." WHERE condicion = 1 " 
                ." AND idcontrato IS NOT NULL;";
        return ejecutarConsulta($sql);
    }

    function getGseEm(){
        $sql = "SELECT count(*) AS cant_em
                FROM servicio 
                WHERE idtservicio = 2
                AND created_time BETWEEN DATE_ADD(NOW(), INTERVAL -30 DAY) AND NOW();";
        return ejecutarConsulta($sql);
    }

    function getEquiposCarteraRM(){
        $sql = "SELECT count(*) cartera "
                ." FROM ascensor " 
                ." WHERE condicion = 1 " 
                ." AND idcontrato IS NOT NULL "
                ." AND codigo LIKE 'FM13%';";
        return ejecutarConsulta($sql);
    }

    function getGseEmRM(){
        $sql = "SELECT count(*) as cant_em "
                ." FROM servicio s "
                ." INNER JOIN ascensor a ON s.idascensor = a.idascensor "
                ." WHERE s.idtservicio = 2 "
                ." AND s.created_time BETWEEN DATE_ADD(NOW(), INTERVAL -30 DAY) AND NOW() "
                ." AND a.codigo LIKE 'FM13%';";
        return ejecutarConsulta($sql);
    }

    function getEquiposCarteraREG(){
        $sql = "SELECT count(*) cartera "
                ." FROM ascensor " 
                ." WHERE condicion = 1 " 
                ." AND idcontrato IS NOT NULL "
                ." AND codigo NOT LIKE 'FM13%';";
        return ejecutarConsulta($sql);
    }

    function getGseEmREG(){
        $sql = "SELECT count(*) as cant_em "
                ." FROM servicio s "
                ." INNER JOIN ascensor a ON s.idascensor = a.idascensor "
                ." WHERE s.idtservicio = 2 "
                ." AND s.created_time BETWEEN DATE_ADD(NOW(), INTERVAL -30 DAY) AND NOW() "
                ." AND a.codigo NOT LIKE 'FM13%';";
        return ejecutarConsulta($sql);
    }

    //////////////////////
    
    function getResumenGSE(){
        $sql = "Select distinct ts.nombre, ts.idtservicio, "
                ." (SELECT count(*) as guiames FROM `servicio` Where MONTH(created_time) = MONTH(curdate()) and YEAR(created_time) = YEAR(curdate()) AND idtservicio = s.idtservicio and closed_time is not null and estadofin is not null) as 'gsemes',"
                ." (SELECT count(*) as guiames FROM `servicio` Where MONTH(created_time) = MONTH(curdate()) and YEAR(created_time) = YEAR(curdate()) AND idtservicio = s.idtservicio and idfactura is not null) as 'gsefact' "
                ." FROM servicio s "
                ." inner join tservicio ts on ts.idtservicio = s.idtservicio "
                ." group by ts.idtservicio ";
        return ejecutarConsulta($sql);
    }
    
    
    function getResumenGSEfiltro($fecha){
        $sql = "Select distinct ts.nombre, ts.idtservicio, "
                ." (SELECT count(*) as guiames FROM `servicio` Where MONTH(created_time) = MONTH('$fecha') and YEAR(created_time) = YEAR('$fecha') AND idtservicio = s.idtservicio and closed_time is not null and estadofin is not null) as 'gsemes',"
                ." (SELECT count(*) as guiames FROM `servicio` Where MONTH(created_time) = MONTH('$fecha') and YEAR(created_time) = YEAR('$fecha') AND idtservicio = s.idtservicio and idfactura is not null) as 'gsefact' "
                ." FROM servicio s "
                ." inner join tservicio ts on ts.idtservicio = s.idtservicio "
                ." group by ts.idtservicio ";
        return ejecutarConsulta($sql);
    }
    
    function getGraficoGSE(){
        /*$sql = "Select mes, mesname, sum(guiamesfact) as 'guiamesfact', SUM(guiames) as 'guiames' "
                ." FROM((SELECT MONTH(created_time) as mes, MONTHNAME(created_time) mesname, 0 as 'guiamesfact',COUNT(*) as 'guiames' FROM `servicio` Where YEAR(created_time) = YEAR(curdate()) group by MONTH(created_time)) union "
                ." (SELECT MONTH(created_time) as mes, MONTHNAME(created_time) mesname, COUNT(*) as 'guiamesfact', 0 FROM `servicio` Where YEAR(created_time) = YEAR(curdate()) and idfactura is not null group by MONTH(created_time))) as tabla "
                ." group by mes order by mes";*/
        $sql = "Select mes, mesname, sum(guiamesfact) as 'guiamesfact', SUM(guiames) as 'guiames' , idtservicio FROM(
                (SELECT MONTH(created_time) as mes, MONTHNAME(created_time) mesname, 0 as 'guiamesfact',COUNT(*) as 'guiames', idtservicio FROM `servicio` Where YEAR(created_time) = YEAR(curdate()) group by MONTH(created_time) , idtservicio) 
                union 
                (SELECT MONTH(created_time) as mes, MONTHNAME(created_time) mesname, COUNT(*) as 'guiamesfact', 0 , idtservicio FROM `servicio` Where YEAR(created_time) = YEAR(curdate()) and idfactura is not null group by MONTH(created_time), idtservicio)) as tabla 
                group by mes , idtservicio order by mes";
        return ejecutarConsulta($sql);
    }
    
    function graficoGSEemergencias(){
        $sql = "SELECT MONTH(created_time) as mes, MONTHNAME(created_time) mesname, 0 as 'guiamesfact',COUNT(*) as 'guiames', idtservicio , estadofin
                FROM `servicio` 
                Where YEAR(created_time) = YEAR(curdate()) and idtservicio = 2 and estadofin is not null
                group by MONTH(created_time) , idtservicio, estadofin";
        return ejecutarConsulta($sql);
    }
    
    function GSEemergencias($fecha){
        $sql = "SELECT MONTH(created_time) as mes, MONTHNAME(created_time) mesname, 0 as 'guiamesfact',COUNT(*) as 'guiames', idtservicio , estadofin
                FROM `servicio` 
                Where YEAR(created_time) = YEAR('$fecha') and MONTH(created_time) = MONTH('$fecha') and idtservicio = 2 and estadofin is not null
                group by MONTH(created_time) , idtservicio, estadofin";
        return ejecutarConsulta($sql);
    }



    //////////////////



    public function selectSupervisor($idregion) {
        if ($idregion == 'TODAS'){
            $sql = "SELECT distinct su.idsupervisor, UPPER(CONCAT( su.nombre, ' ', su.apellido)) supervisor "
                    ." FROM servicio s "
                    ." INNER JOIN ascensor a ON s.idascensor = a.idascensor "
                    ." INNER JOIN tecnico t ON s.idtecnico = t.idtecnico "
                    ." INNER JOIN supervisor su ON t.idsupervisor = su.idsupervisor "
                    ." WHERE s.idtservicio = 2 "
                    ." AND s.created_time BETWEEN DATE_ADD(NOW(), INTERVAL -30 DAY) AND NOW() "
                    ." ORDER BY 2 ASC;";
        }
        if ($idregion == 'REG'){
            $sql = "SELECT distinct su.idsupervisor, UPPER(CONCAT( su.nombre, ' ', su.apellido)) supervisor "
                    ." FROM servicio s "
                    ." INNER JOIN ascensor a ON s.idascensor = a.idascensor "
                    ." INNER JOIN tecnico t ON s.idtecnico = t.idtecnico "
                    ." INNER JOIN supervisor su ON t.idsupervisor = su.idsupervisor "
                    ." WHERE s.idtservicio = 2 "
                    ." AND s.created_time BETWEEN DATE_ADD(NOW(), INTERVAL -30 DAY) AND NOW() "
                    ." AND a.codigo NOT LIKE 'FM13%' "
                    ." ORDER BY 2 ASC;";
        }
        if ($idregion == '01' || $idregion == '02' || $idregion == '03' || $idregion == '04' || $idregion == '05' || $idregion == '06' || $idregion == '07' || $idregion == '08' || $idregion == '09' || $idregion == '10' || $idregion == '11' || $idregion == '12' || $idregion == '13' || $idregion == '14' || $idregion == '15' || $idregion == '16'){
            $sql = "SELECT distinct su.idsupervisor, UPPER(CONCAT( su.nombre, ' ', su.apellido)) supervisor "
                    ." FROM servicio s "
                    ." INNER JOIN ascensor a ON s.idascensor = a.idascensor "
                    ." INNER JOIN tecnico t ON s.idtecnico = t.idtecnico "
                    ." INNER JOIN supervisor su ON t.idsupervisor = su.idsupervisor "
                    ." WHERE s.idtservicio = 2 "
                    ." AND s.created_time BETWEEN DATE_ADD(NOW(), INTERVAL -30 DAY) AND NOW() "
                    ." AND a.codigo LIKE 'FM" . $idregion ."%' "
                    ." ORDER BY 2 ASC;";
        }
        return ejecutarConsulta($sql);
    }

    public function selectTecnico($idregion, $idsuperv) {
        if ($idregion == 'TODAS'){
            $sql = "SELECT distinct t.idtecnico, UPPER(CONCAT( t.nombre, ' ', t.apellidos)) tecnico "
                    ." FROM servicio s "
                    ." INNER JOIN ascensor a ON s.idascensor = a.idascensor "
                    ." INNER JOIN tecnico t ON s.idtecnico = t.idtecnico "
                    ." INNER JOIN supervisor su ON t.idsupervisor = su.idsupervisor "
                    ." WHERE s.idtservicio = 2 "
                    ." AND s.created_time BETWEEN DATE_ADD(NOW(), INTERVAL -30 DAY) AND NOW() "
                    ." AND (a.idsup = $idsuperv or $idsuperv = 0) " 
                    ." ORDER BY 2 ASC;";
        }
        if ($idregion == 'REG'){
            $sql = "SELECT distinct t.idtecnico, UPPER(CONCAT( t.nombre, ' ', t.apellidos)) tecnico "
                    ." FROM servicio s "
                    ." INNER JOIN ascensor a ON s.idascensor = a.idascensor "
                    ." INNER JOIN tecnico t ON s.idtecnico = t.idtecnico "
                    ." INNER JOIN supervisor su ON t.idsupervisor = su.idsupervisor "
                    ." WHERE s.idtservicio = 2 "
                    ." AND s.created_time BETWEEN DATE_ADD(NOW(), INTERVAL -30 DAY) AND NOW() "
                    ." AND a.codigo NOT LIKE 'FM13%' "
                    ." AND (a.idsup = $idsuperv or $idsuperv = 0) " 
                    ." ORDER BY 2 ASC;";
        }
        if ($idregion == '01' || $idregion == '02' || $idregion == '03' || $idregion == '04' || $idregion == '05' || $idregion == '06' || $idregion == '07' || $idregion == '08' || $idregion == '09' || $idregion == '10' || $idregion == '11' || $idregion == '12' || $idregion == '13' || $idregion == '14' || $idregion == '15' || $idregion == '16'){
            $sql = "SELECT distinct t.idtecnico, UPPER(CONCAT( t.nombre, ' ', t.apellidos)) tecnico "
                    ." FROM servicio s "
                    ." INNER JOIN ascensor a ON s.idascensor = a.idascensor "
                    ." INNER JOIN tecnico t ON s.idtecnico = t.idtecnico "
                    ." INNER JOIN supervisor su ON t.idsupervisor = su.idsupervisor "
                    ." WHERE s.idtservicio = 2 "
                    ." AND s.created_time BETWEEN DATE_ADD(NOW(), INTERVAL -30 DAY) AND NOW() "
                    ." AND a.codigo LIKE 'FM" . $idregion ."%' "
                    ." AND (a.idsup = $idsuperv or $idsuperv = 0) " 
                    ." ORDER BY 2 ASC;";
        }
        return ejecutarConsulta($sql);
    }

    function getEquiposCarteraFiltro($idregion,$idsuperv, $idtec){
        //echo $idregion;
        if ($idregion == 'TODAS'){
            $sql = "SELECT count(*) cartera "
                    ." FROM ascensor " 
                    ." WHERE condicion = 1 " 
                    ." AND idcontrato IS NOT NULL "
                    ." AND (idsup = $idsuperv or $idsuperv = 0)"
                    ." AND (idtecnico = $idtec or $idtec = 0);" ;
        }
        if ($idregion == 'REG'){
            $sql = "SELECT count(*) cartera "
                    ." FROM ascensor " 
                    ." WHERE condicion = 1 " 
                    ." AND idcontrato IS NOT NULL "
                    ." AND codigo NOT LIKE 'FM13%' "
                    ." AND (idsup = $idsuperv or $idsuperv = 0)" 
                    ." AND (idtecnico = $idtec or $idtec = 0);" ;
        }
        if ($idregion == '01' || $idregion == '02' || $idregion == '03' || $idregion == '04' || $idregion == '05' || $idregion == '06' || $idregion == '07' || $idregion == '08' || $idregion == '09' || $idregion == '10' || $idregion == '11' || $idregion == '12' || $idregion == '13' || $idregion == '14' || $idregion == '15' || $idregion == '16'){
            $sql = "SELECT count(*) cartera "
                    ." FROM ascensor " 
                    ." WHERE condicion = 1 " 
                    ." AND idcontrato IS NOT NULL "
                    ." AND codigo LIKE 'FM" . $idregion ."%' "
                    ." AND (idsup = $idsuperv or $idsuperv = 0)"
                    ." AND (idtecnico = $idtec or $idtec = 0);" ;
        }

        //echo $sql;

        return ejecutarConsulta($sql);
    }

    function getGseEmFiltro($idregion,$idsuperv, $idtec,$dias){
        if ($idregion == 'TODAS'){
            $sql = "SELECT count(*) AS cant_em
                    FROM servicio s
                    INNER JOIN tecnico t ON s.idtecnico = t.idtecnico 
                    WHERE s.idtservicio = 2
                    AND s.created_time BETWEEN DATE_ADD(NOW(), INTERVAL -$dias DAY) AND NOW()
                    AND (t.idsupervisor = $idsuperv or $idsuperv = 0)
                    AND (t.idtecnico = $idtec or $idtec = 0);";
        }
        if ($idregion == 'REG'){
            $sql = "SELECT count(*) AS cant_em
                    FROM servicio s
                    INNER JOIN ascensor a ON s.idascensor = a.idascensor
                    INNER JOIN tecnico t ON s.idtecnico = t.idtecnico 
                    WHERE s.idtservicio = 2
                    AND s.created_time BETWEEN DATE_ADD(NOW(), INTERVAL -$dias DAY) AND NOW()
                    AND (t.idsupervisor = $idsuperv or $idsuperv = 0)
                    AND (t.idtecnico = $idtec or $idtec = 0)
                    AND a.codigo NOT LIKE 'FM13%';";
        }
        if ($idregion == '01' || $idregion == '02' || $idregion == '03' || $idregion == '04' || $idregion == '05' || $idregion == '06' || $idregion == '07' || $idregion == '08' || $idregion == '09' || $idregion == '10' || $idregion == '11' || $idregion == '12' || $idregion == '13' || $idregion == '14' || $idregion == '15' || $idregion == '16'){
            $sql = "SELECT count(*) AS cant_em
                    FROM servicio s
                    INNER JOIN ascensor a ON s.idascensor = a.idascensor
                    INNER JOIN tecnico t ON s.idtecnico = t.idtecnico 
                    WHERE s.idtservicio = 2
                    AND s.created_time BETWEEN DATE_ADD(NOW(), INTERVAL -$dias DAY) AND NOW()
                    AND (t.idsupervisor = $idsuperv or $idsuperv = 0)
                    AND (t.idtecnico = $idtec or $idtec = 0)
                    AND a.codigo LIKE 'FM" . $idregion ."%';";
        }

        return ejecutarConsulta($sql);
    }

    function listarguiasdetalle($idregion,$idsuperv, $idtec){
        if ($idregion == 'TODAS'){
            $sql = "SELECT s.idservicio, e.nombre, a.codigo
                    FROM servicio s
                    INNER JOIN ascensor a ON s.idascensor = a.idascensor
                    INNER JOIN edificio e ON a.idedificio = e.idedificio
                    WHERE s.created_time BETWEEN DATE_ADD(NOW(), INTERVAL -30 DAY) AND NOW()
                    AND s.idtservicio = 2
                    AND (a.idsup = $idsuperv or $idsuperv = 0)
                    AND (s.idtecnico = $idtec or $idtec = 0)
                    ORDER BY 1 ASC";
        }
        if ($idregion == 'REG'){
            $sql = "SELECT s.idservicio, e.nombre, a.codigo
                    FROM servicio s
                    INNER JOIN ascensor a ON s.idascensor = a.idascensor
                    INNER JOIN edificio e ON a.idedificio = e.idedificio
                    WHERE s.created_time BETWEEN DATE_ADD(NOW(), INTERVAL -30 DAY) AND NOW()
                    AND s.idtservicio = 2
                    AND (a.idsup = $idsuperv or $idsuperv = 0)
                    AND (s.idtecnico = $idtec or $idtec = 0)
                    AND a.codigo NOT LIKE 'FM13%'
                    ORDER BY 1 ASC";
        }
        if ($idregion == '01' || $idregion == '02' || $idregion == '03' || $idregion == '04' || $idregion == '05' || $idregion == '06' || $idregion == '07' || $idregion == '08' || $idregion == '09' || $idregion == '10' || $idregion == '11' || $idregion == '12' || $idregion == '13' || $idregion == '14' || $idregion == '15' || $idregion == '16'){
            $sql = "SELECT s.idservicio, e.nombre, a.codigo
                    FROM servicio s
                    INNER JOIN ascensor a ON s.idascensor = a.idascensor
                    INNER JOIN edificio e ON a.idedificio = e.idedificio
                    WHERE s.created_time BETWEEN DATE_ADD(NOW(), INTERVAL -30 DAY) AND NOW()
                    AND s.idtservicio = 2
                    AND (a.idsup = $idsuperv or $idsuperv = 0)
                    AND (s.idtecnico = $idtec or $idtec = 0)
                    AND a.codigo LIKE 'FM" . $idregion ."%'
                    ORDER BY 1 ASC;";
        }

        return ejecutarConsulta($sql);
    }

    
}
