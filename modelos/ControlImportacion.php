<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once '../config/conexion.php';
/**
 * Description of ControlImportacion
 *
 * 
 */
class ControlImportacion {
    
    function __construct() {
    }
    
    function Listar($mes= '', $anio=''){
        $where = '';
        $contador = 0;
        if($mes !=''){
            $where .= "WHERE MONTH(ctli_fechapedido) = '$mes' ";
            $contador++;
        }
        if($anio != ''){
            if($contador == 0){
                $where .= "WHERE YEAR(ctli_fechapedido) = '$anio' ";
                $contador++;
            }else{
                $where .= "AND YEAR(ctli_fechapedido) = '$anio' ";
            }
        }
        $sql = "SELECT * FROM `controlimportacion` ".$where;
        return ejecutarConsulta($sql);
    }
    
    function Mostrar($ctli_id){
        $sql = "SELECT * FROM `controlimportacion` WHERE ctli_id = $ctli_id";
        return ejecutarConsultaSimpleFila($sql);
    }

    function InsertarImportacion($ctli_iduser,$ctli_numref,$ctli_fechapedido,$ctli_codkm,$ctli_descripcion,$ctli_codst,$ctli_cantidad,$ctli_numpres,$ctli_solicitante,$ctli_observacion,$ctli_st_fecha){
        $sql = "INSERT INTO `controlimportacion` (`ctli_iduser`,`ctli_numref`,`ctli_fechapedido`,`ctli_codkm`,`ctli_descripcion`,`ctli_codst`,`ctli_codst_fecha`,`ctli_cantidad`,`ctli_numpres`,`ctli_solicitante`,`ctli_observacion`) VALUES ('$ctli_iduser','$ctli_numref','$ctli_fechapedido','$ctli_codkm','$ctli_descripcion','$ctli_codst','$ctli_st_fecha',$ctli_cantidad,'$ctli_numpres','$ctli_solicitante','$ctli_observacion')";
        return ejecutarConsulta_retornarID($sql);
    }

    function EditarImportacion($ctli_id,$ctli_iduser,$ctli_numref,$ctli_fechapedido,$ctli_codkm,$ctli_descripcion,$ctli_codst,$ctli_cantidad,$ctli_numpres,$ctli_solicitante,$ctli_observacion,$ctli_st_fecha){
        $sql = "UPDATE `controlimportacion` SET `ctli_iduser` = '$ctli_iduser', `ctli_numref` = '$ctli_numref', `ctli_fechapedido` = '$ctli_fechapedido', `ctli_codkm` = '$ctli_codkm', `ctli_descripcion` = '$ctli_descripcion', `ctli_codst` = '$ctli_codst', `ctli_codst_fecha` = '$ctli_st_fecha', `ctli_cantidad` = $ctli_cantidad, `ctli_numpres` = '$ctli_numpres', `ctli_solicitante` = '$ctli_solicitante',`ctli_observacion` = '$ctli_observacion',`ctli_updated_time` = current_timestamp()  WHERE `ctli_id` = '$ctli_id'";
        return ejecutarConsulta($sql);
    }
    function InsertarFechas($ctli_id,$ctli_fechaestdesp,$ctli_fechadesp,$ctli_fechaestllegada,$ctli_fechallegada){
        $contador = 0;
        $set = '';
        if(!empty($ctli_fechaestdesp)){
            if($contador == 0){
                $set .= "ctli_fechaestdesp = '".$ctli_fechaestdesp."'";
                $contador =$contador +1;
            }else{
                $set .= ", ctli_fechaestdesp = '".$ctli_fechaestdesp."'";
            }
        }

        if(!empty($ctli_fechadesp)){
            if($contador == 0){
                $set .= "ctli_fechadesp = '".$ctli_fechadesp."'";
                $contador =$contador +1;
            }else{
                $set .= ", ctli_fechadesp = '".$ctli_fechadesp."'";
            }
        }

        if(!empty($ctli_fechaestllegada)){
            if($contador == 0){
                $set .= "ctli_fechaestllegada = '".$ctli_fechaestllegada."'";
                $contador =$contador +1;
            }else{
                $set .= ", ctli_fechaestllegada = '".$ctli_fechaestllegada."'";
            }
        }

        if(!empty($ctli_fechallegada)){
            if($contador == 0){
                $set .= "ctli_fechallegada = '".$ctli_fechallegada."'";
                $contador =$contador +1;
            }else{
                $set .= ", ctli_fechallegada = '".$ctli_fechallegada."'";
            }
        }

        $sql = "UPDATE `controlimportacion` SET ".$set." WHERE `ctli_id` = '$ctli_id'";
        return ejecutarConsulta($sql);
    }
    function InsertarComentario($ctli_id,$ctcm_comentario){
        $sql = "INSERT INTO `controlimp_comentarios` (`ctli_id`,`cicm_iduser`,`cicm_comentario`) VALUES ('$ctli_id','" . $_SESSION['iduser'] . "','$ctcm_comentario')";
        return ejecutarConsulta_retornarID($sql);
    }

    function CambiarStatus($ctli_id){
        $sql = "UPDATE `controlimportacion` SET `ctli_status` = 'terminado' WHERE `ctli_id` = $ctli_id";
        return ejecutarConsulta($sql);
    }

    function ListarAgrupado($mes= '', $anio=''){
        $where = '';
        $contador = 0;
        if($mes !=''){
            $where .= "WHERE MONTH(ctli_fechapedido) = '$mes' ";
            $contador++;
        }
        if($anio != ''){
            if($contador == 0){
                $where .= "WHERE YEAR(ctli_fechapedido) = '$anio' ";
                $contador++;
            }else{
                $where .= "AND YEAR(ctli_fechapedido) = '$anio' ";
            }
        }
        $sql = "SELECT *,GROUP_CONCAT(ctli_codkm) AS 'grupo' FROM `controlimportacion` ".$where." GROUP BY ctli_numref";
        return ejecutarConsulta($sql);
    }

    function ListarPantalla($anio, $mes, $estado='%'){
        $where = '';
        $contador = 0;
        if($mes !=''){
            $where .= "WHERE MONTH(ctli_fechapedido) = '$mes' ";
            $contador++;
        }else{
            $where .= " WHERE ctli_status LIKE '$estado' ";
            $contador++;
        }

        if($anio != ''){
            if($contador == 0){
                $where .= "WHERE YEAR(ctli_fechapedido) = '$anio' AND ctli_status LIKE '$estado' ";
                $contador++;
            }else{
                $where .= "AND YEAR(ctli_fechapedido) = '$anio' ";
            }
        }
        //$sql = "SELECT * FROM `controlimportacion` WHERE YEAR(ctli_fechapedido) = '$anio' AND MONTH(ctli_fechapedido) = '$mes' and ctli_status LIKE '$estado' ORDER BY ctli_status, ctli_numref, ctli_fechapedido";
        $sql = "SELECT * FROM `controlimportacion` $where ORDER BY ctli_status, ctli_numref, ctli_fechapedido";
        return ejecutarConsulta($sql);
    }

    function ListarporCodigo($codigo){
        $sql = "SELECT * FROM `controlimportacion` WHERE ctli_numref = '$codigo'";
        return ejecutarConsulta($sql);
    }

    function ListarComentarios($codigo){
        // $sql = "SELECT * FROM `controlimp_comentarios` WHERE ctli_id = '$codigo' order by cicm_id DESC";
        // $sql = "SELECT com.*, CONCAT(us.nombre,' ', us.apellido) AS usuario FROM controlimp_comentarios AS com LEFT JOIN user AS us ON com.cicm_iduser = us.iduser WHERE ctli_id = '$codigo' order by cicm_id DESC";
        $sql = "SELECT com.*, CONCAT(us.username) AS usuario FROM controlimp_comentarios AS com LEFT JOIN user AS us ON com.cicm_iduser = us.iduser WHERE ctli_id = '$codigo' order by cicm_id DESC";
        return ejecutarConsulta($sql);
    }

    function AnioPantalla(){
        $sql = "SELECT distinct year(ctli_fechapedido) AS anio FROM `controlimportacion` ORDER BY 1 DESC";
        return ejecutarConsulta($sql);
    }

    function MesPantalla($anio){
        $sql = "SELECT distinct MONTH(ctli_fechapedido) AS mes,
                  CASE
                      WHEN MONTH(ctli_fechapedido) = 1 THEN 'ENERO'
                      WHEN MONTH(ctli_fechapedido) = 2 THEN 'FEBRERO'
                      WHEN MONTH(ctli_fechapedido) = 3 THEN 'MARZO'
                      WHEN MONTH(ctli_fechapedido) = 4 THEN 'ABRIL'
                      WHEN MONTH(ctli_fechapedido) = 5 THEN 'MAYO'
                      WHEN MONTH(ctli_fechapedido) = 6 THEN 'JUNIO'
                      WHEN MONTH(ctli_fechapedido) = 7 THEN 'JULIO'
                      WHEN MONTH(ctli_fechapedido) = 8 THEN 'AGOSTO'
                      WHEN MONTH(ctli_fechapedido) = 9 THEN 'SEPTIEMBRE'
                      WHEN MONTH(ctli_fechapedido) = 10 THEN 'OCTUBRE'
                      WHEN MONTH(ctli_fechapedido) = 11 THEN 'NOVIEMBRE'
                      WHEN MONTH(ctli_fechapedido) = 12 THEN 'DICIEMBRE'
                  END AS nombre_mes FROM `controlimportacion` WHERE year(ctli_fechapedido) = '$anio' ORDER BY 1 ASC";
        return ejecutarConsulta($sql);
    }

    function ContarComentarios(){
        $sql = "SELECT COUNT(*) AS maximo FROM controlimp_comentarios GROUP BY ctli_id ORDER BY maximo DESC LIMIT 1";
        return ejecutarConsultaSimpleFila($sql);
    }

    function BuscarComentarios($id){
        $sql = "SELECT com.*, CONCAT(us.username) AS usuario FROM controlimp_comentarios AS com LEFT JOIN user AS us ON com.cicm_iduser = us.iduser WHERE ctli_id = '$id' order by cicm_id ASC";
        // $sql = "SELECT * FROM controlimp_comentarios WHERE ctli_id = '$id' ";
        return ejecutarConsulta($sql);
    }

    function listarAnios(){
         $sql = "SELECT YEAR(ctli_fechapedido) AS 'anio' FROM controlimportacion GROUP BY YEAR(ctli_fechapedido)";
        return ejecutarConsulta($sql);
    }
}