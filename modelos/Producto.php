<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once '../config/conexion.php';
require_once 'Bodega.php';
/**
 * Description of Producto
 *
 * @author aaron
 */
class Producto {
    
    //put your code here
    function __construct() {
        
    }
    
    function Insertar($nombre,$descripcion, $codigo, $codigobarras ,$categoria, $created_user, $vigencia, $precio){
        //$sql = "INSERT INTO `producto`( `nombre`, `codigo`, `categoria`, `created_user`, `vigencia`, `precio`,`stockcritico`) "
        //        . "VALUES ('$nombre', '$codigo', $categoria, $created_user, $vigencia, $precio, $stockcritico)";
        
        $sql = "INSERT INTO `producto` (`nombre`, `descripcion`, `codigo`, `codigobarras`, `categoria`, `precio`, `vigencia`, `created_user`) "
                . "VALUES ('$nombre','$descripcion','$codigo','$codigobarras', $categoria, '$precio', $vigencia, '$created_user')";
        
        //var_dump($sql);
        return ejecutarConsulta($sql);
    }
    
    function Editar($idproducto, $nombre,$descripcion, $codigo, $codigobarras ,$categoria, $vigencia, $precio){
        $sql = "UPDATE `producto` SET "
                . "`nombre`= '$nombre',"
                . "`descripcion` = '$descripcion',"
                . "`codigo`= '$codigo',"
                . "`codigobarras` = '$codigobarras', "
                . "`categoria`= $categoria,"
                . "`precio` = $precio,"
                . "`vigencia`= $vigencia, "
                . "WHERE  `idproducto`= $idproducto";
        return ejecutarConsulta($sql);
    }
    
    function editarStock($idproducto, $stock, $tipomovimiento, $idbodega){
        $sql = "SELECT idstockpb, COUNT(*) as existe, IFNULL(stock,0) as stock FROM `stockprodbodega` Where idproducto = $idproducto and idbodega = $idbodega";
        $rspta = ejecutarConsulta($sql);
        
        while ($reg = $rspta->fetch_object()){
            $existe = $reg->existe;
            $stockAnt = $reg->stock;
            $idstockpb = $reg->idstockpb;
        }
        
        //SI EXISTE MODIFICO EL STOCK
        if($existe == 1){
            $stockNuevo = 0;
            /*
             * $tipomovimiento
             * 1 = ingreso
             * 0 = salida
             */
            if($tipomovimiento == 0){
                $stockNuevo = (float)$stockAnt - (float)$stock;
            }else{
                $stockNuevo = (float)$stockAnt + (float)$stock;
            }
            $sql2 = "UPDATE `stockprodbodega` "
                    . "SET `stock`= $stockNuevo "
                    . "WHERE `idstockpb`= $idstockpb " ;
            //var_dump($sql2);
            return ejecutarConsulta($sql2);
            
        }else{
            
            $sql2 = "INSERT INTO `stockprodbodega`(`idproducto`, `idbodega`, `stock`, `stockcritico`, `stockmaximo`) "
                    . "VALUES ($idproducto, $idbodega, $stock, 5, 0) " ;
            //var_dump($sql2);
            return ejecutarConsulta($sql2);
            
        }
        
        
        
        /* ------------- STOCk ------------------ */
        /*$sql = "SELECT stock FROM `producto` Where idproducto = $idproducto ";
        $rspta = ejecutarConsulta($sql);
        
        while ($reg = $rspta->fetch_object()){
            $stockAnt = $reg->stock;
        }
        
        $stockNuevo = 0;
        
        if($tipomovimiento == 0){
            $stockNuevo = (int)$stockAnt - (int)$stock;
        }else{
            $stockNuevo = (int)$stockAnt + (int)$stock;
        }
        
        
        $sql2 = "UPDATE `producto` SET "
                . "`stock` = $stockNuevo"
                . " WHERE  `idproducto`= $idproducto";
        return ejecutarConsulta($sql2);*/
    }
    
    function mostrar($idproducto){
        $sql = "SELECT * FROM `producto` WHERE `idproducto`= $idproducto";
        return ejecutarConsultaSimpleFila($sql);
    }
    
    function ver($idproducto, $bodega){
        /*$sql = "SELECT p.*, cp.nombre as 'nombCategoria' "
                ." FROM producto as p "
                ." inner join categoria_producto as cp on p.categoria = cp.idcategoria "
                ." WHERE `idproducto`= $idproducto";
        
        $sql = "SELECT idproducto, nombre, codigo, precio, stock, stockcritico, IFNULL(SUM(cantidad), 0) as cantidad, categoriaprod, bodega
                FROM
                (SELECT p.* , IF(s.tipomovimiento = 1, s.cantidad, (s.cantidad * -1)) as cantidad, b.nombre as 'bodega', c.nombre as 'categoriaprod'
                 FROM`producto` p
                 INNER JOIN stock s on s.idproducto = p.idproducto
                 INNER JOIN bodega b on b.idbodega = s.idbodega
                 INNER JOIN categoria_producto c on c.idcategoria = p.categoria
                 WHERE p.idproducto = $idproducto and s.idbodega = $bodega) as tabla";*/
        
        $sql = "SELECT p.*, c.nombre as 'categoriaprod',
                IFNULL((SELECT s.stock FROM stockprodbodega s WHere s.idproducto = p.idproducto and s.idbodega = $bodega),0) as 'cantidad',
                IFNULL((SELECT SUM(s.stock) FROM stockprodbodega s WHere s.idproducto = p.idproducto),0) as 'stock',
                IFNULL((SELECT s.stockcritico FROM stockprodbodega s WHere s.idproducto = p.idproducto and s.idbodega = $bodega),0) as 'stockcritico',
                (SELECT b.nombre FROM bodega b WHERE b.idbodega = $bodega) as 'bodega'
                FROM`producto` p
                INNER JOIN categoria_producto c on c.idcategoria = p.categoria
                WHERE p.idproducto = $idproducto";
        
        return ejecutarConsultaSimpleFila($sql);
    }
    
    function Listar(){
        $sql = "SELECT  p.idproducto, p.nombre, p.codigo, cp.nombre as 'nombCategoria', p.vigencia,"
              . " IFNULL((SELECT SUM(stock) FROM stockprodbodega WHERE idproducto = p.idproducto),0) as stock  "
              . " FROM producto as p "
              . " inner join categoria_producto as cp on p.categoria = cp.idcategoria ";
        
        //var_dump($sql);
        return ejecutarConsulta($sql);
    }
    
     function ListarSearch($texto){
        $sql = "SELECT p.*, cp.nombre as 'nombCategoria', "
                ." FROM producto as p "
                ." inner join categoria_producto as cp on p.categoria = cp.idcategoria "
                ." WHERE (p.nombre like '%$texto%' or p.codigo like '%$texto%', or p.codigobarras like '%$texto%')";
        return ejecutarConsulta($sql);
    }
    
    function ListarVigentes(){
        $sql = "SELECT p.*, cp.nombre as 'nombCategoria' "
                ." FROM producto as p "
                ." inner join categoria_producto as cp on p.categoria = cp.idcategoria "
                ." Where p.vigencia = 1";
        return ejecutarConsulta($sql);
    }
    
    function existe($codigo){
        $sql = "SELECT count(*) as 'existe' FROM `producto` WHERE `codigo` = '$codigo'";
        return ejecutarConsultaSimpleFila($sql);
    }
    
    
    function selectproducto($idcategoria){
        $sql = "SELECT p.*, cp.nombre as 'nombCategoria' "
                ." FROM producto as p "
                ." inner join categoria_producto as cp on p.categoria = cp.idcategoria "
                ." Where p.vigencia = 1 and p.categoria = $idcategoria ";
        return ejecutarConsulta($sql);
    }
}
