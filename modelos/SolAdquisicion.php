<?php
require_once '../config/conexion.php';

/**
 * Description of SolicitudAdquisicion
 *
 * @author azuni
 */
class SolAdquisicion {
    //put your code here
    
    function __construct() {
        
    }
    
    /**
     * Inserta los dattos de la solicitud de adquisicion en la base de datos
     * @param int $ccosto
     * @param date $fecha
     * @param string $observacion
     * @param int $estado
     * @param int $fase
     * @param int $solicita
     * @param int $proveedor
     * @param int $condpago
     * @param int $moneda
     * @param string $archivo
     * @return type
     */
    function insertar($ccosto, $fecha, $observacion, $estado, $fase, $solicita, $proveedor, $condpago, $moneda, $archivo){
        $sql = "INSERT INTO `soladquisicion`(`ccosto`, `fecha`, `observacion`, `estado`, `fase`, `solicita`,`proveedor`,`condpago`, `moneda`,`archivo`) "
                . "VALUES ($ccosto, '$fecha', '$observacion', $estado, $fase, $solicita, $proveedor, $condpago, $moneda, '$archivo')";
        //var_dump($sql);
        return ejecutarConsulta_retornarID($sql);
    }
    
    /**
     * Edita los datos de la solicitud de adquisicion
     * @param int $idsolicitud
     * @param int $ccosto
     * @param date $fecha
     * @param string $observacion
     * @param int $estado
     * @param int $fase
     * @param int $solicita
     * @param int $proveedor
     * @param int $condpago
     * @param int $moneda
     * @param string $archivo
     * @return type
     */
    function editar($idsolicitud, $ccosto, $fecha, $observacion, $estado, $fase, $solicita, $proveedor, $condpago, $moneda, $archivo){
        $sql = "UPDATE `soladquisicion` SET "
                . "`ccosto`= $ccosto,"
                . "`fecha`= '$fecha',"
                . "`observacion`= '$observacion',"
                . "`estado`= $estado,"
                . "`fase`= $fase,"
                . "`solicita`= $solicita, "
                . "`proveedor`= $proveedor,"
                . "`condpago`= $condpago,"
                . "`moneda`= $moneda ,"
                . "`archivo`= '$archivo' "
                . " WHERE `idsolicitud`= $idsolicitud ";
        //var_dump($sql);
        return ejecutarConsulta($sql);
    }
    
    /**
     * Obtiene todos los datos de una solicitud de adquisicion
     * @param int $idsolicitud
     * @return type
     */
    function mostrar($idsolicitud){
        $sql = "SELECT * FROM `soladquisicion` WHERE `idsolicitud`= $idsolicitud ";
        return ejecutarConsultaSimpleFila($sql);
    }
    
    /**
     * Obtiene los datos de la solicitud de adquisicion junto con los nombre de los campos tales como centro de costo, moneda, etc.
     * @param int $idsolicitud
     * @return type
     */
    function ver($idsolicitud){
        $sql = "SELECT s.idsolicitud, c.nombre as 'ccosto', DATE_FORMAT(s.fecha,'%d/%m/%Y') as 'fecha', s.observacion, s.estado, s.fase, concat(u.nombre, ' ', u.apellido) as 'usuario', cp.nombre as 'condpago',  s.moneda, m.nombre as 'strmoneda', p.rut, p.razonsocial, u.email,
                (select sum(d.cantidad * d.valor) from detsoladquisicion d Where d.idsolicitud = s.idsolicitud) as 'total' , s.archivo, s.created_time, f.iduser, f.completasolicitud,
                (select max(fa.orden) from faseaprobacion fa Where fa.idtcentrocosto = c.tcentrocosto) as 'fasefinal',
                concat(u2.nombre, ' ', u2.apellido) as 'usuarioaprueba', u2.email as 'aprueba'
                FROM `soladquisicion`s
                INNER JOIN proveedor p on s.proveedor = p.idproveedor
                INNER JOIN centrocosto c on c.idcentrocosto = s.ccosto
                LEFT JOIN condicionpago cp on cp.idcondicionpago = s.condpago
                LEFT JOIN moneda m on m.idmoneda = s.moneda
                INNER JOIN user u on u.iduser = s.solicita
                INNER JOIN faseaprobacion f on f.idtcentrocosto = c.tcentrocosto and f.orden = s.fase
                INNER JOIN user u2 ON u2.iduser = f.iduser
                WHERE `idsolicitud`= $idsolicitud ";
        return ejecutarConsultaSimpleFila($sql);
    }
    
    /**
     * Lista todos las solicitudes de adquisicion que corresponden al usuario
     * @param int $solicita
     * @return type
     */
    function listar($solicita){
        $sql = "SELECT s.*, p.razonsocial , c.nombre, f.iduser, concat(u.nombre, ' ', u.apellido) as 'usuario'
                FROM `soladquisicion` s
                INNER JOIN proveedor p on p.idproveedor = s.proveedor
                INNER JOIN centrocosto c on c.idcentrocosto = s.ccosto
                LEFT JOIN faseaprobacion f on f.idtcentrocosto = c.tcentrocosto and f.orden = s.fase
                LEFT JOIN user u on u.iduser = f.iduser ";
        
        if($solicita != 0){
            $sql.= " WHERE `solicita` = $solicita ";
        }
        
        return ejecutarConsulta($sql);
    }
    
    
    /**
     * Obtiene el listado de solicitudes que estan en estado 1 y estan en proceso de revision
     * el parametro recibido es el usuario actual
     * @param int $revisa
     * @return type
     */
    function listarAprobacion($revisa){
        $sql = "SELECT s.*, p.razonsocial , c.nombre
                FROM `soladquisicion` s
                INNER JOIN proveedor p on p.idproveedor = s.proveedor
                INNER JOIN centrocosto c on c.idcentrocosto = s.ccosto 
                INNER JOIN faseaprobacion f on f.idtcentrocosto = c.tcentrocosto and s.fase = f.orden
                INNER JOIN user u on u.iduser = f.iduser
                Where s.estado = 1 and f.iduser = $revisa ";
        
        return ejecutarConsulta($sql);
    }
    
    /**
     * Avanza la fasede aprobacion, recibe el id de solicitud, el estado y la fase
     * @param int $idsolicitud
     * @param int $estado
     * @param int $fase
     * @return type
     */
    function avanzarFase($idsolicitud, $estado, $fase){
        $sql = "UPDATE `soladquisicion` SET `estado`= $estado,`fase`= $fase WHERE `idsolicitud`= $idsolicitud ";
        return ejecutarConsulta($sql);
    }
    
    /**
     * Actualiza los datos de cotizacion en la solicitu de adquisicion
     * @param int $idsolicitud
     * @param int $proveedor
     * @param int $condpago
     * @param int $moneda
     * @param string $archivo
     * @return type
     */
    function seleccionacotizacion($idsolicitud, $proveedor, $condpago, $moneda, $archivo){
        $sql = "UPDATE `soladquisicion` SET "
                . "`proveedor`= $proveedor,"
                . "`condpago`= $condpago,"
                . "`moneda`= $moneda ,"
                . "`archivo`= '$archivo' "
                . " WHERE `idsolicitud`= $idsolicitud ";
        return ejecutarConsulta($sql);
    }
    
    
    function indicadores($usuario){
        $sql = "Select * from (
                SELECT count(s.idsolicitud) as 'cont', s.estado 
                FROM soladquisicion s 
                Where s.solicita = $usuario 
                GROUP by s.estado
                union
                SELECT count(s.idsolicitud) as 'cont', 'POR REVISAR' as 'estado' 
                FROM `soladquisicion` s 
                INNER JOIN centrocosto c on c.idcentrocosto = s.ccosto 
                INNER JOIN faseaprobacion f on f.idtcentrocosto = c.tcentrocosto and s.fase = f.orden 
                INNER JOIN user u on u.iduser = f.iduser 
                Where s.estado = 1 and f.iduser = $usuario) tabla
                Where cont > 0";
        //var_dump($sql);
        return ejecutarConsulta($sql);
    }
}
