<?php

include_once '../config/conexion.php';

/**
 * Description of Clasificacion_Proyecto
 *
 * @author azuni
 */
class Clasificacion_Proyecto {
    
    function __construct() {
        
    }
    
    function selectClasificacionPro(){
        $sql = "SELECT * FROM tclasificacion WHERE condicion = 1";
        return ejecutarConsulta($sql);
    }
    
}
