<?php 

require "../config/conexion.php";

	Class Equipo{
		//Constructor para instancias
		public function __construct(){

		}

                public function listar(){
		    $sql="SELECT s.idservicio, a.idascensor, a.codigo, e.nombre AS edificio, c.ncontrato AS contrato, q.razon_social, q.rut  FROM servicio s INNER JOIN ascensor a ON s.idascensor=a.idascensor INNER JOIN edificio e ON a.idedificio=e.idedificio INNER JOIN contrato c ON a.idcontrato = c.idcontrato INNER JOIN cliente q ON c.idcliente = q.idcliente ORDER BY s.idservicio ASC";
		    return ejecutarConsulta($sql);
		}

     
	}
?>