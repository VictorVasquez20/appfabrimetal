<?php

require "../config/conexion.php";

Class Presupesto {

    //Constructor para instancias
    public function __construct() {
        
    }

    public function insertar($idascensor, $descripcion, $created_user) {
        $sql = "INSERT INTO `presupuesto`(`idascensor`, `descripcion`,`created_user`) "
                . "VALUES ($idascensor , '$descripcion', $created_user)";
        //var_dump($sql);
        return ejecutarConsulta($sql);
    }

    public function listar_dashpre() {

        $sql = "SELECT DATE_FORMAT(p.created_time, '%d-%m-%Y') AS fecha, p.idpresupuesto, s.idservicio, t.nombre AS tservicio, a.codigo, y.nombre AS tascensor, u.nombre AS estadofin, e.nombre AS edificio, o.nombre AS nomsup, o.apellido AS apesup, p.estado, a.codigocli, a.ubicacion, TIMESTAMPDIFF(DAY, p.created_time, CURRENT_TIMESTAMP) AS antiguedad FROM presupuesto p LEFT JOIN servicio s on p.idservicio=s.idservicio LEFT JOIN tservicio t ON s.idtservicio=t.idtservicio INNER JOIN ascensor a ON s.idascensor = a.idascensor INNER JOIN tascensor y ON a.idtascensor=y.idtascensor INNER JOIN marca m ON a.marca = m.idmarca INNER JOIN modelo n ON a.modelo = n.idmodelo INNER JOIN edificio e ON a.idedificio = e.idedificio LEFT JOIN tecnico i ON s.idtecnico = i.idtecnico LEFT JOIN supervisor o ON i.idsupervisor = o.idsupervisor LEFT JOIN testado u ON s.estadofin = u.id WHERE p.condicion = 1 AND s.estadofin != 7 AND p.estado IN (1,2,3) ORDER BY p.created_time ASC";
        //$sql="SELECT DATE(p.created_time) AS fecha, p.idpresupuesto, s.idservicio, t.nombre AS tservicio, a.codigo, y.nombre AS tascensor, u.nombre AS estadofin, e.nombre AS edificio, o.nombre AS nomsup, o.apellido AS apesup, p.estado, a.codigocli, a.ubicacion, TIMESTAMPDIFF(DAY, p.created_time, CURRENT_TIMESTAMP) AS antiguedad FROM presupuesto p INNER JOIN servicio s on p.idservicio=s.idservicio INNER JOIN tservicio t ON s.idtservicio=t.idtservicio INNER JOIN ascensor a ON s.idascensor = a.idascensor INNER JOIN tascensor y ON a.idtascensor=y.idtascensor INNER JOIN marca m ON a.marca = m.idmarca INNER JOIN modelo n ON a.modelo = n.idmodelo INNER JOIN edificio e ON a.idedificio = e.idedificio INNER JOIN tecnico i ON s.idtecnico = i.idtecnico INNER JOIN supervisor o ON i.idsupervisor = o.idsupervisor INNER JOIN testado u ON s.estadofin = u.id WHERE p.condicion = 1 AND s.estadofin != 7 AND (p.estado = 1 OR p.estado = 2 OR p.estado =3) ORDER BY p.created_time ASC";
        return ejecutarConsulta($sql);
    }

    public function listar_dashdet() {
        $sql = "SELECT DATE_FORMAT(p.created_time, '%d-%m-%Y') AS fecha, p.idpresupuesto, s.idservicio, t.nombre AS tservicio, a.codigo, y.nombre AS tascensor, u.nombre AS estadofin, e.nombre AS edificio, o.nombre AS nomsup, o.apellido AS apesup, p.estado, a.codigocli, a.ubicacion, TIMESTAMPDIFF(DAY, p.created_time, CURRENT_TIMESTAMP) AS antiguedad FROM presupuesto p LEFT JOIN servicio s on p.idservicio=s.idservicio LEFT JOIN tservicio t ON s.idtservicio=t.idtservicio INNER JOIN ascensor a ON p.idascensor = a.idascensor INNER JOIN tascensor y ON a.idtascensor=y.idtascensor INNER JOIN marca m ON a.marca = m.idmarca INNER JOIN modelo n ON a.modelo = n.idmodelo INNER JOIN edificio e ON a.idedificio = e.idedificio LEFT JOIN tecnico i ON s.idtecnico = i.idtecnico LEFT JOIN supervisor o ON i.idsupervisor = o.idsupervisor LEFT JOIN testado u ON s.estadofin = u.id WHERE p.condicion = 1 AND s.estadofin = 7 AND p.estado IN (1,2,3) ORDER BY p.created_time ASC";
        return ejecutarConsulta($sql);
    }

    public function listar_pre($estado,$supervisor,$edificio,$equipo,$estadoequipo) {
        //$sql="SELECT DATE_FORMAT(p.created_time, '%d-%m-%Y') AS created, DATE_FORMAT(p.process_time, '%d-%m-%Y') AS process, DATE_FORMAT(p.revised_time, '%d-%m-%Y') AS revised, DATE_FORMAT(p.aproved_time, '%d-%m-%Y') AS aproved, DATE_FORMAT(p.sent_time, '%d-%m-%Y') AS sent, p.idpresupuesto, p.informado, s.idservicio, a.codigo, u.nombre AS estadofin, e.nombre AS edificio, p.estado, x.nombre AS nomsup, x.apellido AS apesup, a.codigocli, a.ubicacion, p.file, p.fileapro, p.npresupuesto, p.orden FROM presupuesto p INNER JOIN servicio s on p.idservicio=s.idservicio INNER JOIN tservicio t ON s.idtservicio=t.idtservicio INNER JOIN ascensor a ON s.idascensor = a.idascensor INNER JOIN edificio e ON a.idedificio = e.idedificio INNER JOIN testado u ON s.estadofin = u.id INNER JOIN tecnico z ON s.idtecnico=z.idtecnico INNER JOIN supervisor x ON z.idsupervisor = x.idsupervisor WHERE p.condicion = 1 ORDER BY p.created_time ASC";
        $sql = "SELECT DATE_FORMAT(p.created_time, '%d-%m-%Y') AS created, DATE_FORMAT(p.process_time, '%d-%m-%Y') AS process, DATE_FORMAT(p.revised_time, '%d-%m-%Y') AS revised, DATE_FORMAT(p.aproved_time, '%d-%m-%Y') AS aproved, DATE_FORMAT(p.sent_time, '%d-%m-%Y') AS sent, p.idpresupuesto, p.informado, s.idservicio, a.codigo, u.nombre AS estadofin, e.nombre AS edificio, p.estado, IFNULL(x.nombre, us.nombre)  AS nomsup, IFNULL(x.apellido, us.apellido) AS apesup, a.codigocli, a.ubicacion, p.file, p.fileapro, p.npresupuesto, p.orden"
                . " FROM presupuesto p  "
                . " LEFT JOIN servicio s on p.idservicio=s.idservicio "
                . " LEFT JOIN tservicio t ON s.idtservicio=t.idtservicio "
                . " INNER JOIN ascensor a ON p.idascensor = a.idascensor "
                . " INNER JOIN edificio e ON a.idedificio = e.idedificio "
                . " LEFT JOIN testado u ON a.estado = u.id "
                . " LEFT JOIN tecnico z ON p.idtecnico=z.idtecnico "
                . " LEFT JOIN supervisor x ON a.idsup = x.idsupervisor "
                . " LEFT JOIN user us ON us.iduser = p.created_user "
                . " WHERE p.condicion = 1 ";
        if ($estado != 0) {
            $sql .= " AND p.estado =  $estado ";
        }

        /*filtros nuevos*/
        if ($supervisor != '') {
            $sql .= " AND a.idsup =  $supervisor ";
        }
        if ($edificio != '') {
            $sql .= " AND e.idedificio =  $edificio ";
        }
        if ($equipo != '') {
            $sql .= " AND a.codigo =  '$equipo' ";
        }
        if ($estadoequipo != '') {
            $sql .= " AND u.id =  $estadoequipo ";
        }
        $sql .= " ORDER BY p.created_time ASC";
        //echo $sql;
        return ejecutarConsulta($sql);
    }

    public function buscaPresupuestosAntiguos() {
        $sql="SELECT idpresupuesto
                FROM appfabrimetal.presupuesto
                WHERE condicion = 1
                AND estado = 6
                AND DATEDIFF(SYSDATE(),aproved_time) > 7;";
        return Filas($sql);
    }

    function selectano() {
        $sql = "SELECT DISTINCT YEAR(created_time) AS 'ano' FROM `presupuesto` WHERE created_time IS NOT NULL ORDER BY 1 DESC";
        return ejecutarConsulta($sql);
    }

    public function listarpresupuestoFiltro($supervisor,$edificio,$equipo,$estadoequipo) {        
        $sql = "SELECT p.estado, COUNT(p.estado) cantidad
                FROM presupuesto p
                INNER JOIN ascensor a ON p.idascensor = a.idascensor
                INNER JOIN edificio e ON a.idedificio = e.idedificio
                /*LEFT JOIN servicio s on p.idservicio=s.idservicio
                LEFT JOIN testado u ON a.estado = u.id
                LEFT JOIN tservicio t ON s.idtservicio=t.idtservicio
                LEFT JOIN tecnico z ON p.idtecnico=z.idtecnico
                LEFT JOIN supervisor x ON p.idsupervisor = x.idsupervisor
                LEFT JOIN user us ON us.iduser = p.created_user*/
                WHERE p.condicion = 1";
            if ($supervisor != 0) {
                $sql .= " AND a.idsup =  $supervisor ";
            }
            if ($edificio != 0) {
                $sql .= " AND e.idedificio =  $edificio ";
            }
            if ($equipo != '') {
                $sql .= " AND a.codigo =  '$equipo' ";
            }
            if ($estadoequipo != 0) {
                $sql .= " AND a.estado =  $estadoequipo ";
            }
            $sql.=" GROUP BY p.estado";
        //echo $sql;
        return ejecutarConsulta($sql);
    }

    public function listar_preF($estado,$supervisor,$edificio,$equipo,$estadoequipo) {
        //$sql="SELECT DATE_FORMAT(p.created_time, '%d-%m-%Y') AS created, DATE_FORMAT(p.process_time, '%d-%m-%Y') AS process, DATE_FORMAT(p.revised_time, '%d-%m-%Y') AS revised, DATE_FORMAT(p.aproved_time, '%d-%m-%Y') AS aproved, DATE_FORMAT(p.sent_time, '%d-%m-%Y') AS sent, p.idpresupuesto, p.informado, s.idservicio, a.codigo, u.nombre AS estadofin, e.nombre AS edificio, p.estado, x.nombre AS nomsup, x.apellido AS apesup, a.codigocli, a.ubicacion, p.file, p.fileapro, p.npresupuesto, p.orden FROM presupuesto p INNER JOIN servicio s on p.idservicio=s.idservicio INNER JOIN tservicio t ON s.idtservicio=t.idtservicio INNER JOIN ascensor a ON s.idascensor = a.idascensor INNER JOIN edificio e ON a.idedificio = e.idedificio INNER JOIN testado u ON s.estadofin = u.id INNER JOIN tecnico z ON s.idtecnico=z.idtecnico INNER JOIN supervisor x ON z.idsupervisor = x.idsupervisor WHERE p.condicion = 1 ORDER BY p.created_time ASC";
        $sql = "SELECT DATE_FORMAT(p.created_time, '%d-%m-%Y') AS created, DATE_FORMAT(p.process_time, '%d-%m-%Y') AS process, DATE_FORMAT(p.revised_time, '%d-%m-%Y') AS revised, DATE_FORMAT(p.aproved_time, '%d-%m-%Y') AS aproved, DATE_FORMAT(p.sent_time, '%d-%m-%Y') AS sent, p.idpresupuesto, p.informado, s.idservicio, a.codigo, u.nombre AS estadofin, e.nombre AS edificio, p.estado, IFNULL(x.nombre, us.nombre)  AS nomsup, IFNULL(x.apellido, us.apellido) AS apesup, a.codigocli, a.ubicacion, p.file, p.fileapro, p.npresupuesto, p.orden"
                . " FROM presupuesto p  "
                . " LEFT JOIN servicio s on p.idservicio=s.idservicio "
                . " LEFT JOIN tservicio t ON s.idtservicio=t.idtservicio "
                . " INNER JOIN ascensor a ON p.idascensor = a.idascensor "
                . " INNER JOIN edificio e ON a.idedificio = e.idedificio "
                . " LEFT JOIN testado u ON s.estadofin = u.id "
                . " LEFT JOIN tecnico z ON p.idtecnico=z.idtecnico "
                . " LEFT JOIN supervisor x ON a.idsup = x.idsupervisor "
                . " LEFT JOIN user us ON us.iduser = p.created_user "
                . " WHERE p.condicion = 1 "                
                . " AND p.estado in ($estado) ";
        /*filtros nuevos*/
        if ($supervisor != 0) {
            $sql .= " AND a.idsup =  $supervisor ";
        }
        if ($edificio != 0) {
            $sql .= " AND e.idedificio =  $edificio ";
        }
        if ($equipo != 0) {
            $sql .= " AND a.codigo =  $equipo ";
        }
        if ($estadoequipo != 0) {
            $sql .= " AND u.id =  $estadoequipo ";
        }
        
        $sql .= " ORDER BY p.created_time ASC";

        return ejecutarConsulta($sql);
    }

    public function listar_sup($rut) {
        $sql = "SELECT DATE_FORMAT(p.created_time, '%d-%m-%Y') AS created, DATE_FORMAT(p.process_time, '%d-%m-%Y') AS process,  DATE_FORMAT(p.revised_time, '%d-%m-%Y') AS revised, DATE_FORMAT(p.aproved_time, '%d-%m-%Y') AS aproved, DATE_FORMAT(p.sent_time, '%d-%m-%Y') AS sent, p.idpresupuesto, p.informado, s.idservicio, a.codigo, u.nombre AS estadofin, e.nombre AS edificio, p.estado, x.nombre AS nomsup, x.apellido AS apesup, a.codigocli, a.ubicacion, p.file, p.fileapro, p.npresupuesto, p.orden FROM presupuesto p LEFT JOIN servicio s on p.idservicio=s.idservicio INNER JOIN tservicio t ON s.idtservicio=t.idtservicio INNER JOIN ascensor a ON p.idascensor = a.idascensor INNER JOIN edificio e ON a.idedificio = e.idedificio INNER JOIN testado u ON s.estadofin = u.id LEFT JOIN tecnico z ON p.idtecnico=z.idtecnico LEFT JOIN supervisor x ON a.idsup = x.idsupervisor WHERE p.condicion = 1 AND x.rut = '$rut' ORDER BY p.created_time ASC";
        return ejecutarConsulta($sql);
    }

    public function solpdf($idpresupuesto) {
        $sql = "SELECT e.nombre AS edificio, e.calle, e.numero, p.idpresupuesto, s.idservicio, a.codigo, a.codigocli, a.ubicacion, q.nombre AS tascensor, m.nombre AS marca, n.nombre AS modelo, t.nombre AS tservicio, p.created_time, z.nombre AS nomtec, z.apellidos AS apetec, v.nombre AS nomsup, v.apellido AS apesup, i.nombre AS estadofin, p.descripcion, p.solinfo FROM presupuesto p LEFT JOIN servicio s ON p.idservicio=s.idservicio INNER JOIN ascensor a ON p.idascensor = a.idascensor INNER JOIN edificio e ON a.idedificio = e.idedificio INNER JOIN tascensor q ON a.idtascensor = q.idtascensor INNER JOIN marca m ON a.marca=m.idmarca INNER JOIN modelo n ON a.modelo = n.idmodelo INNER JOIN tservicio t ON s.idtservicio = t.idtservicio INNER JOIN testado i ON s.estadofin = i.id LEFT JOIN tecnico z ON p.idtecnico = z.idtecnico LEFT JOIN supervisor v ON p.idsupervisor = v.idsupervisor WHERE p.idpresupuesto = '$idpresupuesto'";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function mostrar($idpresupuesto) {
        $sql = "SELECT p.idpresupuesto, a.idascensor ,a.codigo, q.nombre AS tascensor, m.nombre AS marca, n.nombre AS modelo, p.created_time, p.descripcion, p.descripcionrev, p.descripcionpre, e.nombre, p.solinfo FROM presupuesto p LEFT JOIN servicio s ON p.idservicio=s.idservicio INNER JOIN ascensor a ON p.idascensor = a.idascensor INNER JOIN edificio e ON a.idedificio = e.idedificio INNER JOIN tascensor q ON a.idtascensor = q.idtascensor INNER JOIN marca m ON a.marca=m.idmarca INNER JOIN modelo n ON a.modelo = n.idmodelo WHERE p.idpresupuesto = '$idpresupuesto'";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function mostrarpre($idpresupuesto) {
        $sql = "SELECT p.idpresupuesto, a.codigo, q.nombre AS tascensor, m.nombre AS marca, n.nombre AS modelo, p.created_time, p.descripcion, p.descripcionrev, e.nombre FROM presupuesto p LEFT JOIN servicio s ON p.idservicio=s.idservicio INNER JOIN ascensor a ON p.idascensor = a.idascensor INNER JOIN edificio e ON a.idedificio = e.idedificio INNER JOIN tascensor q ON a.idtascensor = q.idtascensor INNER JOIN marca m ON a.marca=m.idmarca INNER JOIN modelo n ON a.modelo = n.idmodelo WHERE p.idpresupuesto = '$idpresupuesto'";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function mostrarsol($idpresupuesto) {
        $sql = "SELECT p.idpresupuesto, a.codigo, q.nombre AS tascensor, m.nombre AS marca, n.nombre AS modelo, p.created_time, p.descripcion, e.nombre FROM presupuesto p LEFT JOIN servicio s ON p.idservicio=s.idservicio INNER JOIN ascensor a ON p.idascensor = a.idascensor INNER JOIN edificio e ON a.idedificio = e.idedificio INNER JOIN tascensor q ON a.idtascensor = q.idtascensor INNER JOIN marca m ON a.marca=m.idmarca INNER JOIN modelo n ON a.modelo = n.idmodelo WHERE p.idpresupuesto = '$idpresupuesto'";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function revisar($idpresupuesto, $descripcionrev, $iduser) {
        $sql = "UPDATE presupuesto SET descripcionrev = UPPER('$descripcionrev'), revised_time=CURRENT_TIMESTAMP, revised_user='$iduser', estado='3', informado='1'  WHERE idpresupuesto='$idpresupuesto'";
        return ejecutarConsulta($sql);
    }

    public function foto($idpresupuesto, $file, $base64) {
        $sql = "INSERT INTO imagenespre(idpresupuesto, file, base64) VALUES ('$idpresupuesto','$file','$base64')";
        return ejecutarConsulta($sql);
    }

    public function imgref($idpresupuesto) {
        $sql = "SELECT file, base64 FROM imagenespre WHERE idpresupuesto='$idpresupuesto'";
        return ejecutarConsulta($sql);
    }

    public function enviado($idpresupuesto) {
        $sql = "UPDATE presupuesto SET estado='5', sent_time=CURRENT_TIMESTAMP WHERE idpresupuesto='$idpresupuesto'";
        return ejecutarConsulta($sql);
    }

    public function informacion($idpresupuesto, $solinfo, $iduser) {
        $sql = "UPDATE presupuesto SET solinfo = UPPER('$solinfo'), estado='2', request_time=CURRENT_TIMESTAMP, request_user='$iduser' WHERE idpresupuesto='$idpresupuesto'";
        return ejecutarConsulta($sql);
    }

    public function procesar($idpresupuesto, $descripcionpre, $file, $npresupuesto, $iduser) {
        $sql = "UPDATE presupuesto SET descripcionpre = UPPER('$descripcionpre'), file='$file', npresupuesto='$npresupuesto', process_time=CURRENT_TIMESTAMP, process_user='$iduser', estado='4' WHERE idpresupuesto='$idpresupuesto'";
        return ejecutarConsulta($sql);
    }

    public function aprobar($idpresupuesto, $descripcionapro, $fileapro, $orden) {
        $sql = "UPDATE presupuesto SET descripcionapro = UPPER('$descripcionapro'), fileapro='$fileapro', orden='$orden', aproved_time=CURRENT_TIMESTAMP, estado='6' WHERE idpresupuesto='$idpresupuesto'";
        return ejecutarConsulta($sql);
    }

    public function revpdf($idpresupuesto) {
        $sql = "SELECT e.nombre AS edificio, e.calle, e.numero, p.idpresupuesto, s.idservicio, a.codigo, a.codigocli, a.ubicacion, q.nombre AS tascensor, m.nombre AS marca, n.nombre AS modelo, t.nombre AS tservicio, p.created_time, z.nombre AS nomtec, z.apellidos AS apetec, v.nombre AS nomsup, v.apellido AS apesup, i.nombre AS estadofin, p.descripcion, p.descripcionrev, p.revised_time, u.nombre AS revnom, u.apellido AS revape, p.npresupuesto, p.orden, p.process_time, p.sent_time, p.aproved_time, p.descripcionpre, p.descripcionapro, p.solinfo FROM presupuesto p LEFT JOIN servicio s ON p.idservicio=s.idservicio INNER JOIN ascensor a ON p.idascensor = a.idascensor INNER JOIN edificio e ON a.idedificio = e.idedificio INNER JOIN tascensor q ON a.idtascensor = q.idtascensor INNER JOIN marca m ON a.marca=m.idmarca INNER JOIN modelo n ON a.modelo = n.idmodelo INNER JOIN tservicio t ON s.idtservicio = t.idtservicio INNER JOIN testado i ON s.estadofin = i.id LEFT JOIN tecnico z ON p.idtecnico = z.idtecnico LEFT JOIN supervisor v ON p.idsupervisor = v.idsupervisor INNER JOIN user u ON p.revised_user = u.iduser WHERE p.idpresupuesto = '$idpresupuesto'";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function email($idpresupuesto) {
        $sql = "SELECT p.idpresupuesto, s.idservicio, s.created_time AS feser, z.nombre AS estadofin, t.nombre AS nomtec, t.apellidos AS apetec, s.observacionfin, a.codigo, a.ubicacion, a.codigocli, q.nombre AS tascensor, m.nombre AS marca, n.nombre AS modelo, p.created_time, p.descripcion, e.nombre, e.calle, e.numero, p.solinfo, x.nombre AS nomreq, x.apellido AS apereq, p.request_time FROM presupuesto p LEFT JOIN servicio s ON p.idservicio=s.idservicio INNER JOIN ascensor a ON p.idascensor = a.idascensor INNER JOIN edificio e ON a.idedificio = e.idedificio INNER JOIN tascensor q ON a.idtascensor = q.idtascensor INNER JOIN marca m ON a.marca=m.idmarca INNER JOIN modelo n ON a.modelo = n.idmodelo INNER JOIN testado z ON s.estadofin = z.id LEFT JOIN tecnico t ON p.idtecnico = t.idtecnico INNER JOIN user x ON p.request_user = x.iduser WHERE p.idpresupuesto = '$idpresupuesto'";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function inhabilitar($idpresupuesto, $iduser, $motivoCierre, $comentario) {
        $sql = "UPDATE presupuesto SET closed_user='$iduser', closed_time=CURRENT_TIMESTAMP, condicion=0, motivo_inhabilitacion = $motivoCierre, comentario_inhabilitacion = UPPER('$comentario') WHERE idpresupuesto='$idpresupuesto'";
        return ejecutarConsulta($sql);
    }

    public function habilitar($idpresupuesto) {
        $sql = "UPDATE presupuesto SET closed_user= null, closed_time= null, condicion=1, motivo_inhabilitacion = null, comentario_inhabilitacion = null WHERE idpresupuesto='$idpresupuesto'";
        return ejecutarConsulta($sql);
    }

    /*public function regresar($idpresupuesto) {
        $sql = "UPDATE presupuesto SET closed_user='$iduser', closed_time=CURRENT_TIMESTAMP, condicion=0 WHERE idpresupuesto='$idpresupuesto'";
        return ejecutarConsulta($sql);
    }*/

    public function EmailSup($idpresupuesto) {
        $sql = "SELECT sup.email_interno AS email FROM presupuesto p INNER JOIN supervisor sup ON p.idsupervisor = sup.idsupervisor WHERE p.idpresupuesto='$idpresupuesto'";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function listar_preHistorico($mes, $anio) {
        //$sql="SELECT DATE_FORMAT(p.created_time, '%d-%m-%Y') AS created, DATE_FORMAT(p.process_time, '%d-%m-%Y') AS process, DATE_FORMAT(p.revised_time, '%d-%m-%Y') AS revised, DATE_FORMAT(p.aproved_time, '%d-%m-%Y') AS aproved, DATE_FORMAT(p.sent_time, '%d-%m-%Y') AS sent, p.idpresupuesto, p.informado, s.idservicio, a.codigo, u.nombre AS estadofin, e.nombre AS edificio, p.estado, x.nombre AS nomsup, x.apellido AS apesup, a.codigocli, a.ubicacion, p.file, p.fileapro, p.npresupuesto, p.orden FROM presupuesto p INNER JOIN servicio s on p.idservicio=s.idservicio INNER JOIN tservicio t ON s.idtservicio=t.idtservicio INNER JOIN ascensor a ON s.idascensor = a.idascensor INNER JOIN edificio e ON a.idedificio = e.idedificio INNER JOIN testado u ON s.estadofin = u.id INNER JOIN tecnico z ON s.idtecnico=z.idtecnico INNER JOIN supervisor x ON z.idsupervisor = x.idsupervisor WHERE p.condicion = 1 ORDER BY p.created_time ASC";
        $sql = "SELECT DATE_FORMAT(p.created_time, '%d-%m-%Y') AS created, DATE_FORMAT(p.process_time, '%d-%m-%Y') AS process, DATE_FORMAT(p.revised_time, '%d-%m-%Y') AS revised, DATE_FORMAT(p.aproved_time, '%d-%m-%Y') AS aproved, DATE_FORMAT(p.sent_time, '%d-%m-%Y') AS sent, p.idpresupuesto, p.informado, s.idservicio, a.codigo, u.nombre AS estadofin, e.nombre AS edificio, p.estado, IFNULL(x.nombre, us.nombre)  AS nomsup, IFNULL(x.apellido, us.apellido) AS apesup, a.codigocli, a.ubicacion, p.file, p.fileapro, p.npresupuesto, p.orden, p.comentario_inhabilitacion"
                . " FROM presupuesto p  "
                . " LEFT JOIN servicio s on p.idservicio=s.idservicio "
                . " LEFT JOIN tservicio t ON s.idtservicio=t.idtservicio "
                . " INNER JOIN ascensor a ON p.idascensor = a.idascensor "
                . " INNER JOIN edificio e ON a.idedificio = e.idedificio "
                . " LEFT JOIN testado u ON s.estadofin = u.id "
                . " LEFT JOIN tecnico z ON p.idtecnico=z.idtecnico "
                . " LEFT JOIN supervisor x ON p.idsupervisor = x.idsupervisor "
                . " LEFT JOIN user us ON us.iduser = p.created_user "
                . " WHERE p.condicion = 0 ";
        if ($anio != 0){
            $sql .= " AND YEAR(p.created_time) = $anio ";
        }
        if ($mes != 0){
            $sql .= " AND MONTH(p.created_time) = $mes ";
        }
        $sql .= " ORDER BY p.created_time ASC";
        
        return ejecutarConsulta($sql);
    }

    public function listaInfoCierre($idpresupuesto) {
        $sql = "SELECT p.motivo_inhabilitacion, 
                                p.comentario_inhabilitacion, 
                                concat(u.nombre, ' ', u.apellido) user_cierre, 
                                DATE_FORMAT(p.closed_time , '%d-%m-%Y %H:%i') closed_time        
                        FROM presupuesto p
                        LEFT JOIN user u on p.closed_user = u.iduser
                        where p.idpresupuesto = $idpresupuesto;";
        //echo $sql;
        return ejecutarConsulta($sql); 
    }

    public function listarSupervisor(){
        $sql = "SELECT s.idsupervisor AS 'idsupervisor', CONCAT(s.nombre,' ',s.apellido) AS 'nombre' FROM presupuesto p INNER JOIN ascensor a ON p.idascensor = a.idascensor INNER JOIN supervisor s ON s.idsupervisor= a.idsup WHERE p.condicion = 1 GROUP BY s.idsupervisor ORDER BY 2 ASC";
        return ejecutarConsulta($sql);
    }

    public function listarEdificio($supervisor){
        $sql = "SELECT e.idedificio AS 'idedificio', e.nombre AS 'nombre' FROM presupuesto p INNER JOIN ascensor a ON a.idascensor = p.idascensor INNER JOIN edificio e ON a.idedificio = e.idedificio WHERE p.condicion = 1 AND p.idsupervisor='$supervisor' GROUP BY e.idedificio";
        return ejecutarConsulta($sql);
    }

    public function listarEquipo($supervisor,$edificio){
        $sql = "SELECT a.codigo AS 'idascensor', a.codigo AS 'nombre' FROM presupuesto p INNER JOIN ascensor a ON a.idascensor = p.idascensor WHERE p.idsupervisor='$supervisor' AND a.idedificio = '$edificio' AND p.condicion = 1 GROUP BY a.idascensor";
        return ejecutarConsulta($sql);
    }

    public function listarEstadoEquipo(){
        $sql = "SELECT u.id AS 'idestado', u.nombre AS 'nombre' FROM presupuesto p INNER JOIN ascensor a ON a.idascensor= p.idascensor INNER JOIN testado u ON a.estado = u.id WHERE p.condicion = 1 GROUP BY u.id";
        return ejecutarConsulta($sql);
    }

}

?>