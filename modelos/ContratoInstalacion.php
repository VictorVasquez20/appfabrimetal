<?php
require_once '../config/conexion.php';
/**
 * Description of ContratoInstalacion
 *
 * @author azuni
 */
class ContratoInstalacion {
    //put your code here
    function __construct() {
        
    }
    
    function Insertar($idcliente, $idperiocidad, $tcontrato, $idregiones,  $idcomunas, $ncontrato, $nexterno, $nreferencia, $ubicacion, $fecha, $fecha_ini, $fecha_fin, $rautomatica, $observaciones){
        $sql = "INSERT INTO `contrato`(`idcliente`, `idperiocidad`, `tcontrato`, `idregiones`, `idcomunas`, `ncontrato`, `nexterno`, `nreferencia`, `ubicacion`, `fecha`, `fecha_ini`, `fecha_fin`, `rautomatica`, `observaciones`) "
             . " VALUES ($idcliente, $idperiocidad, $tcontrato, $idregiones, $idcomunas, '$ncontrato','$nexterno', '$nreferencia', '$ubicacion', '$fecha', '$fecha_ini','$fecha_fin', '$rautomatica', '$observaciones')";
        return ejecutarConsulta_retornarID($sql);
    }
    
    function Listar(){
        /*$sql = "SELECT p.idproyecto, p.idventa,  p.codigo, p.nombre, p.calle, p.numero, p.idregion, r.region_nombre, p.estado, e.nombre as 'nombestado', e.color, i.codigo as 'codigoimp', v.idmandante, m.razon_social, "
            . "(SELECT count(*) FROM ascensor a WHERE a.idventa = p.idventa) as 'ascensores' "
            . "FROM `proyecto` p  "
            . "INNER JOIN venta v ON v.idventa = p.idventa "
            . "INNER JOIN importacion i ON i.idimportacion = v.idimportacion "
            . "LEFT JOIN mandante m on m.idmandante = v.idmandante "
            . "LEFT JOIN regiones r on r.region_id =  p.idregion "
            . "INNER JOIN estadopro e on e.idestadopro = p.estado "
            . "WHERE p.estado in (11,12, 13) and (SELECT count(*) FROM ascensor a WHERE a.idventa = p.idventa and a.idcontrato is null) != 0";*/
        $sql = "SELECT p.idproyecto, p.idventa,  p.codigo, p.nombre, p.calle, p.numero, p.idregion, r.region_nombre, p.estado, e.nombre as 'nombestado', e.color, i.codigo as 'codigoimp', v.idmandante, m.razon_social, "
            . "    (SELECT count(*) FROM ascensor a WHERE a.idventa = p.idventa) as 'ascensores' "
            . "    ,(SELECT count(*) FROM ascensor a1 WHERE a1.idventa = p.idventa and a1.estadoins in (12)) as 'ascensoresDefinitiva' "
            . "    ,(SELECT count(*) FROM ascensor a1 WHERE a1.idventa = p.idventa and a1.estadoins in (12) and a1.idcontrato is null) as 'ascensoresScontrato' "
            . "    FROM `proyecto` p  "
            . "    INNER JOIN venta v ON v.idventa = p.idventa "
            . "    INNER JOIN importacion i ON i.idimportacion = v.idimportacion "
            . "    LEFT JOIN mandante m on m.idmandante = v.idmandante "
            . "    LEFT JOIN regiones r on r.region_id =  p.idregion "
            . "    INNER JOIN estadopro e on e.idestadopro = p.estado "
            . "    WHERE p.idventa in (SELECT DISTINCT idventa FROM `ascensor` Where estadoins = 12 and idcontrato is null)";
        return ejecutarConsulta($sql);
    }
    
    function Mostrar($idproyecto){
        $sql = "Select * from proyecto Where idproyecto = $idproyecto";
        return ejecutarConsultaSimpleFila($sql);
    }
    
    function listaAscensor($idptoyecto){
        $sql = "SELECT a.* , mar.nombre as 'strmarca', st.nombre as 'strestado', st.color , ta.nombre as 'tiponomb', p.idproyecto "
            . "FROM `ascensor` as a "
            . "INNER JOIN marca as mar on mar.idmarca = a.marca "
            . "INNER JOIN estadopro as st on st.estado = a.estadoins "
            . "INNER JOIN tascensor as ta on ta.idtascensor = a.idtascensor "
            . "INNER JOIN proyecto as p on p.idventa = a.idventa "
            . "Where p.idproyecto = $idptoyecto and a.estadoins = 12 and idcontrato is null";
        return ejecutarConsulta($sql);
    }
    
    public function editarAscensorContrato($iduser, $idascensor, $idedificio, $idcontrato, $ubicacion, $codigocli, $pservicio, $gtecnica, $condicion, $updated_user){
        $sql = "UPDATE `ascensor` "
                . "SET `iduser` = $iduser, "
                . "`idedificio`= $idedificio,"
                . "`idcontrato`= $idcontrato,"
                . "`ubicacion`= '$ubicacion',"
                . "`codigocli`='$codigocli',"
                . "`pservicio`= '$pservicio',"
                . "`gtecnica`= '$gtecnica',"
                . "`condicion` = $condicion, "
                . "`updated_time` = CURRENT_TIMESTAMP ,"
                . "`updated_user` = $updated_user "
                . "WHERE `idascensor`= $idascensor";
        return ejecutarConsulta($sql);
    }
    
    function revisar($idproyecto){
        $sql = "Select p.*,DATE(p.created_time) as fecha, DATE(p.closed_time) as fechafin, e.nombre as 'estadonomb', c.codigo as 'ccnomb', m.razon_social, m.rut "
            . "FROM proyecto p "
            . "INNER JOIN estadopro e on e.estado = p.estado "
            . "INNER JOIN venta ve ON p.idventa = ve.idventa "
            . "INNER JOIN mandante m ON m.idmandante = ve.idmandante "
            . "INNER JOIN centrocosto c on ve.idcentrocosto = c.idcentrocosto "
            . "LEFT JOIN visita v on p.idproyecto = v.idproyecto and v.idestado = p.estado "
            . "WHERE p.idproyecto = $idproyecto "
            . "GROUP BY v.idascensor LIMIT 1";
        return ejecutarConsultaSimpleFila($sql);
    }
    
    function validarContrato($ncontrato){
        $sql = "SELECT * FROM `contrato` WHERE ncontrato = '$ncontrato'";
        return Filas($sql);
    }
}
