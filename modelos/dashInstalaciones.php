<?php
require_once '../config/conexion.php';
/**
 * Description of DashInstalaciones
 *
 * @author azuni
 */
class DashInstalaciones {
    //put your code here
    function __construct() {
        
    }
    
    function instalacionesXestado(){
        $sql = "Select e.idestadopro, e.nombre, count(*) as 'num', (Select count(*) FROM ascensor Where estadoins < 12) as equipos
                from ascensor a
                INNER JOIN estadopro e on e.idestadopro = a.estadoins 
                WHERE a.estadoins < 12
                GROUP BY e.idestadopro";
        return ejecutarConsulta($sql);
    }
    
    
    function visitasXobra(){
        $sql = "Select p.idsupervisor, concat(s.nombre, ' ', s.apellido) as 'sup' , count(*) as 'proy', (SELECT COUNT(*) FROM visita v WHERE v.created_user = u.iduser) as 'visitas'
                , (SELECT COUNT(*) FROM proyecto pro WHERE pro.idsupervisor = s.idsupervisorins and pro.estado = 12) as 'terminados' 
                from proyecto p 
                INNER JOIN supervisorins s ON s.idsupervisorins = p.idsupervisor 
                INNER JOIN user u on u.num_documento = s.rut
                WHERE p.estado < 12 GROUP BY p.idsupervisor";
        return ejecutarConsulta($sql);
    }
    
    function estadosProyectosPm($idpm) {
        
          $sql="SELECT a.idestadopro,
                a.nombre as nombre_estado,
                CASE
                WHEN a.idestadopro = 2 THEN 
                ( SELECT COUNT(1) from proyecto as c where c.estado= a.idestadopro and c.idpm=$idpm and c.tipoproyecto=1
                 AND EXISTS
                (select 1 from visita as d where d.idproyecto=c.idproyecto)
                )
                ELSE
                (SELECT COUNT(1) from proyecto as b where b.estado= a.idestadopro and b.idpm=$idpm and b.tipoproyecto=1 )
                END
                as cantidad,
                
                ROUND(((
                CASE
                WHEN a.idestadopro = 2 THEN 
                ( SELECT COUNT(1) from proyecto as e where e.estado= a.idestadopro and e.idpm=$idpm and e.tipoproyecto=1
                 AND EXISTS
                (select 1 from visita as f where f.idproyecto=e.idproyecto)
                )
                ELSE
                (SELECT COUNT(1) from proyecto as g where g.estado= a.idestadopro and g.idpm=$idpm and g.tipoproyecto=1)
                END
                )
                /
                (SELECT COUNT(1) from proyecto as h where h.idpm=$idpm  and h.tipoproyecto=1))*100,2) 
                as porcentaje

                FROM estadopro as a
                WHERE a.idestadopro  <= 12
                GROUP BY a.idestadopro
                ORDER BY a.idestadopro";
        
               return ejecutarConsulta($sql);
    }
    
    function estapaIniciadoProyectosPm($idpm) {
        $sql="SELECT 2 as idestadopro,'INICIADO' as nombre_estado, 
                COUNT(a.idproyecto) as cantidad ,
                ROUND((COUNT(a.idproyecto)/(select count(1) from proyecto where idpm=$idpm and tipoproyecto=1))*100,2) as porcentaje
                FROM proyecto as a 
                WHERE a.idpm=$idpm 
                AND a.estado=2
                AND a.tipoproyecto=1
                AND NOT EXISTS(select 1 FROM visita as b WHERE b.idproyecto=a.idproyecto)";

        return ejecutarConsulta($sql);
    }
    
    function visitasProyectoPm($idpm) {
        
        $sql="SELECT b.idpm,a.idsupervisorins,
                concat(a.nombre,', ',a.apellido) as nombre_supervisor,
                count(b.idproyecto) as total_proyectos,

                (select count(idascensor) 
                 from ascensor as d
                 INNER JOIN venta as e on e.idventa = d.idventa
                 INNER JOIN proyecto as f on f.idventa = e.idventa
                 where f.idsupervisor=b.idsupervisor
                 and f.idpm = b.idpm
                ) as cantidad_ascensores,

                (select SUM(paradas) 
                 from ascensor as g
                 INNER JOIN venta as h on g.idventa = h.idventa
                 INNER JOIN proyecto as i on i.idventa = h.idventa
                 where i.idsupervisor=a.idsupervisorins
                 and i.idpm = b.idpm
                ) as cantidad_paradas,

                (
                SELECT SUM(l.visitas_proyecto) 
                from (
                    SELECT k.idpm,k.idsupervisor,k.idproyecto,
                    COUNT(j.idvisita)/COUNT(distinct j.idascensor) as visitas_proyecto
                    FROM visita as j
                    INNER JOIN proyecto as k on j.idproyecto=k.idproyecto
                    GROUP by  k.idpm,k.idsupervisor,k.idproyecto
                ) as l
                where l.idpm= b.idpm
                and l.idsupervisor= b.idsupervisor
                ) AS visitas_proyectos,
                
                (
                SELECT COUNT(1)  FROM (
                    SELECT n.idpm,n.idsupervisor,YEAR(m.created_time) as anio,MONTH(m.created_time) as mes
                    FROM visita as m
                    INNER JOIN proyecto as n on m.idproyecto=n.idproyecto
                    GROUP by n.idpm,n.idsupervisor ,YEAR(m.created_time),MONTH(m.created_time)
                    ORDER BY n.idpm,n.idsupervisor ,YEAR(m.created_time),MONTH(m.created_time)
                    ) as o
                    where o.idpm = b.idpm
                    and o.idsupervisor=b.idsupervisor
                ) as cantidad_meses,
                
                (
                SELECT sum(r.visita_ascensores)
                from (
                    SELECT q.idpm,q.idsupervisor,q.idproyecto, COUNT(p.idvisita) as visita_ascensores
                    FROM visita as p
                    INNER JOIN proyecto as q on p.idproyecto=q.idproyecto
                    GROUP by  q.idpm,q.idsupervisor,q.idproyecto
                ) as r
                where r.idpm=b.idpm
                and r.idsupervisor= b.idsupervisor

                ) as visitas_ascensores
                
                from supervisorins as a
                INNER JOIN proyecto as b on a.idsupervisorins = b.idsupervisor
                INNER JOIN user as c on c.num_documento = a.rut
                where b.idpm=$idpm
                GROUP BY a.idsupervisorins";
        
         return ejecutarConsulta($sql);
    }
}
