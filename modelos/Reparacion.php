<?php 

require "../config/conexion.php";

	Class Reparacion{
		//Constructor para instancias
		public function __construct(){

		}
                
		public function SolReparacion($idpresupuesto, $idascensor, $iduser){
			$sql="INSERT INTO reparacion (idpresupuesto, idascensor, created_user) VALUES ('$idpresupuesto','$idascensor','$iduser')";
			return ejecutarConsulta($sql);
		}
                
		public function listar_rep(){
			$sql="SELECT r.idreparacion, p.idpresupuesto, e.nombre AS edificio, a.codigocli, a.ubicacion, a.codigo, r.idsupervisor, 
					s.nombre, s.apellido, DATE(r.created_time) AS created_time, r.estado, IFNULL(DATE(r.update_time),DATE(r.created_time)) AS update_time,
					mp.descripcion motivopend, te.nombre estado_asc, r.tipo_reparacion,p.file AS file, p.fileapro AS fileapro
					FROM reparacion r 
					INNER JOIN presupuesto p ON r.idpresupuesto = p.idpresupuesto 
					INNER JOIN ascensor a ON r.idascensor=a.idascensor 
					INNER JOIN edificio e ON a.idedificio=e.idedificio 
					INNER JOIN testado te ON a.estado = te.id 
					LEFT JOIN user s ON r.idsupervisor = s.iduser
					LEFT JOIN motivopend mp ON r.motivo_pendiente = mp.idmotivopend";
			return ejecutarConsulta($sql);
		}

		public function listarHist($idreparacion){
			$sql="SELECT CONCAT(u.nombre, ' ', u.apellido) nombre, rh.fh_insert, rh.estado_act, rh.comentario 
					FROM reparacion_historial rh
					INNER JOIN user u ON rh.id_user = u.iduser
					WHERE rh.idreparacion = $idreparacion;";
			return ejecutarConsulta($sql);
		}

		public function guardaTipoRep($idreparacion, $idTipoRep){
			$sql="UPDATE reparacion SET tipo_reparacion = $idTipoRep
					WHERE idreparacion = $idreparacion;";
			//echo $sql;
			return ejecutarConsulta($sql);
		}

		public function listarMotivoPendiente(){
			$sql="SELECT idmotivopend, descripcion FROM motivopend WHERE condicion = 1;";
			return ejecutarConsulta($sql);
		}

		public function listarResponsables(){
			$sql="SELECT idsupervisor iduser, CONCAT(nombre, ' ', apellido, ' - ', rut) supervisor  
					FROM supervisor 
					WHERE idcargosup in (2,7)
					ORDER BY  2 ASC;";
			return ejecutarConsulta($sql);
		}

		public function dejarPendiente($idreparacion, $motivoPendiente, $comentarioPendiente){
			$sql="UPDATE reparacion SET estado = 2, motivo_pendiente = $motivoPendiente, comentario_pendiente = UPPER('$comentarioPendiente'), update_time = sysdate() 
					WHERE idreparacion = $idreparacion;";
			//echo $sql;
			return ejecutarConsulta($sql);
		}

		public function pasarPorPlanificar($idreparacion){
			$sql="UPDATE reparacion SET estado = 3, update_time = sysdate() WHERE idreparacion = $idreparacion;";
			//echo $sql;
			return ejecutarConsulta($sql);
		}

		public function programarRepa($idreparacion, $responsable, $prioridad, $inicio, $comentario){
			$sql="UPDATE reparacion SET estado = 4, comentario_progra = UPPER('$comentario'), idsupervisor = $responsable, prioridad = $prioridad,
					inicio_reparacion = '$inicio', update_time = sysdate()
				  WHERE idreparacion = $idreparacion;";
			//echo $sql;
			return ejecutarConsulta($sql);
		}

		public function ejecutar($idreparacion, $comentario){
			$sql="UPDATE reparacion SET estado = 5, comentario_cierre = UPPER('$comentario'), update_time = sysdate()
				  WHERE idreparacion = $idreparacion;";
			//echo $sql;
			return ejecutarConsulta($sql);
		}

		public function volverAPlanificar($idreparacion){
			$sql="UPDATE reparacion SET estado = 7, update_time = sysdate()
				  WHERE idreparacion = $idreparacion;";
			//echo $sql;
			return ejecutarConsulta($sql);
		}

		public function facturar($idreparacion, $estado){
			$sql="UPDATE reparacion SET estado = $estado, update_time = sysdate()
				  WHERE idreparacion = $idreparacion;";
			//echo $sql;
			return ejecutarConsulta($sql);
		}

		public function dejarPendienteHist($iduser, $idreparacion, $comentarioPendiente){
			$sql="INSERT INTO reparacion_historial (idreparacion, fh_insert, estado_act, comentario, id_user)
					VALUES ($idreparacion, sysdate(), 2, UPPER('$comentarioPendiente'), $iduser);";
			//echo $sql;
			return ejecutarConsulta($sql);
		}

		public function pasarPorPlanificarHist($iduser, $idreparacion, $comentario){
			$sql="INSERT INTO reparacion_historial (idreparacion, fh_insert, estado_act, comentario, id_user)
					VALUES ($idreparacion, sysdate(), 3, UPPER('$comentario'), $iduser);";
			//echo $sql;
			return ejecutarConsulta($sql);
		}

		public function programarRepaHist($iduser, $idreparacion, $comentario){
			$sql="INSERT INTO reparacion_historial (idreparacion, fh_insert, estado_act, comentario, id_user)
					VALUES ($idreparacion, sysdate(), 4, UPPER('$comentario'), $iduser);";
			//echo $sql;
			return ejecutarConsulta($sql);
		}

		public function ejecutarHist($iduser, $idreparacion, $comentario){
			$sql="INSERT INTO reparacion_historial (idreparacion, fh_insert, estado_act, comentario, id_user)
					VALUES ($idreparacion, sysdate(), 5, UPPER('$comentario'), $iduser);";
			//echo $sql;
			return ejecutarConsulta($sql);
		}

		public function volverAPlanificarHist($iduser, $idreparacion, $comentario){
			$sql="INSERT INTO reparacion_historial (idreparacion, fh_insert, estado_act, comentario, id_user)
					VALUES ($idreparacion, sysdate(), 7, UPPER('$comentario'), $iduser);";
			//echo $sql;
			return ejecutarConsulta($sql);
		}

		public function facturarHist($iduser, $idreparacion, $estado, $comentario){
			$sql="INSERT INTO reparacion_historial (idreparacion, fh_insert, estado_act, comentario, id_user)
					VALUES ($idreparacion, sysdate(), $estado, UPPER('$comentario'), $iduser);";
			//echo $sql;
			return ejecutarConsulta($sql);
		}

		public function cuentaHist($idreparacion){
		    $sql="SELECT idreparacion_historial FROM reparacion_historial WHERE idreparacion=$idreparacion";
		    return NumeroFilas($sql);
		}

		public function listar_eventos($start,$end){
			$sql="SELECT r.idreparacion, r.estado, p.npresupuesto, r.inicio_reparacion, ADDDATE(r.inicio_reparacion, INTERVAL 1 hour) as 'endtime', a.codigo , e.nombre AS 'nomEdificio', CONCAT(e.calle,' #',e.numero,', ',c.comuna_nombre,', ',re.region_nombre) AS 'direccion' FROM reparacion r INNER JOIN presupuesto p ON r.idpresupuesto = p.idpresupuesto INNER JOIN ascensor a ON r.idascensor= a.idascensor INNER JOIN edificio e ON a.idedificio= e.idedificio INNER JOIN comunas c ON e.idcomunas= c.comuna_id INNER JOIN regiones re ON e.idregiones= re.region_id WHERE r.inicio_reparacion BETWEEN CAST('$start' AS DATE) AND CAST('$end' AS DATE)";
			return ejecutarConsulta($sql);
		}

		public function detalle_rep($id){
			$sql="SELECT r.idreparacion, r.estado, p.npresupuesto, r.inicio_reparacion, a.codigo,a.codigo, e.nombre AS 'nomEdificio', CONCAT(e.calle,' #',e.numero,', ',c.comuna_nombre,', ',re.region_nombre) AS 'direccion' FROM reparacion r INNER JOIN presupuesto p ON r.idpresupuesto = p.idpresupuesto INNER JOIN ascensor a ON r.idascensor= a.idascensor INNER JOIN edificio e ON a.idedificio= e.idedificio INNER JOIN comunas c ON e.idcomunas= c.comuna_id INNER JOIN regiones re ON e.idregiones= re.region_id WHERE r.idreparacion = '$id'";
			return ejecutarConsultaSimpleFila($sql);
		}
                
                /*
                public function listar_dashpre(){
			$sql="SELECT DATE(p.created_time) AS fecha, p.idpresupuesto, s.idservicio, t.nombre AS tservicio, a.codigo, y.nombre AS tascensor, u.nombre AS estadofin, e.nombre AS edificio, o.nombre AS nomsup, o.apellido AS apesup, p.estado, a.codigocli, a.ubicacion, TIMESTAMPDIFF(DAY, p.created_time, CURRENT_TIMESTAMP) AS antiguedad FROM presupuesto p INNER JOIN servicio s on p.idservicio=s.idservicio INNER JOIN tservicio t ON s.idtservicio=t.idtservicio INNER JOIN ascensor a ON s.idascensor = a.idascensor INNER JOIN tascensor y ON a.idtascensor=y.idtascensor INNER JOIN marca m ON a.marca = m.idmarca INNER JOIN modelo n ON a.modelo = n.idmodelo INNER JOIN edificio e ON a.idedificio = e.idedificio INNER JOIN tecnico i ON s.idtecnico = i.idtecnico INNER JOIN supervisor o ON i.idsupervisor = o.idsupervisor INNER JOIN testado u ON s.estadofin = u.id WHERE p.condicion = 1 AND s.estadofin != 7 AND (p.estado = 1 OR p.estado = 2 OR p.estado =3) ORDER BY p.created_time ASC";
			return ejecutarConsulta($sql);
		}
                
                public function listar_dashdet(){
			$sql="SELECT DATE(p.created_time) AS fecha, p.idpresupuesto, s.idservicio, t.nombre AS tservicio, a.codigo, y.nombre AS tascensor, u.nombre AS estadofin, e.nombre AS edificio, o.nombre AS nomsup, o.apellido AS apesup, p.estado, a.codigocli, a.ubicacion, TIMESTAMPDIFF(DAY, p.created_time, CURRENT_TIMESTAMP) AS antiguedad FROM presupuesto p INNER JOIN servicio s on p.idservicio=s.idservicio INNER JOIN tservicio t ON s.idtservicio=t.idtservicio INNER JOIN ascensor a ON s.idascensor = a.idascensor INNER JOIN tascensor y ON a.idtascensor=y.idtascensor INNER JOIN marca m ON a.marca = m.idmarca INNER JOIN modelo n ON a.modelo = n.idmodelo INNER JOIN edificio e ON a.idedificio = e.idedificio INNER JOIN tecnico i ON s.idtecnico = i.idtecnico INNER JOIN supervisor o ON i.idsupervisor = o.idsupervisor INNER JOIN testado u ON s.estadofin = u.id WHERE p.condicion = 1 AND s.estadofin = 7 AND (p.estado = 1 OR p.estado = 2 OR p.estado =3) ORDER BY p.created_time ASC";
			return ejecutarConsulta($sql);
		}
                
                public function listar_pre(){
			$sql="SELECT DATE(p.created_time) AS created, DATE(p.process_time) AS process, DATE(p.revised_time) AS revised, DATE(p.aproved_time) AS aproved, DATE(p.sent_time) AS sent, p.idpresupuesto, p.informado, s.idservicio, a.codigo, u.nombre AS estadofin, e.nombre AS edificio, p.estado, x.nombre AS nomsup, x.apellido AS apesup, a.codigocli, a.ubicacion, p.file, p.fileapro, p.npresupuesto, p.orden FROM presupuesto p INNER JOIN servicio s on p.idservicio=s.idservicio INNER JOIN tservicio t ON s.idtservicio=t.idtservicio INNER JOIN ascensor a ON s.idascensor = a.idascensor INNER JOIN edificio e ON a.idedificio = e.idedificio INNER JOIN testado u ON s.estadofin = u.id INNER JOIN tecnico z ON s.idtecnico=z.idtecnico INNER JOIN supervisor x ON z.idsupervisor = x.idsupervisor WHERE p.condicion = 1 ORDER BY p.created_time ASC";
			return ejecutarConsulta($sql);
		}
                
                public function listar_sup($rut){
			$sql="SELECT DATE(p.created_time) AS created, DATE(p.process_time) AS process, DATE(p.revised_time) AS revised, DATE(p.sent_time) AS sent, DATE(p.aproved_time) AS aproved, p.idpresupuesto, p.informado, p.file, p.fileapro, s.idservicio, a.codigo, a.codigocli, a.ubicacion, u.nombre AS estadofin, e.nombre AS edificio, p.estado, p.npresupuesto, p.orden FROM presupuesto p INNER JOIN servicio s on p.idservicio=s.idservicio INNER JOIN tservicio t ON s.idtservicio=t.idtservicio INNER JOIN ascensor a ON s.idascensor = a.idascensor INNER JOIN edificio e ON a.idedificio = e.idedificio INNER JOIN testado u ON s.estadofin = u.id WHERE p.condicion = 1 AND s.idtecnico IN (SELECT t.idtecnico FROM tecnico t INNER JOIN supervisor s ON t.idsupervisor=s.idsupervisor WHERE s.rut='$rut') ORDER BY p.created_time ASC";
			return ejecutarConsulta($sql);
		}
                
                public function solpdf($idpresupuesto){
			$sql="SELECT p.idpresupuesto, s.idservicio, a.codigo, a.codigocli, a.ubicacion, q.nombre AS tascensor, m.nombre AS marca, n.nombre AS modelo, t.nombre AS tservicio, p.created_time, z.nombre AS nomtec, z.apellidos AS apetec, v.nombre AS nomsup, v.apellido AS apesup, i.nombre AS estadofin, p.descripcion, p.solinfo FROM presupuesto p INNER JOIN servicio s ON p.idservicio=s.idservicio INNER JOIN ascensor a ON s.idascensor = a.idascensor INNER JOIN edificio e ON a.idedificio = e.idedificio INNER JOIN tascensor q ON a.idtascensor = q.idtascensor INNER JOIN marca m ON a.marca=m.idmarca INNER JOIN modelo n ON a.modelo = n.idmodelo INNER JOIN tservicio t ON s.idtservicio = t.idtservicio INNER JOIN testado i ON s.estadofin = i.id INNER JOIN tecnico z ON s.idtecnico = z.idtecnico INNER JOIN supervisor v ON z.idsupervisor = v.idsupervisor WHERE p.idpresupuesto = '$idpresupuesto'";
			return ejecutarConsultaSimpleFila($sql);
		} 
                
                public function mostrar($idpresupuesto){
			$sql="SELECT p.idpresupuesto, a.codigo, q.nombre AS tascensor, m.nombre AS marca, n.nombre AS modelo, p.created_time, p.descripcion, p.descripcionrev, p.descripcionpre, e.nombre, p.solinfo FROM presupuesto p INNER JOIN servicio s ON p.idservicio=s.idservicio INNER JOIN ascensor a ON s.idascensor = a.idascensor INNER JOIN edificio e ON a.idedificio = e.idedificio INNER JOIN tascensor q ON a.idtascensor = q.idtascensor INNER JOIN marca m ON a.marca=m.idmarca INNER JOIN modelo n ON a.modelo = n.idmodelo WHERE p.idpresupuesto = '$idpresupuesto'";
			return ejecutarConsultaSimpleFila($sql);
		}
                
                public function mostrarpre($idpresupuesto){
			$sql="SELECT p.idpresupuesto, a.codigo, q.nombre AS tascensor, m.nombre AS marca, n.nombre AS modelo, p.created_time, p.descripcion, p.descripcionrev, e.nombre FROM presupuesto p INNER JOIN servicio s ON p.idservicio=s.idservicio INNER JOIN ascensor a ON s.idascensor = a.idascensor INNER JOIN edificio e ON a.idedificio = e.idedificio INNER JOIN tascensor q ON a.idtascensor = q.idtascensor INNER JOIN marca m ON a.marca=m.idmarca INNER JOIN modelo n ON a.modelo = n.idmodelo WHERE p.idpresupuesto = '$idpresupuesto'";
			return ejecutarConsultaSimpleFila($sql);
		}
                
                public function mostrarsol($idpresupuesto){
			$sql="SELECT p.idpresupuesto, a.codigo, q.nombre AS tascensor, m.nombre AS marca, n.nombre AS modelo, p.created_time, p.descripcion, e.nombre FROM presupuesto p INNER JOIN servicio s ON p.idservicio=s.idservicio INNER JOIN ascensor a ON s.idascensor = a.idascensor INNER JOIN edificio e ON a.idedificio = e.idedificio INNER JOIN tascensor q ON a.idtascensor = q.idtascensor INNER JOIN marca m ON a.marca=m.idmarca INNER JOIN modelo n ON a.modelo = n.idmodelo WHERE p.idpresupuesto = '$idpresupuesto'";
			return ejecutarConsultaSimpleFila($sql);
		}
                
                public function revisar($idpresupuesto, $descripcionrev, $iduser){
                        $sql="UPDATE presupuesto SET descripcionrev='$descripcionrev', revised_time=CURRENT_TIMESTAMP, revised_user='$iduser', estado='3', informado='1'  WHERE idpresupuesto='$idpresupuesto'";
			return ejecutarConsulta($sql);
		}
                
                public function foto($idpresupuesto, $file, $base64){
			$sql="INSERT INTO imagenespre(idpresupuesto, file, base64) VALUES ('$idpresupuesto','$file','$base64')";
			return ejecutarConsulta($sql);
		}
                
                public function imgref($idpresupuesto){
			$sql="SELECT file, base64 FROM imagenespre WHERE idpresupuesto='$idpresupuesto'";
			return ejecutarConsulta($sql);
		}
                               
                public function enviado($idpresupuesto){
                        $sql="UPDATE presupuesto SET estado='5', sent_time=CURRENT_TIMESTAMP WHERE idpresupuesto='$idpresupuesto'";
			return ejecutarConsulta($sql);
		}
                
                public function informacion($idpresupuesto, $solinfo, $iduser){
                        $sql="UPDATE presupuesto SET solinfo='$solinfo', estado='2', request_time=CURRENT_TIMESTAMP, request_user='$iduser' WHERE idpresupuesto='$idpresupuesto'";
			return ejecutarConsulta($sql);
		}
                
                public function procesar($idpresupuesto, $descripcionpre, $file, $npresupuesto, $iduser){
                        $sql="UPDATE presupuesto SET descripcionpre='$descripcionpre', file='$file', npresupuesto='$npresupuesto', process_time=CURRENT_TIMESTAMP, process_user='$iduser', estado='4' WHERE idpresupuesto='$idpresupuesto'";
			return ejecutarConsulta($sql);
		}
                
                public function aprobar($idpresupuesto, $descripcionapro, $fileapro, $orden){
                        $sql="UPDATE presupuesto SET descripcionapro='$descripcionapro', fileapro='$fileapro', orden='$orden', aproved_time=CURRENT_TIMESTAMP, estado='6' WHERE idpresupuesto='$idpresupuesto'";
			return ejecutarConsulta($sql);
		}
                
                public function revpdf($idpresupuesto){
			$sql="SELECT p.idpresupuesto, s.idservicio, a.codigo, a.codigocli, a.ubicacion, q.nombre AS tascensor, m.nombre AS marca, n.nombre AS modelo, t.nombre AS tservicio, p.created_time, z.nombre AS nomtec, z.apellidos AS apetec, v.nombre AS nomsup, v.apellido AS apesup, i.nombre AS estadofin, p.descripcion, p.descripcionrev, p.revised_time, u.nombre AS revnom, u.apellido AS revape, p.npresupuesto, p.orden, p.process_time, p.sent_time, p.aproved_time, p.descripcionpre, p.descripcionapro, p.solinfo FROM presupuesto p INNER JOIN servicio s ON p.idservicio=s.idservicio INNER JOIN ascensor a ON s.idascensor = a.idascensor INNER JOIN edificio e ON a.idedificio = e.idedificio INNER JOIN tascensor q ON a.idtascensor = q.idtascensor INNER JOIN marca m ON a.marca=m.idmarca INNER JOIN modelo n ON a.modelo = n.idmodelo INNER JOIN tservicio t ON s.idtservicio = t.idtservicio INNER JOIN testado i ON s.estadofin = i.id INNER JOIN tecnico z ON s.idtecnico = z.idtecnico INNER JOIN supervisor v ON z.idsupervisor = v.idsupervisor INNER JOIN user u ON p.revised_user = u.iduser WHERE p.idpresupuesto = '$idpresupuesto'";
			return ejecutarConsultaSimpleFila($sql);
		}
                
                public function email($idpresupuesto){
			$sql="SELECT p.idpresupuesto, s.idservicio, s.created_time AS feser, z.nombre AS estadofin, t.nombre AS nomtec, t.apellidos AS apetec, s.observacionfin, a.codigo, a.ubicacion, a.codigocli, q.nombre AS tascensor, m.nombre AS marca, n.nombre AS modelo, p.created_time, p.descripcion, e.nombre, e.calle, e.numero, p.solinfo, x.nombre AS nomreq, x.apellido AS apereq, p.request_time FROM presupuesto p INNER JOIN servicio s ON p.idservicio=s.idservicio INNER JOIN ascensor a ON s.idascensor = a.idascensor INNER JOIN edificio e ON a.idedificio = e.idedificio INNER JOIN tascensor q ON a.idtascensor = q.idtascensor INNER JOIN marca m ON a.marca=m.idmarca INNER JOIN modelo n ON a.modelo = n.idmodelo INNER JOIN testado z ON s.estadofin = z.id INNER JOIN tecnico t ON s.idtecnico = t.idtecnico INNER JOIN user x ON p.request_user = x.iduser WHERE p.idpresupuesto = '$idpresupuesto'";
			return ejecutarConsultaSimpleFila($sql);
		}
                
                public function inhabilitar($idpresupuesto, $iduser){
                        $sql="UPDATE presupuesto SET closed_user='$iduser', closed_time=CURRENT_TIMESTAMP, condicion=0 WHERE idpresupuesto='$idpresupuesto'";
			return ejecutarConsulta($sql);
		}
                 
                 */
	}
?>