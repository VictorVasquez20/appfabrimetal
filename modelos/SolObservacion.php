<?php
require_once '../config/conexion.php';
/**
 * Description of solObservacion
 *
 * @author azuni
 */
class SolObservacion {
    //put your code here
    
    function insertar($idsolicitud, $iduser, $observacion){
        $sql = "INSERT INTO `solobservacion`(`idsolicitud`, `iduser`, `observacion`) "
                . "VALUES ($idsolicitud, $iduser, '$observacion')";
        return ejecutarConsulta($sql);
    }
    
    function editar($idobservacion, $idsolicitud, $iduser, $observacion){
        $sql = "UPDATE `solobservacion` SET "
                . "`idsolicitud`= $idsolicitud,"
                . "`iduser`= $iduser,"
                . "`observacion`= '$observacion' "
                . "WHERE `idobservacion`= $idobservacion";
        return ejecutarConsulta($sql);
    }
    
    function listar($idsolicitud){
        $sql = "SELECT o.`idobservacion`, o.`idsolicitud`, o.`iduser`, o.`observacion`, o.`created_time` , concat(u.nombre, ' ', u.apellido) as 'nombre'
                FROM `solobservacion` o
                INNER JOIN user u on u.iduser = o.iduser
                WHERE o.`idsolicitud`= $idsolicitud 
                ORDER BY created_time";
        return ejecutarConsulta($sql);
    }
}
