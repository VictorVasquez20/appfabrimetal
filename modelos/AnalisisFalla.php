<?php 

require "../config/conexion.php";

	Class AnalisisFalla{
		//Constructor para instancias
		public function __construct(){

		}

        public function insertar($obra, $tipoFalla, $descripcionFalla, $imputable, $requerimiento, $descRequerimiento, $responsable, $imgs, $dTime){
            $sql = "INSERT INTO "
                    . "`analisis_falla`(`obra`, `tipofalla`, `descripcionfalla`, `imputable`, `requerimiento`, `descrequerimiento`, `responsable`, `fecha`) "
                    . " VALUES ( $obra, '$tipoFalla', '$descripcionFalla', '$imputable', '$requerimiento','$descRequerimiento',$responsable,SYSDATE())";
            //return ejecutarConsulta_retornarID($sql);
            //echo $sql;
            $result = ejecutarConsu_retornarID($sql);
            if ($result)
            {
                //$imgs es array
                foreach ($imgs as $imgname) {
                    if ($imgname)
                    {
                        $i++;
                        $extension = end(explode(".", $imgname));
                        $imgname = $imgname . '___' . $dTime . '.TMP';
                        $newImgName = 'analisisfalla-' . $result . '-' . $i . '-' . date('YmdHis') . '.' . $extension;
                        rename("../../appfabrimetal/files/docsasociados/$imgname", "../../appfabrimetal/files/docsasociados/$newImgName");
                        $sql="INSERT INTO docsasociados (proceso, tabla, datoid, documento) VALUES ('APPFABRIMETAL_ANALISISFALLA', 'analisisfalla','$result','$newImgName')";

                        ejecutarConsulta($sql);
                    }
                }

            }
            return $result;
        }

        public function responsables(){
            $sql = "SELECT iduser, nombre, apellido
                    FROM user
                    WHERE idrole IN (23,24) 
                    AND estado = 1
                    ORDER BY 2, 3 ASC";
            return ejecutarConsulta($sql);
        }

        public function listaranalisis(){
            $sql = "SELECT af.idanalisis_falla, p.nombre obra, af.tipofalla, af.descripcionfalla, af.imputable, af.requerimiento, 
                            af.descrequerimiento, CONCAT(u.nombre, ' ', u.apellido) responsable, af.fecha
                    FROM analisis_falla af
                    INNER JOIN proyecto p ON af.obra = p.idproyecto
                    INNER JOIN user u ON af.responsable = u.iduser
                    WHERE af.estado = 'ingresado';"; //obs_ji IS NULL
            return ejecutarConsulta($sql);
        }    
        
        public function listaranalisis2($idanalisis){
            $sql = "SELECT af.idanalisis_falla, p.nombre obra, af.tipofalla, af.descripcionfalla, af.imputable, af.requerimiento, 
                            af.descrequerimiento, CONCAT(u.nombre, ' ', u.apellido) responsable, af.fecha, af.obs_ji, af.responsable AS idresponsable, u.email
                    FROM analisis_falla af
                    INNER JOIN proyecto p ON af.obra = p.idproyecto
                    INNER JOIN user u ON af.responsable = u.iduser
                    WHERE af.idanalisis_falla = $idanalisis;";
            return ejecutarConsultaSimpleFila($sql);
        }  

        public function guardarSup($idanalisis, $obs_ji){
            $sql = "UPDATE analisis_falla af
                    SET obs_ji = '$obs_ji', estado = 'aprobado'
                    WHERE idanalisis_falla = $idanalisis;";
            return ejecutarConsulta($sql);
        }

        public function rechazar($idanalisis){
            $sql = "UPDATE analisis_falla af
                    SET estado = 'rechazado'
                    WHERE idanalisis_falla = $idanalisis;";
            return ejecutarConsulta($sql);
        }

    }