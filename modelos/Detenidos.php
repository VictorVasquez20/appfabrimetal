<?php

require_once '../config/conexion.php';

class Detenidos{
    function __construct() {
        
    }
    public function listarDetenidos(){
        $sql = "SELECT idascensor
                FROM ascensor 
                where estado = 7;";
        return ejecutarConsulta($sql);
    }

    public function detenidos() {
        $sql = "SELECT COUNT(idascensor) AS total FROM ascensor WHERE estado = 7";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function evaluacion() {
        $sql = "SELECT COUNT(idascensor) AS evaluacion FROM ascensor WHERE estado = 7";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function cotizados() {
        $sql = "SELECT COUNT(idascensor) AS cotizados FROM ascensor WHERE estado = 7";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function aprobados() {
        $sql = "SELECT COUNT(idascensor) AS aprobados FROM ascensor WHERE estado = 7";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function listaDatos($idascensor){
        $sql = "SELECT a.idascensor, s.idservicio, e.nombre edificio, a.codigo, s.closed_time, CONCAT(t.nombre, ' ',t.apellidos) tecnico, UPPER(s.observacionfin) observacionfin, 
                        datediff(sysdate(),s.closed_time) dias_detenido, r.region_ordinal region,
                        p.idpresupuesto, p.estado
                FROM servicio s
                INNER JOIN ascensor a ON s.idascensor = a.idascensor
                INNER JOIN edificio e ON a.idedificio = e.idedificio
                INNER JOIN regiones r ON e.idregiones = r.region_id
                INNER JOIN tecnico t ON s.idtecnico = t.idtecnico
                LEFT JOIN presupuesto p ON p.idservicio = s.idservicio
                WHERE s.idascensor = $idascensor
                ORDER BY s.idservicio DESC
                LIMIT 1;";
        return ejecutarConsulta($sql);
    }

    public function IngresoGSEDetenidosPorMes($anio_busq){
        
        $sql = "SELECT YEAR(created_time) as anio, MONTH(created_time) as mes ,count(idservicio) as cantidad
                FROM servicio
                WHERE YEAR(created_time) = $anio_busq
                GROUP BY YEAR(created_time),MONTH(created_time)
                ORDER BY YEAR(created_time),MONTH(created_time)";
        return ejecutarConsulta($sql);
    }
    
}